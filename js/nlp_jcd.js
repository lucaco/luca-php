
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
// ::::::::::::: Natural Lenguage Processing function Spanish ::::::::::::::
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

// StopWords
const StopWords = ['de', 'la', 'que', 'el', 'en', 'y', 'a', 'los', 'del', 'se', 'las', 'por', 'un', 'para', 'con', 'no', 'una', 'su', 'al', 'lo', 'como', 'más', 'pero', 'sus', 'le', 'ya', 'o', 'este', 'sí', 'porque', 'esta', 'entre', 'cuando', 'muy', 'sin', 'sobre', 'también', 'me', 'hasta', 'hay', 'donde', 'quien', 'desde', 'todo', 'nos', 'durante', 'todos', 'uno', 'les', 'ni', 'contra', 'otros', 'ese', 'eso', 'ante', 'ellos', 'e', 'esto', 'mí', 'antes', 'algunos', 'qué', 'unos', 'yo', 'otro', 'otras', 'otra', 'él', 'tanto', 'esa', 'estos', 'mucho', 'quienes', 'nada', 'muchos', 'cual', 'poco', 'ella', 'estar', 'estas', 'algunas', 'algo', 'nosotros', 'mi', 'mis', 'tú', 'te', 'ti', 'tu', 'tus', 'ellas', 'nosotras', 'vosostros', 'vosostras', 'os', 'mío', 'mía', 'míos', 'mías', 'tuyo', 'tuya', 'tuyos', 'tuyas', 'suyo', 'suya', 'suyos', 'suyas', 'nuestro', 'nuestra', 'nuestros', 'nuestras', 'vuestro', 'vuestra', 'vuestros', 'vuestras', 'esos', 'esas', 'estoy', 'estás', 'está', 'estamos', 'estáis', 'están', 'esté', 'estés', 'estemos', 'estéis', 'estén', 'estaré', 'estarás', 'estará', 'estaremos', 'estaréis', 'estarán', 'estaría', 'estarías', 'estaríamos', 'estaríais', 'estarían', 'estaba', 'estabas', 'estábamos', 'estabais', 'estaban', 'estuve', 'estuviste', 'estuvo', 'estuvimos', 'estuvisteis', 'estuvieron', 'estuviera', 'estuvieras', 'estuviéramos', 'estuvierais', 'estuvieran', 'estuviese', 'estuvieses', 'estuviésemos', 'estuvieseis', 'estuviesen', 'estando', 'estado', 'estada', 'estados', 'estadas', 'estad', 'he', 'has', 'ha', 'hemos', 'habéis', 'han', 'haya', 'hayas', 'hayamos', 'hayáis', 'hayan', 'habré', 'habrás', 'habrá', 'habremos', 'habréis', 'habrán', 'habría', 'habrías', 'habríamos', 'habríais', 'habrían', 'había', 'habías', 'habíamos', 'habíais', 'habían', 'hube', 'hubiste', 'hubo', 'hubimos', 'hubisteis', 'hubieron', 'hubiera', 'hubieras', 'hubiéramos', 'hubierais', 'hubieran', 'hubiese', 'hubieses', 'hubiésemos', 'hubieseis', 'hubiesen', 'habiendo', 'habido', 'habida', 'habidos', 'habidas', 'soy', 'eres', 'es', 'somos', 'sois', 'son', 'sea', 'seas', 'seamos', 'seáis', 'sean', 'seré', 'serás', 'será', 'seremos', 'seréis', 'serán', 'sería', 'serías', 'seríamos', 'seríais', 'serían', 'era', 'eras', 'éramos', 'erais', 'eran', 'fui', 'fuiste', 'fue', 'fuimos', 'fuisteis', 'fueron', 'fuera', 'fueras', 'fuéramos', 'fuerais', 'fueran', 'fuese', 'fueses', 'fuésemos', 'fueseis', 'fuesen', 'sintiendo', 'sentido', 'sentida', 'sentidos', 'sentidas', 'siente', 'sentid', 'tengo', 'tienes', 'tiene', 'tenemos', 'tenéis', 'tienen', 'tenga', 'tengas', 'tengamos', 'tengáis', 'tengan', 'tendré', 'tendrás', 'tendrá', 'tendremos', 'tendréis', 'tendrán', 'tendría', 'tendrías', 'tendríamos', 'tendríais', 'tendrían', 'tenía', 'tenías', 'teníamos', 'teníais', 'tenían', 'tuve', 'tuviste', 'tuvo', 'tuvimos', 'tuvisteis', 'tuvieron', 'tuviera', 'tuvieras', 'tuviéramos', 'tuvierais', 'tuvieran', 'tuviese', 'tuvieses', 'tuviésemos', 'tuvieseis', 'tuviesen', 'teniendo', 'tenido', 'tenida', 'tenidos', 'tenidas', 'tened'];
// Preposiciones
const Preps = ["a", "ante", "bajo", "cabe", "con", "contra", "de", "desde", "durante", "en", "entre", "hacia", "hasta", "mediante", "para", "por", "según", "sin", "so", "sobre", "tras", "versus", "vía"];
// Preposiciones de tiempo
const PrepsTempo = ["a", "antes de", "de", "dentro de", "desde", "desde hace", "después de", "durante", "en", "hasta", "por", "sobre", "tras"];
// Preposiciones Locativas
const PrepsLocat = ["a", "a la derecha de", "a la izquierda de", "al lado de", "alrededor de", "a través de", "cerca de", "contra", "de", "debajo de", "delante de", "detrás de", "encima de", "enfrente de", "entre", "fuera de", "hacia", "junto a", "lejos de", "por", "sobre", "tras"]
// Artículos
const Artic = ["un", "una", "unos", "unas", "el", "los", "la", "las", "lo"];
// Artículos Numero (Singular o Plural)
const ArticNumer = ["Singular", "Singular", "Plurar", "Plurar", "Singular", "Plurar", "Singular", "Plurar", "Singular"];
// Artículos por tipo (Indeterminado, Determinado o Neutro)
const ArticType = ["Indeterminado", "Indeterminado", "Indeterminado", "Indeterminado", "Determinado", "Determinado", "Determinado", "Determinado", "Neutro"];
// Verbos
const Verbs = ['abarcar', 'abolir', 'abonar', 'abordar', 'abrazar', 'abrir', 'absorber', 'acabar', 'acatar', 'acceder', 'acelerar', 'aceptar', 'acercar', 'aclarar', 'acoger', 'acompañar', 'acordar', 'acordar', 'acortar', 'acostarse', 'acrecentar', 'activar', 'actualizar', 'actuar', 'acudir', 'acumular', 'acusar', 'adaptar', 'adecuar', 'adelantar', 'adivinar', 'admirar', 'admitir', 'adoptar', 'adquirir', 'advertir', 'afectar', 'afianzar', 'afiliarse', 'afirmar', 'afrontar', 'agarrar', 'agilizar', 'agradecer', 'agravar', 'agregar', 'agrupar', 'aguantar', 'ahorrar', 'aislar', 'ajustar', 'albergar', 'alcanzar', 'alejar', 'alejarse', 'alentar', 'alertar', 'alimentar', 'aliviar', 'almacenar', 'almorzar', 'alojar', 'alquilar', 'alterar', 'amanecer', 'amar', 'amenazar', 'ampliar', 'añadir', 'analizar', 'andar', 'animar', 'anotar', 'anticipar', 'anular', 'anunciar', 'apagar', 'aparecer', 'apelar', 'aplastar', 'aplazar', 'aplicar', 'aportar', 'apostar', 'apoyar', 'apreciar', 'aprender', 'apretar', 'apretar', 'aprobar', 'aprovechar', 'apuntar', 'argüir', 'armar', 'armonizar', 'arrancar', 'arrastrar', 'arreglar', 'arrestar', 'arriesgar', 'arrojar', 'arruinar', 'articular', 'ascender', 'asegurar', 'asesorar', 'asignar', 'asimilar', 'asistir', 'asociar', 'asumir', 'asustar', 'atacar', 'atajar', 'atar', 'atardecer', 'atender', 'atenuar', 'aterrizar', 'atraer', 'atrapar', 'atravesar', 'atribuir', 'aumentar', 'autorizar', 'avanzar', 'avergonzar', 'averiguar', 'avisar', 'ayudar', 'bailar', 'bajar', 'basar', 'beber', 'beneficiar', 'besar', 'bloquear', 'borrar', 'brillar', 'brindar', 'buscar', 'caber', 'caer', 'calcular', 'calentar', 'calificar', 'callar', 'calmar', 'cambiar', 'caminar', 'canalizar', 'cancelar', 'cantar', 'capacitar', 'captar', 'capturar', 'carecer', 'cargar', 'casar', 'casarse', 'castigar', 'causar', 'cavar', 'cazar', 'ceder', 'celebrar', 'cenar', 'centrar', 'cerrar', 'certificar', 'cesar', 'charlar', 'circular', 'citar', 'clarificar', 'clasificar', 'cobrar', 'cocer', 'cocinar', 'coger', 'coincidir', 'colaborar', 'colgar', 'colmar', 'colocar', 'combatir', 'combinar', 'comentar', 'comenzar', 'comer', 'comercializar', 'cometer', 'comparar', 'comparecer', 'compartir', 'compensar', 'competir', 'compilar', 'complacer', 'complementar', 'completar', 'comprar', 'comprender', 'comprobar', 'comprometer', 'comunicar', 'concebir', 'conceder', 'concentrar', 'concertar', 'concienciar', 'conciliar', 'concluir', 'concretar', 'condenar', 'conducir', 'conectar', 'conferir', 'confesar', 'confiar', 'configurar', 'confirmar', 'confundir', 'congelar', 'conmemorar', 'conocer', 'conquistar', 'conseguir', 'conservar', 'considerar', 'consignar', 'consolidar', 'constar', 'constatar', 'constituir', 'construir', 'consultar', 'consumir', 'contactar', 'contar', 'contemplar', 'contener', 'contestar', 'continuar', 'contraer', 'contratar', 'contribuir', 'controlar', 'convencer', 'conversar', 'convertir', 'convivir', 'convocar', 'cooperar', 'coordinar', 'copiar', 'corregir', 'correr', 'corresponder', 'cortar', 'coser', 'costar', 'crear', 'crecer', 'creer', 'criar', 'criticar', 'cruzar', 'cuantificar', 'cubrir', 'cuestionar', 'cuidar', 'culpar', 'cultivar', 'cumplir', 'curar', 'dañar', 'dar', 'debatir', 'deber', 'debilitar', 'decidir', 'decidirse', 'decir', 'declarar', 'dedicar', 'deducir', 'defender', 'definir', 'dejar', 'delegar', 'delimitar', 'demandar', 'demorar', 'demostrar', 'denegar', 'denunciar', 'depender', 'depositar', 'derogar', 'derribar', 'derrotar', 'desactivar', 'desafiar', 'desalentar', 'desaparecer', 'desarmar', 'desarrollar', 'desayunar', 'descansar', 'descargar', 'descartar', 'descender', 'descifrar', 'describir', 'descubrir', 'desear', 'desempeñar', 'desestabilizar', 'deshacer', 'designar', 'desmantelar', 'despedir', 'despegar', 'despejar', 'desperdiciar', 'despertar', 'desplazar', 'desplegar', 'destacar', 'destinar', 'destrozar', 'destruir', 'desviar', 'detectar', 'detener', 'determinar', 'devolver', 'diagnosticar', 'dialogar', 'dibujar', 'dictar', 'diferenciar', 'difundir', 'dirigir', 'discernir', 'discriminar', 'discutir', 'diseñar', 'disfrutar', 'disipar', 'disminuir', 'disolver', 'disparar', 'disponer', 'distinguir', 'distribuir', 'disuadir', 'diversificar', 'dividir', 'divulgar', 'doblar', 'documentar', 'doler', 'dominar', 'donar', 'dormir', 'dotar', 'dudar', 'duplicar', 'durar', 'echar', 'editar', 'educar', 'efectuar', 'ejecutar', 'ejercer', 'elaborar', 'elegir', 'elevar', 'eliminar', 'eludir', 'emitir', 'empacar', 'empeorar', 'empezar', 'emplear', 'emprender', 'empujar', 'encajar', 'encarar', 'encargar', 'encauzar', 'encender', 'encerrar', 'encomiar', 'encontrar', 'encubrir', 'enfocar', 'enfrentar', 'engañar', 'engrosar', 'enjuiciar', 'enmendar', 'enraizar', 'enriquecer', 'ensayar', 'enseñar', 'entablar', 'entender', 'enterrar', 'entrañar', 'entrar', 'entregar', 'entrenar', 'entrevistar', 'enumerar', 'envejecer', 'enviar', 'equilibrar', 'erradicar', 'errar', 'escalar', 'escapar', 'esclarecer', 'escoger', 'esconder', 'escribir', 'escuchar', 'esparcir', 'especificar', 'esperar', 'espiar', 'estabilizar', 'establecer', 'estallar', 'estar', 'estimar', 'estimular', 'estrechar', 'estructurar', 'estudiar', 'evacuar', 'evadir', 'evaluar', 'evitar', 'evolucionar', 'examinar', 'exceder', 'excluir', 'exhortar', 'exigir', 'existir', 'expandir', 'expedir', 'experimentar', 'explicar', 'explorar', 'explotar', 'exponer', 'exportar', 'expresar', 'expulsar', 'extender', 'extraer', 'extrañar', 'fabricar', 'facilitar', 'fallar', 'faltar', 'favorecer', 'felicitar', 'figurar', 'fijar', 'filmar', 'finalizar', 'financiar', 'fingir', 'firmar', 'fomentar', 'forjar', 'formar', 'formular', 'fortalecer', 'forzar', 'fracasar', 'frenar', 'fumar', 'funcionar', 'fundamentar', 'fundar', 'ganar', 'garantizar', 'gastar', 'gemir', 'generar', 'gestionar', 'girar', 'golpear', 'grabar', 'granizar', 'gritar', 'guardar', 'guiar', 'gustar', 'haber', 'habilitar', 'hablar', 'hacer', 'hallar', 'helar', 'hendir', 'heredar', 'herir', 'honrar', 'huir', 'idear', 'identificar', 'ignorar', 'ilustrar', 'imaginar', 'imitar', 'impartir', 'impedir', 'implantar', 'implementar', 'implicar', 'imponer', 'importar', 'impresionar', 'imprimir', 'impugnar', 'impulsar', 'incentivar', 'incitar', 'incluir', 'incoar', 'incorporar', 'incrementar', 'indemnizar', 'indicar', 'inducir', 'influir', 'informar', 'ingresar', 'iniciar', 'inscribir', 'insertar', 'insistir', 'inspeccionar', 'inspirar', 'instalar', 'instaurar', 'institucionalizar', 'instituir', 'integrar', 'intensificar', 'intentar', 'interactuar', 'intercambiar', 'interponer', 'interpretar', 'interrogar', 'interrumpir', 'intervenir', 'intimidar', 'introducir', 'invadir', 'inventar', 'invertir', 'investigar', 'invitar', 'invocar', 'involucrar', 'ir', 'jugar', 'juntar', 'justificar', 'juzgar', 'lamentar', 'lanzar', 'lastimar', 'lavar', 'leer', 'legislar', 'levantar', 'levantarse', 'liberar', 'librar', 'liderar', 'lidiar', 'ligar', 'limitar', 'limpiar', 'liquidar', 'llamar', 'llegar', 'llenar', 'llevar', 'llorar', 'llover', 'llover', 'localizar', 'lograr', 'luchar', 'lucir', 'lucir', 'maldecir', 'mandar', 'manejar', 'manifestar', 'manipular', 'mantener', 'marcar', 'marchar', 'matar', 'maximizar', 'medir', 'mejorar', 'mencionar', 'menoscabar', 'mentir', 'meter', 'mezclar', 'militar', 'minimizar', 'mirar', 'mitigar', 'modernizar', 'modificar', 'molestar', 'montar', 'morder', 'morder', 'morir', 'mostrar', 'motivar', 'mover', 'moverse', 'movilizar', 'mudar', 'multiplicar', 'muscular', 'nacer', 'nadar', 'navegar', 'necesitar', 'negar', 'negociar', 'neutralizar', 'nevar', 'neviscar', 'nombrar', 'notar', 'notificar', 'obedecer', 'obligar', 'obrar', 'observar', 'obstaculizar', 'obtener', 'ocultar', 'ocupar', 'ocurrir', 'odiar', 'ofrecer', 'oír', 'oler', 'olvidar', 'olvidarse', 'operar', 'optar', 'optimizar', 'orar', 'ordenar', 'organizar', 'orientar', 'otorgar', 'padecer', 'pagar', 'paliar', 'parar', 'parecer', 'participar', 'partir', 'pasar', 'pasear', 'pedir', 'pegar', 'pelear', 'penetrar', 'pensar', 'pensar', 'percibir', 'perder', 'perdonar', 'perfeccionar', 'perjudicar', 'permanecer', 'permitir', 'perpetuar', 'perseguir', 'persuadir', 'perturbar', 'pesar', 'pescar', 'pillar', 'pintar', 'pisar', 'placer', 'planear', 'planificar', 'plantar', 'plantear', 'poblar', 'poder', 'podrir', 'poner', 'poseer', 'posibilitar', 'posponer', 'potenciar', 'practicar', 'precisar', 'predecir', 'predecir', 'preferir', 'preguntar', 'preocupar', 'preocuparse', 'preparar', 'prescindir', 'presenciar', 'presentar', 'preservar', 'presidir', 'presionar', 'prestar', 'presumir', 'pretender', 'prevalecer', 'prevenir', 'prever', 'privar', 'probar', 'proceder', 'procesar', 'procurar', 'producir', 'profundizar', 'programar', 'progresar', 'prohibir', 'prolongar', 'prometer', 'promocionar', 'promover', 'promulgar', 'pronunciar', 'propiciar', 'proponer', 'proporcionar', 'prorrogar', 'proseguir', 'prosperar', 'proteger', 'protestar', 'proveer', 'provocar', 'proyectar', 'pudrir', 'pulsar', 'quebrar', 'quedar', 'quedarse', 'quejarse', 'quemar', 'querer', 'quitar', 'racionalizar', 'rastrear', 'ratificar', 'reaccionar', 'reactivar', 'reafirmar', 'realizar', 'realzar', 'reanudar', 'rebajar', 'recalcar', 'recaudar', 'rechazar', 'recibir', 'reclamar', 'reclutar', 'recoger', 'recomendar', 'reconocer', 'reconsiderar', 'reconstruir', 'recopilar', 'recordar', 'recorrer', 'recortar', 'rectificar', 'recuperar', 'recurrir', 'redactar', 'reducir', 'reembolsar', 'reemplazar', 'reestructurar', 'reflejar', 'reflexionar', 'reformar', 'reforzar', 'registrar', 'reglamentar', 'regresar', 'regular', 'rehabilitar', 'rehusar', 'reiniciar', 'reír', 'reiterar', 'rellenar', 'remediar', 'remitir', 'remover', 'rendir', 'rendir', 'renovar', 'renunciar', 'reorganizar', 'reorientar', 'reparar', 'repartir', 'repasar', 'repetir', 'reportar', 'representar', 'reprimir', 'reproducir', 'requerir', 'resaltar', 'rescatar', 'reservar', 'residir', 'resistir', 'resolver', 'respaldar', 'respetar', 'respirar', 'responder', 'restablecer', 'restaurar', 'restringir', 'resultar', 'resumir', 'retener', 'retirar', 'retirarse', 'retomar', 'retornar', 'retrasar', 'retroceder', 'reunir', 'revelar', 'revertir', 'revisar', 'revitalizar', 'revivir', 'revocar', 'rezar', 'robar', 'rodar', 'roer', 'rogar', 'romper', 'rugir', 'saber', 'sacar', 'sacrificar', 'salir', 'saltar', 'saludar', 'salvaguardar', 'salvar', 'sanar', 'sancionar', 'sangrar', 'satisfacer', 'secuestrar', 'seguir', 'seleccionar', 'sembrar', 'sembrar', 'señalar', 'sensibilizar', 'sentar', 'sentarse', 'sentir', 'separar', 'ser', 'serrar', 'servir', 'significar', 'simplificar', 'simular', 'situar', 'sobrepasar', 'sobrevivir', 'socavar', 'solar', 'solicitar', 'soltar', 'solucionar', 'someter', 'sonar', 'soñar', 'sonreír', 'soportar', 'sorprender', 'sospechar', 'sostener', 'subestimar', 'subir', 'subrayar', 'subsanar', 'suceder', 'sufragar', 'sufrir', 'sugerir', 'suicidarse', 'sumar', 'superar', 'supervisar', 'suponer', 'suprimir', 'surgir', 'suscitar', 'suscribir', 'suspender', 'sustentar', 'sustituir', 'tardar', 'temblar', 'temer', 'tender', 'tener', 'terminar', 'testificar', 'tirar', 'tocar', 'tomar', 'trabajar', 'traer', 'tragar', 'traicionar', 'tramitar', 'transferir', 'trasladar', 'traspasar', 'tratar', 'trazar', 'triunfar', 'ubicar', 'ultimar', 'unir', 'usar', 'utilizar', 'vaciar', 'valer', 'validar', 'valorar', 'variar', 'velar', 'vencer', 'vender', 'vengar', 'venir', 'ventar', 'ver', 'verificar', 'vestir', 'viajar', 'vigilar', 'vincular', 'visitar', 'visualizar', 'vivir', 'volar', 'volver', 'votar', 'yacer']
// Terminaciones Masculinas
const TerminMasc = ["o", "aje", "ambre", "ar", "er", "or", "an", "en", "in", "on", "un", "ate", "ete", "ote", "é", "'és", "che", "l", "ma", "miento", "n", "pa", "ta", "x", "y"];
// Terminaciones Femeninas
const TerminFem = ["a", "ia", "ie", "ad", "ed", "id", "ud", "ez", "eza", "is", "ncia", "umbre", "z", "ción", "sión", "zón"];
// Conectores
const Conector = ["y", "o", "u"]
// Días de la semana
const DiasSemana = ["lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo"];
// Meses del año
const MesesAño = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
// Países
const Paises = ['albania', 'alemania', 'andorra', 'angola', 'antigua y barbuda', 'arabia saudita', 'argelia', 'argentina', 'armenia', 'australia', 'austria', 'azerbaiyán', 'bahamas', 'bangladés', 'barbados', 'baréin', 'bélgica', 'belice', 'benín', 'bielorrusia', 'birmania', 'bolivia', 'bosnia y herzegovina', 'botsuana', 'brasil', 'brunéi', 'bulgaria', 'burkina faso', 'burundi', 'bután', 'cabo verde', 'camboya', 'camerún', 'canadá', 'catar', 'chad', 'chile', 'china', 'chipre', 'ciudad del vaticano', 'colombia', 'comoras', 'corea del norte', 'corea del sur', 'costa de marfil', 'costa rica', 'croacia', 'cuba', 'dinamarca', 'dominica', 'ecuador', 'egipto', 'el salvador', 'emiratos árabes unidos', 'eritrea', 'eslovaquia', 'eslovenia', 'españa', 'estados unidos', 'estonia', 'etiopía', 'filipinas', 'finlandia', 'fiyi', 'francia', 'gabón', 'gambia', 'georgia', 'ghana', 'granada', 'grecia', 'guatemala', 'guyana', 'guinea', 'guinea-bisáu', 'guinea ecuatorial', 'haití', 'honduras', 'hungría', 'india', 'indonesia', 'irak', 'irán', 'irlanda', 'islandia', 'islas marshall', 'islas salomón', 'israel', 'italia', 'jamaica', 'japón', 'jordania', 'kazajistán', 'kenia', 'kirguistán', 'kiribati', 'kuwait', 'laos', 'lesoto', 'letonia', 'líbano', 'liberia', 'libia', 'liechtenstein', 'lituania', 'luxemburgo', 'madagascar', 'malasia', 'malaui', 'maldivas', 'malí', 'malta', 'marruecos', 'mauricio', 'mauritania', 'méxico', 'micronesia', 'moldavia', 'mónaco', 'mongolia', 'montenegro', 'mozambique', 'namibia', 'nauru', 'nepal', 'nicaragua', 'níger', 'nigeria', 'noruega', 'nueva zelanda', 'omán', 'países bajos', 'pakistán', 'palaos', 'panamá', 'papúa nueva guinea', 'paraguay', 'perú', 'polonia', 'portugal', 'reino unido de gran bretaña e irlanda del norte', 'república centroafricana', 'república checa', 'república de macedonia', 'república del congo', 'república democrática del congo', 'república dominicana', 'república sudafricana', 'ruanda', 'rumanía', 'rusia', 'samoa', 'san cristóbal y nieves', 'san marino', 'san vicente y las granadinas', 'santa lucía', 'santo tomé y príncipe', 'senegal', 'serbia', 'seychelles', 'sierra leona', 'singapur', 'siria', 'somalia', 'sri lanka', 'suazilandia', 'sudán', 'sudán del sur', 'suecia', 'suiza', 'surinam', 'tailandia', 'tanzania', 'tayikistán', 'timor oriental', 'togo', 'tonga', 'trinidad y tobago', 'túnez', 'turkmenistán', 'turquía', 'tuvalu', 'ucrania', 'uganda', 'uruguay', 'uzbekistán', 'vanuatu', 'venezuela', 'vietnam', 'yemen', 'yibuti', 'zambia', 'zimbabue'];
// Alfabeto
const Alfabeto = ["a", "b", "c", "d", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
// Vocales 
const Vocales = ["a", "e", "i", "o", "u"]
// Consonantes
const Consonantes = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "ñ", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"];
// Puntos Cardinales
const PuntosCard = ["norte", "sur", "este", "oeste", "oriente", "occidente", "sudeste", "sudoriente", "sudoccidente", "sudoeste", "noreste", "nororiente", "noroccidente", "noroeste"];
// Colores  (Conectado uno a uno con Colores JSON)
const Colors = ['rojo', 'colorado', 'carmesí', 'rubí', 'bermellón', 'escarlata', 'granate', 'burdeos', 'carmín', 'amaranto', 'verde', 'chartreuse', 'pasto', 'esmeralda', 'verde agua', 'agua marina', 'xanadu', 'jade', 'verde veronés', 'arlequín', 'espárrago', 'verde oliva', 'verde cazador', 'verde militar', 'azul', 'azul cobalto', 'azul marino', 'azul petróleo', 'azur', 'zafiro', 'añil', 'índigo', 'turquí', 'azul de prusia', 'azul majorelle', 'azul Klein', 'azul acero', 'magenta', 'fucsia', 'morado', 'malva', 'lila', 'salmón', 'lavanda', 'rosa', 'cian', 'turquesa', 'celeste', 'cerúleo', 'aguamarina', 'menta', 'amarillo', 'amarillo limón', 'lima', 'oro', 'dorado', 'ámbar', 'amarillo indio', 'topacio', 'amarillo selectivo', 'marrón', 'pardo', 'café', 'chocolate', 'caqui', 'ocre', 'siena', 'siena pálido', 'borgoña', 'violeta', 'lavanda floral', 'amatista', 'púrpura', 'púrpura de tiro', 'naranja', 'cara de luz', 'zanahoria', 'sésamo', 'albaricoque', 'beis', 'durazno', 'blanco', 'nieve', 'lino', 'hueso', 'marfil', 'plata', 'zinc', 'gris', 'negro', 'beige', 'plateado'];
// Colores JSON
const ColorsJson = [{ 'name': 'rojo', 'R': 255, 'G': 0, 'B': 0 }, { 'name': 'colorado', 'R': 255, 'G': 0, 'B': 0 }, { 'name': 'carmesí', 'R': 220, 'G': 20, 'B': 60 }, { 'name': 'rubí', 'R': 220, 'G': 20, 'B': 60 }, { 'name': 'bermellón', 'R': 227, 'G': 66, 'B': 51 }, { 'name': 'escarlata', 'R': 255, 'G': 36, 'B': 0 }, { 'name': 'granate', 'R': 128, 'G': 0, 'B': 0 }, { 'name': 'burdeos', 'R': 128, 'G': 0, 'B': 0 }, { 'name': 'carmín', 'R': 150, 'G': 0, 'B': 24 }, { 'name': 'amaranto', 'R': 229, 'G': 43, 'B': 80 }, { 'name': 'verde', 'R': 0, 'G': 255, 'B': 0 }, { 'name': 'chartreuse', 'R': 127, 'G': 255, 'B': 0 }, { 'name': 'pasto', 'R': 76, 'G': 187, 'B': 23 }, { 'name': 'esmeralda', 'R': 80, 'G': 200, 'B': 120 }, { 'name': 'verde agua', 'R': 80, 'G': 200, 'B': 120 }, { 'name': 'agua marina', 'R': 80, 'G': 200, 'B': 120 }, { 'name': 'xanadu', 'R': 115, 'G': 134, 'B': 120 }, { 'name': 'jade', 'R': 0, 'G': 168, 'B': 107 }, { 'name': 'verde veronés', 'R': 64, 'G': 130, 'B': 109 }, { 'name': 'arlequín', 'R': 68, 'G': 148, 'B': 74 }, { 'name': 'espárrago', 'R': 123, 'G': 160, 'B': 91 }, { 'name': 'verde oliva', 'R': 107, 'G': 142, 'B': 35 }, { 'name': 'verde cazador', 'R': 53, 'G': 94, 'B': 59 }, { 'name': 'verde militar', 'R': 53, 'G': 94, 'B': 59 }, { 'name': 'azul', 'R': 0, 'G': 0, 'B': 255 }, { 'name': 'azul cobalto', 'R': 0, 'G': 71, 'B': 171 }, { 'name': 'azul marino', 'R': 18, 'G': 10, 'B': 143 }, { 'name': 'azul petróleo', 'R': 1, 'G': 70, 'B': 99 }, { 'name': 'azur', 'R': 0, 'G': 0, 'B': 250 }, { 'name': 'zafiro', 'R': 1, 'G': 49, 'B': 180 }, { 'name': 'añil', 'R': 75, 'G': 0, 'B': 130 }, { 'name': 'índigo', 'R': 75, 'G': 0, 'B': 130 }, { 'name': 'turquí', 'R': 0, 'G': 0, 'B': 128 }, { 'name': 'azul de prusia', 'R': 0, 'G': 49, 'B': 83 }, { 'name': 'azul majorelle', 'R': 96, 'G': 80, 'B': 220 }, { 'name': 'azul Klein', 'R': 0, 'G': 47, 'B': 167 }, { 'name': 'azul acero', 'R': 176, 'G': 196, 'B': 222 }, { 'name': 'magenta', 'R': 255, 'G': 0, 'B': 255 }, { 'name': 'fucsia', 'R': 253, 'G': 63, 'B': 146 }, { 'name': 'morado', 'R': 197, 'G': 75, 'B': 140 }, { 'name': 'malva', 'R': 224, 'G': 176, 'B': 255 }, { 'name': 'lila', 'R': 200, 'G': 162, 'B': 200 }, { 'name': 'salmón', 'R': 254, 'G': 195, 'B': 172 }, { 'name': 'lavanda', 'R': 230, 'G': 230, 'B': 250 }, { 'name': 'rosa', 'R': 255, 'G': 192, 'B': 203 }, { 'name': 'cian', 'R': 0, 'G': 255, 'B': 255 }, { 'name': 'turquesa', 'R': 48, 'G': 213, 'B': 200 }, { 'name': 'celeste', 'R': 135, 'G': 206, 'B': 255 }, { 'name': 'cerúleo', 'R': 155, 'G': 196, 'B': 226 }, { 'name': 'aguamarina', 'R': 127, 'G': 255, 'B': 212 }, { 'name': 'menta', 'R': 127, 'G': 255, 'B': 212 }, { 'name': 'amarillo', 'R': 255, 'G': 255, 'B': 0 }, { 'name': 'amarillo limón', 'R': 253, 'G': 232, 'B': 15 }, { 'name': 'lima', 'R': 253, 'G': 232, 'B': 15 }, { 'name': 'oro', 'R': 255, 'G': 215, 'B': 0 }, { 'name': 'dorado', 'R': 255, 'G': 215, 'B': 0 }, { 'name': 'ámbar', 'R': 255, 'G': 192, 'B': 5 }, { 'name': 'amarillo indio', 'R': 227, 'G': 168, 'B': 87 }, { 'name': 'topacio', 'R': 227, 'G': 168, 'B': 87 }, { 'name': 'amarillo selectivo', 'R': 255, 'G': 186, 'B': 0 }, { 'name': 'marrón', 'R': 150, 'G': 75, 'B': 0 }, { 'name': 'pardo', 'R': 150, 'G': 75, 'B': 0 }, { 'name': 'café', 'R': 150, 'G': 75, 'B': 0 }, { 'name': 'chocolate', 'R': 150, 'G': 75, 'B': 0 }, { 'name': 'caqui', 'R': 148, 'G': 129, 'B': 43 }, { 'name': 'ocre', 'R': 204, 'G': 119, 'B': 34 }, { 'name': 'siena', 'R': 184, 'G': 115, 'B': 51 }, { 'name': 'siena pálido', 'R': 218, 'G': 138, 'B': 95 }, { 'name': 'borgoña', 'R': 128, 'G': 0, 'B': 32 }, { 'name': 'violeta', 'R': 139, 'G': 0, 'B': 255 }, { 'name': 'lavanda floral', 'R': 181, 'G': 126, 'B': 220 }, { 'name': 'amatista', 'R': 153, 'G': 102, 'B': 204 }, { 'name': 'púrpura', 'R': 102, 'G': 0, 'B': 153 }, { 'name': 'púrpura de tiro', 'R': 102, 'G': 2, 'B': 60 }, { 'name': 'naranja', 'R': 255, 'G': 112, 'B': 40 }, { 'name': 'cara de luz', 'R': 255, 'G': 127, 'B': 80 }, { 'name': 'zanahoria', 'R': 237, 'G': 145, 'B': 33 }, { 'name': 'sésamo', 'R': 255, 'G': 140, 'B': 105 }, { 'name': 'albaricoque', 'R': 251, 'G': 206, 'B': 177 }, { 'name': 'beis', 'R': 245, 'G': 222, 'B': 179 }, { 'name': 'durazno', 'R': 255, 'G': 200, 'B': 160 }, { 'name': 'blanco', 'R': 255, 'G': 255, 'B': 225 }, { 'name': 'nieve', 'R': 255, 'G': 255, 'B': 250 }, { 'name': 'lino', 'R': 250, 'G': 240, 'B': 230 }, { 'name': 'hueso', 'R': 245, 'G': 245, 'B': 220 }, { 'name': 'marfil', 'R': 255, 'G': 253, 'B': 208 }, { 'name': 'plata', 'R': 192, 'G': 192, 'B': 192 }, { 'name': 'zinc', 'R': 186, 'G': 196, 'B': 200 }, { 'name': 'gris', 'R': 128, 'G': 128, 'B': 128 }, { 'name': 'negro', 'R': 0, 'G': 0, 'B': 0 }, { 'name': 'beige', 'R': 245, 'G': 222, 'B': 179 }, { 'name': 'plateado', 'R': 192, 'G': 192, 'B': 192 }];

// a + el = al    (preposicion + artículo)
// de + el = del  (preposicion + artículo)

$(document).ready(function () {
  /*** LOAD JSON FILES ***/
  loadJSONS_All().then((r) => r);
})

/*:::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*:::::::::::::::: FUNCTION PRINCIPAL :::::::::::::::*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::*/
let nl_Tokenize = function (txt) {

  let newtxt = CleanPuntuation(txt);
  let x = newtxt.split(" ");
  let txtSW = nl_CleanStopWords(newtxt);
  let r = [];
  let output = [];
  let general = [];
  let Gente = [];
  let el, EspacioBlanco, NextWord, PrevWord, StpWord, Token, Pais, Mes, DiaSem, PntCard, color, NameHand, WebPage, isNumber;
  let Sentiment_value = 0;
  let Sentiment_result = 'Sin Data';
  let Sent_Val_Item = 0;
  let R_avg = 0, G_avg = 0, B_avg = 0, R_tot = 0, G_tot = 0, B_tot = 0, R_cnt = 0, G_cnt = 0, B_cnt = 0, HexAvg = '', ColoresArr = [];

  for (let i = 0; i < x.length; i++) {

    el = x[i].toLowerCase();

    // Análisis de Sentimiento
    var sntWr = SearchSentimentWord(el);
    Sent_Val_Item = (sntWr != undefined) ? sntWr.val : 0;
    Sentiment_value += Sent_Val_Item;

    // White Space And Next Word
    if (i == 0) {
      EspacioBlanco = { "antes": "", "despues": " " };
      NextWord = x[i + 1];
      PrevWord = "";
    } else if (i == x.length - 1) {
      EspacioBlanco = { "antes": " ", "despues": "" };
      NextWord = "";
      PrevWord = x[i - 1];
    } else {
      EspacioBlanco = { "antes": " ", "despues": " " };
      NextWord = x[i + 1];
      PrevWord = x[i - 1];
    }

    // StopWord
    StpWord = (StopWords.indexOf(el) > -1) ? true : false;

    // ---> Token
    if (Preps.indexOf(el) > -1) {
      Token = { "id": "PREP", "name": "preposición" };
    } else if (Artic.indexOf(el) > -1) {
      Token = { "id": "ARTI", "name": "artículo", "tipo": ArticType[Artic.indexOf(x[i])], "numero": ArticNumer[Artic.indexOf(x[i])] };
    } else if (Verbs.indexOf(el) > -1) {
      Token = { "id": "VERB", "name": "verbo", "tiempo": "presente" };
    } else if (BuscarAdjetivo(el) != undefined) {
      Token = { "id": "ADJE", "name": "adjetivo", "data": BuscarAdjetivo(el) };
    } else {
      Token = { "id": "", "name": "No encontrado" };
    }
    // Es Numero
    isNumber = !isNaN(el);
    // Es una Página Web
    WebPage = isWebPage(el);
    // Es un País
    Pais = (Paises.indexOf(el) > -1) ? true : false;
    // Es un mes
    Mes = (MesesAño.indexOf(el) > -1) ? true : false;
    // Es día del año
    DiaSem = (DiasSemana.indexOf(el) > -1) ? true : false;
    // Punto Cardinal
    PntCard = (PuntosCard.indexOf(el) > -1) ? true : false;
    // Color
    if (Colors.indexOf(el) > -1) {
      var R = ColorsJson[Colors.indexOf(el)].R;
      var G = ColorsJson[Colors.indexOf(el)].G;
      var B = ColorsJson[Colors.indexOf(el)].B;
      var HexColor = RGBtoHEX("rgb(" + R + "," + G + "," + B + ")");
      var ColorName = ColorsJson[Colors.indexOf(el)].name;
      R_tot += R;
      G_tot += G;
      B_tot += B;
      R_cnt += 1;
      G_cnt += 1;
      B_cnt += 1;
      color = { "isColor": true, "R": R, "G": G, "B": B, "HEX": HexColor };
      ColoresArr.push({ "color": ColorName, "R": R, "G": G, "B": B, "HEX": HexColor });
    } else {
      color = { "isColor": false };
    }
    // Nombre y Apellido
    if (isName(el)) {
      var Genero;
      if (GenderByName(el) == 1) {
        Genero = "Hombre";
      } else if (GenderByName(el) == 2) {
        Genero = "Mujer";
      } else {
        Genero = 'Indeterminado';
      }
      NameHand = { "Tipo": "Nombre", "Gender": Genero };
      Gente.push(x[i]);
    } else if (isLastName(el)) {
      NameHand = { "Tipo": "Apellido" };
      Gente.push(x[i]);
    } else {
      NameHand = {};
    }

    r.push({
      "Original": x[i],
      "Around": {
        "EspacioBlanco": EspacioBlanco,
        "PrevWord": PrevWord,
        "NextWord": NextWord
      },
      "StpWord": StpWord,
      "Token": Token,
      "TipoDato": {
        "Numerico": isNumber,
        "Pais": Pais,
        "Mes": Mes,
        "DiaSemana": DiaSem,
        "PuntoCardinal": PntCard,
        "Color": color,
        "WebPage": WebPage
      },
      "NameHandler": NameHand,
      "Sentiment_Value": Sent_Val_Item
    });

  }

  // 4. AGREGAR MAS PALABRAS AL VECTOR DE FRECUENCIA 
  // 5. AGREGAR VECTORES DE SENTIMIENTOS (POSI Y NEGA)
  // 6. AGREGAR EN TOKEN: 
  //        6.1. Si es Numero (-Además: Si es Dinero, Porcentaje o Solo Número)
  // 7. HACER VECTORES EN GENERAL DE LOS TOKENS: PAISES, NUMEROS, FECHAS, ....

  // Estadisticas 
  let NumSentences = (txt.match(/\w[.?!](\s|$)/gm) || []).length;
  let NumParrafos = (txt.match(/\w[.?!]($)/gm) || []).length;
  let NumWords = x.length;
  let NumCaracteres = txt.length;
  general["stats"] = { "Frases": NumSentences, "Parrafos": NumParrafos, "Palabras": NumWords, "Caracteres": NumCaracteres };

  // Frecuencia de palabras
  CntWords = CountWords(txt);
  general["contador"] = CntWords;

  // Sentimiento
  Sentiment_result = SentimentResult(Sentiment_value);
  general["sentimiento"] = { "Sentiment_result": Sentiment_result, "Sentiment_value": Sentiment_value };

  // Lista de personas 
  if (Gente.length > 0) { general["nombres"] = Gente }

  // Lista de Nombres Propios
  let NombrePropio = GetNomPropio(txt);
  if (NombrePropio.length > 0) { general["nombresPropios"] = NombrePropio }

  // Lista de Correos Electrónicos
  let CorreosElectr = GetEmailsInText(txt);
  if (CorreosElectr.length > 0) { general["correos"] = CorreosElectr }

  // Tipo de Datos 
  let DataTypes = GetDataType(txt);
  if (!isEmpty(DataTypes)) { general["tiposDato"] = DataTypes }

  // Fechas
  let Fechas = [];

  let FechasCompuestas = GetDatesInText(txt);
  if (FechasCompuestas.length > 0) { Fechas["Compuestas"] = FechasCompuestas }
  let Años = GetYearsInText(txt);
  if (Años.length > 0) { Fechas["Años"] = Años }
  let Meses = GetMonthsInText(txt);
  if (Meses.length > 0) { Fechas["Meses"] = Meses }
  let DiaSemana = GetDayWeekInText(txt);
  if (DiaSemana.length > 0) { Fechas["DiaSemana"] = DiaSemana }
  let FechaIndirecta = GetIndirectDates(txt);
  if (FechaIndirecta.length > 0) { Fechas["FechaIndirecta"] = FechaIndirecta }

  if (!isEmpty(Fechas)) { general["fechas"] = Fechas }

  // Color Promedio del texto
  if (R_cnt > 0 && G_cnt > 0 && B_cnt > 0) {
    R_avg = Math.round(R_tot / R_cnt);
    G_avg = Math.round(G_tot / G_cnt);
    B_avg = Math.round(B_tot / B_cnt);
    HexAvg = RGBtoHEX("rgb(" + R_avg + "," + G_avg + "," + B_avg + ")");
  } else {
    R_avg = '';
    G_avg = '';
    B_avg = '';
    HexAvg = '';
  }
  if (ColoresArr.length > 0) {
    general["Colores"] = { "data": ColoresArr, "ColorPromedio": { "R": R_avg, "G": G_avg, "B": B_avg, "HEX": HexAvg } };
  }

  output = { "general": general, "data": r };
  return output;

}

// :::::::::::: CONTAR PALABRAS ::::::::::::
// Regresa la frecuencia de palabras y da la palabra más repetida.
// Ej: CountWords("Yo estoy estoy muy feliz pero muy feliz");
let CountWords = function (txt) {
  let freq = [];
  let general = [];
  let output = [];
  cltxt = CleanPuntuation(txt).toLowerCase();
  cltxt = nl_CleanStopWords(cltxt);
  let x = cltxt.split(" ");
  let maxWord = '';
  let maxCount = -1;

  x = x.filter(function (value, index, self) { return self.indexOf(value) === index; });
  for (var i = 0; i < x.length; i++) {
    var r = new RegExp("(^|\\W)" + x[i] + "($|\\W)", "g");
    var len = (cltxt.match(r) || []).length;
    freq.push({ "palabra": x[i], "cuenta": len });
    maxCount = (len >= maxCount) ? len : maxCount;
    maxWord = (len >= maxCount) ? x[i] : maxWord;
  }
  general.push({ "maxWord": maxWord, "maxCount": maxCount });

  output = { "general": general, "frecuencias": freq };
  return output;
}

// ::::::: LIMPIAR STOPWORDS ::::::::::
let nl_CleanStopWords = function (txt) {
  let x = CleanPuntuation(txt).split(" ");
  let new_txt = "";
  x.forEach((r) => { if (StopWords.indexOf(r) == -1) { new_txt += r + ' '; } });
  new_txt = new_txt.trim();
  return new_txt;
}

// :::::::::::::: RGB a HEXAGESIMAL :::::::::::::
// EJ: RGBtoHEX("rgb(255,255,255)") -> "#ffffff"
let RGBtoHEX = (function (colorval) {
  var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  delete (parts[0]);
  for (var i = 1; i <= 3; ++i) {
    parts[i] = parseInt(parts[i]).toString(16);
    if (parts[i].length == 1) parts[i] = '0' + parts[i];
  }
  color = '#' + parts.join('');
  return color;
});


// Filtro de solo valores unicos
let onlyUnique2 = (function (value, index, self) { return self.indexOf(value) === index; });


// :::::::::: OBTENER NOMBRES PROPIOS DE UN TEXTO :::::::
let GetNomPropio = function (txt) {
  return (txt.match(/([A-ZÁÉÍÓÚÑ]([a-záéíóúüñ]+|\.))(?:\sde)?(?:\sla)?(?:\s[A-ZÁÉÍÓÚÑ]([a-záéíóúüñ]+|\.)){1,10}/gm) || []).filter(onlyUnique2);
}

// :::::::::: OBTENER FECHAS DE UN TEXTO :::::::::::::
let GetDatesInText = function (txt) {
  return (txt.toLowerCase().match(/\b([0-3]\d{1}|[1-9])(?:\sdel|\sde)?((\s|\/|-)+((enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dec|\d{2}|\d{1})))(?:\sdel|\sde)?(?:(\s|\/|-)+(([1-2]\d{3})|([09]\d{1})))?\b/gm) || []).filter(onlyUnique2);
  // return (txt.toLowerCase().match(/\b(?:[0-3]\d{1}|[1-9])?(?:\sdel|\sde)?((?:\s|\/|-)?(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dec|\d{2}|\d{1}))(?:\sdel|\sde)?(?:(\s|\/|-)+(([1-2]\d{3})|([09]\d{1})))?\b/gm) || []).filter(onlyUnique2);
}

let GetYearsInText = function (txt) {
  return (txt.match(/\b([12]\d{3})\b/gm) || []).filter(onlyUnique2);
}

let GetMonthsInText = function (txt) {
  //return (txt.toLowerCase().match(/\b(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)\b/gm) || []).filter(onlyUnique2);
  return (txt.toLowerCase().match(/\b(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)(?:(\s|\sdel|\sde)+(([1-2]\d{3})|([09]\d{1})))?\b/gm) || []).filter(onlyUnique2);
}

let GetDayWeekInText = function (txt) {
  return (txt.toLowerCase().match(/\b(lunes|martes|miércoles|jueves|viernes|sábado|domingo)\b/gm) || []).filter(onlyUnique2);
}

let GetIndirectDates = function (txt) {
  return (txt.match(/\b(ayer|antes de ayer|antier|antesdeayer|anteayer|hoy|mañana|pasado mañana|esta semana|corrido de la semana|semana pasada|semana anterior|semana que pasó|última semana|semana corrida|semana|este mes|corrido del mes|mes pasado|mes que pasó|mes anterior|último mes|mes corrido|mes|este año|año pasado|año anterior|año que pasó|último año|año corrido|corrido del año|año)/gm) || []).filter(onlyUnique2);
}

// ::::::: OBTENER ID  ::::::::
let GetIdsInText = function (txt) {
  return (txt.replace(/\s/gm, "").match(/\d{5,12}/gm) || []);
}

// ::::::: OBTENER TELEFONO ::::::::
let GetTelInText = function (txt) {
  return txt.replace(/\s/gm, "").match(/\d{7}/gm) || [];
}

// ::::::: OBTENER CELULAR ::::::::
let GetCelInText = function (txt) {
  return txt.replace(/\s/gm, "").match(/[3]\d{9}/gm) || [];
}

// ::::::: OBTENER GENERO ::::::::::
let GetGenderInText = function (txt) {
  return (txt.toLowerCase().match(/\b(masculino|femenino|hombre|mujer)\b/gm) || []).filter(onlyUnique2);
}

// ::::::: OBTENER EMAIL ::::::::::
let GetEmailsInText = function (txt) {
  return (txt.toLowerCase().match(/(\w+)(@)(\w+)(\.)(\w+)(?:\.\w+){0,2}/gm) || []).filter(onlyUnique2);
}


// :::::::: OBTENER LOS TIPO DE DATOS DEL TEXTO ::::::::::
let GetDataType = function (txt) {
  let r = {};
  let val;
  let txt_proc = nl_CleanStopWords(CleanPuntuation(txt)).toLowerCase();
  let dt = (txt_proc.match(/\b(nombre|identificación|id|celular|teléfono|tel|cel|cédula ciudadania|cédula extranjería|cédula|nit|pasaporte|fecha nacimiento|nacimiento|cumpleaños|género|sexo|correo electrónico|correo|email|e-mail)\b/gm) || []);
  let HasName = false, HasGender = false, HasEmail = false;
  for (let i = 0; i < dt.length; i++) {

    if ((dt[i].match(/nombre/gm) || []).length > 0) {
      val = GetNomPropio(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["nombre"] = val;
      HasName = true;
    } else if ((dt[i].match(/identificación|id|cédula ciudadania|cédula extranjería|cédula|nit|pasaporte/gm) || []).length > 0) {
      val = GetIdsInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["id"] = val;
      r["tipoId"] = dt[i];
    } else if ((dt[i].match(/celular|cel/gm) || []).length > 0) {
      val = GetCelInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["celular"] = val;
    } else if ((dt[i].match(/teléfono|tel/gm) || []).length > 0) {
      val = GetTelInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["telefono"] = val;
    } else if ((dt[i].match(/cumpleaños/gm) || []).length > 0) {
      val = GetDatesInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["cumpleaños"] = val;
    } else if ((dt[i].match(/fecha nacimiento|nacimiento/gm) || []).length > 0) {
      val = GetDatesInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["nacimiento"] = val;
    } else if ((dt[i].match(/correo electrónico|correo|email|e-mail/gm) || []).length > 0) {
      val = GetEmailsInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["email"] = val;
      HasEmail = true;
    } else if ((dt[i].match(/género|sexo/gm) || []).length > 0) {
      val = GetGenderInText(txt_proc.substring(txt_proc.indexOf(dt[i]), txt_proc.length))[0] || '';
      r["genero"] = val;
      HasGender = true;
    }

  }
  if (HasName == false) {
    val = GetNomPropio(txt)[0] || '';
    if (val != '') { r["nombre"] = val }
  }
  if (HasGender == false) {
    val = GetGenderInText(txt)[0] || '';
    if (val != '') { r["genero"] = val }
  }
  if (HasEmail == false) {
    val = GetEmailsInText(txt.toLowerCase())[0] || '';
    if (val != '') { r["email"] = val }
  }
  return r;
}

//::::: VALIDAR SI TEXTO ES UNA PAGINA WEB ::::::::::
let isWebPage = function (txt) {
  txt = txt.toLowerCase();
  let r = (txt.match(/(\.(?=com|co|net|org|vn|edu))|(www(?=\.))|(http(?=:)|https(?=:))/g) || []).length;
  return (r > 0) ? true : false;
}

// :::::: LIMPIAR PUNTUACIONES DEL TEXTO :::::::
let CleanPuntuation = function (txt) {
  return txt.replace(/[:;?¿\-=()&#!¡]/g, "").replace(/(?=\D)\.(?=\s|$)/g, "").replace(/(?=\D)\,(?=\D|\s)/g, "").trim();
}

// ::::::: CONVERTIR DE SINGULAR -> PLURAL ::::::::
let Plural = function (txt) {
  let w = (txt.toLowerCase());
  let r = '';
  if (w.length <= 1) {
    r = w;
  } else {
    let lastWord = w.substring(w.length - 1, w.length);
    let last2Word = w.substring(w.length - 2, w.length);
    if (lastWord == 'a' || lastWord == 'e' || lastWord == 'o') {
      r = txt + 's';
    } else if (Consonantes.indexOf(lastWord) > -1) {
      r = txt + 'es';
    } else if (last2Word == 'ón') {
      r = quitaacentos2(txt) + 'es';
    } else if (lastWord == 's' || lastWord == 'x') {
      r = txt;
    } else if (lastWord == 'z') {
      r = txt.substring(0, txt.length - 1) + 'ces';
    }
  }
  return r;
}

// ::::::: CONVERTIR DE PLURAL -> SINGULAR ::::::::
let Singular = function (txt) {
  let w = (txt.toLowerCase());
  let r = '';
  let Exptn = ["es"];
  if (w.length <= 1 || Exptn.indexOf(w) > -1) {
    r = w;
  } else {
    let lastWord = w.substring(w.length - 1, w.length);
    let last2Word = w.substring(w.length - 2, w.length);
    let lWord2 = w.substring(w.length - 2, w.length - 1);
    let last3Word = w.substring(w.length - 3, w.length);
    let lWord3 = w.substring(w.length - 3, w.length - 2);
    let last4Word = w.substring(w.length - 4, w.length);
    let lWord4 = w.substring(w.length - 4, w.length - 3);
    if (last3Word == 'ces') {
      r = txt.replace(/ces$/g, "z");
    } else if (last4Word == "ones") {
      r = txt.replace(/ones$/g, "ón");
    } else if (last2Word == "es" && (lWord3 == "n" || lWord3 == "r" || lWord3 == "s")) {
      r = txt.replace(/es$/g, "");
    } else if (lastWord == "s" && (lWord2 == 'a' || lWord2 == 'e' || lWord2 == 'o')) {
      r = txt.replace(/s$/g, "");
    } else {
      r = txt;
    }
  }
  return r;
}


//::::::::: BUSCAR ADEJTIVO :::::::.
let BuscarAdjetivo = function (txt) {
  for (let i = 0; i < ADJETIVOS.length; i++) {
    if ((ADJETIVOS[i].val.toLowerCase()) == txt.toLowerCase()) {
      return ADJETIVOS[i];
    }
  }
}
// Buscar adjetivos sin tener en cuenta la ortografía
let BuscarAdjetivoSinOrtog = function (txt) {
  txt = quitaacentos2(txt);
  for (let i = 0; i < ADJETIVOS.length; i++) {
    if (quitaacentos2(ADJETIVOS[i].val.toLowerCase()) == txt.toLowerCase()) {
      return ADJETIVOS[i];
    }
  }
}

//::::::::::: QUITAR ACENTOS DE UNA PALABRA :::::::::
let quitaacentos2 = (function (s) {
  var r = s.toLowerCase();
  r = r.replace(new RegExp(/[àáâãäå]/g), "a");
  r = r.replace(new RegExp(/[èéêë]/g), "e");
  r = r.replace(new RegExp(/[ìíîï]/g), "i");
  r = r.replace(new RegExp(/[òóôõö]/g), "o");
  r = r.replace(new RegExp(/[ùúûü]/g), "u");
  return r;
});


// :::::::: CHECK IF A JSON OBJECT IS EMPTY :::::::..
let isEmpty = function (obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

// :::::::::::::::::::: FUNCIONES PARA CARGAR JSON ::::::::::::::::
const proxyurl = "https://cors-anywhere.herokuapp.com/";
let loadJSONS_All = async function () {
  try {
    await fetch('https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { NAMES_COLOMBIA = r2.names });
    await fetch('https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { APELLIDOS_COLOMBIA = r2.lastnames });
    await fetch('https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { NAMES_GENDER = r2.genders });
    await fetch('https://www.2luca.co/json/adjetivos.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { ADJETIVOS = r2.adjetivos });
    await fetch('https://www.2luca.co/json/sentiment_words.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { SENTIMENT_WORDS = r2.sentiment_words });
  } catch (e) {
    await fetch(proxyurl + 'https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { NAMES_COLOMBIA = r2.names });
    await fetch(proxyurl + 'https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { APELLIDOS_COLOMBIA = r2.lastnames });
    await fetch(proxyurl + 'https://www.2luca.co/json/names.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { NAMES_GENDER = r2.genders });
    await fetch(proxyurl + 'https://www.2luca.co/json/adjetivos.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { ADJETIVOS = r2.adjetivos });
    await fetch(proxyurl + 'https://www.2luca.co/json/sentiment_words.json?' + generateRandomNumber(3)).then((r) => r.json()).then((r2) => { SENTIMENT_WORDS = r2.sentiment_words });
  }
}

// :::::::::: ANALISIS DE SENTIMIENTO :::::::::::::
// Buscar Sentimiento de una palabra
let SearchSentimentWord = function (word) {
  for (let i = 0; i < SENTIMENT_WORDS.length; i++) {
    if (SENTIMENT_WORDS[i].key.toLowerCase() == word.toLowerCase()) {
      return SENTIMENT_WORDS[i];
    }
  }
}
// Determinar el resultado (Positivo, negaivo o neturo) a partir de un puntaje de sentiemiento
let SentimentResult = function (Sentiment_value) {
  let thInf = -0.2;
  let thSup = 0.2;
  if (Sentiment_value >= thInf && Sentiment_value <= thSup) {
    Sentiment_result = "Neutro";
  } else if (Sentiment_value < thInf) {
    Sentiment_result = "Negativo";
  } else if (Sentiment_value > thSup) {
    Sentiment_result = "Positivo";
  } else {
    Sentiment_result = '';
  }
  return Sentiment_result;
}
//::::::::::::::::::::::::::::::::::::::::::::::::::

//:::::::::::::: MANEJAR NOMBRES Y GENERO :::::::::::::::::
// Ver si la palabra es un nombre
let isName = (function (name) {
  name = quitaacentos2(name).toUpperCase();
  let exist = (NAMES_COLOMBIA != undefined) ? (NAMES_COLOMBIA.indexOf(name) > -1) : false;
  return (exist);
});

// Ver si la palabra es un apellido
let isLastName = (function (lastname) {
  lastname = quitaacentos2(lastname).toUpperCase();
  let exist = (APELLIDOS_COLOMBIA != undefined) ? (APELLIDOS_COLOMBIA.indexOf(lastname) > -1) : false;
  return (exist);
});

// Genero de una persona dado un nombre
let GenderByName = (function (name) {
  let jsonStringList = NAMES_GENDER;
  name = quitaacentos2(name).toUpperCase();
  let result = 9;
  for (var i = 0; i < jsonStringList.length; i++) {
    if (jsonStringList[i]["name"] == name) {
      result = jsonStringList[i]["gender"];
      break;
    }
  }
  return result;
});

/* HANDLER OF FULLNAME OF A PERSON 
INPUT: JUAN CAMILO DIAZ HERRERA
x[1]["shortFullName"] = JUAN DIAZ -> First name and First LastName
x[1]["firstName"]
x[1]["firstLastName"]
x[1]["GenderAprox"]

x[0] -> {0: "name", 1: "name", 2: "lastname", 3: "lastname"}
x[0][0] -> name
x[0][1] -> name
x[0][2] -> lastname
x[0][3] -> lastname
*/
let nameHandler = (function (FullName) {
  let result = "";
  let arrName = FullName.split(" ");
  let firstName = "";
  let firstLastName = "";
  let i_isName, i_isLastName;
  let gender = 9;
  let genderStr = '';
  let NameFound = false;
  let LastNameFound = false;
  let shortFullName = "";
  let jsonString = '[{';
  for (var i = 0; i < arrName.length; i++) {
    jsonString += '"' + i + '" : ';
    i_isName = isName(arrName[i]);
    i_isLastName = isLastName(arrName[i]);
    if (i_isName == true) {
      if (gender == 9) {
        gender = GenderByName(arrName[i]);
      }
      jsonString += '"name"';
      if (NameFound == false) {
        shortFullName += arrName[i];
        firstName = arrName[i];
        NameFound = true;
      }
    }
    if (i_isLastName == true && i_isName == false) {
      jsonString += '"lastname"';
      if (LastNameFound == false) {
        if (i > 0 && arrName[i - 1].toUpperCase() == "DE") {
          firstLastName = arrName[i - 1] + ' ' + arrName[i];
          shortFullName += ' ' + arrName[i - 1] + ' ' + arrName[i];
        } else {
          firstLastName = arrName[i];
          shortFullName += ' ' + arrName[i];
        }
        LastNameFound = true;
      }
    }
    if (i_isName == false && i_isLastName == false && arrName[i].toUpperCase() != "DE") {
      jsonString += '"NA"';
    }
    if (arrName[i].toUpperCase() == "DE") {
      jsonString += '"DE"';
    }
    if (i < arrName.length - 1) {
      jsonString += ', ';
    }
  }
  if (shortFullName == '') {
    shortFullName = arrName[0];
  }
  if (gender == 1) {
    genderStr = 'Hombre';
  } else if (gender == 2) {
    genderStr = 'Mujer';
  } else {
    genderStr = '';
  }
  jsonString += '}, {"shortFullName" : "' + shortFullName.trim() + '", "firstName" : "' + firstName.trim() + '", "firstLastName" : "' + firstLastName.trim() + '", "GenderAprox" : "' + genderStr + '"}]';
  jsonResult = JSON.parse(jsonString);
  return (jsonResult);

});
