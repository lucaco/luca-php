$(document).ready(function () {

    // Create payment object
    let payObj = new paymentsProccessObj(true);

    $('#MainRoller_background').hide();
    $("#mp_analiycs").click(function () { OpenMpAnalytics(); });
    $("#mp_chatbot").click(function () { OpenMpChatbot(); });
    $("#mp_tracker").click(function () { OpenMpTracker(); });


    // ****************** INICIO FUNCTIONES ******************
    let checkMpStates = function () {
        $.ajax({
            url: "check_mp_states.php",
            dataType: "json",
            data: ({ x: 1 }),
            type: "POST",
            success: function (r) {
                if (r[0][0] == "S") {
                    (r[1]["pkg_1"] == "1") ? toggle_pkg(1, "active") : toggle_pkg(1, "inactive");
                    (r[1]["pkg_2"] == "1") ? toggle_pkg(2, "active") : toggle_pkg(2, "inactive");
                    (r[1]["pkg_3"] == "1") ? toggle_pkg(3, "active") : toggle_pkg(3, "inactive");
                    // (r[1]["pkg_4"]=="1")?toggle_pkg(4,"active"):toggle_pkg(4,"inactive");
                    // (r[1]["pkg_5"]=="1")?toggle_pkg(5,"active"):toggle_pkg(5,"inactive");
                    // (r[1]["pkg_6"]=="1")?toggle_pkg(6,"active"):toggle_pkg(6,"inactive");
                } else if (r[0][0] == "E") {
                    alertify.alert("", "Se ha producido un error.");
                }
            }
        });
    }

    let toggle_frtrl_mp = function (pkg_id) {
        $.ajax({
            url: "activate_ftrl_mp.php",
            dataType: "json",
            data: ({ pkg_id }),
            type: "POST",
            success: function (r) {
                if (r[0][0] == 'S') {
                    alertify.message('Tu período de prueba de 7 días ha empezado!');
                    toggle_pkg(pkg_id, 'inactive')
                } else if (r[0][0] == 'E') {
                    alertify.alert('', 'Se ha producido un error.');
                    throw "Error #345346-A";
                }
            }
        });
    }

    // ******************************************************

    checkMpStates();
    LoadEpayDataCust();

    $("#btn_str_tst_1").click(() => toggle_frtrl_mp(1));
    $("#btn_str_tst_2").click(() => toggle_frtrl_mp(2));
    $("#btn_str_tst_3").click(() => toggle_frtrl_mp(3));

    $('#btn_p_1').click(function () {
        payObj._assign_product(1);
        payObj._open();
    });

    $('#btn_p_2').click(function () {
        payObj._assign_product(2);
        payObj._open();
    });

    $('#btn_p_3').click(function () {
        payObj._assign_product(3);
        payObj._open();
    });

});

