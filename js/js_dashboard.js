let tx_data2;


// Define some important dates
var now = new Date();
var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
var twoWeeksAgo = new Date(lastWeek.getFullYear(), lastWeek.getMonth(), lastWeek.getDate() - 7);
var threeWeeksAgo = new Date(twoWeeksAgo.getFullYear(), twoWeeksAgo.getMonth(), twoWeeksAgo.getDate() - 7);
var fourWeeksAgo = new Date(threeWeeksAgo.getFullYear(), threeWeeksAgo.getMonth(), threeWeeksAgo.getDate() - 7);
var lastMonth = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
var twoMonthsAgo = new Date(today.getFullYear(), today.getMonth() - 2, today.getDate());
var sixMonthsAgo = new Date(today.getFullYear(), today.getMonth() - 6, today.getDate());
var lastYear = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
var twoYearsAgo = new Date(today.getFullYear() - 2, today.getMonth(), today.getDate());
var allData = new Date(today.getFullYear() - 50, today.getMonth(), today.getDate());

var currYear = new Date(today.getFullYear(), 0, 1);
var currMonth = new Date(today.getFullYear(), today.getMonth(), 1);
//var currQuarter = new Date(today.getFullYear(), today.getMonth(), 1);
var prevYear = new Date(today.getFullYear() - 1, 0, 1);

// ::::::::: FUNCIONES ::::::::::

let formatoFecha = function (date, format, separator) {
    format = (format === undefined) ? "WWWW DD MMMM YYYY" : format;
    separator = (separator === undefined) ? ' ' : separator;

    let monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];
    let weekNames = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];

    let day = date.getDate();
    let monthIndex = date.getMonth();
    let year = date.getFullYear();
    let week = date.getDay();

    let monthNum = parseInt(monthIndex) + 1;

    if (format == "WWWW DD MMMM YYYY") {
        // Miércoles, 03 Enero 1990
        return weekNames[week] + ', ' + day + separator + monthNames[monthIndex] + separator + year;
    } else if (format == "DD MMMM YYYY") {
        // 03 Enero 1990
        return day + separator + monthNames[monthIndex] + separator + year;
    } else if (format == "DD MMM YYYY") {
        // 03 Ene 1990
        return day + separator + monthNames[monthIndex].substr(0, 3) + separator + year;
    } else if (format == "DD MM YYYY") {
        // 03 01 1990
        return day + separator + monthNum + separator + year;
    } else if (format == "DD MMM YY") {
        // 03 Ene 90
        return day + separator + monthNames[monthIndex].substr(0, 3) + separator + year.toString().substr(2, 2);
    } else if (format == "WWW DD MMM YY") {
        // Mié, 03 Ene 90
        return weekNames[week].substr(0, 3) + ',' + day + separator + monthNames[monthIndex].substr(0, 3) + separator + year.toString().substr(2, 2);
    } else if (format == "WWW DD MMM") {
        // Mié, 03 Ene
        return weekNames[week].substr(0, 3) + ',' + day + separator + monthNames[monthIndex].substr(0, 3);
    } else if (format == "DD MM YY") {
        // 03 01 90
        return day + separator + monthNum + separator + year.toString().substr(2, 2);
    } else if (format == "MMMM YYYY") {
        // Enero 1990
        return monthNames[monthIndex] + separator + year;
    } else if (format == "MMM YYYY") {
        // Ene 1990
        return monthNames[monthIndex].substr(0, 3) + separator + year;
    } else if (format == "WWW MMM YY") {
        // Mié, Ene 90
        return weekNames[week].substr(0, 3) + ',' + monthNames[monthIndex].substr(0, 3) + separator + year.toString().substr(2, 2);
    } else if (format == "MMM YY") {
        // Ene 90
        return monthNames[monthIndex].substr(0, 3) + separator + year.toString().substr(2, 2);
    } else if (format == "MM YYYY") {
        // Ene 90
        return monthNames[monthIndex].substr(0, 3) + separator + year;
    } else if (format == "MM YY") {
        // 01 90
        return monthNum + separator + year.toString().substr(2, 2);
    }

}

// groups the data by day
var groupByDay = function (xs) {
    return xs.reduce(function (rv, x) {
        day = x['trx_date'].substr(0, 10);
        (rv[day] = rv[day] || []).push(x);
        return rv;
    }, {});
};

// groups the data by bills
var groupByBills = function (xs) {
    return xs.reduce(function (rv, x) {
        bill = x['id_bill'];
        (rv[bill] = rv[bill] || []).push(x);
        return rv;
    }, {});
};

// groups the data by profile name
var groupByProfile = function (xs) {
    return xs.reduce(function (rv, x) {
        bill = x['prof_name'];
        (rv[bill] = rv[bill] || []).push(x);
        return rv;
    }, {});
};

// groups the data by month
var groupByMonth = function (xs) {
    return xs.reduce(function (rv, x) {
        month = x['trx_date'].substr(0, 7);
        (rv[month] = rv[month] || []).push(x);
        return rv;
    }, {});
};

// groups the data by hour
var groupByHour = function (xs) {
    return xs.reduce(function (rv, x) {
        hour = x['trx_date'].substr(0, 13);
        (rv[hour] = rv[hour] || []).push(x);
        return rv;
    }, {});
};

// groups the data by customer
var groupByCustomer = function (xs) {
    return xs.reduce(function (rv, x) {
        customer = x['cust_key'];
        (rv[customer] = rv[customer] || []).push(x);
        return rv;
    }, {});
};

// group by payment type
let groupByPaymentType = function (xs) {
    return xs.reduce(function (rv, x) {
        payment = x['payment_type'];
        (rv[payment] = rv[payment] || []).push(x);
        return rv;
    }, {});
};

// Total valor de transacciones por datos
var SumTotalValue = function (data, rnd) {
    rnd = (rnd === undefined) ? 2 : rnd;
    let suma = 0;
    let bills = groupByBills(data)
    for (bill in bills) { suma += parseFloat(bills[bill][0]["trx_value"]) };
    return numRound(suma, rnd);
}

// Total valor de transacciones por datos
var countItems = function (data, rnd) {
    rnd = (rnd === undefined) ? 2 : rnd;
    let suma = 0;
    let bills = groupByBills(data)
    for (bill in bills) { suma += parseFloat(bills[bill][0]["num_items"]) };
    return numRound(suma, rnd);
}

var customerGenders = function (xs) {
    var by_customers = xs.reduce(function (rv, x) {
        if (x['cust_key'] != "") {
            rv[x['cust_key']] = x['cust_gender'];
        }
        return rv;
    }, {});

    return Object.values(by_customers);
};

var malePercentage = function (data) {
    var genders = customerGenders(data);
    var counts = {};

    if (genders === undefined || genders.length === 0) {
        return -1;
    } else {
        for (i in genders) {
            counts[genders[i]] = (counts[genders[i]] || 0) + 1;
        }
        return counts['1'] != 0 ? counts['1'] / (counts['1'] + counts['2']) * 100 : 0;
    }

};

// filters the data, leaving data with a date greater than or equal to the given one
var filterByDate = function (data, date) {
    return data.filter(function (x) {
        return new Date(Date.parse(x["trx_date"].replace(" ", "T"))) >= date;
    });
};

// Sums all of the values from the different transactions
var getPeriodTotal = function (data) {
    var total = 0;
    for (k in data) {
        var temp = data[k];
        for (var i = 0; i < temp.length; i++) {
            total += parseFloat(temp[i]['item_value']);
        }
    }
    return total;
};

// Sums of number of items from transactions
var getPeriodCountItems = function (data) {
    var total = 0;
    for (k in data) {
        var temp = data[k];
        for (var i = 0; i < temp.length; i++) {
            total += parseFloat(temp[i]['num_items']);
        }
    }
    return total;
};


// Contar número de ítems, transacciones y valor total por tipo de pago
let countByPaymentType = function (data, updatePage) {
    updatePage = (updatePage === undefined) ? false : updatePage;
    let r = {};
    const paymentNames = ['Ninguno', 'Efectivo', 'Tarjeta de Crédito', 'Tarjeta Débito', 'Otro'];
    let paymentType = groupByPaymentType(data);
    let totVentas = 0;
    let totIngresos = 0;

    for (p in paymentType) {
        let bills = groupByBills(paymentType[p]);
        let numVent = Object.keys(bills).length;
        totVentas += parseFloat(numVent);
        let totSalesVal = 0;
        for (bill in bills) { totSalesVal += parseFloat(bills[bill][0]["trx_value"]) };
        totIngresos += parseFloat(totSalesVal);
        r[p] = { "name": paymentNames[p], "numItems": paymentType[p].length, "numVentas": numVent, "totSalesVal": totSalesVal };
    }

    for (p in paymentType) {
        if (totVentas > 0) {
            r[p].porVentas = numRound((r[p].numVentas / totVentas) * 100, 2);
        } else {
            r[p].porVentas = 0;
        }
        if (totIngresos > 0) {
            r[p].porSalesVal = numRound((r[p].totSalesVal / totIngresos) * 100, 2);
        } else {
            r[p].porSalesVal = 0;
        }
    }

    if (updatePage && data.length > 0) {
        for (var i = 1; i <= 4; i++) {
            $("#cptp_" + i).text(0);
            $("#cpval_" + i).text(0);
        }
        let typePT = "porVentas";
        let typePT2 = "numVentas";
        if ($("#pyVentas").hasClass("PyTyActive")) {
            typePT = "porVentas";
            typePT2 = "numVentas";
        } else if ($("#pyMonto").hasClass("PyTyActive")) {
            typePT = "porSalesVal";
            typePT2 = "totSalesVal";
        }
        for (p in r) {
            $("#cptp_" + p).text(numCommas(numRound(r[p][typePT], 0)));
            if ($("#pyVentas").hasClass("PyTyActive")) {
                $("#cpval_" + p).text(numCommas(numRound(r[p][typePT2], 0)));
            } else if ($("#pyMonto").hasClass("PyTyActive")) {
                $("#cpval_" + p).text("$ " + formatBigNumber(r[p][typePT2], 0));
            }
        }
    } else if (updatePage && data.length === 0) {
        for (var i = 1; i <= 4; i++) {
            $("#cptp_" + i).text(0);
            $("#cpval_" + i).text(0);
        }
    }

    return r;
}

// Contar número de ítems, transacciones y valor total por cualquier variable agrupada 
// ingresando la función que agrupa y los datos a agrupar
let statsByGroup = function (data, funtionGroup, getExtraVariable) {
    getExtraVariable = (getExtraVariable === undefined) ? "" : getExtraVariable;
    let r = {};
    let dataByGroup = funtionGroup(data);
    let totVentas = 0;
    let totIngresos = 0;

    for (p in dataByGroup) {
        let bills = groupByBills(dataByGroup[p]);
        let numVent = Object.keys(bills).length;
        totVentas += parseFloat(numVent);
        let totSalesVal = 0;
        let totItems = 0;
        for (bill in bills) {
            totSalesVal += parseFloat(bills[bill][0]["trx_value"]);
            totItems += parseFloat(bills[bill][0]["num_items"]);
        };
        totIngresos += parseFloat(totSalesVal);
        let varExtra = (getExtraVariable != "") ? dataByGroup[p][0][getExtraVariable] : "";
        if (getExtraVariable != "") {
            r[p] = { "extra_var1": varExtra, "name": p, "numItems": totItems, "numVentas": numVent, "totSalesVal": totSalesVal };
        } else {
            r[p] = { "name": p, "numItems": totItems, "numVentas": numVent, "totSalesVal": totSalesVal };
        }
    }

    for (p in dataByGroup) {
        if (totVentas > 0) {
            r[p].porVentas = numRound((r[p].numVentas / totVentas) * 100, 2);
        } else {
            r[p].porVentas = 0;
        }
        if (totIngresos > 0) {
            r[p].porSalesVal = numRound((r[p].totSalesVal / totIngresos) * 100, 2);
        } else {
            r[p].porSalesVal = 0;
        }
    }

    return r;
}

var getPeriodCount = function (data) {
    var total = 0;
    for (k in data) {
        total += data[k].length;
    }
    return total;
};

var valueFormat = function (value) {
    f = new Intl.NumberFormat('co-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    });

    //return f.format(value);
    return new Intl.NumberFormat("co-CO", { minimumFractionDigits: 0, maximumFractionDigits: 0 }).format(value);
};

var trxMean = function (data) {
    var total = 0;
    for (let i = 0; i < data.length; i++) {
        total += parseFloat(data[i]['trx_value']);
    }
    return data.length > 0 ? total / data.length : 0;
};

var prodMean = function (data) {
    var total = 0;
    for (let i = 0; i < data.length; i++) {
        total += parseInt(data[i]['num_items']);
    }
    return data.length > 0 ? total / data.length : 0;
};

var dominantGender = function (male_proportion) {
    if (male_proportion > 60) {
        return "Hombre";
    } else if (male_proportion < 40) {
        return "Mujer";
    } else {
        return "Ambos Géneros";
    }
};

var sortDataByDate = function (data) {
    return data.sort(function (a, b) { date1 = new Date(Date.parse(a["trx_date"].replace(" ", "T"))); date2 = new Date(Date.parse(b["trx_date"].replace(" ", "T"))); return date1 - date2 });
}

var getCustomerBdates = function (data) {
    return data.reduce(function (rv, x) {
        var cust = x['cust_key'];
        if (cust != "") {
            rv[cust] = rv[cust] || x['cust_bdate'];
        }
        return rv;
    }, {});
};

var maxCustomerAge = function (data) {
    var bdates = Object.values(getCustomerBdates(data));
    if (bdates === undefined || bdates.length > 0) {
        var sorted = bdates.sort(function (a, b) { return a.localeCompare(b); });
        var year = sorted[0].split("-")[0]
        return today.getFullYear() - year;
    } else {
        return 0;
    }
};

var minCustomerAge = function (data) {
    var bdates = Object.values(getCustomerBdates(data));
    if (bdates === undefined || bdates.length > 0) {
        var sorted = bdates.sort(function (a, b) { return a.localeCompare(b); });
        var year = sorted[sorted.length - 1].split("-")[0];
        return today.getFullYear() - year;
    } else {
        return 0;
    }
};

var getCustomerVisits = function (data) {
    return data.reduce(function (rv, x) {
        cust = x['cust_key'];
        if (cust != "") {
            rv[cust] = (rv[cust] || 0) + 1;
        }
        return rv;
    }, {});
};

var avgVisits = function (data) {
    var visits = getCustomerVisits(data);
    var visits_number = Object.values(visits);
    var total = 0;
    for (var i = 0; i < visits_number.length; i++) {
        total += visits_number[i];
    }
    return visits_number.length > 0 ? total / visits_number.length : 0;
};

var getRecurrentData = function (data) {

    var statsCust = statsByGroup(data, groupByCustomer);
    var summary = { 'returning': 0, 'new': 0, 'no_data': 0, 'returning_perc': 0, 'new_perc': 0 };
    var totCusts = 0;

    jQuery.each(statsCust, (i, v) => {
        var n = v.numVentas;
        if (i != "") {
            n > 1 ? summary['returning']++ : summary['new']++;
            totCusts += 1;
        } else {
            summary['no_data'] = n;
        }
    });

    summary['returning_perc'] = (totCusts > 0) ? 100 * summary['returning'] / totCusts : 0;
    summary['new_perc'] = (totCusts > 0) ? 100 * summary['new'] / totCusts : 0;

    return summary;
};

var getAgeRangeCount = function (data, minAge, maxAge) {
    var ages = getCustomerBdates(data);
    var total = 0;
    for (var i in ages) {
        if (ages[i] != "1900-01-01") {
            var now = new Date();
            var age = now.getFullYear() - ages[i].split("-")[0];
            if (age >= minAge && age <= maxAge) {
                total++;
            }
        }
    }
    return total;
};

var itemsBySales = function (xs) {
    var sales_obj = xs.reduce(function (rv, x) {
        var prod = x['prod_name'];
        rv[prod] = (rv[prod] || 0) + 1;
        return rv;
    }, {});

    var res = [];
    for (var prod in sales_obj) {
        res.push([prod, sales_obj[prod]]);
    }

    return res.sort(function (a, b) {
        return b[1] - a[1];
    });
};

var getTrxWeekDays = function (data) {
    return data.map(function (x) {
        trx_date = new Date(Date.parse(x['trx_date'].replace(" ", "T")));
        return trx_date.getDay();
    });
};

var getTrxDayHours = function (data) {
    return data.map(function (x) {
        trx_date = new Date(Date.parse(x['trx_date'].replace(" ", "T")));
        return trx_date.getHours();
    });
};

var getRangeCount = function (values, min, max) {
    var total = 0;
    for (var i in values) {
        if (values[i] >= min && values[i] <= max) {
            total++;
        }
    }
    return total;
};

var getRangeTotal = function (data, minDate, maxDate) {
    var total = 0
    for (var i = 0; i < data.length; i++) {
        var txDate = new Date(Date.parse(data[i]['trx_date'].replace(" ", "T")));
        if (txDate >= minDate && txDate < maxDate) {
            total += parseFloat(data[i]['item_value']);
        }
    }
    return total;
};

var getDateRangeCount = function (values, minDate, maxDate) {
    var total = 0;
    let dayNext = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate() + 1)
    for (var i in values) {
        var date = new Date(Date.parse(values[i].replace(" ", "T")));
        if (date >= minDate && date < maxDate) {
            total++;
        }
    }
    return total;
};

var calculatePeriodsDiff = function (data, prev_date, current_date) {
    var full_total = getPeriodTotal(groupByHour(filterByDate(data, prev_date)));
    var curr_total = getPeriodTotal(groupByHour(filterByDate(data, current_date)));
    var prev_total = full_total - curr_total;

    if (prev_total == 0 && curr_total == 0) {
        return 0;
    } else if (prev_total == 0) {
        return 100;
    } else if (curr_total == 0) {
        return -100;
    } else {
        return ((curr_total - prev_total) / prev_total) * 100
    }
};

var calculatePtgs = function (values, rnd) {
    rnd = (rnd === undefined) ? 2 : rnd;
    if (values === [] || values === 0) { return 0 }
    var total = 0;
    for (let i = 0; i < values.length; i++) {
        total += values[i];
    }
    return total !== 0 ? values.map(function (x) { return numRound(x / total * 100, rnd) }) : 0;
}

var formatDate = function (date) {
    return date.toISOString().slice(0, 10);
}

var getPeriodTotalFromLists = function (data) {
    return data.map(function (x) {
        var total = 0;
        for (var i = 0; i < x.length; i++) {
            total += parseFloat(x[i]['item_value']);
        }
        return total;
    });
}

var update_other_items_ptg = function (period_sales_ptgs) {
    if (period_sales_ptgs.length > 5) {
        var other_items = period_sales_ptgs.slice(5, period_sales_ptgs.length);
        var ptg_sum = other_items.sumar();

        $("#other-items").text("Otros");
        $("#other-items-value").text(numRound(ptg_sum, 1) + "%")
    } else {
        $("#other-items").text("");
        $("#other-items-value").text("");
    }
};

let updateProfiles = function (profileStats) {
    $("#vendWrapItems").html("");
    let htmlTxt = "";
    let var1, var2;
    for (x in profileStats) {
        if ($("#vendVentas").hasClass("PyTyActive")) {
            var1 = numCommas(numRound(profileStats[x]["porVentas"], 0));
            var2 = numCommas(numRound(profileStats[x]["numVentas"], 0));
        } else if ($("#vendMonto").hasClass("PyTyActive")) {
            var1 = numCommas(numRound(profileStats[x]["porSalesVal"], 0));
            var2 = "$ " + formatBigNumber(profileStats[x]["totSalesVal"], 0);
        }
        htmlTxt += '<div><img title="' + p + '" src="' + profileStats[x]["extra_var1"] + '">';
        htmlTxt += '<p><span id="cptp_1">' + var1 + '</span>%</p>';
        htmlTxt += '<p>(<span id="cpval_1">' + var2 + '</span>)</p></div>';
    }
    $("#vendWrapItems").html(htmlTxt);
}

// ::::::::::::: END OF FUNCTIONS ::::::::::::


$(document).ready(function () {
    // Default Open Lateral Menu
    //OpenMenu();
    // Check Cookies Support
    checkCookiesSupport();
    // Inicializate Spinner and Rollers
    $('#MainRoller_background').hide();
    $('.bar_loading_cont').hide();
    $("#noInfoDashboard").hide()

    // AQUI VA LO QUE ESTA ARRIBA FUNCIONES----

    let bl = new BarLoading(15000, "Cargando Estadísticas");
    bl.start();

    $.ajax({
        url: 'get_dashboard_data.php',
        dataType: 'json',
        data: ({ x: 1 }),
        type: 'POST',
        success: function (tx_data) {

            if (tx_data === undefined || tx_data.length == 0) {
                $("#noInfoDashboard").show();
                $("#controlTabs").hide();
                $("#business").hide();
                $("#customer").hide();
                bl.stop();
                return false;
            }

            bl.stop();
            moment.locale("es");

            // Sort data by Date
            tx_data = sortDataByDate(tx_data);
            tx_data2 = tx_data;

            var monthly_data = filterByDate(tx_data, lastMonth);
            var grouped = groupByHour(monthly_data);
            var biquarter_data = filterByDate(tx_data, sixMonthsAgo);
            var male_ptg = malePercentage(biquarter_data);
            var sorted_items_by_sales = itemsBySales(monthly_data);
            var trx_week_days = getTrxWeekDays(monthly_data);
            var trx_day_hours = getTrxDayHours(monthly_data);
            var trx_dates = monthly_data.map(function (x) { return x['trx_date'] });
            var current_delta = calculatePeriodsDiff(tx_data, twoMonthsAgo, lastMonth);

            var numBills = Object.keys(groupByBills(monthly_data)).length;

            var pymType = countByPaymentType(monthly_data, true);

            var profileStats = statsByGroup(monthly_data, groupByProfile, "prof_avatar");
            updateProfiles(profileStats);

            // Set the initial defaults.
            $("#trx-total").text("$ " + valueFormat(SumTotalValue(monthly_data)));
            $("#trx-amount").text(numBills);
            $("#trx-numitems").text(valueFormat(countItems(monthly_data)));

            var delta_value = numRound(Math.abs(current_delta), 1);
            delta_value = (isNaN(delta_value)) ? "-" : delta_value;
            $("#delta-value").text(delta_value + "%");

            if (current_delta >= 0) {
                $("#delta-direction").text("Más");
                $("#delta-value").css("color", "#3fb378");
            } else {
                $("#delta-direction").text("Menos");
                $("#delta-value").css("color", "#dc3545");
            }

            // Set invariant amounts.
            if (male_ptg > -1) {
                $("#male-ptg").text(numRound(male_ptg, 0));
                $("#female-ptg").text(numRound(100 - male_ptg, 0));
            } else {
                $("#male-ptg").text(0);
                $("#female-ptg").text(0);
            }
            $("#avg-txn-pc").text("$ " + valueFormat(trxMean(biquarter_data)));
            $("#prod-per-txn").text(numRound(prodMean(biquarter_data), 0));
            $("#dom-gender").text(dominantGender(male_ptg));
            $("#min-cust-age").text(minCustomerAge(biquarter_data));
            $("#max-cust-age").text(maxCustomerAge(biquarter_data));
            $("#visits-per-month").text(numRound(avgVisits(biquarter_data), 0));

            let recurrentData = getRecurrentData(biquarter_data);
            $("#recurrent-ptg").text(numRound(recurrentData['returning_perc'], 1));
            $("#new-ptg").text(numRound(recurrentData['new_perc'], 1));
            $("#no-info").text(recurrentData['no_data']);

            for (var i = 10; i <= recurrentData['returning_perc']; i += 10) {
                $("#ptg-" + i).css('color', '#00bb7d');
            };

            var items_sales_ptgs = calculatePtgs(sorted_items_by_sales.map(function (x) { return x[1] }));

            for (var i = 0; i < 5; i++) {
                if (sorted_items_by_sales[i] !== undefined) {
                    var elem = sorted_items_by_sales[i];
                    $("#item-" + (i + 1)).text(elem[0]);
                    $("#item-" + (i + 1) + "-value").text(numRound(items_sales_ptgs[i], 1) + "%");
                }
            };

            update_other_items_ptg(items_sales_ptgs);

            // Initialize the items pie chart
            var piectx = $("#pieChart");
            var pieChart = new Chart(piectx, {
                type: 'doughnut',
                data: {
                    labels: sorted_items_by_sales.map(function (x) { return x[0] }),
                    datasets: [{
                        data: calculatePtgs((items_sales_ptgs)),
                        backgroundColor: [
                            'rgb(0,187,125)',
                            'rgb(0,185,141)',
                            'rgb(0,183,150)',
                            'rgb(0,182,157)',
                            'rgb(0,180,173)'
                        ],
                        borderColor: [
                            'rgb(250,250,250)',
                            'rgb(250,250,250)',
                            'rgb(250,250,250)',
                            'rgb(250,250,250)',
                            'rgb(250,250,250)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

            // Initialize the sales area chart
            var linectx = $("#lineChart");
            var lineChart = new Chart(linectx,
                {
                    "type": "line",
                    "data": {
                        "labels": [],
                        "datasets": [
                            {
                                "data": [],
                                "fill": "origin",
                                "borderWidth": 2,
                                "borderColor": "rgba(0, 187, 125, 0.8)",
                                "backgroundColor": "rgba(0, 187, 125, 0.2)",
                                "lineTension": 0.1,
                                "pointBackgroundColor": "rgba(0, 187, 125, 0.8)"
                            }
                        ]
                    },
                    "options": {
                        legend: { display: false },
                        scales: {
                            xAxes: [{
                                type: 'time',
                                ticks: {
                                    source: 'data'
                                },
                                time: {
                                    displayFormats: {
                                        day: 'DD',
                                        week: 'ddd',
                                        month: 'MMM',
                                        hour: 'hA',
                                        quarter: '[T]Q'
                                    },
                                    tooltipFormat: 'dddd, DD [de] MMMM [del] YYYY - hA',
                                    unit: 'day'
                                },
                                gridLines: { display: false }
                            }],
                            yAxes: [
                                {
                                    ticks: {
                                        callback: function (label, index, labels) {
                                            return numRound(label / 1000, 2) + 'K';
                                        }
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: '1K = 1000'
                                    },
                                    gridLines: { display: false }
                                }
                            ]
                        }
                    }
                }
            );

            // Initialize the day sales bar chart
            var barctx1 = $("#barChart1");
            var barChart1 = new Chart(barctx1, {
                "type": 'horizontalBar',
                "data": {
                    "labels": ["Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                    "datasets": [
                        {
                            "data": [
                                getRangeCount(trx_week_days, 1, 1),
                                getRangeCount(trx_week_days, 2, 2),
                                getRangeCount(trx_week_days, 3, 3),
                                getRangeCount(trx_week_days, 4, 4),
                                getRangeCount(trx_week_days, 5, 5),
                                getRangeCount(trx_week_days, 6, 6),
                                getRangeCount(trx_week_days, 0, 0)
                            ],
                            "backgroundColor": [
                                'rgb(0,187,125)',
                                'rgb(0,185,141)',
                                'rgb(0,183,150)',
                                'rgb(0,182,157)',
                                'rgb(0,181,165)',
                                'rgb(0,180,173)'
                            ]
                        }
                    ]
                },
                "options": {
                    legend: { display: false },
                    scales: {
                        xAxes: [{
                            gridLines: { display: false },
                            ticks: { display: false }
                        }],
                        yAxes: [{
                            gridLines: { display: false }
                        }]
                    },
                    animation: {
                        onComplete: function (animation) {
                            var chart = animation.animationObject.chart;
                            var ctx = chart.ctx;
                            ctx.TextAlign = "center";
                            ctx.TextBaseline = "bottom";

                            chart.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillStyle = "#aaa";
                                    ctx.fillText(dataset.data[i], model.x + 10, model.y);
                                }
                            });
                        }
                    }
                }
            });

            // Initialize the day time bar chart
            var barctx2 = $("#barChart2");
            var barChart2 = new Chart(barctx2, {
                "type": 'horizontalBar',
                "data": {
                    "labels": ["<10am", "10 - 12 pm", "12 - 2 pm", "2 - 4 pm", "4 - 6 pm", "6 - 8 pm", ">8pm"],
                    "datasets": [
                        {
                            "data": [
                                getRangeCount(trx_day_hours, 0, 9),
                                getRangeCount(trx_day_hours, 10, 11),
                                getRangeCount(trx_day_hours, 12, 13),
                                getRangeCount(trx_day_hours, 14, 15),
                                getRangeCount(trx_day_hours, 16, 17),
                                getRangeCount(trx_day_hours, 18, 19),
                                getRangeCount(trx_day_hours, 20, 24)
                            ],
                            "backgroundColor": [
                                'rgb(0,187,125)',
                                'rgb(0,185,141)',
                                'rgb(0,183,150)',
                                'rgb(0,182,157)',
                                'rgb(0,181,165)',
                                'rgb(0,180,173)'
                            ]
                        }
                    ]
                },
                "options": {
                    legend: { display: false },
                    scales: {
                        xAxes: [{
                            gridLines: { display: false },
                            ticks: { display: false }
                        }],
                        yAxes: [{
                            gridLines: { display: false }
                        }]
                    },
                    animation: {
                        onComplete: function (animation) {
                            var chart = animation.animationObject.chart;
                            var ctx = chart.ctx;
                            ctx.TextAlign = "center";
                            ctx.TextBaseline = "bottom";

                            chart.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillStyle = "#aaa";
                                    ctx.fillText(dataset.data[i], model.x + 10, model.y);
                                }
                            });
                        }
                    }
                }
            });

            // Initialize the customer age bar chart
            var barctx3 = $("#barChart3");
            var barChart3 = new Chart(barctx3, {
                "type": 'horizontalBar',
                "data": {
                    "labels": ["< 18", "19 - 25", "25 - 35", "36 - 50", "> 50"],
                    "datasets": [
                        {
                            "data": [
                                getAgeRangeCount(biquarter_data, 0, 18),
                                getAgeRangeCount(biquarter_data, 19, 25),
                                getAgeRangeCount(biquarter_data, 26, 35),
                                getAgeRangeCount(biquarter_data, 36, 60),
                                getAgeRangeCount(biquarter_data, 50, 150)
                            ],
                            "backgroundColor": [
                                'rgb(0,187,125)',
                                'rgb(0,185,141)',
                                'rgb(0,183,150)',
                                'rgb(0,182,157)',
                                'rgb(0,180,173)'
                            ]
                        }
                    ]
                },
                "options": {
                    legend: { display: false },
                    scales: {
                        xAxes: [{
                            gridLines: { display: false },
                            ticks: { display: false }
                        }],
                        yAxes: [{
                            gridLines: { display: false },
                            barThickness: 30
                        }]
                    },
                    animation: {
                        onComplete: function (animation) {
                            var chart = animation.animationObject.chart;
                            var ctx = chart.ctx;
                            ctx.TextAlign = "center";
                            ctx.TextBaseline = "bottom";

                            chart.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillStyle = "#aaa";
                                    ctx.fillText(dataset.data[i], model.x + 10, model.y);
                                }
                            });
                        }
                    }
                }
            });

            $("#menuPaymentType>div>div").click(function () {
                $("#menuPaymentType>div>div").removeClass("PyTyActive");
                $(this).addClass("PyTyActive");
                updatePaymentType($("#period-btn").val());
            });

            $("#menuVendedores>div>div").click(function () {
                $("#menuVendedores>div>div").removeClass("PyTyActive");
                $(this).addClass("PyTyActive");
                updateVendedores($("#period-btn").val());
            });

            $("#period-btn").change(function () {
                period = $(this).val();
                updateLineSalesChart(period);
                updateHourBarChart(period);
                updateDayBarChart(period);
                updatePieChart(period);
                updateSummary(period);
            });


            // ========= UPDATE LINE CHART SALES ========= 
            var updateLineSalesChart_Obj = function (grouped, timeUnit = "day") {
                var per_labels = Object.keys(grouped);

                lineChart.options.scales.xAxes[0].time.unit = timeUnit;

                if (timeUnit == 'hour') {
                    per_labels = per_labels.map((e) => e + ":");
                }
                per_labels = per_labels.map((e) => new Date(e));

                var per_data = getPeriodTotalFromLists(Object.values(grouped));

                lineChart.data.labels = per_labels;
                lineChart.data.datasets.forEach((dataset) => {
                    dataset.data = per_data;
                });
                lineChart.update();
            }

            // Default Setup
            updateLineSalesChart_Obj(groupByDay(filterByDate(tx_data, lastMonth)), 'day');

            var updateLineSalesChart = function (period) {

                if (period === "last-month") {
                    var grouped = groupByDay(filterByDate(tx_data, lastMonth));
                    updateLineSalesChart_Obj(grouped, 'day');
                } else if (period === "last-week") {
                    var grouped = groupByDay(filterByDate(tx_data, lastWeek));
                    updateLineSalesChart_Obj(grouped, 'week');
                } else if (period === "last-year") {
                    var grouped = groupByMonth(filterByDate(tx_data, lastYear));
                    updateLineSalesChart_Obj(grouped, 'month');
                } else if (period === "last-day") {
                    var grouped = groupByHour(filterByDate(tx_data, yesterday));
                    updateLineSalesChart_Obj(grouped, 'hour');
                } else if (period === "this-year") {
                    var grouped = groupByMonth(filterByDate(tx_data, currYear));
                    updateLineSalesChart_Obj(grouped, 'month');
                } else if (period === "this-month") {
                    var grouped = groupByDay(filterByDate(tx_data, currMonth));
                    updateLineSalesChart_Obj(grouped, 'day');
                } else if (period === "all") {
                    var grouped = groupByMonth(filterByDate(tx_data, allData));
                    updateLineSalesChart_Obj(grouped, 'month');
                }
            };

            // ============ UPDATE DAY OF WEEK BARCHART ============

            var updateDayBarChart_Obj = function (filterDate) {
                var week_days = getTrxWeekDays(filterByDate(tx_data, filterDate));

                barChart1.data.datasets.forEach((dataset) => {
                    dataset.data = [
                        getRangeCount(week_days, 1, 1),
                        getRangeCount(week_days, 2, 2),
                        getRangeCount(week_days, 3, 3),
                        getRangeCount(week_days, 4, 4),
                        getRangeCount(week_days, 5, 5),
                        getRangeCount(week_days, 6, 6),
                        getRangeCount(week_days, 0, 0)
                    ];
                });

                barChart1.update();

            }

            var updateDayBarChart = function (period) {
                if (period === "last-month") {
                    updateDayBarChart_Obj(lastMonth);
                } else if (period === "last-week") {
                    updateDayBarChart_Obj(lastWeek);
                } else if (period === "last-year") {
                    updateDayBarChart_Obj(lastYear);
                } else if (period === "last-day") {
                    updateDayBarChart_Obj(yesterday);
                } else if (period === "this-year") {
                    updateDayBarChart_Obj(currYear);
                } else if (period === "this-month") {
                    updateDayBarChart_Obj(currMonth);
                } else if (period === "all") {
                    updateDayBarChart_Obj(allData);
                }
            };

            // ============ UPDATE HOUR OF WEE BARCHART ============

            var updateHourBarChart_Obj = function (filterDate) {
                var day_hours = getTrxDayHours(filterByDate(tx_data, filterDate));

                barChart2.data.datasets.forEach((dataset) => {
                    dataset.data = [
                        getRangeCount(day_hours, 0, 9),
                        getRangeCount(day_hours, 10, 11),
                        getRangeCount(day_hours, 12, 13),
                        getRangeCount(day_hours, 14, 15),
                        getRangeCount(day_hours, 16, 17),
                        getRangeCount(day_hours, 18, 19),
                        getRangeCount(day_hours, 20, 24)
                    ];
                });

                barChart2.update();
            }

            var updateHourBarChart = function (period) {
                if (period === "last-month") {
                    updateHourBarChart_Obj(lastMonth);
                } else if (period === "last-week") {
                    updateHourBarChart_Obj(lastWeek);
                } else if (period === "last-year") {
                    updateHourBarChart_Obj(lastYear);
                } else if (period === "last-day") {
                    updateHourBarChart_Obj(yesterday);
                } else if (period === "this-year") {
                    updateHourBarChart_Obj(currYear);
                } else if (period === "this-month") {
                    updateHourBarChart_Obj(currMonth);
                } else if (period === "all") {
                    updateHourBarChart_Obj(allData);
                }
            };

            // ============ UPDATE PIE DATA OF TOP ITEMS ============

            var updatePieChart_Obj = function (filterDate) {
                var period_data = filterByDate(tx_data, filterDate);
                var period_items_by_sales = itemsBySales(period_data);
                var period_sales_ptgs = calculatePtgs(period_items_by_sales.map(function (x) { return x[1] }));
                pieChart.data.labels = period_items_by_sales.map(function (x) { return x[0] });
                pieChart.data.datasets.forEach((dataset) => {
                    dataset.data = calculatePtgs(period_sales_ptgs);
                });

                pieChart.update();
                for (var i = 0; i < 5; i++) {
                    if (period_items_by_sales[i] !== undefined) {
                        var elem = period_items_by_sales[i];
                        $("#item-" + (i + 1)).text(elem[0]);
                        $("#item-" + (i + 1) + "-value").text(numRound(period_sales_ptgs[i], 1) + "%");
                    } else {
                        $("#item-" + (i + 1)).text("");
                        $("#item-" + (i + 1) + "-value").text("");
                    }
                };

                update_other_items_ptg(period_sales_ptgs);
            }

            var updatePieChart = function (period) {
                if (period === "last-month") {
                    updatePieChart_Obj(lastMonth);
                } else if (period === "last-week") {
                    updatePieChart_Obj(lastWeek);
                } else if (period === "last-year") {
                    updatePieChart_Obj(lastYear);
                } else if (period === "last-day") {
                    updatePieChart_Obj(yesterday);
                } else if (period === "this-year") {
                    updatePieChart_Obj(currYear);
                } else if (period === "this-month") {
                    updatePieChart_Obj(currMonth);
                } else if (period === "all") {
                    updatePieChart_Obj(allData);
                }
            };

            // ============ UPDATE DELTA ============

            var updateDelta_Obj = function (periodDiff_prev, periodDiff_curr, txt) {
                var current_delta = calculatePeriodsDiff(tx_data, periodDiff_prev, periodDiff_curr);
                var delta_value = numRound(Math.abs(current_delta), 1);
                delta_value = (isNaN(delta_value)) ? "-" : delta_value;
                $("#delta-value").text(delta_value + "%");
                $("#delta-period").text(txt);

                if (current_delta > 0) {
                    $("#delta-direction").text("Más");
                    $("#delta-value").css("color", "#3fb378");
                } else if (current_delta < 0) {
                    $("#delta-direction").text("Menos");
                    $("#delta-value").css("color", "#dc3545");
                } else {
                    $("#delta-direction").text("Igual");
                    $("#delta-value").css("color", "#dc3545");
                }

            }

            var updateDelta = function (period) {
                if (period === "last-month") {
                    updateDelta_Obj(twoMonthsAgo, lastMonth, "el mes");
                } else if (period === "last-week") {
                    updateDelta_Obj(twoWeeksAgo, lastWeek, "la semana");
                } else if (period === "last-year") {
                    updateDelta_Obj(twoYearsAgo, lastYear, "el año");
                } else if (period === "last-day") {
                    updateDelta_Obj(yesterday, today, "el día");
                } else if (period === "this-year") {
                    updateDelta_Obj(yesterday, today, "el año");
                } else if (period === "this-month") {
                    updateDelta_Obj(yesterday, today, "el mes");
                } else if (period === "all") {
                    updateDelta_Obj(twoYearsAgo, lastYear, "el período");
                }
            };

            // ============ UPDATE PAYMENT TYPE ============

            var updatePaymentType = function (period) {
                if (period === "last-month") {
                    var period_data = filterByDate(tx_data, lastMonth);
                } else if (period === "last-year") {
                    var period_data = filterByDate(tx_data, lastYear);
                } else if (period === "last-week") {
                    var period_data = filterByDate(tx_data, lastWeek);
                } else if (period === "last-day") {
                    var period_data = filterByDate(tx_data, yesterday);
                } else if (period === "this-year") {
                    var period_data = filterByDate(tx_data, currYear);
                } else if (period === "this-month") {
                    var period_data = filterByDate(tx_data, currMonth);
                } else if (period === "all") {
                    var period_data = tx_data;
                }
                countByPaymentType(period_data, true);
            }

            // ============ UPDATE SALESMAN ============

            var updateVendedores = function (period) {
                if (period === "last-month") {
                    var period_data = filterByDate(tx_data, lastMonth);
                } else if (period === "last-year") {
                    var period_data = filterByDate(tx_data, lastYear);
                } else if (period === "last-week") {
                    var period_data = filterByDate(tx_data, lastWeek);
                } else if (period === "last-day") {
                    var period_data = filterByDate(tx_data, yesterday);
                } else if (period === "this-year") {
                    var period_data = filterByDate(tx_data, currYear);
                } else if (period === "this-month") {
                    var period_data = filterByDate(tx_data, currMonth);
                } else if (period === "all") {
                    var period_data = tx_data;
                }
                var profileStats = statsByGroup(period_data, groupByProfile, "prof_avatar");
                updateProfiles(profileStats);
            }

            // ============ UPDATE SUMMARY ============

            var updateSummary = function (period) {

                if (period === "last-month") {
                    var period_data = filterByDate(tx_data, lastMonth);
                } else if (period === "last-year") {
                    var period_data = filterByDate(tx_data, lastYear);
                } else if (period === "last-week") {
                    var period_data = filterByDate(tx_data, lastWeek);
                } else if (period === "last-day") {
                    var period_data = filterByDate(tx_data, yesterday);
                } else if (period === "this-year") {
                    var period_data = filterByDate(tx_data, currYear);
                } else if (period === "this-month") {
                    var period_data = filterByDate(tx_data, currMonth);
                } else if (period === "all") {
                    var period_data = tx_data;
                }

                var bills = groupByBills(period_data);
                var numBills = Object.keys(bills).length;
                var numItems = countItems(period_data);
                $("#trx-total").text("$ " + valueFormat(SumTotalValue(period_data)));
                $("#trx-amount").text(numBills);
                $("#trx-numitems").text(valueFormat(numItems));
                updateDelta(period);
                countByPaymentType(period_data, true);
                updateProfiles(statsByGroup(period_data, groupByProfile, "prof_avatar"));
            }

        }
    });



});