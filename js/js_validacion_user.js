$(document).ready(function () {

  $('.bar_loading_cont').hide();
  loadCountries("newCountry");
  setTimeout(function(){$("#newCountry").val("CO");},4000); 
  $("#newCountry").addClass("select_country_sty");
  $("#newCountry").css({'background-image':'url("../icons/flags/small/co.png")'});
  
  let validate_extra_vars = function() {

    // VALIDAR DIRECCION (OBLIGATORIO)
    let address = $("#act_dir").val().trim();
    let AcctCountry2 = $("#newCountry").val().trim(); 
    let AcctCity2 = $("#newCity").val().trim(); 
    let phone = $("#act_tel").val().trim();
    let NIT = $("#act_nit").val().trim(); 
    let email = $("#act_email").val().trim();
    let name = $("#act_name").val().trim();
    let hash = $("#act_hash").val().trim();
    let actEconom = $("#act_tip_act_ecom").val();


    if(email.length===0 || name.length === 0 || hash === 0){
      ErrorMsgModalBox('open', 'errorMsgValidation', 'Error en la validación.');
      return false;
    }

    if (address != "") {
      if (address.length > 50 || address.length <= 0) {
        ErrorMsgModalBox('open', 'errorMsgValidation', 'El número de caracteres de la dirección debe ser menor a 50.');
        ErrorInputBox('act_dir');
        return false;
      }
    }else{
      ErrorMsgModalBox('open', 'errorMsgValidation', 'El campo dirección es obligatiorio');
      ErrorInputBox('act_dir');
      return false
    }

    // VALIDAR PAIS (OBLIGATORIO)
    if(AcctCountry2==="" || AcctCountry2 === null){
      ErrorMsgModalBox('open', 'errorMsgValidation', 'El campo país es obligatiorio.');
      ErrorInputBox('newCountry');
      return false;
    }

    // VALIDAR CIUDAD (OBLIGATORIO)    
    if(AcctCity2===""){
      ErrorMsgModalBox('open', 'errorMsgValidation', 'El campo ciudad es obligatiorio.');
      ErrorInputBox('newCity');
      return false;
    }else if(AcctCity2.length>50){
      ErrorMsgModalBox('open', 'errorMsgValidation', 'El número de caracteres del campo ciudad debe ser menor a 50.');
      ErrorInputBox('newCity');
      return false;
    }

    // Validate NIT (OBLIGATORIO)
    if (NIT != "") {
      NIT = NIT.replace(/\./g, "");
      NIT = NIT.replace(/\-/g, "");
      NIT = NIT.replace(/\s/g, "");

      if (NIT.match(/^\d+$/)) {
        if(NIT.length>20){
          ErrorMsgModalBox('open', 'errorMsgValidation', 'El número de caracteres del campo debe ser menor a 20.');
          ErrorInputBox('act_nit');
          return false;
        }
      } else {
        ErrorMsgModalBox('open', 'errorMsgValidation', 'El campo de NIT debe contener solo valores numericos.');
        ErrorInputBox('act_nit');
        return false;
      }
    }else{
      ErrorMsgModalBox('open', 'errorMsgValidation', 'El campo NIT o ID es obligatiorio.');
      ErrorInputBox('act_nit');
      return false;
    }

    // VALIDAR TELEFONO (OPCIONAL)
    if (phone != "") {      
      if (phone.length > 15 || phone.length <= 0) {
        ErrorMsgModalBox('open', 'errorMsgValidation', 'El número de caracteres del teléfono debe ser menor a 15.');
        ErrorInputBox('act_tel');
        return false;
      }
    }

    return true;

  }


  $("#act_submit").click(function(){
    let bl = new BarLoading(10000,"Activando tu cuenta.");
    bl.start();

    let isValid = validate_extra_vars();
    if(isValid){
      let address = $("#act_dir").val().trim();
      let country = $("#newCountry").val().trim(); 
      let city = $("#newCity").val().trim(); 
      let phone = $("#act_tel").val().trim().replace(/[^0-9]/gm,"");
      let nit = $("#act_nit").val().trim().replace(/[^0-9]/gm,"");
      let email = $("#act_email").val().trim();
      let act_econ = $("#act_tip_act_ecom").val() || '0';
      let name = $("#act_name").val().trim();
      let hash = $("#act_hash").val().trim();

      $.ajax({
        url: 'activate_user.php',
        dataType: 'json',
        data: ({address,country,city,phone,nit,email,act_econ,hash,name}),
        type: 'POST',
        success: function(r){
          bl.stop();
          console.log(r);
          if(r[0][0]=="S"){
            alertify.message("Listo!");
            window.location.href = "https://www.2luca.co/php/sales.php";
          }else if(r[0][0]=="E"){
            ErrorMsgModalBox('open', 'errorMsgValidation', r[0][1]);
          }else{
            ErrorMsgModalBox('open', 'errorMsgValidation', 'Ocurrió un error durante la activación.');
          }
        }
      });
      
    }else{
      bl.stop();
    }

    
  })


});