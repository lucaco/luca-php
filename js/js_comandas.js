let x_first = true;
let alertOrders = true;
let talkOrders = true;
let newOrderList;
let stateWanted = 2;

$(document).ready(function () {

    $('#MainRoller_background').hide();

    get_data_comandas(2);

    setInterval(function () {
        get_data_comandas(stateWanted);
    }, 3000);

});

function get_data_comandas(s) {

    stateWanted = s;

    $.ajax({
        url: 'get_comandas_data.php',
        dataType: 'json',
        data: ({ x: 1 }),
        type: 'POST',
        success: function (orders) {
            if (orders.length > 0) {
                orders2 = orders;
                let orders_active = filterByState(orders, s);
                if (orders_active.length > 0) {
                    // Get pending Orders
                    getOrders(orders_active);
                    // Check the status
                    timer(orders_active);
                } else {
                    $(".body_kitchen_display").html('');
                    let htmlOrder = "<div class='no_prods_msg'><img src='../icons/SVG/41-shopping/basket-2.svg'> <p> No hay ninguna orden pendiente.</p></div>";
                    $(".body_kitchen_display").append(htmlOrder);
                }
            } else {
                $(".body_kitchen_display").html('');
                let htmlOrder = "<div class='no_prods_msg'><img src='../icons/SVG/41-shopping/basket-2.svg'> <p> No hay ninguna orden pendiente.</p></div>";
                $(".body_kitchen_display").append(htmlOrder);
            }
        }
    });

}

let refresh_comnd = function () {
    get_data_comandas(2);
    alertify.message("Comandas actualizadas.");
}

var filterByState = function (data, state) {
    return data.filter(function (x) {
        return x["comanda_state"] != state;
    });
};

//Generate the orders boxes
function getOrders(orders) {

    $(".body_kitchen_display").html('');

    if (orders.length > 0) {
        var orderList = [];
        var orderList2 = {};
        var counts = {};
        var counts2 = {};
        var htmlOrder = "";
        var text = "";
        var aux_text = 0;

        var initial = 0;
        var final = orders[0].id_bill;

        for (var i = 0; i < orders.length; i++) {

            orderList.push(orders[i].id_bill);
            orderList2[i] = orders[i]['num_items'] + " " + orders[i]['prod_name'] + " " + orders[i]['note'];

            //Order bottons at the end creation
            if (orders[i].id_bill !== final) {
                htmlOrder += "<div class='check_order' id='" + orders[i - 1]["id_bill"] + "_check' onclick='take_order(&#39;" + orders[i - 1]["id_bill"] + "&#39;)'> Tomar Orden <span class='mdi mdi-gesture-double-tap'></span> </div> </div> </div>"
                $(".body_kitchen_display").append(htmlOrder);
                htmlOrder = "";
                aux_text = 1;
                final = orders[i].id_bill;
                //Change the state given the order
                if (orders[i - 1].comanda_state == 0) {
                    counts[orders[i - 1].id_bill] = 1 + (counts[orders[i - 1].id_bill] || 0);
                } else if (orders[i - 1].comanda_state == 1) {
                    counts2[orders[i - 1].id_bill] = 1 + (counts2[orders[i - 1].id_bill] || 0);
                    $('#' + orders[i - 1].id_bill + '_check').replaceWith("<div class='check_order' id='" + orders[i - 1].id_bill + "_check' onclick='finish_order(&#39;" + orders[i - 1].id_bill + "&#39;)'> Terminar Orden <span class='mdi mdi-arrow-right-bold-outline'></span> </div>");
                    $('#' + orders[i - 1].id_bill + '_check').css("background-color", "#dddddd");
                } else if (orders[i - 1].comanda_state == 2) {
                    $('#' + orders[i - 1].id_bill + '_check').replaceWith("<div class='check_order' id='" + orders[i - 1].id_bill + "_check' onclick='take_order(&#39;" + orders[i - 1].id_bill + "&#39;)'> Retomar Orden <span class='mdi mdi-undo-variant'></span> </div>");
                    $('#' + orders[i - 1].id_bill + '_check').css("background-color", "#dddddd");
                }
            } else {
                if (aux_text == 0) {
                    text += orders[i]['item_count'] + " " + orders[i]['prod_name'] + " " + orders[i]['note'];
                }
            }
            //Order top creation
            if (orders[i].id_bill !== initial) {
                htmlOrder += "<div id='" + orders[i]["id_bill"] + "_box' class='order_box'>";
                htmlOrder += "<span class='cust_key hide'>" + orders[i]["cust_key"] + "</span>";
                htmlOrder += "<span class='cust_name hide'>" + orders[i]["cust_name"] + "</span>";
                htmlOrder += "<span class='cust_phone hide'>" + orders[i]["cust_phone"] + "</span>";
                htmlOrder += "<span class='bus_name hide'>" + orders[i]["bus_name"] + "</span>";
                htmlOrder += "<div class='order_box_top'><p>" + orders[i]['trx_date'].substring(11, 16) + " - " + orders[i]['salesman'] + " - " + orders[i]['id_bill'] + "</p> </div><div class='order_box_body'>";
                initial = orders[i].id_bill
            }
            //Order box body creation     
            if (orders[i].note !== "") {
                htmlOrder += "<div class='order_box_item_note'>";
                htmlOrder += "<p>" + orders[i]['item_count'] + " x " + orders[i]['prod_name'] + "</p><p>" + orders[i]['note'] + "</p></div>"
            } else {
                htmlOrder += "<div class='order_box_item'>";
                htmlOrder += "<p>" + orders[i]['item_count'] + " x " + orders[i]['prod_name'] + "</p></div>";
            }
        }

        //The last order card ending
        htmlOrder += "<div class='check_order' id='" + orders[orders.length - 1]["id_bill"] + "_check' onclick='take_order(&#39;" + orders[i - 1]["id_bill"] + "&#39;)'> Tomar Orden <span class='mdi mdi-gesture-double-tap'></span> </div> </div> </div>";
        $(".body_kitchen_display").append(htmlOrder);

        //Change the state given the order
        if (orders[orders.length - 1].comanda_state == 0) {
            counts[orders[orders.length - 1].id_bill] = 1 + (counts[orders[orders.length - 1].id_bill] || 0);
        } else if (orders[orders.length - 1].comanda_state == 1) {
            counts2[orders[orders.length - 1].id_bill] = 1 + (counts2[orders[orders.length - 1].id_bill] || 0);
            $('#' + orders[orders.length - 1].id_bill + '_check').replaceWith("<div class='check_order' id='" + orders[orders.length - 1].id_bill + "_check' onclick='finish_order(&#39;" + orders[orders.length - 1].id_bill + "&#39;)'> Terminar Orden <span class='mdi mdi-arrow-right-bold-outline'></span> </div>");
            $('#' + orders[orders.length - 1].id_bill + '_check').css("background-color", "#dddddd");
        } else if (orders[i - 1].comanda_state == 2) {
            $('#' + orders[orders.length - 1].id_bill + '_check').replaceWith("<div class='check_order' id='" + orders[orders.length - 1].id_bill + "_check' onclick='take_order(&#39;" + orders[orders.length - 1].id_bill + "&#39;)'> Retomar Orden <span class='mdi mdi-arrow-right-bold-outline'></span> </div>");
            $('#' + orders[orders.length - 1].id_bill + '_check').css("background-color", "#dddddd");
        }

    } else {
        htmlOrder = "<div class='no_prods_msg'><img src='../icons/SVG/41-shopping/basket-2.svg'> <p> No hay ninguna orden pendiente.</p></div>";
        $(".body_kitchen_display").append(htmlOrder);
    }


    //Alert new orders
    if (x_first) {
        newOrderList = orderList;
        x_first = false;
    } else {
        // comparing both arrays to check new items
        if (JSON.stringify(newOrderList) != JSON.stringify(orderList)) {

            if (stateWanted == 2) {
                if (alertOrders) {
                    PlaySound("beepsound");
                }
                if (talkOrders) {
                    readMe(text);
                }
            }
            newOrderList = orderList;
        }
    }

    //Number of orders taken and in queue
    $('#active_orders').text(Object.keys(counts).length);
    $('#taked_orders').text(Object.keys(counts2).length);

}

function PlaySound(soundObj) {
    let sound = document.getElementById(soundObj);
    sound.play();
}

function arrayRemove(arr, value) {

    return arr.filter(function (ele) {
        return ele != value;
    });

}

//Change the color by time waiting
function timer(orders) {

    if (orders.length > 0) {

        var today = new Date();

        for (var i = 0; i < orders.length; i++) {
            orderTime = new Date(Date.parse(orders[i].trx_date));
            diffTime = Date.dateDiff('n', orderTime, today);
            if (diffTime > 15) {
                $('#' + orders[i].id_bill + "_box .order_box_top").removeClass("order_box_top").addClass("order_box_top_alert")
            } else if (diffTime > 10) {
                $('#' + orders[i].id_bill + "_box .order_box_top").removeClass("order_box_top").addClass("order_box_top_slow")
            }
        }
    }
}

// datepart: 'y', 'm', 'w', 'd', 'h', 'n', 's'
Date.dateDiff = function (datepart, fromdate, todate) {
    datepart = datepart.toLowerCase();
    var diff = todate - fromdate;
    var divideBy = {
        w: 604800000,
        d: 86400000,
        h: 3600000,
        n: 60000,
        s: 1000
    };

    return Math.floor(diff / divideBy[datepart]);
}

//Change the status of the order: new to taked
function take_order(idBox) {

    let mr = new MainRoller();
    mr.startMainRoller();

    $.ajax({
        url: 'update_comandas_state.php',
        dataType: 'json',
        data: ({ id_bill: idBox, state: 1 }),
        type: 'POST',
        success: function (r) {
            mr.stopMainRoller();
            if (r[0][0] == 'S') {
                $('#' + idBox + '_check').replaceWith("<div class='check_order' id='" + idBox + "_check' onclick='finish_order(&#39;" + idBox + "&#39;)'> Terminar Orden <span class='mdi mdi-arrow-right-bold-outline'></span> </div>")
                $('#' + idBox + '_check').css("background-color", "#dddddd");
            } else {
                alertify.message('Error tomando la orden.');
            }
        }
    });

}

//Change the status of the order: finished
function finish_order(idBox, msgAlert) {

    if (typeof (msgAlert) === 'undefined') msgAlert = true;

    let cust_key = $('#' + idBox + '_box .cust_key').text().trim();
    let cust_name = $('#' + idBox + '_box .cust_name').text().trim();
    let cust_phone = $('#' + idBox + '_box .cust_phone').text().trim();
    let bus_name = $('#' + idBox + '_box .bus_name').text().trim();

    cust_name = (cust_name != '') ? cust_name.split(' ')[0] : cust_name;

    $.ajax({
        url: 'update_comandas_state.php',
        dataType: 'json',
        data: ({ id_bill: idBox, state: 2 }),
        type: 'POST',
        success: function (r) {
            if (r[0][0] == 'S') {

                $('#' + idBox + '_box').remove();
                newOrderList = arrayRemove(newOrderList, idBox);

                if (msgAlert) {
                    alertify.message(idBox + " order archivada.");
                }

                // ======= Send SMS Message TWILIO =======
                // {
                //     # "date_sent": "2020-07-14 18:44:10+00:00",
                //     # "sid": "SM57c011c03fbb4ab1b6dc8d5cd224460a",
                //     # "status": "queued",
                //     # "to": "+351920437821"
                //     #
                // }

                if (cust_phone != '' && cust_phone != '0' && cust_phone.length > 5 && cust_phone.length < 15 && cust_phone != undefined && cust_phone != 'null' && cust_key != '' && cust_key != 'null') {
                    let request = new XMLHttpRequest();
                    request.open('GET', 'https://us-central1-lucav1-209023.cloudfunctions.net/sms_order_tracker?cust_key=' + cust_key + '&cust_name=' + cust_name + '&bus_name=' + bus_name + '&cust_to=' + cust_phone, true);
                    request.onload = function () {
                        if (request.status >= 200 && request.status < 400) {
                            var data = JSON.parse(this.response);
                            let r_status = data['status'];
                            let sms_sid = data['sid'];
                            if (r_status == "queued" || r_status == "sent") {
                                alertify.message('SMS enviado');
                                // Save to log sms table
                                $.ajax({
                                    url: 'push_log_sms_order_tracker.php',
                                    dataType: 'json',
                                    data: ({ sms_sid, id_bill: idBox }),
                                    type: 'POST',
                                    success: function (r) {
                                        if (r[0][0] != 'S') {
                                            throw "Error pushing data to log sms order tracker: " + r[0][1];
                                        }
                                    }
                                });

                            } else {
                                console.log("Error sending SMS Twilio.");
                                console.log(data);
                            }
                        } else {
                            console.log("SMS API - Server Found An Error", "STATUS: " + request.status);
                        }
                    }
                    request.send();
                }
            } else {
                alertify.message('Error finalizando la orden.');
            }
        }
    });

}

function alert_activate() {

    if (alertOrders) {
        alertOrders = false;
        $('#alarm > img').attr('src', "../icons/SVG/05-time/alarm-silent.svg");
        alertify.message('Alerta desactivada.');
    } else {
        alertOrders = true;
        $('#alarm > img').attr('src', "../icons/SVG/05-time/alarm-sound.svg");
        alertify.message('Alerta activada.');
    }
}

function talk_activate() {

    if (talkOrders) {
        talkOrders = false;
        $('#talk > img').attr('src', "../icons/SVG/10-messages-chat/bubble-chat-remove-2.svg");
        alertify.message('Voz desactivada.');
    } else {
        talkOrders = true;
        $('#talk > img').attr('src', "../icons/SVG/10-messages-chat/bubble-chat-check-2.svg");
        alertify.message('Voz activada.');
    }
}