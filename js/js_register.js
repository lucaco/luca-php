$(document).ready(function () {

  $('#checkmark_register').hide();
  $('#LoadingRoller_register').hide();

  // ************ LOGIN ******************
  $('#login-form').submit(function () {
    email = $("#login-email").val();
    pass = $("#login-pass").val();
    remem = String($("#login-rememberme").prop('checked'));

    $.ajax({
      url: "login_func.php",
      data: ({
        login_email: email,
        login_pass: pass,
        login_remember: remem
      }),

      type: "POST",
      success: function (r) {
        if (r == '1') {
          window.location.href = "sales.php";
        } else {
          ErrorMsgModalBox('open', 'errorMsgLogin', r);
        }

      }
    });
    return false;
  });
  //**************************************

  $("#reg-industry").off("change").change(function () {
    let id = $(this).val();
    if (id !== null) {
      $("#reg-industry").addClass("selectIcn");
      $.ajax({
        url: 'get_icon_typeind.php',
        data: ({ id }),
        type: 'POST',
        success: function (r) {
          r = encodeURI(r).replace(/%E2%81%A9/gm, "").replace(/%0A/gm, "");
          $("#reg-industry").css({ "background-image": "url(" + r + ")" })
        },
        error: function () {
          $("#reg-industry").removeClass("selectIcn");
        }
      });
    } else {
      $("#reg-industry").removeClass("selectIcn");
    }
  });

  // ::::::::::::::: Validar el formulario de registro ::::::::::::
  let validateRegForm = function () {

    var isOk = true;
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    // Business Name 
    if ($("#reg-business-name").val() == "") {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Ingresa el nombre de tu negocio.');
      ErrorInputBox("reg-business-name");
      isOk = false;
      $("#reg-business-name").focus();
      return isOk;
    }

    if ($("#reg-business-name").val().length > 50) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Nombre del negocio demasiado largo');
      ErrorInputBox("reg-business-name");
      isOk = false;
      $("#reg-business-name").focus();
      return isOk;
    }
    // email
    if ($("#reg-email").val() == "") {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Ingresa el email');
      ErrorInputBox("reg-email");
      isOk = false;
      $("#reg-email").focus();
      return isOk;
    }

    if ($("#reg-email").val().length > 50) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Correo electrónico demasiado largo');
      ErrorInputBox("reg-email");
      isOk = false;
      $("#reg-email").focus();
      return isOk;
    }

    // VALIDACION DE EMAIL 
    if (testEmail.test($("#reg-email").val())) { } else {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Ingresar un email válido');
      ErrorInputBox("reg-email");
      isOk = false;
      $("#reg-email").focus();
      return isOk;
    }

    // Password 1
    if ($("#reg-pass").val() == "") {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Ingresa la contraseña');
      ErrorInputBox("reg-pass");
      isOk = false;
      $("#reg-pass").focus();
      return isOk;
    }

    // Validar Número de caracteres en contraseña
    if ($("#reg-pass").val().length < 7) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'La contraseña debe contener mínimo 7 caracteres');
      ErrorInputBox("reg-pass");
      isOk = false;
      $("#reg-pass").focus();
      return isOk;
    }

    // Validar Número de caracteres en contraseña
    if ($("#reg-pass").val().length > 30) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'La contraseña debe contener máximo 30 caracteres');
      ErrorInputBox("reg-pass");
      isOk = false;
      $("#reg-pass").focus();
      return isOk;
    }

    var strPas = checkStrengthPass($("#reg-pass").val());
    // Password 1 - Strenght
    if (strPas <= 3) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'La contraseña es muy débil. Intenta usando una combianción de letras mayúsculas, minúsculas, números y caracteres especiales.');
      ErrorInputBox("reg-pass");
      isOk = false;
      $("#reg-pass").focus();
      return isOk;
    }

    // Password 2
    if ($("#reg-pass-2").val() == "") {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Ingresa la validación de la contraseña');
      ErrorInputBox("reg-pass-2");
      isOk = false;
      $("#reg-pass-2").focus();
      return isOk;
    }

    // VALIDACIÓN DE CONTRASEÑA
    if ($("#reg-pass").val() != $("#reg-pass-2").val()) {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Las contraseñas no coinciden');
      ErrorInputBox("reg-pass-2");
      isOk = false;
      $("#reg-pass-2").focus();
      return isOk;
    }

    // Industria 
    if ($("#reg-industry option:selected").text() == "Tipo de industria:") {
      ErrorMsgModalBox('open', 'errorMsgRegister', 'Selecciona el tipo de industria');
      ErrorInputBox("reg-industry");
      isOk = false;
      $("#reg-industry").focus();
      return isOk;
    }

    return isOk

  }

  $("#logo_image").click(function () {
    window.location.href = "../index.php";
  });

  $('#reg-pass').keyup(function () {
    var pass = $("#reg-pass").val();
    var strg_pass = checkStrengthPass(pass);

    if (strg_pass <= 2) {
      $('#pass_lvl').text("Débil");
      $('#pass_lvl_bar').css("width", "40px");
      $('#pass_lvl_bar').css("background-color", "#de5e5e");
    } else if (strg_pass == 3) {
      $('#pass_lvl').text("Media");
      $('#pass_lvl_bar').css("width", "80px");
      $('#pass_lvl_bar').css("background-color", "#FF9900");
    } else if (strg_pass == 4) {
      $('#pass_lvl').text("Buena");
      $('#pass_lvl_bar').css("width", "120px");
      $('#pass_lvl_bar').css("background-color", "#FFCC00");
    } else if (strg_pass == 5) {
      $('#pass_lvl').text("Muy buena");
      $('#pass_lvl_bar').css("width", "160px");
      $('#pass_lvl_bar').css("background-color", "#779933");
    } else if (strg_pass == 6) {
      $('#pass_lvl').text("Excelente!");
      $('#pass_lvl_bar').css("width", "200px");
      $('#pass_lvl_bar').css("background-color", "#669933");
    }

  });

  $("#RegForm").submit(function (e) {

    var IsValid = validateRegForm();

    if (IsValid) {
      $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        type: $(this).attr('method'),
        dataType: 'json',
        cache: false,
        beforeSend: function () {
          $('#reg-submit').hide();
          $('#LoadingRoller_register').show();
          $('#checkmark_register').hide();
        },
        success: function (r) {
          $('#LoadingRoller_register').hide();
          if (r[0] == 'OK') {
            $('#checkmark_register').show();
            setTimeout(function () {
              $('#reg-submit').show();
              let al1 = new NotifyAlert("¡Bienvenido!", "Revisa la bandeja de entrada de tu correo electrónico. Hemos enviado un email para que confirmes tu correo antes de poder acceder a tu cuenta. Recuerda revisar en tu bandeja de correos no deseados.", function () {
                window.location.href = "https://www.2luca.co/";
              }).open();
              SimpleMsgModalBox('open', 'simpleMsgRegister', '¡Excelente!');
              $('#checkmark_register').hide();
            }, 2000);
          } else if (r[0] == 'UAR') {
            let al1 = new NotifyAlert("El correo ya existe", "Parece que el email ingresado ya está registrado. Da click aquí para ingresar a tu cuenta.", function () {
              OpenLogInForm(); new NotifyAlert().close();
            }).open();
            $('#reg-submit').show();
          } else if (r[0] == 'UNA') {
            let al1 = new NotifyAlert("La cuenta no está activa", "Parece que la cuenta no está activada todavía. Hemos enviado de nuevo el link de activación a tu correo.", function () {
              window.location.href = "https://www.2luca.co/";
            }).open();
            $('#reg-submit').show();
          } else if (r[0] == 'UG') {
            let al1 = new NotifyAlert("¿Quieres resgresar?", r[1], function () {
              window.location.href = "https://www.2luca.co/php/" + r[2];
            }).open();
            $('#reg-submit').show();
          } else if (r[0] == 'ERR') {
            ErrorMsgModalBox('open', 'errorMsgRegister', 'Upps! Hubo un error registrando tu cuenta. Intenta de nuevo.');
            $('#reg-submit').show();
          } else {
            console.log(r);
            ErrorMsgModalBox('open', 'errorMsgRegister', 'Upps! Hubo un error registrando tu cuenta. Intenta de nuevo.');
            $('#reg-submit').show();
          }
        },
        error: function (request, status, error) {
          console.log(error);
          ErrorMsgModalBox('open', 'errorMsgRegister', 'No fue posible registrar al cliente. Por favor, intenta de nuevo.');
          $('#reg-submit').show();
          $('#checkmark_register').hide();
          $('#LoadingRoller_register').hide();
        }
      });

    }

    return false;

  });

});