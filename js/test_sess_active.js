(function () {
    /**** CHECK IF THERE IS AN ACTIVE SESSION ****/
    let intervalSession = setInterval(function () {
        if (navigator.onLine) { TestSessionIsActive() }
    }, 5000);

    function TestSessionIsActive() {
        $.ajax({
            url: "../php/test_sess_active.php",
            data: ({ x: 1 }),
            type: "POST",
            success: function (r) {
                if (r == '') {
                    ReLoginActiveUser();
                    // alertify.confirm('Sesión Desconectada', "La sesión de usuario se desconectó. ¿Deseas reconectarla?", function () {
                    //     ReLoginActiveUser();
                    // }, function () {
                    //     window.location = '../php/logout.php';
                    // });
                }
            }
        });
    }

    function ReLoginActiveUser() {
        var x = GetVariableCookie('CURRACCKEY').trim();
        $.ajax({
            url: "../php/reconect_sess.php",
            data: ({ CURRACCKEY: x }),
            type: "POST",
            success: function (r) {
                if (r.trim() == '1') {
                    alertify.message("Sesión reconectada");
                } else {
                    alertify.alert("", "No fue posible reconectar la sesión");
                    window.location = '../php/logout.php';
                }
            }
        });
    }
}());