let cur_orders = {};

$(document).ready(function () {

    class ordersTrackerObj {
        /**
         * Creates or updates an order tracker.
         * 
         * @param {string} id_target Id target of div element in DOM.
         * @param {string} bus_name Name of the business to display.
         * @param {string} order_id Order bill Id.
         * @param {string} state Stage to activate in the order. Can be: 'initial', 'stage_1', 'stage_2', 'stage_3', 'stage_4'.
         * @param {string} id_el Id for the element.
         * @param {string} date_creation Date of creation of order to display.
         * 
         * @example
         * var x1 = new ordersTrackerObj("orders_wrap", "La Mona Que Baila", "AAD-1");
         * var x2 = new ordersTrackerObj("orders_wrap", "La Mona Que Rie", "XD-11", "stage_1");    
         * // To update stage...
         * x1.set_stage("stage_3");
         */
        constructor(id_target, bus_name, order_id = '', state = 'initial', id_el = '', date_creation = '') {
            this.id_target = id_target;
            this.order_id = order_id;
            this.bus_name = bus_name;
            this.state = state;
            this.prm_msg = 'Aguarda un momento';
            this.scn_msg = '';
            this.st_msg = '';
            this.creation_time = (date_creation === '') ? "Inició a las " + moment().format("h:mm a") : "Inició a las " + moment(date_creation).format("h:mm a");
            this.stage_1_st = 'inactive';
            this.stage_2_st = 'inactive';
            this.stage_3_st = 'inactive';
            this.stage_4_st = 'inactive';

            this.id_el = (id_el === '') ? RandomIntNumber(10000, 99999) : id_el;

            this.build();
            this.set_stage(this.state);
        }

        build() {
            $('#' + this.id_el).remove();
            let txtHTML = '';
            txtHTML += '<div id="' + this.id_el + '" class="boxc1_wrap bigmrg animate_bounce_1"><span class="mdi mdi-close close_btn"></span>';
            txtHTML += '<h5 class="ord_title fs19 mayusc letSp1">' + this.bus_name + ' <span>ORDEN #' + this.order_id + '</span></h5>';
            txtHTML += '<div class="orders_card_container">';
            txtHTML += '<div class="orders_icn_wrap">';
            txtHTML += '<div><svg class="stg1 icon ' + this.stage_1_st + '" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewBox="0 0 32 32"><path d="M19.404,7.303L20.5,6.207l0.146,0.146C20.744,6.451,20.872,6.5,21,6.5s0.256-0.049,0.354-0.146 c0.195-0.195,0.195-0.512,0-0.707l-1-1c-0.195-0.195-0.512-0.195-0.707,0s-0.195,0.512,0,0.707L19.793,5.5l-1.096,1.096 c-1.303-1.18-2.911-2.03-4.697-2.394V1h1c0.276,0,0.5-0.224,0.5-0.5S15.276,0,15,0H9C8.724,0,8.5,0.224,8.5,0.5S8.724,1,9,1h1 v3.202C5.441,5.131,2,9.171,2,14c0,5.515,4.486,10,10,10s10-4.485,10-10C22,11.422,21.012,9.078,19.404,7.303z M11,1h2v3.051 C12.671,4.018,12.337,4,12,4s-0.671,0.018-1,0.051V1z M12,23c-4.962,0-9-4.038-9-9s4.038-9,9-9s9,4.038,9,9S16.962,23,12,23z"></path><path d="M7.854,9.146c-0.195-0.195-0.512-0.195-0.707,0s-0.195,0.512,0,0.707l4.5,4.5c0.195,0.195,0.512,0.195,0.707,0 c0.195-0.195,0.195-0.512,0-0.707L7.854,9.146z"></path></svg></div>';
            txtHTML += '<div><svg class="stg2 icon ' + this.stage_2_st + '" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewBox="0 0 32 32"><path d="M12.502,13h-8c-0.276,0-0.5,0.224-0.5,0.5v6c0,0.276,0.224,0.5,0.5,0.5h8c0.276,0,0.5-0.224,0.5-0.5v-6 C13.002,13.224,12.778,13,12.502,13z M12.002,19h-7v-5h7V19z"></path><path d="M23.952,6.29c-0.002-0.004-0.001-0.009-0.003-0.013l-2-4C21.864,2.107,21.691,2,21.502,2h-19 C2.312,2,2.14,2.107,2.055,2.276l-2,4C0.053,6.281,0.054,6.285,0.052,6.29c-0.03,0.064-0.05,0.135-0.05,0.21v1 c0,0.89,0.359,1.777,0.986,2.436c0.298,0.312,0.643,0.553,1.014,0.723V22.5c0,0.276,0.224,0.5,0.5,0.5h12h5h2 c0.276,0,0.5-0.224,0.5-0.5V10.674c1.164-0.553,2-1.781,2-3.174v-1C24.002,6.424,23.982,6.354,23.952,6.29z M22.693,6h-3.801 l-0.75-3h3.051L22.693,6z M18.002,9.341c-0.701,1.035-1.825,1.653-3.041,1.653c-0.937,0-1.799-0.386-2.459-1.069V7h5.5V9.341z M6.002,7h5.5v2.926c-0.66,0.683-1.519,1.069-2.451,1.069c-1.201,0-2.354-0.633-3.049-1.654V7z M11.502,3v3H6.143l0.75-3H11.502z M17.111,3l0.75,3h-5.359V3H17.111z M2.811,3h3.051l-0.75,3H1.311L2.811,3z M1.002,7.5V7h4v2.27 c-1.06,0.936-2.407,0.901-3.29-0.025C1.261,8.772,1.002,8.135,1.002,7.5z M15.002,22v-8h4v8H15.002z M21.002,22h-1v-8.5 c0-0.276-0.224-0.5-0.5-0.5h-5c-0.276,0-0.5,0.224-0.5,0.5V22h-11V10.929c0.823,0.09,1.685-0.127,2.416-0.69 c0.893,1.095,2.239,1.756,3.633,1.756c1.104,0,2.135-0.402,2.951-1.143c0.818,0.74,1.852,1.142,2.959,1.142 c1.414,0,2.732-0.646,3.626-1.754c0.639,0.491,1.344,0.748,2.068,0.748c0.118,0,0.231-0.024,0.347-0.038V22z M23.002,7.5 c0,1.325-1.097,2.487-2.347,2.487c-0.571,0-1.137-0.254-1.653-0.716V7h4V7.5z"></path><circle cx="17.502" cy="18.5" r="0.5"></circle></svg></div>';
            txtHTML += '<div><svg class="stg3 icon ' + this.stage_3_st + '" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewBox="0 0 32 32"> <path d="M21.5,2h-19C1.121,2,0,3.122,0,4.5v14C0,19.878,1.121,21,2.5,21h19c1.379,0,2.5-1.122,2.5-2.5v-14 C24,3.122,22.879,2,21.5,2z M2.5,3h19C22.327,3,23,3.673,23,4.5V7H1V4.5C1,3.673,1.673,3,2.5,3z M21.5,20h-19 C1.673,20,1,19.327,1,18.5V8h22v10.5C23,19.327,22.327,20,21.5,20z"></path><circle cx="4" cy="5" r="1"></circle><circle cx="7" cy="5" r="1"></circle><circle cx="10" cy="5" r="1"></circle><path d="M18,11H6c-1.654,0-3,1.346-3,3s1.346,3,3,3h12c1.654,0,3-1.346,3-3S19.654,11,18,11z M13.387,12l-4,4H7.801l4-4H13.387z M6,12h1.387l-3.07,3.07C4.118,14.761,4,14.395,4,14C4,12.896,4.897,12,6,12z M5.051,15.75L8.801,12h1.586l-4,4H6 C5.655,16,5.335,15.904,5.051,15.75z M18,16h-7.199l4-4H18c1.103,0,2,0.896,2,2C20,15.103,19.103,16,18,16z"></path></svg></div>';
            txtHTML += '<div><svg class="stg4 icon ' + this.stage_4_st + '" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewBox="0 0 32 32"><path d="M16.659,8.135l-7.146,6.67l-2.159-2.158c-0.195-0.195-0.512-0.195-0.707,0s-0.195,0.512,0,0.707l2.5,2.5 c0.19,0.19,0.497,0.196,0.695,0.012l7.5-7c0.202-0.188,0.212-0.505,0.024-0.707C17.177,7.957,16.861,7.946,16.659,8.135z"></path><path d="M12,0C5.383,0,0,5.383,0,12s5.383,12,12,12s12-5.383,12-12S18.617,0,12,0z M12,23C5.935,23,1,18.066,1,12 C1,5.935,5.935,1,12,1s11,4.935,11,11C23,18.066,18.065,23,12,23z"></path></svg></div>';
            txtHTML += '</div><div class="orders_msg_wrap">';
            txtHTML += '<span class="timer">' + this.creation_time + '</span> ';
            txtHTML += '<p class="prm_msg ' + this.st_msg + '">' + this.prm_msg + '</p>';
            txtHTML += '<span class="scn_msg">' + this.scn_msg + '</span> ';
            txtHTML += '</div></div></div>';
            $('#' + this.id_target).prepend(txtHTML)

            let me = this;
            $("#" + this.id_el + " .close_btn").off('click').click(() => { me.destroy() })
        }

        destroy() {
            $('#' + this.id_el).remove();
        }

        get_cur_stage() {
            return this.state;
        }

        set_stage(newStage) {
            this.state = newStage;

            $("#" + this.id_el + " .stg1").removeClass("inactive");
            $("#" + this.id_el + " .stg1").removeClass("active");
            $("#" + this.id_el + " .stg2").removeClass("inactive");
            $("#" + this.id_el + " .stg2").removeClass("active");
            $("#" + this.id_el + " .stg3").removeClass("inactive");
            $("#" + this.id_el + " .stg3").removeClass("active");
            $("#" + this.id_el + " .stg4").removeClass("inactive");
            $("#" + this.id_el + " .stg4").removeClass("active");
            $("#" + this.id_el + " .prm_msg").removeClass('active');

            if (this.state === 'initial') {
                $("#" + this.id_el + " .stg1").addClass('inactive');
                $("#" + this.id_el + " .stg2").addClass('inactive');
                $("#" + this.id_el + " .stg3").addClass('inactive');
                $("#" + this.id_el + " .stg4").addClass('inactive');
                $("#" + this.id_el + " .prm_msg").text('Aguarda un momento');
                $("#" + this.id_el + " .scn_msg").text('');
            } else if (this.state === 'stage_1') {
                $("#" + this.id_el + " .stg1").addClass('active');
                $("#" + this.id_el + " .stg2").addClass('inactive');
                $("#" + this.id_el + " .stg3").addClass('inactive');
                $("#" + this.id_el + " .stg4").addClass('inactive');
                $("#" + this.id_el + " .prm_msg").text('Tu orden está en espera');
                $("#" + this.id_el + " .scn_msg").text('');
            } else if (this.state === 'stage_2') {
                $("#" + this.id_el + " .stg1").addClass('');
                $("#" + this.id_el + " .stg2").addClass('active');
                $("#" + this.id_el + " .stg3").addClass('inactive');
                $("#" + this.id_el + " .stg4").addClass('inactive');
                $("#" + this.id_el + " .prm_msg").text('Tu orden fue aceptada');
                $("#" + this.id_el + " .scn_msg").text('');
            } else if (this.state === 'stage_3') {
                $("#" + this.id_el + " .stg1").addClass('');
                $("#" + this.id_el + " .stg2").addClass('');
                $("#" + this.id_el + " .stg3").addClass('active');
                $("#" + this.id_el + " .stg4").addClass('inactive');
                $("#" + this.id_el + " .prm_msg").text('Tu orden está en proceso');
                $("#" + this.id_el + " .scn_msg").text('');
            } else if (this.state === 'stage_4') {
                $("#" + this.id_el + " .stg1").addClass('');
                $("#" + this.id_el + " .stg2").addClass('');
                $("#" + this.id_el + " .stg3").addClass('');
                $("#" + this.id_el + " .stg4").addClass('active');
                $("#" + this.id_el + " .prm_msg").text('Tu orden está lista!');
                $("#" + this.id_el + " .scn_msg").text('Puedes pasar a recoger tu orden.');
                $("#" + this.id_el + " .prm_msg").addClass('active');
            }

        }
    }


    /**
     * Make call to server every 3 seconds to check orders states.
     */
    $("#ord_tracker_container").hide();


    setInterval(function () {

        let cust_key = $("#ckphp").text();

        if (cust_key != '') {
            $.ajax({
                url: 'get_orderstracker.php',
                dataType: 'json',
                data: ({ cust_key }),
                type: 'POST',
                success: function (orders) {
                    if (orders.length > 0) {
                        $("#ord_tracker_container").show();
                        for (var i = 0; i < orders.length; i++) {
                            var bus_name = orders[i]["bus_name"];
                            var id_bill = orders[i]["id_bill"];
                            var trx_date = orders[i]["trx_date"];
                            var com_state = orders[i]["comanda_state"];
                            if (com_state == 0 && !(id_bill in cur_orders)) {
                                cur_orders[id_bill] = new ordersTrackerObj("orders_wrap", bus_name, id_bill, 'stage_2', id_bill, trx_date);
                            } else if (com_state > 0 && !(id_bill in cur_orders)) {
                                cur_orders[id_bill] = new ordersTrackerObj("orders_wrap", bus_name, id_bill, 'stage_' + (parseInt(com_state) + 2), id_bill, trx_date);
                            } else if (com_state > 0 && (id_bill in cur_orders)) {
                                cur_orders[id_bill].set_stage("stage_" + (parseInt(com_state) + 2));
                            }
                        }
                    } else {
                        $("#ord_tracker_container").hide();
                    }
                }
            });
        }

    }, 3000);

});