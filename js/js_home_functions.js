
let NAMES_COLOMBIA, APELLIDOS_COLOMBIA, NAMES_GENDER;
let msgOfflineAlert = false;
let msgOfflineAlert2 = false;
let UnidadesMedida = ['MICRA', 'MICRAS', 'MILIMETRO', 'MILIMETROS', 'CENTIMETRO', 'CENTIMETROS', 'METRO', 'METROS', 'KILOMETRO', 'KILOMETROS', 'PULGADA', 'PULGADAS', 'PIE', 'PIES', 'YARDA', 'YARDAS', 'MILLA', 'MILLAS', 'PIE CUADRADO', 'PIES CUADRADOS', 'YARDA CUADRADA', 'YARDAS CUADRADAS', 'MILLA CUADRADA', 'MILLAS CUADRADAS', 'METRO CUADRADO', 'METROS CUADRADOS', 'HECTAREA', 'HECTAREAS', 'KILOMETRO CUADRADO', 'KILOMETROS CUADRADOS', 'CENTIMETRO CUBICO', 'CENTIMETROS CUBICOS', 'METRO CUBICO', 'METROS CUBICOS', 'PIE CUBICO', 'PIES CUBICOS', 'YARDA CUBICA', 'YARDAS CUBICAS', 'MICROLITRO', 'MICROLITROS', 'MILILITRO', 'MILILITROS', 'LITRO', 'LITROS', 'MICROGRAMO', 'MICROGRAMOS', 'MILIGRAMO', 'MILIGRAMOS', 'GRAMO', 'GRAMOS', 'KILOGRAMO', 'KILOGRAMOS', 'TONELADA METRICA', 'TONELADAS METRICAS', 'ONZA', 'ONZAS', 'LIBRA', 'LIBRAS', 'LIBRA POR PULGADA CUADRADA', 'LIBRAS POR PULGADA CUADRADA', 'PARTES POR MIL', 'PARTES POR MILES', 'PARTES POR MILLON', 'PARTES POR MILLONES', 'SEGUNDO', 'SEGUNDOS', 'MINUTO', 'MINUTOS', 'HORA', 'HORAS', 'KILOVATIO HORA', 'KILOVATIOS HORA'];

let Item = (function (name, id, price, units, tax, category, color_category, icon_src, cost) {
    this.name = name;
    this.id = id;
    this.price = numRound(Number(price), 2);
    this.cost = numRound(Number(cost), 2);;
    this.units = units;
    this.tax = tax;
    this.category = category;
    this.color_category = color_category;
    this.icon_src = icon_src;
});


/**
 * Convertit un objeto JSON a arreglo. {{},{},{},...} => [{},{},{},...]
 * 
 * @param {json} json_obj Json Object
 * 
 * @example
 * toJsonArray({0: {'nombre':'Juan'}, 1: {'nombre':'Maria'}})
 */
let toJsonArray = (function (json_obj) {
    let keys_ = Object.keys(json_obj);
    let jsArr = [];
    keys_.forEach((e) => { jsArr.push(json_obj[e]) });
    return jsArr;
});

/***************************************************************************/
/****************************** CLASSES ************************************/
/***************************************************************************/

class modalsManager {
    constructor(object_id) {
        this.object_id = object_id;
    }

    /**
     * Open the modal.
     */
    open() {
        CloseMenu();
        $('body').addClass('noscroll');
        $('#' + this.object_id + ' .msg-box-1').css({ "display": "block" });
        $('#' + this.object_id).css({ "display": "block" });
    }

    /**
     * Close the modal.
     */
    close() {
        $('body').removeClass('noscroll');
        $('#' + this.object_id + ' .msg-box-1').css({ "display": "none" });
        $('#' + this.object_id).css({ "display": "none" });
    }

    /**
     * Close the modal that is over other modal.
     */
    close_over() {
        $('#' + this.object_id + ' .msg-box-1').css({ "display": "none" });
        $('#' + this.object_id).css({ "display": "none" });
    }

}

/**
 * paymentsProccessObj - Creates modal of payment process given a package id.
 *
 * @author  Juan Díaz (original founder) <2lucaco@gmail.com>
 */
class paymentsProccessObj {
    constructor(close_over = false) {
        this.close_over = close_over;
        this.is_set = false;
        this.pkg_id = -1;
        this.susc_id = '';
        this.pkg_name = '';
        this.id_object = 'msg-box-container-PaymentInfo';
        this.fun_success = () => { };

        this.build();
        this.add_events();
    }

    build() {
        $("#" + this.id_object).remove();
        let txtHTML = '';
        txtHTML += '<div class="msg-box-container" id="' + this.id_object + '"><div class="msg-box-1" id="msg-box-PaymentInfo">';
        txtHTML += '<p class="msgb-box-1-exit-mark">X</p>';
        txtHTML += '<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-credit-card"></span> DATOS DE PAGO</p>';
        txtHTML += '<div class="paym_info_box">';
        txtHTML += '<div><img id="pck_img" src="" alt="frt_icon"></div>';
        txtHTML += '<div><p id="pck_name"></p><p id="pck_cost"></p></div>';
        txtHTML += '</div>';
        txtHTML += '<h2 class="subtitle_1 border_btn">Datos del Titular</h2>';
        txtHTML += '<div class="py_tit_data_cont">';
        txtHTML += '<div class="luca_link" id="fill_bus_data_py">Utilizar datos del negocio</div>';
        txtHTML += '<h4>Nombre del titular:</h4><input class="msg-box-1-input" type="text" name="payName" id="payName" placeholder="Eduardo Herrera">';
        txtHTML += '<h4>Email:</h4><input class="msg-box-1-input" type="email" name="payEmail" id="payEmail" placeholder="eduher@ejemplo.com">';
        txtHTML += '<h4>Ciudad:</h4><input class="msg-box-1-input" type="text" name="payCity" id="payCity" placeholder="Bogotá">';
        txtHTML += '<h4>Dirección:</h4><input class="msg-box-1-input" type="text" name="payAddress" id="payAddress" placeholder="Calle 123A # 45 - 67">';
        txtHTML += '<h4>Teléfono:</h4><input class="msg-box-1-input input_phone" type="text" name="payTel" id="payTel" placeholder="7654321">';
        txtHTML += '<h4>Tipo Documento:</h4>';
        txtHTML += '<select class="msg-box-1-input" name="payTypDoc" id="payTypDoc"> <option value="" selected disabled hidden>Selecciona una opción</option><option value="CC">Cédula Ciudadania</option><option value="CE">Cédula Extranjería</option><option value="PPN">Pasaporte</option><option value="SSN">Número de Seguridad Social</option><option value="LIC">Licencia de Conducción</option><option value="NIT">NIT</option><option value="TI">Tarjeta de Identidad</option><option value="DNI">Documento Nacional de Identificación</option></select>';
        txtHTML += '<h4>Número de Identificación:</h4><input class="msg-box-1-input input_int" type="text" name="payDoc" id="payDoc" placeholder="123456789">';
        txtHTML += '</div>';
        txtHTML += '<h2 class="subtitle_1 border_btn">Datos de Tarjeta de Crédito</h2><div class="tag black5 text-light mall20"><b>Importante:</b> Por seguridad las tarjetas de crédito nunca serán almacenadas.</div>';
        txtHTML += '<div class="py_cc_data_cont">';
        txtHTML += '<h4>Número de Tarjeta:</h4>';
        txtHTML += '<input class="msg-box-1-input input_crcd" type="text" name="payCarNum" id="payCarNum" placeholder="****************">';
        txtHTML += '<div class="py_cc_data">';
        txtHTML += '<div><h4>Vencimiento:</h4>';
        txtHTML += '<input class="msg-box-1-input mw80 input_crcd_mm" type="text" name="payCardMonth" id="payCardMonth" placeholder="mm">';
        txtHTML += '<input class="msg-box-1-input mw80 input_crcd_yyyy" type="text" name="payCardYear" id="payCardYear" placeholder="yyyy">';
        txtHTML += '</div>';
        txtHTML += '<div><h4>CSV:</h4><input class="msg-box-1-input mw100 input_crcd_csv" type="text" name="payCardCSV" id="payCardCSV" placeholder="123">';
        txtHTML += '</div></div>';
        txtHTML += '<div class="banner_epayco"><img src="../images/epayco_banner.png" alt="epayco banner"></div>';
        txtHTML += '</div><span class="errorMsg" id="errorMsgPaymentInfo"></span><button class="btn_green_full luca_grad_left" id="BtnPaymentInfo">PAGAR</button>';
        txtHTML += '</div></div>';

        $("body").append(txtHTML);
    }

    add_events() {
        let me = this;

        // Close event
        $("#" + this.id_object + " .msgb-box-1-exit-mark").off('click').click(() => {
            (me.close_over) ? me._close_over() : me._close();
        });

        // Assign Key and focusout Events
        $("#" + this.id_object + " .input_crcd").keyup(function (e) { keyup_format_ccard(e, this); });
        $("#" + this.id_object + " .input_crcd_mm").keyup(function (e) { keyup_format_ccard_mm(e, this); });
        $("#" + this.id_object + " .input_crcd_mm").focusout(function () { focusout_format_dd(this); });
        $("#" + this.id_object + " .input_crcd_yyyy").keyup(function (e) { keyup_format_ccard_yy(e, this); });
        $("#" + this.id_object + " .input_crcd_csv").keyup(function (e) { keyup_format_ccard_csv(e, this); });

        // Fill business data
        $("#fill_bus_data_py").off('click').click(() => {
            $.ajax({
                url: "get_session_vars.php",
                dataType: "json",
                data: ({ x: "1" }),
                type: "POST",
                success: function (r) {
                    $("#payEmail").val(r["bus_email"]);
                    $("#payAddress").val(r["bus_address"]);
                    $("#payCity").val(r["bus_city"]);
                    $("#payTel").val(r["bus_phone"]);
                    $("#payName").val(r["bus_name"]);
                    $("#payDoc").val(r["bus_nit"]);
                    $("#payTypDoc").val("NIT");
                    alertify.message("Datos cargados");
                }
            });
        })

        // Process Payment
        $("#BtnPaymentInfo").off('click').click(() => {
            if (me.is_set && me.pkg_id != -1 && me.susc_id != '') {
                let x1A = CheckInputText("payName", "errorMsgPaymentInfo", "Nombre del titular", true, 0, 50, false, true, true);
                if (!x1A) { return false };
                let x2A = CheckInputText("payEmail", "errorMsgPaymentInfo", "email", true, 0, 50, false, false, false, "email");
                if (!x2A) { return false };
                let x3A = CheckInputText("payCity", "errorMsgPaymentInfo", "Ciudad", true, 0, 50, false, true, true);
                if (!x3A) { return false };
                let x4A = CheckInputText("payAddress", "errorMsgPaymentInfo", "Dirección", true, 0, 50, false, false, false);
                if (!x4A) { return false };
                let x5A = CheckInputText("payTel", "errorMsgPaymentInfo", "Télefono", true, 0, 15, false, false, false, "phone");
                if (!x5A) { return false };

                let docType = $("#payTypDoc").val();
                let x10A = !(docType == "" || docType == null);
                if (!x10A) {
                    ErrorMsgModalBox('open', "errorMsgPaymentInfo", "Revisa el tipo de documento.");
                    ErrorInputBox("payTypDoc");
                    return false;
                };

                let x11A = CheckInputText("payDoc", "errorMsgPaymentInfo", "Número de Identificación", true, 0, 30, true, false, true);
                if (!x11A) { return false };

                let cc_num = $("#payCarNum").val().replace(/[^0-9]/g, "").trim();
                let x6A = cc_num.length >= 15 && cc_num.length <= 16;
                if (!x6A) {
                    ErrorMsgModalBox('open', "errorMsgPaymentInfo", "Revisa el número de tarjeta de crédito.");
                    ErrorInputBox("payCarNum");
                    return false;
                };

                let cc_mth = $("#payCardMonth").val().replace(/[^0-9]/g, "").trim();
                cc_mth = (cc_mth.length == 1) ? "0" + cc_mth : cc_mth;
                let x7A = cc_mth.length > 0 && cc_mth.length <= 2;
                let x7B = cc_mth > 0 && cc_mth <= 12;
                if (!x7A || !x7B) {
                    ErrorMsgModalBox('open', "errorMsgPaymentInfo", "Revisa el mes de la tarjeta de crédito.");
                    ErrorInputBox("payCardMonth");
                    return false;
                };

                let cc_year = $("#payCardYear").val().replace(/[^0-9]/g, "").trim();
                let x8A = cc_year.length == 4;
                let curDate = parseInt(moment().year().toString());
                let x8B = x8A >= curDate;
                //if(!x8A || !x8B){
                if (!x8A) {
                    ErrorMsgModalBox('open', "errorMsgPaymentInfo", "Revisa el año de la tarjeta de crédito.");
                    ErrorInputBox("payCardYear");
                    return false;
                };

                let cc_csv = $("#payCardCSV").val().replace(/[^0-9]/g, "").trim();
                let x9A = cc_csv.length == 3;
                if (!x9A) {
                    ErrorMsgModalBox('open', "errorMsgPaymentInfo", "Revisa el CSV de la tarjeta de crédito.");
                    ErrorInputBox("payCardCSV");
                    return false;
                };

                if (x1A * x2A * x3A * x4A * x5A * x6A * x7A * x7B * x8A * x9A * x10A * x11A) {
                    let mr = new MainRoller(30000);
                    mr.startMainRoller();

                    let payName = $("#payName").val().toString().trim();
                    let payCity = $("#payCity").val().toString().trim();
                    let payAddress = $("#payAddress").val().toString().trim();
                    let payTel = $("#payTel").val().toString().trim().replace(/[^0-9]/g, "");
                    let payEmail = $("#payEmail").val().toString().trim().toLowerCase();
                    let payDoc = $("#payDoc").val().toString().trim();

                    $.ajax({
                        url: 'epay_suscribe.php',
                        dataType: 'json',
                        data: ({
                            card_numer: cc_num,
                            card_year: cc_year,
                            card_month: cc_mth,
                            card_cvc: cc_csv,
                            cust_name: payName,
                            cust_email: payEmail,
                            cust_city: payCity,
                            cust_address: payAddress,
                            cust_phone: payTel,
                            susc_id: me.susc_id,
                            pkg_id: me.pkg_id,
                            doc_type: docType,
                            doc_number: payDoc
                        }),
                        type: 'POST',
                        success: function (r) {
                            mr.stopMainRoller();
                            if (r[0][0] == "E") {
                                me._send_error_msg(r[0][1].replace(/[^A-Za-záéíóúÁÉÍÓÚñÑ\.\,\d ]/gi, "").trim());
                            } else if (r[0][0] == "S") {
                                me.fun_success();
                                alertify.alert("!Bienvenido al módulo de " + me.pkg_name + "!", "Desde ahora puedes usar todas las funciones del módulo. Te hemos dado 7 días gratis para que disfrutes del plan.");
                                me._close();

                                // Activate Module
                                toggle_pkg(me.pkg_id, "active");
                                // Clean TDC
                                $("#payCardCSV").val('');
                                $("#payCardYear").val('');
                                $("#payCardMonth").val('');
                                $("#payCarNum").val('');
                            } else {
                                me._send_error_msg("Ocurrió un error generando la suscripción");
                            }
                        },
                        error: function () {
                            mr.stopMainRoller();
                            me._send_error_msg("Ocurrió un error generando la suscripción");
                        }
                    });
                    return true;
                } else {
                    me._send_error_msg("Error validando la información.");
                    return false;
                }
            }
        });
    }

    _assign_product(id_product) {
        // id_product -> 1, 2, 3, 4, ...
        if (id_product == 1) {
            $("#pck_name").text("Paquete Analítica");
            $("#pck_cost").text("Suscripción: $ 29,900 COP/mes");
            $("#pck_img").attr("src", "https://www.2luca.co/icons/illustrations/mp_analytics2.svg");
            this.is_set = true;
            this.pkg_id = 1;
            this.susc_id = 'Analitica_30K';
            this.pkg_name = 'Analítica';
            this.fun_success = () => { CloseMpAnalytics(); };
        } else if (id_product == 2) {
            $("#pck_name").text("Paquete Chatbot");
            $("#pck_cost").text("Suscripción: $ 19,900 COP/mes");
            $("#pck_img").attr("src", "https://www.2luca.co/icons/SVG/82-school-science/science-robot-1.svg");
            this.is_set = true;
            this.pkg_id = 2;
            this.susc_id = 'bot_20K';
            this.pkg_name = 'Chatbot';
            this.fun_success = () => { CloseMpChatbot(); };
        } else if (id_product == 3) {
            $("#pck_name").text("Paquete Localizador");
            $("#pck_cost").text("Suscripción: $ 29,900 COP/mes");
            $("#pck_img").attr("src", "https://www.2luca.co/icons/SVG/21-share/signal-person.svg");
            this.is_set = true;
            this.pkg_id = 3;
            this.susc_id = 'ordersAlerts_30K'; //ordersAlerts_30K
            this.pkg_name = 'Localizador';
            this.fun_success = () => { CloseMpTracker(); };
        } else {
            $("#pck_name").text("-");
            $("#pck_cost").text("-");
            $("#pck_img").attr("src", "");
            this.is_set = false;
            this.pkg_id = -1;
            this.susc_id = '';
            this.pkg_name = '';
            this.fun_success = () => { };
        }
    }

    _send_error_msg(msg) {
        ErrorMsgModalBox('open', 'errorMsgPaymentInfo', msg)
    }

    _close_over() {
        $("#msg-box-PaymentInfo").css({ "display": "none" });
        $("#msg-box-container-PaymentInfo").css({ "display": "none" });
    }

    _close() {
        $('body').removeClass('noscroll');
        $("#msg-box-PaymentInfo").css({ "display": "none" });
        $("#msg-box-container-PaymentInfo").css({ "display": "none" });
    }

    _open() {
        CloseMenu();
        $('body').addClass('noscroll');
        $("#msg-box-PaymentInfo").css({ "display": "block" });
        $("#msg-box-container-PaymentInfo").css({ "display": "block" });
    }
}

/*---------------------------------------------------------------*/

class itemsBillsModal {
    /**
     * Open modal that display a list of all the items inside a specific bill.
     *  
     * @param {string} idBill Id of bill to be display in the modal.
     * @param {string} id_target Id of the div element in body where want to be placed the modal. 
     * @param {boolean} close_over True if want to open the modal on top of other modal.
     * @param {title} title Frontend title of the modal.
     * 
     * @example
     * new itemsBillsModal("AD-12", "item-bills-container", true)
     */
    constructor(idBill, id_target, close_over = true, title = 'ÍTEMS') {
        this.idBill = idBill;
        this.close_over = close_over;
        this.id_target = id_target;
        this.title = title;

        this.build();
        this.add_interactions()
        this.populate();
    }

    build() {
        $('#' + this.id_target).html('');
        let textHTML = '';
        textHTML += '<div class="msg-box-container" id="msg-box-container-itemsbill"><div class="msg-box-1" id="msg-box-itemsbill">';
        textHTML += '<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark-itemsbill">X</p>';
        textHTML += '<p class="msgb-box-1-title" id="msgb-box-1-title"> ' + this.title + '</p>';
        textHTML += '<center><div class="items_bill_cont"><div id="items_bill_wrap" class="items_bill_wrap"></div></div></center>';
        textHTML += '</div></div>';
        $('#' + this.id_target).html(textHTML);
    }

    add_interactions() {
        // Close behavour
        (this.close_over) ? $('#msgb-box-1-exit-mark-itemsbill').off().click(() => { CloseItemsBill_over() }) : $('#msgb-box-1-exit-mark-itemsbill').off().click(() => { CloseItemsBill() });
    }


    populate() {
        let mr = new MainRoller();
        mr.startMainRoller();
        $.ajax({
            url: "get_bill.php",
            dataType: "json",
            data: ({ idBill: this.idBill }),
            type: "POST",
            success: function (r) {
                mr.stopMainRoller();
                $("#items_bill_wrap").html('');
                var htxt = '';

                if (r.length > 0) {
                    for (var i = 0; i < r.length; i++) {
                        htxt += '<div id="ib_' + r[i]["item_id"] + '" class="item_bill_row">';
                        htxt += '<div class="item_bill_icon"><img src="' + r[i]["prod_icon"] + '"></div>';
                        htxt += '<div class="item_bill_data">';

                        htxt += '<p id="ib_id" class="ib_id">' + r[i]["item_id"] + '</p>';
                        htxt += '<p id="ib_name" class="ib_name">' + r[i]["prod_name"] + '</p>';
                        htxt += '<div class="ibd_c1"><div class="ibd_c11"><span id="ib_cnt">' + r[i]["item_count"] + '</span><span id="ib_unt"> ' + r[i]["prod_units"] + '</span></div><div class="ibd_c12">$ <span id="ib_price">' + numCommas(numRound(r[i]["item_value"], 0)) + '</span></div>';
                        if (r[i]["item_discount"] != '0' && r[i]["item_discount"] != '') {
                            htxt += '<div class="ibd_c13" title="$ ' + numCommas(numRound(r[i]["item_discount"], 0)) + '"><span class="mdi mdi-brightness-percent"></span></div>';
                        }
                        if (r[i]["item_note"] != '') {
                            htxt += '<div class="ibd_c13" title="' + r[i]["item_note"] + '"><span class="mdi mdi-bookmark-outline"></span></div>';
                        }
                        htxt += '</div></div></div>';
                    }
                    $("#items_bill_wrap").append(htxt);
                }
                OpenItemsBill();
            }
        });
    }
}

/** 
 * Class that creates an object in HTML with the modal of New Customer.
 *  
 * @param {function} on_success Function that will be trigger once the customer is created correcly.
 * 
 * @example
 * let on_success = () => { RefreshCustomers() };
 * let newConstObj = new newCustomerObj(on_success);
 * */
class newCustomerObj {

    constructor(on_success = function () { }) {
        this.build();
        this.assignActions();
        this.on_success = on_success;
    }

    build() {
        let htmlTxt = '';
        htmlTxt += '<div class="msg-box-container" id="msg-box-container-newcustomer"><div class="msg-box-1" id="msg-box-newcustomer"><p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseNewCustomer()">X</p>';
        htmlTxt += '<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-account-plus-outline"></span> NUEVO CLIENTE</p>';
        htmlTxt += '<form action="" method="POST" name="new-customer-form" id="new-customer-form">';
        htmlTxt += '<input type="reset" value="Limpiar todo"/>';
        htmlTxt += '<h4>Nombre:</h4><input type="text" name="ncust_name" id="ncust_name" placeholder="Ej: Juan Pérez" required maxlength="60">';
        htmlTxt += '<h4>Identificación:</h4><input type="text" name="ncust_id" id="ncust_id" placeholder="Ej: 98765432" maxlength="30" required>';
        htmlTxt += '<h4>Tipo Identificación:</h4><select name="ncust_type_id" id="ncust_type_id" required> ';
        htmlTxt += '<option value="1" selected>Cédula Ciudadania</option><option value="2">Cédula Extranjería</option><option value="3">Pasaporte</option><option value="4">Tarjeta de Identidad</option><option value="5">Registro Civil</option><option value="6">Otro</option></select>';
        htmlTxt += '<h4>e-mail:</h4><input type="email" name="ncust_email" id="ncust_email" placeholder="Ej: juanperez@micorreo.com" maxlength="50">';
        htmlTxt += '<h4>Celular/Teléfono:</h4><input type="tel" name="ncust_phone" id="ncust_phone" class="input_phone" placeholder="Ej: (321) 123 4567" maxlength="14">';
        htmlTxt += '<h4>Fecha Nacimiento (aaaa-mm-dd):</h4><input type="date" class="msg-box-date" name="ncust_bdate" id="ncust_bdate" placeholder="Ej: 1990-02-11" min="1910-01-01" max="2100-12-31" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">';
        htmlTxt += '<h4>Genero:</h4><div id="container_gender">';
        htmlTxt += '<label class="container_radiobtn" for="genderMale"><img src="../avatars/manblackjacket.svg"><input type="radio" id="genderMale" name="gender" value="1"><span class="radiobtnstl"></span></label>';
        htmlTxt += '<label class="container_radiobtn" for="genderFemale"><img src="../avatars/girlearingsgreen.svg"><input type="radio" id="genderFemale" name="gender" value="2"><span class="radiobtnstl"></span></label></div>';
        htmlTxt += '<span class="simpleMsg" id="simpleMsgNewCustomer"></span><span class="errorMsg" id="errorMsgNewCustomer"></span><input type="submit" name="newcustomer-submit" id="newcustomer-submit" value="CREAR CLIENTE">';
        htmlTxt += '</form></div></div>';
        $("body").append(htmlTxt);
    }

    assignActions() {

        let me = this;

        $("#new-customer-form").submit(function () {
            let ncust_name = $("#ncust_name").val();
            let ncust_email = $("#ncust_email").val().toLowerCase();
            let ncust_phone = $("#ncust_phone").val();
            let ncust_bdate = $("#ncust_bdate").val();
            let gender = $('input[name=gender]:checked').val();
            let ncust_id = $("#ncust_id").val();
            let ncust_type_id = $("#ncust_type_id").val();

            if (ncust_name == '') {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Debes ingresar el nombre');
                ErrorInputBox('ncust_name');
                return false;
            }
            if (ncust_name.length > 50) {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El nombre no puede tener más de 50 caracteres');
                ErrorInputBox('ncust_name');
                return false;
            }
            let validName = /[0-9!"#$%&/()=?¡'¿¨*´+><._]/g;
            if (validName.test(ncust_name)) {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El nombre del cliente no puede contener números ni caracteres especiales');
                ErrorInputBox('ncust_name');
                return false;
            }
            if (ncust_id == '') {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Debes ingresar el número de identificación');
                ErrorInputBox('ncust_id');
                return false;
            }
            if (ncust_id.length > 30) {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El número de identificación no puede tener más de 30 caracteres');
                ErrorInputBox('ncust_id');
                return false;
            }
            if (ncust_id != '' && ncust_type_id == '1' && isNaN(ncust_id) == true) {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'La cédula de cuidadania debe contener únicamente valores numéricos y no puede contener espacios');
                ErrorInputBox('ncust_id');
                return false;
            }
            if (ncust_bdate != '') {
                let typeDate1 = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(ncust_bdate);
                let splitDate = ncust_bdate.split("-");

                if (splitDate.length > 1 && typeDate1 == true) {
                    if (splitDate[1] > 12 || splitDate[1] <= 0 || splitDate[2] > 31 || splitDate[2] <= 0 || splitDate[0] < 1910) {
                        ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Fecha de nacimiento incorrecta');
                        ErrorInputBox('ncust_bdate');
                        return false;
                    }
                    let bdayDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                    let today = new Date();

                    if (bdayDate >= today) {
                        ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'La fecha de nacimiento debe ser menor que la fecha actual');
                        ErrorInputBox('ncust_bdate');
                        return false;
                    } else if (isNaN(bdayDate)) {
                        ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Fecha de nacimiento incorrecta');
                        ErrorInputBox('ncust_bdate');
                        return false;
                    }
                }
            }
            if (gender == undefined) {
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Ingresa el genero del cliente');
                return false;
            }

            $.ajax({
                url: 'push_new_customer.php',
                dataType: "json",
                data: ({
                    ncust_name: ncust_name,
                    ncust_email: ncust_email,
                    ncust_phone: ncust_phone,
                    ncust_bdate: ncust_bdate,
                    gender: gender,
                    ncust_type_id: ncust_type_id,
                    ncust_id: ncust_id
                }),
                type: "POST",
                success: function (r) {
                    let SimpleName = nameHandler(ncust_name)[1]["shortFullName"];
                    let firstName = ncust_name.split(" ")[0];
                    if (SimpleName == '') {
                        SimpleName = firstName;
                    }
                    if (r[0][0] == 'E') {
                        if (r[0][1] == '1') {
                            SimpleMsgModalBox('open', 'simpleMsgNewCustomer', 'Completa todos los campos');
                            return false;
                        } else if (r[0][1] == '2') {
                            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Se produjo un error ingresando el cliente');
                        } else {
                            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Se produjo un error ingresando el cliente');
                        }
                    } else if (r[0][0] == 'F') {
                        if (r[0][1] == '1') {
                            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El cliente con ID: ' + r[1]["cust_id"] + ' ya existe con el nombre de ' + r[1]["cust_name"] + '.');
                        }
                    } else if (r[0][0] == 'S') {
                        CloseNewCustomer();
                        $('#new-customer-form').trigger("reset");
                        alertify.message("Cliente ingresado correctamente");
                        me.on_success();
                    }
                },
                error: function (request, status, error) {
                    console.log(error);
                    ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'No fue posible registrar al cliente. Por favor, intenta de nuevo.');
                }
            });
            return false;
        });


        $("#ncust_name").off().focusout(function () {
            let valeName = $("#ncust_name").val();
            if (valeName != '') {
                let GenderAprox = nameHandler(valeName)[1]["GenderAprox"];
                if (GenderAprox == 'Hombre') {
                    $("#genderMale").prop("checked", true);
                    $("#genderFemale").prop("checked", false);
                } else if (GenderAprox == 'Mujer') {
                    $("#genderMale").prop("checked", false);
                    $("#genderFemale").prop("checked", true);
                }

            }
        });
    }
}


/**
 * Class that creates the object in HTML with the modal of Edit Customer. 
 * 
 * @param {function} on_success Function that will be trigger once the customer is edited correcly.
 * 
 * @example
 * let on_success = () => { myFunction() };
 * let editConstObj = new editCustomerObj(on_success);
 * // Assign the submit action to the form with the customer key
 * editConstObj.assignActions('1231214124');
 * // Initialize and open the modal form with the data prepopulated for the specific cust key.
 * editConstObj.iniciateEditCustomer('1231214124') 
 * */
class editCustomerObj {

    constructor(on_success = function () { }) {
        this.build();
        this.assignActions();
        this.on_success = on_success;
    }

    build() {
        let htmlTxt = '';
        htmlTxt += '<div class="msg-box-container" id="msg-box-container-editcustomer"><div class="msg-box-1" id="msg-box-editcustomer"><p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditCustomer()">X</p>    ';
        htmlTxt += '<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-account-plus-outline"></span> EDITAR CLIENTE</p>  ';
        htmlTxt += '<form name="edit-customer-form" id="edit-customer-form"><input type="reset" value="Limpiar todo"/>';
        htmlTxt += '<h4>Nombre:</h4><input type="text" name="ed_cust_name" id="ed_cust_name" placeholder="Ej: Juan Pérez" required maxlength="60">';
        htmlTxt += '<h4>Identificación:</h4><input type="text" name="ed_cust_id" id="ed_cust_id" placeholder="Ej: 98765432" maxlength="30" required>';
        htmlTxt += '<input type="text" name="ed_cust_id_old" id="ed_cust_id_old" hidden>';
        htmlTxt += '<h4>Tipo Identificación:</h4><select name="ed_cust_type_id" id="ed_cust_type_id" required> <option value="1" selected>Cédula Ciudadania</option><option value="2">Cédula Extranjería</option><option value="3">Pasaporte</option><option value="4">Tarjeta de Identidad</option><option value="5">Registro Civil</option><option value="6">Otro</option></select>';
        htmlTxt += '<input type="text" name="ed_cust_type_id_old" id="ed_cust_type_id_old" hidden>';
        htmlTxt += '<h4>e-mail:</h4><input type="email" name="ed_cust_email" id="ed_cust_email" placeholder="Ej: juanperez@micorreo.com" maxlength="50">';
        htmlTxt += '<h4>Celular/Teléfono:</h4><input type="tel" name="ed_cust_phone" id="ed_cust_phone" placeholder="Ej: (321) 123 4567" maxlength="14">';
        htmlTxt += '<h4>Fecha Nacimiento (aaaa-mm-dd):</h4><input type="date" class="msg-box-date" name="ed_cust_bdate" id="ed_cust_bdate" placeholder="Ej: 1990-02-11" min="1910-01-01" max="2100-12-31" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">';
        htmlTxt += '<h4>Genero:</h4><div id="ed_container_gender">';
        htmlTxt += '<label class="container_radiobtn" for="ed_genderMale"><img src="../avatars/manblackjacket.svg"><input type="radio" id="ed_genderMale" name="ed_gender" value="1"><span class="radiobtnstl"></span></label>';
        htmlTxt += '<label class="container_radiobtn" for="ed_genderFemale"><img src="../avatars/girlearingsgreen.svg"><input type="radio" id="ed_genderFemale" name="ed_gender" value="2"><span class="radiobtnstl"></span></label></div>';
        htmlTxt += '<span class="simpleMsg" id="simpleMsgEditCustomer"></span><span class="errorMsg" id="errorMsgEditCustomer"></span>';
        htmlTxt += '<input type="submit" name="editcustomer-submit" id="editcustomer-submit" value="EDITAR CLIENTE">	';
        htmlTxt += '</form></div></div>';
        $("body").append(htmlTxt);
    }

    setOnSuccess(fun) {
        this.on_success = fun;
    }

    assignActions() {
        $("#ed_cust_name").off().focusout(function () {
            let valeName = $("#ed_cust_name").val();
            if (valeName != '') {
                let GenderAprox = nameHandler(valeName)[1]["GenderAprox"];
                if (GenderAprox == 'Hombre') {
                    $("#ed_genderMale").prop("checked", true);
                    $("#ed_genderFemale").prop("checked", false);
                } else if (GenderAprox == 'Mujer') {
                    $("#ed_genderMale").prop("checked", false);
                    $("#ed_genderFemale").prop("checked", true);
                }

            }
        });
    }

    assignSubmit(cust_key) {
        let me = this;

        $("#edit-customer-form").off().submit(function () {
            let ncust_name = $("#ed_cust_name").val();
            let ncust_email = $("#ed_cust_email").val().toLowerCase();
            let ncust_phone = $("#ed_cust_phone").val();
            let ncust_bdate = $("#ed_cust_bdate").val();
            let gender = $('input[name=ed_gender]:checked').val()
            let ncust_type_id = $("#ed_cust_type_id").val();
            let ncust_id = $("#ed_cust_id").val();
            let ncust_id_old = $("#ed_cust_id_old").val();
            let ncust_type_id_old = $("#ed_cust_type_id_old").val();

            if (ncust_name == '') {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Debes ingresar el nombre');
                ErrorInputBox('ed_cust_name');
                return false;
            }
            if (ncust_name.length > 50) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El nombre no puede tener más de 50 caracteres');
                ErrorInputBox('ed_cust_name');
                return false;
            }
            if (ncust_id == '') {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Debes ingresar el número de identificación');
                ErrorInputBox('ed_cust_id');
                return false;
            }
            if (ncust_id.length > 30) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El número de identificación no puede tener más de 30 caracteres');
                ErrorInputBox('ed_cust_id');
                return false;
            }
            let validName = /[0-9!"#$%&/()=?¡'¿¨*´+><._]/g;
            if (validName.test(ncust_name)) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El nombre del cliente no puede contener números ni caracteres especiales');
                ErrorInputBox('ed_cust_name');
                return false;
            }
            if (ncust_id != '' && ncust_type_id == '1' && isNaN(ncust_id) == true) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'La cédula de cuidadania debe contener únicamente valores numéricos y no contener espacios');
                ErrorInputBox('ed_cust_id');
                return false;
            }
            if (ncust_bdate != '') {
                let typeDate1 = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(ncust_bdate);
                let splitDate = ncust_bdate.split("-");

                if (splitDate.length > 1 && typeDate1 == true) {
                    if (splitDate[1] > 12 || splitDate[1] <= 0 || splitDate[2] > 31 || splitDate[2] <= 0 || splitDate[0] < 1910) {
                        ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Fecha de nacimiento incorrecta');
                        ErrorInputBox('ed_cust_bdate');
                        return false;
                    }
                    let bdayDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                    let today = new Date();

                    if (bdayDate >= today) {
                        ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'La fecha de nacimiento debe ser menor que la fecha actual');
                        ErrorInputBox('ed_cust_bdate');
                        return false;
                    } else if (isNaN(bdayDate)) {
                        ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Fecha de nacimiento incorrecta');
                        ErrorInputBox('ed_cust_bdate');
                        return false;
                    }
                }
            }
            if (ncust_phone != '' && ncust_phone.length > 10) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El número de teléfono debe tener máximo 10 dígitos.');
                ErrorInputBox('ed_cust_phone');
                return false;
            }

            if (gender == undefined) {
                ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Ingresa el genero del cliente');
                return false;
            }

            $.ajax({
                url: "edit_customer.php",
                dataType: "json",
                data: ({
                    ncust_name: ncust_name,
                    ncust_email: ncust_email,
                    ncust_phone: ncust_phone,
                    ncust_bdate: ncust_bdate,
                    gender: gender,
                    ncust_type_id: ncust_type_id,
                    ncust_id: ncust_id,
                    ncust_id_old: ncust_id_old,
                    ncust_type_id_old: ncust_type_id_old,
                    cust_key: cust_key
                }),
                type: "POST",
                success: function (r) {
                    if (r.length > 0) {
                        if (r[0][0] == 'E') {
                            ErrorMsgModalBox('open', 'errorMsgEditCustomer', "Se presentó un error (ERR:98423)");
                            return false;
                        } else if (r[0][0] == 'F') {
                            if (r[0][1] == 1) {
                                ErrorMsgModalBox('open', 'errorMsgEditCustomer', "El cliente con el ID: " + r[1]["cust_id"] + " ya existe con el nombre de " + r[1]["cust_name"]);
                                ErrorInputBox('ed_cust_id');
                                return false;
                            }
                        } else if (r[0][0] == 'S') {
                            alertify.message("Cliente actualizado correctamente");
                            CloseEditCustomer();
                            me.on_success();
                        }
                    } else {
                        ErrorMsgModalBox('open', 'errorMsgEditCustomer', "Se presentó un error (ERR:93856)");
                        return false;
                    }
                }
            });
            return false;
        });
    }

    iniciateEditCustomer(cust_key) {

        $("#ed_cust_name").val('');
        $("#ed_cust_email").val('');
        $("#ed_cust_phone").val('');
        $("#ed_cust_bdate").val('')
        $("#genderMale").prop("checked", false);
        $("#genderFemale").prop("checked", false);
        $("#ed_cust_id_old").val('');
        $("#ed_cust_id").val('');
        $("#ed_cust_type_id").val('1');
        $("#ed_cust_type_id_old").val('1');

        $.ajax({
            url: "get_cust_info.php",
            dataType: "json",
            data: ({ cust_key: cust_key }),
            type: "POST",
            success: function (r) {
                if (r.length > 0) {
                    let cust_name = r[0]['cust_name'];
                    let cust_email = r[0]['cust_email'];
                    let cust_phone = r[0]['cust_phone'];
                    let cust_bdate = r[0]['cust_bdate'];
                    let cust_gender = r[0]['cust_gender'];
                    let cust_id = r[0]['cust_id'];
                    let cust_type_id = r[0]['cust_type_id'];

                    if (cust_name != '') { $("#ed_cust_name").val(cust_name) };
                    if (cust_email != '') { $("#ed_cust_email").val(cust_email); }
                    if (cust_id != '') { $("#ed_cust_id").val(cust_id); }
                    if (cust_id != '') { $("#ed_cust_id_old").val(cust_id); }
                    if (cust_type_id != '' && cust_type_id != '0') { $("#ed_cust_type_id").val(cust_type_id); }
                    if (cust_type_id != '' && cust_type_id != '0') { $("#ed_cust_type_id_old").val(cust_type_id); }
                    if (cust_phone != '' && cust_phone != '0') { $("#ed_cust_phone").val(cust_phone); }
                    if (cust_bdate != '' && cust_bdate != '1900-01-01') { $("#ed_cust_bdate").val(cust_bdate) }
                    if (cust_gender == '1') { $("#ed_genderMale").prop("checked", true); }
                    if (cust_gender == '2') { $("#ed_genderFemale").prop("checked", true); }

                    OpenEditCustomer();
                }
            }
        });
    }
}



(function () {

    /** CHECK IF THERE IS INTERNET CONNECTION */
    class checkConnection {
        constructor() {
            this.create();
        }

        create() {
            let objHTML = '<div id="OffLineMsg" class="OffLineMsg"><div></div><p>Sin conexión</p><span class="mdi mdi-signal-off"></span></div>';
            $('body').append(objHTML);
        }

        connectionExist() {
            if (!navigator.onLine) {
                $("#OffLineMsg").addClass("OffLineMsgOpen");
                if (msgOfflineAlert2 == false && window.location.pathname == "/php/sales.php") {
                    msgOfflineAlert2 = true;
                    msgOfflineAlert = false;
                    $("#offline_noty").show();
                    // push_all_customersOffline();
                    this.OfflineSetUp('offline');
                }

            } else {
                $("#OffLineMsg").removeClass("OffLineMsgOpen");
                if (msgOfflineAlert == false && window.location.pathname == "/php/sales.php") {
                    msgOfflineAlert = true;
                    msgOfflineAlert2 = false;
                    // push_all_customers();
                    this.OfflineSetUp('online');
                    let offbills = GetOfflineBills();
                    if (offbills.length == 0) {
                        $("#offline_noty").hide();
                    } else {
                        $("#offline_noty").show();
                        $("#num_off_bills").text(offbills.length);
                        let tx1 = (offbills.length == 1) ? "cuenta pendiente" : "cuentas pendientes";
                        alertify.alert("Cuentas Offline", "Tienes " + offbills.length + " " + tx1 + " por enviar. Las cuentas permanecerán en estado pendiente y no harán parte de tu historial de ventas hasta que decidas enviarlas.");
                    }
                }
            }
        }

        start(timer = 7000) {
            let me = this;
            let intervalConnection = setInterval(function () { me.connectionExist(); }, timer);
        }

        /**** Setup the environment depending the state of internet connection ***/
        OfflineSetUp(status) {
            if (status == 'offline') {
                $("#search_consumer").addClass("disabled");
                $("#cb_icon_new_cons").addClass("disabled");
                $("#refrAllItems").addClass("disabled");
                $("#opNewItem").addClass("disabled");
                $("#selectItmsImportCSV").addClass("disabled");
                $("#selectItmsDel").addClass("disabled");
                $("#selectItmsExportCSV").addClass("disabled");
                $("#optionItemEdit").addClass("disabled");
                $("#optionItemStats").addClass("disabled");
                $("#qrCodeGenerate").addClass("disabled");
                $("#optionItemDelete").addClass("disabled");
                $("#BtnOpenCreateProfile").addClass("disabled");
                $("#sendAllOffBills").addClass("disabled");
                $("#opVoiceCom").addClass("disabled");
                $(".chatBotContainer").addClass("disabled");
            } else {
                $("#search_consumer").removeClass("disabled");
                $("#cb_icon_new_cons").removeClass("disabled");
                $("#refrAllItems").removeClass("disabled");
                $("#opNewItem").removeClass("disabled");
                $("#selectItmsImportCSV").removeClass("disabled");
                $("#selectItmsDel").removeClass("disabled");
                $("#selectItmsExportCSV").removeClass("disabled");
                $("#optionItemEdit").removeClass("disabled");
                $("#optionItemStats").removeClass("disabled");
                $("#qrCodeGenerate").removeClass("disabled");
                $("#optionItemDelete").removeClass("disabled");
                $("#BtnOpenCreateProfile").removeClass("disabled");
                $("#sendAllOffBills").removeClass("disabled");
                $("#opVoiceCom").removeClass("disabled");
                $(".chatBotContainer").removeClass("disabled");

            }
        }

    }

    let connCheck = new checkConnection();
    connCheck.start();

}());

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::::::::::::::::::::::: OFFLINE :::::::::::::::::::::::::::*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/


/*** Get all current Offline Bills ****/
let GetOfflineBills = function () {
    let ls_keys = Object.keys(localStorage);
    let bills_off_str = '[';
    let c = 0;
    let curr_acct_key = GetVariableCookie().CURRACCKEY;
    for (let i = 0; i < ls_keys.length; i++) {
        let spl_key = ls_keys[i].split("-") || "";
        if (spl_key.length === 3) {
            if (spl_key[0] == "tempBill" && spl_key[2] == curr_acct_key) {
                c = c + 1;
                bills_off_str += '{"id":' + parseInt(spl_key[1]) + ',"value":' + localStorage.getItem(ls_keys[i]) + '},';
            }
        }
    }
    if (c == 0) {
        return [];
    } else {
        bills_off_str = bills_off_str.substring(0, bills_off_str.length - 1);
        bills_off_str += "]";
        return JSON.parse(bills_off_str);
    }
}

/*** Get Offline Bill by ID ****/
let GetOfflineBillById = function (idItem) {
    let x = GetOfflineBills();
    let res = {};
    x.forEach((r) => { if (r.id == idItem) { res = r; }; })
    return res;
}

/*** Load in Modal all Off Bills ****/
let loadOffBIlls = function () {
    $("#wrap-offline-bills").html('');
    let ob = GetOfflineBills();
    if (ob.length > 0) {
        let html = '';
        for (let i = 0; i < ob.length; i++) {
            html += '<div><div>';
            html += '<div onclick="sendOffBill(' + ob[i].id + ')"><i class="far fa-paper-plane"></i></div>';
            html += '<div onclick="removeOffBill(' + ob[i].id + ')"><i class="far fa-trash-alt"></i></div>';
            html += '</div><div>';
            html += '<span><b>ID ' + ob[i].id + '</b> - ' + ob[i].value.timeLocal + ' - $' + numCommas(ob[i].value.super_total_bill) + '</span>';
            html += '</div></div>';
        }
        $("#wrap-offline-bills").html(html);
    }
}

/***** Send Bill By ID  *****/
let sendOffBill = function (idItem) {
    if (navigator.onLine) {
        let bill = GetOfflineBillById(idItem);
        if (bill.id != undefined) {
            let itemJSON = JSON.stringify(bill.value.ItemsInBill);

            $.ajax({
                type: "POST",
                url: "push_new_bill_bulk.php",
                data: ({
                    trx_value: bill.value.super_total_bill,
                    trx_type: '1',
                    num_items: bill.value.total_items_bill,
                    discount: bill.value.discount_bill,
                    tips: bill.value.tips_bill,
                    taxes: bill.value.tax_bill,
                    id_cupon: '',
                    bill_type: bill.value.bill_print_type,
                    payment_type: bill.value.paymentType,
                    other_payment_type: bill.value.otherPaymentOption,
                    creditcard_compr_number: bill.value.creditCardComprNum,
                    cash_change: bill.value.cash_change,
                    cust_key: bill.value.cust_key,
                    sqlInput: itemJSON,
                    profsessid: bill.value.profsessid,
                    points_redem: bill.value.PointsRedem
                }),
                success: function (r) {
                    if (r == '0') {
                        ClearOfflineBills(bill.id);
                        alertify.message("Cuenta #" + bill.id + " enviada");
                    } else if (r != '0') {
                        alertify.message("Error enviando cuenta #" + bill.id);
                    }
                }
            });
        }
    } else {
        alertify.alert("Procesar Offline", "No estás conectado a internet.");
    }

}

/***** Remove BIll By ID  *****/
let removeOffBill = function (idItem) {
    alertify.confirm("Borrar Cuenta Offline", "¿Seguro deseas borrar la cuenta?", function () {
        ClearOfflineBills(idItem);
        alertify.message("Cuenta eliminada");
    }, function () { });
}

/***** Send Bills to Server *****/
let PushServerBills = function () {

    if (navigator.onLine) {
        let AllOffBills = GetOfflineBills();
        if (AllOffBills.length > 0) {
            AllOffBills.forEach(function (bill) {

                let itemJSON = JSON.stringify(bill.value.ItemsInBill);

                $.ajax({
                    type: "POST",
                    url: "push_new_bill_bulk.php",
                    data: ({
                        trx_value: bill.value.super_total_bill,
                        trx_type: '1',
                        num_items: bill.value.total_items_bill,
                        discount: bill.value.discount_bill,
                        tips: bill.value.tips_bill,
                        taxes: bill.value.tax_bill,
                        id_cupon: '',
                        bill_type: bill.value.bill_print_type,
                        payment_type: bill.value.paymentType,
                        other_payment_type: bill.value.otherPaymentOption,
                        creditcard_compr_number: bill.value.creditCardComprNum,
                        cash_change: bill.value.cash_change,
                        cust_key: bill.value.cust_key,
                        sqlInput: itemJSON,
                        profsessid: bill.value.profsessid,
                        points_redem: bill.value.PointsRedem
                    }),
                    success: function (r) {
                        if (r == '0') {
                            ClearOfflineBills(bill.id);
                            alertify.message("Cuenta #" + bill.id + " enviada");
                        } else if (r != '0') {
                            alertify.message("Error enviando cuenta #" + bill.id);
                        }
                    }
                });

            });
        }
    } else {
        alertify.alert("Procesar Offline", "No estás conectado a internet.");
    }
}

/*** Clear Offline Bills ****/
let ClearOfflineBills = function (idBill) {
    let curr_acct_key = GetVariableCookie().CURRACCKEY;
    if (idBill == undefined) {
        let offbills = GetOfflineBills();
        if (offbills.length > 0) {
            alertify.confirm("Borrar Cuentas", "¿Seguro desea borrar todas las cuenta?", function () {
                for (let i = 0; i < offbills.length; i++) {
                    localStorage.removeItem("tempBill-" + offbills[i].id + "-" + curr_acct_key);
                }
                $("#num_off_bills").text("0");
                $("#offline_noty").hide();
                alertify.message("Cuentas borradas con éxito.");
            }, function () { });
        } else {
            alertify.message("No hay cuentas para borrar");
        }
    } else {
        localStorage.removeItem("tempBill-" + idBill + "-" + curr_acct_key);
        let x = GetOfflineBills();
        $("#num_off_bills").text(x.length);
        loadOffBIlls();
        if (x.length == 0) {
            $("#offline_noty").hide();
            OffLineAdmin.close();
        }
    }
}


/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::::::::::::::::: RECONOCIMIENTO DE VOZ :::::::::::::::::*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

/**** Leer lo texto ****/
let readMe = function (message, rate, pitch, volume, voice) {
    message = (message === undefined) ? '' : message;
    rate = (rate === undefined) ? 1 : rate;
    pitch = (pitch === undefined) ? 1 : pitch;
    volume = (volume === undefined) ? 1 : volume;
    voice = (voice === undefined) ? 52 : voice;
    try {
        let spch = new SpeechSynthesisUtterance();
        spch.text = message;
        spch.volume = volume;
        spch.rate = rate;
        spch.pitch = pitch;
        let voices = window.speechSynthesis.getVoices();
        let SpnVoices = voices.filter(function (x) { return x["lang"] == "es-ES" });
        if (voices[voice] !== undefined) {
            spch.voice = voices[voice];
        } else {
            spch.voice = SpnVoices[0];
        }

        window.speechSynthesis.speak(spch);
    } catch (e) {
        console.log(e);
        alertify.alert("Error", e);
        return false;
    }
}

let StartSpeachRecog = function () {

    try {
        let SpeechRecog = webkitSpeechRecognition || SpeechRecognition;
        let Recog = new SpeechRecog();
        Recog.lang = "es-CO";

        Recog.onstart = function () {
            alertify.message("Puedes empezar a hablar");
            if (!isMobileDevice()) { PlaySound("beepsound") }
        }

        Recog.onresult = function (event) {
            if (!isMobileDevice()) { PlaySound("beepsound") };
            let current = event.resultIndex;
            let transcript = event.results[current][0].transcript;
            // console.log(transcript);

            let NPL_VOICE_RECOG;
            try {
                NPL_VOICE_RECOG = nl_Tokenize(transcript);
                NVR = NPL_VOICE_RECOG;
                // console.log(NPL_VOICE_RECOG);
            } catch (e) {
                console.log("No fue posible cargar NLP");
            }

            let trsc = transcript.trim().toLowerCase();
            trsc = nl_CleanStopWords(trsc);

            if (trsc != "") {

                let trTokens = trsc.split(" ");
                let trTokensOrig = transcript.toLowerCase().split(" ");

                /*
                 * COMANDOS:
                 *
                 * Anadir XXXX -> Nombre de producto o id de producto
                 * Nuevo Cliente -> Abre ventana de nuevo cliente
                 * Nuevo Cliente XXXX -> Abre ventana de nuevo cliente precargando el nombre XXXXX
                 * Nuevo Ítem -> Abre ventana de nuevo producto
                 * Nuevo Ítem XXXX -> Abre ventana de nuevo ítem precargando el nombre XXXXX
                 * Limpiar -> Limpiar la cuenta actualVentas 
                 * Reporte | ventas(vendí) | estadísticas -> Crea reporte para la fecha solicitada. EJ: Crear reporte de hoy, crear reporte del año pasado
                 * Chat -> Intento de chat con el asistente.
                 * 
                 * Actualizar || Actualiza -> Refresca los ítems y la página
                 * Ayuda || Ayúdame -> Ayuda al usuario
                 */

                let cmdsAdd = SearchCommand(trTokens, ["añadir", "añade", "agregar", "agrega", "incluir", "incluye", "adicionar", "adiciona"]);
                let cmdsRemove = SearchCommand(trTokens, ["remover", "suprimir", "quitar", "desagregar", "quita", "remueve", "suprime"]);
                let cmdsReport = SearchCommand(trTokens, ["reporte", "ventas", "vendí", "vendido", "estadísticas"]);
                let cmdsPay = SearchCommand(trTokens, ["pagar", "procesar", "facturar", "pago"]);
                let cmdsHelp = SearchCommand(trTokens, ["ayuda", "ayúdame", "ayudar"]);

                if (trTokens.indexOf("nuevo") > -1) {
                    if (trTokens.indexOf("cliente") > -1) {
                        let tiposDato = NPL_VOICE_RECOG.general.tiposDato;
                        if (tiposDato != undefined) {
                            $("#new-customer-form").trigger("reset");
                            let id = tiposDato.id || '';
                            let nacimiento = tiposDato.nacimiento || tiposDato.cumpleaños || '';
                            let nombre = tiposDato.nombre || NPL_VOICE_RECOG.general.nombres.join(' ').nompropio() || '';
                            let tipoId = tiposDato.tipoId || '';
                            let celular = tiposDato.celular || tiposDato.telefono || '';
                            let genero = tiposDato.genero || '';
                            let email = tiposDato.email || '';
                            $("#ncust_name").val(nombre);
                            $("#ncust_id").val(id);
                            $("#ncust_phone").val(celular);
                            $("#ncust_email").val(email);
                            if (genero == 'masculino' || genero == 'hombre') {
                                $("#genderMale").attr("checked", true);
                                $("#genderFemale").attr("checked", false)
                            } else if (genero == 'femenino' || genero == 'mujer') {
                                $("#genderMale").attr("checked", false);
                                $("#genderFemale").attr("checked", true)
                            }
                            // $("#ncust_bdate").val(name.nompropio())
                        }
                        OpenNewCustomer();
                    } else if (trTokens.indexOf("ítem") > -1) {
                        let name = trTokens.subarray(trTokens.indexOf("ítem") + 1).join(' ');
                        if (name != '') { $("#ni_prod_name").val(name.nompropio()) }
                        OpenNewItem();
                    }
                } else if (cmdsAdd.Founded) {
                    let x = trTokensOrig.subarray(trTokensOrig.indexOf(cmdsAdd.Command) + 1).join(' ');
                    if (x.length > 0) {
                        txt = quitaacentos(x.toLowerCase()).trim();
                        let txtSpl = txt.split(/\by\b|\bcon\b/gm) || [];
                        let result = [];
                        let c = 0;
                        let nameItem = '';
                        txtSpl.forEach(function (testVal) {
                            testVal = testVal.trim();
                            let testVal_id = testVal.replace(/[\s]/g, "");
                            if (isNaN(testVal_id) == false) { testVal = testVal_id }
                            let regex = new RegExp(testVal, "ig");
                            dataProducts.forEach(function (val) {
                                let nombre = quitaacentos(val.prod_name);
                                let ref_code = val.prod_id;
                                if ((nombre.search(regex) != -1) || (ref_code.search(regex) != -1)) {
                                    nameItem = val.prod_name;
                                    result.push(val.prod_id);
                                    addItemtoBillByIdCode(val.prod_id);
                                    alertify.message(val.prod_id + " agregado a la cuenta.");
                                    c = c + 1;
                                }
                            });
                        });
                        if (c == 1) {
                            readMe(nameItem + " añadido correctamente.");
                        } else if (c > 1) {
                            readMe("Se añarieron " + c + " artículos a la cuenta.");
                        } else {
                            readMe("No hay ningún ítem con ese nombre.");
                            alertify.warning("No se encontró: " + x.nompropio())
                        }
                    } else {
                        readMe("Intenta decir " + cmdsAdd.Command + " seguido del nombre o código del ítem.");
                    }
                } else if (cmdsRemove.Founded) {
                    let x = trTokens.subarray(trTokens.indexOf(cmdsRemove.Command) + 1).join(' ');
                    if (x.length > 0) {
                        let xid = x.replace(/[\s]/g, "");
                        if (isNaN(xid) == false) { x = xid }
                        let regex = new RegExp(quitaacentos(x), "ig");
                        let c = 0;
                        let nameItem = '';
                        dataProducts.forEach(function (val) {
                            let nombre = quitaacentos(val.prod_name);
                            let ref_code = val.prod_id;
                            if ((nombre.search(regex) != -1) || (ref_code.search(regex) != -1)) {
                                delete_item_bill(val.prod_id, val.prod_unit_price, val.tax);
                                nameItem = val.prod_name;
                                c = c + 1;
                            }
                        });
                        if (c == 1) {
                            readMe(nameItem + " removido correctamente.");
                        } else if (c > 1) {
                            readMe("Se removieron " + c + " artículos de la cuenta.");
                        } else {
                            readMe("No hay ningún ítem con ese nombre.");
                            alertify.warning("No se encontró: " + x.nompropio())
                        }
                    } else {
                        readMe("Intenta decir " + cmdsRemove.Command + " seguido del nombre o código del ítem.");
                    }
                } else if (cmdsReport.Founded) {
                    let fechasNP = NPL_VOICE_RECOG.general.fechas;
                    if (fechasNP != undefined) {
                        let fechasNP_compu = fechasNP.Compuestas || '';
                        let fechasNP_indir = fechasNP.FechaIndirecta || '';
                        let fechasNP_meses = fechasNP.Meses || '';
                        let fechasNP_diase = fechasNP.DiaSemana || '';
                        let fechasNP_años = fechasNP.Años || '';
                        let IntFecha;
                        if (fechasNP_compu != '') {
                            IntFecha = ExtraeFechaTexto(fechasNP_compu[0]);
                            if (IntFecha.perDate == "Futuro") {
                                readMe('La fecha que solicitaste es una fecha en el futuro.');
                                alertify.message('La fecha que solicitaste es una fecha en el futuro.');
                            } else {
                                if (IntFecha.fullDate != undefined) {
                                    GetDataVR_Report(IntFecha.fullDate, IntFecha.fullDate).then((r) => {
                                        readMe(r.speech);
                                        SpeechReport(r);
                                    });
                                } else {
                                    readMe('No se pudo generar el reporte');
                                    alertify.message('No se pudo generar el reporte');
                                }
                            }
                        } else if (fechasNP_meses != '') {
                            IntFecha = ExtraeFechaTexto(fechasNP_meses[0], true);
                            if (IntFecha.isRange) {
                                GetDataVR_Report(IntFecha.dateFrom, IntFecha.dateTo).then((r) => {
                                    readMe(r.speech);
                                    SpeechReport(r);
                                });
                            }
                        } else if (fechasNP_años != '') {
                            IntFecha = ExtraeFechaTexto(fechasNP_años[0], true);
                            if (IntFecha.isRange) {
                                GetDataVR_Report(IntFecha.dateFrom, IntFecha.dateTo).then((r) => {
                                    readMe(r.speech);
                                    SpeechReport(r);
                                });
                            }
                        } else if (fechasNP_diase != '') {
                            IntFecha = ExtraeFechaTexto(fechasNP_diase[0], true);
                            if (IntFecha.isRange) {
                                GetDataVR_Report(IntFecha.dateFrom, IntFecha.dateTo).then((r) => {
                                    readMe(r.speech);
                                    SpeechReport(r);
                                });
                            }
                        } else if (fechasNP_indir != '') {
                            IntFecha = ExtraeFechaTexto(fechasNP_indir[0]);
                            if (IntFecha.isRange) {
                                GetDataVR_Report(IntFecha.dateFrom, IntFecha.dateTo).then((r) => {
                                    readMe(r.speech);
                                    SpeechReport(r);
                                });
                            } else {
                                GetDataVR_Report(IntFecha.fullDate, IntFecha.fullDate).then((r) => {
                                    readMe(r.speech);
                                    SpeechReport(r);
                                });
                            }
                        } else {
                            readMe('No se encontró una fecha para crear el reporte.');
                            alertify.message('No se encontró una fecha para crear el reporte.');
                        }
                        // console.log(IntFecha);
                    } else {
                        readMe('No se pudo generar el reporte');
                        alertify.message('No se pudo generar el reporte');
                    }
                } else if (cmdsPay.Founded) {
                    checkoutStart();
                } else if (cmdsHelp.Founded) {
                    OpenHelpModule();
                } else if (trTokens.indexOf("limpiar") > -1) {
                    clear_bill();
                } else if (trTokens.indexOf("actualizar") > -1 || trTokens.indexOf("actualiza") > -1) {
                    refresh_items();
                } else {
                    let txtChat = SearchChatVR(transcript);
                    readMe(txtChat);
                }

            }
        }

        Recog.start();
    } catch (e) {
        console.log(e);
        alertify.alert("Error", "Parece que tu dispositivo o navegador no soporta la función de reconocimiento de voz. Intenta desde otro dispositivo.");
        return false;
    }
}

// **** INTENTO DE CHAT 
let SearchChatVR = function (txt) {

    let CleanPuntuation = function (txt) {
        return txt.replace(/[:;?¿\-=()&#!¡]/g, "").replace(/(?=\D)\.(?=\s|$)/g, "").replace(/(?=\D)\,(?=\D|\s)/g, "").trim();
    }
    let RandomIntNumber = function (desde, hasta) {
        return Math.floor(Math.random() * hasta) + desde;
    }

    txt = CleanPuntuation(txt.toLowerCase());

    for (var i = 0; i < ChatVR_QA.length; i++) {
        for (var j = 0; j < ChatVR_QA[i].commands.length; j++) {
            if (txt.indexOf(ChatVR_QA[i].commands[j]) > -1) {
                return ChatVR_QA[i].response[RandomIntNumber(0, ChatVR_QA[i].response.length)];
            }
        }
    }

    return "No logré entender el comando solicitado. Intenta decir ayuda.";
}

let ChatVR_QA = [
    {
        "commands": ["ayuda", "ayúdame", "ayudar"],
        "response": ["Puedes utilizar diferentes comandos para realizar diferentes acciones en Luca. Intenta decir comandos como añadir o remover, seguido por código de identificación del producto para realizar la acción deseada."]
    },
    {
        "commands": ["cómo estás", "cómo te encuentras", "cómo te va"],
        "response": ["Hoy me encuentro espléndida, gracias por preguntar.", "Estoy muy bien.", "Estoy como nunca."]
    },
    {
        "commands": ["para qué sirves", "qué puedes hacer", "cómo funciona", "cómo funcionas", "qué haces"],
        "response": ["Soy tu tu asistente e intentaré facilitarte la vida.", "Soy tu asistente virtual y puedo ayudarte con diferentes operaciones."]
    },
    {
        "commands": ["cómo te llamas", "qué puedes hacer", "quién eres"],
        "response": ["Soy tu asistente de Luca e intentaré facilitarte la vida.", "Soy tu asistente virtual."]
    },
    {
        "commands": ["hola", "buenos días", "buenas tardes", "buenas noches", "estás ahí"],
        "response": ["¡Hola!, un gusto en saludarte.", "Hola, ¿en qué puedo ayudarte?"]
    }
]
// ******* 

let SpeechReport = function (r) {

    OpenSpeechReport();

    $("#sp_rp_numCnt").text(0);
    $("#sp_rp_amt").text(0);

    if (parseInt(r.num_cuentas) > 0) {
        $("#sp_rp_numCnt").text(numCommas(r.num_cuentas));
        $("#sp_rp_amt").text("$ " + numCommas(numRound(r.valor_total, 0)));
    }

}

let GetDataVR_Report = function (dateFrom, dateTo) {
    let bl = new BarLoading(12000, "Generando el reporte");
    bl.start();
    let defer = $.Deferred();
    $.ajax({
        url: 'voice_recog_report.php',
        dataType: 'json',
        data: ({ dateFrom, dateTo }),
        type: 'POST',
        success: function (r) {
            bl.stop();
            let x = {};
            if (r.length > 0) {
                x["num_cuentas"] = r[0].NUM_CUENTAS;
                x["valor_total"] = r[0].VALOR_TOTAL;
                let txt1 = (r[0].NUM_CUENTAS == 1) ? "venta" : "ventas";
                x["speech"] = "Hiciste " + r[0].NUM_CUENTAS + " " + txt1 + " por un monto total de " + r[0].VALOR_TOTAL + " pesos.";
            } else {
                x["num_cuentas"] = 0
                x["valor_total"] = 0;
                x["speech"] = "No hiciste ninguna venta."
            }
            defer.resolve(x)
        },
        error: function (r) {
            bl.stop();
            alertify.alert("Error", "Ocurrió un error generando el reporte.");
        }
    });
    return defer.promise();
}
// Extrae una fecha dado una cadena tipo texto (EJ: enero de 2019, o 3 de febrero de 1980, o 1980 o enero o hoy, o ayer,...)
// No puede tener mas de una fecha -> ejemplo: 3 de enero de 2010 y 5 de enero. Toma la primera ocurrencia.
let ExtraeFechaTexto = function (txt, crearRango) {
    const meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
    const diasem = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];
    const diasem2 = ["lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo"];
    let d, m, y, dStr, mStr, yStr, dStr_from, mStr_from, yStr_from, dStr_to, mStr_to, yStr_to, fullDate;
    let currDate = new Date();
    let curr_d = currDate.getDate();
    let curr_m = currDate.getMonth() + 1;
    let curr_y = currDate.getFullYear();
    let d_from, m_from, y_from, d_to, m_to, y_to, date_range, spcFrom, spcTo, spcDate;
    let isRng = false;
    let dDefault, mDefault, yDefault;
    txt = (txt == undefined) ? "" : txt;
    crearRango = (crearRango == undefined) ? false : true;
    if (crearRango) {
        dDefault = 0;
        mDefault = 0;
        yDefault = 0;
        isRng = true;
    } else {
        dDefault = (dDefault == undefined) ? curr_d : dDefault;
        mDefault = (mDefault == undefined) ? curr_m : mDefault;
        yDefault = (yDefault == undefined) ? curr_y : yDefault;
    }

    txt = txt.toLowerCase();

    let addDays = function (y, m, d, dias) {
        var date = new Date(y, m - 1, d);
        date.setDate(date.getDate() + dias);
        return date;
    }
    let addMonths = function (y, m, d, meses) {
        var date = new Date(y, m - 1, d);
        return new Date(date.setMonth(date.getMonth() + meses));
        // return new Date(date.getFullYear(),date.getMonth()+1+meses,0);
    }
    let speachDate = function (d, m, y) {
        var date = new Date(y, m - 1, d);
        return diasem[date.getDay()] + ", " + d + " de " + meses[m - 1] + " del " + y;
    }
    let lastDayOfMonth = function (y, m) {
        return new Date(y, m, 0).getDate();
    }
    let getWeekBounds = function (y, m, d, dayOff) {
        dayOff = (dayOff == undefined) ? 0 : dayOff;
        let dateFirst = new Date(y, m - 1, d);
        let dateLast = new Date(y, m - 1, d);
        dateFirst = dateFirst.setDate(dateFirst.getDate() - (dateFirst.getDay() - dayOff));
        dateLast = dateLast.setDate(dateLast.getDate() + (6 - dateLast.getDay()) + dayOff)
        return { "dateFirst": new Date(dateFirst), "dateLast": new Date(dateLast) };
    }
    let diffDates = function (y1, m1, d1, y2, m2, d2) {
        let cd = new Date();
        y2 = (y2 == undefined) ? cd.getFullYear() : y2;
        m2 = (m2 == undefined) ? cd.getMonth() + 1 : m2;
        d2 = (d2 == undefined) ? cd.getDate() : d2;
        let date1 = new Date(y1, m1 - 1, d1);
        let date2 = new Date(y2, m2 - 1, d2);
        let diffTime = (date2.getTime() - date1.getTime());
        return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    }
    // dayOffWeek = 0->lunes, 1->martes, ... 6-> domingo
    let GetDayOfWeek = function (y, m, d, dayOffWeek) {
        let date = new Date(y, m - 1, d);
        let wBndr = getWeekBounds(date.getFullYear(), date.getMonth() + 1, date.getDate(), 1);
        return addDays(wBndr.dateFirst.getFullYear(), wBndr.dateFirst.getMonth() + 1, wBndr.dateFirst.getDate(), dayOffWeek);
    }

    // Día
    d = parseInt(txt.match(/\b([0-3]\d{1}|[1-9])\b/gm) || dDefault);

    // Año
    y = parseInt(txt.match(/\b([12]\d{3})\b/gm) || yDefault);

    // Mes
    if ((txt.match(/enero|en/gm) || []).length > 0) {
        m = 1;
    } else if ((txt.match(/febrero|feb/gm) || []).length > 0) {
        m = 2;
    } else if ((txt.match(/marzo|feb/gm) || []).length > 0) {
        m = 3;
    } else if ((txt.match(/abril|mar/gm) || []).length > 0) {
        m = 4;
    } else if ((txt.match(/mayo|abr/gm) || []).length > 0) {
        m = 5;
    } else if ((txt.match(/junio|may/gm) || []).length > 0) {
        m = 6;
    } else if ((txt.match(/julio|jun/gm) || []).length > 0) {
        m = 7;
    } else if ((txt.match(/agosto|ago/gm) || []).length > 0) {
        m = 8;
    } else if ((txt.match(/septiembre|sep/gm) || []).length > 0) {
        m = 9;
    } else if ((txt.match(/octubre|oct/gm) || []).length > 0) {
        m = 10;
    } else if ((txt.match(/noviembre|nov/gm) || []).length > 0) {
        m = 11;
    } else if ((txt.match(/diciembre|dic/gm) || []).length > 0) {
        m = 12;
    } else {
        m = mDefault;
    }

    // Desde Fechas Indirectas
    if (txt == "hoy") {
        d = curr_d;
        m = curr_m;
        y = curr_y;
    } else if (txt == "ayer") {
        let ayer = addDays(curr_y, curr_m, curr_d, -1);
        d = ayer.getDate();
        m = ayer.getMonth() + 1;
        y = ayer.getFullYear();
    } else if (txt == "antes de ayer" || txt == "antier" || txt == "antesdeayer" || txt == "anteayer") {
        let ayer2 = addDays(curr_y, curr_m, curr_d, -2);
        d = ayer2.getDate();
        m = ayer2.getMonth() + 1;
        y = ayer2.getFullYear();
    } else if (txt == "este año" || txt == "año" || txt == "corrido del año") {
        d_from = 1; m_from = 1; y_from = curr_y;
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (txt == "año pasado" || txt == "año anterior" || txt == "año que pasó") {
        d_from = 1; m_from = 1; y_from = curr_y - 1;
        d_to = 31; m_to = 12; y_to = curr_y - 1;
        isRng = true;
    } else if (txt == "último año" || txt == "año corrido") {
        d_from = curr_d; m_from = curr_m; y_from = curr_y - 1;
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (txt == "este mes" || txt == "mes" || txt == "corrido del mes") {
        d_from = 1; m_from = curr_m; y_from = curr_y;
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (txt == "mes pasado" || txt == "mes anterior" || txt == "mes que pasó") {
        y_from = (curr_m == 1) ? curr_y - 1 : curr_y;
        m_from = (curr_m == 1) ? 12 : curr_m - 1;
        d_from = 1;
        y_to = (curr_m == 1) ? curr_y - 1 : curr_y;
        m_to = (curr_m == 1) ? 12 : curr_m - 1;
        d_to = lastDayOfMonth(y_to, m_to);
        isRng = true;
    } else if (txt == "último mes" || txt == "mes corrido") {
        let prevDate = addMonths(curr_y, curr_m, curr_d, -1);
        d_from = prevDate.getDate(); m_from = prevDate.getMonth() + 1; y_from = prevDate.getFullYear();
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (txt == "esta semana" || txt == "semana" || txt == "corrido de la semana") {
        let w1 = getWeekBounds(curr_y, curr_m, curr_d, 1);
        d_from = w1.dateFirst.getDate(); m_from = w1.dateFirst.getMonth() + 1; y_from = w1.dateFirst.getFullYear();
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (txt == "semana pasada" || txt == "semana anterior" || txt == "semana que pasó") {
        let lw = addDays(curr_y, curr_m, curr_d, -7);
        let w1 = getWeekBounds(lw.getFullYear(), lw.getMonth() + 1, lw.getDate(), 1);
        d_from = w1.dateFirst.getDate(); m_from = w1.dateFirst.getMonth() + 1; y_from = w1.dateFirst.getFullYear();
        d_to = w1.dateLast.getDate(); m_to = w1.dateLast.getMonth() + 1; y_to = w1.dateLast.getFullYear();
        isRng = true;
    } else if (txt == "última semana" || txt == "semana corrida") {
        let lw = addDays(curr_y, curr_m, curr_d, -7);
        d_from = lw.getDate(); m_from = lw.getMonth() + 1; y_from = lw.getFullYear();
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (diasem2.indexOf(txt) > -1) {
        let gdw = GetDayOfWeek(curr_y, curr_m, curr_d, diasem2.indexOf(txt));
        d_from = gdw.getDate(); m_from = gdw.getMonth() + 1; y_from = gdw.getFullYear();
        d_to = gdw.getDate(); m_to = gdw.getMonth() + 1; y_to = gdw.getFullYear();
        isRng = true;
    } else if (d == 0 && m == 0 && y != 0) {
        d_from = 1; m_from = 1; y_from = y;
        d_to = 31; m_to = 12; y_to = y;
        isRng = true;
    } else if (d == 0 && m != 0 && y == 0) {
        d_from = 1; m_from = m; y_from = curr_y;
        m_to = m; y_to = curr_y;
        d_to = lastDayOfMonth(y_to, m_to);
        isRng = true;
    } else if (d != 0 && m == 0 && y == 0) {
        d_from = d; m_from = curr_m; y_from = curr_y;
        d_to = d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    } else if (d != 0 && m != 0 && y == 0) {
        d_from = d; m_from = m; y_from = curr_y;
        d_to = d; m_to = m; y_to = curr_y;
        isRng = true;
    } else if (d == 0 && m != 0 && y != 0) {
        d_from = 1; m_from = m; y_from = y;
        m_to = m; y_to = y;
        d_to = lastDayOfMonth(y_to, m_to);
        isRng = true;
    } else if (d != 0 && m == 0 && y != 0) {
        d_from = d; m_from = curr_m; y_from = y;
        d_to = d; m_to = curr_m; y_to = y;
        isRng = true;
    } else if (d != 0 && m != 0 && y != 0) {
        d_from = d; m_from = m; y_from = y;
        d_to = d; m_to = m; y_to = y;
        isRng = true;
    } else if (d == 0 && m == 0 && y == 0) {
        d_from = curr_d; m_from = curr_m; y_from = curr_y;
        d_to = curr_d; m_to = curr_m; y_to = curr_y;
        isRng = true;
    }

    if (isRng) {
        spcFrom = speachDate(d_from, m_from, y_from);
        spcTo = speachDate(d_to, m_to, y_to);
        dStr_to = (d_to < 10) ? '0' + d_to.toString() : d_to.toString();
        mStr_to = (m_to < 10) ? '0' + m_to.toString() : m_to.toString();
        yStr_to = y_to.toString();
        dStr_from = (d_from < 10) ? '0' + d_from.toString() : d_from.toString();
        mStr_from = (m_from < 10) ? '0' + m_from.toString() : m_from.toString();
        yStr_from = y_from.toString();
        date_range = { "isRange": true, "spcFrom": spcFrom, "spcTo": spcTo, "d_from": dStr_from, "m_from": mStr_from, "y_from": yStr_from, "d_to": d_to, "m_to": m_to, "y_to": y_to, "dateFrom": yStr_from + "-" + mStr_from + "-" + dStr_from, "dateTo": yStr_to + "-" + mStr_to + "-" + dStr_to };

        return date_range;
    }

    dStr = (d < 10) ? '0' + d.toString() : d.toString();
    mStr = (m < 10) ? '0' + m.toString() : m.toString();
    yStr = y.toString();

    // Periodo
    let difDate = diffDates(y, m, d);
    let perDate;
    if (difDate > 0) {
        perDate = 'Pasado';
    } else if (difDate < 0) {
        perDate = 'Futuro';
    } else if (difDate == 0) {
        perDate = 'Presente';
    } else {
        perDate = '';
    }

    // Speach Date
    spcDate = speachDate(d, m, y);

    // CREAR FULL DATE
    fullDate = yStr + "-" + mStr + "-" + dStr;

    let z = new Date(mStr + "/" + dStr + "/" + yStr);
    let z2 = z.getDay();
    let DayOfWeek = diasem[z2];

    return { "perDate": perDate, "isRange": false, "spcDate": spcDate, "d": dStr, "m": mStr, "y": yStr, "fullDate": fullDate, "DayOfWeek": DayOfWeek };
}

// Busca la primera ocurrencia dentro de una lista de comandos en un texto dado
let SearchCommand = function (txt, listOfCommands) {
    let Command = "";
    let IndexOfCommand = -1;
    let IndexInText = -1;
    let founded = false;
    if (typeof txt == "string") {
        txt = txt.toLowerCase();
    }
    for (let i = 0; i < listOfCommands.length; i++) {
        if (txt.indexOf(listOfCommands[i].toLowerCase()) > -1) {
            Command = listOfCommands[i];
            IndexInText = txt.indexOf(listOfCommands[i]);
            IndexOfCommand = i;
            founded = true;
            break;
        }
    }
    return { "Founded": founded, "Command": Command, "IndexOfCommand": IndexOfCommand, "IndexInText": IndexInText };
}


// Convertir texto a nombre propio 
String.prototype.nompropio = function () {
    let x = this.split(" ");
    let txt = '';
    for (let i = 0; i < x.length; i++) {
        txt += CapitalizeFirstLetter(x[i]) + ' ';
    }
    return txt.trim();
}

// Convertir texto a primera en mayúscula 
String.prototype.capitalize = function () {
    return CapitalizeFirstLetter(this).trim();
}

let CapitalizeFirstLetter = function (txt) {
    return txt.charAt(0).toUpperCase() + txt.slice(1);
}

//::::::::::: QUITAR ACENTOS DE UNA PALABRA :::::::::
let quitaacentos = (function (s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");
    return r;
});

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/


// Crear numero aleatorio entero
let RandomIntNumber = function (desde, hasta) {
    return Math.floor(Math.random() * hasta) + desde;
}

/** Crear Cadena Aleatorea de Numeros */
let generateRandomNumber = function (len) {
    len = (len === undefined) ? 10 : len;
    let characters = '0123456789';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        randomString = randomString + characters[Math.floor(Math.random() * characters.length - 1) + 1];
    }
    return randomString;
}

/** Crear Cadena Aleatorea de texto */
let generateRandomString = function (len) {
    len = (len === undefined) ? 10 : len;
    let characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        randomString = randomString + characters[Math.floor(Math.random() * characters.length - 1) + 1];
    }
    return randomString;
}


$(document).ready(function () {

    //if($(window).width()>=1018){OpenMenu()}

    /*************************************************/
    /******************* CLASSES *********************/
    /*************************************************/

    /** CLIENTE:
    *   Inicializar CLiente        =>    let JC = new Cliente("78609621132360");
    *   Obtener Info del cliente   =>    JC.ObtenerInfo(); --- > Crea: JC2.cust_info
    *   Usar la info del cliente una vez obtenida => JC.cust_info[0].bus_email
    */
    class Cliente {
        constructor(cust_key) {
            this.cust_key = cust_key
        }
        async ObtenerInfo() {
            let promise = new Promise((resolve, reject) => {
                $.ajax({
                    url: "get_cust_info.php",
                    dataType: "json",
                    data: ({ cust_key: this.cust_key }),
                    type: "POST",
                    success: function (r) {
                        resolve(r);
                    },
                    error: function () {
                        reject(new Error("El cliente no existe"));
                    }
                });
            });
            let result = await promise;
            this.cust_info = result;
        }
    }

    /******************************************************/
    /******************** INFOBOX JS **********************/
    /******************************************************/

    // OCULTAR COSAS CUANDO SE DE CLICK EN OTRO LADO
    $(window).click(function () {
        $(".infobox>div").css({ "opacity": "0" });
    });

    $(".infobox").off("hover").hover(function () {
        $(this).children('div').css({ "display": "block" });
        $(this).children('div').css({ "opacity": "1" });
    }, function () {
        $(this).children('div').css({ "display": "none" });
        $(this).children('div').css({ "opacity": "0" });
    });

    $(".infobox").off("click").click(function (event) {
        event.stopPropagation();
        $(".infobox>div").css({ "opacity": "0" });
        let op = $(this).children('div').css("opacity");
        if (op == "0") {
            $(this).children('div').css({ "display": "block" });
            $(this).children('div').css({ "opacity": "1" });
        } else {
            $(this).children('div').css({ "display": "none" });
            $(this).children('div').css({ "opacity": "0" });
        }
    });

    /******************************************************/
    /******************** INPUT FORMATS *******************/
    /******************************************************/

    /********* FORMAT PERCENTAGE 34231.12 % *********/
    $(".input_perc").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            if (parseInt(value) > 100) {
                value = "100";
            } else if (parseInt(value) < 0) {
                value = "0";
            }
            $(this).val(NumericInput2(value, true, false, 2, true) + "%");
        }
    });

    /********* FORMAT DECIMAL 34231.12 *********/
    $(".input_dec").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            $(this).val(NumericInput2(value, true, false, 2));
        }
    });

    /********* FORMAT DECIMAL WITH FORMAT NUMBER EJ: 34,231.12 *********/
    $(".input_dec_sty").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            $(this).val(NumericInput2(value, true, true, 2));
        }
    });

    /********* FORMAT INTEGER 34231  *********/
    $(".input_int").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            $(this).val(NumericInput2(value, false, false, 0));
        }
    });

    /********* FORMAT INTEGER WITH NUMB FORMAT 34,231  *********/
    $(".input_int_sty").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            $(this).val(NumericInput2(value, false, true, 0));
        }
    });


    /********* FORMAT PHONE *********/
    $(".input_phone").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

        } else {
            let value = ($(this).val());
            value = value.replace(/[^0-9]/g, "");
            if (value.length > 7) {
                value = value.replace(/\D+/g, "").replace(/([0-9]{1,3})([0-9]{3})([0-9]{4}$)/gi, "($1) $2 $3");
            } else {
                value = value.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{4}$)/gi, "$1 $2");
            }
            $(this).val(value);
        }
    });

    //******** FORMAT CURRENCY ******
    // $('.input_currency').off("keyup").keyup(function() {
    //     var numInput = ($(this).val().replace(/[^\-\d\.]/g, ""));
    //     var first_pos = numInput.substr(0, 1);
    //     var last_pos = numInput.substr(numInput.length - 1, 1); 
    //     if (last_pos != '.') {
    //         if (!isNaN(numInput)) {
    //             if (last_pos == '0') {
    //                 $(this).val('$ ' + numCommas(numInput));
    //             } else {
    //                 $(this).val('$ ' + numCommas(numRound(numInput, 2)));
    //             }
    //         } else {
    //             $(this).val('');
    //         }
    //     }
    // });
    $('.input_currency').off("focusout").focusout(function () {
        var numInput = ($(this).val().replace(/[^\-\d\.]/g, ""));
        if (!isNaN(numInput)) {
            $(this).val('$ ' + numCommas(numRound(numInput, 2)));
        } else {
            $(this).val('');
        }

    });

    // CHANGE EVENT FOR SELECT COUNTRY country_input (See function: loadCountries )
    $(".country_input").change(function () {
        var code = this.value;
        if (code == '') {
            $(this).removeClass("select_country_sty");
            $(this).css({ 'background-image': 'none' });
        } else {
            $(this).addClass("select_country_sty");
            $(this).css({ 'background-image': 'url("../icons/flags/small/' + code.toLowerCase() + '.png")' });
        }
    });

});


/*** DESACTIVATE SUSCRIPTION TO PACKAGE  ***/
let desactivatePkg = function (pkg_id, event, fun_on_success = () => { }) {
    if (event.type != 'load') {
        let mp = new MainRoller(15000);
        mp.startMainRoller();

        $.ajax({
            url: 'epay_unsuscribe.php',
            dataType: 'json',
            data: ({ pkg_id }),
            type: 'POST',
            success: function (r) {
                mp.stopMainRoller();
                if (r[0][0] == 'S') {
                    alertify.message(r[0][1]);
                    fun_on_success();
                    $('#mp_price_' + pkg_id).show();
                    $('#btn_p_' + pkg_id).show();
                    $('#wr_res_' + pkg_id).html("");
                    $('#btn_str_tst_' + pkg_id).hide();
                    checkFreeTrail(pkg_id).then(function (r) {
                        if (!r.isactive && r.date_str == '1900-01-01 00:00:00') {
                            $('#btn_str_tst_' + pkg_id).show();
                        } else if (!r.isactive && r.date_str != '1900-01-01 00:00:00') {
                            $('#btn_str_tst_' + pkg_id).hide();
                            $('#wr_res_' + pkg_id).html("<div class='tag black5 mt40 center_hr'>Tu período de prueba a caducado.</div>");
                        } else if (r.isactive) {
                            $('#btn_str_tst_' + pkg_id).hide();
                            $('#wr_res_' + pkg_id).html("<div class='tag black5 mt40 center_hr'>Te quedan " + r.days_to_end + " días de tu prueba gratuita.</div>");
                        };
                    });
                } else if (r[0][0] == 'E') {
                    alertify.alert("Error", r[0][1]);
                }
            },
            error: function () {
                mp.stopMainRoller();
                alertify.alert("Error", "Ocurrió un error durante la ejecución.");
            }
        });

    }
}

/*** TOGGLE PACKAGE  ***/
let toggle_pkg = function (pkg_id, tg_state) {
    if (tg_state == 'active') {
        $('#mp_price_' + pkg_id).hide();
        $('#btn_p_' + pkg_id).hide();
        $('#btn_str_tst_' + pkg_id).hide();
        $('#wr_res_' + pkg_id).html("");
        $('#wr_res_' + pkg_id).html("<div class='tag black5 mt40 center_hr'>El paquete se encuentra activo actualmente</div><div id='btn_rmp_" + pkg_id + "' class='luca_link mt10 center_txt'>Da click aquí para desactivar la suscripción al módulo.</div>");
        let fun_succ = () => { };
        if (pkg_id == 1) {
            fun_succ = CloseMpAnalytics();
        } else if (pkg_id == 2) {
            fun_succ = CloseMpChatbot();
        } else if (pkg_id == 3) {
            fun_succ = CloseMpTracker();
        }
        document.getElementById("btn_rmp_" + pkg_id).addEventListener("click", () => desactivatePkg(pkg_id, event, () => { fun_succ }), false);
    } else if (tg_state == 'inactive') {
        $('#mp_price_' + pkg_id).show();
        $('#btn_p_' + pkg_id).show();
        $('#wr_res_' + pkg_id).html("");
        $('#btn_str_tst_' + pkg_id).hide();
        checkFreeTrail(pkg_id).then(function (r) {
            if (!r.isactive && r.date_str == '1900-01-01 00:00:00') {
                $('#btn_str_tst_' + pkg_id).show();
            } else if (!r.isactive && r.date_str != '1900-01-01 00:00:00') {
                $('#btn_str_tst_' + pkg_id).hide();
                $('#wr_res_' + pkg_id).html("<div class='tag black5 mt40 center_hr'>Tu período de prueba a caducado.</div>");
            } else if (r.isactive) {
                $('#btn_str_tst_' + pkg_id).hide();
                $('#wr_res_' + pkg_id).html("<div class='tag black5 mt40 center_hr'>Te quedan " + r.days_to_end + " días de tu prueba gratuita.</div>");
            };
        });
    }
}

/*** LOAD CUSTOMER INFO FROM EPAYCO ***/
let LoadEpayDataCust = function () {
    $.ajax({
        url: 'epay_getcur_cust.php',
        dataType: 'json',
        data: ({ var1: 1 }),
        type: 'POST',
        success: function (r) {
            if (r[0][0] == "S") {
                $("#payEmail").val(r[1]["email"]);
                $("#payAddress").val(r[1]["address"]);
                $("#payTel").val(r[1]["phone"]);
                $("#payName").val(r[1]["name"]);
            }
        }
    });
}


/******************************************************************************/
/************************* AUXILIAR EVENTS FUNCTIONS **************************/
/******************************************************************************/

/**
 * Key up event: Format credit card style and assign credit card icon to background.
 * 4575623182290326 -> (VISA ICON) 4575 6231 8229 0326
 * @param {event} e Event
 * @param {object} o Object Element Input
 * 
 * @example
 * $(".input_crcd").keyup((e) => { keyup_format_ccard(e, this); });
 */
function keyup_format_ccard(e, o) {
    if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

    } else {
        let value = $(o).val();
        let value_new = NumericInput2(value, false, false, 0);
        value_new = cc_format(value_new);
        let frq = getCreditCardType(value_new);
        $(o).css({ "background-image": "url(" + frq.iconURL + ")" });
        $(o).val(value_new);
    }
}

/**
 * Key up event: Format month input of credit card.
 * @param {event} e Event
 * @param {object} o Object Element Input
 * 
 * @example
 * $(".input_crcd_mm").keyup((e) => { keyup_format_ccard_mm(e, this); });
 */
function keyup_format_ccard_mm(e, o) {
    if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

    } else {
        let value = $(o).val();
        if (value.length > 2) { value = value.substr(0, 2); }
        $(o).val(NumericInput2(value, false, false, 0));
    }
}

/**
 * Key up event: Format year input of credit card.
 * @param {event} e Event
 * @param {object} o Object Element Input
 * 
 * @example
 * $(".input_crcd_yyyy").keyup((e) => { keyup_format_ccard_yy(e, this); });
 */
function keyup_format_ccard_yy(e, o) {
    if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

    } else {
        let value = $(o).val();
        if (value.length > 4) {
            value = value.substr(0, 4);
        }
        $(o).val(NumericInput2(value, false, false, 0));
    }
}

/**
 * Focusout event that will format a input object that has 1 digit into 2 digits after leave the input. Ex: "1" -> "01".
 * @param {object} o Object Element Input
 * 
 * @example
 * $(".input_crcd_mm").focusout((e) => { focusout_format_dd(this); });
 */
function focusout_format_dd(o) {
    let value = $(o).val();
    let value_new = (value.length == 1) ? "0" + value : value;
    $(o).val(value_new);
}


/**
 * Key up event: Format CSV input of credit card.
 * @param {event} e Event
 * @param {object} o Object Element Input
 * 
 * @example
 * $(".input_crcd_csv").keyup((e) => { keyup_format_ccard_csv(e, this); });
 */
function keyup_format_ccard_csv(e, o) {
    if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

    } else {
        let value = $(o).val();
        if (value.length > 3) {
            value = value.substr(0, 3);
        }
        $(o).val(NumericInput2(value, false, false, 0));
    }
}


/******************************************/
/********** STATISTIC FUNCTIONS ***********/
/******************************************/

/**
 * Returns the percentile of the given value in a numeric array.
 * @param {array} array Array of numbers.
 * @param {number} n Number to compute the percentile.
 * 
 * @example
 * percentRank([1,3,2,1,2,3,4,5,2,3],4)
 * > 0.85
 */
let percentRank = function (array, n) {
    if (typeof n !== 'number') throw new TypeError('n must be a number');
    var L = 0;
    var S = 0;
    var N = array.length
    L = array.map((el) => el < n).reduce((a, v) => a + v);
    S = array.map((el) => el === n).reduce((a, v) => a + v);
    return (L + (0.5 * S)) / N
}

/**
 * Returns the value at a given percentile in a numeric array.
 * "Linear interpolation between closest ranks" method
 * @param {array} array Array of numbers.
 * @param {number} p Percentile. Between 0 an 1.
 * 
 * @example
 * percentile([12,3.4,3.5,1,3.1,2,1,4],0.5)
 * > 3.25
 */
function percentile(array, p) {
    array.sort((a, b) => a - b);
    if (array.length === 0) return 0;
    if (typeof p !== 'number') throw new TypeError('p must be a number');
    if (p <= 0) return array[0];
    if (p >= 1) return array[array.length - 1];
    var index = (array.length - 1) * p,
        lower = Math.floor(index),
        upper = lower + 1,
        weight = index % 1;
    if (upper >= array.length) return array[lower];
    return array[lower] * (1 - weight) + array[upper] * weight;
}

/**
 * Get only unique values of array from a filter.
 * @param {*} value 
 * @param {*} index 
 * @param {*} self 
 * 
 * @example
 * [1,2,3,1,2,3,1,3].filter(onlyUnique);
 * > [1, 2, 3]
 */
let onlyUnique = (function (value, index, self) { return self.indexOf(value) === index; });

/**
* Number.prototype.format(n, x)
* 
* @param integer n: length of decimal
* @param integer x: length of sections
*/
Number.prototype.formaty = function (n, x) {
    let re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/* SUMAR ELEMENTOS DE UN ARREGLO --- EJ: [2,2,1].sumar(); --> 5 */
Array.prototype.sumar = function () { return this.reduce(function (a, b) { return a + b; }); }
/* MULTIPLICAR ELEMENTOS DE UN ARREGLO --- EJ: [2,2,2].multiplicar(); --> 8 */
Array.prototype.multiplicar = function () { return this.reduce(function (a, b) { return a * b; }); }
/* OBTENER EL MAXIMO NUMERO DE UN ARRAGLO  --- EJ: [3,5,2,6,7,3,100,0,1,0].max(); ---> 100*/
Array.prototype.max = function () { return Math.max(...this) };
/* OBTENER EL MINIMO NUMERO DE UN ARRAGLO  --- EJ: [3,5,2,6,7,3,100,0,1,0].max(); ---> 100*/
Array.prototype.min = function () { return Math.min(...this) };
/* TOMAR UNICAMENTE LOS VALORES UNICOS DE UN ARREGLO --- EJ: [1,3,4,1,3,4,6,7,8,2,12,1,1,1,1].unique() ---> [1, 3, 4, 6, 7, 8, 2, 12] */
Array.prototype.unique = function () { return this.filter(onlyUnique) };
/* TOMAR SUB ELEMENTOS DE UN ARREGLO */
Array.prototype.subarray = function (start, end) {
    start = (start == undefined || start == '') ? 0 : start;
    end = (end == undefined || end == '') ? this.length : end;
    let new_arr = [];
    for (let i = 0; i < this.length; i++) {
        if (i >= start && i <= end) { new_arr.push(this[i]) }
    }
    return new_arr;
}
/* TOMAR LOS ULTIMOS N ELEMENTOS DE UN ARREGLO*/
Array.prototype.last = function (n) { if (n == null) return this[this.length - 1]; return this.slice(Math.max(this.length - n, 0)); }
/* EXTENDER UN ARREGLO CON OTRO --- EJ: [1,2,3].extender([4,5]) ---> [1, 2, 3, 4, 5]*/
Array.prototype.extender = function (a) { let x = []; this.forEach(c => x.push(c)); a.forEach(c => x.push(c)); return x; };

/* OBTERNER EL TIPO DE BROWSER: SAFARI, FIREFOX , .... */
let get_browser_info = (function () {
    let ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: 'IE ', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
        name: M[0],
        version: M[1]
    };
});

/**** FUNCIONES DE COOKIES ******/
let GetVariableCookie = (function (vari) {
    return Cookies.get(vari);
});

/******* CONVERTIR ARREGLO DE DATOS JSON (OBJETO O EN FORMA DE STRING CON JSON.stringyfy) EN CSV ESTILO *********/
let convertToCSV = (function (objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','
            line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
});

/**
 * Open Help Modal with the HTML that is passed in the function.
 * @param {string} HTMLText String in HTML form with the content of the modal.
 * 
 * @example
 * OpenHelpModalHTML("<h1 class='mb10'>Bienvenido! 👋</h1><p>Parece que es la primera vez que ingresas a el modulo de analitica.</p>")
 */
let OpenHelpModalHTML = (function (HTMLText) {
    $('#help_container').append('');
    $('#help_container').append(HTMLText);
    OpenHelp();
});

/******* EXPORTAR CSV *********/
let exportCSVFile = (function (headers, items, fileTitle) {
    if (headers) {
        items.unshift(headers);
    }
    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);
    var csv = convertToCSV(jsonObject);
    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) {
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        } else {
            var csvWindow = window.open("", "CSV Datos", "");
            csvWindow.document.write("<p>" + csv + "</p>");
        }
    }
});

/* VERIFICAR SI SE ESTA EN UN DISPOSITIVO MOVIL */
let isMobileDevice = (function () {
    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobile = true;
    }
    return isMobile;
});

/**
 * Transform a string RGB into HEX
 * @param {string} colorval String with the RGB color.
 * 
 * @example
 * hexc("rgb(255,255,255)")
 * > "#ffffff"
 */
let hexc = (function (colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete (parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    color = '#' + parts.join('');
    return color;
});

// ******** SANITIZ STRING ********
let sanitizeString = function (str) {
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
    return str.trim();
}

/**
 * Format Big Numbers.
 * @param {numer} num Number to be formated.
 * @param {number} rnd Number of digits to round the number.
 * @param {string} connector Connector between number and magnitude. 
 * 
 * @example
 * formatBigNumber(1200,1,'')
 * > "1.2K"
 */
let formatBigNumber = function (num, rnd, connector = ' ') {
    rnd = (rnd === undefined) ? 1 : rnd;
    let newNum;
    if (Math.abs(num) >= 0 && Math.abs(num) < 1000) {
        newNum = numCommas(numRound(num, rnd));
    } else if (Math.abs(num) >= 1000 && Math.abs(num) < 1000000) {
        newNum = numCommas(numRound(num / 1000, rnd)) + connector + 'K';
    } else if (Math.abs(num) >= 1000000) {
        newNum = numCommas(numRound(num / 1000000, rnd)) + connector + 'M';
    } else {
        newNum = numCommas(num);
    }
    return newNum;
}

/**
 * Format numbers in thousands.
 * 
 * @param {number} x Number to be formated.
 * @param {string} separator Separator of thousands.
 * 
 * @example
 * numCommas(1300, ",")
 * > "1,300"
 */
let numCommas = (function (x, separator = ",") {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
});

/******* FUNCTION THAT ROUNDS A NUMBER ***************/
/**
 * Round a number.
 * 
 * @param {number} x Number
 * @param {*} n Number of decimals.
 * 
 * @example
 * numRound(1200.23141,1);
 * > 1200.2
 */
let numRound = (function (x, n) {
    var ri = Math.pow(10, n);
    return Math.round(Number(x) * ri) / ri;
});

/************************************************************************/

/* CHECK IF COOKIES ARE ENABLED */
let checkCookiesSupport = (function () {
    if (!navigator.cookieEnabled) {
        alertify.alert('Cookies Inhabilitadas', 'Parece que tu navegador tiene deshabilitado el uso de cookies y la página puede no funcionar correctamente. Para disfrutar de todas las opciones de Luca, por favor ingresa el menú de configuración de tu navegador y permite que los sitios web lean y guarden datos de cookies. Gracias!');
    }
});

/* DELETE ALL COOKIES JS */
let deleteAllCookies = (function () {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
});

/* CHECK INPUT (TEXT,PASS,...) */
let CheckInputText = (function (idTag, errorMsgId, nameCampo, NoEmpty, lenMin, lenMax, JustNumeric, JustText, NoSpecialChars, typeInput) {
    if (lenMin == null || lenMin == undefined) { lenMin = 0 };
    if (lenMax == null || lenMax == undefined) { lenMax = 5000 };
    if (NoEmpty == null || NoEmpty == undefined) { NoEmpty = false };
    if (JustNumeric == null || JustNumeric == undefined) { JustNumeric = false };
    if (JustText == null || JustText == undefined) { JustText = false };
    if (NoSpecialChars == null || NoSpecialChars == undefined) { NoSpecialChars = false };
    if (nameCampo == null) { nameCampo = "" };
    if (typeInput == null || typeInput == undefined) { typeInput = "" }
    //if(NoSpace == null){NoSpace = false};
    var MsgError = '';

    if (NoEmpty == true && $('#' + idTag).val().length == 0) {
        MsgError = 'El campo ' + nameCampo + ' no puede estar vacío';
        if (errorMsgId != null && errorMsgId != '') {
            ErrorMsgModalBox('open', errorMsgId, MsgError);
        }
        setTimeout(function () { ErrorInputBox(idTag) }, 100);
        return false;
    }
    if ((($('#' + idTag).val().length < lenMin) || ($('#' + idTag).val().length > lenMax))) {
        MsgError = 'El campo ' + nameCampo + ' debe tener máximo ' + lenMax + ' caracteres.';
        if (errorMsgId != null && errorMsgId != '') {
            ErrorMsgModalBox('open', errorMsgId, MsgError);
        }
        setTimeout(function () { ErrorInputBox(idTag) }, 100);
        return false;
    }
    if (JustNumeric == true && isNaN($('#' + idTag).val())) {
        MsgError = 'El campo ' + nameCampo + ' debe ser numerico';
        if (errorMsgId != null && errorMsgId != '') {
            ErrorMsgModalBox('open', errorMsgId, MsgError);
        }
        setTimeout(function () { ErrorInputBox(idTag) }, 100);
        return false;
    }
    if (JustText == true && $('#' + idTag).val().match(/[^A-Za-z áéíóúâêôãõçÁÉÍÓÚÂÊÔÃÕÇüñÜÑ]/) != null) {
        MsgError = 'El campo ' + nameCampo + ' debe contener solo texto';
        if (errorMsgId != null && errorMsgId != '') {
            ErrorMsgModalBox('open', errorMsgId, MsgError);
        }
        setTimeout(function () { ErrorInputBox(idTag) }, 100);
        return false;
    }
    if (NoSpecialChars == true && $('#' + idTag).val().match(/[^-A-Za-z áéíóúâêôãõçÁÉÍÓÚÂÊÔÃÕÇüñÜÑ\d\.]/) != null) {
        MsgError = 'El campo ' + nameCampo + ' no debe contener caracteres especiales';
        if (errorMsgId != null && errorMsgId != '') {
            ErrorMsgModalBox('open', errorMsgId, MsgError);
        }
        setTimeout(function () { ErrorInputBox(idTag) }, 100);
        return false;
    }
    if (typeInput == "phone") {
        if ($('#' + idTag).val().match(/[^0-9\(\)\s]/g) != null) {
            MsgError = 'El campo ' + nameCampo + ' no es correcto.';
            if (errorMsgId != null && errorMsgId != '') {
                ErrorMsgModalBox('open', errorMsgId, MsgError);
            }
            setTimeout(function () { ErrorInputBox(idTag) }, 100);
            return false;
        }
    }
    if (typeInput == "email") {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let xem = re.test(String($('#' + idTag).val()).toLowerCase());
        if (!xem) {
            MsgError = 'El email no es correcto.';
            if (errorMsgId != null && errorMsgId != '') {
                ErrorMsgModalBox('open', errorMsgId, MsgError);
            }
            setTimeout(function () { ErrorInputBox(idTag) }, 100);
            return false;
        }
    }
    return true;
});
/* ERROR INPUT (BORDER RED) */
let ErrorInputBox = (function (InputId) {
    $('#' + InputId).focus();
    $('#' + InputId).addClass('ErrorInputStyle');
    setTimeout(function () {
        $('#' + InputId).removeClass('ErrorInputStyle');
    }, 4000);
});

/***** CHECK PASSWORD STRENGTH *****/
let checkStrengthPass = (function (password) {
    var pass = password;
    var strength = 1;
    var arr = [/.{7,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/, /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/];
    jQuery.map(arr, function (regexp) {
        if (pass.match(regexp))
            strength++;
    });
    return strength;
});

//***** NAVIGATION BAR DEL LANDING PAGE *****
let openMenuSS = false;
$(document).ready(function () {

    $('#menu_icon').click(function () {

        let ScrollY = $(window).scrollTop();

        if (openMenuSS == false) {
            $('#menu_small_screen').css('transform', 'translateX(0%)');
            $('#menu_icon').css({ 'transform': 'rotate(180deg)', 'color': '#00bb7d' });
            $('#menu_icon').mouseover(function () {
                $(this).css('color', '#009d69');
            }).mouseout(function () {
                $(this).css('color', '#00bb7d');
            });
            $('#navigation_container').css({
                'box-shadow': 'none',
                'background-color': 'rgba(255, 255, 255, 1)'
            });
            openMenuSS = true;
        } else {
            $('#menu_small_screen').css('transform', 'translateX(100%)');
            $('#menu_icon').css({ 'transform': 'rotate(0deg)' });
            if (ScrollY >= 200) {
                $('#navigation_container').css({
                    'box-shadow': '0 4px 8px 0 rgba(82,97,115,0.18)',
                    'background-color': 'rgba(255, 255, 255, 1)'
                });
                $(".logo_nav_tx").attr("src", "../logos/logo5.png");
                $(".menu_icon").removeClass('white');
                $('#menu_icon').css({ 'color': '#00bb7d' });
                $('#menu_icon').mouseover(function () {
                    $(this).css('color', '#009d69');
                }).mouseout(function () {
                    $(this).css('color', '#00bb7d');
                });
            } else {
                $('#navigation_container').css({
                    'box-shadow': 'none',
                    'background-color': 'transparent'
                })
                $(".logo_nav_tx").attr("src", "../logos/logo8.png");
                $(".menu_icon").addClass('white');
                $('#menu_icon').css({ 'color': '#00bb7d' });
                $('#menu_icon').mouseover(function () {
                    $(this).css('color', '#009d69');
                }).mouseout(function () {
                    $(this).css('color', '#00bb7d');
                });
            }
            openMenuSS = false;
        }

    });

    $('#menu_wide_screen li:first-child').click(function () { window.location = '../php/analytics_home.php'; });
    $('#menu_small_screen li:first-child').click(function () { window.location = '../php/analytics_home.php'; });

    $('#menu_wide_screen li:nth-child(2)').click(function () { window.open('https://medium.com/blog-2luca', '_blank'); });
    $('#menu_small_screen li:nth-child(2)').click(function () { window.open('https://medium.com/blog-2luca', '_blank'); });

    $('#menu_wide_screen li:nth-child(3)').click(function () {
        $("#msg-box-1").css({ "display": "block" });
        $("#msg-box-container").css({ "display": "block" });
    });
    $('#menu_small_screen li:nth-child(3)').click(function () {
        $("#msg-box-1").css({ "display": "block" });
        $("#msg-box-container").css({ "display": "block" });
    });

    $('#menu_wide_screen li:nth-child(4)').click(function () { window.location = '../php/register.php'; });
    $('#menu_small_screen li:nth-child(4)').click(function () { window.location = '../php/register.php'; });


    $('#img_logo_nav').click(function () {
        window.location = '../index.php';
    });
});

$(window).scroll(function () {

    var ScrollY = $(window).scrollTop();

    if (ScrollY >= 200) {
        $('#navigation_container').css({
            'box-shadow': '0 4px 8px 0 rgba(82,97,115,0.18)',
            'background-color': 'rgba(255, 255, 255, 1)'
        });
        $(".logo_nav_tx").attr("src", "../logos/logo5.png");
        $(".menu_icon").removeClass('white');
    } else {
        $(".logo_nav_tx").attr("src", "../logos/logo8.png");
        $(".menu_icon").addClass('white');
        if (openMenuSS == false) {
            $('#navigation_container').css({
                'box-shadow': 'none',
                'background-color': 'transparent'
            });
        } else {
            $('#navigation_container').css({
                'box-shadow': 'none',
                'background-color': 'rgba(255, 255, 255, 1)'
            });
        }

    }
});

//**************************

/*::::::::::::::::::::::::::::::::::::::::*/
/*::::::::::::::: LOADINGS :::::::::::::::*/
/*::::::::::::::::::::::::::::::::::::::::*/

class MainRoller {
    constructor(maxTime) {
        this.MROpen = false;
        this.maxTime = (maxTime == undefined) ? 12000 : maxTime;
        //this.maxTime = maxTime;
    }
    startMainRoller() {
        $('#MainRoller_background').show();
        this.MROpen = true;
        setTimeout(() => {
            if (this.MROpen && $('#MainRoller_background').css('display') == "block") {
                this.stopMainRoller();
                alertify.alert('', "El proceso está tomando demasiado tiempo.");
            }
        }, this.maxTime);
    }
    stopMainRoller() {
        $('#MainRoller_background').hide();
        this.MROpen = false;
    }
}

/*
    let bl = new BarLoading(30000,"Procesando pago");
    bl.start();
    bl.stop();
*/
class BarLoading {
    constructor(maxTime, msg) {
        this.MROpen = false;
        this.maxTime = (maxTime === undefined) ? 12000 : maxTime;
        msg = msg || "Cargando";
        $('.bar_ld_msg').text(msg);
        $('.bar_loading_cont').hide();
    }
    start() {
        $('.bar_loading_cont').show();
        this.MROpen = true;
        setTimeout(() => {
            if (this.MROpen && $('.bar_loading_cont').css('display') == "block") {
                this.stop();
                alert("El proceso está tomando demasiado tiempo.");
            }
        }, this.maxTime);
    }
    stop() {
        $('.bar_loading_cont').hide();
        this.MROpen = false;
    }
}

/*::::::::::::::::::::::::::::::::::::::::*/


/**
 * 
 * ::::::::::::::::::::::::: NOTIFY ALERT ::::::::::::::::::::::
 * HTML ===>
 * <div class="notify_alert"><span>x</span><div class="na_title"><i class="fas fa-bell"></i><span></span></div><div class="na_msg"></div></div>
 * 
 * ACTIVATE ===>
 * let al1 = new NotifyAlert("Título de prueba","Subtitulo de prueba",function(){alert("Prueba");},"#333333","#ffffff");
 * al1.open();
 * 
 * let al3 = new NotifyAlert("Título de prueba 3","Subtitulo de prueba 3").open();
 * 
 * CLOSE (OPTIONAL)===>
 * let alclose = new NotifyAlert().close();
 *
*/
class NotifyAlert {

    constructor(title, subtitle, action_fn, BgColor, TxtColor) {
        this.title = title;
        this.subtitle = subtitle;
        this.action_fn = action_fn;
        this.BgColor = BgColor;
        this.TxtColor = TxtColor;
        $(".na_title>span").text(this.title);
        $(".na_msg").text(this.subtitle);
        $(".notify_alert>span").click(function (event) {
            event.stopPropagation();
            $(".notify_alert").removeClass("open");
        });
        this.action_fn = (this.action_fn === undefined) ? function () { } : this.action_fn;
        this.BgColor = (this.BgColor === undefined) ? "rgba(50, 50, 50, 0.8)" : this.BgColor;
        this.TxtColor = (this.TxtColor === undefined) ? "#ffffff" : this.TxtColor;
        $(".notify_alert").css({ "color": this.TxtColor, "background": this.BgColor })
        $(".notify_alert").off().click(this.action_fn);
    }
    open() {
        $(".notify_alert").removeClass("open").addClass("open");
    }
    close() {
        $(".na_title>span").text('');
        $(".na_msg").text('');
        $(".notify_alert").click(function () { });
        $(".notify_alert").removeClass("open");
    }

}

// GO TO LINK 
let gotoLink = function (link_str) {
    window.location.href = link_str;
}

/**
 * Check Package Access.
 * 
 * @param {*} pkg_id 
 * 
 * @example
 * CheckPkgAccess(3).then(function (r) { console.log(r) });
 */
let CheckPkgAccess = function (pkg_id) {
    let defer = $.Deferred();
    if (pkg_id != undefined) {
        $.ajax({
            url: "check_mp_states.php",
            dataType: "json",
            data: ({ x: 1 }),
            type: "POST",
            success: function (r) {
                if (r[0][0] == "S") {
                    (r[1]["pkg_" + pkg_id] == "1") ? defer.resolve(true) : defer.resolve(false);
                } else if (r[0][0] == "E") {
                    throw "Error #18237-B";
                }
            }
        });
        return defer.promise();
    } else {
        throw "Error #18237-A";
    }
}

/**
 * Check Free Trail.
 * 
 * @param {*} pkg_id 
 * 
 * @example
 * checkFreeTrail(3).then(function (r) { console.log(r) });
 */
let checkFreeTrail = function (pkg_id) {
    let defer = $.Deferred();
    $.ajax({
        url: "check_freetrail_mp.php",
        dataType: "json",
        data: ({ pkg_id }),
        type: "POST",
        success: function (r) {
            if (r[0][0] == 'S') {
                if (r[1]["pkg_freetrl_active"] == "0") {
                    defer.resolve({ isactive: false, days_to_end: r[1]["days_to_end"], date_str: r[1]["date_str"] });
                } else if (r[1]["pkg_freetrl_active"] == "1") {
                    defer.resolve({ isactive: true, days_to_end: r[1]["days_to_end"], date_str: r[1]["date_str"] });
                } else {
                    throw "Error #31237-B";
                }
            } else if (r[0][0] == 'E') {
                throw "Error #31237-A";
            }
        }
    });
    return defer.promise();
}


/**
 * ERROR MESSAGE
 * 
 * Se dispara usando: ErrorMsgModalBox(action, id_object, msg, timeout=10000);
 * 
 * y en html: \<span class="errorMsg" id="id_object">\</span>
 * 
 * @param {string} action Action. 'open' or 'close'
 * @param {*} id_object Id of the element
 * @param {*} msg Message to display.
 * @param {*} timeout For how many time it is display before desapear. In milisconds. Default value is 10000.
 * 
 * @example
 * //In HTML
 * <span class="errorMsg" id="errorMsgNewProduct"></span>;
 * //In JS
 * ErrorMsgModalBox('open','errorMsgNewCustomer','Fecha de nacimiento incorrecta');
 */
let ErrorMsgModalBox = (function (action, id_object, msg, timeout) {
    if (typeof (timeout) === 'undefined') timeout = 10000;
    if (action == 'open') {
        $("#" + id_object).text(msg);
        $("#" + id_object).addClass('errorMsgDisplay');
        setTimeout(function () {
            $("#" + id_object).text('');
            $("#" + id_object).removeClass('errorMsgDisplay');
        }, timeout);
    } else if (action == 'close') {
        $("#" + id_object).text('');
        $("#" + id_object).removeClass('errorMsgDisplay');
    } else {
        $("#" + id_object).text('');
        $("#" + id_object).removeClass('errorMsgDisplay');
    }
});
/* ************* SIMPLE MESSAGE CLICKABLE *******************
	Se dispara utilizando:
		SimpleMsgModalBox(action, id_object, msg, timeout=10000);	
	y HTML: 
		<span class="simpleMsg" id="id_object"></span>
	Ejemplo: 
		<span class="simpleMsg" id="simpleMsgNewCustomer"></span>
		SimpleMsgModalBox('open','simpleMsgNewCustomer','Ya existe un cliente registrado con el email');
*/
let SimpleMsgModalBox = (function (action, id_object, msg, timeout) {
    if (typeof (timeout) === 'undefined') timeout = 10000;
    if (action == 'open') {
        $("#" + id_object).text(msg);
        $("#" + id_object).addClass('simpleMsgDisplay');
        setTimeout(function () {
            $("#" + id_object).text('');
            $("#" + id_object).removeClass('simpleMsgDisplay');
        }, timeout);
    } else if (action == 'close') {
        $("#" + id_object).text('');
        $("#" + id_object).removeClass('simpleMsgDisplay');
    } else {
        $("#" + id_object).text('');
        $("#" + id_object).removeClass('simpleMsgDisplay');
    }
});

/******** CONVERT INPUT VALUE TO NUMERIC ********/
let NumericInput = (function (input_id, e, decimal, format) {
    if (typeof (decimal) === 'undefined') decimal = true;
    if (typeof (format) === 'undefined') format = false;
    if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

    } else {
        if (decimal == true) {
            x = ($("#" + input_id).val().toString().replace(/[^0-9.]/g, ""));
        } else {
            x = ($("#" + input_id).val().toString().replace(/[^0-9]/g, ""));
        }
        if (format == true) {
            var numInput = x;
            var last_pos = numInput.substr(numInput.length - 1, 1);
            if (last_pos != '.') {
                if (!isNaN(numInput) && numInput > 0) {
                    if (last_pos == '0') {
                        $("#" + input_id).val('$ ' + numCommas(numInput));
                    } else {
                        $("#" + input_id).val('$ ' + numCommas(numRound(numInput, 2)));
                    }
                } else {
                    $("#" + input_id).val('');
                }
            }
        } else {
            $("#" + input_id).val(x);
        }
    }
});


let NumericInput2 = (function (input_value, decimal, format, numDecimals, acceptZeo) {
    if (typeof (decimal) === 'undefined') decimal = true;
    if (typeof (format) === 'undefined') format = false;
    if (typeof (numDecimals) == 'undefined') numDecimals = 2;
    acceptZeo = (acceptZeo === undefined) ? false : true;

    let x, last_pos, numInput;

    if (decimal == true) {
        x = (input_value.toString().replace(/[^0-9.]/g, ""));
    } else {
        x = (input_value.toString().replace(/[^0-9]/g, ""));
    }
    if (format == true) {
        numInput = x;
        last_pos = numInput.substr(numInput.length - 1, 1);
        if (last_pos != '.') {
            if (!isNaN(numInput) && numInput > 0) {
                if (last_pos == '0') {
                    return numCommas(numInput);
                } else {
                    return numCommas(numRound(numInput, numDecimals));
                }
            } else {
                if (acceptZeo) {
                    return 0;
                } else {
                    return '';
                }
            }
        }
    } else {
        numInput = x;
        last_pos = numInput.substr(numInput.length - 1, 1);
        if (last_pos != '.') {
            if (!isNaN(numInput) && numInput > 0) {
                if (last_pos == '0') {
                    return (numInput);
                } else {
                    return (numRound(numInput, numDecimals));
                }
            } else {
                if (acceptZeo) {
                    return 0;
                } else {
                    return '';
                }
            }
        }
        return x;
    }
});


/*******  CONVERT INPUT IN DECIMAL AND INTEGER *********/
let DecimalInput = (function (input_id, round_digits) {
    if (typeof (round_digits) === 'undefined') round_digits = 2;
    x = DecimalFormat($("#" + input_id).val().toString());
    if (x == '') {
        $("#" + input_id).val('');
    } else {
        $("#" + input_id).val(numCommas(numRound(x, round_digits)));
    }
});

/**
 * CONVERT INPUT VALUE IN CURRENCY FORMAT.
 *
 * @param {String} input_id
 * @param {Number} round_digits
 * @param {String} currency_char
*/
let CurrencyInput = (function (input_id, round_digits = 2, currency_char = "$") {
    x = DecimalFormat($("#" + input_id).val().toString());
    if (x == '') {
        $("#" + input_id).val('');
    } else {
        $("#" + input_id).val(currency_char + ' ' + numCommas(numRound(x, round_digits)));
    }
});

/******** CONVERT INPUT VALUE TO PERCENTAGE ********/
let PercentInput = (function (input_id, round_digits) {
    if (typeof (round_digits) === 'undefined') round_digits = 2;
    x = DecimalFormat($("#" + input_id).val().toString());
    if (x == '') {
        $("#" + input_id).val('');
    } else {
        $("#" + input_id).val(numCommas(numRound(x, round_digits)) + ' %');
    }
});

let DecimalFormat = (function (n) {
    x = n.toString().replace(/[^0-9\.\-]/g, "").split(".");
    x1 = x[0];
    x2 = x[1];
    var xR;
    if (isNaN(x1)) {
        xR = '';
    } else {
        if (isNaN(x2)) {
            xR = Number(x1);
        } else {
            xR = Number(x1 + "." + x2);
        }
    }
    return xR;
});

// SEARCH BARCODE IN DATABASE 
let SearchBarCodeExt = (function (barcode) {
    var defer = $.Deferred();
    $.ajax({
        url: "search_barcode_ext.php",
        dataType: "json",
        data: ({ barcode: barcode }),
        type: "POST",
        success: function (r) {
            defer.resolve(r);
        }
    });
    return defer.promise();
});

/**
 * Check if account is restaurant/bar/...
 * 
 * @return {boolean} return.value : True if is restaurant.
 * @return {string} return.industry : Type of industry.
 * 
 * @example
 * isRestaurant().then(function(r){console.log(r)})
 * > {value: true, industry: "13"}
 */
let isRestaurant = (function () {
    var defer = $.Deferred();
    $.ajax({
        url: 'get_session_vars.php',
        dataType: 'json',
        data: ({ var1: '1' }),
        type: 'POST',
        success: function (r) {
            if (r["bus_industry"] == "13"
                || r["bus_industry"] == "2"
                || r["bus_industry"] == "37"
                || r["bus_industry"] == "5"
                || r["bus_industry"] == "49") {
                r2 = { "value": true, "industry": r["bus_industry"] };
            } else {
                r2 = { "value": false, "industry": r["bus_industry"] };
            }
            defer.resolve(r2);
        }
    });
    return defer.promise();
});

// 
/**
 * Obtener el objecto JSON Array con todas las adiciones.
 * 
 * @example
 * GetAdiciones().then(function(r){console.log(r)});
 * > [{prod_id: "ADO-2483", 
 *     prod_name: "Aceite de oliva", 
 *     prod_unit_price: "0.00", 
 *     prod_unit_cost: "150.00",
 *     prod_units: "Unidades", 
 *     prod_icon: "../icons/SVG/74-food/sugar-icing.svg",
 *     tax: "0"}]
 */
let GetAdiciones = (function () {
    var defer = $.Deferred();
    $.ajax({
        url: 'get_adiciones.php',
        dataType: 'json',
        data: ({ var1: '1' }),
        type: 'POST',
        success: function (r) {
            defer.resolve(r);
        }
    });
    return defer.promise();
});

/**** INICIAR EL MODAL QUE MUESTRA LA LISTA DE ITEMS DADO UN NUMERO DE CUENTA ********/
let IniciateItemsBill = (function (idBill) {
    let mr = new MainRoller();
    mr.startMainRoller();
    $.ajax({
        url: "get_bill.php",
        dataType: "json",
        data: ({ idBill: idBill }),
        type: "POST",
        success: function (r) {
            mr.stopMainRoller();
            $("#items_bill_wrap").html('');
            var htxt = '';

            if (r.length > 0) {
                for (var i = 0; i < r.length; i++) {
                    htxt += '<div id="ib_' + r[i]["item_id"] + '" class="item_bill_row">';
                    htxt += '<div class="item_bill_icon"><img src="' + r[i]["prod_icon"] + '"></div>';
                    htxt += '<div class="item_bill_data">';

                    htxt += '<p id="ib_id" class="ib_id">' + r[i]["item_id"] + '</p>';
                    htxt += '<p id="ib_name" class="ib_name">' + r[i]["prod_name"] + '</p>';
                    htxt += '<div class="ibd_c1"><div class="ibd_c11"><span id="ib_cnt">' + r[i]["item_count"] + '</span><span id="ib_unt"> ' + r[i]["prod_units"] + '</span></div><div class="ibd_c12">$ <span id="ib_price">' + numCommas(numRound(r[i]["item_value"], 0)) + '</span></div>';
                    if (r[i]["item_discount"] != '0' && r[i]["item_discount"] != '') {
                        htxt += '<div class="ibd_c13" title="$ ' + numCommas(numRound(r[i]["item_discount"], 0)) + '"><span class="mdi mdi-brightness-percent"></span></div>';
                    }
                    if (r[i]["item_note"] != '') {
                        htxt += '<div class="ibd_c13" title="' + r[i]["item_note"] + '"><span class="mdi mdi-bookmark-outline"></span></div>';
                    }
                    htxt += '</div></div></div>';
                }
                $("#items_bill_wrap").append(htxt);
            }

            OpenItemsBill();

        }
    });
});

/**
 * Send specific invoice by email.
 * 
 * @param {string} id_bill Id of bill
 * @param {numeric} type_of_mail Type of mail to be sent.
 * - 1 : First time bill
 * - 2 : Re send bill
 * 
 * @example
 * SendInvoiceByIdBill("ad233Xad12", 1);  
 */
let SendInvoiceByIdBill = (function (id_bill, type_of_mail) {
    if (id_bill == '') {
        alertify.alert("Error #39284", "Error enviando el correo.");
        return false;
    }
    type_of_mail = (type_of_mail == undefined) ? 1 : type_of_mail;

    let mr = new MainRoller();
    mr.startMainRoller();

    $.ajax({
        url: "send_invoice_by_bill.php",
        dataType: "json",
        data: ({ id_bill: id_bill, type_of_mail: type_of_mail }),
        type: "POST",
        success: function (r) {
            mr.stopMainRoller();
            if (r[0][0] == "S" && r[1][0] == true) {
                if (type_of_mail == 1) {
                    alertify.message("Email enviado con éxito");
                } else if (type_of_mail == 2) {
                    alertify.message("Email reenviado con éxito");
                }
            } else {
                alertify.alert("ERROR #139382", "Error enviando el correo.");
                return false;
            }
        }
    });
});

/**
 * Open new tab with the Invoice of a given bill.
 * 
 * @param {string} id_bill Id of bill. 
 * 
 * @example
 * PrintInvoiceByIdBill("kxzEl0VzvTmJ")
 */
let PrintInvoiceByIdBill = (function (id_bill) {
    window.open("https://www.2luca.co/php/bill_format.php?id_bill=" + id_bill);
});

let getCurrentPrinter = (function (idObject) {
    $.ajax({
        url: 'get_user_printer.php',
        dataType: 'json',
        data: ({ var1: '1' }),
        type: 'POST',
        success: function (r) {
            if (r.length > 0) {
                $("#" + idObject).val(r[0]["printer_id"]);
            }
        }
    });
});

let updatePrinter = (function (printer_id) {
    $.ajax({
        url: 'update_printers.php',
        dataType: 'json',
        data: ({ printer_id }),
        type: 'POST',
        success: function (r) {
            if (r[0][0] == "S") {
                alertify.message("Impresora actualizada correctamente.");
            } else {
                alertify.message("Error actualizando impresora.");
            }
        }
    });
});

let loadPrinters = (function (idObject) {
    $.ajax({
        url: 'get_all_printers.php',
        dataType: 'json',
        data: ({ var1: '1' }),
        type: 'POST',
        success: function (r) {
            $("#" + idObject).html('');
            var html = '<option value="0" selected>Ninguna</option>';
            if (r.length > 0) {
                for (var i = 0; i < r.length; i++) {
                    html += '<option value="' + r[i]["printer_id"] + '">' + r[i]["printer_name"] + '</option>';
                }
            }
            $("#" + idObject).append(html);
        }
    });
});

let loadCountries = (function (idObject) {
    $.ajax({
        url: "get_all_countries.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            $("#" + idObject).html('');
            var html = '<option value="" selected>Selecciona un país</option>';
            if (r.length > 0) {
                for (var i = 0; i < r.length; i++) {
                    html += '<option value="' + r[i]["country_id"] + '">' + r[i]["country_name"] + '</option>';
                }
            }
            $("#" + idObject).append(html);
        }
    });
});

/**
 * FORMAT CREDIT CARD NUMBER
 * 
 * @param {number} value Credit card number
 * 
 * @example
 * cc_format(4575623182290326)
 * > "4575 6231 8229 0326"
 */
let cc_format = function (value) {
    let v = String(value).replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    let matches = v.match(/\d{4,16}/g);
    let match = matches && matches[0] || '';
    let parts = [];

    for (i = 0, len = match.length; i < len; i += 4) {
        parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
        return parts.join(' ');
    } else {
        return value;
    }
}

/**
 * Get the name and the icon of the franchise of a bank card number.
 * 
 * @param {numeric} card_number Credit card number
 * 
 * @return {string} return.**name** : Name of the franchise.
 * @return {string} return.**iconURL** : URL of the .svg icon.
 * 
 * @example
 * getCreditCardType(4575623182290326)
 * > {name: "visa", iconURL: "https://www.2luca.co/icons/SVG/44-money/credit-card-visa.svg"}
 */
let getCreditCardType = function (card_number) {
    card_number = String(card_number).replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    let result = { name: "na", iconURL: "https://www.2luca.co/icons/SVG/44-money/credit-card.svg" };
    if (/^5[1-5]/.test(card_number)) {
        result = { name: "mastercard", iconURL: "https://www.2luca.co/icons/SVG/44-money/credit-card-master-card.svg" };
    } else if (/^4/.test(card_number)) {
        result = { name: "visa", iconURL: "https://www.2luca.co/icons/SVG/44-money/credit-card-visa.svg" };
    } else if (/^3[47]/.test(card_number)) {
        result = { name: "amex", iconURL: "https://www.2luca.co/icons/SVG/44-money/credit-card-amex.svg" };
    }
    return result;
}

/**
 * Validate the last number of credit cards using the Luhn algorithm.
 * 
 * Devuelve true o false si es la TDC o no valida.
 * 
 * @param {*} cc_number Credit card number of 16 digits.
 * 
 * @example
 * validateCreditCard("4575623182290326")
 * > true
 */
let validateCreditCard = function (cc_number) {
    // The Luhn Algorithm
    let cc = cc_number;
    let sumx2 = 0;
    let sumx1 = 0;
    let x = 0;
    let y, y2;
    let id = parseInt(cc.substring(cc.length - 1));
    let cc_s = cc.substring(0, cc.length - 1);

    for (let i = 0; i < cc_s.length; i++) {
        if (x == 0) {
            y = (parseInt(cc[i]) * 2).toString();
            if (y.length > 1) {
                y2 = 0;
                for (var j = 0; j < y.length; j++) {
                    y2 += parseInt(y[j]);
                }
                sumx2 = sumx2 + parseInt(y2);
            } else {
                sumx2 = sumx2 + parseInt(y);
            }
            x = 1;
        } else {
            sumx1 = sumx1 + parseInt(cc[i]);
            x = 0;
        }
    }
    id_test = Math.ceil((sumx1 + sumx2) / 10) * 10 - (sumx1 + sumx2);
    return id_test == id;

}

/*****************************/
/***** Profile Session *******/
/*****************************/

/**
 * Check if a profile session is currently active.
 * 
 * @return {boolean} True if is active false if not.
 */
function SessionActive() { return !(Cookies.get("PROFSESSID") == null); }

/**
 * Remove all profile session cookies.
 */
function DeleteSessionProfile() {
    Cookies.remove('PROFSESSID');
    Cookies.remove('cp_avatar');
    Cookies.remove('cp_name');
}

/**
 * Check if the current active profile is administratior or not.
 * 
 * @return 1 if is admin 0 if not.
 * 
 * @example
 * ProfileIsAdmin().then((e)=>console.log(e))
 * > 0
 */
function ProfileIsAdmin() {
    let prof_key = GetVariableCookie('PROFSESSID');

    if (prof_key != null) {
        let defer = $.Deferred();
        $.ajax({
            url: "check_if_admin.php",
            dataType: 'json',
            data: ({ prof_key: prof_key }),
            type: "POST",
            success: function (r) {
                defer.resolve(r);
            }
        });
        return defer.promise();
    }
}

/**
 * Verify if current active profile has permissions for the action id.
 * 
 * @param {string} id Id of type of permission. 
 * 
 * @return 
 * - 1 - Has permission
 * - 0 - Don't have permission
 * - 2 - Error
 * 
 * @example
 * HasPermission("bills_1").then((e)=>console.log(e))
 * > 1
 */
function HasPermission(id) {
    let prof_key = GetVariableCookie('PROFSESSID');
    let defer = $.Deferred();
    $.ajax({
        url: "check_permission.php",
        dataType: 'json',
        data: ({ prof_key: prof_key, perm_type: id }),
        type: "POST",
        success: function (r) {
            defer.resolve(r);
        }
    });
    return defer.promise();
}

/******** ANALYTICS CARDS ******/

function selectBadge(id, txtDesc) {
    $("#badges_cust>div").removeClass('badge_selected');
    $("#" + id).addClass('badge_selected');
    $("#descr_badge").removeClass("hide");
    $("#descr_badge>p").html(txtDesc);
}