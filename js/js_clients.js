var actFilters = new filtersOrders();
// Edit customer modal
let editCustObj = new editCustomerObj(() => { RefreshCustomers(); })

var salesman_v = [];
let customers2;

/*************************************************/
/******************* CLASSES *********************/
/*************************************************/

class barChart {
    /**
     * Bar Plot Chart with personalized design.
     * 
     * @param {string} id_target Id of the target element in the DOM where the chart will be placed.
     * @param {object} data Array or list with the data points for the barchart.
     * @param {object} labels Array or list with the labels for the data.
     * @param {object} tooltips Array or list with the tooltip text for each point. (Optional)
     * @param {boolean} format_bignum True if want to rescale the big numbers. Ex: 1000 -> 1K
     * @param {integer} bar_wide Scale of wide for each bar. (Optional)
     * @param {strind} bcolor Background color for the bar. (Optional) - "default", "luca"
     * 
     * @example
     * var bChart = new barChart("barChart-1", chart_data, short_lbls, lbls_format, false);
     */
    constructor(id_target, data, labels, tooltips = [], format_bignum = false, bar_wide = 15, bcolor = 'default') {
        this.id_target = id_target;
        this.data = data;
        this.labels = labels;
        this.tooltips = tooltips;
        this.bar_wide = bar_wide;
        this.bcolor = bcolor;
        this.format_bignum = format_bignum;

        this.num_bars = this.data.length;

        if (this.num_bars > 0) {
            this.sum_data = this.data.sumar();
            this.max_data = this.data.max();
            this.normalize_data();
            this.build();
            this.set_data();
        } else {
            this.build();
        }
    }

    normalize_data() {
        this.norm_data = [];
        if (this.max_data > 0) {
            for (var i = 0; i < this.num_bars; i++) {
                this.norm_data.push(this.data[i] / this.max_data);
            }
        }
    }

    build() {
        $('#' + this.id_target).html('');
        let textHTML = '';
        var id_temp = '';
        this.id_list = [];
        textHTML += '<div class="chart_bar">';
        for (var i = 0; i < this.num_bars; i++) {
            id_temp = this.id_target + '_bar' + i;
            this.id_list.push(id_temp);
            textHTML += '<div id="' + id_temp + '" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar ch_bar_bcolor_' + this.bcolor + ' w' + this.bar_wide + '"></div></div><div class="ch_bar_lbl"></div></div>';
        }
        textHTML += '</div>';
        $('#' + this.id_target).append(textHTML);
    }

    reset() {
        for (var i = 0; i < this.num_bars; i++) {
            this.setValueCharBar(this.id_list[i], "0", 0, '');
        }
    }

    set_data() {
        this.reset();
        for (var i = 0; i < this.num_bars; i++) {
            var tooltip_temp = (this.tooltips.length > 0) ? this.tooltips[i] : '';
            this.setValueCharBar(this.id_list[i], this.data[i], this.norm_data[i], this.labels[i], tooltip_temp)
        }
    }

    update(newdata, newlabels = [], newtooltip = []) {
        this.data = newdata;
        this.labels = (newlabels.length > 0) ? newlabels : this.labels;
        this.tooltips = (newtooltip.length > 0) ? newtooltip : this.tooltips;

        this.num_bars = this.data.length;
        this.sum_data = this.data.sumar();

        this.normalize_data();
        this.build();
        this.set_data();
    }

    setValueCharBar(id_bar, val, valPer, lbl, tooltip = "") {
        var val_txt;
        $("#" + id_bar + ">.ch_bar_wrap>.ch_bar").css("top", "calc(100px - " + valPer * 100 + "px)");
        if (this.format_bignum && val >= 1000) {
            val_txt = formatBigNumber(val, 0).replace(" ", "");
        } else {
            val_txt = val
        }
        $("#" + id_bar + ">.ch_bar_wrap>.ch_bar").attr('data-content', val_txt);
        if (tooltip != "") {
            $("#" + id_bar + ">.ch_bar_wrap>.ch_bar").attr('data-content-tooltip', numCommas(val) + " - " + tooltip);
        }
        $("#" + id_bar + ">.ch_bar_lbl").text(lbl);
    }
}

/** CLIENTE: **/

class Customer {

    constructor(cust_key) {
        this.key = cust_key;
        this.createCustomer();
        this.loadOrders();
        this.pushLastOrdersCust();
        // Get Analytics Data
    }

    createCustomer() {
        let cust_info = customers2.filter((el) => el.cust_key == this.key)[0];
        if (cust_info != undefined) {
            this.points = numRound(cust_info["cust_points"], 0)
            this.bdate = moment(cust_info["cust_bdate"].substring(0, 10));
            this.date_register = moment(cust_info["cust_date_register"]);
            this.email = cust_info.cust_email;
            this.gender = cust_info.cust_gender;
            this.gender_txt = this.textGender(cust_info.cust_gender);
            this.id = cust_info.cust_id;
            this.name = cust_info.cust_name;
            this.phone = cust_info.cust_phone;
            this.type_id = cust_info.cust_type_id;
            this.type_id_txt = this.textDocument(cust_info.cust_type_id);
            this.since = this.date_register.fromNow();
            let shortName = '';
            try {
                shortName = nameHandler(this.name)[1]["shortFullName"];
            } catch (error) {
                shortName = '';
            }
            if (shortName == '') { shortName = this.name }
            this.shortName = shortName;
        }
    }

    textGender(gender_id) {
        if (gender_id == "1") {
            return "Hombre";
        } else if (gender_id == "2") {
            return "Mujer";
        } else {
            return "Otro";
        }
    }

    textDocument(doc_id) {
        if (doc_id == '1') {
            return "Cédula Ciudadania";
        } else if (doc_id == '2') {
            return "Cédula Extranjería";
        } else if (doc_id == '3') {
            return "Pasaporte";
        } else if (doc_id == '4') {
            return "Tarjeta de Identidad";
        } else if (doc_id == '5') {
            return "Registro Civil";
        } else if (doc_id == '6') {
            return "Otro";
        } else {
            return "ID";
        }
    }

    /** Function that gets the recomendations for the customer and open the modal window. **/
    getRecomendations(showMsgNoRecom, msgnoaccess) {
        msgnoaccess = (msgnoaccess == undefined) ? true : msgnoaccess;
        showMsgNoRecom = (showMsgNoRecom == undefined) ? true : showMsgNoRecom;

        let bl = new BarLoading(15000, "Cargando Recomendaciones");
        if (msgnoaccess) {
            bl.start();
        }
        let me = this;

        CheckPkgAccess(1).then(function (cpa) {
            checkFreeTrail(1).then(function (cft) {
                if (!msgnoaccess) {
                    bl.stop();
                }
                if (cpa || cft.isactive) {
                    if (cft.isactive && msgnoaccess && cft.days_to_end <= 3 && !cpa) {
                        if (cft.days_to_end >= 2) {
                            alertify.message("Tu período de prueba caduca en " + cft.days_to_end + " días.");
                        } else if (cft.days_to_end == 1) {
                            alertify.message("Tu período de prueba caduca mañana.");
                        } else if (cft.days_to_end == 0) {
                            alertify.message("Tu período de prueba caduca hoy.");
                        }
                    }
                    $("#itemrecom_description").html('');
                    $("#itemrecom_items").html('');
                    $.ajax({
                        url: "pull_client_based_recom.php",
                        dataType: "json",
                        data: ({ cust_key: me.key }),
                        type: "POST",
                        success: function (r) {
                            bl.stop();
                            if (r.length > 0 && r != null) {
                                let description = '';
                                if (r.length == 1) {
                                    description = "Hemos encontrado el siguiente ítem para ofrecerle a <b>" + me.name + "</b>. Las recomendaciones están basadas en las preferencias de compra del cliente. Conoce más <a href='https://medium.com/blog-2luca' target='_blank'>aquí</a>.";
                                } else {
                                    description = "Hemos encontrado los siguientes ítems para ofrecerle a <b>" + me.name + "</b>. Las recomendaciones están basadas en las preferencias de compra del cliente. Conoce más <a href='https://medium.com/blog-2luca' target='_blank'>aquí</a>.";
                                }
                                $("#itemrecom_description").html(description);
                                let htmlItemsRecom = '';
                                for (let i = 0; i < r.length; i++) {
                                    var item_id = r[i]['item_id'];
                                    var item_name = r[i]['prod_name'];
                                    var item_icon = r[i]['prod_icon'];
                                    if (item_id != null && item_id != "") {
                                        htmlItemsRecom += '<div class="msg-box-1-optionbox"><img src="' + item_icon + '"><p>' + item_name + '<br>(' + item_id + ')</p></div>';
                                    }
                                }
                                $("#itemrecom_items").html(htmlItemsRecom);
                                OpenItemRecom();
                            } else {
                                if (showMsgNoRecom == 1) {
                                    alertify.message("No hay recomendaciones para " + me.name + '.');
                                }
                            }
                        }
                    });
                } else {
                    bl.stop();
                    if (msgnoaccess) {
                        alertify.alert('', 'No tienes acceso al paquete de analítica.');
                    }
                }
            });
        });
    }

    delete(on_delete) {
        let me = this;
        alertify.confirm('Borrar Cliente', "¿Seguro desea eliminar el cliente?. Esta acción es irreversible.", function () {
            $.ajax({
                url: "dropCustomer.php",
                data: ({ cust_key: me.key }),
                type: "POST",
                success: function (c) {
                    if (c == '1') {
                        alertify.message("Cliente eliminado correctamente");
                        on_delete();
                    } else {
                        alertify.alert("", "No fue posible eliminar el cliente. Intenta de nuevo.");
                    }
                }
            });
        }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
    }

    loadOrders() {
        let me = this;

        /**
         * Update the Analytics Cards
         * @param {string} cust_key Key of customer
         */
        let updateAnalytics = function (cust_key, shortName) {

            CheckPkgAccess(1).then(function (cpa) {
                checkFreeTrail(1).then(function (cft) {
                    if (cpa || cft.isactive) {

                        $.ajax({
                            url: "get_cust_analytics.php",
                            dataType: "json",
                            data: ({ cust_key: cust_key }),
                            type: "POST",
                            success: function (r) {

                                if (r.length > 0) {
                                    // Update Segments and NBA
                                    if (r[0] != undefined) {
                                        $('#anl_seg').removeClass('hide');
                                        $('#anl_nba').removeClass('hide');

                                        $("#segm_cust_box").text(r[0]['segment']);
                                        $("#segm_cust_desc").text(shortName + ' ' + r[0]['description']);
                                        $('#segm_cut_icon').attr('src', r[0]['img_path']);
                                        $("#segm_cont").css({ 'color': r[0]['text_color'], 'background': r[0]['backgrnd_color'] });
                                        $("#nba_cust_txt").text(r[0]['nba']);
                                        $("#nba_icon").attr('src', r[0]['nbg_img_path']);
                                        $("#nba_cont").css({ 'color': r[0]['text_color'] });
                                    }

                                    // Update Insignias
                                    if (r[1] != undefined && r[2] != undefined) {

                                        let limit_b_tips = 2;
                                        let limit_b_prsh = 2;
                                        let limit_b_inco = 1.5;
                                        let b_tips = 0;
                                        let b_prsh = 0;
                                        let b_inco = 0;

                                        if (parseFloat(r[1]["tips"]) / parseFloat(r[2]["TIPS_AVG"]) > limit_b_tips) {
                                            b_tips = 1;
                                            let b_tips_val = numRound(((parseFloat(r[1]["tips"]) / parseFloat(r[2]["TIPS_AVG"]))), 1);
                                            let b_tips_txt = shortName + ' deja <b>' + b_tips_val + ' veces más propinas</b> que el promedio de las compras.';
                                            $("#badges_cust").append('<div id="badge_tip" onclick="selectBadge(&#39;badge_tip&#39;,&#39;' + b_tips_txt + '&#39;)"><img src="../icons/general/badge_tip.svg"><p>Buenas Propinas</p></div>');
                                        }
                                        if (parseFloat(r[1]["ventas"]) / parseFloat(r[2]["N_VIST_AVG_CUST"]) > limit_b_prsh) {
                                            b_prsh = 1;
                                            let b_prsh_val = numRound(((parseFloat(r[1]["ventas"]) / parseFloat(r[2]["N_VIST_AVG_CUST"]))), 1);
                                            let b_prsh_txt = shortName + ' <b>visita tu negocio ' + b_prsh_val + ' veces más</b> que el promedio de tus clientes.';
                                            $("#badges_cust").append('<div id="badge_pursh" onclick="selectBadge(&#39;badge_pursh&#39;,&#39;' + b_prsh_txt + '&#39;)"><img src="../icons/general/badge_pursh.svg"><p>Comprador Estrella</p></div>');
                                        }
                                        if (parseFloat(r[1]["ingresos"]) / parseFloat(r[2]["INC_AVG_CUST"]) > limit_b_inco) {
                                            b_inco = 1;
                                            let b_inco_val = numRound(((parseFloat(r[1]["ingresos"]) / parseFloat(r[2]["INC_AVG_CUST"]))), 1);
                                            let b_inco_txt = shortName + ' te deja <b>' + b_inco_val + ' veces más ingresos</b> que el promedio de tus clientes.';
                                            $("#badges_cust").append('<div id="badge_income" onclick="selectBadge(&#39;badge_income&#39;,&#39;' + b_inco_txt + '&#39;)"><img src="../icons/general/badge_income.svg"><p>Alto Valor</p></div>');
                                        }

                                        let b_tot = b_tips + b_prsh + b_inco;
                                        if (b_tot > 0) { $('#anl_insg').removeClass('hide'); }

                                    }
                                }
                            }
                        });

                    } else {
                        $('#anl_seg').html('');
                        $('#anl_nba').html('');
                        $('#anl_insg').html('');
                        $('#anl_seg').addClass('hide');
                        $('#anl_nba').addClass('hide');
                        $('#anl_insg').addClass('hide');
                    }
                })
            });
        }

        /**
         * Update the card of payment types in customer page.
         * @param {object} data Transactional json array object. Ex: [{},{},...]
         * @param {string} type Type of statistic. Could be: "tot_sales", "tot_sales_value", "per_sales" or "per_sales_val".
         */
        let updatePaymentCard = function (data, type) {

            let dateToday = moment();
            let dateAllHistory = moment().subtract(50, 'year');

            let dataReport = reportOfSales(data, dateAllHistory, dateToday);

            if (type == 'per_sale' || type == 'per_sales_val') {
                $("#tp_efectivo").text("0%");
                $("#tp_credito").text("0%");
                $("#tp_debito").text("0%");
                $("#tp_otro").text("0%");
            } else {
                $("#tp_efectivo").text("0");
                $("#tp_credito").text("0");
                $("#tp_debito").text("0");
                $("#tp_otro").text("0");
            }

            // Update Tipo de Pago
            let paymetList = Object.keys(dataReport.by_payment);
            for (var i = 0; i < paymetList.length; i++) {

                if (type == 'per_sales') {
                    var per_val = parseInt(dataReport.by_payment[paymetList[i]].per_sales * 100) + "%";
                } else if (type == 'per_sales_val') {
                    var per_val = parseInt(dataReport.by_payment[paymetList[i]].per_sales_val * 100) + "%";
                } else if (type == 'tot_sales') {
                    var per_val = numCommas(parseInt(dataReport.by_payment[paymetList[i]].tot_sales));
                } else if (type == 'per_sales_val') {
                    var per_val = "$" + numCommas(parseInt(dataReport.by_payment[paymetList[i]].per_sales_val));
                } else {
                    var per_val = parseInt(dataReport.by_payment[paymetList[i]].per_sales * 100) + "%";
                }

                if (paymetList[i] == "1") {
                    $("#tp_efectivo").text(per_val);
                } else if (paymetList[i] == "2") {
                    $("#tp_credito").text(per_val);
                } else if (paymetList[i] == "3") {
                    $("#tp_debito").text(per_val);
                } else if (paymetList[i] == "4") {
                    $("#tp_otro").text(per_val);
                }
            }
        }

        /**
         * Update Charts Manager Function
         * @param {object} data Array with transactions data. Ex: [{},{},...]
         * @param {string} type_freq Type of frequency. Can be 'last_year', 'last_week', 'last_month'
         * @param {string} chart_to_update Id of the chart to update, or use 'all' if want to update them 'all
         */
        let updateCharts = function (data, type_freq, chart_to_update = 'all') {

            let dateToday = moment();
            let dateOneYearAgo = moment().subtract(12, 'months');
            let dateOneMonthAgo = moment().subtract(1, 'months');
            let dateOneWeekAgo = moment().subtract(7, 'days');
            let dataReport;

            if (type_freq == 'last_year') {
                dataReport = reportOfSales(data, dateOneYearAgo, dateToday, "MMMM", 'months', 0);
            } else if (type_freq == 'last_week') {
                dataReport = reportOfSales(data, dateOneWeekAgo, dateToday, "dddd", 'days', 0);
            } else if (type_freq == 'last_month') {
                dataReport = reportOfSales(data, dateOneMonthAgo, dateToday, "DD", 'days', 0);
            }

            if (dataReport) {
                // Update Chart Visitas Promedio.
                if (chart_to_update == 'barChart-1' || chart_to_update == 'all') {
                    $("#sales_prom_indc").text("0");
                    $("#sales_prom_inc_name").text("visitas");
                    if ("by_frequency" in dataReport) {
                        var num_sales = numRound(dataReport.by_frequency.avg_by_freq.sales, 0);
                        var lbls = dataReport.by_frequency.fill_labels;
                        var lbls_format = dataReport.by_frequency.fill_labels_format;
                        lbls = lbls.map((el) => el.capitalize());
                        var short_lbls = lbls.map((el) => el.substring(0, 3));
                        var chart_data = dataReport.by_frequency.fill_totals.sales;
                        $("#sales_prom_indc").text(num_sales);
                        var bChart = new barChart("barChart-1", chart_data, short_lbls, lbls_format, false);

                        var txtLbl_1 = (num_sales == 1) ? 'visita' : 'visitas';
                        if (type_freq == 'last_year') {
                            $("#sales_prom_inc_name").text(txtLbl_1 + " al mes");
                        } else if (type_freq == 'last_week') {
                            $("#sales_prom_inc_name").text(txtLbl_1 + " al día");
                        } else if (type_freq == 'last_month') {
                            $("#sales_prom_inc_name").text(txtLbl_1 + " al día");
                        }
                    } else {
                        new barChart("barChart-1", [], []);
                    }
                }

                // Update Gasto Promedio Chart
                if (chart_to_update == 'barChart-2' || chart_to_update == 'all') {
                    $("#gasto_prom_indc").text("0");
                    if ("by_frequency" in dataReport) {
                        var sales_value = numRound(dataReport.by_frequency.avg_by_freq.sales_value, 0);
                        var lbls = dataReport.by_frequency.fill_labels;
                        var lbls_format = dataReport.by_frequency.fill_labels_format;
                        lbls = lbls.map((el) => el.capitalize());
                        var short_lbls = lbls.map((el) => el.substring(0, 3));
                        var chart_data = dataReport.by_frequency.fill_totals.sales_value;

                        $("#gasto_prom_indc").text("$ " + numCommas(sales_value));
                        var bChart2 = new barChart("barChart-2", chart_data, short_lbls, lbls_format, true);
                        if (type_freq == 'last_year') {
                            $("#gasto_prom_ind_name").text("al mes");
                        } else if (type_freq == 'last_week') {
                            $("#gasto_prom_ind_name").text("al día");
                        } else if (type_freq == 'last_month') {
                            $("#gasto_prom_ind_name").text("al día");
                        }
                    } else {
                        new barChart("barChart-2", [], []);
                    }
                }

            }

        }

        // Reset Elements
        reset_elements();

        $.ajax({
            url: "get_all_orders_by_cons.php",
            dataType: "json",
            data: ({ cust_key: this.key }),
            type: "POST",
            success: function (r) {
                let icn_payt_ty = '';
                let cod = '';
                let TotItems = 0;
                let TotVentas = 0;
                let TotIngreso = 0;

                if (r.length > 0) {

                    me.all_bills = r;

                    // ----- Update Cards -------                                        

                    updateCharts(me.all_bills, 'last_year', 'all');
                    updatePaymentCard(me.all_bills, 'per_sales');
                    updateAnalytics(me.key, me.shortName);

                    // ---- Update Click Events ------
                    $("#pay_perc_op").off('click').click(() => {
                        $('#pay_perc_op').addClass('card-options-active');
                        $('#pay_value_op').removeClass('card-options-active');
                        updatePaymentCard(me.all_bills, 'per_sales');
                    });

                    $("#pay_value_op").off('click').click(() => {
                        $('#pay_perc_op').removeClass('card-options-active');
                        $('#pay_value_op').addClass('card-options-active');
                        updatePaymentCard(me.all_bills, 'tot_sales');
                    });

                    $("#last_y_ch1").off('click').click(() => {
                        $('#last_y_ch1').addClass('card-options-active');
                        $('#last_m_ch1').removeClass('card-options-active');
                        $('#last_w_ch1').removeClass('card-options-active');
                        updateCharts(me.all_bills, 'last_year', 'barChart-1');
                    });

                    $("#last_m_ch1").off('click').click(() => {
                        $('#last_y_ch1').removeClass('card-options-active');
                        $('#last_m_ch1').addClass('card-options-active');
                        $('#last_w_ch1').removeClass('card-options-active');
                        updateCharts(me.all_bills, 'last_month', 'barChart-1');
                    });

                    $("#last_w_ch1").off('click').click(() => {
                        $('#last_y_ch1').removeClass('card-options-active');
                        $('#last_m_ch1').removeClass('card-options-active');
                        $('#last_w_ch1').addClass('card-options-active');
                        updateCharts(me.all_bills, 'last_week', 'barChart-1');
                    });

                    // ---- Update Gasto Promedio Chart 
                    $("#last_y_ch2").off('click').click(() => {
                        $('#last_y_ch2').addClass('card-options-active');
                        $('#last_m_ch2').removeClass('card-options-active');
                        $('#last_w_ch2').removeClass('card-options-active');
                        updateCharts(me.all_bills, 'last_year', 'barChart-2');
                    });

                    $("#last_m_ch2").off('click').click(() => {
                        $('#last_y_ch2').removeClass('card-options-active');
                        $('#last_m_ch2').addClass('card-options-active');
                        $('#last_w_ch2').removeClass('card-options-active');
                        updateCharts(me.all_bills, 'last_month', 'barChart-2');
                    });

                    $("#last_w_ch2").off('click').click(() => {
                        $('#last_y_ch2').removeClass('card-options-active');
                        $('#last_m_ch2').removeClass('card-options-active');
                        $('#last_w_ch2').addClass('card-options-active');
                        updateCharts(me.all_bills, 'last_week', 'barChart-2');
                    });

                    //---------------------

                    var billsIdList = [];
                    for (let i = 0; i < r.length; i++) {
                        var num_items = parseInt(r[i]['num_items']);
                        var trx_value = parseInt(r[i]['trx_value']);
                        var trx_date = moment(r[i]['trx_date']);
                        billsIdList.push(r[i]['id_bill']);

                        TotItems = TotItems + num_items;
                        TotVentas = TotVentas + 1;
                        TotIngreso = TotIngreso + trx_value;

                        cod = cod + '<div id="ord_' + i + '" class="con_bill_line">';
                        cod = cod + '<div><div class="textElli60">' + r[i]['id_bill'] + '</div></div><div>' + r[i]['trx_date'].substring(0, 10) + '</div><div>$ ' + numCommas(numRound(r[i]['trx_value'], 0)) + '</div>';
                        if (r[i]['payment_type'] == '1') {
                            icn_payt_ty = 'bank-notes-3.svg';
                        } else if (r[i]['payment_type'] == '2') {
                            icn_payt_ty = 'credit-card-visa.svg';
                        } else if (r[i]['payment_type'] == '3') {
                            icn_payt_ty = 'credit-card.svg';
                        } else if (r[i]['payment_type'] == '4') {
                            icn_payt_ty = 'wallet-3.svg';
                        } else {
                            icn_payt_ty = 'piggy-bank.svg';
                        }
                        cod = cod + '<div><img src="../icons/SVG/44-money/' + icn_payt_ty + '"></div></div>';
                        cod = cod + '<div id="ordaux_' + i + '" class="con_bill_aux"><div class="cba_row1">';
                        cod = cod + '<div><p>' + numCommas(r[i]['num_items']) + '</p><p>Ítems</p></div>';
                        cod = cod + '<div><p>$ ' + numCommas(numRound(r[i]['discount'], 0)) + '</p><p>Descuento</p></div>';
                        cod = cod + '<div><p>$ ' + numCommas(numRound(r[i]['taxes'], 0)) + '</p><p>Impuesto</p></div>';
                        cod = cod + '</div><div id="orddetails_' + i + '" class="cba_row2"><p>Ver detalles de la compra</p></div></div>';
                    }

                    let cod_summ = '';
                    cod_summ = cod_summ + '<div><p>' + TotVentas + '</p><p>Ventas</p></div>';
                    cod_summ = cod_summ + '<div><p>$ ' + numCommas(numRound(TotIngreso, 0)) + '</p><p>Ingresos</p></div>';
                    cod_summ = cod_summ + '<div><p>' + TotItems + '</p><p>ítems</p></div>';

                    $("#summ_bill_cons").append(cod_summ);
                    $("#allord_cont").append(cod);

                    // Assign card icons
                    $("#icn_sales").text(TotVentas);
                    $("#icn_sales_val").text("$ " + numCommas(numRound(TotIngreso, 0)));
                    $("#icn_sales_items").text(TotItems);

                    let toggleOrderAuxiliar = function (id) {
                        if ($("#" + id).height() > 0) {
                            $("#" + id).removeClass("con_bill_aux_opn");
                        } else {
                            $("#" + id).addClass("con_bill_aux_opn");
                        }
                    }
                    for (let i = 0; i < r.length; i++) {
                        // Assign toggle aux functions
                        $("#ord_" + i).off().click(() => { toggleOrderAuxiliar("ordaux_" + i) })
                        // Assign modal items list
                        $("#orddetails_" + i).off().click(() => { new itemsBillsModal(billsIdList[i], "item-bills-container", true) });
                    }
                }
            }
        });
    }

    pushLastOrdersCust() {

        let me = this;
        $("#cont_last_orders_cust").html('');
        let ConOrdHTML = '';
        $("#s3_a").show();

        $.ajax({
            url: "get_last_orders_by_cons.php",
            data: ({ cust_key: me.key }),
            type: "POST",
            dataType: "json",
            success: function (c) {
                if (c.length == 0) {
                    $("#s3_a").hide();
                    ConOrdHTML = ConOrdHTML + '<div class="ordersCons_empty">';
                    ConOrdHTML = ConOrdHTML + '<img src="../icons/SVG/41-shopping/basket-2.svg">';
                    ConOrdHTML = ConOrdHTML + '<p>El cliente no tiene compras</p>';
                    ConOrdHTML = ConOrdHTML + '</div>';
                } else {

                    for (let i = 0; i < c.length; i++) {
                        ConOrdHTML = ConOrdHTML + '<div class="pursh_line" onclick="new itemsBillsModal(&#39;' + c[i]["id_bill"] + '&#39;, &#39;item-bills-container&#39;, false)">';
                        ConOrdHTML = ConOrdHTML + '<div class="label_n_data br_lnd w20p">';
                        ConOrdHTML = ConOrdHTML + '<p>Orden</p><p class="bill_nc_data" title="' + c[i]['id_bill'] + '">' + c[i]['id_bill'] + '</p></div>';
                        ConOrdHTML = ConOrdHTML + '<div class="label_n_data br_lnd w40p">';
                        ConOrdHTML = ConOrdHTML + '<p>Fecha</p><p>' + c[i]['trx_date'].substring(0, 10) + '</p></div>';
                        ConOrdHTML = ConOrdHTML + '<div class="label_n_data w40p">';
                        ConOrdHTML = ConOrdHTML + '<p>Valor</p><p>$ <span id="valor_pursh_line">' + numCommas(numRound(c[i]['trx_value'], 0)) + '</span></p></div>';
                        ConOrdHTML = ConOrdHTML + '</div>';
                    }
                }
                $("#cont_last_orders_cust").append(ConOrdHTML);
            }
        });
    }
}

$(document).ready(function () {

    moment.locale("es");

    // New customer modal
    let newCustObj = new newCustomerObj(() => { RefreshCustomers() });

    // Inicializate Spinner and Rollers
    $('#LoadingRoller_filterOrders').hide();
    $('.bar_loading_cont').hide();

    // Populate Filter Salesman
    PopulateFilterSalesman();

    // Get all Orders
    RefreshCustomers();

    // Element Clicks
    $("#s3_a2").click(() => { OpenConsOrders_NoAsk() });

    /* MAX DATE  */
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("filter_date_from").setAttribute("max", today);
    $("#filter_date_to").val(today);
    document.getElementById("filter_date_to").setAttribute("max", today);
    $("#filter_date_to").val(today);

    $("#filter_date_sel").on('change', function () {
        var selection = this.value;
        var date_from, date_to;

        $("#filter_date_from_cont").hide();
        $("#filter_date_to_cont").hide();

        if (selection == "0") {
            date_from = "1910-01-01";
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 1) {
            date_from = moment().format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 2) {
            date_from = moment(moment().subtract(1, 'days')._d).format("YYYY-MM-DD");
            date_to = moment(moment().subtract(1, 'days')._d).format("YYYY-MM-DD");
        } else if (selection == 3) {
            date_from = moment(moment().subtract(7, 'days')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 4) {
            date_from = moment(moment().subtract(15, 'days')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 5) {
            date_from = moment(moment().subtract(1, 'months')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 6) {
            date_from = moment(moment().subtract(1, 'years')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 7) {
            date_from = moment().format("YYYY") + "-" + moment().format("MM") + "-01";
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 8) {
            date_from = moment().format("YYYY") + "-01-01"
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 9) {
            date_from = "";
            date_to = "";
            $("#filter_date_from").focus();
            $("#filter_date_from_cont").show();
            $("#filter_date_to_cont").show();
        } else {
            date_from = moment().format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        }

        $("#filter_date_from").val(date_from);
        $("#filter_date_to").val(date_to);

        actFilters.dateFrom = date_from;
        actFilters.dateTo = date_to;

    });

    $("#btnFilterOrders").click(function () {
        actFilters.dateFrom = $("#filter_date_from").val();
        actFilters.dateTo = $("#filter_date_to").val();
        if (actFilters.valorFrom == '') { actFilters.valorFrom = 0 };
        if (actFilters.valorTo == '') { actFilters.valorTo = 100000000 };
        if (actFilters.dateFrom == '') { actFilters.dateFrom = "1910-01-01" };
        if (actFilters.dateTo == '') { actFilters.dateTo = moment().format("YYYY-MM-DD") };

        if ($("#filter_val_min").val() != '') {
            actFilters.valorFrom = parseFloat($("#filter_val_min").val().toString().replace(/[^0-9.]/g, ""));
        }
        if ($("#filter_val_max").val() != '') {
            actFilters.valorTo = parseFloat($("#filter_val_max").val().toString().replace(/[^0-9.]/g, ""));
        }
        if ($("#filter_val_min").val() != '' && $("#filter_val_max").val() != '') {
            if (actFilters.valorFrom > actFilters.valorTo) {
                ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'El valor no puede ser mayor que el del campo: menor o igual.');
                ErrorInputBox('filter_val_min');
                return false;
            }
        }
        if ($("#filter_date_from").val() != '' && $("#filter_date_to").val() != '') {
            if (actFilters.dateFrom > actFilters.dateTo) {
                ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'La fecha inicial no puede ser mayor que la fecha final.');
                ErrorInputBox('filter_date_from');
                return false;
            }
        }
        if ($("#filter_date_from").val() == '' && $("#filter_date_sel").val() == '9') {
            ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'Ingresa el campo de fecha de incio');
            ErrorInputBox('filter_date_from');
            return false;
        }

        if ($("#filter_date_to").val() == '' && $("#filter_date_sel").val() == '9') {
            ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'Ingresa el campo de fecha final');
            ErrorInputBox('filter_date_to');
            return false;
        }
        $("#reset_filter_content").removeClass("hide");
        RefreshCustomers(true);
        CloseFilterBills();

    });

});

function RefreshCustomers(isFilter = false, specificCust = '') {

    let mr = new MainRoller();
    mr.startMainRoller();

    $.ajax({
        url: "get_customers.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            mr.stopMainRoller();
            $("#cont_table_data").html("");
            var textHTML = "";
            var tot_cons = 0;
            var cust_keys = [];
            var firstKey = '';

            if (r.length > 0) {
                let customers = r;
                customers2 = customers

                for (var i = 0; i <= r.length - 1; i++) {

                    cust_points = numRound(r[i]["cust_points"], 0);
                    cust_date_register = moment(r[i]["cust_date_register"].substring(0, 10));
                    cust_bdate = moment(r[i]["cust_bdate"].substring(0, 10));
                    cust_id = r[i]['cust_id'];
                    cust_type_id = numRound(r[i]["cust_type_id"], 0);
                    cust_gender = numRound(r[i]["cust_gender"], 0);
                    cust_name = r[i]['cust_name'];
                    cust_email = r[i]["cust_email"];
                    cust_phone = r[i]["cust_phone"];
                    cust_key = r[i]["cust_key"];
                    cust_keys.push(cust_key);
                    cust_since = cust_date_register.fromNow();

                    if (i == 0) { firstKey = cust_key }

                    // Apply Filters
                    if (isFilter) {
                        if (actFilters.salesman.indexOf(itm_vendor) > -1 && actFilters.topym.indexOf(itm_tyopm) > -1 && itm_value >= actFilters.valorFrom && itm_value <= actFilters.valorTo && itm_date >= moment(actFilters.dateFrom) && itm_date <= moment(actFilters.dateTo)) {
                        } else {
                            continue;
                        }
                    }

                    tot_cons += 1;
                    textHTML += "<tr id='" + cust_key + "'>";
                    textHTML += "<td class='td_name'>" + cust_name + "</td>";
                    textHTML += "<td class='td_id'>" + cust_id + "</td>";
                    textHTML += "</tr>";
                }

                $("#tot_header").text(tot_cons);
                $("#cont_table_data").append(textHTML);
                $("#noClientsDataWrap").html("");
                $("#noClientsDataWrap").hide();
                $("#bills_cont").show();

                let load_customer = function (cust) {
                    if (cust) {
                        $("#infocust_name").text(cust.name);
                        $("#infocust_since").text("Cliente " + cust.since);
                        $("#infocust_puntos").text(cust.points);
                        $("#infocust_id").text(cust.id);
                        $("#infocust_id_type").text(cust.type_id_txt);
                        let bdate = (cust.bdate.format("YYYY") == "1900") ? "-" : cust.bdate.format("MMM D YYYY").replace(".", ",").capitalize();
                        $("#infocust_bday").text(bdate);
                        let email = (cust.email == "") ? "-" : cust.email;
                        let phone = (cust.phone == "" || cust.phone == 0) ? "-" : cust.phone;
                        $("#infocust_email").text(email);
                        $("#infocust_tel").text(phone);

                        // Assign trigger to object
                        $("#input_btn_recom").off().click((el) => { cust.getRecomendations() });

                        // Assign customer key to edit class
                        editCustObj.assignSubmit(cust.key);
                        $("#edit_customer").off().click((el) => { editCustObj.iniciateEditCustomer(cust.key); });
                        editCustObj.setOnSuccess(() => { RefreshCustomers(false, cust.key); });

                        // Assign Delete Customer
                        $("#delete_customer").off().click((el) => { cust.delete(() => { RefreshCustomers(); }) })
                    }
                }

                for (var i = 0; i < cust_keys.length; i++) {
                    $("#" + cust_keys[i]).off().click((el) => { load_customer(new Customer(el.currentTarget.id)) });
                }

                // Load First Customer
                if (specificCust == '') {
                    load_customer(new Customer(firstKey));
                } else {
                    load_customer(new Customer(specificCust));
                }

                if (isFilter) { alertify.message("Filtro actualizado correctamente"); }

            } else {
                textHTML += "<img src='../icons/general/cactus.svg'>";
                textHTML += "<p>No tienes clientes</p>";
                $("#noClientsDataWrap").html("");
                $("#noClientsDataWrap").append(textHTML);
                $("#cont_table_data").html("");
                $("#tot_header").text('0');
                $("#noClientsDataWrap").show();
                $("#bills_cont").hide();
            }
        }
    });
}

function resetFilters() {
    actFilters.topym = ["1", "2", "3", "4"];
    actFilters.dateFrom = '1910-01-01';
    actFilters.dateTo = moment().format("YYYY-MM-DD");
    actFilters.salesman = [];
    actFilters.valorFrom = 0;
    actFilters.valorTo = 100000000;

    $("#topym_1").addClass("opt_selected");
    $("#topym_2").addClass("opt_selected");
    $("#topym_3").addClass("opt_selected");
    $("#topym_4").addClass("opt_selected");

    var x = document.getElementById("content_vendors_filter").getElementsByClassName("salesman_obj");
    var vendName = "";
    for (var i = 0; i < x.length; i++) {
        $("#" + x[i].id).addClass("opt_selected");
        vendName = x[i].innerText || x[i].textContent;
        vendName = (vendName == "ADMIN") ? "Administrador" : vendName;
        actFilters.salesman.push(vendName);
    }

    $("#filter_val_min").val("");
    $("#filter_val_max").val("");
    $("#filter_date_from").val("");
    $("#filter_date_to").val("");
    $("#filter_date_sel").val("0");
    $("#filter_date_from_cont").hide();
    $("#filter_date_to_cont").hide();
    $("#reset_filter_content").addClass("hide");

    RefreshCustomers();
    CloseFilterBills();
}


function toogleDeepData(idBill) {
    if ($("#bill_info_" + idBill).height() > 0) {
        $("#bill_info_" + idBill).removeClass("info_bill_on");
        $("#cont_" + idBill).removeClass("display-block");
    } else {
        CloseAllBillRows();
        $("#bill_info_" + idBill).addClass("info_bill_on");
        $("#cont_" + idBill).addClass("display-block");
    }
}

function find_cust() {
    let input = document.getElementById("search");
    let filter = input.value.toLowerCase().trim();
    filter = quitaacentos(filter)
    let table = document.getElementById("table");
    let tr = table.getElementsByTagName("tbody")[0].getElementsByTagName("tr");
    let k = 0;

    for (i = 0; i < tr.length; i++) {
        var td_id = tr[i].getElementsByClassName("td_id")[0];
        var txtID = td_id.textContent || td_id.innerText;
        txtID = quitaacentos(txtID);
        txtID = txtID.trim();
        var td_name = tr[i].getElementsByClassName("td_name")[0];
        var txtName = td_name.textContent || td_name.innerText;
        txtName = quitaacentos(txtName);
        txtName = txtName.trim();

        if (txtID.toLowerCase().indexOf(filter) > -1 || txtName.toLowerCase().indexOf(filter) > -1) {
            k += 1;
            $(tr[i]).removeClass("bye_row");
        } else {
            $(tr[i]).addClass("bye_row");
        }
    }
    $("#tot_header").text(k);
}


function SelectItemFilter(e) {
    var id_e = e.id;
    var x;
    if (id_e.indexOf("vend_") > -1) {
        x = id_e.replace("vend_", "");
        x = salesman_v[x];

        if (e.classList.contains("opt_selected")) {
            $(e).removeClass("opt_selected");
            actFilters.salesman.splice(actFilters.salesman.indexOf(x), 1);
        } else {
            $(e).addClass("opt_selected");
            actFilters.salesman.push(x);
        }

    } else if (id_e.indexOf("topym_") > -1) {
        x = id_e.replace("topym_", "");

        if (e.classList.contains("opt_selected")) {
            $(e).removeClass("opt_selected");
            actFilters.topym.splice(actFilters.topym.indexOf(x), 1);
        } else {
            $(e).addClass("opt_selected");
            actFilters.topym.push(x);
        }

    }
}

function PopulateFilterSalesman() {

    $.ajax({
        url: "pull_salesmen.php",
        dataType: "json",
        data: ({ x: '1' }),
        type: "POST",
        beforeSend: function () { },
        success: function (r) {
            $("#content_vendors_filter").html('');
            if (r.length > 0) {
                $("#wrap_vendors_filter").show();
                var htmlTxt = '';
                salesman_v = [];
                for (var i = 0; i < r.length; i++) {
                    salesman_v.push(r[i]["prof_name"]);
                    actFilters.salesman.push(r[i]["prof_name"]);
                    if (r[i]["admin"] == '1') {
                        htmlTxt += '<div id="vend_' + i + '" onclick="SelectItemFilter(this)" class="salesman_obj opt_selected"><img src="' + r[i]["avatar"] + '"><p>ADMIN</p></div>';
                    } else {
                        htmlTxt += '<div id="vend_' + i + '" onclick="SelectItemFilter(this)" class="salesman_obj opt_selected"><img src="' + r[i]["avatar"] + '"><p>' + r[i]["prof_name"] + '</p></div>';
                    }
                }
                $("#content_vendors_filter").append(htmlTxt);
            } else {
                $("#wrap_vendors_filter").hide();
            }
        }
    });
}
function filtersOrders() {
    this.topym = ["1", "2", "3", "4"];
    this.dateFrom = '1910-01-01';
    this.dateTo = moment().format("YYYY-MM-DD");
    this.salesman = [];
    this.valorFrom = 0;
    this.valorTo = 100000000;
}

function export_customers_csv() {

    let mr = new MainRoller();
    mr.startMainRoller();

    var headers = { cust_id: 'ID', cust_type_id: 'TIPO DE ID', cust_name: 'NOMBRE', cust_date_register: 'FECHA REGISTRO', cust_bdate: 'FECHA NACIMIENTO', cust_phone: 'TELÉFONO', cust_email: 'EMAIL', cust_gender: 'GENERO', cust_points: 'PUNTOS LUCA' };

    $.ajax({
        url: "get_customers.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            var items = [];
            if (r.length > 0) {
                r.forEach(function (item) {
                    items.push({
                        cust_id: item.cust_id.replace(/,/g, ''),
                        cust_type_id: item.cust_type_id.replace(/,/g, ''),
                        cust_name: item.cust_name.replace(/,/g, ''),
                        cust_date_register: item.cust_date_register.replace(/,/g, ''),
                        cust_bdate: item.cust_bdate.replace(/,/g, ''),
                        cust_phone: item.cust_phone.replace(/,/g, ''),
                        cust_email: item.cust_email.replace(/,/g, ''),
                        cust_gender: item.cust_gender.replace(/,/g, ''),
                        cust_points: item.cust_points.replace(/,/g, '')

                    });
                });
                var fileTitle = 'descarga_clientes_' + moment().format("YYYY_MM_DD");
                mr.stopMainRoller();
                exportCSVFile(headers, items, fileTitle);
            } else {
                mr.stopMainRoller();
                alertify.alert("", "No hay datos para descargar");
            }
        }
    });

}

function DeleteInvoiceByIdBill(id_bill) {
    alertify.confirm('Borrar cuenta', "¿Seguro desea eliminar la cuenta?", function () {
        let mr = new MainRoller();
        mr.startMainRoller();
        $.ajax({
            url: "delete_trx.php",
            dataType: "json",
            data: ({ id_bill: id_bill }),
            type: "POST",
            success: function (r) {
                mr.stopMainRoller();
                if (r[0][0] == "S") {
                    RefreshCustomers();
                    alertify.message("Cuenta eliminada correctamente");
                } else {
                    alertify.alert("Error", "No se pudo eliminar la cuenta");
                }
            }
        });
    }, function () { });
}


/**
 * Get a full report of metrics given a range of dates and a frequency.
 * @param {object} data Array object with transacional data. Ex: [{},{},...]
 * @param {date} dateFrom Date of start in moment form or date form. Ex: new Date(), moment(), moment("2019-01-01")
 * @param {date} dateTo Date of end in moment form or date form. Ex: new Date(), moment(), moment("2019-01-01")
 * @param {string} frequency Moment format of date representation to be grouped by it. Ex: 'MMMM' will group by months in string format 'junio', 'julio',...
 * @param {string} fillGaps Frequency of how the gaps should be filled for the dates. Need to be equal to the frequency. Ex: 'months', 'years', 'days' 
 * @param {integer} roundDigits Round digits number. Ex: 1, 4, 5, 0 ... 
 */
let reportOfSales = function (data, dateFrom, dateTo, frequency = 'MMMM', fillGaps = '', roundDigits = 0) {
    var results = {};

    let data_filter = data.filter((el) => { return moment(el.trx_date) >= dateFrom && moment(el.trx_date) <= dateTo });
    if (data_filter.length == 0) {
        return { metrics: { 'tot_sales': 0, 'tot_sales_value': 0, 'tot_num_items': 0, 'tot_tips': 0, 'tot_points_redeem': 0 } }
    }
    let data_filter_sort = data_filter.sort(function (a, b) { date1 = new Date(Date.parse(a["trx_date"].replace(" ", "T"))); date2 = new Date(Date.parse(b["trx_date"].replace(" ", "T"))); return date1 - date2 });
    let data_map = data_filter_sort.map((el) => { return { 'date': moment(el.trx_date).format(frequency), 'value': parseFloat(el.trx_value), 'num_items': parseInt(el.num_items), 'points_redeem': parseInt(el.points_redeem), 'tips': parseFloat(el.tips), 'cont': 1 } });

    var total_sales = data_map.flatMap((a) => a.cont).reduce((a, v) => a + v);
    var total_sales_value = data_map.flatMap((a) => a.value).reduce((a, v) => a + v);
    var total_num_items = data_map.flatMap((a) => a.num_items).reduce((a, v) => a + v);
    var total_tips = data_map.flatMap((a) => a.tips).reduce((a, v) => a + v);
    var total_points_redeem = data_map.flatMap((a) => a.points_redeem).reduce((a, v) => a + v);

    var avg_sales_value = (total_sales && total_sales > 0) ? numRound(total_sales_value / total_sales, roundDigits) : 0;
    var avg_num_items = (total_sales && total_sales > 0) ? numRound(total_num_items / total_sales, roundDigits) : 0;
    var avg_tips = (total_sales && total_sales > 0) ? numRound(total_tips / total_sales, roundDigits) : 0;
    var avg_points_redeem = (total_sales && total_sales > 0) ? numRound(total_points_redeem / total_sales, roundDigits) : 0;

    results['metrics'] = { 'tot_sales': total_sales, 'tot_sales_value': total_sales_value, 'tot_num_items': total_num_items, 'tot_tips': total_tips, 'tot_points_redeem': total_points_redeem };
    results['avg_by_sale'] = { 'sales_value': avg_sales_value, 'num_items': avg_num_items, 'tips': avg_tips, 'points_redeem': avg_points_redeem };

    // Sales by payment method
    let reducer_by_paytype = data_filter_sort.reduce((a, v) => { (a[v['payment_type']] = a[v['payment_type']] || []).push(v); return a }, {})
    var labels_pay = Object.keys(reducer_by_paytype);

    let by_payment = {};
    let total_py_sales = 0;
    let total_py_sales_value = 0;

    for (var i = 0; i < labels_pay.length; i++) {
        var tot_sales = reducer_by_paytype[labels_pay[i]].length;
        var tot_sales_value = reducer_by_paytype[labels_pay[i]].flatMap((el) => parseFloat(el.trx_value)).reduce((a, v) => a + v);
        total_py_sales += tot_sales;
        total_py_sales_value += tot_sales_value;
        by_payment[labels_pay[i]] = { tot_sales, tot_sales_value }
    }
    for (var i = 0; i < Object.keys(by_payment).length; i++) {
        if (total_py_sales > 0) {
            by_payment[Object.keys(by_payment)[i]]['per_sales'] = by_payment[Object.keys(by_payment)[i]]['tot_sales'] / total_py_sales;
        } else {
            by_payment[Object.keys(by_payment)[i]]['per_sales'] = 0;
        }
        if (tot_sales_value > 0) {
            by_payment[Object.keys(by_payment)[i]]['per_sales_val'] = by_payment[Object.keys(by_payment)[i]]['tot_sales_value'] / tot_sales_value;
        } else {
            by_payment[Object.keys(by_payment)[i]]['per_sales_val'] = 0;
        }
    }

    results['by_payment'] = by_payment;

    if (frequency != '') {

        let sales_val_by_data = [];
        let sales_by_data = [];
        let num_items_by_data = [];
        let tips_by_data = [];
        let total_points_redeem_by_data = [];
        let avg_sales_val_fq = [];
        let avg_num_items_fq = [];
        let avg_tips_fq = [];
        let avg_points_redeem_fq = [];
        let reducer_by_date = data_map.reduce((a, v) => { (a[v['date']] = a[v['date']] || []).push(v); return a }, {})
        var labels = Object.keys(reducer_by_date);

        // Sales by date
        for (var i = 0; i < labels.length; i++) {
            sales_val_by_data.push(reducer_by_date[labels[i]].flatMap((a) => a.value).reduce((a, v) => a + v));
            sales_by_data.push(reducer_by_date[labels[i]].length);
            num_items_by_data.push(reducer_by_date[labels[i]].flatMap((a) => a.num_items).reduce((a, v) => a + v));
            tips_by_data.push(reducer_by_date[labels[i]].flatMap((a) => a.tips).reduce((a, v) => a + v));
            total_points_redeem_by_data.push(reducer_by_date[labels[i]].flatMap((a) => a.points_redeem).reduce((a, v) => a + v));

            avg_sales_val_fq.push(numRound(sales_val_by_data[i] / sales_by_data[i], roundDigits));
            avg_num_items_fq.push(numRound(num_items_by_data[i] / sales_by_data[i], roundDigits));
            avg_tips_fq.push(numRound(tips_by_data[i] / sales_by_data[i], roundDigits));
            avg_points_redeem_fq.push(numRound(total_points_redeem_by_data[i] / sales_by_data[i], roundDigits));
        }

        var avg_sales_by_fq = total_sales / labels.length;
        var avg_sales_value_by_fq = total_sales_value / labels.length;
        var avg_num_items_by_fq = total_num_items / labels.length;
        var avg_tips_by_fq = total_tips / labels.length;
        var avg_points_redeem_by_fq = total_points_redeem / labels.length;

        results['by_frequency'] = {
            labels,
            totals: { 'sales': sales_by_data, 'sales_value': sales_val_by_data, 'num_items': num_items_by_data, 'tips': tips_by_data, 'points_redeem': total_points_redeem_by_data },
            avg_by_sales: { 'sales_value': avg_sales_val_fq, 'num_items': avg_num_items_fq, 'tips': avg_tips_fq, 'points_redeem': avg_points_redeem_fq },
            avg_by_freq: { 'sales': avg_sales_by_fq, 'sales_value': avg_sales_value_by_fq, 'num_items': avg_num_items_by_fq, 'tips': avg_tips_by_fq, 'points_redeem': avg_points_redeem_by_fq }
        }

        if (fillGaps != '') {
            var fill_dates_labels = [];
            var fill_dates_labels_format = [];
            var fill_sales = [];
            var fill_sales_val = [];
            var fill_sales_num_items = [];
            var fill_sales_tips = [];
            var fill_sales_points_redeem = [];
            var fill_avg_sales_val = [];
            var fill_avg_sales_num_items = [];
            var fill_avg_sales_tips = [];
            var fill_avg_sales_points_redeem = [];

            var diff_time = Math.abs(dateTo.diff(dateFrom, fillGaps));

            for (var i = 0; i <= diff_time - 1; i++) {
                var date_temp_1 = dateFrom.add(1, fillGaps);
                var date_temp = date_temp_1.format(frequency);
                fill_dates_labels_format.push(date_temp_1.format("YYYY-MM-DD"))
                fill_dates_labels.push(date_temp);
                var indx = labels.indexOf(date_temp);
                if (indx > -1) {
                    fill_sales.push(sales_by_data[indx]);
                    fill_sales_val.push(sales_val_by_data[indx]);
                    fill_sales_num_items.push(num_items_by_data[indx]);
                    fill_sales_tips.push(tips_by_data[indx]);
                    fill_sales_points_redeem.push(total_points_redeem_by_data[indx]);
                    fill_avg_sales_val.push(avg_sales_val_fq[indx]);
                    fill_avg_sales_num_items.push(avg_num_items_fq[indx]);
                    fill_avg_sales_tips.push(avg_tips_fq[indx]);
                    fill_avg_sales_points_redeem.push(avg_points_redeem_fq[indx]);
                } else {
                    fill_sales.push(0);
                    fill_sales_val.push(0);
                    fill_sales_num_items.push(0);
                    fill_sales_tips.push(0);
                    fill_sales_points_redeem.push(0);
                    fill_avg_sales_val.push(0);
                    fill_avg_sales_num_items.push(0);
                    fill_avg_sales_tips.push(0);
                    fill_avg_sales_points_redeem.push(0);
                }
            }
            results['by_frequency']['fill_labels'] = fill_dates_labels;
            results['by_frequency']['fill_labels_format'] = fill_dates_labels_format;
            results['by_frequency']['fill_totals'] = { 'sales': fill_sales, 'sales_value': fill_sales_val, 'num_items': fill_sales_val, 'tips': fill_sales_tips, 'points_redeem': fill_sales_points_redeem };
            results['by_frequency']['fill_avg_by_sales'] = { 'sales_value': fill_avg_sales_val, 'num_items': fill_avg_sales_num_items, 'tips': fill_avg_sales_tips, 'points_redeem': fill_avg_sales_points_redeem };
        }
    }

    return results;
}

let reset_elements = function () {
    $("#allord_cont").html('');
    $("#summ_bill_cons").html('');
    $("#icn_sales").text("0");
    $("#icn_sales_val").text("$ 0");
    $("#icn_sales_items").text("0");
    $("#gasto_prom_indc").text("0");
    $("#sales_prom_indc").text("0");
    $("#tp_efectivo").text("0%");
    $("#tp_credito").text("0%");
    $("#tp_debito").text("0%");
    $("#tp_otro").text("0%");
    $('#last_y_ch1').addClass('card-options-active');
    $('#last_m_ch1').removeClass('card-options-active');
    $('#last_w_ch1').removeClass('card-options-active');
    $('#last_y_ch2').addClass('card-options-active');
    $('#last_m_ch2').removeClass('card-options-active');
    $('#last_w_ch2').removeClass('card-options-active');
    $('#pay_perc_op').addClass('card-options-active');
    $('#pay_value_op').removeClass('card-options-active');

    new barChart("barChart-1", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec']);
    new barChart("barChart-2", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec']);

    $('#anl_seg').addClass('hide');
    $('#anl_nba').addClass('hide');
    $('#anl_insg').addClass('hide');
    $("#badges_cust").html('');
    $("#descr_badge>p").text('');
    $("#descr_badge").addClass("hide");
}

let get_clv_all = function () {
    $.ajax({
        url: 'get_dashboard_data.php',
        dataType: 'json',
        data: ({ x: 1 }),
        type: 'POST',
        success: function (tx_data) {
            r2_by_cust = tx_data.reduce((a, v) => { (a[v['cust_key']] = a[v['cust_key']] || []).push(v); return a }, {})
            r2_keys = Object.keys(r2_by_cust).filter((el) => el != "");
            var tot_sales, tot_sales_val, sales, sales_val;
            tot_sales = 0; tot_sales_val = 0;
            for (var i = 0; i < r2_keys.length; i++) {
                sales = r2_by_cust[r2_keys[i]].length;
                sales_val = r2_by_cust[r2_keys[i]].flatMap((a) => parseFloat(a.trx_value)).reduce((a, v) => a + v);
                tot_sales += sales;
                tot_sales_val += sales_val;
            }
        }
    });
}