let curBill = '';
let AllCustomersJSON = '';
let ProfilesJSON = '';
let curr_profile = '';
let prof_chngpass = '';
let ScannerIsRunning = false;
let QuaggaLastResult = '';
let GoodsImport = 0;
let BadsImport = 0;
let CSVToImport = [];
let SelectedCustomer = '';
let BarcodeFoundScanned = [];
let startTimeScan = new Date();
let timeoutId = 0;
let CurrHoldItem = '';
let togg_top_sales_open = true;
let emailDisable = false;
let emailBillChecked = false;
let printBillChecked = false;
let noPrintBillChecked = false;
let PointSystem = 700;
let CashChecked = false;
let CreditCardChecked = false;
let DebitCardChecked = false;
let OtherChecked = false;
let SelectedItemsHome = [];
let dataProducts = "";
let refItemEditBill = '';
let ItemsAddedCashier = 1;
let CurTipsPer = 0;
let CurDiscountPer = 0;
let D1QR = new Date();
let QRlastShot = '';
let D2QR = new Date();
let D3QR = new Date();
let video = document.createElement("video");
let dataItemStats1 = '';
let box1Chart;
let box2Chart;
let OffLineAdmin = new ModalBasic("OffLineAdmin");
let isRestnt = false;
moment.locale('es');


$(document).ready(function () {

    // Inicializate Spinner and Rollers
    $('#MainRoller_background').hide();
    $('.bar_loading_cont').hide();
    $('#LoadingRoller_Salesmen').hide();
    $('#LoadingRoller_PassProf').hide();
    $('#LoadingRoller_checkout').hide();
    $('#checkmark_checkout').hide();
    $('#LoadingRoller_EditItem').hide();
    $('#checkmark_EditItem').hide();
    $('#LoadingRoller_itemStat').hide();

    // Hide stuff
    $('#discount_lucapoints_option').hide();
    $('#lr_cont').hide();

    // Check Cookies Support
    checkCookiesSupport();
    // Load Customers 
    push_all_customers("cust_name");
    LoadCustomers();
    // Load Top 10 Item Sales
    push_top_items();
    // Open First profile Choose
    CheckAdminStatus();
    // Get Acct Data
    GetActSecData();
    // Push All Items in the list
    push_all_items("#box_products", "prod_name");

    // Check Resolution State
    checkCurrResolution().then((r) => { });

    // Prevent default in forms
    $(".cb_search_consumer").submit((event) => event.preventDefault());
    $(".search").submit((event) => event.preventDefault());

    // **** ICONS MODAL ****
    // Cargar Galeria de imagenes
    displayImgsPreloaded("74-food", "loadLogoGallery_Icon");
    // Cargar la caterogía seleccionada de la galeria de imagenes
    $('#logo_categ_list').change(function () {
        displayImgsPreloaded(this.value, "loadLogoGallery_Icon");
    });
    // ********

    // Voice recognition access
    CheckPkgAccess(2).then(function (cpa) {
        checkFreeTrail(2).then(function (cft) {
            if (cpa || cft.isactive) {
                $("#opVoiceCom").removeClass("hide");
                $("#opVoiceCom").off('click').click(() => { StartSpeachRecog(); })
            } else {
                $("#opVoiceCom").remove();
                $("#opVoiceCom").off('click');
            }
        });
    });

    // WHEN USER REFRESHS THE PAGE
    window.onbeforeunload = function () {
        if (Number($('#total_items_bill').text()) > 0) {
            return "Una factura está en proceso y la información se perderá. ¿Seguro deseas continuar?";
        } else {
            return;
        }
    };

    // ACTIVATE ONLY FOR RESTAURANTS
    $('#edititembill_adicion').select2();
    $("#cmd_lnk").click(function () { window.open('../php/comandas.php'); });

    isRestaurant().then(function (r) {
        if (r.value) {
            isRestnt = true;
            $('.rest_vis').show();
            $(".select2").show();
            $("#cmd_lnk").show();
        } else {
            isRestnt = false;
            $('.rest_vis').hide();
            $(".select2").hide();
            $("#cmd_lnk").hide();
        }
    });


    // :::::::::::::::: EVENT FUNCTIONS ::::::::::::::::

    $("#ex_id").click(function () { $("#menu_ex_id").fadeToggle("fast") });
    $("#co1_type_email").click(function () { checkTypeOfBill('email') });
    $("#co1_type_print").click(function () { checkTypeOfBill('print') });
    $("#co1_type_none").click(function () { checkTypeOfBill('noprint') });

    $("#sendAllOffBills").click(function () {
        $("#menu_ex_id").fadeToggle("fast");
        PushServerBills();
    });
    $("#sendAllOffBills2").click(function () {
        PushServerBills();
        OffLineAdmin.close();
    });
    $("#cleanAllOffBills").click(function () {
        $("#menu_ex_id").fadeToggle("fast");
        ClearOfflineBills();
    });
    $("#cleanAllOffBills2").click(function () {
        ClearOfflineBills();
        OffLineAdmin.close();
    });
    $("#seeOffBills").click(function () {
        let offbills = GetOfflineBills();
        if (offbills.length > 0) {
            $("#menu_ex_id").fadeToggle("fast");
            loadOffBIlls();
            OffLineAdmin.open();
        } else {
            alertify.message("No hay cuentas pendientes por enviar");
        }
    });
    $('#EditItemInBill_addCant').off().click(() => {
        let currVal = parseFloat($('#edititembill_count').val().replace(/[^\d\.]/gi, ""));
        let newVal = currVal + 1;
        (isNaN(newVal)) ? $('#edititembill_count').val('') : $('#edititembill_count').val(newVal);
    });
    $('#EditItemInBill_subCant').off().click(() => {
        let currVal = parseFloat($('#edititembill_count').val().replace(/[^\d\.]/gi, ""));
        let newVal = currVal - 1;
        if (isNaN(newVal)) {
            $('#edititembill_count').val('');
        } else {
            newVal = (newVal < 0) ? 0 : newVal;
            $('#edititembill_count').val(numRound(newVal, 3));
        }
    });

    // ::::: SUGGEST PRICES :::::
    let addNewProductByIdSearch = function (value, msgFound, msgNotFound) {
        msgFound = (msgFound === undefined) ? false : msgFound;
        msgNotFound = (msgNotFound === undefined) ? false : msgNotFound;

        SearchBarCodeExt(value).then(function (x) {
            if (x.length > 0) {
                $("#ni_prod_name").val(x[0]["prod_name_long"]);
                $("#ni_prod_units").val(x[0]["prod_units"]);
                $("#icon_selection_btn").attr('src', x[0]["prod_icon"]);
                $("#ni_prod_icon").val(x[0]["prod_icon"]);
                $("#ni_prod_name").focus();
                if (msgFound) {
                    alertify.message("Se encontró un producto asociado.");
                }
            } else {
                if (msgNotFound) {
                    alertify.message("No se encontró ningún producto");
                }
            }
        });
    }

    $("#ni_prod_code").off("blur").blur(function () {
        let id = $("#ni_prod_code").val();
        if (id.length > 0) {
            addNewProductByIdSearch(id, true, false);
        }
    });

    $("#suggestPrice").off("click").click(function () {
        let id = $("#ni_prod_code").val();
        if (id.length > 0) {
            SearchBarCodeExt(id).then(function (x) {
                if (x.length > 0) {
                    $("#ni_prod_price").val("$ " + numCommas(numRound(x[1]["AVG_PRICE"], 0)));
                    alertify.message("Se encontró un precio sugerido.");
                    $("#ni_prod_price").focus();
                } else {
                    alertify.message("No hay sugerencias de precio para este producto.");
                }
            });
        } else {
            alertify.message("Ingresa un código de barras.");
            $("#ni_prod_code").focus();
        }
    });

    // :::::::::::::::::

    $("#header_top_sales").click(function () {
        if (togg_top_sales_open == true) {
            $("#btn_head_top_sales").css("transform", "rotate(180deg) translateX(-16px)");
            togg_top_sales_open = false;
        } else {
            $("#btn_head_top_sales").css("transform", "rotate(0deg) translateX(0px)");
            togg_top_sales_open = true;
        }

    });

    $("#btn_create_cat").click(function () {
        let x = $(".color_picked").css("backgroundColor");
        if (x == null) {
            colorCar = "#666";
        } else {
            colorCar = hexc(x);
        }

        let nameCat = $("#catg_name").val();

        if (nameCat.length > 30) {
            $("#msg_cat").text("Nombre de la categoría muy extenso");
            $("#msg_cat").css({ "color": "#666", "display": "block" });
        } else if (nameCat.length == 0) {
            $("#msg_cat").text("Ingresa el nombre de la categoría");
            $("#msg_cat").css({ "color": "#666", "display": "block" });
        } else {
            $.ajax({
                url: "push_category.php",
                data: ({ cat_name: nameCat, cat_color: colorCar }),
                type: "POST",
                success: function (r) {

                    if (r == 'Categoría agregada correctamente') {
                        $("#new_category").css({ "height": "0px", "padding": "0px" });
                        $("#msg_cat").css({ "color": "#666", "display": "none" });
                        $('#ni_prod_category').append('<option value="' + nameCat + '" selected>' + nameCat + '</option>');
                    } else {
                        $("#msg_cat").text(r);
                        $("#msg_cat").css({ "color": "#666", "display": "block" });
                    }
                }
            });

        }

    });

    $("#btn_create_cat2").click(function () {
        let x = $(".color_picked").css("backgroundColor");
        if (x == null) {
            colorCar = "#666";
        } else {
            colorCar = hexc(x);
        }

        let nameCat = $("#catg_name2").val();

        if (nameCat.length > 30) {
            $("#msg_cat2").text("Nombre de la categoría muy extenso");
            $("#msg_cat2").css({ "color": "#666", "display": "block" });
        } else if (nameCat.length == 0) {
            $("#msg_cat2").text("Ingresa el nombre de la categoría");
            $("#msg_cat2").css({ "color": "#666", "display": "block" });
        } else {
            $.ajax({
                url: "push_category.php",
                data: ({ cat_name: nameCat, cat_color: colorCar }),
                type: "POST",
                success: function (r) {
                    if (r == 'Categoría agregada correctamente') {
                        $("#new_category2").css({ "height": "0px", "padding": "0px" });
                        $("#msg_cat2").css({ "color": "#666", "display": "none" });
                        $('#ni_prod_category2').append('<option value="' + nameCat + '" selected>' + nameCat + '</option>');
                    } else {
                        $("#msg_cat2").text(r);
                        $("#msg_cat2").css({ "color": "#666", "display": "block" });
                    }
                }
            });


        }

    });

    // Auto selection in units in New Product
    let options = {
        url: "../json/units.json",
        getValue: "name",
        list: {
            maxNumberOfElements: 3,
            match: { enabled: true },
            sort: { enabled: true },
            showAnimation: {
                type: "slide",
                time: 200,
                callback: function () { }
            },
            hideAnimation: {
                type: "slide",
                time: 200,
                callback: function () { }
            }
        }
    };

    let togg_top_sales_open = true;
    $("#btn_head_products").click(function () {
        if (togg_top_sales_open == true) {
            $("#btn_head_products").css("transform", "rotate(180deg) translateX(-16px)");
            togg_top_sales_open = false;
        } else {
            $("#btn_head_products").css("transform", "rotate(0deg) translateX(0px)");
            togg_top_sales_open = true;
        }

    });
    $("#header_products>p:first-child").click(function () {
        if (togg_top_sales_open == true) {
            $("#btn_head_products").css("transform", "rotate(180deg) translateX(-16px)");
            togg_top_sales_open = false;
        } else {
            $("#btn_head_products").css("transform", "rotate(0deg) translateX(0px)");
            togg_top_sales_open = true;
        }
    });

    $("#ni_prod_units").easyAutocomplete(options);
    $("#ni_prod_units2").easyAutocomplete(options);

    // SUBMIT NEW CUSTOMER 
    $("#new-customer-form").submit(function () {
        let ncust_name = $("#ncust_name").val();
        let ncust_email = $("#ncust_email").val().toLowerCase();
        let ncust_phone = $("#ncust_phone").val();
        let ncust_bdate = $("#ncust_bdate").val();
        let gender = $('input[name=gender]:checked').val();
        let ncust_id = $("#ncust_id").val();
        let ncust_type_id = $("#ncust_type_id").val();

        if (ncust_name == '') {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Debes ingresar el nombre');
            ErrorInputBox('ncust_name');
            return false;
        }
        if (ncust_name.length > 50) {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El nombre no puede tener más de 50 caracteres');
            ErrorInputBox('ncust_name');
            return false;
        }
        let validName = /[0-9!"#$%&/()=?¡'¿¨*´+><._]/g;
        if (validName.test(ncust_name)) {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El nombre del cliente no puede contener números ni caracteres especiales');
            ErrorInputBox('ncust_name');
            return false;
        }
        if (ncust_id == '') {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Debes ingresar el número de identificación');
            ErrorInputBox('ncust_id');
            return false;
        }
        if (ncust_id.length > 30) {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'El número de identificación no puede tener más de 30 caracteres');
            ErrorInputBox('ncust_id');
            return false;
        }
        if (ncust_id != '' && ncust_type_id == '1' && isNaN(ncust_id) == true) {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'La cédula de cuidadania debe contener únicamente valores numéricos y no puede contener espacios');
            ErrorInputBox('ncust_id');
            return false;
        }
        if (ncust_bdate != '') {
            let typeDate1 = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(ncust_bdate);
            let splitDate = ncust_bdate.split("-");

            if (splitDate.length > 1 && typeDate1 == true) {
                if (splitDate[1] > 12 || splitDate[1] <= 0 || splitDate[2] > 31 || splitDate[2] <= 0 || splitDate[0] < 1910) {
                    ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Fecha de nacimiento incorrecta');
                    ErrorInputBox('ncust_bdate');
                    return false;
                }
                let bdayDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                let today = new Date();

                if (bdayDate >= today) {
                    ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'La fecha de nacimiento debe ser menor que la fecha actual');
                    ErrorInputBox('ncust_bdate');
                    return false;
                } else if (isNaN(bdayDate)) {
                    ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Fecha de nacimiento incorrecta');
                    ErrorInputBox('ncust_bdate');
                    return false;
                }
            }
        }
        if (gender == undefined) {
            ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'Ingresa el genero del cliente');
            return false;
        }

        $.ajax({
            url: 'push_new_customer.php',
            dataType: "json",
            data: ({
                ncust_name: ncust_name,
                ncust_email: ncust_email,
                ncust_phone: ncust_phone,
                ncust_bdate: ncust_bdate,
                gender: gender,
                ncust_type_id: ncust_type_id,
                ncust_id: ncust_id
            }),
            type: "POST",
            success: function (r) {
                x1 = r;
                let SimpleName = nameHandler(ncust_name)[1]["shortFullName"];
                let firstName = ncust_name.split(" ")[0];
                if (SimpleName == '') {
                    SimpleName = firstName;
                }
                if (r[0][0] == 'E') {
                    if (r[0][1] == '1') {
                        SimpleMsgModalBox('open', 'simpleMsgNewCustomer', 'Completa todos los campos');
                        return false;
                    } else if (r[0][1] == '2') {
                        SimpleMsgModalBox('open', 'simpleMsgNewCustomer', 'Se produjo un error ingresando el cliente');
                    } else {
                        SimpleMsgModalBox('open', 'simpleMsgNewCustomer', 'Se produjo un error ingresando el cliente');
                    }
                } else if (r[0][0] == 'F') {
                    if (r[0][1] == '1') {
                        $("#simpleMsgNewCustomer").click(function () {
                            ncust_name = r[1]["cust_name"];
                            SimpleName = nameHandler(ncust_name)[1]["shortFullName"];
                            firstName = ncust_name.split(" ")[0];
                            if (SimpleName == '') {
                                SimpleName = firstName;
                            }
                            $("#search_consumer").val(SimpleName + " - " + r[1]["cust_id"]);
                            AddCustToSearchBox(r[1]["cust_key"]);
                            $("#cb_icon_new_cons").addClass("hide_new_cons");
                            $("#cb_icon_clear_cons").removeClass("hide_clearconsumer");
                            $("#cb_icon_info_cons").removeClass("hide_clearconsumer");
                            CloseNewCustomer();
                            $('#new-customer-form').trigger("reset");
                        });
                        SimpleMsgModalBox('open', 'simpleMsgNewCustomer', 'El cliente ingresado con ID: ' + r[1]["cust_id"] + ' ya existe con el nombre de ' + r[1]["cust_name"] + '. Da click aquí para agregarlo a esta venta.');
                    }
                } else if (r[0][0] == 'S') {
                    $("#search_consumer").val(SimpleName + " - " + r[1]["cust_id"]);
                    AddCustToSearchBox(r[1]["cust_key"]);
                    $("#cb_icon_new_cons").addClass("hide_new_cons");
                    $("#cb_icon_clear_cons").removeClass('hide_clearconsumer');
                    $("#cb_icon_info_cons").removeClass('hide_clearconsumer');
                    CloseNewCustomer();
                    $('#new-customer-form').trigger("reset");
                    alertify.message("Cliente ingresado correctamente");
                }

            },
            error: function (request, status, error) {
                console.log(error);
                ErrorMsgModalBox('open', 'errorMsgNewCustomer', 'No fue posible registrar al cliente. Por favor, intenta de nuevo.');
            }
        });
        LoadCustomers();
        return false;
    });

    // SUBMIT EDIT CUSTOMER 
    $("#edit-customer-form").submit(function () {

        let ncust_name = $("#ed_cust_name").val();
        let ncust_email = $("#ed_cust_email").val().toLowerCase();
        let ncust_phone = $("#ed_cust_phone").val();
        let ncust_bdate = $("#ed_cust_bdate").val();
        let gender = $('input[name=ed_gender]:checked').val()
        let ncust_type_id = $("#ed_cust_type_id").val();
        let ncust_id = $("#ed_cust_id").val();
        let ncust_id_old = $("#ed_cust_id_old").val();
        let ncust_type_id_old = $("#ed_cust_type_id_old").val();

        let cust_key = SelectedCustomer;

        if (ncust_name == '') {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Debes ingresar el nombre');
            ErrorInputBox('ed_cust_name');
            return false;
        }
        if (ncust_name.length > 50) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El nombre no puede tener más de 50 caracteres');
            ErrorInputBox('ed_cust_name');
            return false;
        }
        if (ncust_id == '') {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Debes ingresar el número de identificación');
            ErrorInputBox('ed_cust_id');
            return false;
        }
        if (ncust_id.length > 30) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El número de identificación no puede tener más de 30 caracteres');
            ErrorInputBox('ed_cust_id');
            return false;
        }
        let validName = /[0-9!"#$%&/()=?¡'¿¨*´+><._]/g;
        if (validName.test(ncust_name)) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El nombre del cliente no puede contener números ni caracteres especiales');
            ErrorInputBox('ed_cust_name');
            return false;
        }
        if (ncust_id != '' && ncust_type_id == '1' && isNaN(ncust_id) == true) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'La cédula de cuidadania debe contener únicamente valores numéricos y no contener espacios');
            ErrorInputBox('ed_cust_id');
            return false;
        }
        if (ncust_bdate != '') {
            let typeDate1 = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(ncust_bdate);
            let splitDate = ncust_bdate.split("-");

            if (splitDate.length > 1 && typeDate1 == true) {
                if (splitDate[1] > 12 || splitDate[1] <= 0 || splitDate[2] > 31 || splitDate[2] <= 0 || splitDate[0] < 1910) {
                    ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Fecha de nacimiento incorrecta');
                    ErrorInputBox('ed_cust_bdate');
                    return false;
                }
                let bdayDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                let today = new Date();

                if (bdayDate >= today) {
                    ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'La fecha de nacimiento debe ser menor que la fecha actual');
                    ErrorInputBox('ed_cust_bdate');
                    return false;
                } else if (isNaN(bdayDate)) {
                    ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Fecha de nacimiento incorrecta');
                    ErrorInputBox('ed_cust_bdate');
                    return false;
                }
            }
        }
        if (ncust_phone != '' && ncust_phone.length > 10) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'El número de teléfono debe tener máximo 10 dígitos.');
            ErrorInputBox('ed_cust_phone');
            return false;
        }

        if (gender == undefined) {
            ErrorMsgModalBox('open', 'errorMsgEditCustomer', 'Ingresa el genero del cliente');
            return false;
        }

        $.ajax({
            url: "edit_customer.php",
            dataType: "json",
            data: ({
                ncust_name: ncust_name,
                ncust_email: ncust_email,
                ncust_phone: ncust_phone,
                ncust_bdate: ncust_bdate,
                gender: gender,
                ncust_type_id: ncust_type_id,
                ncust_id: ncust_id,
                ncust_id_old: ncust_id_old,
                ncust_type_id_old: ncust_type_id_old,
                cust_key: cust_key
            }),
            type: "POST",
            success: function (r) {
                x1 = r;
                if (r.length > 0) {
                    if (r[0][0] == 'E') {
                        ErrorMsgModalBox('open', 'errorMsgEditCustomer', "Se presentó un error (ERR:98423)");
                        return false;
                    } else if (r[0][0] == 'F') {
                        if (r[0][1] == 1) {
                            ErrorMsgModalBox('open', 'errorMsgEditCustomer', "El cliente con el ID: " + r[1]["cust_id"] + " ya existe con el nombre de " + r[1]["cust_name"]);
                            ErrorInputBox('ed_cust_id');
                            return false;
                        }
                    } else if (r[0][0] == 'S') {
                        alertify.message("Cliente actualizado correctamente");
                        LoadCustomers();
                        ClearConsumerActive();
                        CloseEditCustomer();
                    }
                } else {
                    ErrorMsgModalBox('open', 'errorMsgEditCustomer', "Se presentó un error (ERR:93856)");
                    return false;
                }
            }
        });

        return false;
    });


    $("#search_consumer").keyup(function () {
        if ($('#discType').val() == 'lucapoints') {
            $("#discount_bill").text("0");
            $('#discAp').text('');
            $('#discType').val('');
            RefreshBill();
        }
        let valueSC = $(this).val();
        if (valueSC == '') {
            $("#cb_icon_new_cons").removeClass("hide_new_cons");
            $("#num_luca_points").removeClass("noty_show");
            $("#cb_icon_clear_cons").addClass("hide_clearconsumer");
            $("#cb_icon_info_cons").addClass("hide_clearconsumer");
        } else {
            $("#cb_icon_new_cons").addClass("hide_new_cons");
            $("#cb_icon_clear_cons").removeClass("hide_clearconsumer");
        }
    });

    $("#input_luca_points").keyup(function () {
        let x = $(this).val();
        let rs = 7; // Un punto vale 7 pesos
        if (x != '' && !isNaN(x)) {
            x = parseInt(x.replace(/[^0-9]/g, ""));
            $(this).val(x);
            if (!isNaN(x)) {
                let sbt = Number($('#lr_subtot').text().replace(",", ""));
                let tot = Number($('#lr_tot').text().replace(",", ""));
                let maxPoints = parseInt($('#lr_0_points').text());
                if (x <= 0) {
                    $('#lr_subtot').text($('#super_total_bill').text());
                    $('#lr_save').text("0");
                    $('#lr_tot').text($('#super_total_bill').text());
                } else {
                    //x = 100 puntos;
                    //y = (100 * 7) = $ 700
                    //new_tot = ($10,000 - $700) = $9,300
                    //necc_points_full = CEIL($10,000/$7) = 1429 puntos
                    //maxPoints = 500 puntos
                    // El cliente maximo puede redimir maxPoints puntos
                    let y, new_tot;
                    let necc_points_full = Math.ceil(sbt / rs);
                    let num_punts_tot = 0;

                    if (x > maxPoints) {
                        $(this).val(0);
                        $('#lr_subtot').text($('#super_total_bill').text());
                        $('#lr_save').text("0");
                        $('#lr_tot').text($('#super_total_bill').text());
                        ErrorMsgModalBox('open', 'errorMsgDiscount', 'El cliente máximo puede redimir ' + maxPoints + ' puntos');
                    }

                    if (maxPoints >= necc_points_full) {
                        // El cliente tiene los puntos necesarios
                        if (x == necc_points_full) {
                            $(this).val(necc_points_full);
                            $('#lr_save').text(numCommas(sbt));
                            $('#lr_tot').text(0);
                        } else if (x > necc_points_full) {
                            $(this).val(necc_points_full);
                            $('#lr_save').text(numCommas(sbt));
                            $('#lr_tot').text(0);
                            ErrorMsgModalBox('open', 'errorMsgDiscount', 'No puedes redimir más puntos que el total de la factura.');
                        } else if (x < necc_points_full) {
                            y = (x * rs);
                            new_tot = sbt - y;
                            $('#lr_save').text(numCommas(y));
                            $('#lr_tot').text(numCommas(new_tot));
                        }
                    } else {
                        // El cliente NO tiene los puntos necesarios
                        if (x <= maxPoints) {
                            y = (x * rs);
                            new_tot = sbt - y;
                            $('#lr_save').text(numCommas(y));
                            $('#lr_tot').text(numCommas(new_tot));
                        } else if (x > maxPoints) {
                            y = (maxPoints * rs);
                            new_tot = sbt - y;
                            $(this).val(maxPoints);
                            $('#lr_save').text(numCommas(y));
                            $('#lr_tot').text(numCommas(new_tot));
                            ErrorMsgModalBox('open', 'errorMsgDiscount', 'El cliente no tiene suficientes puntos.');
                        }
                    }
                }
            } else {
                $('#lr_subtot').text($('#super_total_bill').text());
                $('#lr_save').text("0");
                $('#lr_tot').text($('#super_total_bill').text());
            }
        } else {
            $('#lr_subtot').text($('#super_total_bill').text());
            $('#lr_save').text("0");
            $('#lr_tot').text($('#super_total_bill').text());
        }
    });
    $('#lr_0_all_points').click(function () {
        let sbt = Number($('#lr_subtot').text().replace(",", ""));
        let rs = 7;
        let maxPoints = parseInt($('#lr_0_points').text());
        let necc_points_full = Math.ceil(sbt / rs);
        $('#input_luca_points').focus();
        if (maxPoints >= necc_points_full) {
            $("#input_luca_points").val(necc_points_full);
            $('#lr_save').text(numCommas(sbt));
            $('#lr_tot').text(0);
        } else {
            y = (maxPoints * rs);
            new_tot = sbt - y;
            $("#input_luca_points").val(maxPoints);
            $('#lr_save').text(numCommas(y));
            $('#lr_tot').text(numCommas(new_tot));
        }
    });
    $("#ncust_name").focusout(function () {
        let valeName = $("#ncust_name").val();
        if (valeName != '') {
            let GenderAprox = nameHandler(valeName)[1]["GenderAprox"];
            if (GenderAprox == 'Hombre') {
                $("#genderMale").prop("checked", true);
                $("#genderFemale").prop("checked", false);
            } else if (GenderAprox == 'Mujer') {
                $("#genderMale").prop("checked", false);
                $("#genderFemale").prop("checked", true);
            }

        }
    });
    $("#ed_cust_name").focusout(function () {
        let valeName = $("#ed_cust_name").val();
        if (valeName != '') {
            let GenderAprox = nameHandler(valeName)[1]["GenderAprox"];
            if (GenderAprox == 'Hombre') {
                $("#ed_genderMale").prop("checked", true);
                $("#ed_genderFemale").prop("checked", false);
            } else if (GenderAprox == 'Mujer') {
                $("#ed_genderMale").prop("checked", false);
                $("#ed_genderFemale").prop("checked", true);
            }

        }
    });


    $("#ncust_bdate").keyup(function (e) {
        if (get_browser_info()["name"] == "Safari") {
            if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

            } else {
                let value = ($(this).val());
                value = value.replace(/[^0-9dmy]/g, "");
                if (value.length > 4) {
                    value = value.replace(/\D+/g, "").replace(/([0-9]{1,4})([0-9]{2})([0-9]{2}$)/gi, "$1-$2-$3");
                }
                $(this).val(value);
            }
        }
    });


    // SUBMIT NEW PRODUCT ITEM
    $("#new-item-form").submit(function () {

        let valid = validation_new_product();

        if (valid == true) {
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: $(this).attr('method'),
                success: function (r) {
                    if (r == '1') {
                        push_all_items("#box_products", "prod_name");
                        let prodname = $("#ni_prod_name").val();
                        let prodref = $("#ni_prod_code").val();
                        $("#itemNamePostAdd").text(prodname);
                        CloseNewItem();
                        OpenNewItemEnd();
                        let linkURLQR = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&choe=ISO-8859-1&chl=" + prodref.trim();
                        $("#QRCodeImg").attr("src", linkURLQR);
                        $("#aQRCode").attr("href", linkURLQR);
                        $('#new-item-form').trigger("reset");
                        $('#icon_selection_btn').attr('src', '../icons/SVG/78-video-games/mario-question-box.svg');
                        $('#ni_prod_icon').attr('value', '../icons/SVG/78-video-games/mario-question-box.svg');
                    } else {
                        ErrorMsgModalBox('open', 'errorMsgNewProduct', r);
                    }
                }
            });
        }

        return false;
    });
    // EDIT PRODUCT ITEM
    $("#edit-item-form").submit(function () {

        let valid = validation_edit_product();

        if (valid == true) {
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: $(this).attr('method'),
                beforeSend: function () {
                    $('#LoadingRoller_EditItem').show();
                    $('#editItemSubmit').hide();
                    $('#checkmark_EditItem').hide();
                },
                success: function (r) {
                    if (r == '1') {
                        push_all_items("#box_products", "prod_name");

                        $('#LoadingRoller_EditItem').hide();
                        $('#checkmark_EditItem').show();
                        setTimeout(function () {
                            $('#checkmark_EditItem').hide();
                            $('#editItemSubmit').show();

                            let prodname = $("#ni_prod_name2").val();
                            let prodref = $("#ni_prod_code2").val();
                            let prodprice = $("#ni_prod_price2").val();
                            let produnits = $("#ni_prod_units2").val();
                            let prodtax = $("#ni_prod_tax2").val();
                            $("#itemNamePostAdd").text(prodname);
                            CloseEditItem();
                            CloseItemOptions();
                            refresh_items();
                            $('#edit-item-form').trigger("reset");
                            $('#icon_selection_btn2').attr('src', '../icons/SVG/78-video-games/mario-question-box.svg');
                            $('#ni_prod_icon2').attr('value', '../icons/SVG/78-video-games/mario-question-box.svg');

                        }, 2000);

                    } else {
                        $('#LoadingRoller_EditItem').hide();
                        $('#checkmark_EditItem').hide();
                        $('#editItemSubmit').show();
                        ErrorMsgModalBox('open', 'errorMsgEditProduct', r);
                    }
                }
            });
        }

        return false;
    });

    let newcatopen = false;
    $("#open_new_catg").click(function () {
        if (newcatopen == false) {
            $("#new_category").css({ "height": "190px", "padding": "15px 7px" });
            newcatopen = true;
        } else {
            $("#new_category").css({ "height": "0px", "padding": "0px" });
            $("#msg_cat").css({ "color": "#666", "display": "none" });
            newcatopen = false;
        }

    });

    let newcatopen2 = false;
    $("#open_new_catg2").click(function () {
        if (newcatopen2 == false) {
            $("#new_category2").css({ "height": "190px", "padding": "15px 7px" });
            newcatopen2 = true;
        } else {
            $("#new_category2").css({ "height": "0px", "padding": "0px" });
            $("#msg_cat2").css({ "color": "#666", "display": "none" });
            newcatopen2 = false;
        }

    });


    // ******** HELP WITH TAXES ********
    let data_iva = prod_iva;
    $('#tax-search').keyup(function () {
        let searchField = $(this).val();
        if (searchField === '') {
            $('#tax-filter-records').html('');
            return;
        }
        let regex = new RegExp(quitaacentos(searchField), "ig");
        let output = '';
        let count = 1;

        $.each(data_iva, function (key, val) {
            let nombre = quitaacentos(val.name);
            let cod_ciu = val.ciu.toString();
            if ((nombre.search(regex) != -1) || (cod_ciu.search(regex) != -1)) {
                output += '<div class="item_list_tax" onclick="settax(' + (val.iva) * 100 + ')">';
                output += '<p>' + val.name + '</p>';
                output += '<p> Código:' + cod_ciu + '</p>'
                output += '<p> IVA: ' + (val.iva) * 100 + '%</p>'
                output += '</div>';
                count++;
            }
        });
        $('#tax-filter-records').html(output);

    });

    // ******* SEARCH PRODUCT ******
    $('#items_search').keyup(function () {
        let searchField = sanitizeString($(this).val());
        let table = "";

        if (searchField === '') {
            $('#cont_top_sells').show();
            if (dataProducts.length == 0) {
                table += "<div class='no_prods_msg'><img onclick='OpenNewItem()' src='../icons/SVG/41-shopping/shopping-cart-add-2.svg'> <p> No tienes ningún producto registrado. <span onclick='OpenNewItem()'><b>Crea productos aquí</b></span></p></div>";
            } else {
                for (let i = 0; i < dataProducts.length; i++) {
                    let item = new Item(dataProducts[i]["prod_name"], dataProducts[i]["prod_id"], dataProducts[i]["prod_unit_price"], dataProducts[i]["prod_units"], dataProducts[i]["tax"], dataProducts[i]["category"], dataProducts[i]["cat_color"], dataProducts[i]["prod_icon"], dataProducts[i]["prod_unit_cost"]);
                    table += create_item_box(item);
                }
            }
            $('#box_products').html(table);
            return;
        }

        let regex = new RegExp(quitaacentos(searchField), "ig");
        let output = '';
        let count = 0;
        $.each(dataProducts, function (key, val) {
            let nombre = quitaacentos(val.prod_name);
            let ref_code = val.prod_id;
            let category = val.category;
            if ((nombre.search(regex) != -1) || (ref_code.search(regex) != -1) || (category.search(regex) != -1)) {
                let item = new Item(val.prod_name, val.prod_id, val.prod_unit_price, val.prod_units, val.tax, val.category, val.cat_color, val.prod_icon, val.prod_unit_cost);
                output += create_item_box(item);
                count = count + 1;
            }
        });
        if (searchField.length >= 2 && count == 0) {
            NoOutput = '<div onclick="openNewProdNoData(&#39;' + searchField + '&#39;)" class="ProdSearchNoData"><img src="../icons/general/cactus.svg"><p>SIN RESULTADOS</p><h5>Da click aquí para agregar este producto como nuevo</h5></div>';
            $('#box_products').html(NoOutput);
        } else {
            $('#box_products').html(output);
        }
        $('#cont_top_sells').hide();
    });

    // IMPORT CSV DATA PRODUCT 
    $('#inp_import_csvfile').on("change", function () { uploadCsvFile(); });

    //**** DISCOUNT ****
    $("#cb_discount").click(function () {
        OpenDiscount();
    });
    $("#cb_discount_1").click(function () {
        OpenDiscount();
    });

    $("#discount_perc_option").click(function () {
        $('#lr_cont').hide();
        $("#input_discount").show();
        $("#discount_money_option").css("border", "1px solid #efefef");
        $("#discount_perc_option").css("border", "1px solid #00bb7d");
        $("#input_discount").attr("placeholder", "Ej: 50%");
        $("#input_discount").val('');
        $("#discType").val("perc");
        $("#input_discount").keyup(function (e) {
            if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

            } else if (e.keyCode == 13) {
                $("#input_btn_discount").click();
            } else {
                let discInput = $(this).val();
                let newDiscInput = discInput.replace(/[^0-9.]/g, "") + "%";
                $("#input_discount").val(newDiscInput);
            }
        });
    });
    $("#discount_money_option").click(function () {
        $('#lr_cont').hide();
        $("#input_discount").show();
        $("#discount_perc_option").css("border", "1px solid #efefef");
        $("#discount_money_option").css("border", "1px solid #00bb7d");
        $("#input_discount").attr("placeholder", "Ej: $ 100,000");
        $("#input_discount").val('');
        $("#discType").val("money");
        $("#input_discount").keyup(function (e) {
            if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

            } else if (e.keyCode == 13) {
                $("#input_btn_discount").click();
            } else {
                let discInput = $(this).val();
                let newDiscInput = "$ " + discInput.replace(/[^0-9.]/g, ""); //.formaty()
                $("#input_discount").val(newDiscInput);
            }
        });
    });
    $("#discount_lucapoints_option").click(function () {
        $('#lr_cont').show();
        $("#input_discount").hide();
        $("#discType").val("lucapoints");
        $("#input_discount").val('');
        $("#input_luca_points").focus();
        $('body').addClass('noscroll');
        $("#msg-box-discount").css({ "display": "block" });
        $("#msg-box-container-discount").css({ "display": "block" });
        $("#discount_money_option").css("border", "1px solid #efefef");
        $("#discount_perc_option").css("border", "1px solid #efefef");
    });

    //**** TIPS ****
    $("#cb_tip").click(function () {
        OpenTips();
    });
    $("#cb_tip_1").click(function () {
        OpenTips();
    });
    $("#tips_perc_option").click(function () {
        $("#tips_money_option").css("border", "1px solid #efefef");
        $("#tips_perc_option").css("border", "1px solid #00bb7d");
        $("#input_tips").attr("placeholder", "Ej: 50%");
        $("#input_tips").val('');
        $("#tipsType").val("perc");
        $("#input_tips").keyup(function (e) {
            if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

            } else if (e.keyCode == 13) {
                $("#input_btn_tips").click();
            } else {
                let discInput = $(this).val();
                let newDiscInput = discInput.replace(/[^0-9.]/g, "") + "%";
                $("#input_discount").val(newDiscInput);
            }
        });
    });
    $("#tips_money_option").click(function () {
        $("#tips_perc_option").css("border", "1px solid #efefef");
        $("#tips_money_option").css("border", "1px solid #00bb7d");
        $("#input_tips").attr("placeholder", "Ej: $ 100,000");
        $("#input_tips").val('');
        $("#tipsType").val("money");
        $("#input_tips").keyup(function (e) {
            if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {

            } else if (e.keyCode == 13) {
                $("#input_btn_tips").click();
            } else {
                let discInput = $(this).val();
                let newDiscInput = "$ " + discInput.replace(/[^0-9.]/g, ""); //.formaty()
                $("#input_tips").val(newDiscInput);
            }
        });
    });


    /* MAX DATE IN B-DAY CUSTOMER */
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("ncust_bdate").setAttribute("max", today);


    /******** CHECKOUT BILL ********/
    $("#go_to_co1").click(function () {
        OpenCheckOut1();
        CloseCheckOut2();
        CloseCheckOut3();
    });
    $("#go_to_co2").click(function () {
        CloseCheckOut1();
        OpenCheckOut2();
        CloseCheckOut3();
    });

    /* CHECKOUT 1 */
    $("#checkout_bill").off("click").click(function () {
        checkoutStart();
    });
    $("#little_bill").off("click").click(function () {
        checkoutStart();
    });

    /*CHECKOUT 2*/
    $("#cash_option_other_value").focusin(function () {
        $("#cash_option_other_value").addClass("cash_other_value_active");
        $("#next_other_cash").addClass("next_other_cash_active");
    });
    $("#cash_option_other_value").focusout(function () {
        if ($("#cash_option_other_value").val() == '') {
            $("#cash_option_other_value").removeClass("cash_other_value_active");
            $("#next_other_cash").removeClass("next_other_cash_active");
        }

    });

    $("#cash_option_other_value").focusout(function () {
        CurrencyInput("cash_option_other_value");
    });

    $("#next_other_cash").click(function () {
        TotalValue = curBill.super_total_bill;
        cash_pay = Number($("#cash_option_other_value").val().toString().replace(/[^0-9\.\-]/g, ""));
        if (cash_pay >= TotalValue) {
            cash_change = cash_pay - TotalValue;
            curBill.cash_change = numRound(cash_change, 2);
            $("#change_bill").text(numCommas(curBill.cash_change));
            $("#payment_final_bill").text(numCommas(numRound(cash_pay, 2)));
            curBill.creditCardComprNum = 0;
            curBill.otherPaymentOption = '';
            OpenCheckOut3();
        } else {
            ErrorMsgModalBox('open', 'errorMsgCash', "El valor ingresado no puede ser menor a $ " + numCommas(numRound(TotalValue, 2)));
        }
    });
    $("#next_other").click(function () {
        otherPaymentOptionValue = $("#other_option_value").val().toString();
        if (otherPaymentOptionValue.length > 50) {
            ErrorMsgModalBox('open', 'errorMsgOther', 'El valor es demasiado extenso. Máximo 50 caracteres.');
        } else {
            curBill.otherPaymentOption = otherPaymentOptionValue;
            $("#change_bill").text(numCommas(0));
            $("#payment_final_bill").text(numCommas(numRound(curBill.super_total_bill, 2)));
            curBill.creditCardComprNum = 0;
            curBill.cash_change = 0;
            OpenCheckOut3();
        }
    });

    $("#credit_card").click(function () {
        $(this).addClass("SimpleOptionsBox_selected");
        $("#debit_card").removeClass("SimpleOptionsBox_selected");
        CreditCardChecked = true;
        DebitCardChecked = false;
        $("#card_option_value").focus();
    });
    $("#debit_card").click(function () {
        $(this).addClass("SimpleOptionsBox_selected");
        $("#credit_card").removeClass("SimpleOptionsBox_selected");
        CreditCardChecked = false;
        DebitCardChecked = true;
        $("#card_option_value").focus();
    });
    $("#next_card").click(function () {
        if (CreditCardChecked == false && DebitCardChecked == false) {
            ErrorMsgModalBox('open', 'errorMsgCard', 'Selecciona el tipo de tarjeta', 5000);
        } else {
            if (CreditCardChecked) {
                curBill.paymentType = 2;
            } else if (DebitCardChecked) {
                curBill.paymentType = 3;
            } else {
                curBill.paymentType = 0;
            }
            comprNumber = $("#card_option_value").val().toString().replace(/[^0-9]/g, "");
            if (comprNumber.length <= 10) {
                if (comprNumber.length == 0) {
                    curBill.creditCardComprNum = 0;
                } else {
                    curBill.creditCardComprNum = Number(comprNumber);
                }
                $("#change_bill").text(numCommas(0));
                $("#payment_final_bill").text(numCommas(numRound(curBill.super_total_bill, 2)));
                curBill.cash_change = 0;
                curBill.otherPaymentOption = '';
                OpenCheckOut3();
            } else if (comprNumber.length > 10) {
                ErrorMsgModalBox('open', 'errorMsgCard', 'Número de comprobación demasiado largo. Máximo 10 caracteres.', 5000);
            }
        }
    });

    $("#card_option_value").keyup(function (e) {
        NumericInput("card_option_value", e, false, false);
    });

    /* CHECKOUT 3 */
    $("#endBillTrx").click(function () {

        if (navigator.onLine && curBill.isOffline === false) {

            if (SessionActive() == false) {
                alertify.alert('', 'Debes ingresar a un perfil antes de continuar');
                OpenSalesman_admin_first();
                return false;
            } else {
                profsessid = Cookies.getJSON().PROFSESSID;
            }

            $('#endBillTrx').show();
            let itemJSON = JSON.stringify(curBill.ItemsInBill);

            $.ajax({
                type: "POST",
                url: "push_new_bill.php",
                data: ({
                    trx_value: curBill.super_total_bill,
                    trx_type: '1',
                    id_bill: curBill.bill_id,
                    num_items: curBill.total_items_bill,
                    discount: curBill.discount_bill,
                    tips: curBill.tips_bill,
                    taxes: curBill.tax_bill,
                    id_cupon: '',
                    bill_type: curBill.bill_print_type,
                    payment_type: curBill.paymentType,
                    other_payment_type: curBill.otherPaymentOption,
                    creditcard_compr_number: curBill.creditCardComprNum,
                    cash_change: curBill.cash_change,
                    cust_key: curBill.cust_key,
                    sqlInput: itemJSON,
                    profsessid: profsessid,
                    points_redem: curBill.PointsRedem
                }),
                beforeSend: function () {
                    $('#LoadingRoller_checkout').show();
                    $('#endBillTrx').hide();
                    $('#checkmark_checkout').hide();
                },
                success: function (r) {
                    if (r == '0') {
                        // Tipo de factura
                        if (curBill.bill_print_type == 1) {
                            SendInvoiceByIdBill(curBill.bill_id, 1);
                        } else if (curBill.bill_print_type == 2) {
                            PrintInvoiceByIdBill(curBill.bill_id);
                        }
                        ClearFullBill();
                        curBill = '';
                        push_top_items();
                        $('#LoadingRoller_checkout').hide();
                        $('#checkmark_checkout').show();
                        setTimeout(function () {
                            CloseCheckOut3_clean();
                            $('#checkmark_checkout').hide();
                            $('#endBillTrx').show();
                        }, 2000);

                    } else if (r != '0') {
                        $('#LoadingRoller_checkout').hide();
                        $('#endBillTrx').show();
                        ErrorMsgModalBox('open', 'errorMsgCheckOut', 'Ocurrió un error (E-' + r + ') durante el proceso de registro de venta. Por favor intenta de nuevo.', 5000);
                    }
                }
            });
        } else {

            if (SessionActive() == false) {
                alertify.alert('', 'No tienes un perfil de vendedor activo por lo que no es posible continuar.');
                return false;
            } else {
                curBill.profsessid = Cookies.getJSON().PROFSESSID;
            }
            $('#endBillTrx').hide();
            $('#checkmark_checkout').show();
            setTimeout(function () {
                CloseCheckOut3_clean();
                $('#checkmark_checkout').hide();
                $('#endBillTrx').show();
            }, 2000);
            let acct = GetVariableCookie().CURRACCKEY;
            localStorage.setItem('tempBill-1' + generateRandomNumber(3) + '-' + acct, JSON.stringify(curBill));
            $("#offline_noty").show();
            alertify.message("OFFLINE - Cuenta agregada correctamente a la cola.");
            let offbills = GetOfflineBills();
            $("#num_off_bills").text(offbills.length);
            ClearFullBill();
            curBill = '';
        }
    });

    /********************************/

    /******** PUNTOS LUCA ********/
    $("#num_luca_points").click(function () {
        OpenDiscount();
    });
    $("#red_pnt_cust").click(function () {
        OpenDiscount();
        CloseConsumerInfoPanel();
    });
    /****** SALESMAN **********/
    $("#choose_salesman_bx1").click(function () {
        OpenSalesman_admin();
    });

    $('#lockProfile').click(function () {
        if ($('#lockProfile').prop('checked') == true) {
            $('#passProfile').addClass('mv2a_show');
        }
        if ($('#lockProfile').prop('checked') == false) {
            $('#passProfile').removeClass('mv2a_show');
        }
    });

    $('#BtnOpenCreateProfile').click(function () {
        OpenSalesman();
    });

    $('#choose_avtr').click(function () {
        PopulateAvatars('avtProf', false, 'OpenSalesman');
        OpenSetAvatarProf('CloseSalesman');
    });
    $('#edit_choose_avtr').click(function () {
        PopulateAvatars('avtEditProf', false, 'OpenEditSalesman');
        OpenSetAvatarProf('CloseEditSalesman');
    });

    $('#BtnCreateProfile').click(function () {
        let prof_name = $('#inpNameProfile').val();
        let prof_pass = $('#inpPassProf1').val();
        let prof_pass2 = $('#inpPassProf2').val();
        let avatar = $('#avtProf').attr('src');
        let items_1 = Number($('#cbItmsCre').is(":checked"));
        let items_2 = Number($('#cbItmsEdi').is(":checked"));
        let items_3 = Number($('#cbItmsDel').is(":checked"));
        let customers_1 = Number($('#cbCustCre').is(":checked"));
        let customers_2 = Number($('#cbCustEdi').is(":checked"));
        let customers_3 = Number($('#cbCustDel').is(":checked"));
        let bills_1 = Number($('#cbBillsCre').is(":checked"));
        let bills_2 = Number($('#cbBillsDel').is(":checked"));
        let analytics_1 = Number($('#cbAnalStats').is(":checked"));
        let analytics_2 = Number($('#cbAnalDashb').is(":checked"));

        let isOk = true;
        if ($('#lockProfile').is(':checked') && (prof_pass != prof_pass2)) {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'Las contraseñas no coinciden');
            isOk = false;
        }
        if ($('#lockProfile').is(':checked') && (prof_pass2 == '')) {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'Ingresa la validación de la contraseña');
            $('#inpPassProf2').focus();
            isOk = false;
        }
        if ($('#lockProfile').is(':checked') && (prof_pass == '')) {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'Ingresa la contraseña');
            $('#inpPassProf1').focus();
            isOk = false;
        }
        if ($('#lockProfile').is(':checked') && (isNaN(prof_pass) == true || prof_pass.length != 4)) {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'La contraseña debe ser numerica de 4 digitos');
            $('#inpPassProf1').focus();
            isOk = false;
        }
        if ($('#lockProfile').is(':checked') == false) {
            prof_pass = '';
        }
        if (prof_name == '') {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'Ingresa el nombre del perfil');
            $('#inpNameProfile').focus();
            isOk = false;
        }
        if (prof_name == 'Administrador') {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'El nombre "Administrador" está reservado para el sistema. Por favor ingresa otro nombre.');
            $('#inpNameProfile').focus();
            isOk = false;
        }
        if (prof_name.length > 20) {
            ErrorMsgModalBox('open', 'errorMsgCreateProfile', 'Nombre del perfil debe ser menor a 20 caracteres');
            $('#inpNameProfile').focus();
            isOk = false;
        }
        if (isOk == true) {
            $.ajax({
                url: "push_new_profile.php",
                data: ({
                    prof_name: prof_name
                    , prof_pass: prof_pass
                    , avatar: avatar
                    , items_1: items_1
                    , items_2: items_2
                    , items_3: items_3
                    , customers_1: customers_1
                    , customers_2: customers_2
                    , customers_3: customers_3
                    , bills_1: bills_1
                    , bills_2: bills_2
                    , analytics_1: analytics_1
                    , analytics_2: analytics_2
                }),
                type: "POST",
                success: function (r) {
                    if (r == '1') {
                        CloseSalesman();
                        ClearSalesman();
                        UpdateProfiles();
                    } else {
                        ErrorMsgModalBox('open', 'errorMsgCreateProfile', r);
                    }

                }
            });
        }

    });

    $('#PassProf').keyup(function () {
        if ($('#PassProf').val().length == 4) { $('#BtnPassword').click(); }
    });

    $('#BtnPassword').click(function () {

        let prof_name = ProfilesJSON[curr_profile]['prof_name'];
        let pass = $('#PassProf').val();

        $.ajax({
            url: "check_pass_profile.php",
            data: ({ prof_name: prof_name, pass: pass }),
            type: "POST",
            beforeSend: function () {
                $('#LoadingRoller_PassProf').show();
                $('#BtnPassword').hide();
            },
            success: function (r) {
                $('#LoadingRoller_PassProf').hide();
                $('#BtnPassword').show();
                if (r == 'e-1') {
                    ErrorMsgModalBox('open', 'errorPassword', 'La contraseña es incorrecta');
                    $('#PassProf').focus();
                } else if (r == 's-0') {
                    OpenProfile();
                } else {
                    ErrorMsgModalBox('open', 'errorPassword', 'Se ha producido un error. Intenta de nuevo.');
                }
            }
        });
    });

    $('#BtnSetPassword').click(function () {
        prof_pass = $('#inpPassProf1_b').val();
        prof_pass2 = $('#inpPassProf2_b').val();

        let isOk = true;
        if (prof_pass != prof_pass2) {
            ErrorMsgModalBox('open', 'errorSetPassword', 'Las contraseñas no coinciden');
            $('#inpPassProf2_b').focus();
            isOk = false;
        }
        if (prof_pass2 == '') {
            ErrorMsgModalBox('open', 'errorSetPassword', 'Ingresa la validación de la contraseña');
            $('#inpPassProf2_b').focus();
            isOk = false;
        }
        if (prof_pass == '') {
            ErrorMsgModalBox('open', 'errorSetPassword', 'Ingresa la contraseña');
            $('#inpPassProf1_b').focus();
            isOk = false;
        }
        if (isNaN(prof_pass) == true || prof_pass.length != 4) {
            ErrorMsgModalBox('open', 'errorSetPassword', 'La contraseña debe ser numerica de 4 digitos');
            $('#inpPassProf1_b').focus();
            isOk = false;
        }
        if (prof_chngpass == '') {
            ErrorMsgModalBox('open', 'errorSetPassword', 'No hay un perfil cargado');
            isOk = false;
        }

        if (isOk == true) {
            $.ajax({
                url: "set_pass_profile.php",
                data: ({ prof_name: prof_chngpass, prof_pass: prof_pass }),
                type: "POST",
                success: function (r) {
                    if (r == "1") {
                        CloseSetPassProf();
                        alertify.message("Contraseña creada correctamente");
                        $('#inpPassProf1_b').val('');
                        $('#inpPassProf2_b').val('');
                        prof_chngpass = '';
                        $('#pv_n').text('');
                    } else {
                        ErrorMsgModalBox('open', 'errorSetPassword', 'Ocurrio un error durante el proceso. Por favor intenta de nuevo.');
                    }
                }
            });
        }
    });

    $('#inpPassLuca').focusin(function () {
        $('#passProfileAdmin').addClass('passProfAdminOnFocus');
    });

    $('#inpPassLuca').focusout(function () {
        if ($('#inpPassLuca').val() == '') {
            $('#passProfileAdmin').removeClass('passProfAdminOnFocus');
        }
    });

    $('#BtnOpenAdminCreate').click(function () {
        let pass = $('#inpPassLuca').val();
        let pass_admin = $('#inpPassProfAdmin').val();
        let pass_admin2 = $('#inpPassProfAdmin2').val();

        if (pass == '') {
            ErrorMsgModalBox('open', 'errorMsgAdminCreate', 'Ingresa la contraseña Luca');
            $('#inpPassLuca').focus();
        } else if (pass_admin == '') {
            ErrorMsgModalBox('open', 'errorMsgAdminCreate', 'Ingresa la contraseña de administrador');
            $('#inpPassProfAdmin').focus();
        } else if (pass_admin.length != 4 || isNaN($('#inpPassProfAdmin').val())) {
            ErrorMsgModalBox('open', 'errorMsgAdminCreate', 'La contraseña debe ser numerica de 4 digitos');
            $('#inpPassProfAdmin').focus();
        } else if (pass_admin2 == '') {
            ErrorMsgModalBox('open', 'errorMsgAdminCreate', 'Ingresa el campo de repetir contraseña');
            $('#inpPassProfAdmin2').focus();
        } else if (pass_admin != pass_admin2) {
            ErrorMsgModalBox('open', 'errorMsgAdminCreate', 'Las contraseñas no coinciden');
            $('#inpPassProfAdmin2').focus();
        } else {
            $.ajax({
                url: "create_admin_profile.php",
                data: ({ pass: pass, pass_admin: pass_admin, pass_admin2: pass_admin2 }),
                type: "POST",
                success: function (r) {
                    if (r == '1') {
                        alertify.message('Perfil creado correctamente');
                        OpenSalesman_admin_first();
                        CloseAdminCreator();
                    } else {
                        ErrorMsgModalBox('open', 'errorMsgAdminCreate', r);
                    }
                }
            });
        }
    });
    $('#unlockProfile').change(function () {
        if ($(this).is(":checked")) {
            alertify.alert('Desproteger el perfil', "Esta opción eliminará la protección de este perfil y lo dejará desprotegido.", function () {
                $('#unlockProfile').attr("checked", true);
                $('#passEditProfile').removeClass('mv2a_show');
            });
        } else {
            $('#passEditProfile').addClass('mv2a_show');
        }
    });
    $('#BtnEditProfile').click(function () {
        r = EditProfile_CheckInputs();
        let prof_name = $('#inpNameEditProfileOriginal').val();
        let prof_name_new = $('#inpNameEditProfile').val();
        let prof_pass = $('#inpPassEditProf1').val();
        let avatar = $('#avtEditProf').attr('src');
        let unprotectprofile = Number($('#unlockProfile').is(":checked"));
        let items_1 = Number($('#edit_cbItmsCre').is(":checked"));
        let items_2 = Number($('#edit_cbItmsEdi').is(":checked"));
        let items_3 = Number($('#edit_cbItmsDel').is(":checked"));
        let customers_1 = Number($('#edit_cbCustCre').is(":checked"));
        let customers_2 = Number($('#edit_cbCustEdi').is(":checked"));
        let customers_3 = Number($('#edit_cbCustDel').is(":checked"));
        let bills_1 = Number($('#edit_cbBillsCre').is(":checked"));
        let bills_2 = Number($('#edit_cbBillsDel').is(":checked"));
        let analytics_1 = Number($('#edit_cbAnalStats').is(":checked"));
        let analytics_2 = Number($('#edit_cbAnalDashb').is(":checked"));
        if (r) {
            if (prof_name != prof_name_new) {
                alertify.confirm('Editar Perfil', "¿Seguro deseas cambiar el nombre del perfil de " + prof_name + " por " + prof_name_new + "?", function () {
                    $.ajax({
                        url: "edit_profile.php",
                        data: ({
                            prof_name: prof_name
                            , prof_name_new: prof_name_new
                            , prof_pass: prof_pass
                            , avatar: avatar
                            , unprotectprofile: unprotectprofile
                            , items_1: items_1
                            , items_2: items_2
                            , items_3: items_3
                            , customers_1: customers_1
                            , customers_2: customers_2
                            , customers_3: customers_3
                            , bills_1: bills_1
                            , bills_2: bills_2
                            , analytics_1: analytics_1
                            , analytics_2: analytics_2
                        }),
                        type: "POST",
                        success: function (r) {
                            if (r == "1") {
                                ResetEditProfile();
                                UpdateProfiles();
                                CloseEditSalesman();
                                alertify.message('Perfil actualizado correctamente');
                            } else {
                                ErrorMsgModalBox('open', 'errorMsgEditProfile', 'Ocurrió un error en el proceso, intenta de nuevo');
                            }
                        }
                    }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
                }, function () { });
            } else {
                $.ajax({
                    url: "edit_profile.php",
                    data: ({
                        prof_name: prof_name
                        , prof_name_new: prof_name_new
                        , prof_pass: prof_pass
                        , avatar: avatar
                        , unprotectprofile: unprotectprofile
                        , items_1: items_1
                        , items_2: items_2
                        , items_3: items_3
                        , customers_1: customers_1
                        , customers_2: customers_2
                        , customers_3: customers_3
                        , bills_1: bills_1
                        , bills_2: bills_2
                        , analytics_1: analytics_1
                        , analytics_2: analytics_2
                    }),
                    type: "POST",
                    success: function (r) {
                        if (r == "1") {
                            ResetEditProfile();
                            UpdateProfiles();
                            CloseEditSalesman();
                            alertify.message('Perfil actualizado correctamente');
                        } else {
                            ErrorMsgModalBox('open', 'errorMsgEditProfile', 'Ocurrió un error en el proceso, intenta de nuevo');
                        }
                    }
                });
            }
        }
    });

    $("#QRcmpause").click(function () {
        openQRScanner2('1');
    });

    let EditItemInBill_Pesos = false;
    let EditItemInBill_Porce = false;
    $("#EditItemInBill_Porce").click(function () {
        if (EditItemInBill_Porce == true) {
            $("#edititembill_discount_val").attr("placeholder", "Selecciona el tipo");
            $(this).removeClass("checked");
            EditItemInBill_Porce = false;
        } else {
            $("#edititembill_discount_val").attr("placeholder", "Ej: 50%");
            $("#edititembill_discount_val").focus();
            $("#EditItemInBill_Pesos").removeClass("checked");
            if ($("#edititembill_discount_val").val() != '') {
                PercentInput("edititembill_discount_val", 2);
            }
            $(this).addClass("checked");
            EditItemInBill_Porce = true;
            EditItemInBill_Pesos = false;
        }

    });

    $("#EditItemInBill_Pesos").click(function () {
        if (EditItemInBill_Pesos == true) {
            $("#edititembill_discount_val").attr("placeholder", "Selecciona el tipo");
            $(this).removeClass("checked");
            EditItemInBill_Pesos = false;
        } else {
            $("#edititembill_discount_val").attr("placeholder", "Ej: $ 10,000");
            $("#EditItemInBill_Porce").removeClass("checked");
            $("#edititembill_discount_val").focus();
            if ($("#edititembill_discount_val").val() != '') {
                CurrencyInput("edititembill_discount_val", 2, '$');
            }
            $(this).addClass("checked");
            EditItemInBill_Pesos = true;
            EditItemInBill_Porce = false;
        }
    });

    $("#edititembill_discount_val").focusout(function () {
        if (EditItemInBill_Pesos == true) {
            CurrencyInput("edititembill_discount_val", 2, '$');
        }
        if (EditItemInBill_Porce == true) {
            PercentInput("edititembill_discount_val", 2);
        }
    });

    $("#edititembill_count").focusout(function () {
        DecimalInput("edititembill_count", 3);
    });

    $("#edititembill_form").off("submit").submit(function () {
        flag_1 = 0;
        flag_2 = 0;
        flag_3 = 0;

        // Cantidad
        if ($("#edititembill_count").val() != '') {
            c1 = Number(DecimalFormat($("#edititembill_count").val().toString()));
            if (isNaN(c1)) {
                ErrorMsgModalBox('open', 'errorMsgEdititembill', 'Cantidad invalida');
                $("#edititembill_count").focus();
                return false;
            } else if (c1 <= 0) {
                ErrorMsgModalBox('open', 'errorMsgEdititembill', 'La cantidad debe ser mayor que cero');
                $("#edititembill_count").focus();
                return false;
            } else if (c1 >= 10000) {
                ErrorMsgModalBox('open', 'errorMsgEdititembill', 'La cantidad debe ser menor que 10,000');
                $("#edititembill_count").focus();
                return false;
            } else {
                // CORRECT INPUT
                flag_1 = 1;
            }
        } else {
            flag_1 = 2;
        }

        // **** Adiciones (Solo para restaurante)
        // Get current adiciones of parent
        let adicionesVec = getAdicionesInBill_id(refItemEditBill);
        let n_adds = adicionesVec.length;
        // Remove Adiciones if exist
        for (var i = 0; i < n_adds; i++) {
            deleteAdiciones(refItemEditBill, adicionesVec[i].id, false);
        }

        if ($("#edititembill_adicion").val().length > 0) {
            let nadds = $("#edititembill_adicion").val().length;

            let txtAdds = "";
            txtAdds = (nadds == 1) ? "El ítem tiene 1 adición" : "El ítem tiene " + nadds + " adiciones";
            $("#" + refItemEditBill).children(".item_bill_info").append(" <span id='div_adiciones'> | </span><span title='" + txtAdds + ".' id='adiciones_itemBill' class='mdi mdi-paperclip discount_itemBill'>");

            let txtAdds2 = "<b>Adiciones: </b>";
            txtAdds2 += "<div>";
            for (var i = 0; i < nadds; i++) {
                var splitTxtAdds2 = $("#edititembill_adicion").val()[i].split(" | ");
                var nameAdd = splitTxtAdds2[0] || "";
                var idAdd = splitTxtAdds2[1] || "";
                if (idAdd != "") {
                    if (i == (nadds - 1)) {
                        txtAdds2 += "<span title='Click para remover' class='adic_item' id='" + refItemEditBill + "  " + idAdd + "' onclick='deleteAdiciones(&#39;" + refItemEditBill + "&#39;,&#39;" + idAdd + "&#39;)'>" + nameAdd + "</span>";
                    } else {
                        txtAdds2 += "<span title='Click para remover' class='adic_item' id='" + refItemEditBill + "  " + idAdd + "' onclick='deleteAdiciones(&#39;" + refItemEditBill + "&#39;,&#39;" + idAdd + "&#39;)'>" + nameAdd + " | </span>";
                    }
                    addItemtoBillByIdCode(idAdd);
                }
            }
            txtAdds2 += "</div>";
            $("#" + refItemEditBill).children(".item_bill_adiciones").html("");
            $("#" + refItemEditBill).children(".item_bill_adiciones").append(txtAdds2);
        }

        // Descuento
        if ($("#edititembill_discount_val").val() != '') {

            d = Number(DecimalFormat($("#edititembill_discount_val").val().toString()));
            cp = DecimalFormat($("#" + refItemEditBill).children("#item_bill_amount").text());
            if ((EditItemInBill_Porce == false && EditItemInBill_Pesos == false)) {
                ErrorMsgModalBox('open', 'errorMsgEdititembill', 'Selecciona un tipo de descuento');
                $("#edititembill_discount_val").focus();
                return false;
            } else {
                if (isNaN(d)) {
                    ErrorMsgModalBox('open', 'errorMsgEdititembill', 'Valor de descuento invalido');
                    $("#edititembill_discount_val").focus();
                    return false;
                } else if (d <= 0) {
                    ErrorMsgModalBox('open', 'errorMsgEdititembill', 'El descuento debe ser mayor que 0');
                    $("#edititembill_discount_val").focus();
                    return false;
                } else if (d > 100 && EditItemInBill_Porce == true) {
                    ErrorMsgModalBox('open', 'errorMsgEdititembill', 'El porcentaje de descuento no puede ser mayor a 100%');
                    $("#edititembill_discount_val").focus();
                    return false;
                } else if (d > cp && EditItemInBill_Pesos == true) {
                    ErrorMsgModalBox('open', 'errorMsgEdititembill', 'El porcentaje de descuento no puede ser mayor que el valor del actual ítem ($ ' + numCommas(cp) + ')');
                    $("#edititembill_discount_val").focus();
                    return false;
                } else {
                    // CORRECT
                    flag_2 = 1;
                }
            }

            if ($("#" + refItemEditBill).children(".item_bill_info").children("#discount_itemBill").length > 0) {
                SimpleMsgModalBox('open', 'simpleMsgEdititembill', 'Ya existe un descuento creado para este ítem. Da click aquí para reemplazarlo');
                $("#simpleMsgEdititembill").off();
                $("#simpleMsgEdititembill").bind("click", function (e) {
                    e.preventDefault();
                    deleteDiscountItemBill_silent(refItemEditBill, UnitPriceItemEditBill);
                    $("#edititembill-submit").click();
                });
                $("#edititembill_discount_val").focus();
                return false;
            }

        } else {
            flag_2 = 2;
        }

        // Note
        if ($("#edititembill_note").val() != '') {
            n = $("#edititembill_note").val().trim();
            if (n.length > 50) {
                ErrorMsgModalBox('open', 'errorMsgEdititembill', 'La nota no debe ser mayor a 50 caracteres');
                $("#edititembill_note").focus();
                return false;
            } else {
                flag_3 = 1;
            }

            if ($("#" + refItemEditBill).children(".item_bill_info").children("#note_itemBill").length > 0) {
                SimpleMsgModalBox('open', 'simpleMsgEdititembill', 'Ya existe una nota creada para este ítem. Da click aquí para reemplazarla');
                $("#simpleMsgEdititembill").off();
                $("#simpleMsgEdititembill").bind("click", function (e) {
                    e.preventDefault();
                    deleteNoteItem_silent(refItemEditBill);
                    $("#edititembill-submit").click();
                });
                $("#edititembill_discount_val").focus();
                return false;
            }
        } else {
            flag_3 = 2;
        }

        if (flag_1 > 0 && flag_2 > 0 && flag_3 > 0) {

            if (flag_2 == 1) {
                if (EditItemInBill_Porce == true) {
                    d = (cp * (d / 100));
                    x = cp - d;
                } else if (EditItemInBill_Pesos == true) {
                    x = cp - d;
                }
                if (x <= 0) { x = 0 };
                $("#" + refItemEditBill).children("#item_bill_amount").text(numCommas(numRound(x, 2)));
                $("#" + refItemEditBill).children(".item_bill_info").append(" <span id='div_disc'> | </span><span title='Da click aquí para eliminar este descuento' onclick='deleteDiscountItemBill(&#39;" + refItemEditBill + "&#39;, &#39;" + UnitPriceItemEditBill + "&#39;)' id='discount_itemBill' class='mdi mdi-sale discount_itemBill'></span> <span title='Descuento' id='discount_itemBill_value'> $ " + numCommas(numRound(d, 2)) + "</span>");
            }

            if (flag_3 == 1) {
                $("#" + refItemEditBill).children(".item_bill_info").append(" <span id='div_note'> | </span><span title='" + n + "' onclick='deleteNoteItem(&#39;" + refItemEditBill + "&#39;)' id='note_itemBill' class='mdi mdi-bookmark-outline discount_itemBill'>");
            }

            if (flag_1 == 1) {
                cAct = DecimalFormat($("#" + refItemEditBill).children('#item_bill_count').text());
                vAct = DecimalFormat($("#" + refItemEditBill).children('#item_bill_amount').text());
                dAct = 0;
                if ($("#" + refItemEditBill).children(".item_bill_info").children("#discount_itemBill").length > 0) {
                    dAct = DecimalFormat($("#" + refItemEditBill).children(".item_bill_info").children("#discount_itemBill_value").text());
                }
                unitPrice = UnitPriceItemEditBill;
                $("#" + refItemEditBill).children('#item_bill_count').text('x' + numRound(c1, 3));
                x = (c1 * unitPrice) - dAct;
                if (x < 0) {
                    x = 0;
                }
                $("#" + refItemEditBill).children('#item_bill_amount').text('$ ' + numCommas(numRound(x, 2)));
            }

            RefreshBill();
            CloseEditItemBill();
            $("#edititembill_note").val('');
            $("#edititembill_discount_val").val('');
            $("#edititembill_count").val('');
            $("#EditItemInBill_Porce").removeClass("checked");
            $("#EditItemInBill_Pesos").removeClass("checked");
            return false;
        } else {
            return false;
        }

        return false;

    });

    $("#optionItemEdit").click(function () {
        if (SessionActive() == false) {
            alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
            OpenSalesman_admin_first();
            return false;
        }

        HasPermission('items_2').then(function (pms) {
            if (pms == "1") {
                $("#ni_prod_name2").val(CurrHoldItem.name);
                $("#ni_prod_code2").val(CurrHoldItem.id);
                $("#itemEditId").text(CurrHoldItem.id);
                $("#ni_prod_price2").val('$ ' + numCommas(numRound(CurrHoldItem.price, 2)));
                $("#ni_prod_cost2").val('$ ' + numCommas(numRound(CurrHoldItem.cost, 2)));
                $("#ni_prod_units2").val(CurrHoldItem.units);

                $("#ni_prod_tax2").val(CurrHoldItem.tax);
                $("#icon_selection_btn2").attr('src', CurrHoldItem.icon_src);
                $('#ni_prod_icon2').attr('value', CurrHoldItem.icon_src);

                OpenEditItem();
            } else {
                alertify.alert('Permiso Denegado', 'El perfil actual no tiene permisos para entrar a este modulo.');
            }
        });

    });


    $("#optionItemDelete").click(function () {
        if (SessionActive() == false) {
            alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
            OpenSalesman_admin_first();
            return false;
        }

        HasPermission('items_3').then(function (pms) {
            if (pms == "1") {
                let itemId = $("#configitem_idItem1").text();
                if (itemId != '') {
                    alertify.confirm('Eliminar Ítem', "¿Seguro deseas eliminar el ítem: " + itemId + " permanentemente de la base?", function () {
                        $.ajax({
                            url: "dropItem.php",
                            data: ({ item_id: itemId }),
                            type: "POST",
                            success: function (r) {
                                if (r == 'OK') {
                                    push_all_items("#box_products", "prod_name");
                                    alertify.alert("Ítem Eliminado", "El ítem " + itemId + " fue eliminado correctamente");
                                    CloseItemOptions();
                                } else {
                                    alertify.alert("Upps ...", "Algo ocurrió y el ítem no pudo ser eliminado. Por favor, intenta de nuevo.");
                                }
                            }
                        });
                    }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
                } else {
                    alert('No hay ítem para eliminar');
                }
            } else {
                alertify.alert('Permiso Denegado', 'El perfil actual no tiene permisos para entrar a este modulo.');
            }
        });
    });

    $("#optionItemStats").click(function () {
        if (SessionActive() == false) {
            alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
            OpenSalesman_admin_first();
            return false;
        }

        HasPermission('analytics_1').then(function (pms) {
            if (pms == "1") {
                $("#UnitsStatsItem").text(CurrHoldItem.units);
                $("#UnitsStatsItem2").text(CurrHoldItem.units);

                UpdateItemStats(7);
                OpenItemStats();
            } else {
                alertify.alert('Permiso Denegado', 'El perfil actual no tiene permisos para entrar a este modulo.');
            }
        });


    });

    $("#qrCodeGenerate").click(function () {
        OpenQRCode();
    });

    $("#TimeStats").change(function () {
        if (box1Chart != null) { box1Chart.destroy(); }
        if (box2Chart != null) { box2Chart.destroy(); }
        UpdateItemStats($(this).val());
    });



});



// ****************************************************************
// *********************** INICIO FUNCIONES ***********************
// ****************************************************************

// Get Adiciones by ID of parent inside the bill 
let getAdicionesInBill_id = function (id_parent) {
    var adicionesVec = [];
    var num_adds = $("#" + id_parent + " .item_bill_adiciones div span").length;
    for (var i = 0; i < num_adds; i++) {
        var objAd = $("#" + id_parent).children(".item_bill_adiciones").children("div").children("span")[i];
        var nameAdd = objAd.innerText.replace(" | ", "").replace(" |", "");
        var idAd = objAd.id.split("  ")[1];
        var AddRes = { "name": nameAdd, "id": idAd };
        adicionesVec.push(AddRes);
    }
    return adicionesVec;
}

// ******** CLICK AND HOLD TO OPEN ITEM*************   
function config_item(item_id) {
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    timeoutId = setTimeout(function () {
        let item_full = searchItemById(item_id);
        CurrHoldItem = item_full;
        ConfItem_Set(item_full);
        OpenItemOptions();
    }, 1000);
}

function ConfItem_Set(item) {
    $("#configitem_nameItem1").text(item.name);
    $("#configitem_idItem1").text(item.id);
}
function QRModule_Set(item) {
    let qrlink = URL_QRCode(item.id);
    $("#QRCodeImg2").attr("src", qrlink);
    $("#aQRCode2").attr("href", qrlink);
    $("#linkQRCode").text(qrlink);
}

function URL_QRCode(prodref) {
    return linkURLQR = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&choe=ISO-8859-1&chl=" + prodref.trim();
}

function push_top_items() {
    $.ajax({
        type: "POST",
        url: "topitems.php",
        data: ({ x: '1' }),
        dataType: "json",
        success: function (data) {
            let table = "";
            if (data.length == 0) {
                table += "<div class='no_prods_msg'><img src='../icons/SVG/41-shopping/basket-2.svg'> <p> No has realizado ninguna venta.</p></div>";
            } else {
                for (let i = 0; i < data.length; i++) {
                    let item = new Item(data[i]["prod_name"], data[i]["prod_id"], data[i]["prod_unit_price"], data[i]["prod_units"], data[i]["tax"], data[i]["category"], data[i]["cat_color"], data[i]["prod_icon"], data[i]["prod_unit_cost"]);
                    table += create_item_box(item, 'top', false);
                }
            }
            $("#box_top_sells").html(table);
        }
    });
}

function resetCheckOut() {
    $("#cash_option_other_value").val('');
    $("#card_option_value").val('');
    $("#other_option_value").val('');
    $("#co1_pay_card").removeClass('paymentbox_selected');
    $("#co1_pay_cash").addClass('paymentbox_selected');
    $("#co1_pay_other").removeClass('paymentbox_selected');
    $("#debit_card").removeClass('SimpleOptionsBox_selected');
    $("#credit_card").removeClass('SimpleOptionsBox_selected');
    $("#emailbill_checked").css("display", "none");
    $("#printbill_checked").css("display", "none");
    $("#noprintill_checked").css("display", "none");
    $("#final_points").addClass("hide");
    emailDisable = false;
    emailBillChecked = false;
    printBillChecked = false;
    noPrintBillChecked = false;
    CashChecked = false;
    CreditCardChecked = false;
    DebitCardChecked = false;
    OtherChecked = false;
    curBill = '';
}

function cashOptionsSelected(id) {
    TotalValue = curBill.super_total_bill;
    cash_pay = Number($("#cash_option" + id).text().replace(/[^0-9\.\-]/g, ""));
    cash_change = cash_pay - TotalValue;
    curBill.cash_change = numRound(cash_change, 2);
    $("#change_bill").text(numCommas(curBill.cash_change));
    $("#payment_final_bill").text(numCommas(numRound(cash_pay, 2)));
    curBill.creditCardComprNum = '';
    curBill.otherPaymentOption = '';
    OpenCheckOut3();
}

/*
 * Obtener los datos de la cuenta actual 
*/
let getCurrentBillData = function () {
    this.total_items_bill = Number($('#total_items_bill').text());
    search_consumer = $("#search_consumer").val().toString();
    if (search_consumer != '') {
        this.cust_key = SelectedCustomer;
    } else {
        this.cust_key = '';
    }
    this.tax_bill = Number($("#tax_bill").text().replace(/[^0-9\.\-]/g, ""));
    this.subtotal_bill = Number($("#subtotal_bill").text().replace(/[^0-9\.\-]/g, ""));
    this.discount_bill = Number($("#discount_bill").text().replace(/[^0-9\.\-]/g, ""));
    this.tips_bill = Number($("#tips_bill").text().replace(/[^0-9\.\-]/g, ""));
    this.super_total_bill = Number($("#super_total_bill").text().replace(/[^0-9\.\-]/g, ""));
    this.cash_change = 0;
    this.bill_print_type = '';
    this.creditCardComprNum = 0;
    this.otherPaymentOption = '';
    this.paymentType = 0;
    this.totalbeforetaxes = this.subtotal_bill - this.tax_bill;
    this.paidbyuser = 0;
    this.PointsRedem = 0;

    let ItemsInBill = [];
    $('#cb_cont_items').children('div').each(function () {

        itemId = $(this).attr('id');
        item_a = searchItemById(itemId);
        if (item_a != undefined) {
            itemIcon = item_a["icon_src"];
            itemUnits = item_a["units"];
        } else {
            itemIcon = "../icons/SVG/78-video-games/mario-question-box.svg";
            itemUnits = "Unidades";
        }
        itemCount = Number($(this).children('#item_bill_count').text().replace(/[^0-9.]/g, ""));
        itemAmount = Number($(this).children('#item_bill_amount').text().replace(/[^0-9.]/g, ""));
        itemName = $(this).children('#item_bill_name').children('#nameItemBill').text().trim();
        if ($(this).children(".item_bill_info").children("#discount_itemBill").length > 0) {
            itemDiscount = DecimalFormat($(this).children(".item_bill_info").children("#discount_itemBill_value").text());
        } else {
            itemDiscount = 0;
        }
        if ($(this).children(".item_bill_info").children("#note_itemBill").length > 0) {
            itemNote = $(this).children(".item_bill_info").children("#note_itemBill").attr("title");
        } else {
            itemNote = '';
        }
        itemCoupon = '';

        // Adiciones
        var itemAdds = $("#" + this.id + " .item_bill_adiciones div span").text();
        var adicionesVec = [];
        var itemsAdds_sql = "";
        var num_adds = $("#" + this.id + " .item_bill_adiciones div span").length;
        for (var i = 0; i < num_adds; i++) {
            var objAd = $(this).children(".item_bill_adiciones").children("div").children("span")[i];
            var nameAdd = objAd.innerText.replace(" | ", "").replace(" |", "");
            var idAd = objAd.id.split("  ")[1];
            var AddRes = { "name": nameAdd, "id": idAd };
            itemsAdds_sql = itemsAdds_sql + "(" + idAd + ") " + nameAdd + " ; ";
            adicionesVec.push(AddRes);
        }

        let itemBill = new ItemBill(itemId, itemName, itemCount, itemAmount, itemDiscount, itemNote, itemCoupon, itemIcon, itemUnits, itemAdds, itemsAdds_sql, adicionesVec);
        ItemsInBill.push(itemBill);
    });
    this.ItemsInBill = ItemsInBill;
    if (SelectedCustomer != '') {
        this.PointThisBill = Math.floor(Number($("#super_total_bill").text().replace(/[^0-9.]/g, "")) / PointSystem);
    } else {
        this.PointThisBill = 0;
    }
    this.profsessid = "";
    this.isOffline = false;
    this.timeLocal = moment().format("YYYY-MM-DD HH:mm:ss");
}

function ItemBill(id, name, count, value, discount, note, coupon, icon, units, adiciones_txt_comd, adiciones_txt_sql, adiciones_vec) {
    if (typeof (discount) === undefined) discount = 0;
    if (typeof (coupon) === undefined) coupon = '';
    if (typeof (note) === undefined) note = '';
    if (typeof (adiciones) === undefined) adiciones = '';
    if (typeof (adiciones) === undefined) adiciones = [];
    this.itemId = id;
    this.itemName = name;
    this.itemCount = count;
    this.itemAmount = value;
    this.itemAmountStyle = numCommas(numRound(value, 2));
    this.itemDiscount = discount;
    this.itemCoupon = coupon;
    this.itemNote = note;
    this.itemIcon = icon;
    this.itemAdiciones_txt_comd = adiciones_txt_comd;
    this.itemAdiciones_txt_sql = adiciones_txt_sql;
    this.itemAdiciones_vec = adiciones_vec;
    let icn = icon.replace('..', '');
    icn = icn.replace('/SVG/', '/PNG/');
    icn = icn.replace('.svg', '.png');

    this.itemIconHTTP = "https://www.2luca.co" + icn;
    this.itemUnits = units;
}

let checkCurrResolution = function (alertMsg) {
    alertMsg = (alertMsg === undefined) ? true : alertMsg;
    let defer = $.Deferred();
    $.ajax({
        url: "get_resolution_current.php",
        data: ({ x: 1 }),
        dataType: "json",
        type: "POST",
        success: function (res_curr) {
            if (res_curr.length > 0) {
                if (res_curr[0][0] == "S") {
                    $("#co1_type_print").removeClass("disabled");
                    if (res_curr[0][1].length > 0 && res_curr[0][2] != '') {
                        let curr_factura = parseInt(res_curr[0][2]);
                        let max_fact_res = parseInt(res_curr[0][1][0]["range_reso_last"]);
                        let n_fact_left = (max_fact_res - curr_factura);
                        if (n_fact_left <= 0) {
                            if (res_curr[0][3] == '1') {
                                $("#co1_type_print").addClass("disabled");
                                let al1 = new NotifyAlert("¡Atención!", "Se ha terminado el lote actual de la resolución " + res_curr[0][1][0]["resolution"] + ". Actualiza una nueva resolución para poder generar más facturas.", function () {
                                    window.open("../php/account_mng.php");
                                }, "#333").open();
                            } else {
                                let al1 = new NotifyAlert("¡Atención!", "Se ha terminado el lote actual de la resolución " + res_curr[0][1][0]["resolution"] + ".", function () {
                                    window.open("../php/account_mng.php");
                                }, "#333").open();
                            }
                        } else if (n_fact_left <= 5) {
                            if (res_curr[0][3] == '1') {
                                let al1 = new NotifyAlert("¡Atención!", "La resolución actual está apunto de vencer. Quedan " + n_fact_left + " facturas más para terminar el lote actual. Después del vencimiento no será posible generar más facturas hasta actualizar una nueva resolución.", function () { }, "#333").open();
                            } else {
                                let al1 = new NotifyAlert("¡Atención!", "La resolución actual está apunto de vencer. Quedan " + n_fact_left + " facturas más para terminar el lote actual.", function () { }, "#333").open();
                            }
                        }
                    }
                } else {
                    if (res_curr[0][0] == "F" && res_curr[0][1] == "1") {
                        if (alertMsg) {
                            let al1 = new NotifyAlert("¡Atención!", "No se encontró una resolución activa por lo que no es posible generar una factura. Da click aquí para agregar una resolución.", function () {
                                window.location.href = "../php/account_mng.php";
                            }, "#333").open();
                        }
                    }
                    $("#co1_type_print").addClass("disabled");
                }
            } else {
                res_curr = [];
            }
            defer.resolve(res_curr);
        }
    });
    return defer.promise();
};

let checkoutStart = function () {

    // Check resolution
    checkCurrResolution(false).then((r) => { });

    // Functions
    let checkClientExistance = function (cust_key) {
        let defer = $.Deferred();
        $.ajax({
            url: "check_customer_exist.php",
            data: ({ cust_key: cust_key }),
            dataType: "json",
            type: "POST",
            success: function (res) {
                defer.resolve(res);
            }
        });
        return defer.promise();
    }

    let bl = new BarLoading(15000, "Procesando la compra");
    bl.start();

    $("#offlineTagCheckout").hide();
    $("#co1_type_print").removeClass("disabled");

    // Puntos a redimir
    let pntsRedem = 0;
    if ($('#discType').val() == 'lucapoints' && parseFloat($('#discount_bill').text().replace(/[^0-9]/g, '')) > 0) {
        pntsRedem = parseInt($('#discAp').text().replace(/[^0-9]/g, ''));
    }

    if (SessionActive() == false) {
        bl.stop();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }

    if (SelectedCustomer == "" && $("#search_consumer").val() != '') {
        bl.stop();
        $("#search_consumer").focus();
        alertify.alert("", "Por favor utiliza la lista desplegable para elegir un cliente o ingresa un cliente nuevo en la opción de agregar cliente.");
        return false;
    }

    if (!navigator.onLine) {
        bl.stop();
        let MaxOffLineBills = 30;
        curBill = new getCurrentBillData();
        let offbills = GetOfflineBills();
        if (curBill.ItemsInBill.length > 0) {
            if (offbills.length <= MaxOffLineBills - 1) {
                $("#customer_name").text('CLIENTE');
                $("#co1_type_email").addClass("disabled");
                $("#co1_type_print").addClass("disabled");
                emailDisable = true;
                $("#bill_id").text(offbills.length + 1);
                curBill.PointsRedem = 0;
                curBill.PointThisBill = 0;
                curBill.isOffline = true;
                $("#total_bill").text("$ " + numCommas(numRound(curBill.super_total_bill, 2)));
                $("#co2_cash_total_value").text("$ " + numCommas(numRound(curBill.super_total_bill, 2)));
                checkTypeOfPayment('cash');
                checkTypeOfBill('none');
                let CashOptionsOff = calculateCashAprox(curBill.super_total_bill);
                let strCashOff = '';
                for (let i = 0; i < CashOptionsOff.length; i++) {
                    strCashOff += '<div class="cash_option" id="cash_option' + (i + 1) + '" onclick="cashOptionsSelected(' + (i + 1) + ')">$ ' + numCommas(CashOptionsOff[i]) + '</div>';
                }
                $("#cash_options_cont").html(strCashOff);
                $("#offlineTagCheckout").show();
                bl.stop();
                OpenCheckOut1();
            } else {
                alertify.alert("PROCESAR PAGO - OFFLINE", "No puedes procesar más de " + MaxOffLineBills + " pagos sin conexión. Intenta conectarte a una red para poder continuar.");
            }
        } else {
            alertify.alert("PROCESAR PAGO - OFFLINE", "No hay ningún ítem en la factura");
        }
        return false;
    }

    HasPermission('bills_1').then(function (pms) {
        if (pms == "1") {
            curBill = new getCurrentBillData();

            checkClientExistance(curBill.cust_key).then(function (r) {
                if (r[0]['r'] == "1") {
                    if (curBill.total_items_bill > 0) {

                        let numBill = generateRandomString(7);
                        $("#bill_id").text(numBill);
                        curBill.bill_id = numBill;

                        if (pntsRedem > 0) {
                            if (pntsRedem > parseInt(r[1][0]["cust_points"]) && !isNaN(r[1][0]["cust_points"])) {
                                bl.stop();
                                alertify.alert("", 'El cliente no tiene los puntos suficientes para redimir. Revisa el descuento de nuevo.');
                                OpenDiscount();
                                return false;
                            } else {
                                curBill.PointsRedem = pntsRedem;
                            }
                        } else {
                            curBill.PointsRedem = pntsRedem;
                        }

                        if (SelectedCustomer != '') {
                            $("#customer_name").text(nameHandler(r[1][0]["cust_name"])[1]["shortFullName"].toUpperCase());
                            if (r[1][0]["cust_email"] != '') {
                                $("#co1_type_email").removeClass("disabled");
                                emailDisable = false;
                            } else {
                                $("#co1_type_email").addClass("disabled");
                                emailDisable = true;
                            }
                        } else {
                            $("#customer_name").text('CLIENTE');
                            $("#co1_type_email").addClass("disabled");
                            emailDisable = true;
                        }

                        $("#total_bill").text("$ " + numCommas(numRound(curBill.super_total_bill, 2)));
                        $("#co2_cash_total_value").text("$ " + numCommas(numRound(curBill.super_total_bill, 2)));

                        checkTypeOfPayment('cash');
                        checkTypeOfBill('none');
                        let CashOptions = calculateCashAprox(curBill.super_total_bill);
                        let strCash = '';
                        for (let i = 0; i < CashOptions.length; i++) {
                            strCash += '<div class="cash_option" id="cash_option' + (i + 1) + '" onclick="cashOptionsSelected(' + (i + 1) + ')">$ ' + numCommas(CashOptions[i]) + '</div>';
                        }
                        $("#cash_options_cont").html(strCash);

                        if (SelectedCustomer != '') {
                            $("#final_points").removeClass("hide");
                            if (curBill.PointThisBill == 1) {
                                $("#label_luca_points").text('PUNTO LUCA');
                            } else {
                                $("#label_luca_points").text('PUNTOS LUCA');
                            }
                            $("#points_this_bill").text(numCommas(curBill.PointThisBill));
                        }
                        bl.stop();
                        OpenCheckOut1();
                    } else {
                        bl.stop();
                        alertify.alert("PROCESAR PAGO", "No hay ningún ítem en la factura");
                    }
                } else {
                    bl.stop();
                    alertify.alert("", "El cliente ingresado no existe, por favor utiliza la lista desplegable para elegir un cliente o ingresa un cliente nuevo en la opción de agregar cliente.");
                    return false;
                }
            });
        } else {
            bl.stop();
            alertify.alert('Permiso Denegado', 'El perfil actual no tiene permisos para entrar a este modulo.');
        }
    });

}

function calculateCashAprox(value, num_values) {
    if (typeof (num_values) === 'undefined') num_values = 3;
    let bills = [1000, 2000, 5000, 10000, 20000, 50000, 100000];
    let coins = [100, 200, 500, 1000];
    let all = [100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000];
    let x1 = [];
    let r = [];
    for (let i = 0; i < all.length; i++) {
        x1.push(Math.ceil(value / all[i]) * all[i]);
    }
    let unique = x1.filter(onlyUnique);
    for (let i = 0; i <= (num_values - 1); i++) {
        if (!isNaN(unique[i])) {
            r.push(unique[i]);
        }
    }
    return r;
}

function checkTypeOfBill(type_op) {
    if (type_op == 'email' && emailDisable == false) {
        $("#emailbill_checked").css("display", "block");
        $("#printbill_checked").css("display", "none");
        $("#noprintill_checked").css("display", "none");
        emailBillChecked = true;
        printBillChecked = false;
        noPrintBillChecked = false;
        curBill.bill_print_type = 1;
        OpenCheckOut2();
    } else if (type_op == 'print') {
        $("#emailbill_checked").css("display", "none");
        $("#printbill_checked").css("display", "block");
        $("#noprintill_checked").css("display", "none");
        emailBillChecked = false;
        printBillChecked = true;
        noPrintBillChecked = false;
        curBill.bill_print_type = 2;
        OpenCheckOut2();
    } else if (type_op == 'noprint') {
        $("#emailbill_checked").css("display", "none");
        $("#printbill_checked").css("display", "none");
        $("#noprintill_checked").css("display", "block");
        emailBillChecked = false;
        printBillChecked = false;
        noPrintBillChecked = true;
        curBill.bill_print_type = 3;
        OpenCheckOut2();
    } else if (type_op == 'none') {
        $("#emailbill_checked").css("display", "none");
        $("#printbill_checked").css("display", "none");
        $("#noprintill_checked").css("display", "none");
        emailBillChecked = false;
        printBillChecked = false;
        noPrintBillChecked = false;
        curBill.bill_print_type = 0;
    }
}

function checkTypeOfPayment(type_op) {
    if (type_op == 'card') {
        $("#cardPay_checked").css("display", "block");
        $("#cashPay_checked").css("display", "none");
        $("#otherPay_checked").css("display", "none");
        $("#co1_pay_card").addClass("paymentbox_selected");
        $("#co1_pay_cash").removeClass("paymentbox_selected");
        $("#co1_pay_other").removeClass("paymentbox_selected");
        $("#cash_option_cont").addClass("hide");
        $("#card_option_cont").removeClass("hide");
        $("#other_option_cont").addClass("hide");
        CashChecked = false;
        CreditCardChecked = false;
        DebitCardChecked = false;
        OtherChecked = false;
    } else if (type_op == 'cash') {
        $("#cardPay_checked").css("display", "none");
        $("#cashPay_checked").css("display", "block");
        $("#otherPay_checked").css("display", "none");
        $("#co1_pay_card").removeClass("paymentbox_selected");
        $("#co1_pay_cash").addClass("paymentbox_selected");
        $("#co1_pay_other").removeClass("paymentbox_selected");
        $("#cash_option_cont").removeClass("hide");
        $("#card_option_cont").addClass("hide");
        $("#other_option_cont").addClass("hide");
        CashChecked = true;
        CreditCardChecked = false;
        DebitCardChecked = false;
        OtherChecked = false;
        curBill.paymentType = 1;
    } else if (type_op == 'other') {
        $("#cardPay_checked").css("display", "none");
        $("#cashPay_checked").css("display", "none");
        $("#otherPay_checked").css("display", "block");
        $("#co1_pay_card").removeClass("paymentbox_selected");
        $("#co1_pay_cash").removeClass("paymentbox_selected");
        $("#co1_pay_other").addClass("paymentbox_selected");
        $("#cash_option_cont").addClass("hide");
        $("#card_option_cont").addClass("hide");
        $("#other_option_cont").removeClass("hide");
        CashChecked = false;
        CreditCardChecked = false;
        DebitCardChecked = false;
        OtherChecked = true;
        curBill.paymentType = 4;
    } else if (type_op == 'none') {
        $("#cardPay_checked").css("display", "none");
        $("#cashPay_checked").css("display", "none");
        $("#otherPay_checked").css("display", "none");
        $("#co1_pay_card").removeClass("paymentbox_selected");
        $("#co1_pay_cash").removeClass("paymentbox_selected");
        $("#co1_pay_other").removeClass("paymentbox_selected");
        $("#cash_option_cont").addClass("hide");
        $("#card_option_cont").addClass("hide");
        $("#other_option_cont").addClass("hide");
        CashChecked = false;
        CreditCardChecked = false;
        DebitCardChecked = false;
        OtherChecked = false;
        curBill.paymentType = 0;
    }
}

function searchItemById(item_id) {
    for (let i = 0; i < dataProducts.length; i++) {
        if (dataProducts[i].prod_id == item_id) {
            let item = new Item(dataProducts[i]["prod_name"], dataProducts[i]["prod_id"], dataProducts[i]["prod_unit_price"], dataProducts[i]["prod_units"], dataProducts[i]["tax"], dataProducts[i]["category"], dataProducts[i]["cat_color"], dataProducts[i]["prod_icon"], dataProducts[i]["prod_unit_cost"]);
            return item;
        }
    }
}

function create_item_box(item, section, edit) {
    section = (typeof section !== 'undefined') ? section : "main";
    edit = (typeof edit !== 'undefined') ? edit : true;

    let item_code = "";
    let itemBoxId = section + "_item_" + item.id;
    let itemNotyId = 'sel_' + section + "_item_" + item.id;

    if (isMobileDevice()) {
        item_code += "<div id='" + itemBoxId + "' class='item_box_stl_2' onmousedown='config_item(&#39;" + item.id + "&#39;)' ontouchstart='config_item(&#39;" + item.id + "&#39;)'  onmouseup='restartItem(&#39;" + itemNotyId + "&#39;)' ontouchend='restartItem(&#39;" + itemNotyId + "&#39;)' onmouseleave='restartItem(&#39;" + itemNotyId + "&#39;)' onclick='add_item_to_bill(&#39;" + item.name + "&#39;,&#39;" + item.id + "&#39;,&#39;" + item.price + "&#39;,&#39;" + item.units + "&#39;, &#39;" + item.tax + "&#39;,1)' title='" + item.name + " &#10; $" + numCommas(item.price) + " &#10; " + item.units + " &#10; " + item.id + " &#10; " + item.category + "'>";
    } else {
        if (edit) {
            item_code += "<div id='" + itemBoxId + "' class='item_box_stl_2' onmouseenter='hover_item_home(&#39;" + itemNotyId + "&#39;)' onmousedown='config_item(&#39;" + item.id + "&#39;)' ontouchstart='config_item(&#39;" + item.id + "&#39;)'  onmouseup='restartItem(&#39;" + itemNotyId + "&#39;)' ontouchend='restartItem(&#39;" + itemNotyId + "&#39;)' onmouseleave='restartItem(&#39;" + itemNotyId + "&#39;)' onclick='add_item_to_bill(&#39;" + item.name + "&#39;,&#39;" + item.id + "&#39;,&#39;" + item.price + "&#39;,&#39;" + item.units + "&#39;, &#39;" + item.tax + "&#39;,1)' title='" + item.name + " &#10; $" + numCommas(item.price) + " &#10; " + item.units + " &#10; " + item.id + " &#10; " + item.category + "'>";
        } else {
            item_code += "<div id='" + itemBoxId + "' class='item_box_stl_2' onmousedown='config_item(&#39;" + item.id + "&#39;)' ontouchstart='config_item(&#39;" + item.id + "&#39;)'  onmouseup='restartItem(&#39;" + itemNotyId + "&#39;)' ontouchend='restartItem(&#39;" + itemNotyId + "&#39;)' onmouseleave='restartItem(&#39;" + itemNotyId + "&#39;)' onclick='add_item_to_bill(&#39;" + item.name + "&#39;,&#39;" + item.id + "&#39;,&#39;" + item.price + "&#39;,&#39;" + item.units + "&#39;, &#39;" + item.tax + "&#39;,1)' title='" + item.name + " &#10; $" + numCommas(item.price) + " &#10; " + item.units + " &#10; " + item.id + " &#10; " + item.category + "'>";
        }
    }
    item_code += "<img src='" + item.icon_src + "'>";
    item_code += "<div class='item_style_txt'>";
    item_code += "<p>" + item.id + "</p>";
    item_code += "<p>" + item.name + "</p>";
    item_code += "<p><span class='currency_item_style'>$" + numCommas(item.price) + "</span></p>";
    item_code += "</div>";
    item_code += "<div class='item_catg_div' style='background-color:" + item.color_category + "'></div>";
    if (edit) {
        item_code += "<div id='" + itemNotyId + "' onclick='select_item_home(&#39;" + itemBoxId + "&#39,&#39;" + itemNotyId + "&#39,&#39;" + item.id + "&#39;,event)' class='noty_circle noty-top_right_inside noty_grey'><div class='noty_check_mark'></div></div>";
    }
    item_code += "</div>";
    return item_code;
}
function hover_item_home(itemNotyId) {
    $("#" + itemNotyId).addClass("noty_show");
}
function select_item_home(itemBoxId, itemNotyId, itemID, e) {
    if (e != undefined) {
        e.stopPropagation();
    }
    if ($("#" + itemNotyId + " div.noty_check_mark").css('display') == "none") {
        if (SelectedItemsHome.indexOf(itemID) < 0) {
            $("#" + itemBoxId).addClass("animate_shake");
            $("#" + itemNotyId).removeClass("noty_grey");
            $("#" + itemNotyId).addClass("noty_luca");
            $("#" + itemNotyId + " div.noty_check_mark").show();
            document.getElementById('selectItmsNum').classList.remove("ItemHomeSelected");
            void document.getElementById('selectItmsNum').offsetWidth;
            document.getElementById('selectItmsNum').classList.add("ItemHomeSelected");
            SelectedItemsHome.push(itemID);
        }
    } else {
        if (SelectedItemsHome.indexOf(itemID) > -1) {
            document.getElementById('selectItmsNum').classList.remove("ItemHomeSelected");
            void document.getElementById('selectItmsNum').offsetWidth;
            document.getElementById('selectItmsNum').classList.add("ItemHomeSelected");
            deselect_one_item_home(itemBoxId, itemNotyId, itemID);
            SelectedItemsHome.splice(SelectedItemsHome.indexOf(itemID), 1);
        }
    }
    $("#" + itemNotyId).addClass("noty_show");

    if (SelectedItemsHome.length > 0) {
        $("#selectItmsNum").text(SelectedItemsHome.length);
        show_sel_items_icons();
    } else {
        restart_sel_items();
    }
}

function export_selected_items_csv() {
    if (SelectedItemsHome.length == 0) return false;
    let mr = new MainRoller();
    mr.startMainRoller();
    let itemTxt = "";
    SelectedItemsHome.forEach(function (item) { itemTxt += "'" + item + "'," });
    itemTxt = itemTxt.substring(0, itemTxt.length - 1);

    let headers = { prod_id: 'REFERENCIA', prod_name: 'NOMBRE', prod_unit_price: 'PRECIO', prod_units: 'UNIDADES', category: 'CATEGORIA', tax: 'IMPUESTO' };

    $.ajax({
        url: "get_items_data_stats.php",
        dataType: "json",
        data: ({ itemTxt: itemTxt }),
        type: "POST",
        success: function (r) {
            let items = [];
            if (r.length > 0) {
                r.forEach(function (item) {
                    items.push({
                        prod_id: item.prod_id.replace(/,/g, ''),
                        prod_name: item.prod_name.replace(/,/g, ''),
                        prod_unit_price: item.prod_unit_price.replace(/,/g, ''),
                        prod_units: item.prod_units.replace(/,/g, ''),
                        category: item.category.replace(/,/g, ''),
                        tax: item.tax.replace(/,/g, '')
                    });
                });
                let fileTitle = 'descarga_items';
                mr.stopMainRoller();
                exportCSVFile(headers, items, fileTitle);
            }
        }
    });

}

function select_all_items_home() {
    let itemBoxId, itemNotyId, itemID;
    let n_items = document.getElementById('box_products').getElementsByClassName('item_box_stl_2').length;
    if (n_items > 0) {
        for (let i = 0; i < n_items; i++) {
            itemID = document.getElementById('box_products').getElementsByClassName('item_box_stl_2')[i].id.replace("main_item_", "");
            itemBoxId = document.getElementById('box_products').getElementsByClassName('item_box_stl_2')[i]["id"];
            itemNotyId = "sel_main_item_" + itemID;
            if (!$("#" + itemNotyId).hasClass("noty_show")) {
                select_item_home(itemBoxId, itemNotyId, itemID);
            }
        }
        show_sel_items_icons();
    }

}
function deselect_one_item_home(itemBoxId, itemNotyId, itemID) {
    $("#" + itemBoxId).removeClass("animate_shake");
    $("#" + itemNotyId).addClass("noty_grey");
    $("#" + itemNotyId).removeClass("noty_luca");
    $("#" + itemNotyId + " div.noty_check_mark").hide();
}
function deselect_all_items_home() {
    let section = "main";
    let itemID = "";
    let itemBoxId = "";
    let itemNotyId = "";
    let x = SelectedItemsHome;

    if (x.length > 0) {
        for (let i = 0; i <= x.length - 1; i++) {
            itemID = x[i];
            itemBoxId = section + "_item_" + itemID;
            itemNotyId = 'sel_' + section + "_item_" + itemID;
            $("#" + itemNotyId).removeClass("noty_show");
            deselect_one_item_home(itemBoxId, itemNotyId, itemID);
        }
        SelectedItemsHome = [];
        $("#selectItmsNum").text(SelectedItemsHome.length);
        restart_sel_items();
    }
}
function restartItem(itemNotyId) {
    clearTimeout(timeoutId);
    restartHoverItemSel(itemNotyId);
}
function restart_sel_items() {
    $("#selectItmsExportCSV").addClass("hide");
    $("#selectItmsDel").addClass("hide");
    $("#selectItmsDeselectAll").addClass("hide");
    $("#selectItmsNum").addClass("hide");
    $("#selectItmsNum").text("0");
    $("#selectItmsNum").removeClass("ItemHomeSelected");
}
function show_sel_items_icons() {
    $("#selectItmsExportCSV").removeClass("hide");
    $("#selectItmsDel").removeClass("hide");
    $("#selectItmsDeselectAll").removeClass("hide");
    $("#selectItmsNum").removeClass("hide");
}
function restartHoverItemSel(itemNotyId) {
    //$("#h_item_"+item_id).removeClass("animate_shake");
    if ($("#" + itemNotyId + " div.noty_check_mark").css('display') == "none") {
        $("#" + itemNotyId).removeClass("noty_show");
    }
}
function AddColorSelectionClass(elem) {
    $(".color_crcl").removeClass("color_picked");
    $(elem).addClass("color_picked");
    let x = $(elem).css('backgroundColor');
    colorHEX = hexc(x);
    $("#catg_name").css("border", "2px solid " + colorHEX);
    $("#catg_name2").css("border", "2px solid " + colorHEX);
}

function push_all_items(target_div, order_by_var) {
    let order_by_index = order_by_var;

    $.ajax({
        type: "POST",
        url: "allitemstojson.php",
        data: ({ order_by: order_by_index }),
        dataType: "json",
        success: function (data) {

            dataProducts = data;

            let table = "";
            if (data.length == 0) {
                table += "<div class='no_prods_msg'><img onclick='OpenNewItem()' src='../icons/SVG/41-shopping/shopping-cart-add-2.svg'> <p> No tienes ningún producto registrado. <span onclick='OpenNewItem()'><b>Crea productos aquí</b></span></p></div>";
            } else {
                for (let i = 0; i < data.length; i++) {
                    let item = new Item(data[i]["prod_name"], data[i]["prod_id"], data[i]["prod_unit_price"], data[i]["prod_units"], data[i]["tax"], data[i]["category"], data[i]["cat_color"], data[i]["prod_icon"], data[i]["prod_unit_cost"]);
                    table += create_item_box(item);
                }
            }
            $(target_div).html(table);
        }
    });
}


let push_all_customersOffline = function () {
    if (!navigator.onLine) {
        var options = {
            data: AllCustomersJSON,
            getValue: function (element) {
                let cust_email = element.cust_email;
                let cust_name = element.cust_name;
                let cust_phone = element.cust_phone;
                let cust_key = element.cust_key;
                let cust_id = element.cust_id;
                let cust_type_id = element.cust_type_id;
                let shortName = nameHandler(cust_name)[1]["shortFullName"];
                if (shortName == '') { shortName = cust_name }
                let dataIdCust = cust_id;
                let labCust = cust_name + " - " + dataIdCust;

                return labCust;
            }, list: {
                maxNumberOfElements: 4,
                match: {
                    enabled: true
                },
                onChooseEvent: function () {

                    let cust_name = $("#search_consumer").getSelectedItemData().cust_name;
                    let cust_key = $("#search_consumer").getSelectedItemData().cust_key;
                    let shortName = nameHandler(cust_name)[1]["shortFullName"];
                    if (shortName == '') { shortName = cust_name }
                    if (cust_key != '') { AddCustToSearchBoxOffline(cust_key) }

                }
            },
            template: {
                type: "description",
                fields: {
                    description: "cust_points"
                }
            }
        };

        $("#search_consumer").easyAutocomplete(options);

    }
}

let push_all_customers = function (order_by_var) {

    let options = {
        url: function (phrase) {
            return "get_customers.php";
        },
        getValue: function (element) {
            let cust_email = element.cust_email;
            let cust_name = element.cust_name;
            let cust_phone = element.cust_phone;
            let cust_key = element.cust_key;
            let cust_id = element.cust_id;
            let cust_type_id = element.cust_type_id;
            let shortName = nameHandler(cust_name)[1]["shortFullName"];
            if (shortName == '') { shortName = cust_name }
            let dataIdCust = cust_id;
            let labCust = cust_name + " - " + dataIdCust;

            return labCust;
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: { order_by: order_by_var }
        },
        preparePostData: function (data) {
            data.phrase = $("#search_consumer").val();
            return data;
        },
        list: {
            maxNumberOfElements: 4,
            match: {
                enabled: true
            },
            onChooseEvent: function () {

                let cust_name = $("#search_consumer").getSelectedItemData().cust_name;
                let cust_key = $("#search_consumer").getSelectedItemData().cust_key;
                let shortName = nameHandler(cust_name)[1]["shortFullName"];
                if (shortName == '') { shortName = cust_name }

                if (cust_key != '') {
                    AddCustToSearchBox(cust_key);
                    CustRecommWindow(cust_key, shortName, 0, false);
                }

            }
        },
        template: {
            type: "description",
            fields: {
                description: "cust_points"
            }
        }
    };

    $("#search_consumer").easyAutocomplete(options);

}

function ToggleOrderAuxiliar(id) {
    if ($("#" + id).height() > 0) {
        $("#" + id).removeClass("con_bill_aux_opn");
    } else {
        $("#" + id).addClass("con_bill_aux_opn");
    }
}

function LoadAllConsOrders() {

    let cust_name = $("#infocust_name").text();
    let shortName = nameHandler(cust_name)[1]["shortFullName"];
    if (shortName == '') { shortName = cust_name }

    let cust_key = SelectedCustomer;

    $.ajax({
        url: "get_all_orders_by_cons.php",
        dataType: "json",
        data: ({ cust_key: cust_key }),
        type: "POST",
        success: function (r) {
            $("#allord_cont").html('');
            $("#summ_bill_cons").html('');
            let icn_payt_ty = '';
            let cod = '';
            let TotItems = 0;
            let TotVentas = 0;
            let TotIngreso = 0;

            if (r.length > 0) {
                for (let i = 0; i < r.length; i++) {
                    TotItems = TotItems + parseInt(r[i]['num_items']);
                    TotVentas = TotVentas + 1;
                    TotIngreso = TotIngreso + parseInt(r[i]['trx_value']);

                    cod = cod + '<div id="ord_' + i + '" class="con_bill_line" onclick="ToggleOrderAuxiliar(&#39;ordaux_' + i + '&#39;)">';
                    cod = cod + '<div><div class="textElli60">' + r[i]['id_bill'] + '</div></div><div>' + r[i]['trx_date'].substring(0, 10) + '</div><div>$ ' + numCommas(numRound(r[i]['trx_value'], 0)) + '</div>';
                    if (r[i]['payment_type'] == '1') {
                        icn_payt_ty = 'bank-notes-3.svg';
                    } else if (r[i]['payment_type'] == '2') {
                        icn_payt_ty = 'credit-card-visa.svg';
                    } else if (r[i]['payment_type'] == '3') {
                        icn_payt_ty = 'credit-card.svg';
                    } else if (r[i]['payment_type'] == '4') {
                        icn_payt_ty = 'wallet-3.svg';
                    } else {
                        icn_payt_ty = 'piggy-bank.svg';
                    }
                    cod = cod + '<div><img src="../icons/SVG/44-money/' + icn_payt_ty + '"></div></div>';
                    cod = cod + '<div id="ordaux_' + i + '" class="con_bill_aux"><div class="cba_row1">';
                    cod = cod + '<div><p>' + numCommas(r[i]['num_items']) + '</p><p>Ítems</p></div>';
                    cod = cod + '<div><p>$ ' + numCommas(numRound(r[i]['discount'], 0)) + '</p><p>Descuento</p></div>';
                    cod = cod + '<div><p>$ ' + numCommas(numRound(r[i]['taxes'], 0)) + '</p><p>Impuesto</p></div>';
                    cod = cod + '</div><div onclick="IniciateItemsBill(&#39;' + r[i]["id_bill"] + '&#39;)" class="cba_row2"><p>Ver detalles de la compra</p></div></div>';
                }

                let cod_summ = '';
                cod_summ = cod_summ + '<div><p>' + TotVentas + '</p><p>Ventas</p></div>';
                cod_summ = cod_summ + '<div><p>$ ' + numCommas(numRound(TotIngreso, 0)) + '</p><p>Ingresos</p></div>';
                cod_summ = cod_summ + '<div><p>' + TotItems + '</p><p>ítems</p></div>';

                $("#summ_bill_cons").append(cod_summ);
                $("#allord_cont").append(cod);

            } else {
                alertify.alert("", "El cliente no tiene compras");
            }
        }
    });

}
function IniciateEditCustomer() {

    let cust_name = $("#infocust_name").text();
    let shortName = nameHandler(cust_name)[1]["shortFullName"];
    if (shortName == '') { shortName = cust_name }

    let cust_key = SelectedCustomer;

    $("#ed_cust_name").val('');
    $("#ed_cust_email").val('');
    $("#ed_cust_phone").val('');
    $("#ed_cust_bdate").val('')
    $("#genderMale").prop("checked", false);
    $("#genderFemale").prop("checked", false);
    $("#ed_cust_id_old").val('');
    $("#ed_cust_id").val('');
    $("#ed_cust_type_id").val('1');
    $("#ed_cust_type_id_old").val('1');

    $.ajax({
        url: "get_cust_info.php",
        dataType: "json",
        data: ({ cust_key: cust_key }),
        type: "POST",
        success: function (r) {
            if (r.length > 0) {
                let cust_name = r[0]['cust_name'];
                let cust_email = r[0]['cust_email'];
                let cust_phone = r[0]['cust_phone'];
                let cust_bdate = r[0]['cust_bdate'];
                let cust_gender = r[0]['cust_gender'];
                let cust_date_register = r[0]['cust_date_register'];
                let cust_id = r[0]['cust_id'];
                let cust_type_id = r[0]['cust_type_id'];

                if (cust_name != '') { $("#ed_cust_name").val(cust_name) };
                if (cust_email != '') { $("#ed_cust_email").val(cust_email); }
                if (cust_id != '') { $("#ed_cust_id").val(cust_id); }
                if (cust_id != '') { $("#ed_cust_id_old").val(cust_id); }
                if (cust_type_id != '' && cust_type_id != '0') { $("#ed_cust_type_id").val(cust_type_id); }
                if (cust_type_id != '' && cust_type_id != '0') { $("#ed_cust_type_id_old").val(cust_type_id); }
                if (cust_phone != '' && cust_phone != '0') { $("#ed_cust_phone").val(cust_phone); }
                if (cust_bdate != '' && cust_bdate != '1900-01-01') { $("#ed_cust_bdate").val(cust_bdate) }
                if (cust_gender == '1') { $("#ed_genderMale").prop("checked", true); }
                if (cust_gender == '2') { $("#ed_genderFemale").prop("checked", true); }

                OpenEditCustomer();
            }
        }
    });
}

function GetCustAnalytics() {

    let cust_name = $("#infocust_name").text();
    shortName = nameHandler(cust_name)[1]["shortFullName"];
    if (shortName == '') { shortName = cust_name }

    let cust_key = SelectedCustomer;

    $("#segm_cust_box").text('');
    $("#segm_cust_desc").text('');
    $('#segm_cut_icon').attr('src', '../icons/general/segm_6.svg');
    $("#segm_cont").css({ 'color': '#FFFF', 'background': '#FFFF' });
    $("#nba_cont").css({ 'color': '#FFFF' });
    $("#nba_cust_txt").text('');
    $("#modAnalCust_2").addClass('hide');
    $("#warn_days_lp").removeClass('display-block');
    $("#cptp_1").text('0');
    $("#cptp_2").text('0');
    $("#cptp_3").text('0');
    $("#cptp_4").text('0');
    $("#ca_ventas").text('0');
    $("#ca_ingresos").text('0');
    $("#ca_days_last_prsh").text('-');
    $("#badges_cust").html('');
    $("#descr_badge>p").text('');
    $("#descr_badge").addClass("hide");
    $("#badges_wrap").addClass("hide");
    let b_tips = 0;
    let b_prsh = 0;
    let b_inco = 0;

    $.ajax({
        url: "get_cust_analytics.php",
        dataType: "json",
        data: ({ cust_key: cust_key }),
        type: "POST",
        success: function (r) {
            x1 = r;
            if (r.length > 0) {
                if (r[0] != undefined) {
                    $("#segm_cust_box").text(r[0]['segment']);
                    $("#segm_cust_desc").text(shortName + ' ' + r[0]['description']);
                    $('#segm_cut_icon').attr('src', r[0]['img_path']);
                    $("#segm_cont").css({ 'color': r[0]['text_color'], 'background': r[0]['backgrnd_color'] });
                    $("#nba_cust_txt").text(r[0]['nba']);
                    $("#nba_icon").attr('src', r[0]['nbg_img_path']);
                    $("#nba_cont").css({ 'color': r[0]['text_color'] });
                    $("#modAnalCust_2").removeClass('hide');
                }
                if (r[1] != undefined) {
                    if (r[1]['ventas'] != NaN) {
                        $("#ca_ventas").text(r[1]['ventas']);
                    } else {
                        $("#ca_ventas").text('0');
                    }
                    if (r[1]['ingresos'] != NaN) {
                        $("#ca_ingresos").text(numCommas(numRound(r[1]['ingresos'], 0)));
                    } else {
                        $("#ca_ingresos").text('0');
                    }
                    if (r[1]['days_last_prsh'] != NaN) {
                        $("#ca_days_last_prsh").text(r[1]['days_last_prsh']);
                        if (parseInt(r[1]['days_last_prsh']) >= 100) {
                            $("#warn_days_lp").addClass('display-block');
                        } else {
                            $("#warn_days_lp").removeClass('display-block');
                        }
                    } else {
                        $("#ca_days_last_prsh").text('-');
                    }
                    if (parseInt(r[1]['ventas']) > 0) {
                        let x1 = parseInt(r[1]['cnt_typy_1']) / parseInt(r[1]['ventas']);
                        let x2 = parseInt(r[1]['cnt_typy_2']) / parseInt(r[1]['ventas']);
                        let x3 = parseInt(r[1]['cnt_typy_3']) / parseInt(r[1]['ventas']);
                        let x4 = parseInt(r[1]['cnt_typy_4']) / parseInt(r[1]['ventas']);
                        $("#cptp_1").text(numRound(x1 * 100, 0));
                        $("#cptp_2").text(numRound(x2 * 100, 0));
                        $("#cptp_3").text(numRound(x3 * 100, 0));
                        $("#cptp_4").text(numRound(x4 * 100, 0));
                    } else {
                        $("#cptp_1").text('0');
                        $("#cptp_2").text('0');
                        $("#cptp_3").text('0');
                        $("#cptp_4").text('0');
                    }
                }
                if (r[1] != undefined && r[2] != undefined) {
                    let limit_b_tips = 2;
                    let limit_b_prsh = 2;
                    let limit_b_inco = 1.5;
                    if (parseFloat(r[1]["tips"]) / parseFloat(r[2]["TIPS_AVG"]) > limit_b_tips) {
                        b_tips = 1;
                        let b_tips_val = numRound(((parseFloat(r[1]["tips"]) / parseFloat(r[2]["TIPS_AVG"]))), 1);
                        let b_tips_txt = shortName + ' deja <b>' + b_tips_val + ' veces más propinas</b> que el promedio de las compras.';
                        $("#badges_cust").append('<div id="badge_tip" onclick="selectBadge(&#39;badge_tip&#39;,&#39;' + b_tips_txt + '&#39;)"><img src="../icons/general/badge_tip.svg"><p>Buenas Propinas</p></div>');
                    }
                    if (parseFloat(r[1]["ventas"]) / parseFloat(r[2]["N_VIST_AVG_CUST"]) > limit_b_prsh) {
                        b_prsh = 1;
                        let b_prsh_val = numRound(((parseFloat(r[1]["ventas"]) / parseFloat(r[2]["N_VIST_AVG_CUST"]))), 1);
                        let b_prsh_txt = shortName + ' <b>visita tu negocio ' + b_prsh_val + ' veces más</b> que el promedio de tus clientes.';
                        $("#badges_cust").append('<div id="badge_pursh" onclick="selectBadge(&#39;badge_pursh&#39;,&#39;' + b_prsh_txt + '&#39;)"><img src="../icons/general/badge_pursh.svg"><p>Comprador Estrella</p></div>');
                    }
                    if (parseFloat(r[1]["ingresos"]) / parseFloat(r[2]["INC_AVG_CUST"]) > limit_b_inco) {
                        b_inco = 1;
                        let b_inco_val = numRound(((parseFloat(r[1]["ingresos"]) / parseFloat(r[2]["INC_AVG_CUST"]))), 1);
                        let b_inco_txt = shortName + ' te deja <b>' + b_inco_val + ' veces más ingresos</b> que el promedio de tus clientes.';
                        $("#badges_cust").append('<div id="badge_income" onclick="selectBadge(&#39;badge_income&#39;,&#39;' + b_inco_txt + '&#39;)"><img src="../icons/general/badge_income.svg"><p>Alto Valor</p></div>');
                    }

                    let b_tot = b_tips + b_prsh + b_inco;
                    if (b_tot > 0) { $("#badges_wrap").removeClass("hide"); }

                }
            }
        }
    });
}

function CustomerRecomInfoPanel() {
    let cust_name = $("#infocust_name").text();
    shortName = nameHandler(cust_name)[1]["shortFullName"];
    if (shortName == '') { shortName = cust_name }
    CloseConsumerInfoPanel();

    let cust_key = SelectedCustomer;
    CustRecommWindow(cust_key, shortName, 1, true);
}
function LoadCustomers() {
    let order_by_var = "cust_name";
    $.ajax({
        url: "get_customers.php",
        data: ({ order_by: order_by_var }),
        type: "POST",
        dataType: "json",
        success: function (c) {
            AllCustomersJSON = c;
        }
    });
}
function addRecomItem(item_code) {
    addItemtoBillByIdCode(item_code);
    $("#pr_count_" + item_code).css("display", "block");
    let x = Number($("#pr_count_" + item_code).text());
    $("#pr_count_" + item_code).text(x + 1);
}

function addItemtoBillByIdCode(item_code, noDelete) {
    noDelete = (noDelete === undefined) ? false : noDelete;
    let item = searchItemById(item_code);
    add_item_to_bill(item.name, item.id, item.price, item.units, item.tax, 1, noDelete);
}

function deleteItemtoBillByIdCode(item_code, noAlert) {
    noAlert = (noAlert === undefined) ? true : noAlert;
    let item = searchItemById(item_code);
    delete_item_bill(item.id, item.price, item.tax, noAlert);
}

function AddItemsRecom() {
    CloseItemRecom();
}

function add_item_to_bill(name, id, price, units, tax, cant, noDelete) {

    noDelete = (noDelete === undefined) ? false : noDelete;

    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar a un perfil antes de agregar ítems a la canasta');
        OpenSalesman_admin_first();
        return false;
    }

    if (typeof (cant) === 'undefined') cant = 1;

    price = numRound(price, 2);

    let item_exists = false;

    // Check if id Exist already
    $('#cb_cont_items').children('div').each(function () {
        if ($(this).attr('id') == id) {
            item_exists = true;
        }
    });

    let totPrice = Number(price) * cant;
    totPrice = numRound(totPrice, 2);

    if (!item_exists) {
        // Item does not exist so include it
        let item_in_bill = "";
        let item_id = id;

        item_in_bill += "<div class='item_in_bill' id='" + item_id + "'>";
        item_in_bill += "<p class='item_bill_name' id='item_bill_name' onclick='open_modal_item_bill(&#39;" + item_id + "&#39;,&#39;" + price + "&#39;)'><span class='refItemBill'>" + id + "</span> - <span id='nameItemBill'>" + name + "</span></p>";
        item_in_bill += "<p class='item_bill_count' id='item_bill_count' onclick='open_modal_item_bill(&#39;" + item_id + "&#39;,&#39;" + price + "&#39;)'>x" + cant + "</p>";
        item_in_bill += "<p class='item_bill_amount' id='item_bill_amount'>$" + numCommas(totPrice) + "</p>";
        if (noDelete == false) {
            item_in_bill += "<p class='item_bill_delete' id='item_bill_delete' title='Quitar 1 " + units + "' onclick='delete_item_bill(&#39;" + item_id + "&#39;,&#39;" + price + "&#39;,&#39;" + tax + "&#39;)'><span class='mdi mdi-delete-outline'></span></p>";
        }
        item_in_bill += "<p class='item_bill_info'><span title='Unidades' id='item_bill_units'>" + units + "</span> | <span title='Impuesto' id='item_bill_tax'>" + tax + "%</span> </p>";
        item_in_bill += "<div class='item_bill_adiciones'></div>";
        item_in_bill += "</div>";

        let html_bill = $("#cb_cont_items").html();

        let full_parse = html_bill;
        full_parse += item_in_bill;

        $("#cb_cont_items").html(full_parse);
        //PlaySound("beepsound");
    } else {
        // Item already exist so add one to count
        let num_of_items = $("#" + id + ">#item_bill_count").text().replace("x", "");
        num_of_items = Number(num_of_items);
        let new_num_of_items = num_of_items + cant;
        $("#" + id + ">#item_bill_count").text("x" + new_num_of_items);
        let unit_price = Number(price);
        let total_value_item = numRound(unit_price * new_num_of_items, 2);
        $("#" + id + ">#item_bill_amount").text("$" + numCommas(total_value_item));
        //PlaySound("beepsound");
    }
    RefreshBill();
}

function open_modal_item_bill(item_id, unit_price) {
    let mr = new MainRoller(20000);
    mr.startMainRoller();

    // Cargar opciones de lisata adiciones
    GetAdiciones().then(function (r) {
        mr.stopMainRoller();
        $("#edititembill_adicion").html("");
        if (r.length > 0) {
            for (i = 0; i < r.length; i++) {
                $("#edititembill_adicion").append("<option val='" + r[i].prod_id + "'>" + r[i].prod_name + " | " + r[i].prod_id + "</option>");
            }
        }

        // Adiciones Item
        $('#edititembill_adicion').val(null).trigger('change');
        var num_adds = $("#" + item_id + " .item_bill_adiciones div span").length;
        var x1 = [];
        for (var i = 0; i < num_adds; i++) {
            var objAd = $("#" + item_id).children(".item_bill_adiciones").children("div").children("span")[i];
            var nameAdd = objAd.innerText.replace(" | ", "").replace(" |", "");
            var idAd = objAd.id.split("  ")[1];
            var txt1 = nameAdd + " | " + idAd;
            x1.push(txt1);
        }
        $('#edititembill_adicion').val(x1).trigger('change');

    });

    //item = searchItemById(item_id);
    c = $("#" + item_id).children("#item_bill_count").text().replace(/[^0-9.]/g, "");
    n = $("#" + item_id).children(".item_bill_info").children("#note_itemBill").attr("title");
    if (n == null) { n = '' }

    $("#edititembill_count").val(c);
    $("#edititembill_note").val(n);
    refItemEditBill = item_id;
    UnitPriceItemEditBill = unit_price;
    $("#removeAllItem").click(function () {
        remove_item_bill(refItemEditBill);
        CloseEditItemBill();
    });

    OpenEditItemBill();
}

let deleteAdiciones = function (id_item, id_adicion, noAlert) {
    noAlert = (noAlert === undefined) ? true : noAlert;
    document.getElementById(id_item + "  " + id_adicion).remove();
    deleteItemtoBillByIdCode(id_adicion, noAlert);
}

let delete_item_bill = function (item_id, unit_price, tax, msgAlert) {
    if (typeof (msgAlert) === 'undefined') msgAlert = true;

    unit_price = numRound(Number(unit_price), 2);
    tax = Number(tax);
    let num_of_items = $("#" + item_id + ">#item_bill_count").text().replace("x", "");
    num_of_items = Number(num_of_items);
    let dAct = 0;
    if ($("#" + item_id).children(".item_bill_info").children("#discount_itemBill").length > 0) {
        dAct = DecimalFormat($("#" + item_id).children(".item_bill_info").children("#discount_itemBill_value").text());
    }
    if (num_of_items <= 1) {
        remove_item_bill(item_id);
        RefreshBill();
    } else {
        let new_num_of_items = num_of_items - 1;
        $("#" + item_id + ">#item_bill_count").text("x" + numRound(new_num_of_items, 3));
        let total_value_item = numRound((unit_price * new_num_of_items) - dAct, 2);
        if (total_value_item <= 0) {
            total_value_item = 0;
        }
        $("#" + item_id + ">#item_bill_amount").text("$" + numCommas(total_value_item));
        RefreshBill();

    }
    if (msgAlert) {
        alertify.message(item_id + " fue borrado de la lista");
    }
}

let remove_item_bill = function (item_id) {
    // Delete all adiciones if exist
    let adicionesVec = getAdicionesInBill_id(item_id);
    let n_adds = adicionesVec.length;
    if (n_adds > 0) {
        for (var i = 0; i < n_adds; i++) {
            deleteAdiciones(item_id, adicionesVec[i].id, false);
        }
    }
    $("#" + item_id).remove();

    RefreshBill();
}

function ItemSubTotalCount(type_op, cant) {
    if (typeof (cant) === 'undefined') cant = 1;

    let nitms_t = $("#litmsg_tot_items").text();
    let nitms_n = Number(nitms_t);
    let new_nitms = 0;
    if (type_op == "sum") {
        new_nitms = nitms_n + cant;
    }
    if (type_op == "rest") {
        new_nitms = nitms_n - cant;
    }
    if (new_nitms <= 0) {
        new_nitms = 0;
    }
    $("#litmsg_tot_items").text(new_nitms);
    $("#total_items_bill").text(new_nitms);
}

function ItemSubTotalSumValue(type_op, price) {
    let subtotal_t = $("#subtotal_bill").text().replace(/[^0-9\.\-]/g, "");
    let subtotal_n = Number(subtotal_t);
    let price_n = Number(price);
    let new_subtotal = 0;
    if (type_op == "sum") {
        new_subtotal = subtotal_n + price_n;
    }
    if (type_op == "rest") {
        new_subtotal = subtotal_n - price_n;
    }

    if (new_subtotal <= 0) {
        new_subtotal = 0;
    } else {
        new_subtotal = numRound(new_subtotal, 2);
    }

    if (new_subtotal <= 0) { new_subtotal = 0; }
    $("#subtotal_bill").text(numCommas(new_subtotal));
}

function TotalTaxBillValue(type_op, tax, price) {
    let actual_tax = Number($("#tax_bill").text().replace(/[^0-9\.\-]/g, ""));
    price = Number(price);
    tax = Number(tax);
    let new_tax = 0;
    if (type_op == "sum") {
        new_tax = actual_tax + (price * (tax / 100));
    }
    if (type_op == "rest") {
        new_tax = actual_tax - (price * (tax / 100));
    }
    new_tax = numRound(new_tax, 2);
    if (new_tax <= 0) { new_tax = 0 }
    $("#tax_bill").text(numCommas(new_tax));
}

function TotalBillValue() {
    let subtotal = Number($("#subtotal_bill").text().replace(/[^0-9\.\-]/g, ""));
    let discount = Number($("#discount_bill").text().replace(/[^0-9\.\-]/g, ""));
    let tips = Number($("#tips_bill").text().replace(/[^0-9\.\-]/g, ""));

    if (CurDiscountPer > 0) {
        NewDisc = numRound(subtotal * CurDiscountPer, 2);
        $("#discount_bill").text(numCommas(NewDisc));
        discount = NewDisc;
    }

    if (CurTipsPer > 0) {
        NewTips = numRound(subtotal * CurTipsPer, 2);
        $("#tips_bill").text(numCommas(NewTips));
        tips = NewTips;
    }

    let total = numRound((subtotal + tips - discount), 2);

    if (total < 0) { total = 0; }

    $("#super_total_bill").text(numCommas(total));
    $("#litmsg_tot_value").text(numCommas(total));
}

let RefreshBill = function () {
    Subtotal = 0;
    Taxes = 0;
    Count = 0;
    $('#cb_cont_items').children('div').each(function () {

        // Refresh Adiciones
        var num_adds = $("#" + this.id + " .item_bill_adiciones div span").length;
        $(this).children('.item_bill_info').children("#div_adiciones").remove();
        $(this).children('.item_bill_info').children("#adiciones_itemBill").remove();
        if (num_adds > 0) {
            $(this).children(".item_bill_adiciones").show();
            $(this).children(".item_bill_info").append(" <span id='div_adiciones'> | </span><span title='El ítem tiene " + num_adds + " adiciones.' id='adiciones_itemBill' class='mdi mdi-paperclip discount_itemBill'>");
        } else {
            $(this).children(".item_bill_adiciones").hide();
        }

        // Refresh prices
        c = DecimalFormat($(this).children('#item_bill_count').text());
        v = DecimalFormat($(this).children('#item_bill_amount').text());
        t = DecimalFormat($(this).children('.item_bill_info').children("#item_bill_tax").text());
        units = ($(this).children('.item_bill_info').children("#item_bill_units").text());

        if ((UnidadesMedida.indexOf(quitaacentos(units).toUpperCase()) != -1)) {
            Count = Count + 1;
        } else {
            Count = Count + c;
        }
        Subtotal = Subtotal + (v);
        Taxes = Taxes + ((v) * (t / 100));

    });
    $("#subtotal_bill").text(numCommas(numRound(Subtotal, 2)));
    $("#tax_bill").text(numCommas(numRound(Taxes, 2)));
    $("#total_items_bill").text(numCommas(Count));
    $("#litmsg_tot_items").text(numCommas(Count));

    discount = Number($("#discount_bill").text().replace(/[^0-9\.\-]/g, ""));
    tips = Number($("#tips_bill").text().replace(/[^0-9\.\-]/g, ""));

    if (CurDiscountPer > 0) {
        NewDisc = numRound(Subtotal * CurDiscountPer, 2);
        $("#discount_bill").text(numCommas(NewDisc));
        discount = NewDisc;
    }

    if (CurTipsPer > 0) {
        NewTips = numRound(Subtotal * CurTipsPer, 2);
        $("#tips_bill").text(numCommas(NewTips));
        tips = NewTips;
    }

    total = numRound((Subtotal + tips - discount), 2);
    if (total < 0) { total = 0; }

    $("#super_total_bill").text(numCommas(total));
    $("#litmsg_tot_value").text(numCommas(total));
}

let refresh_items = (function (order_by) {
    if (typeof (order_by) === 'undefined') order_by = "prod_name";
    push_all_items("#box_products", order_by);
    push_top_items();
    alertify.message("Ítems actualizados correctamente");
    restart_sel_items();
});

function ClearConsumerActive() {
    SelectedCustomer = '';
    $("#search_consumer").removeClass("cust_selected");
    $('#search_consumer').attr('readonly', false);
    $("#search_consumer").prop('disabled', false);
    $("#search_consumer").val("");
    $("#num_luca_points").removeClass("noty_show");
    $("#cb_icon_clear_cons").addClass('hide_clearconsumer');
    $("#cb_icon_info_cons").addClass('hide_clearconsumer');
    $("#cb_icon_new_cons").removeClass('hide_new_cons');
    if ($('#discType').val() == 'lucapoints') {
        $("#discount_bill").text("0");
        $('#discAp').text('');
        $('#discType').val('');
        RefreshBill();
    }
}
function DeleteCustomer() {
    alertify.confirm('Borrar Cliente', "¿Seguro desea eliminar el cliente?. Esta acción es irreversible.", function () {
        let cust_key = SelectedCustomer;
        if (cust_key != '') {
            $.ajax({
                url: "dropCustomer.php",
                data: ({ cust_key: cust_key }),
                type: "POST",
                success: function (c) {
                    if (c == '1') {
                        alertify.message("Cliente eliminado correctamente");
                        ClearConsumerActive();
                        CloseConsumerInfoPanel();
                        LoadCustomers();
                    } else {
                        alertify.alert("", "No fue posible eliminar el cliente. Intenta de nuevo.");
                    }
                }
            });
        } else {
            alertify.alert("", "No fue posible eliminar el cliente. Intenta de nuevo.");
        }
    }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
}
function clear_bill() {
    c = 0;
    $('#cb_cont_items').children('div').each(function () {
        c = c + 1;
    });
    if (c > 0) {
        alertify.confirm('Borrar toda la lista', "¿Seguro desea limpiar toda la cuenta?", function () {
            ClearFullBill();
        }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
    } else {
        ClearFullBill();
    }
}
function ClearFullBill() {
    $('#cb_cont_items').children('div').each(function () {
        $(this).remove();
    });
    $("#super_total_bill").text("0");
    $("#subtotal_bill").text("0");
    $("#litmsg_tot_value").text("0");
    $("#tax_bill").text("0");
    $("#litmsg_tot_items").text("0");
    $("#total_items_bill").text("0");
    $("#discount_bill").text("0");
    $("#tips_bill").text("0");
    $("#search_consumer").val("");
    $("#num_luca_points").removeClass("noty_show");
    $("#cb_icon_clear_cons").addClass('hide_clearconsumer');
    $("#cb_icon_info_cons").addClass('hide_clearconsumer');
    $("#cb_icon_new_cons").removeClass('hide_new_cons');
    $("#search_consumer").removeClass("cust_selected");
    $('#search_consumer').attr('readonly', false);
    $("#search_consumer").prop('disabled', false);
    $('#discAp').text('');
    $('#discType').val('');
    ItemsAddedCashier = 1;
    SelectedCustomer = '';
}

function Generate_Code() {
    let name = $("#ni_prod_name").val();
    let namespl = name.split(" ");
    let name_short = "";
    if (namespl.length == 1) {
        name_short = name.substr(0, 3).toUpperCase();
    } else {
        let it = 0;
        for (let i = 0; i < namespl.length; i++) {
            it += 1;
            if (it <= 5) {
                name_short = name_short + namespl[i].substr(0, 1).toUpperCase();
            }
        }
    }
    let rnd_num = Math.floor((Math.random() * 9999) + 1);
    let code = '';
    if (name == '') {
        code = rnd_num;
    } else {
        code = name_short + '-' + rnd_num;
    }
    $("#ni_prod_code").val(code);
}

function validation_new_product() {

    if (!CheckInputText("ni_prod_name", "errorMsgNewProduct", "nombre", true, 0, 50, false, false, true)) { return false; };
    if (!CheckInputText("ni_prod_code", "errorMsgNewProduct", "código/referencia", true, 0, 15, false, false, true)) { return false; };
    if (!CheckInputText("ni_prod_units", "errorMsgNewProduct", "unidades", true, 0, 20, false, true, false)) { return false; };

    let prodtax = parseFloat($("#ni_prod_tax").val().replace(/[^\d\.]/g, ""));
    let prodprice = parseFloat($("#ni_prod_price").val().replace(/[^\d\.\-]/g, ""));
    let prodcost = parseFloat($("#ni_prod_cost").val().replace(/[^\d\.\-]/g, ""));

    if (isNaN(prodprice) || prodprice > 10000000) {
        ErrorMsgModalBox('open', 'errorMsgNewProduct', 'Revisa el valor del precio');
        ErrorInputBox("ni_prod_price");
        return false;
    }

    if ($("#ni_prod_cost").val() != "") {
        if (isNaN(prodcost) || prodcost > 10000000) {
            ErrorMsgModalBox('open', 'errorMsgNewProduct', 'Revisa el valor del cost');
            ErrorInputBox("ni_prod_cost");
            return false;
        }
    }

    if ($("#ni_prod_tax").val() != "") {
        if (isNaN(prodtax) || prodtax > 100 || prodtax < 0) {
            ErrorMsgModalBox('open', 'errorMsgNewProduct', 'Revisa el valor del impuesto');
            ErrorInputBox("ni_prod_tax");
            return false;
        }
    }

    return true;
}

function validation_edit_product() {

    if ($("#ni_prod_name2").val().length > 50) {
        ErrorInputBox("ni_prod_name2");
        return false;
    }
    if ($("#ni_prod_name2").val().length == 0) {
        ErrorInputBox("ni_prod_name2");
        return false;
    }
    if ($("#ni_prod_code2").val().length == 0) {
        ErrorInputBox("ni_prod_code2");
        return false;
    }

    if ($("#ni_prod_units2").val().length > 20) {
        ErrorInputBox("ni_prod_units2");
        return false;
    }
    if ($("#ni_prod_units2").val().length == 0) {
        ErrorInputBox("ni_prod_units2");
        return false;
    }

    if ($("#ni_prod_price2").val().length == 0) {
        ErrorInputBox("ni_prod_price2");
        return false;
    }

    let prodtax = parseFloat($("#ni_prod_tax2").val().replace(/[^\d\.]/g, ""));
    let prodprice = parseFloat($("#ni_prod_price2").val().replace(/[^\d\.\-]/g, ""));
    let prodcost = parseFloat($("#ni_prod_cost2").val().replace(/[^\d\.\-]/g, ""));

    if (isNaN(prodprice) || prodprice > 10000000) {
        ErrorMsgModalBox('open', 'errorMsgEditProduct', 'Revisa el valor del precio');
        ErrorInputBox("ni_prod_price2");
        return false;
    }

    if ($("#ni_prod_cost2").val() != "") {
        if (isNaN(prodcost) || prodcost > 10000000) {
            ErrorMsgModalBox('open', 'errorMsgEditProduct', 'Revisa el valor del cost');
            ErrorInputBox("ni_prod_cost2");
            return false;
        }
    }

    if ($("#ni_prod_tax2").val() != "") {
        if (isNaN(prodtax) || prodtax > 100 || prodtax < 0) {
            ErrorMsgModalBox('open', 'errorMsgEditProduct', 'Revisa el valor del impuesto');
            ErrorInputBox("ni_prod_tax2");
            return false;
        }
    }

    return true;

}


function settax(tax) {
    $('#ni_prod_tax').val(tax + "%");
    CloseHelpTax();
}

function cashier_btn_pressed(action) {
    let actual_value = $("#cash_value").text().replace(/[^0-9.]/g, "");

    if (Number(actual_value) >= 1000000000000) {
        $("#cash_value").text("¡Número Gigante!")
    } else if (action == "0" || action == "1" || action == "2" || action == "3" || action == "4" || action == "5" || action == "6" || action == "7" || action == "8" || action == "9" || action == "000") {
        if (Number(actual_value) == 0) {
            actual_value = action;
        } else {
            actual_value += action;
        }
        $("#cash_value").text('$ ' + Number(actual_value).formaty());

    } else if (action == "erase") {
        actual_value = actual_value.substring(0, actual_value.length - 1);
        $("#cash_value").text('$ ' + Number(actual_value).formaty());
    } else if (action == "clear") {
        $("#cash_value").text("$ 0");
        $("#cash_cant").text("1");
    } else if (action == "ok") {
        let valNum = Number(actual_value);
        if (!isNaN(valNum) && valNum > 0) {
            let rnd_num = Math.floor((Math.random() * 9999) + 1);
            let cantAct = Number($("#cash_cant").text());
            add_item_to_bill("Ítem " + ItemsAddedCashier, 'REF-' + rnd_num, valNum, "Unidades", "0", cantAct);
            ItemsAddedCashier++;
            $("#cash_value").text("$ 0");
            $("#cash_cant").text("1");
            CloseCashier();
        }
    }
}

function quantUp() {
    let cantAct = Number($("#cash_cant").text());
    let NewAct = cantAct + 1;
    if (NewAct > 9000) { NewAct = 9000 };
    $("#cash_cant").text(NewAct);
}
function quantDown() {
    let cantAct = Number($("#cash_cant").text());
    let NewAct = cantAct - 1;
    if (NewAct < 1) { NewAct = 1 };
    $("#cash_cant").text(NewAct);
}

function ApplyTips() {
    let inputDisc = $("#input_tips").val();
    let DiscType = $("#tipsType").val();
    let subtotalActual = Number($("#subtotal_bill").text().replace(/[^0-9.]/g, ""));

    if (inputDisc == "") {
        alertify.warning("Ingresa el valor de la propina");
    } else {
        let disc1 = Number(inputDisc.replace(/[^0-9.]/g, ""));
        disc1 = numRound(disc1, 2);

        if (isNaN(disc1)) {
            $("#tips_bill").text(0);
            TotalBillValue();
            CloseTips();
            return;
        }

        if (DiscType == "perc") {
            if (disc1 < 0 || disc1 > 100) {
                alertify.warning("El porcentaje de propina debe estar entre 0% y 100%");
            } else {
                let valueOfDiscount = subtotalActual * (disc1 / 100);
                valueOfDiscount = numRound(valueOfDiscount, 2)
                CurTipsPer = (disc1 / 100);
                $("#tipsAp").text(" (" + disc1 + "%)");
                $("#tips_bill").text(numCommas(valueOfDiscount));
                TotalBillValue();
                CloseTips();
            }
        }

        if (DiscType == "money") {
            let valueOfDiscount = disc1;
            CurTipsPer = 0;
            $("#tipsAp").text("");
            $("#tips_bill").text(numCommas(valueOfDiscount));
            TotalBillValue();
            CloseTips();
        }
    }
}

function ApplyDiscount() {
    let inputDisc = $("#input_discount").val();
    let DiscType = $("#discType").val();
    let subtotalActual = Number($("#subtotal_bill").text().replace(/[^0-9.]/g, ""));

    if (DiscType == "perc" || DiscType == "money") {
        if (inputDisc == "") {
            alertify.warning("Ingresa el valor del descuento");
        } else {
            let disc1 = Number(inputDisc.replace(/[^0-9.]/g, ""));
            disc1 = numRound(disc1, 2);

            if (isNaN(disc1)) {
                $("#discount_bill").text(0);
                TotalBillValue();
                CloseDiscount();
                return;
            }

            if (DiscType == "perc") {
                if (disc1 < 0 || disc1 > 100) {
                    alertify.warning("El descuento debe estar entre 0% y 100%");
                } else {
                    let valueOfDiscount = subtotalActual * (disc1 / 100);
                    valueOfDiscount = numRound(valueOfDiscount, 2)
                    CurDiscountPer = (disc1 / 100);
                    $("#discAp").text(" (" + disc1 + "%)");
                    $("#discount_bill").text(numCommas(valueOfDiscount));
                    TotalBillValue();
                    CloseDiscount();
                }
            }

            if (DiscType == "money") {
                if (disc1 < 0 || disc1 > subtotalActual) {
                    alertify.warning("El descuento no puede ser mayor que el subtotal actual de $ " + numCommas(subtotalActual));
                } else {
                    let valueOfDiscount = disc1;
                    CurDiscountPer = 0;
                    $("#discAp").text("");
                    $("#discount_bill").text(numCommas(valueOfDiscount));
                    TotalBillValue();
                    CloseDiscount();
                }
            }
        }
    }

    if (DiscType == "lucapoints") {
        if (Number($("#input_luca_points").val()) > 0) {
            $("#discount_bill").text($('#lr_save').text());
            $("#discAp").text(" (" + $("#input_luca_points").val() + " PL)");
            TotalBillValue();
        } else {
            $("#discAp").text("");
            $("#discount_bill").text(0);
            RefreshBill();
        }
        CloseDiscount();
    }

}

function loadLogoGallery_Icon(pathGalleryLogo) {

    if (CurrHoldItem.id != null) {
        $('#icon_selection_btn2').attr('src', pathGalleryLogo);
        $('#ni_prod_icon2').attr('value', pathGalleryLogo);
    } else {
        $('#icon_selection_btn').attr('src', pathGalleryLogo);
        $('#ni_prod_icon').attr('value', pathGalleryLogo);
    }

    CloseGallery();
}

function openQRScanner2(reOpen) {
    // NUEVA FUNCION PARA LEER CODIGOS QR 
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }

    let heightQR = $(".QRVideoScanCont").height();
    //let video = document.createElement("video");
    let canvasElement = document.getElementById("canvasQR");
    let canvas = canvasElement.getContext("2d");

    function drawLine(begin, end, color) {
        canvas.beginPath();
        canvas.moveTo(begin.x, begin.y);
        canvas.lineTo(end.x, end.y);
        canvas.lineWidth = 4;
        canvas.strokeStyle = color;
        canvas.stroke();
    }

    if (ScannerIsRunning == true) {
        Quagga_Toggle('barcodecam_canvas');
    }

    if (heightQR > 0 && reOpen == '0') {
        // Stop the Video Stream
        video.srcObject.getTracks().forEach(function (track) { track.stop(); })

        $(".QRVideoScanCont").css({ "height": "0px" });
    } else {
        $(".QRVideoScanCont").css({ "height": "auto" });
        // Use facingMode: environment to attemt to get the front camera on phones
        navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function (stream) {
            video.srcObject = stream;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.play();
            QRlastShot = new Date();
            requestAnimationFrame(tick);
            $("#QRcmpause").addClass('hide');
        });


        function tick() {
            D3QR = new Date();
            let dls = ((D3QR - QRlastShot) / 1000);
            if (dls < 10) {
                if (video.readyState === video.HAVE_ENOUGH_DATA) {
                    canvasElement.hidden = false;
                    canvasElement.height = video.videoHeight;
                    canvasElement.width = video.videoWidth;
                    canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                    let imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                    let code = jsQR(imageData.data, imageData.width, imageData.height, {
                        inversionAttempts: "dontInvert",
                    });
                    if (code) {
                        drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#00bb7d");
                        drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#00bb7d");
                        drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#00bb7d");
                        drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#00bb7d");
                        D2QR = new Date();
                        let diff_last_scan = ((D2QR - D1QR) / 1000);
                        if (diff_last_scan > 2) {
                            // console.log(code.data);
                            scanQRItem(code.data);
                            D1QR = D2QR;
                            QRlastShot = D2QR;
                        }
                    }
                }
                requestAnimationFrame(tick);

            } else {
                $("#QRcmpause").removeClass('hide');
            }
        }
    }

}
function openQRScanner() {

    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }

    let browser = get_browser_info();

    if (browser.name == "Safari" || browser.name == "IE8+") {
        alertify.alert("Opción no compatible", "Lastimosamente por el momento esta opción no es compatible en navegadores Safari e IE ni en dispositivos iOS. Intenta utilizando otro navegador.");
    } else {
        let heightQR = $(".QRVideoScanCont").height();

        if (ScannerIsRunning == true) {
            Quagga_Toggle('barcodecam_canvas');
        }

        if (heightQR > 0) {
            scanner.stop();
            $(".QRVideoScanCont").css({ "height": "0px" });
        } else {
            $(".QRVideoScanCont").css({ "height": "auto" });

            Instascan.Camera.getCameras().then(function (cameras) {
                let cameralink = "";
                if (cameras.length > 0) {
                    for (let i = 0; i < cameras.length; i++) {
                        let j = i + 1;
                        cameralink += "<p onclick='openQRScanner_2(" + i + ")'><span class='mdi mdi-camera'></span> " + cameras[i].name + "</p>";
                    }
                    $("#cameras_link").html(cameralink);

                    if (cameras.length > 1) {
                        scanner.start(cameras[1]);
                    } else {
                        scanner.start(cameras[0]);
                    }
                } else {
                    alertify.alert("¡Atención!", 'No se encontraron camaras disponibles');
                }
            }).catch(function (e) {
                console.error(e);
            });
        }

    }

}

function openQRScanner_2(camera_id) {
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[camera_id]);
        } else {
            alertify.alert("¡Atención!", 'No se encontraron camaras disponibles');
        }
    }).catch(function (e) {
        console.error(e);
    });
}
function PlaySound(soundObj) {
    let sound = document.getElementById(soundObj);
    sound.play();
}
let checkProdExistsById = function (id) {
    for (let i = 0; i < dataProducts.length - 1; i++) {
        if (dataProducts[i].prod_id.trim() == id.trim()) {
            return true;
        }
    }
    return false;
}
function scanQRItem(dataQR) {
    if (dataQR.length == 0) {
        alertify.message("No se pudo reconocer el código QR del Ítem");
    } else {
        if (checkProdExistsById(dataQR)) {
            addItemtoBillByIdCode(dataQR);
            PlaySound("beepsound");
        } else {
            alertify.message("No se pudo reconocer el código QR del Ítem");
        }
    }
}
function ExitFinishedNewItem() {
    CloseNewItemEnd();
    CloseNewItem();
}
function AddNewItem() {
    CloseNewItemEnd();
    OpenNewItem();
}
function deleteNoteItem(item_id) {
    let id = item_id;
    alertify.confirm("Eliminar Nota", "¿Deseas eliminar la nota actual al ítem " + item_id + '?', function () {
        $("#" + id).children(".item_bill_info").children("#note_itemBill").remove();
        $("#" + id).children(".item_bill_info").children("#div_note").remove();
        RefreshBill();
        alertify.warning("Nota eliminada");
    }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
}
function deleteNoteItem_silent(item_id) {
    let id = item_id;
    $("#" + id).children(".item_bill_info").children("#note_itemBill").remove();
    $("#" + id).children(".item_bill_info").children("#div_note").remove();
    RefreshBill();
}

function deleteDiscountItemBill(item_id, unit_price) {
    let id = item_id;
    alertify.confirm("Eliminar Descuento", "¿Deseas eliminar el descuento actual al ítem " + item_id + '?', function () {
        ad = DecimalFormat($("#" + id).children(".item_bill_info").children("#discount_itemBill_value").text());
        cp = DecimalFormat($("#" + id).children("#item_bill_amount").text());
        cc = DecimalFormat($("#" + id).children("#item_bill_count").text());
        $("#" + id).children('#item_bill_amount').text('$ ' + numCommas(numRound(cc * unit_price, 2)));
        $("#" + id).children(".item_bill_info").children("#discount_itemBill_value").remove();
        $("#" + id).children(".item_bill_info").children("#discount_itemBill").remove();
        $("#" + id).children(".item_bill_info").children("#div_disc").remove();
        RefreshBill();
        alertify.warning("Descuento eliminado");
    }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
}

function deleteDiscountItemBill_silent(item_id, unit_price) {
    let id = item_id;
    ad = DecimalFormat($("#" + id).children(".item_bill_info").children("#discount_itemBill_value").text());
    cp = DecimalFormat($("#" + id).children("#item_bill_amount").text());
    cc = DecimalFormat($("#" + id).children("#item_bill_count").text());
    $("#" + id).children('#item_bill_amount').text('$ ' + numCommas(numRound(cc * unit_price, 2)));
    $("#" + id).children(".item_bill_info").children("#discount_itemBill_value").remove();
    $("#" + id).children(".item_bill_info").children("#discount_itemBill").remove();
    $("#" + id).children(".item_bill_info").children("#div_disc").remove();
    RefreshBill();
}

function UpdateItemStats(ndays) {

    $("#NoSalesItem").addClass("hide");
    $("#infoStatsItem").addClass("hide");
    $("#chartsItem").addClass("hide");

    let ctx = $("#ItemChartLastWeekSales");
    let ctx2 = $("#ItemChartLastWeekSalesValue");

    if (ndays == '7') {
        formatXAxis = "ddd D";
    } else if (ndays == '15') {
        formatXAxis = "D";
    } else if (ndays == '30') {
        formatXAxis = "D"
        ndays = moment().diff(moment().subtract(1, 'month'), 'days') - 1;
    } else if (ndays == '31') {
        formatXAxis = "D"
        ndays = moment().get("date") - 1;
    }

    $.ajax({
        type: "POST",
        url: "ItemStats.php",
        data: ({ item_id: CurrHoldItem.id, num_days: ndays }),
        dataType: "json",
        beforeSend: function () {
            $('#LoadingRoller_itemStat').show();
        },
        success: function (data) {
            $('#LoadingRoller_itemStat').hide();

            dataItemStats1 = data;
            y = [];
            y_full = [];
            x = [];
            x_full = [];
            z = [];
            z_full = [];
            totSalesCount = 0;
            totSalesValue = 0;
            c = 0;
            if (data.length > 0) {

                $("#NoSalesItem").addClass("hide");
                $("#infoStatsItem").removeClass("hide");
                $("#chartsItem").removeClass("hide");
                $("#TimeStatsContainer").show();

                for (let i = 0; i <= ndays; i++) {
                    x1 = moment(moment().subtract(ndays - i, 'days')._d).format(formatXAxis);
                    x_full.push(x1);
                    y_full.push(0);
                    z_full.push(0);
                }

                for (let i = 0; i < data.length; i++) {
                    x_i = moment(data[i].x).format(formatXAxis);
                    y_i = Number(data[i].y);
                    z_i = Number(data[i].z);
                    x.push(x_i);
                    y.push(y_i);
                    z.push(z_i);
                    if (x_full.indexOf(x_i) > -1) {
                        y_full[x_full.indexOf(x_i)] = numRound(y_i, 2);
                        z_full[x_full.indexOf(x_i)] = numRound(z_i, 2);
                    }
                    totSalesCount = totSalesCount + y_i;
                    totSalesValue = totSalesValue + z_i;
                    c = c + 1;
                }

                avgSales = totSalesCount / c;
                $("#TotWeekAvgSales").text(numRound(avgSales, 1))
                avgSalesValue = totSalesValue / c;
                $("#TotWeekAvgSalesValue").text('$ ' + numCommas(numRound(avgSalesValue, 0)));

                $("#TotWeekSales").text(numCommas(numRound(totSalesCount, 2)));
                $("#TotWeekMoney").text('$ ' + numCommas(totSalesValue));

                yMax = Math.max.apply(Math, y);
                //yMax = Math.ceil(yMax * 1.1);
                zMax = Math.max.apply(Math, z);

                box1Chart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: x_full,
                        datasets: [{
                            label: 'Ventas',
                            data: y_full,
                            backgroundColor: 'rgba(0, 187, 125, 0.2)',
                            hoverBackgroundColor: 'rgba(0, 187, 125, 0.3)',
                            borderColor: 'rgba(0, 187, 125, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                display: false,
                                ticks: {
                                    beginAtZero: true,
                                    max: yMax,
                                    min: 0,
                                    stepSize: 1
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    display: false
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }
                });

                box2Chart = new Chart(ctx2, {
                    type: 'line',
                    data: {
                        labels: x_full,
                        datasets: [{
                            label: 'Ventas ($)',
                            data: z_full,
                            backgroundColor: 'rgba(0, 187, 125, 0.2)',
                            hoverBackgroundColor: 'rgba(0, 187, 125, 0.3)',
                            borderColor: 'rgba(0, 187, 125, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                display: false,
                                ticks: {
                                    beginAtZero: true,
                                    max: zMax,
                                    min: 0,
                                    stepSize: 1
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    display: false
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }
                });

            } else {
                $("#NoSalesItem").removeClass("hide");
                $("#infoStatsItem").addClass("hide");
                $("#chartsItem").addClass("hide");
                $("#TimeStatsContainer").show();
            }

        }
    });

}
function PopulateAvatars(idImg, final, modalFunctionAfter) {
    // Final : If the update to the server should be inmediatly

    $('#avtrs_container').empty();
    $('#avtrs_close_mark').empty();

    txtMC = '';
    txtMC = txtMC + '<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseSetAvatarProf(&#39;' + modalFunctionAfter + '&#39;)">X</p>';
    $('#avtrs_close_mark').append(txtMC);

    let AvtrNames = ["blackman1", "blackman2", "blackmanorangetir", "blackmansute", "blackwomandress", "girlearingsgreen", "blondmanredjacket", "boldmanred", "boldmansutebeard", "chaplin", "einstein", "girlafroblackdress", "girlblackdress", "girlblonddressgray", "girlbrownhairsute", "girlreddress", "girlreddresspony", "hpotter", "manblackhat", "manblackjacket", "mangreenjacket", "maninsute", "mansutebrown", "sjobs"];

    txtHtml = '';
    txtHtml = txtHtml + '<div id="AvtChooseProf" class="AvatarProf_Cont">';
    for (let i = 0; i < AvtrNames.length; i++) {
        txtHtml = txtHtml + '<img id="' + AvtrNames[i] + '" onclick="ChooseAvtImg(&#39;' + AvtrNames[i] + '&#39;, &#39;' + idImg + '&#39;, ' + final + ', &#39;' + modalFunctionAfter + '&#39;)" src="../avatars/' + AvtrNames[i] + '.svg">';
    }
    txtHtml = txtHtml + '</div>';

    $('#avtrs_container').append(txtHtml);

}
function ChooseAvtImg(idAvatar, idImg, final, modalFunctionAfter) {
    avatar_dir = "../avatars/" + idAvatar + ".svg";
    $("#" + idImg).attr("src", avatar_dir);

    if (final) {
        prof_name = ProfilesJSON[curr_profile]['prof_name'];
        $.ajax({
            url: "set_avatar_profile.php",
            data: ({ prof_name: prof_name, avatar_dir: avatar_dir }),
            type: "POST",
            success: function (r) {
                if (r == "1") {
                    UpdateProfiles();
                    alertify.message('Avatar actualizado correctamente');
                    curr_profile = '';
                    if (Cookies.getJSON().cp_name == prof_name) {
                        $("#prof_avatar").attr("src", avatar_dir);
                    }
                    CloseSetAvatarProf(modalFunctionAfter);
                } else {
                    alertify.alert('Error', 'Error actualizando el avatar. Por favor, intenta de nuevo.');
                }

            }
        });
    } else {
        CloseSetAvatarProf(modalFunctionAfter);
        curr_profile = '';
    }
}
function ClearSalesman() {
    $('#inpNameProfile').val('');
    $('#inpPassProf1').val('');
    $('#inpPassProf2').val('');
    $('#avtProf').attr('src', '../avatars/manblackjacket.svg');
    $('#cbItmsCre').prop("checked", false);
    $('#cbItmsEdi').prop("checked", false);
    $('#cbItmsDel').prop("checked", false);
    $('#cbCustCre').prop("checked", true);
    $('#cbCustEdi').prop("checked", true);
    $('#cbCustDel').prop("checked", false);
    $('#cbBillsCre').prop("checked", true);
    $('#cbBillsDel').prop("checked", false);
    $('#cbAnalStats').prop("checked", false);
    $('#cbAnalDashb').prop("checked", false);
}

function UpdateProfiles() {
    checkCookiesSupport();
    $('#cont_admin').empty();
    $('#cont_ap').empty();
    $.ajax({
        url: "pull_salesmen.php",
        dataType: "json",
        data: ({ x: '1' }),
        type: "POST",
        beforeSend: function () {
            $('#cont_ap').hide();
            $('#LoadingRoller_Salesmen').show();
        },
        success: function (r) {
            $('#LoadingRoller_Salesmen').hide();
            $('#cont_ap').show();
            ProfilesJSON = r;
            let txtHTML = '';
            let txtHTMLAd = '';
            let posa = 0;
            let contProfiles = 0;
            let adminExist = 0;

            for (let i = 0; i < r.length; i++) {
                if (r[i]['admin'] == "1") {
                    txtHTMLAd = txtHTMLAd + '<div class="av1 ad">';
                    //txtHTMLAd = txtHTMLAd + '<div class="av1c"><img src="../icons/SVG/20-lock/lock-close-1.svg" title="Cambiar Contraseña" onclick="OpenSetPassProf();"></div>';
                    txtHTMLAd = txtHTMLAd + '<div class="av1a" title="Cambiar Avatar"><img id="AvtrAdmin" src="' + r[i]['avatar'] + '" alt="AVATAR" onclick="ChangeProfileAvatar(&#39;' + posa + '&#39;,&#39;AvtrAdmin&#39,true,&#39;OpenSalesman_admin&#39)"></div>';
                    txtHTMLAd = txtHTMLAd + '<div class="av1b adb" onclick="OpenSelectProf(&#39;' + posa + '&#39;)">';
                    txtHTMLAd = txtHTMLAd + '<p>ADMINISTRADOR</p>';
                    txtHTMLAd = txtHTMLAd + '</div>';
                    txtHTMLAd = txtHTMLAd + '</div>';
                    adminExist = 1;
                }
            }
            for (let i = 0; i < r.length; i++) {
                if (r[i]['admin'] != "1") {
                    txtHTML = txtHTML + '<div class="av1">';
                    txtHTML = txtHTML + '<div class="av1c"><img src="../icons/SVG/01-content-edition/pencil-3.svg" title="Editar Perfil" onclick="OpenEditProf(&#39;' + i + '&#39;)">';
                    if (r[i]['has_pass'] == "0") {
                        txtHTML = txtHTML + '<img src="../icons/SVG/20-lock/lock-close-1.svg" title="Proteger perfil" onclick="OpenSetPassProf2(&#39;' + i + '&#39;);">';
                    }
                    txtHTML = txtHTML + '<img src="../icons/SVG/01-content-edition/bin-1.svg" title="Eliminar perfil" onclick="OpenDeleteProf(&#39;' + i + '&#39;)"></div>';
                    txtHTML = txtHTML + '<div class="av1a" title="Cambiar Avatar" onclick="ChangeProfileAvatar(&#39;' + i + '&#39;,&#39;img_xa_' + i + '&#39,true,&#39;OpenSalesman_admin&#39)"><img src="' + r[i]['avatar'] + '" id="img_xa_' + i + '" alt="AVATAR"></div>';
                    txtHTML = txtHTML + '<div class="av1b" onclick=OpenSelectProf(&#39;' + i + '&#39;)>';
                    txtHTML = txtHTML + '<p>' + r[i]['prof_name'].toUpperCase() + '</p>';
                    txtHTML = txtHTML + '<p class="av1b_d">' + moment(r[i]['creation_date']).format('[Desde: ] DD [de] MMMM [del] YYYY') + '</p>';
                    txtHTML = txtHTML + '</div></div> ';
                    contProfiles = contProfiles + 1;
                }
            }
            if (adminExist == 1) {
                $('#cont_admin').empty();
                $('#cont_admin').append(txtHTMLAd);
            } else {
                OpenGuideToCreateAdmin();
            }
            if (contProfiles > 0) {
                $('#cont_ap').empty();
                $('#cont_ap').append(txtHTML);
            } else {
                $('#cont_ap').empty();
                $('#cont_ap').append('<div class="emptyProfiles"><img src="../icons/SVG/63-space/astronaut-2.svg"><p>No tienes ningun perfil creado</p></div>');
            }

        }
    });
}

function ChangeProfileAvatar(idKey, idImg, final, modalFunctionAfter) {
    if (Cookies.getJSON().cp_name == ProfilesJSON[idKey]['prof_name'] || Cookies.getJSON().cp_name == 'Administrador') {
        curr_profile = idKey;
        PopulateAvatars(idImg, final, modalFunctionAfter);
        OpenSetAvatarProf('CloseSalesman_admin');
    } else {
        alertify.alert('', 'Necesitas estar dentro del perfil de ' + ProfilesJSON[idKey]['prof_name'] + ' para acceder a esta opción');
    }
}

function OpenSelectProf(idKey) {
    curr_profile = idKey;
    if (ProfilesJSON[idKey]['has_pass'] == '1') {
        $("#pv_n_2").text(ProfilesJSON[idKey]['prof_name']);
        OpenEntPassProf();
    } else {
        OpenProfile();
    }
}

function OpenEditProf(idKey) {
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        return false;
    }
    ProfileIsAdmin().then(function (r) {
        if (r == "1") {
            DefineEditProfile(idKey);
        } else {
            alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        }
    });
}
function DefineEditProfile(idKey) {
    prof_name = ProfilesJSON[idKey]['prof_name'];
    $.ajax({
        url: "get_vars_profname.php",
        dataType: "json",
        data: ({ prof_name: prof_name }),
        type: "POST",
        success: function (r) {
            ResetEditProfile();
            SetEditProfileModule(r);
            OpenEditSalesman();
            //ClearFullBill();
        }
    });
}
function SetEditProfileModule(r) {
    $('#inpNameEditProfile').val(r[0]['prof_name']);
    $('#inpNameEditProfileOriginal').val(r[0]['prof_name']);
    $('#avtEditProf').attr('src', r[0]['avatar']);
    if (r[0]['has_pass'] == '0') {
        $('#pep_1').addClass('pep_1_nopass');
        $('#pep_1').text('Este perfil no está protegido. Ingresa una contraseña para protegerlo.');
        $('#lblunLockProfile').hide();
    } else if (r[0]['has_pass'] == '1') {
        $('#pep_1').removeClass('pep_1_nopass');
        $('#pep_1').text('Ingresa los siguientes campos si quieres cambiar la contraseña del perfil');
        $('#lblunLockProfile').show();
    }
    (r[0]['items_1'] == '1') ? ($('#edit_cbItmsCre').prop('checked', true)) : ($('#edit_cbItmsCre').prop('checked', false));
    (r[0]['items_1'] == '1') ? ($('#edit_cbItmsEdi').prop('checked', true)) : ($('#edit_cbItmsEdi').prop('checked', false));
    (r[0]['items_1'] == '1') ? ($('#edit_cbItmsDel').prop('checked', true)) : ($('#edit_cbItmsDel').prop('checked', false));
    (r[0]['customers_1'] == '1') ? ($('#edit_cbCustCre').prop('checked', true)) : ($('#edit_cbCustCre').prop('checked', false));
    (r[0]['customers_2'] == '1') ? ($('#edit_cbCustEdi').prop('checked', true)) : ($('#edit_cbCustEdi').prop('checked', false));
    (r[0]['customers_3'] == '1') ? ($('#edit_cbCustDel').prop('checked', true)) : ($('#edit_cbCustDel').prop('checked', false));
    (r[0]['analytics_1'] == '1') ? ($('#edit_cbAnalStats').prop('checked', true)) : ($('#edit_cbAnalStats').prop('checked', false));
    (r[0]['analytics_2'] == '1') ? ($('#edit_cbAnalDashb').prop('checked', true)) : ($('#edit_cbAnalDashb').prop('checked', false));
    (r[0]['bills_1'] == '1') ? ($('#edit_cbBillsCre').prop('checked', true)) : ($('#edit_cbBillsCre').prop('checked', false));
    (r[0]['bills_2'] == '1') ? ($('#edit_cbBillsDel').prop('checked', true)) : ($('#edit_cbBillsDel').prop('checked', false));
}
function ResetEditProfile() {
    $('#inpNameEditProfile').val('');
    $('#inpPassEditProf1').val('');
    $('#inpPassEditProf2').val('');
    $('#passEditProfile').addClass('mv2a_show');
    $('#unlockProfile').prop('checked', false);
    $('#edit_cbItmsCre').prop('checked', false);
    $('#edit_cbItmsEdi').prop('checked', false);
    $('#edit_cbItmsDel').prop('checked', false);
    $('#edit_cbCustCre').prop('checked', false);
    $('#edit_cbCustEdi').prop('checked', false);
    $('#edit_cbCustDel').prop('checked', false);
    $('#edit_cbAnalStats').prop('checked', false);
    $('#edit_cbAnalDashb').prop('checked', false);
    $('#edit_cbBillsCre').prop('checked', false);
    $('#edit_cbBillsDel').prop('checked', false);
}
function EditProfile_CheckInputs() {
    if ($('#inpNameEditProfile').val() == '') {
        ErrorMsgModalBox('open', 'errorMsgEditProfile', 'Ingresa el nombre del perfil');
        ErrorInputBox('inpNameEditProfile');
        return false;
    } else if ($('#inpNameEditProfile').val() == 'Administrador') {
        ErrorMsgModalBox('open', 'errorMsgEditProfile', 'El nombre "Administrador" está reservado para el sistema. Por favor ingresa otro nombre');
        ErrorInputBox('inpNameEditProfile');
        return false;
    } else if ($('#inpNameEditProfile').length > 20) {
        ErrorMsgModalBox('open', 'errorMsgEditProfile', 'Nombre del perfil debe ser menor a 20 caracteres');
        ErrorInputBox('inpNameEditProfile');
        return false;
    } else if ($('#inpPassEditProf1').val() != '' && (isNaN($('#inpPassEditProf1').val()) == true || $('#inpPassEditProf1').val().length != 4)) {
        ErrorMsgModalBox('open', 'errorMsgEditProfile', 'La contraseña debe ser numerica de 4 digitos');
        ErrorInputBox('inpPassEditProf1');
        return false;
    } else if ($('#inpPassEditProf1').val() != $('#inpPassEditProf2').val()) {
        ErrorMsgModalBox('open', 'errorMsgEditProfile', 'Las contraseñas no coinciden');
        ErrorInputBox('inpPassEditProf2');
        return false;
    } else {
        return true;
    }
};
function OpenDeleteProf(idKey) {
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        return false;
    }
    ProfileIsAdmin().then(function (r) {
        if (r == "1") {
            alertify.confirm('Eliminar perfil', "¿Seguro desea eliminar este perfil?", function () {
                $.ajax({
                    url: "delete_profile.php",
                    data: ({ prof_name: ProfilesJSON[idKey]['prof_name'] }),
                    type: "POST",
                    success: function (r) {
                        UpdateProfiles();
                        alertify.message('Perfil borrado correctamente');
                    }
                });
            }, function () { }).set('labels', { ok: 'Sí', cancel: 'Cancelar' });
        } else {
            alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        }
    });
}
function OpenSetPassProf2(idKey) {
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        return false;
    }
    prof_chngpass = ProfilesJSON[idKey]['prof_name'];
    ProfileIsAdmin().then(function (r) {
        if (r == "1") {
            $("#pv_n").text(ProfilesJSON[idKey]['prof_name']);
            OpenSetPassProf();
        } else {
            alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a esta opción');
        }
    });
}
function OpenProfile() {
    $('#PassProf').val('');
    GetKeyProfile(curr_profile);
    StartSessionProfile();
    SetProfile(curr_profile);
    CloseEntPassProf();
    CloseSalesman_admin();
}
function SetProfile() {
    let name = GetVariableCookie('cp_name');
    let avatar = GetVariableCookie('cp_avatar');

    $('#prof_name').text(name.toUpperCase());
    $('#prof_avatar').attr('src', avatar);
    alertify.message('Perfil ' + name.toUpperCase() + ' seleccionado');

    curr_profile = '';
    ProfilesJSON = '';
}

function GetKeyProfile(id) {
    let prof_name = ProfilesJSON[id]['prof_name'];
    $.ajax({
        url: "get_profile_key.php",
        data: ({ prof_name: prof_name }),
        type: "POST",
        success: function (r) {
            Cookies.set('PROFSESSID', r);
        }
    });
}
function GetActSecData() {
    $.ajax({
        url: "get_curr_acct_hsh.php",
        data: ({ x1: 1 }),
        type: "POST",
        success: function (r) {
            Cookies.set('CURRACCKEY', r.trim());
        }
    });
}
function StartSessionProfile() {
    Cookies.set('cp_name', ProfilesJSON[curr_profile]['prof_name']);
    Cookies.set('cp_avatar', ProfilesJSON[curr_profile]['avatar']);
}

function CheckAdminStatus() {
    $.ajax({
        url: "check_admin_stat.php",
        data: ({ x: '1' }),
        type: "POST",
        success: function (r) {
            if (r == 0) {
                OpenGuideToCreateAdmin();
            } else {
                if (SessionActive() == false) {
                    OpenSalesman_admin_first();
                } else {
                    let name_prof = GetVariableCookie('cp_name');
                    SetProfile();
                }

            }
        }
    });
}

function OpenGuideToCreateAdmin() {
    OpenAdminCreator();
}
function Quagga_Init(idBox, deviceId) {
    let heightQR = $(".QRVideoScanCont").height();

    if (heightQR > 0) {
        $(".QRVideoScanCont").css({ "height": "0px" });
    }
    Quagga.init({
        inputStream: {
            name: "Live",
            type: "LiveStream",
            target: document.querySelector('#' + idBox),
            constraints: {
                width: 640,
                height: 480,
                aspectRatio: { min: 1, max: 100 },
                facingMode: "environment",
                deviceId: deviceId
            }
        },
        locator: {
            patchSize: "large",
            halfSample: true
        },
        numOfWorkers: 4,
        locate: true,
        multiple: true,
        decoder: {
            readers: ["ean_reader", "ean_8_reader"],
            debug: {
                showCanvas: true,
                showPatches: true,
                showFoundPatches: true,
                showSkeleton: true,
                showLabels: true,
                showPatchLabels: true,
                showRemainingPatchLabels: true,
                boxFromPatches: {
                    showTransformed: true,
                    showTransformedBox: true,
                    showBB: true
                }
            }
        }
    }, function (err) {
        if (err) {
            console.log(err);
            return;
        }
        Quagga.start();
        ScannerIsRunning = true;
    });

    Quagga.onProcessed(function (result) {
        let drawingCtx = Quagga.canvas.ctx.overlay,
            drawingCanvas = Quagga.canvas.dom.overlay;

        if (result) {
            if (result.boxes) {
                drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                result.boxes.filter(function (box) {
                    return box !== result.box;
                }).forEach(function (box) {
                    Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: "#00bb7d", lineWidth: 2 });
                });
            }

            if (result.box) {
                Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: "#00bb7d", lineWidth: 2 });
            }

            if (result.codeResult && result.codeResult.code) {
                Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: '#ad4f50', lineWidth: 3 });
            }
        }
    });

    Quagga.onDetected(function (r) {
        let TimeScan = new Date();
        let scnSec = (TimeScan.getTime() - startTimeScan.getTime()) / 1000;
        let difScanSec = 2;
        itemExist = false;
        if (scnSec >= difScanSec) {
            startTimeScan = new Date();
            //QuaggaLastResult = r.codeResult.code;
            for (let i = 0; i < dataProducts.length; i++) {
                if (dataProducts[i].prod_id == r.codeResult.code) {
                    // El item ya existe en la base de items
                    PlaySound("beepsound");
                    addItemtoBillByIdCode(r.codeResult.code);
                    itemExist = true;
                    return;
                }
            }
            if (itemExist == false) {
                // Search if exist in BARCODE DATABASE
                SearchBarCodeExt(r.codeResult.code).then(function (res) {
                    if (res.length > 0) {
                        BarcodeFoundScanned = res;
                        ClearBarcodeFound();
                        let eanCode = r.codeResult.code;
                        let eanName = res[0].prod_name_long.trim();
                        let avgEanPrice = parseFloat(res[1].AVG_PRICE.trim());
                        let avgEanPriceFormat = numCommas(res[1].AVG_PRICE.trim());
                        let eanIconPath = res[0].prod_icon.trim();
                        let eanUnits = res[0].prod_units.trim();
                        let imgBarcodeURL = '';

                        if (eanCode.length == 13) {
                            imgBarcodeURL = "<img src='https://barcode.tec-it.com/barcode.ashx?data=" + eanCode + "&code=EAN13&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0'/>";
                        }
                        $('#barcode_fnd_name').text(eanName);
                        $('#barcode_img_cont').append(imgBarcodeURL);
                        $('#barcode_img_cont').removeClass('hide');
                        $('#barcode_fnd_icon').attr('src', eanIconPath);
                        OpenBarcodeFound();

                        $("#btn_BarcodeEditAndAdd").click(function () {
                            (eanName != '' && eanName != undefined) ? $("#ni_prod_name").val(eanName) : $("#ni_prod_name").val('');
                            (eanCode != '' && eanCode != undefined) ? $("#ni_prod_code").val(eanCode) : $("#ni_prod_code").val('');
                            (avgEanPriceFormat != '' && avgEanPriceFormat != undefined) ? $("#ni_prod_price").val(avgEanPriceFormat) : $("#ni_prod_price").val('');
                            (eanUnits != '' && eanUnits != undefined) ? $("#ni_prod_units").val(eanUnits) : $("#ni_prod_units").val('Unidades');
                            (eanIconPath != '' && eanIconPath != undefined) ? $("#icon_selection_btn").attr('src', eanIconPath) : $("#icon_selection_btn").attr('src', '../icons/SVG/78-video-games/mario-question-box.svg');
                            (eanIconPath != '' && eanIconPath != undefined) ? $("#ni_prod_icon").val(eanIconPath) : $("#ni_prod_icon").val('../icons/SVG/78-video-games/mario-question-box.svg');
                            CloseBarcodeFound();
                            OpenNewItem();
                        });
                    } else {
                        // Agregar el item
                        alertify.confirm('Agregar nuevo ítem', "El ítem escaneado de referencia " + r.codeResult.code + " no existe. ¿Deseas agregarlo a tus ítems?", function () {
                            $('#ni_prod_code').val(r.codeResult.code);
                            OpenNewItem();
                        }, function () { });
                    }
                });


            }
        }
    });
}
function ClearBarcodeFound() {
    $('#barcode_fnd_name').text('');
    $('#barcode_img_cont').html('');
    $('#barcode_fnd_icon').attr('src', '');
    $('#barcode_img_cont').addClass('hide');
    BarcodeFoundScanned = [];
}
function Quagga_Toggle(idBox) {
    if (video.srcObject != null) {
        video.srcObject.getTracks().forEach(function (track) { track.stop(); })
    }
    if (ScannerIsRunning == true) {
        $('#barcodecam_cont').removeClass('barcodecam_cont_opn');
        Quagga.stop();
        ScannerIsRunning = false;
    } else {
        // Set the cameras
        navigator.mediaDevices.enumerateDevices().then(function (devices) {
            let cameralink = '';
            let cameras_len = 0;
            let camerasID = [];
            devices.forEach(function (device) {
                if (device.kind == 'videoinput') {
                    cameralink += "<p onclick='Quagga_Init(&#39;" + idBox + "&#39;,&#39;" + device.deviceId + "&#39;)'><span class='mdi mdi-camera'></span> " + device.label + "</p>";
                    camerasID[cameras_len] = device.deviceId;
                    cameras_len += 1;
                }
            });
            $("#cameras_link2").html(cameralink);
            if (cameras_len > 0) {
                if (cameras_len > 1) {
                    Quagga_Init(idBox, camerasID[1]);
                } else {
                    Quagga_Init(idBox, camerasID[0]);
                }
            } else {
                alertify.alert("Escanear Código", "No se encontraron camaras disponibles");
                return false;
            }

        }).catch(function (err) {
            console.log(err.name + ": " + err.message);
        });

        $('#barcodecam_cont').addClass('barcodecam_cont_opn');
    }
}

function openNewProdNoData(name) {
    $('#ni_prod_name').val(name);
    OpenNewItem();
}
function goto_scan(id) {
    $('#help_scan_1').removeClass('box_help_scan_opn');
    $('#help_scan_2').removeClass('box_help_scan_opn');
    $('#help_scan_3').removeClass('box_help_scan_opn');

    $('#help_scan_' + id).addClass('box_help_scan_opn');
}
function uploadCsvFile() {

    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        alertify.alert('Navegador no compatible', 'Tu navegador no soporta este tipo de proceso de carga de archivos. Por favor intenta desde otro navegador.');
        return;
    }
    $('#msg_idatprod').empty();
    $('#wrap_tableErr').empty();
    $('#cont_inputcsv').removeClass('cont_inputcsv_close');
    CSVToImport = [];
    let regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    let input = document.getElementById('inp_import_csvfile');
    if (!input) {
        ErrorMsgModalBox('open', 'errorMsgCsvFile', 'No fue posible cargar el archivo.');
        input.value = "";
    } else if (!input.files) {
        ErrorMsgModalBox('open', 'errorMsgCsvFile', 'No fue posible cargar el archivo.');
        input.value = "";
        //}else if (!input.files[0]) {
        //  ErrorMsgModalBox('open', 'errorMsgCsvFile', 'No se seleccionó ningún archivo.');  
        //  input.value = "";          
    } else {
        if (regex.test(input.value.toLowerCase())) {
            file = input.files[0];
            fr = new FileReader();
            fr.onload = receivedText;
            fr.readAsText(file);
        } else {
            ErrorMsgModalBox('open', 'errorMsgCsvFile', 'El tipo de archivo no coincide. Selecciona un archivo .csv o .txt');
            input.value = "";
        }
    }
}

function receivedText() {
    txt = fr.result;
    txt2 = txt.split(/\r\n|\n/);
    input = document.getElementById('inp_import_csvfile');
    deliminter = $('#inp_import_delimiter').val();
    if (deliminter == '' || deliminter.length > 1) {
        ErrorMsgModalBox('open', 'errorMsgCsvFile', 'Error en el delimitador. Solo se acepta un caracter y no puede ser vacio.');
        $('#inp_import_delimiter').focus();
        input.value = "";
        return;
    }

    if (txt2.length >= 2) {
        if (txt2.length > 201) {
            ErrorMsgModalBox('open', 'errorMsgCsvFile', 'Por cuestiones de procesamiento puedes subir un máximo de 200 ítems por carga.');
            input.value = "";
            return;
        } else {
            if (CheckInputGeneral(txt2, deliminter) == false) {
                input.value = "";
                return;
            }
        }

    } else {
        ErrorMsgModalBox('open', 'errorMsgCsvFile', 'El archivo ingresado debe tener al menos dos filas incluyendo el encabezado y un registro.');
        input.value = "";
        return;
    }

}
function ArrayToLowerCase(a) {
    let s = [];
    for (let i = 0; i < a.length; i++) {
        s.push(a[i].toLowerCase());
    }
    return s;
}

function CheckInputGeneral(arrIn, deliminter) {
    // LIMPIAR EL INPUT ARR DE CUALQUIER PROBLEMA DE LiNEAS VACIAS 
    let arr = [];
    for (let i = 0; i < arrIn.length; i++) {
        if (arrIn[i] != "") arr.push(arrIn[i]);
    }
    input = document.getElementById('inp_import_csvfile');
    let tableHTML = '';
    let tableHTML_Head = '';
    let tableHTML_Body = '';
    let tableHTML_Foot = '';

    // REVISAR HEADERS
    let HeaderT = ArrayToLowerCase(arr[0].split(deliminter));
    let labelsHead = ["nombre", "referencia", "precio", "unidades", "impuesto"];
    let ck_empty = [0, 0, 0, 1, 1]; // 1: could be empty, 0: could not be empty
    let ck_type = ['ansc', 'ansc', 'n', 't', 'n']; // Type: n-> Numeric, t-> Text, a -> Any , ansc -> Any No Special Chars
    let ck_maxChrs = [50, 20, -1, 25, -1]; // max num of chars if is text (-1 if is numeric)
    let ck_nonNeg = [0, 0, 1, 0, 1]; // For numeric: 1 if just positive numbers, 0 if not. (-1 other type or NA)
    let ck_valMax = [-1, -1, -1, -1, 100]; // Maximo valor para campos numericos (-1 si no es numerico)
    let ck_valMin = [-1, -1, -1, -1, 0];   // Minimo valor para campos numericos (-1 si no es numerico)
    let labelsPos = [];
    for (let i = 0; i < labelsHead.length; i++) {
        labelsPos.push(HeaderT.indexOf(labelsHead[i].toLowerCase()));
        if (labelsPos[i] == -1) {
            ErrorMsgModalBox('open', 'errorMsgCsvFile', 'No se ha encontrado una etiqueta con el campo: ' + labelsHead[i] + '. Revisa los requerimientos de importación de ítems si necesitas ayuda.');
            input.value = "";
            return false;
        }
    }

    let AllErrs = [];
    let AllVals = [];
    let AllIncidents = [];
    let AllGoods = [];
    let Goods = 0;
    let Bads = 0;
    let ItemsRef = [];

    // REVISAR QUE LOS ITEMS NO EXISTAN YA
    for (let i = 1; i < arr.length; i++) {
        let x = arr[i].split(deliminter);
        ItemsRef.push(x[labelsPos[1]]);
    }
    let JsonItemsRef = JSON.stringify(ItemsRef);

    $.ajax({
        url: "check_items_exist.php",
        dataType: "json",
        data: ({ JsonItems: JsonItemsRef }),
        type: "POST",
        success: function (r) {
            // Duplicity array
            let duplicates = [];
            for (let i = 0; i < r.length; i++) {
                duplicates.push(r[i].prod_id);
            }

            // REVISAR CUERPO
            for (let i = 1; i < arr.length; i++) {
                let x = arr[i].split(deliminter);
                let values = [];
                let goodVals = [];
                let ErrorsV = [];
                let Incident = [];
                let Bad_i = false;

                // Revisar por duplicado de referencia
                if (duplicates.indexOf(x[labelsPos[1]]) > -1) {
                    ErrorsV.push('El ítem con referencia ' + x[labelsPos[1]] + ' ya existe en tu base de datos');
                    Incident.push(x[labelsPos[1]]);
                    Bad_i = true;
                }
                // Revisar cada campo k de la fila i
                for (let k = 0; k < labelsHead.length; k++) {
                    values.push(x[labelsPos[k]]);
                    goodVals.push(x[labelsPos[k]]);
                    // Change , for . if numeric
                    if (ck_type[k] == 'n') {
                        values[k] = values[k].replace(",", ".");
                        goodVals[k] = goodVals[k].replace(",", ".");
                    }
                    // Check EMPTY
                    if (values[k].toString() == '' && ck_empty[k] == 0) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' no puede estar vacio');
                        Incident.push(values[k]);
                        Bad_i = true;
                    }
                    // Check TYPE
                    if (ck_type[k] == 't' && values[k].match(/[^-A-Za-z áéíóúâêôãõçÁÉÍÓÚÂÊÔÃÕÇüñÜÑ]/) != null) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' debe ser solo texto');
                        Incident.push(values[k]);
                        Bad_i = true;
                    } else if (ck_type[k] == 'ansc' && values[k].match(/[^-&A-Za-z áéíóúâêôãõçÁÉÍÓÚÂÊÔÃÕÇüñÜÑ\d]/) != null) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' no puede contener caracteres especiales');
                        Incident.push(values[k]);
                        Bad_i = true;
                    } else if (ck_type[k] == 'n' && isNaN(values[k])) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' debe ser numérico');
                        Incident.push(values[k]);
                        Bad_i = true;
                    }
                    // Check Max Num of Chars
                    if (ck_maxChrs[k] > -1 && values[k].toString().length > ck_maxChrs[k]) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' debe tener igual o menos de ' + ck_maxChrs[k] + ' caracteres');
                        Incident.push(values[k]);
                        Bad_i = true;
                    }
                    // Check if negative
                    if (ck_nonNeg[k] == 1 && !isNaN(values[k]) && values[k] < 0) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' no puede ser negativo');
                        Incident.push(values[k]);
                        Bad_i = true;
                    }
                    // Check Range of numerical Value
                    if ((ck_valMax[k] > -1 && !isNaN(values[k]) && values[k] > ck_valMax[k]) || (ck_valMin[k] > -1 && !isNaN(values[k]) && values[k] < ck_valMin[k])) {
                        ErrorsV.push('El campo ' + labelsHead[k] + ' debe ser estar entre ' + ck_valMin[k] + ' y ' + ck_valMax[k]);
                        Incident.push(values[k]);
                        Bad_i = true;
                    }

                }
                if (Bad_i == true) {
                    Bads = Bads + 1;
                } else {
                    Goods = Goods + 1;
                    AllGoods.push(goodVals);
                }
                AllVals.push(values);
                AllErrs.push(ErrorsV);
                AllIncidents.push(Incident);
            }
            GoodsImport = Goods;
            BadsImport = Bads;
            CSVToImport = AllGoods;

            if (Bads > 0) {
                $('#cont_inputcsv').addClass('cont_inputcsv_close');
                input.value = "";
                tableHTML_Body = '';
                if (Goods > 0) {
                    $('#msg_idatprod').append('<p>Número de ítems a importar = ' + Goods + '</p>');
                    $('#msg_idatprod').append('<p>Número de ítems con conflicto = ' + Bads + '</p>');
                    $('#msg_idatprod').append('<button class="btn_green_full" onclick="StartImportCSV()">CONTINUAR</button>');
                    $('#msg_idatprod').append('<p class="UploadCSVAgain" onclick="UploadCSVAgain()">Cargar nuevo archivo</button>');
                    tableHTML_Desc = '<p class="textErrTable">Revisa el siguiente reporte, si das click en continuar <b>únicamente se importarán los ' + Goods + ' ítems sin conflicto</b> o puedes corregir los conflictos y <span class="impptx1" onclick="UploadCSVAgain()">cargar el archivo de nuevo.</span></p>'
                } else {
                    tableHTML_Desc = '<p class="textErrTable">No hay ningún ítem para importar. Por favor, revisa el siguiente <b>reporte de conflictos</b>, corrige los problemas e intenta <span class="impptx1" onclick="UploadCSVAgain()">cargar el archivo de nuevo.</span></p>'
                }
                tableHTML_Head = '<table><thead><tr><th class="wrap_tableErr_col1">Ítem</th><th class="wrap_tableErr_col2">Conflicto</th></tr></thead><tbody>';
                tableHTML_Foot = tableHTML_Foot + '</tbody></table>';
                for (let h = 0; h < AllErrs.length; h++) {
                    if (AllErrs[h].length > 0) {
                        tableHTML_Body = tableHTML_Body + '<tr>';
                        tableHTML_Body = tableHTML_Body + '<td class="wrap_tableErr_col1">' + (h + 1) + '</td>';
                        insErrHnd = '';
                        for (let w = 0; w < AllErrs[h].length; w++) {
                            insErrHnd = insErrHnd + '<p>⚠ ' + AllErrs[h][w].toString() + '</p>';
                        }
                        tableHTML_Body = tableHTML_Body + '<td class="wrap_tableErr_col2">' + insErrHnd + '</td>';
                        tableHTML_Body = tableHTML_Body + '</tr>';
                    } else {
                        // Ningun conflicto
                        tableHTML_Body = tableHTML_Body + '<tr>';
                        tableHTML_Body = tableHTML_Body + '<td class="wrap_tableErr_col1">' + (h + 1) + '</td>';
                        tableHTML_Body = tableHTML_Body + '<td class="wrap_tableErr_col2">✔ No se encontró ningún conflicto</td>';
                        tableHTML_Body = tableHTML_Body + '</tr>';
                    }
                }
                tableHTML = tableHTML_Desc + tableHTML_Head + tableHTML_Body + tableHTML_Foot;
                $('#wrap_tableErr').append(tableHTML);
                return false;
            } else {
                $('#cont_inputcsv').addClass('cont_inputcsv_close');
                input.value = "";
                $('#msg_idatprod').append('<center><img src="../icons/illustrations/check_done.svg" class="img_check_done_csv"></center>');
                $('#msg_idatprod').append('<p class="CSV_AllCorr1">¡TODO CORRECTO!</p>');
                $('#msg_idatprod').append('<p class="CSV_AllCorr2">Todos los ' + Goods + ' ítems ingresados pasaron todos los filtros. Da click en continuar para comenzar la <b>carga de la información</b>.</p>');
                $('#msg_idatprod').append('<button class="btn_green_full" onclick="StartImportCSV()">CONTINUAR</button>');
                $('#msg_idatprod').append('<p class="UploadCSVAgain" onclick="UploadCSVAgain()">Cargar nuevo archivo</button>');
                return true;
            }
        }
    });
}
function UploadCSVAgain() {
    $('#msg_idatprod').empty();
    $('#wrap_tableErr').empty();
    $('#cont_inputcsv').removeClass('cont_inputcsv_close');
    input.value = "";
    CSVToImport = [];
};
function StartImportCSV() {
    if (CSVToImport.length > 0) {
        let JsonItems = JSON.stringify(CSVToImport);
        $.ajax({
            url: "push_csv_items.php",
            data: ({ JsonItems: JsonItems }),
            type: "POST",
            success: function (r) {
                if (r == 1) {
                    refresh_items();
                    CloseImportDataProduct();
                    CloseNewItem();
                    alertify.message('Ítems importados correctamente');
                } else {
                    alertify.alert('Error cargando ítems', 'Se ha producido un error inesperado. Por favor intenta de nuevo cargar el archivo.');
                    UploadCSVAgain();
                }
            }
        });
    } else {
        console.log('CSVToImport sin data');
    }
}
function OpenHelpCsv() {
    HTMLText = '';
    HTMLText = HTMLText + '<p class="help_title">REQUERIMIENTOS DE IMPORTACIÓN CSV</p>';
    HTMLText = HTMLText + '<p class="help_body big_break">Desde <b>Luca</b> podrás importar tus ítems desde un archivo .csv o .txt de forma rápida y sencilla. Ten en cuenta los siguientes requerimientos y consejos para que el proceso de importación sea éxitoso.</p>';
    HTMLText = HTMLText + '<p class="help_subtitle">1. EXTENSIÓN CORRECTA</p>';
    HTMLText = HTMLText + '<img class="help_img mdm_img" src="../icons/illustrations/csv_file_icn.svg">';
    HTMLText = HTMLText + '<p class="help_body big_break">La extensión del archivo solo puede ser <b>.csv o .txt</b> y se recomienda que el archivo se encuentre en codificación <b>Unicode (UTF-8)</b> para que la lectura de los campos de texto sea correcta</p>';
    HTMLText = HTMLText + '<p class="help_subtitle">2. DELIMITADOR</p>';
    HTMLText = HTMLText + '<img class="help_img" src="../icons/illustrations/csv_delimitator.svg">';
    HTMLText = HTMLText + '<p class="help_body big_break">Usa la opción del delimitador para escoger cómo está <b>delimitado tu archivo</b>: comas (,), punto y coma (;), guión (-), etc ...</p>';
    HTMLText = HTMLText + '<p class="help_subtitle">3. ENCABEZADOS ADECUADOS</p>';
    HTMLText = HTMLText + '<img class="help_img" src="../icons/illustrations/tableCSV.svg">';
    HTMLText = HTMLText + '<p class="help_body">El archivo que vas a importar debe tener encabezados en la primera fila y estos deben llevar los nombre de los <b>campos adecuados</b> para que la importación sea efectiva. Los campos deben ser:</p>';
    HTMLText = HTMLText + '<ol class="help_body indentList"><li><b>Nombre:</b> Corresponde al nombre del ítem que deseas ingresar (Ej: <code>Café</code>)</li>';
    HTMLText = HTMLText + '<li><b>Referencia:</b> Corresponde al código de referencia del ítem a ingresar, este puede ser el código EAN u otro código interno. (Ej: <code>1234567890123</code>)</li>';
    HTMLText = HTMLText + '<li><b>Precio:</b> Corresponde al precio del ítem a ingresar. No uses simbolos de divisas, ni comas para separar los miles. (Ej: <code>3100</code>)</li>';
    HTMLText = HTMLText + '<li><b>Unidades:</b> Unidades del producto a ingresar. (Ej: <code>Libra</code>)</li>';
    HTMLText = HTMLText + '<li><b>Impuesto:</b> Porcentaje de impuesto del producto. Corresponde a un valor entre 0 y 100. No uses simbolo de porcentaje. (Ej: <code>5</code>)</li>';
    HTMLText = HTMLText + '</ol>';
    HTMLText = HTMLText + '<p class="help_body big_break">No importa el orden de las columnas lo importante es que los nombres anteriormente mencionados <b>coincidan</b> con tu encabezado.</p>';
    HTMLText = HTMLText + '<p class="help_subtitle">4. CAMPOS OBLIGATORIOS</p>';
    HTMLText = HTMLText + '<p class="help_body big_break">Ten en cuenta que los campos de <b>Nombre, Referencia y Precio</b> son extrictamente obligatiorios y de faltar alguno todo el ítem entrará en conflico.</p>';
    HTMLText = HTMLText + '<p class="help_subtitle">5. MÁXIMO 200 ÍTEMS</p>';
    HTMLText = HTMLText + '<p class="help_body">Ten en cuenta que máximo está permitido subir <b>200 ítems por carga</b>. Puedes hacer <b>varias cargas</b> si deseas cargar más registros.</p>';

    $('#help_container').append(HTMLText);
    OpenHelp();
}
function AddCustToSearchBox(customer_key) {
    // Agregar un cliente a el buscador dado una llave de cliente
    $("#search_consumer").addClass("cust_selected");
    $('#search_consumer').attr('readonly', true);
    $("#search_consumer").prop('disabled', true);
    SelectedCustomer = customer_key;
    FillBasicInfoCust(customer_key);
    PushLastOrdersCust(customer_key);
}
function AddCustToSearchBoxOffline(customer_key) {
    // Agregar un cliente a el buscador dado una llave de cliente cuando está Offline el Navegador
    $("#search_consumer").addClass("cust_selected");
    $('#search_consumer').attr('readonly', true);
    $("#search_consumer").prop('disabled', true);
    SelectedCustomer = customer_key;
}
function FillBasicInfoCust(customer_key) {
    // Poblar con info el modulo de Info Panel y activar la notificacion de puntos en el buscador
    $("#infocust_name").text('Sin Nombre');
    $("#infocust_tel").text(' - ');
    $("#infocust_email").text(' - ');
    $("#infocust_puntos").text('0');
    $("#infocust_bday").text(' - ');
    $.ajax({
        url: "get_cust_info.php",
        dataType: "json",
        data: ({ cust_key: customer_key }),
        type: "POST",
        success: function (r) {
            if (r.length > 0 && r != null) {
                let cust_name = r[0]['cust_name'];
                let cust_id = r[0]['cust_id'];
                let cust_type_id = r[0]['cust_type_id'];
                let cust_email = r[0]['cust_email'];
                let cust_phone = r[0]['cust_phone'];
                let cust_points = r[0]['cust_points'];
                let cust_bdate = r[0]['cust_bdate'];
                $("#cb_icon_info_cons").removeClass("hide_clearconsumer");

                if (cust_name != '') { $("#infocust_name").text(cust_name); }
                if (cust_phone != '0' && cust_phone != '') { $("#infocust_tel").text(cust_phone); }
                if (cust_email != '') { $("#infocust_email").text(cust_email); }
                if (cust_id != '') { $("#infocust_id").text(cust_id); }
                if (cust_bdate != '' && cust_bdate != '1900-01-01') { $("#infocust_bday").text(cust_bdate); }
                ActivateNotifyPoint(cust_points);
            }
        }
    });
}
function ActivateNotifyPoint(points) {
    // Activar la notificacion de puntos en el buscador
    if (points > 0) {
        $("#num_luca_points").addClass("noty_show");
        $("#num_luca_points").text(points);
        $("#infocust_puntos").text(points);
    } else {
        $("#num_luca_points").removeClass("noty_show");
        $("#num_luca_points").text('');
        $("#infocust_puntos").text('0');
    }
}
function PushLastOrdersCust(customer_key) {
    // Poblar las ultimas tres (max) pedidos del cliente en el panel de información dado una llave de cliente
    $("#cont_last_orders_cust").html('');
    let ConOrdHTML = '';
    $("#s3_a").show();
    $.ajax({
        url: "get_last_orders_by_cons.php",
        data: ({ cust_key: customer_key }),
        type: "POST",
        dataType: "json",
        success: function (c) {
            if (c.length == 0) {
                $("#s3_a").hide();
                ConOrdHTML = ConOrdHTML + '<div class="ordersCons_empty">';
                ConOrdHTML = ConOrdHTML + '<img src="../icons/SVG/41-shopping/basket-2.svg">';
                ConOrdHTML = ConOrdHTML + '<p>El cliente no tiene ninguna compra</p>';
                ConOrdHTML = ConOrdHTML + '</div>';
            } else {
                for (let i = 0; i < c.length; i++) {
                    ConOrdHTML = ConOrdHTML + '<div class="pursh_line">';
                    ConOrdHTML = ConOrdHTML + '<div class="label_n_data br_lnd w20p">';
                    ConOrdHTML = ConOrdHTML + '<p>Orden</p><p class="bill_nc_data" title="' + c[i]['id_bill'] + '">' + c[i]['id_bill'] + '</p></div>';
                    ConOrdHTML = ConOrdHTML + '<div class="label_n_data br_lnd w40p">';
                    ConOrdHTML = ConOrdHTML + '<p>Fecha</p><p>' + c[i]['trx_date'].substring(0, 10) + '</p></div>';
                    ConOrdHTML = ConOrdHTML + '<div class="label_n_data w40p">';
                    ConOrdHTML = ConOrdHTML + '<p>Valor</p><p>$ <span id="valor_pursh_line">' + numCommas(numRound(c[i]['trx_value'], 0)) + '</span></p></div>';
                    ConOrdHTML = ConOrdHTML + '</div>';
                }
            }
            $("#cont_last_orders_cust").append(ConOrdHTML);
        }
    });
}

function CustRecommWindow(customer_key, shortName, showMsgNoRecom, msgnoaccess) {
    msgnoaccess = (msgnoaccess == undefined) ? false : msgnoaccess;
    let bl = new BarLoading(15000, "Cargando Recomendaciones");
    if (msgnoaccess) {
        bl.start();
    }
    CheckPkgAccess(1).then(function (cpa) {
        checkFreeTrail(1).then(function (cft) {
            if (!msgnoaccess) {
                bl.stop();
            }
            if (cpa || cft.isactive) {
                if (cft.isactive && msgnoaccess && cft.days_to_end <= 3 && !cpa) {
                    if (cft.days_to_end >= 2) {
                        alertify.message("Tu período de prueba caduca en " + cft.days_to_end + " días.");
                    } else if (cft.days_to_end == 1) {
                        alertify.message("Tu período de prueba caduca mañana.");
                    } else if (cft.days_to_end == 0) {
                        alertify.message("Tu período de prueba caduca hoy.");
                    }
                }
                $("#itemrecom_description").html('');
                $("#itemrecom_items").html('');
                $.ajax({
                    url: "pull_client_based_recom.php",
                    dataType: "json",
                    data: ({ cust_key: customer_key }),
                    type: "POST",
                    success: function (r) {
                        bl.stop();
                        if (r.length > 0 && r != null) {
                            let description = '';
                            if (r.length == 1) {
                                description = "Hemos encontrado el siguiente ítem para ofrecerle a <b>" + shortName + "</b>.  Da click sobre el para agregarlo a la venta actual.";
                            } else {
                                description = "Hemos encontrado los siguientes ítems para ofrecerle a <b>" + shortName + "</b>.  Da click sobre ellos para agregarlos a la venta actual.";
                            }
                            $("#itemrecom_description").html(description);
                            let htmlItemsRecom = '';
                            let item_rec = '';
                            for (let i = 0; i < r.length; i++) {
                                item_rec = searchItemById(r[i]['item_id']);
                                if (item_rec != null) {
                                    htmlItemsRecom += '<div class="msg-box-1-optionbox" onclick="addRecomItem(&#39;' + r[i]['item_id'] + '&#39;)"><img src="' + item_rec.icon_src + '"><p>' + item_rec.name + '<br>($ ' + numCommas(numRound(item_rec.price, 2)) + ')</p><div class="counter_item_add" id="pr_count_' + r[i]['item_id'] + '">0</div></div>';
                                }
                            }
                            $("#itemrecom_items").html(htmlItemsRecom);
                            OpenItemRecom();
                        } else {
                            if (showMsgNoRecom == 1) {
                                alertify.message("No hay recomendaciones para " + shortName);
                            }
                        }
                    }
                });
            } else {
                bl.stop();
                if (msgnoaccess) {
                    alertify.alert('', 'No tienes acceso al paquete de analítica.');
                }
            }
        });
    });
}
function DropSelectedItems() {

    if (SelectedItemsHome.length > 0) {

        if (SessionActive() == false) {
            alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
            return false;
        }
        HasPermission('items_3').then(function (pms) {
            if (pms == "1") {
                alertify.confirm('Borrar ítems', "¿Seguro deseas borrar todos los ítems seleccionados? Esta acción eliminará los ítems de tu base de datos permanentemente.", function () {
                    let itmSelJSON = JSON.stringify(SelectedItemsHome);
                    $.ajax({
                        url: "dropMultiItems.php",
                        data: ({ itmSelJSON: itmSelJSON }),
                        type: "POST",
                        success: function (r) {
                            if (r == '1') {
                                push_all_items("#box_products", "prod_name");
                                alertify.alert("Ítems eliminados", "Se eliminaron " + SelectedItemsHome.length + " ítems correctamente.");
                                SelectedItemsHome = [];
                                restart_sel_items();
                            } else {
                                alertify.alert("Error", "No fue posible eliminar los ítems.");
                            }

                        }
                    });

                }, function () { });
            } else {
                alertify.alert('Permiso Denegado', 'El perfil actual no tiene permisos para entrar a este modulo.');
            }
        });

    }
}