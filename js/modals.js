/******** MENU BAR APP ********/
$(document).ready(function () {

    // Create the HTML of the menu
    let createMenuNav = function () {
        let activePage = window.location.pathname.split("/").pop().split(".")[0];

        let prinActive = (activePage == 'sales') ? "MenuOptionActive" : "";
        let dashActive = (activePage == 'dashboard') ? "MenuOptionActive" : "";
        let clieActive = (activePage == 'clients') ? "MenuOptionActive" : "";
        let ventActive = (activePage == 'bills') ? "MenuOptionActive" : "";
        let markActive = (activePage == 'marketplace') ? "MenuOptionActive" : "";
        let usuaActive = (activePage == 'account_mng') ? "MenuOptionActive" : "";

        if (activePage != '' && activePage != undefined) {
            let navHTML = '<div><div class="FirstMenuBlock">';
            navHTML += '<div id="NavSales" title="PRINCIPAL"><span class="mdi mdi-cash-register ' + prinActive + '"></span><p class="' + prinActive + '">Principal</p></div>';
            navHTML += '<div id="NavDashboard" title="DASHBOARD"><span class="mdi mdi-finance ' + dashActive + '"></span><p  class="' + dashActive + '">Dashboard</p></div>';
            navHTML += '<div id="NavCustomers" title="CLIENTES"><span class="mdi mdi-account ' + clieActive + '"></span><p  class="' + clieActive + '">Clientes</p></div>';
            navHTML += '<div id="NavBills" title="VENTAS"><span class="mdi mdi-shopping ' + ventActive + '"></span><p  class="' + ventActive + '">Transacciones</p></div>';
            navHTML += '</div><div class="MenuDivier"></div><div class="SecondMenuBlock">';
            navHTML += '<div id="NavMarketplace" title="MARKETPLACE"><span class="mdi mdi-toolbox-outline ' + markActive + '"></span><p  class="' + markActive + '">Marketplace</p></div>';
            navHTML += '<div id="NavAccnt" title="CUENTA DE USUARIO"><span class="mdi mdi-shield-account-outline ' + usuaActive + '"></span><p  class="' + usuaActive + '">Cuenta de usuario</p></div>';
            navHTML += '<div id="NavLogout" title="CERRAR SESIÓN"><span class="mdi mdi-logout-variant"></span><p>Cerrar sesión</p></div>';
            navHTML += '</div></div><span id="MenuNavgBtn" class="MenuNavgBtn"><span class="mdi mdi-menu"></span></span>';
            $("#MenuNavg").html(navHTML);
        }
    }

    createMenuNav();

    $("#wrapper").click(function () { CloseMenu() });
    $(window).scroll(function () { CloseMenu() });

    $("#NavSales").click(function () { window.location = '../php/sales.php'; });
    $("#NavDashboard").click(function () { window.location = '../php/dashboard.php'; });
    $("#NavCustomers").click(function () { window.location = '../php/clients.php'; });
    $("#NavBills").click(function () { window.location = '../php/bills.php'; });
    $("#NavMarketplace").click(function () { window.location = '../php/marketplace.php'; });
    $("#NavAccnt").click(function () { window.location = '../php/account_mng.php'; });
    $("#NavLogout").click(function () { window.location = '../php/logout.php'; });
    $("#MenuNavgBtn").click(function () { ToggleMenu(); });
});


// ******** LATERAL MENU ************
function ToggleMenu() {
    var MenuOpen = false;
    if (Number($('#MenuNavg').css('left').replace(/[^0-9]/g, "")) == 0) {
        var MenuOpen = true;
    }
    (MenuOpen == false) ? OpenMenu() : CloseMenu();
}
function OpenMenu() {
    $("#MenuNavgBtn").addClass("MenuNavgBtnOpen");
    $("#MenuNavg").addClass("MenuNavgActive");
    $("#MenuNavgBtn span").removeClass("mdi-menu").addClass("mdi-window-close");
    $("#wrapper").addClass("wrapper_opn");
}
function CloseMenu() {
    $("#MenuNavgBtn").removeClass("MenuNavgBtnOpen");
    $("#MenuNavg").removeClass("MenuNavgActive");
    $("#MenuNavgBtn span").removeClass("mdi-window-close").addClass("mdi-menu");
    $("#wrapper").removeClass("wrapper_opn");
}

/****** LOGIN FORM ********/
function OpenLogInForm() {
    $("#msg-box-1").css({ "display": "block" });
    $("#msg-box-container").css({ "display": "block" });
}
function CloseLogInForm() {
    $("#msg-box-1").css({ "display": "none" });
    $("#msg-box-container").css({ "display": "none" });
}
$(document).mouseup(function (e) {
    var modal_login = $('#msg-box-1');
    if (!modal_login.is(e.target) && modal_login.has(e.target).length === 0) {
        $("#msg-box-1").css({ "display": "none" });
        $("#msg-box-container").css({ "display": "none" });
    }
});
/**************************/

/****** ICONS MODAL ******/
function OpenGallery() {
    CloseMenu();
    $('body').css('overflow', 'hidden');
    $("#container_prelogos").css({ "display": "block" });
    $("#container_prelogos_back").css({ "display": "block" });
}

function CloseGallery() {
    $('body').css('overflow', 'auto');
    $("#container_prelogos").css({ "display": "none" });
    $("#container_prelogos_back").css({ "display": "none" });
}

// Render all images of logos pre loaded 
function displayImgsPreloaded(category, action_on_click_icon) {

    // Clear all content in div
    $('#ctn_prelogos_i2').empty();

    var categ = category;
    var dir = "../icons/SVG/" + categ;

    $.ajax({
        type: "GET",
        url: "readicons.php",
        data: ({ folder: dir }),
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i] !== '.' && data[i] !== '..') {
                    $("#ctn_prelogos_i2").append("<img src='" + dir + '/' + data[i] + "' alt='" + data[i] + "' onclick='" + action_on_click_icon + "(&quot;" + dir + '/' + data[i] + "&quot;)'>");
                }
            }
        }
    });

}
/********/

/****** NEW ITEMS FORM ********/
function OpenNewItem() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    HasPermission('items_1').then(function (pms) {
        if (pms == "1") {
            $('body').addClass('noscroll');
            $.ajax({
                type: "POST",
                url: "get_categories.php",
                data: ({ email: '1' }),
                dataType: "json",
                success: function (data) {
                    mr.stopMainRoller();
                    var table = "<option value='0' disabled hidden></option>";
                    if (isRestnt) {
                        table += "<optgroup label='Predefinidas'>";
                        table += "<option value='PRED_ADICION' selected>- Adiciones -</option>";
                        table += "</optgroup>"
                    }
                    table += "<optgroup label='Tus Categorías'>";
                    table += "<option value='Otros' selected>Otros</option>";
                    if (data.length == 0) {
                        table += "";
                    } else {
                        for (var i = 0; i < data.length; i++) {
                            table += "<option value='" + data[i]["cat_name"] + "'>" + data[i]["cat_name"] + "</option>";
                        }
                    }
                    table += "</optgroup>"
                    $("#ni_prod_category").html(table);
                }
            });

            $("#msg-box-new-item").css({ "display": "block" });
            $("#msg-box-container-new-item").css({ "display": "block" });
        } else {
            mr.stopMainRoller();
            alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
        }
    });
}
function OpenNewItem_Silent() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-new-item").css({ "display": "block" });
    $("#msg-box-container-new-item").css({ "display": "block" });
}
function CloseNewItem() {
    $('body').removeClass('noscroll');
    $("#msg-box-new-item").css({ "display": "none" });
    $("#msg-box-container-new-item").css({ "display": "none" });
}

/****** NEW ITEMS END REGISTER ********/
function OpenNewItemEnd() {
    CloseNewItem();
    $('body').addClass('noscroll');
    $("#msg-box-end-newprod").css({ "display": "block" });
    $("#msg-box-container-end-newprod").css({ "display": "block" });
    $("#ni_prod_name").focus();
}
function CloseNewItemEnd() {
    $('body').removeClass('noscroll');
    $("#msg-box-end-newprod").css({ "display": "none" });
    $("#msg-box-container-end-newprod").css({ "display": "none" });
}

/****** ITEM RECOMENDATION ********/
function OpenItemRecom() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-itemrecom").css({ "display": "block" });
    $("#msg-box-container-itemrecom").css({ "display": "block" });
}
function CloseItemRecom() {
    $('body').removeClass('noscroll');
    $("#msg-box-itemrecom").css({ "display": "none" });
    $("#msg-box-container-itemrecom").css({ "display": "none" });
}
$(document).mouseup(function (e) {
    var modal_login = $('#msg-box-itemrecom');
    if (!modal_login.is(e.target) && modal_login.has(e.target).length === 0) {
        $("#msg-box-itemrecom").css({ "display": "none" });
        $("#msg-box-container-itemrecom").css({ "display": "none" });
    }
});

/******* HELP WITH TAX ********/
function OpenHelpTax() {
    CloseNewItem();
    $('body').addClass('noscroll');
    $("#msg-box-help-tax").css({ "display": "block" });
    $("#msg-box-container-help-tax").css({ "display": "block" });
}
function CloseHelpTax() {
    OpenNewItem_Silent();
    $('body').removeClass('noscroll');
    $("#msg-box-help-tax").css({ "display": "none" });
    $("#msg-box-container-help-tax").css({ "display": "none" });
}

/*********** ITEM OPTIONS ******************/
function OpenItemOptions() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-configitem").css({ "display": "block" });
    $("#msg-box-container-configitem").css({ "display": "block" });
}
function CloseItemOptions() {
    $('body').removeClass('noscroll');
    $("#msg-box-configitem").css({ "display": "none" });
    $("#msg-box-container-configitem").css({ "display": "none" });
    $("#configitem_nameItem1").text('');
    $("#configitem_idItem1").text('');
    CurrHoldItem = '';
    $("#QRCodeImg2").attr("src", '');
    $("#aQRCode2").attr("href", '');
    $("#linkQRCode").text('');
}
function CloseItemOptions_silent() {
    $('body').removeClass('noscroll');
    $("#msg-box-configitem").css({ "display": "none" });
    $("#msg-box-container-configitem").css({ "display": "none" });
}

/****** QR CODE ********/
function OpenQRCode() {
    CloseItemOptions_silent();
    QRModule_Set(CurrHoldItem);
    $('body').addClass('noscroll');
    $("#msg-box-qrcode").css({ "display": "block" });
    $("#msg-box-container-qrcode").css({ "display": "block" });
}
function CloseQRCode() {
    OpenItemOptions();
    ConfItem_Set(CurrHoldItem);
    $('body').removeClass('noscroll');
    $("#msg-box-qrcode").css({ "display": "none" });
    $("#msg-box-container-qrcode").css({ "display": "none" });
}

/****** ITEM STATS ********/
function OpenItemStats() {
    CloseItemOptions_silent();
    $('body').addClass('noscroll');
    $("#msg-box-itemstats").css({ "display": "block" });
    $("#msg-box-container-itemstats").css({ "display": "block" });
}
function CloseItemStats() {
    OpenItemOptions();
    ConfItem_Set(CurrHoldItem);
    $('body').removeClass('noscroll');
    $("#msg-box-itemstats").css({ "display": "none" });
    $("#msg-box-container-itemstats").css({ "display": "none" });
    if (box1Chart != null) { box1Chart.destroy(); }
    if (box2Chart != null) { box2Chart.destroy(); }
}

/****** EDIT ITEMS ********/
function OpenEditItem() {
    CloseItemOptions_silent();
    $('body').addClass('noscroll');
    $.ajax({
        type: "POST",
        url: "get_categories.php",
        data: ({ email: '1' }),
        dataType: "json",
        success: function (data) {

            var table = "<option value='0' disabled hidden></option> <option value='Otros' selected>Otros</option>";
            if (data.length == 0) {
                table += "";
            } else {
                for (var i = 0; i < data.length; i++) {
                    table += "<option value='" + data[i]["cat_name"] + "'>" + data[i]["cat_name"] + "</option>";
                }
            }
            $("#ni_prod_category2").html(table);
            $('#ni_prod_category2').val(CurrHoldItem.category);
        }
    });
    $("#msg-box-edititem").css({ "display": "block" });
    $("#msg-box-container-edititem").css({ "display": "block" });
    $("#ni_prod_name2").focus();
}
function CloseEditItem() {
    OpenItemOptions();
    $('body').removeClass('noscroll');
    $("#msg-box-edititem").css({ "display": "none" });
    $("#msg-box-container-edititem").css({ "display": "none" });
}

/*********** EDIT ITEM IN BILL ******************/
function OpenEditItemBill() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-edititembill").css({ "display": "block" });
    $("#msg-box-container-edititembill").css({ "display": "block" });
    $("#edititembill_count").focus();
}
function CloseEditItemBill() {
    $('body').removeClass('noscroll');
    $("#msg-box-edititembill").css({ "display": "none" });
    $("#msg-box-container-edititembill").css({ "display": "none" });
}

/*********** NEW CUSTOMER ******************/
function OpenNewCustomer() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    HasPermission('customers_1').then(function (pms) {
        if (pms == "1") {
            mr.stopMainRoller();
            $('body').addClass('noscroll');
            $("#msg-box-newcustomer").css({ "display": "block" });
            $("#msg-box-container-newcustomer").css({ "display": "block" });
            $("#ncust_name").focus();
        } else {
            mr.stopMainRoller();
            alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
        }
    });
}
function CloseNewCustomer() {
    $('body').removeClass('noscroll');
    $("#msg-box-newcustomer").css({ "display": "none" });
    $("#msg-box-container-newcustomer").css({ "display": "none" });
}

/*********** EDITAR CUSTOMER ******************/
function OpenEditCustomer() {
    CloseConsumerInfoPanel();
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    HasPermission('customers_2').then(function (pms) {
        if (pms == "1") {
            mr.stopMainRoller();
            $('body').addClass('noscroll');
            $("#msg-box-editcustomer").css({ "display": "block" });
            $("#msg-box-container-editcustomer").css({ "display": "block" });
            $("#ncust_name").focus();
        } else {
            mr.stopMainRoller();
            alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
        }
    });
}
function CloseEditCustomer() {
    $('body').removeClass('noscroll');
    $("#msg-box-editcustomer").css({ "display": "none" });
    $("#msg-box-container-editcustomer").css({ "display": "none" });
}
function CloseEditCustomer_over() {
    $("#msg-box-editcustomer").css({ "display": "none" });
    $("#msg-box-container-editcustomer").css({ "display": "none" });
}

/***** OPEN CONSUMER INFO PANEL *****/
function OpenConsumerInfoPanel() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    HasPermission('customers_2').then(function (pms) {
        if (pms == "1") {
            mr.stopMainRoller();
            $('body').addClass('noscroll');
            $("#msg-box-consinfopanel").css({ "display": "block" });
            $("#msg-box-container-consinfopanel").css({ "display": "block" });
        } else {
            mr.stopMainRoller();
            alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
        }
    });
}
function CloseConsumerInfoPanel() {
    $('body').removeClass('noscroll');
    $("#msg-box-consinfopanel").css({ "display": "none" });
    $("#msg-box-container-consinfopanel").css({ "display": "none" });
}


/***** OPEN CONSUMER ORDERS HISTORY *****/
function OpenConsOrders() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    HasPermission('customers_2').then(function (pms) {
        if (pms == "1") {
            LoadAllConsOrders();
            mr.stopMainRoller();
            $('body').addClass('noscroll');
            $("#msg-box-consorders").css({ "display": "block" });
            $("#msg-box-container-consorders").css({ "display": "block" });
        } else {
            mr.stopMainRoller();
            alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
        }
    });
}
function OpenConsOrders_NoAsk() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    if (SessionActive() == false) {
        mr.stopMainRoller();
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    mr.stopMainRoller();
    $('body').addClass('noscroll');
    $("#msg-box-consorders").css({ "display": "block" });
    $("#msg-box-container-consorders").css({ "display": "block" });
}
function CloseConsOrders() {
    $('body').removeClass('noscroll');
    $("#msg-box-consorders").css({ "display": "none" });
    $("#msg-box-container-consorders").css({ "display": "none" });
}
function CloseConsOrders_over() {
    $("#msg-box-consorders").css({ "display": "none" });
    $("#msg-box-container-consorders").css({ "display": "none" });
}


/***** OPEN ANALYTICS CUSTOMER *****/
let OpenAnalyticsCust = function () {
    let bl = new BarLoading(15000, "Cargando Analítica");
    bl.start();
    CheckPkgAccess(1).then(function (cpa) {
        checkFreeTrail(1).then(function (cft) {
            if (cpa || cft.isactive) {
                if (cft.isactive && cft.days_to_end <= 3 && !cpa) {
                    if (cft.days_to_end >= 2) {
                        alertify.message("Tu período de prueba caduca en " + cft.days_to_end + " días.");
                    } else if (cft.days_to_end == 1) {
                        alertify.message("Tu período de prueba caduca en mañana.");
                    } else if (cft.days_to_end == 0) {
                        alertify.message("Tu período de prueba caduca hoy.");
                    }
                }
                CloseMenu();
                if (SessionActive() == false) {
                    bl.stop();
                    alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
                    OpenSalesman_admin_first();
                    return false;
                }
                HasPermission('customers_2').then(function (pms) {
                    if (pms == "1") {
                        GetCustAnalytics();
                        bl.stop();
                        $('body').addClass('noscroll');
                        $("#msg-box-analyticscust").css({ "display": "block" });
                        $("#msg-box-container-analyticscust").css({ "display": "block" });
                    } else {
                        bl.stop();
                        alertify.alert('Permiso Denegado', 'No tienes permisos para entrar a este modulo.');
                    }
                });
            } else {
                bl.stop();
                alertify.alert('', 'No tienes acceso al paquete de analítica.');
            }
        })
    });
}
function CloseAnalyticsCust() {
    $('body').removeClass('noscroll');
    $("#msg-box-analyticscust").css({ "display": "none" });
    $("#msg-box-container-analyticscust").css({ "display": "none" });
}
function CloseAnalyticsCust_over() {
    $("#msg-box-analyticscust").css({ "display": "none" });
    $("#msg-box-container-analyticscust").css({ "display": "none" });
}

/******* CASHIER ********/
function OpenCashier() {
    CloseMenu();
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar a un perfil antes de continuar con este módulo');
        OpenSalesman_admin_first();
        return false;
    }
    $('body').addClass('noscroll');
    $("#msg-box-cashier").css({ "display": "block" });
    $("#msg-box-container-cashier").css({ "display": "block" });
}
function CloseCashier() {
    $('body').removeClass('noscroll');
    $("#msg-box-cashier").css({ "display": "none" });
    $("#msg-box-container-cashier").css({ "display": "none" });
}
$(document).mouseup(function (e) {
    var modal_login = $('#msg-box-cashier');
    if (!modal_login.is(e.target) && modal_login.has(e.target).length === 0) {
        $("#msg-box-cashier").css({ "display": "none" });
        $("#msg-box-container-cashier").css({ "display": "none" });
    }
});


//***************************  edit bills
function OpenEditorBills() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-editorBills").css({ "display": "block" });
    $("#msg-box-container-editorBills").css({ "display": "block" });
}
function CloseEditorBills() {
    $('body').removeClass('noscroll');
    $("#msg-box-editorBills").css({ "display": "none" });
    $("#msg-box-container-editorBills").css({ "display": "none" });
}
$(document).mouseup(function (e) {
    var modal_login = $('#msg-box-editorBills');
    if (!modal_login.is(e.target) && modal_login.has(e.target).length === 0) {
        $("#msg-box-editorBills").css({ "display": "none" });
        $("#msg-box-container-editorBills").css({ "display": "none" });
    }
});
/******* TIPS ********/
function OpenTips() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-tips").css({ "display": "block" });
    $("#msg-box-container-tips").css({ "display": "block" });
    $("#tips_money_option").css("border", "1px solid #efefef");
    $("#tips_perc_option").css("border", "1px solid #00bb7d");
    $("#input_tips").attr("placeholder", "Ej: 50%");
    $("#tipsType").val("perc");
    $("#input_tips").val('');
    $("#input_tips").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
        } else if (e.keyCode == 13) {
            $("#input_btn_tips").click();
        } else {
            var dInput = $(this).val();
            var newDInput = dInput.replace(/[^0-9.]/g, "") + "%";
            $("#input_tips").val(newDInput);
        }

    });
    $("#input_tips").focus();
}
function CloseTips() {
    $('body').removeClass('noscroll');
    $("#msg-box-tips").css({ "display": "none" });
    $("#msg-box-container-tips").css({ "display": "none" });
}

/******* DISCOUNT ********/
function OpenDiscount() {
    CloseMenu();
    let mr = new MainRoller();
    mr.startMainRoller();
    var lp_redem = 7;
    var search_consumer = $('#search_consumer').val();
    if (search_consumer != '') {
        cust_key = SelectedCustomer;
        $.ajax({
            url: "get_cust_info.php",
            dataType: "json",
            data: ({ cust_key: cust_key }),
            type: "POST",
            success: function (r) {
                mr.stopMainRoller();
                if (r.length == 1 && r[0].cust_points > 0) {
                    var sbt = Number($('#super_total_bill').text().replace(",", ""));
                    var rs = 7;
                    var maxPoints = parseInt(r[0].cust_points);
                    var necc_points_full = Math.ceil(sbt / rs);

                    $('#discount_lucapoints_option').show();
                    $('#lr_cont').show();
                    $('#lr_0_name').text(r[0].cust_name);
                    $('#lr_0_points').text(r[0].cust_points);
                    $('#lr_0_p_cash').text(numCommas(r[0].cust_points * lp_redem));
                    $('#lr_subtot').text($('#super_total_bill').text());

                    if (maxPoints >= necc_points_full) {
                        $("#input_luca_points").val(necc_points_full);
                        $('#lr_save').text(numCommas(sbt));
                        $('#lr_tot').text(0);
                        $('#lr_0_all_points').text('paga toda la cuenta.');
                    } else {
                        y = (maxPoints * rs);
                        new_tot = sbt - y;
                        $("#input_luca_points").val(maxPoints);
                        $('#lr_save').text(numCommas(y));
                        $('#lr_tot').text(numCommas(new_tot));
                        $('#lr_0_all_points').text('usa todos los puntos.');
                    }

                    $('body').addClass('noscroll');
                    $("#msg-box-discount").css({ "display": "block" });
                    $("#msg-box-container-discount").css({ "display": "block" });
                    $("#discount_money_option").css("border", "1px solid #efefef");
                    $("#discount_perc_option").css("border", "1px solid #efefef");
                    $("#input_discount").hide();
                    $("#discType").val("lucapoints");
                    $("#input_discount").val('');
                    $("#input_luca_points").focus();
                } else {
                    $('#discount_lucapoints_option').hide();
                    $('#lr_cont').hide();
                    OpenDiscount2();
                }
            },
            error: function (request, status, error) {
                mr.stopMainRoller();
                $('#discount_lucapoints_option').hide();
                $('#lr_cont').hide();
                OpenDiscount2();
            }
        });
    } else {
        mr.stopMainRoller();
        $('#discount_lucapoints_option').hide();
        $('#lr_cont').hide();
        OpenDiscount2();
    }


}
function OpenDiscount2() {
    CloseMenu();
    $("#input_discount").show();
    $('body').addClass('noscroll');
    $("#msg-box-discount").css({ "display": "block" });
    $("#msg-box-container-discount").css({ "display": "block" });
    $("#discount_money_option").css("border", "1px solid #efefef");
    $("#discount_perc_option").css("border", "1px solid #00bb7d");
    $("#input_discount").attr("placeholder", "Ej: 50%");
    $("#discType").val("perc");
    $("#input_discount").val('');
    $("#input_discount").keyup(function (e) {
        if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
        } else if (e.keyCode == 13) {
            $("#input_btn_discount").click();
        } else {
            var discInput = $(this).val();
            var newDiscInput = discInput.replace(/[^0-9.]/g, "") + "%";
            $("#input_discount").val(newDiscInput);
        }

    });
    $("#input_discount").focus();
}
function CloseDiscount() {
    $('body').removeClass('noscroll');
    $("#msg-box-discount").css({ "display": "none" });
    $("#msg-box-container-discount").css({ "display": "none" });
}

/****** CHECKOUT 1 ********/
function OpenCheckOut1() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-checkout1").css({ "display": "block" });
    $("#msg-box-container-checkout1").css({ "display": "block" });
}
function CloseCheckOut1() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout1").css({ "display": "none" });
    $("#msg-box-container-checkout1").css({ "display": "none" });
}
function CloseCheckOut1_clean() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout1").css({ "display": "none" });
    $("#msg-box-container-checkout1").css({ "display": "none" });
    resetCheckOut();
}

/****** CHECKOUT 2 ********/
function OpenCheckOut2() {
    CloseCheckOut1();
    $('body').addClass('noscroll');
    $("#msg-box-checkout2").css({ "display": "block" });
    $("#msg-box-container-checkout2").css({ "display": "block" });
}
function CloseCheckOut2() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout2").css({ "display": "none" });
    $("#msg-box-container-checkout2").css({ "display": "none" });
}
function CloseCheckOut2_clean() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout2").css({ "display": "none" });
    $("#msg-box-container-checkout2").css({ "display": "none" });
    resetCheckOut();
}

/****** CHECKOUT 3 ********/
function OpenCheckOut3() {
    CloseCheckOut2();
    $('body').addClass('noscroll');
    $("#msg-box-checkout3").css({ "display": "block" });
    $("#msg-box-container-checkout3").css({ "display": "block" });
}
function CloseCheckOut3() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout3").css({ "display": "none" });
    $("#msg-box-container-checkout3").css({ "display": "none" });
}
function CloseCheckOut3_clean() {
    $('body').removeClass('noscroll');
    $("#msg-box-checkout3").css({ "display": "none" });
    $("#msg-box-container-checkout3").css({ "display": "none" });
    $("#num_luca_points").removeClass("noty_show");
    $("#num_luca_points").text('');
    resetCheckOut();
}

/****** CREATE ADMINISTRATOR *********/
function OpenAdminCreator() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-adminCreator").css({ "display": "block" });
    $("#msg-box-container-adminCreator").css({ "display": "block" });
}
function CloseAdminCreator() {
    $('body').removeClass('noscroll');
    $("#msg-box-adminCreator").css({ "display": "none" });
    $("#msg-box-container-adminCreator").css({ "display": "none" });
}

/****** SALESMAN *********/
function OpenSalesman_admin() {
    CloseMenu();
    UpdateProfiles();
    $('#exit_mark_profile').show();
    $('body').addClass('noscroll');
    $("#msg-box-salesman_admin").css({ "display": "block" });
    $("#msg-box-container-salesman_admin").css({ "display": "block" });
}
function OpenSalesman_admin_first() {
    CloseMenu();
    UpdateProfiles();
    $('#exit_mark_profile').hide();
    $('body').addClass('noscroll');
    $("#msg-box-salesman_admin").css({ "display": "block" });
    $("#msg-box-container-salesman_admin").css({ "display": "block" });
}
function CloseSalesman_admin() {
    $('body').removeClass('noscroll');
    $("#msg-box-salesman_admin").css({ "display": "none" });
    $("#msg-box-container-salesman_admin").css({ "display": "none" });
}

/****** CREATE SALESMAN *********/
function OpenSalesman() {
    CloseMenu();
    if (SessionActive() == false) {
        alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a este módulo');
        return false;
    }
    ProfileIsAdmin().then(function (r) {
        if (r == "1") {
            $('body').addClass('noscroll');
            $("#msg-box-salesman").css({ "display": "block" });
            $("#msg-box-container-salesman").css({ "display": "block" });
        } else {
            alertify.alert('', 'Debes ingresar al perfil de Administrador para acceder a este módulo');
            return false;
        }
    });
}
function CloseSalesman() {
    $('body').removeClass('noscroll');
    $("#msg-box-salesman").css({ "display": "none" });
    $("#msg-box-container-salesman").css({ "display": "none" });
}
/****** EDIT SALESMAN *********/
function OpenEditSalesman() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-editsalesman").css({ "display": "block" });
    $("#msg-box-container-editsalesman").css({ "display": "block" });
}
function CloseEditSalesman() {
    $('body').removeClass('noscroll');
    $("#msg-box-editsalesman").css({ "display": "none" });
    $("#msg-box-container-editsalesman").css({ "display": "none" });
}
/****** SET PASSWORD PROFILE *********/
function OpenSetPassProf() {
    CloseSalesman_admin();
    $('body').addClass('noscroll');
    $("#msg-box-SetPassProf").css({ "display": "block" });
    $("#msg-box-container-SetPassProf").css({ "display": "block" });
}
function CloseSetPassProf() {
    OpenSalesman_admin();
    $('body').removeClass('noscroll');
    $("#msg-box-SetPassProf").css({ "display": "none" });
    $("#msg-box-container-SetPassProf").css({ "display": "none" });
}
/****** ENTER PASSWORD PROFILE *********/
function OpenEntPassProf() {
    $('#PassProf').focus();
    CloseSalesman_admin();
    $('body').addClass('noscroll');
    $("#msg-box-EntPassProf").css({ "display": "block" });
    $("#msg-box-container-EntPassProf").css({ "display": "block" });
}
function CloseEntPassProf() {
    OpenSalesman_admin();
    $('body').removeClass('noscroll');
    $("#msg-box-EntPassProf").css({ "display": "none" });
    $("#msg-box-container-EntPassProf").css({ "display": "none" });
}
/******* CHOOSE AVATAR PROFILE ******/
function OpenSetAvatarProf(ModalToClose_Str) {
    if (ModalToClose_Str != null) {
        window[ModalToClose_Str]();
    }
    $('body').addClass('noscroll');
    $("#msg-box-SetAvatarProf").css({ "display": "block" });
    $("#msg-box-container-SetAvatarProf").css({ "display": "block" });
}
function CloseSetAvatarProf(ModalToOpenAfter_Str) {
    if (ModalToOpenAfter_Str != null) {
        window[ModalToOpenAfter_Str]();
    }
    $('body').removeClass('noscroll');
    $("#msg-box-SetAvatarProf").css({ "display": "none" });
    $("#msg-box-container-SetAvatarProf").css({ "display": "none" });
}
/*******  HELP SCAN ******/
function OpenHelpScan() {
    CloseMenu();
    goto_scan('1');
    $('body').addClass('noscroll');
    $("#msg-box-helpscan").css({ "display": "block" });
    $("#msg-box-container-helpscan").css({ "display": "block" });
}
function CloseHelpScan() {
    $('body').removeClass('noscroll');
    $("#msg-box-helpscan").css({ "display": "none" });
    $("#msg-box-container-helpscan").css({ "display": "none" });
}
/*******  IMPORT DATA PRODUCTS ******/
function OpenImportDataProduct() {
    CloseMenu();
    CloseNewItem();
    $('body').addClass('noscroll');
    $("#msg-box-impodatprod").css({ "display": "block" });
    $("#msg-box-container-impodatprod").css({ "display": "block" });
}
function CloseImportDataProduct() {
    $('#msg_idatprod').empty();
    $('#wrap_tableErr').empty();
    $('#cont_inputcsv').removeClass('cont_inputcsv_close');
    $('body').removeClass('noscroll');
    $("#msg-box-impodatprod").css({ "display": "none" });
    $("#msg-box-container-impodatprod").css({ "display": "none" });
}
/*******  HELP SCAN ******/
function OpenHelp() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-help").css({ "display": "block" });
    $("#msg-box-container-help").css({ "display": "block" });
}
function CloseHelp() {
    $('#help_container').empty();
    $('body').removeClass('noscroll');
    $("#msg-box-help").css({ "display": "none" });
    $("#msg-box-container-help").css({ "display": "none" });
}
function CloseHelp_over() {
    $('#help_container').empty();
    $("#msg-box-help").css({ "display": "none" });
    $("#msg-box-container-help").css({ "display": "none" });
}

/****** BARCODE FOUND */
function OpenBarcodeFound() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-barcodefound").css({ "display": "block" });
    $("#msg-box-container-barcodefound").css({ "display": "block" });
}
function CloseBarcodeFound() {
    ClearBarcodeFound();
    $('body').removeClass('noscroll');
    $("#msg-box-barcodefound").css({ "display": "none" });
    $("#msg-box-container-barcodefound").css({ "display": "none" });
}

/****** ITEMS BILL  ******/
function OpenItemsBill() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-itemsbill").css({ "display": "block" });
    $("#msg-box-container-itemsbill").css({ "display": "block" });
}
function CloseItemsBill() {
    $('body').removeClass('noscroll');
    $("#msg-box-itemsbill").css({ "display": "none" });
    $("#msg-box-container-itemsbill").css({ "display": "none" });
}
function CloseItemsBill_over() {
    $("#msg-box-itemsbill").css({ "display": "none" });
    $("#msg-box-container-itemsbill").css({ "display": "none" });
}

/****** FILTER BILL  ******/
function OpenFilterBills() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-filterbills").css({ "display": "block" });
    $("#msg-box-container-filterbills").css({ "display": "block" });
}
function CloseFilterBills() {
    $('body').removeClass('noscroll');
    $("#msg-box-filterbills").css({ "display": "none" });
    $("#msg-box-container-filterbills").css({ "display": "none" });
}
function CloseFilterBills_over() {
    $("#msg-box-filterbills").css({ "display": "none" });
    $("#msg-box-container-filterbills").css({ "display": "none" });
}


/***** UPDATE USER VARS ******/
function OpenUpdateUserVars() {
    CloseMenu();
    $("#newCountry").val(us_cont);
    $('body').addClass('noscroll');
    $("#msg-box-updateuservars").css({ "display": "block" });
    $("#msg-box-container-updateuservars").css({ "display": "block" });
}
function CloseUpdateUserVars() {
    $('body').removeClass('noscroll');
    $("#msg-box-updateuservars").css({ "display": "none" });
    $("#msg-box-container-updateuservars").css({ "display": "none" });
}
function CloseUpdateUserVars_over() {
    $("#msg-box-updateuservars").css({ "display": "none" });
    $("#msg-box-container-updateuservars").css({ "display": "none" });
}

/***** UPDATE FACTURACION ******/
function OpenUpdateFactNum() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-updatefactnum").css({ "display": "block" });
    $("#msg-box-container-updatefactnum").css({ "display": "block" });
}
function CloseUpdateFactNum() {
    $('body').removeClass('noscroll');
    $("#msg-box-updatefactnum").css({ "display": "none" });
    $("#msg-box-container-updatefactnum").css({ "display": "none" });
}
function CloseUpdateFactNum_over() {
    $("#msg-box-updatefactnum").css({ "display": "none" });
    $("#msg-box-container-updatefactnum").css({ "display": "none" });
}

/***** EDITAR FACTURACION ******/
function OpenEditFactNum() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-editfactnum").css({ "display": "block" });
    $("#msg-box-container-editfactnum").css({ "display": "block" });
}
function CloseEditFactNum() {
    $('body').removeClass('noscroll');
    $("#msg-box-editfactnum").css({ "display": "none" });
    $("#msg-box-container-editfactnum").css({ "display": "none" });
}
function CloseEditFactNum_over() {
    $("#msg-box-editfactnum").css({ "display": "none" });
    $("#msg-box-container-editfactnum").css({ "display": "none" });
}

/***** UPDATE USER VARS ******/
function OpenAdminResolutions() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-resolutionsadm").css({ "display": "block" });
    $("#msg-box-container-resolutionsadm").css({ "display": "block" });
}
function CloseAdminResolutions() {
    $('body').removeClass('noscroll');
    $("#msg-box-resolutionsadm").css({ "display": "none" });
    $("#msg-box-container-resolutionsadm").css({ "display": "none" });
}
function CloseAdminResolutions_over() {
    $("#msg-box-resolutionsadm").css({ "display": "none" });
    $("#msg-box-container-resolutionsadm").css({ "display": "none" });
}

/****** MARKETPLACE - ANALITICA */
function OpenMpAnalytics() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-mp_analitycs").css({ "display": "block" });
    $("#msg-box-container-mp_analitycs").css({ "display": "block" });
}
function CloseMpAnalytics() {
    $('body').removeClass('noscroll');
    $("#msg-box-mp_analitycs").css({ "display": "none" });
    $("#msg-box-container-mp_analitycs").css({ "display": "none" });
}
function CloseMpAnalytics_over() {
    $("#msg-box-mp_analitycs").css({ "display": "none" });
    $("#msg-box-container-mp_analitycs").css({ "display": "none" });
}

/****** MARKETPLACE - CHATBOT */
function OpenMpChatbot() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-mp_chatbot").css({ "display": "block" });
    $("#msg-box-container-mp_chatbot").css({ "display": "block" });
}
function CloseMpChatbot() {
    $('body').removeClass('noscroll');
    $("#msg-box-mp_chatbot").css({ "display": "none" });
    $("#msg-box-container-mp_chatbot").css({ "display": "none" });
}
function CloseMpChatbot_over() {
    $("#msg-box-mp_chatbot").css({ "display": "none" });
    $("#msg-box-container-mp_chatbot").css({ "display": "none" });
}

/****** MARKETPLACE - TRACKER */
function OpenMpTracker() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-mp_tracker").css({ "display": "block" });
    $("#msg-box-container-mp_tracker").css({ "display": "block" });
}
function CloseMpTracker() {
    $('body').removeClass('noscroll');
    $("#msg-box-mp_tracker").css({ "display": "none" });
    $("#msg-box-container-mp_tracker").css({ "display": "none" });
}
function CloseMpTracker_over() {
    $("#msg-box-mp_tracker").css({ "display": "none" });
    $("#msg-box-container-mp_tracker").css({ "display": "none" });
}


/****** SPEECH REPORT  ******/
function OpenSpeechReport() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-SpeechReport").css({ "display": "block" });
    $("#msg-box-container-SpeechReport").css({ "display": "block" });
}
function CloseSpeechReport() {
    $('body').removeClass('noscroll');
    $("#msg-box-SpeechReport").css({ "display": "none" });
    $("#msg-box-container-SpeechReport").css({ "display": "none" });
}
function CloseSpeechReport_over() {
    $("#msg-box-SpeechReport").css({ "display": "none" });
    $("#msg-box-container-SpeechReport").css({ "display": "none" });
}


/********** HELP VOICE COMMANDS  ************/
function OpenHelpModule() {
    CloseMenu();
    $('body').addClass('noscroll');
    $("#msg-box-HelpModule").css({ "display": "block" });
    $("#msg-box-container-HelpModule").css({ "display": "block" });
}
function CloseHelpModule() {
    $('body').removeClass('noscroll');
    $("#msg-box-HelpModule").css({ "display": "none" });
    $("#msg-box-container-HelpModule").css({ "display": "none" });
}
function CloseHelpModule_over() {
    $("#msg-box-HelpModule").css({ "display": "none" });
    $("#msg-box-container-HelpModule").css({ "display": "none" });
}


/****** CLASS ******/
class ModalBasic {
    constructor(name) {
        this.name = name;
    }
    open() {
        CloseMenu();
        $('body').addClass('noscroll');
        $("#msg-box-" + this.name).css({ "display": "block" });
        $("#msg-box-container-" + this.name).css({ "display": "block" });
    }
    close() {
        $('body').removeClass('noscroll');
        $("#msg-box-" + this.name).css({ "display": "none" });
        $("#msg-box-container-" + this.name).css({ "display": "none" });
    }
    close_over() {
        $("#msg-box-" + this.name).css({ "display": "none" });
        $("#msg-box-container-" + this.name).css({ "display": "none" });
    }
}
