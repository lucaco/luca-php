let tx_data_test = undefined;

// ACTUALIZAR EL TX_DATA DESPUES DE CADA NUEVA COMPRA Y ESO.

$(function () {

    class chatBotLuca {

        constructor() {
            this.init_msg = "Hola! Soy María, tu asistente virtual de Luca. Puedo ayudar con las dudas que tengas de tu negocio. Preguntame cualquier cosa.";
            this.defaultMsgApi = "Lo siento, no logré entenderte. Intenta reformular la pregunta.";
            this.api_key = "7BZubyla4N6bo1wqDzc9";
            this.numCurrNotifications = 0;
            this.numUserMsgs = 0;
            this.numServerMsgs = 0;
            this.ratingSent = false;
            this.hasRated = false;
            this.ratingMinInteractions = 10;
            this.lastInteraction = new Date().getTime();
            this.timerWaitRating = 10 * 60 * 1000; // 10 mins = 600000
            this.isOpen = false;
            this.tx_data = undefined;

            this.getSalesData();
            this.defineDates();
            this.createChat('María');
            this.clean_notyChat();

            // Timer for Rating and initial msg
            let me = this;
            setTimeout(function () {
                me.initialMsg();
                tx_data_test = me.tx_data;
            }, 5000);
            this.timeoutRating = setTimeout(function () { }, 1000);
            clearTimeout(this.timeoutRating);
        }

        createChat(nameServer = 'María') {
            let chatHTML = '';
            chatHTML += '<div class="chatBotContainer">';
            chatHTML += '<div>';
            chatHTML += '<div><span class="mdi mdi-robot"></span>' + nameServer + '</div>';
            chatHTML += '<div><div><span class="mdi mdi-delete"></span></div>';
            chatHTML += '<div><span class="mdi mdi-close"></span></div></div>';
            chatHTML += '</div>';
            chatHTML += '<div>';
            chatHTML += '<div class="msg_bx_chat"></div>';
            chatHTML += '<div><input></input><div><span class="mdi mdi-send-circle-outline"></span></div></div>';
            chatHTML += '</div>';
            chatHTML += '<div class="notyChat">0</div>';
            chatHTML += '</div>';
            $('body').append(chatHTML);
        }

        // Get all sales data from server
        getSalesData() {
            let me = this;
            $.ajax({
                url: 'get_dashboard_data.php',
                dataType: 'json',
                data: ({ x: 1 }),
                type: 'POST',
                success: function (tx_data) {
                    me.tx_data = me.sortDataByDate(tx_data);
                    console.log("Chatbot: Data loaded.");
                },
                error: function () {
                    console.log("Chatbot: Problem getting Sales Data from server");
                }
            });
        }

        updateSalesData() {
            let me = this;
            $.ajax({
                url: 'get_dashboard_data.php',
                dataType: 'json',
                data: ({ x: 1 }),
                type: 'POST',
                success: function (tx_data) {
                    me.tx_data = me.sortDataByDate(tx_data);
                    console.log("Chatbot: Data updated.");
                },
                error: function () {
                    console.log("Chatbot: Problem getting Sales Data from server");
                }
            });
        }

        // Send card object to chat
        sendCardToChat(title, itemsList, imgSrc) {
            let chatHTML = '<div class="cardChat">';
            chatHTML += '<div><img src="' + imgSrc + '"></div>';
            chatHTML += '<div><div class="title">' + title + '</div>';
            for (let i = 0; i < itemsList.length; i++) {
                chatHTML += itemsList[i];
            }
            chatHTML += '</div></div>';
            $('.msg_bx_chat').append(chatHTML);
            $('.msg_bx_chat').scrollTop($('.msg_bx_chat')[0].scrollHeight);
        }

        // Send Message to Chat
        sendMsgToChat(txt, side) {
            let dt = new Date();
            let mins = dt.getMinutes();
            if (mins < 10) {
                mins = "0" + mins;
            }
            let cur_time = dt.getHours() + ":" + mins;
            if (txt != "") {
                $(".chatBotContainer>div:nth-child(2)>div:first-child").append('<div class="botBubble ' + side + '"><p>' + txt + '</p><span>' + cur_time + '</span></div>');
                $(".chatBotContainer>div:nth-child(2)>div:nth-child(2)>input").val("");
                $('.msg_bx_chat').scrollTop($('.msg_bx_chat')[0].scrollHeight);
            }
            if (side == "User") {
                this.numUserMsgs += 1;
                this.lastInteraction = new Date().getTime();
                clearTimeout(this.timeoutRating);
            }
            if (side == "Server") {
                this.numServerMsgs += 1;
                this.add_notyChat(1);
            };

            //let countDown =  dt.getTime()-this.lastInteraction;
            //console.log(countDown);

            // If the user has interacting with the chat more than N times and the user has not talk for N seconds, then send a the rating.
            if (this.numUserMsgs + this.numServerMsgs >= this.ratingMinInteractions && !this.ratingSent) {
                this.resetTimerRating(this.timerWaitRating);
            }
        }

        getRandomIntVect(min, max, numItems) {
            min = Math.ceil(min);
            max = Math.floor(max);
            let vecResult = [];
            for (let i = 0; i < numItems; i++) {
                vecResult.push(Math.floor(Math.random() * (max - min + 1)) + min);
            }
            return vecResult;
        }

        // Send Options to Chat
        sendOptions(optionsList, functionsList) {
            let ids_obj = this.getRandomIntVect(10000, 99999, optionsList.length);
            let f1 = "<div class='chat_btnOps'>";
            for (let i = 0; i < optionsList.length; i++) {
                f1 = f1 + "<div id='" + ids_obj[i] + "'>" + optionsList[i] + "</div>";
            }
            f1 = f1 + "</div>";
            $('.msg_bx_chat').append(f1);

            // Associate Functions
            for (let j = 0; j < optionsList.length; j++) {
                if (functionsList[j] != null) {
                    document.getElementById(ids_obj[j]).onclick = functionsList[j];
                }
            }
            $('.msg_bx_chat').scrollTop($('.msg_bx_chat')[0].scrollHeight);
        }


        // Reset Timer of Rating
        resetTimerRating(timeTimer) {
            let me = this;
            clearTimeout(this.timeoutRating);
            this.timeoutRating = setTimeout(function () {
                if (!me.ratingSent) {
                    me.sendRating();
                    me.ratingSent = true;
                }
            }, timeTimer);
        }

        // Open Chat
        openChat() {
            this.clean_notyChat();
            this.isOpen = true;
            $(".chatBotContainer").addClass('anim_chat');
            this.animateObj(".botBubble");
            this.animateObj(".chat_btnOps");
            this.animateObj(".cardChat");
            setTimeout(function () {
                $(".chatBotContainer>div:nth-child(2)").show();
                $('.msg_bx_chat').scrollTop($('.msg_bx_chat')[0].scrollHeight);
            }, 700);
        }

        // Animate Object open
        animateObj(obj) {
            $(obj).css('opacity', '0');
            $(obj).addClass('anim_bubb_open');
        }

        // Close Chat
        closeChat() {
            this.isOpen = false;
            $(".chatBotContainer>div:nth-child(2)").hide();
            $(".chatBotContainer").removeClass('anim_chat');
            $(".botBubble").removeClass('anim_bubb_open');
            $(".chat_btnOps").removeClass('anim_bubb_open');
            $(".cardChat").removeClass('anim_bubb_open');
        }

        // Initial Message
        initialMsg() {
            let numItems = $('.msg_bx_chat>div').length;
            if (numItems === 0) {
                this.sendMsgToChat(this.init_msg, "Server");
            }
        }

        // Noty Chat - Clear
        clean_notyChat() {
            $(".notyChat").removeClass('anim_notyChat');
            $(".notyChat").html(0);
            this.numCurrNotifications = 0;
        }

        // Add Number to Noty Chat
        add_notyChat(addNum) {
            if (!this.isOpen) {
                let newNumber = this.numCurrNotifications + addNum;
                this.numCurrNotifications = newNumber;
                if (newNumber > 99) {
                    $(".notyChat").addClass('anim_notyChat');
                    $(".notyChat").html("+99");
                } else {
                    $(".notyChat").addClass('anim_notyChat');
                    $(".notyChat").html(newNumber);
                }
            }
        }

        // Clean Chat
        cleanChat() {
            $('.msg_bx_chat').html('');
            this.clean_notyChat();
            this.initialMsg();
        }

        // Rating - Send Rating to Chat
        sendRating() {
            let ids_obj = this.getRandomIntVect(10000, 99999, 5);
            this.sendMsgToChat('Hola! ¿Qué tan útil te ha resultado esta conversación?', 'Server');
            let chatHTML = '<div class="chatRating">';
            chatHTML += '<div id="' + ids_obj[0] + '"><p>Increíble!</p><img alt="Incredible" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaWQ9Ikljb25zIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmU1MDA7fS5jbHMtMntmaWxsOiNlYmNiMDA7fS5jbHMtM3tmaWxsOiNmZmY0OGM7fS5jbHMtNHtmaWxsOiM0NTQxM2M7b3BhY2l0eTowLjE1O30uY2xzLTV7ZmlsbDpub25lO30uY2xzLTUsLmNscy03LC5jbHMtOHtzdHJva2U6IzQ1NDEzYztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7fS5jbHMtNntmaWxsOiNmZmFhNTQ7fS5jbHMtN3tmaWxsOiNmZmIwY2E7fS5jbHMtOHtmaWxsOiNmZjg3YWY7fTwvc3R5bGU+PC9kZWZzPjx0aXRsZS8+PGcgZGF0YS1uYW1lPSImbHQ7R3JvdXAmZ3Q7IiBpZD0iX0dyb3VwXyI+PGNpcmNsZSBjbGFzcz0iY2xzLTEiIGN4PSIyNCIgY3k9IjIxLjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfIiByPSIyMCIvPjxwYXRoIGNsYXNzPSJjbHMtMiIgZD0iTTI0LDEuNWEyMCwyMCwwLDEsMCwyMCwyMEEyMCwyMCwwLDAsMCwyNCwxLjVabTAsMzdBMTguMjUsMTguMjUsMCwxLDEsNDIuMjUsMjAuMjUsMTguMjUsMTguMjUsMCwwLDEsMjQsMzguNVoiIGRhdGEtbmFtZT0iJmx0O0NvbXBvdW5kIFBhdGgmZ3Q7IiBpZD0iX0NvbXBvdW5kX1BhdGhfIi8+PGVsbGlwc2UgY2xhc3M9ImNscy0zIiBjeD0iMjQiIGN5PSI1LjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfMiIgcng9IjYiIHJ5PSIxLjUiLz48ZWxsaXBzZSBjbGFzcz0iY2xzLTQiIGN4PSIyNCIgY3k9IjQ1LjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfMyIgcng9IjE2IiByeT0iMS41Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTUiIGN4PSIyNCIgY3k9IjIxLjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfNCIgcj0iMjAiLz48cGF0aCBjbGFzcz0iY2xzLTYiIGQ9Ik0zOC41LDI2LjVjMCwuODMtMS4xMiwxLjUtMi41LDEuNXMtMi41LS42Ny0yLjUtMS41UzM0LjYyLDI1LDM2LDI1LDM4LjUsMjUuNjcsMzguNSwyNi41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8iLz48cGF0aCBjbGFzcz0iY2xzLTYiIGQ9Ik05LjUsMjYuNWMwLC44MywxLjEyLDEuNSwyLjUsMS41czIuNS0uNjcsMi41LTEuNVMxMy4zOCwyNSwxMiwyNSw5LjUsMjUuNjcsOS41LDI2LjVaIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXzIiLz48ZyBkYXRhLW5hbWU9IiZsdDtHcm91cCZndDsiIGlkPSJfR3JvdXBfMiI+PHBhdGggY2xhc3M9ImNscy03IiBkPSJNMTcuOTQsMjkuNWEuOTQuOTQsMCwwLDAtLjcxLjMzLDEsMSwwLDAsMC0uMjIuNzYsNy4wOSw3LjA5LDAsMCwwLDE0LDAsMSwxLDAsMCwwLS4yMi0uNzYuOTQuOTQsMCwwLDAtLjcxLS4zM1oiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfMyIvPjxwYXRoIGNsYXNzPSJjbHMtOCIgZD0iTTI5LjU3LDMzLjg0QTExLjM3LDExLjM3LDAsMCwwLDI0LDMyLjUzYTExLjM3LDExLjM3LDAsMCwwLTUuNTcsMS4zMSw3LjE2LDcuMTYsMCwwLDAsMTEuMTQsMFoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfNCIvPjwvZz48cGF0aCBjbGFzcz0iY2xzLTUiIGQ9Ik0xMCwyMi43NWExLjc1LDEuNzUsMCwwLDEsMy41LDAiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfNSIvPjxwYXRoIGNsYXNzPSJjbHMtNSIgZD0iTTM0LjUsMjIuNzVhMS43NSwxLjc1LDAsMCwxLDMuNSwwIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXzYiLz48L2c+PC9zdmc+"></div>';
            chatHTML += '<div id="' + ids_obj[1] + '"><p>Buena</p><img alt="Buena"" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaWQ9Ikljb25zIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmU1MDA7fS5jbHMtMntmaWxsOiNlYmNiMDA7fS5jbHMtM3tmaWxsOiNmZmY0OGM7fS5jbHMtNHtmaWxsOiM0NTQxM2M7b3BhY2l0eTowLjE1O30uY2xzLTUsLmNscy02e2ZpbGw6bm9uZTtzdHJva2U6IzQ1NDEzYztzdHJva2UtbGluZWNhcDpyb3VuZDt9LmNscy01e3N0cm9rZS1saW5lam9pbjpyb3VuZDt9LmNscy02e3N0cm9rZS1taXRlcmxpbWl0OjEwO30uY2xzLTd7ZmlsbDojZmZhYTU0O308L3N0eWxlPjwvZGVmcz48dGl0bGUvPjxnIGRhdGEtbmFtZT0iJmx0O0dyb3VwJmd0OyIgaWQ9Il9Hcm91cF8iPjxjaXJjbGUgY2xhc3M9ImNscy0xIiBjeD0iMjQiIGN5PSIyMS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXyIgcj0iMjAiLz48cGF0aCBjbGFzcz0iY2xzLTIiIGQ9Ik0yNCwxLjVhMjAsMjAsMCwxLDAsMjAsMjBBMjAsMjAsMCwwLDAsMjQsMS41Wm0wLDM3QTE4LjI1LDE4LjI1LDAsMSwxLDQyLjI1LDIwLjI1LDE4LjI1LDE4LjI1LDAsMCwxLDI0LDM4LjVaIiBkYXRhLW5hbWU9IiZsdDtDb21wb3VuZCBQYXRoJmd0OyIgaWQ9Il9Db21wb3VuZF9QYXRoXyIvPjxlbGxpcHNlIGNsYXNzPSJjbHMtMyIgY3g9IjI0IiBjeT0iNS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXzIiIHJ4PSI2IiByeT0iMS41Ii8+PGVsbGlwc2UgY2xhc3M9ImNscy00IiBjeD0iMjQiIGN5PSI0NS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXzMiIHJ4PSIxNiIgcnk9IjEuNSIvPjxjaXJjbGUgY2xhc3M9ImNscy01IiBjeD0iMjQiIGN5PSIyMS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXzQiIHI9IjIwIi8+PHBhdGggY2xhc3M9ImNscy02IiBkPSJNMjksMjkuNDhhOSw5LDAsMCwxLTEwLDAiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfIi8+PHBhdGggY2xhc3M9ImNscy03IiBkPSJNMzguNSwyNi41YzAsLjgzLTEuMTIsMS41LTIuNSwxLjVzLTIuNS0uNjctMi41LTEuNVMzNC42MiwyNSwzNiwyNSwzOC41LDI1LjY3LDM4LjUsMjYuNVoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfMiIvPjxwYXRoIGNsYXNzPSJjbHMtNyIgZD0iTTkuNSwyNi41YzAsLjgzLDEuMTIsMS41LDIuNSwxLjVzMi41LS42NywyLjUtMS41UzEzLjM4LDI1LDEyLDI1LDkuNSwyNS42Nyw5LjUsMjYuNVoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfMyIvPjxwYXRoIGNsYXNzPSJjbHMtNSIgZD0iTTEwLDIyLjc1YTEuNzUsMS43NSwwLDAsMSwzLjUsMCIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF80Ii8+PHBhdGggY2xhc3M9ImNscy01IiBkPSJNMzQuNSwyMi43NWExLjc1LDEuNzUsMCwwLDEsMy41LDAiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfNSIvPjwvZz48L3N2Zz4="></div>';
            chatHTML += '<div id="' + ids_obj[2] + '"><p>Neutral</p><img alt="Neutral" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaWQ9Ikljb25zIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmU1MDA7fS5jbHMtMntmaWxsOiNlYmNiMDA7fS5jbHMtM3tmaWxsOiNmZmY0OGM7fS5jbHMtNCwuY2xzLTZ7ZmlsbDojNDU0MTNjO30uY2xzLTR7b3BhY2l0eTowLjE1O30uY2xzLTV7ZmlsbDpub25lO30uY2xzLTUsLmNscy02e3N0cm9rZTojNDU0MTNjO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDt9LmNscy03e2ZpbGw6I2ZmYWE1NDt9PC9zdHlsZT48L2RlZnM+PHRpdGxlLz48ZyBkYXRhLW5hbWU9IiZsdDtHcm91cCZndDsiIGlkPSJfR3JvdXBfIj48Y2lyY2xlIGNsYXNzPSJjbHMtMSIgY3g9IjI0IiBjeT0iMjEuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV8iIHI9IjIwIi8+PHBhdGggY2xhc3M9ImNscy0yIiBkPSJNMjQsMS41YTIwLDIwLDAsMSwwLDIwLDIwQTIwLDIwLDAsMCwwLDI0LDEuNVptMCwzN0ExOC4yNSwxOC4yNSwwLDEsMSw0Mi4yNSwyMC4yNSwxOC4yNSwxOC4yNSwwLDAsMSwyNCwzOC41WiIgZGF0YS1uYW1lPSImbHQ7Q29tcG91bmQgUGF0aCZndDsiIGlkPSJfQ29tcG91bmRfUGF0aF8iLz48ZWxsaXBzZSBjbGFzcz0iY2xzLTMiIGN4PSIyNCIgY3k9IjUuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV8yIiByeD0iNiIgcnk9IjEuNSIvPjxlbGxpcHNlIGNsYXNzPSJjbHMtNCIgY3g9IjI0IiBjeT0iNDUuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV8zIiByeD0iMTYiIHJ5PSIxLjUiLz48Y2lyY2xlIGNsYXNzPSJjbHMtNSIgY3g9IjI0IiBjeT0iMjEuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV80IiByPSIyMCIvPjxwYXRoIGNsYXNzPSJjbHMtNiIgZD0iTTE0LjUsMjAuNWExLDEsMCwxLDEtMS0xQTEsMSwwLDAsMSwxNC41LDIwLjVaIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXyIvPjxwYXRoIGNsYXNzPSJjbHMtNiIgZD0iTTMzLjUsMjAuNWExLDEsMCwxLDAsMS0xQTEsMSwwLDAsMCwzMy41LDIwLjVaIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXzIiLz48cGF0aCBjbGFzcz0iY2xzLTciIGQ9Ik0zOC41LDI2LjVjMCwuODMtMS4xMiwxLjUtMi41LDEuNXMtMi41LS42Ny0yLjUtMS41UzM0LjYyLDI1LDM2LDI1LDM4LjUsMjUuNjcsMzguNSwyNi41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8zIi8+PHBhdGggY2xhc3M9ImNscy03IiBkPSJNOS41LDI2LjVjMCwuODMsMS4xMiwxLjUsMi41LDEuNXMyLjUtLjY3LDIuNS0xLjVTMTMuMzgsMjUsMTIsMjUsOS41LDI1LjY3LDkuNSwyNi41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF80Ii8+PGxpbmUgY2xhc3M9ImNscy01IiBkYXRhLW5hbWU9IiZsdDtMaW5lJmd0OyIgaWQ9Il9MaW5lXyIgeDE9IjE5IiB4Mj0iMjkiIHkxPSIzMSIgeTI9IjMxIi8+PC9nPjwvc3ZnPg=="></div>';
            chatHTML += '<div id="' + ids_obj[3] + '"><p>Mala</p><img alt="Mala" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaWQ9Ikljb25zIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmU1MDA7fS5jbHMtMntmaWxsOiNlYmNiMDA7fS5jbHMtM3tmaWxsOiNmZmY0OGM7fS5jbHMtNHtmaWxsOm5vbmU7fS5jbHMtNCwuY2xzLTZ7c3Ryb2tlOiM0NTQxM2M7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO30uY2xzLTUsLmNscy02e2ZpbGw6IzQ1NDEzYzt9LmNscy01e29wYWNpdHk6MC4xNTt9LmNscy03e2ZpbGw6I2ZmYWE1NDt9PC9zdHlsZT48L2RlZnM+PHRpdGxlLz48ZyBkYXRhLW5hbWU9IiZsdDtHcm91cCZndDsiIGlkPSJfR3JvdXBfIj48Y2lyY2xlIGNsYXNzPSJjbHMtMSIgY3g9IjI0IiBjeT0iMjEuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV8iIHI9IjIwIi8+PHBhdGggY2xhc3M9ImNscy0yIiBkPSJNMjQsMS41YTIwLDIwLDAsMSwwLDIwLDIwQTIwLDIwLDAsMCwwLDI0LDEuNVptMCwzN0ExOC4yNSwxOC4yNSwwLDEsMSw0Mi4yNSwyMC4yNSwxOC4yNSwxOC4yNSwwLDAsMSwyNCwzOC41WiIgZGF0YS1uYW1lPSImbHQ7Q29tcG91bmQgUGF0aCZndDsiIGlkPSJfQ29tcG91bmRfUGF0aF8iLz48ZWxsaXBzZSBjbGFzcz0iY2xzLTMiIGN4PSIyNCIgY3k9IjUuNSIgZGF0YS1uYW1lPSImbHQ7RWxsaXBzZSZndDsiIGlkPSJfRWxsaXBzZV8yIiByeD0iNiIgcnk9IjEuNSIvPjxjaXJjbGUgY2xhc3M9ImNscy00IiBjeD0iMjQiIGN5PSIyMS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXzMiIHI9IjIwIi8+PGVsbGlwc2UgY2xhc3M9ImNscy01IiBjeD0iMjQiIGN5PSI0NS41IiBkYXRhLW5hbWU9IiZsdDtFbGxpcHNlJmd0OyIgaWQ9Il9FbGxpcHNlXzQiIHJ4PSIxNiIgcnk9IjEuNSIvPjxwYXRoIGNsYXNzPSJjbHMtNiIgZD0iTTE1LDIxLjVhMSwxLDAsMSwxLTEtMUExLDEsMCwwLDEsMTUsMjEuNVoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfIi8+PHBhdGggY2xhc3M9ImNscy03IiBkPSJNOS41LDI2LjVjMCwuODMsMS4xMiwxLjUsMi41LDEuNXMyLjUtLjY3LDIuNS0xLjVTMTMuMzgsMjUsMTIsMjUsOS41LDI1LjY3LDkuNSwyNi41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8yIi8+PHBhdGggY2xhc3M9ImNscy02IiBkPSJNMzMsMjEuNWExLDEsMCwxLDAsMS0xQTEsMSwwLDAsMCwzMywyMS41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8zIi8+PHBhdGggY2xhc3M9ImNscy03IiBkPSJNMzguNSwyNi41YzAsLjgzLTEuMTIsMS41LTIuNSwxLjVzLTIuNS0uNjctMi41LTEuNVMzNC42MiwyNSwzNiwyNSwzOC41LDI1LjY3LDM4LjUsMjYuNVoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfNCIvPjxwYXRoIGNsYXNzPSJjbHMtNCIgZD0iTTI5LjYyLDMwLjY3Yy0yLjYyLS45LTYuNjYsMC05Ljg0LDIuMzQiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7IiBpZD0iX1BhdGhfNSIvPjwvZz48L3N2Zz4="></div>';
            chatHTML += '<div id="' + ids_obj[4] + '"><p>Terrible</p><img alt="Terrible" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaWQ9Ikljb25zIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmU1MDA7fS5jbHMtMntmaWxsOiNlYmNiMDA7fS5jbHMtM3tmaWxsOiNmZmY0OGM7fS5jbHMtNHtmaWxsOiM0NTQxM2M7b3BhY2l0eTowLjE1O30uY2xzLTV7ZmlsbDpub25lO30uY2xzLTUsLmNscy03LC5jbHMtOHtzdHJva2U6IzQ1NDEzYztzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7fS5jbHMtNntmaWxsOiNmZmFhNTQ7fS5jbHMtN3tmaWxsOiNmZmIwY2E7fS5jbHMtOHtmaWxsOiNmZjg2NmU7fTwvc3R5bGU+PC9kZWZzPjx0aXRsZS8+PGcgZGF0YS1uYW1lPSImbHQ7R3JvdXAmZ3Q7IiBpZD0iX0dyb3VwXyI+PGNpcmNsZSBjbGFzcz0iY2xzLTEiIGN4PSIyNCIgY3k9IjIxLjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfIiByPSIyMCIvPjxwYXRoIGNsYXNzPSJjbHMtMiIgZD0iTTI0LDEuNWEyMCwyMCwwLDEsMCwyMCwyMEEyMCwyMCwwLDAsMCwyNCwxLjVabTAsMzdBMTguMjUsMTguMjUsMCwxLDEsNDIuMjUsMjAuMjUsMTguMjUsMTguMjUsMCwwLDEsMjQsMzguNVoiIGRhdGEtbmFtZT0iJmx0O0NvbXBvdW5kIFBhdGgmZ3Q7IiBpZD0iX0NvbXBvdW5kX1BhdGhfIi8+PGVsbGlwc2UgY2xhc3M9ImNscy0zIiBjeD0iMjQiIGN5PSI1LjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfMiIgcng9IjYiIHJ5PSIxLjUiLz48ZWxsaXBzZSBjbGFzcz0iY2xzLTQiIGN4PSIyNCIgY3k9IjQ1LjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfMyIgcng9IjE2IiByeT0iMS41Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTUiIGN4PSIyNCIgY3k9IjIxLjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfNCIgcj0iMjAiLz48cGF0aCBjbGFzcz0iY2xzLTYiIGQ9Ik0zOC41LDI2LjVjMCwuODMtMS4xMiwxLjUtMi41LDEuNXMtMi41LS42Ny0yLjUtMS41UzM0LjYyLDI1LDM2LDI1LDM4LjUsMjUuNjcsMzguNSwyNi41WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8iLz48cGF0aCBjbGFzcz0iY2xzLTYiIGQ9Ik05LjUsMjYuNWMwLC44MywxLjEyLDEuNSwyLjUsMS41czIuNS0uNjcsMi41LTEuNVMxMy4zOCwyNSwxMiwyNSw5LjUsMjUuNjcsOS41LDI2LjVaIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXzIiLz48ZyBkYXRhLW5hbWU9IiZsdDtHcm91cCZndDsiIGlkPSJfR3JvdXBfMiI+PGxpbmUgY2xhc3M9ImNscy01IiBkYXRhLW5hbWU9IiZsdDtMaW5lJmd0OyIgaWQ9Il9MaW5lXyIgeDE9IjEyIiB4Mj0iMTUuNSIgeTE9IjE5IiB5Mj0iMjIuNSIvPjxsaW5lIGNsYXNzPSJjbHMtNSIgZGF0YS1uYW1lPSImbHQ7TGluZSZndDsiIGlkPSJfTGluZV8yIiB4MT0iMTUuNSIgeDI9IjEyIiB5MT0iMTkiIHkyPSIyMi41Ii8+PC9nPjxnIGRhdGEtbmFtZT0iJmx0O0dyb3VwJmd0OyIgaWQ9Il9Hcm91cF8zIj48bGluZSBjbGFzcz0iY2xzLTUiIGRhdGEtbmFtZT0iJmx0O0xpbmUmZ3Q7IiBpZD0iX0xpbmVfMyIgeDE9IjM2IiB4Mj0iMzIuNSIgeTE9IjE5IiB5Mj0iMjIuNSIvPjxsaW5lIGNsYXNzPSJjbHMtNSIgZGF0YS1uYW1lPSImbHQ7TGluZSZndDsiIGlkPSJfTGluZV80IiB4MT0iMzIuNSIgeDI9IjM2IiB5MT0iMTkiIHkyPSIyMi41Ii8+PC9nPjxwYXRoIGNsYXNzPSJjbHMtNSIgZD0iTTEwLjY3LDE2YTQuNDYsNC40NiwwLDAsMSwyLjgzLTEsNC41MSw0LjUxLDAsMCwxLDIuODMsMSIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF8zIi8+PHBhdGggY2xhc3M9ImNscy01IiBkPSJNMzcuMzMsMTZhNC40Niw0LjQ2LDAsMCwwLTIuODMtMSw0LjUxLDQuNTEsMCwwLDAtMi44MywxIiBkYXRhLW5hbWU9IiZsdDtQYXRoJmd0OyIgaWQ9Il9QYXRoXzQiLz48ZyBkYXRhLW5hbWU9IiZsdDtHcm91cCZndDsiIGlkPSJfR3JvdXBfNCI+PGNpcmNsZSBjbGFzcz0iY2xzLTciIGN4PSIyNCIgY3k9IjMxLjUiIGRhdGEtbmFtZT0iJmx0O0VsbGlwc2UmZ3Q7IiBpZD0iX0VsbGlwc2VfNSIgcj0iNy41Ii8+PGVsbGlwc2UgY2xhc3M9ImNscy04IiBjeD0iMjQiIGN5PSIzNS40OCIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiIGlkPSJfUGF0aF81IiByeD0iNi4zNSIgcnk9IjMuNTIiLz48L2c+PC9nPjwvc3ZnPg=="></div>';
            chatHTML += '</div>';
            $('.msg_bx_chat').append(chatHTML);
            for (let i = 0; i < ids_obj.length; i++) {
                document.getElementById(ids_obj[i]).onclick = () => { this.ratingChat(this, i); };
            }
            $('.msg_bx_chat').scrollTop($('.msg_bx_chat')[0].scrollHeight);
            $(".chatRating").css('opacity', '0');
            $(".chatRating").addClass('anim_bubb_open');
            this.add_notyChat(1);
        }

        // TO-DO: Send the rating of the current chat to the server.
        ratingChat(el, rating) {
            if (!el.hasRated) {
                console.log("Rating:" + rating);
                el.sendMsgToChat("¡Gracias por tomarte el tiempo de calificarme!", "Server");
                el.hasRated = true;
            } else {
                console.log("Rating:" + rating);
                el.sendMsgToChat("Has actualizado tu calificación.", "Server");
            }

        }

        // Make request to the API Service
        call_api(query) {

            console.log(this.fullTextAnalysis(query));

            // Make calls to the API of chatbot
            let me = this;
            let request = new XMLHttpRequest();
            request.open('POST', 'https://api-luca.herokuapp.com/api/v1/model/chatbot/predict?key_id=' + this.api_key + '&query=' + query, true)
            request.onload = function () {
                var data = JSON.parse(this.response);
                if (request.status >= 200 && request.status < 400) {

                    let respuesta = data['answer'];
                    console.log("Probabilidad: " + data['predict_proba']);

                    if (respuesta == "--Contar Clientes Total--") {
                        me.chatContarClientesTodos();
                    } else if (respuesta == '--Contar Productos Total--') {
                        me.chatContarProductosTodos();
                    } else if (respuesta == '--procesar_pago--') {
                        checkoutStart();
                        me.closeChat();
                    } else if (respuesta == '--limpiar_cuenta--') {
                        clear_bill();
                        me.sendMsgToChat("Listo, la cuenta actual fue limpiada.", "Server");
                    } else if (respuesta == '--nuevo_cliente--') {
                        OpenNewCustomer();
                        me.closeChat();
                    } else if (respuesta == '--nuevo_item--') {
                        OpenNewItem();
                        me.closeChat();
                    } else if (respuesta == '--reporte_general_ventas--') {
                        me.salesInterpreter(query);
                    } else {
                        if (respuesta != me.defaultMsgApi) {
                            me.sendMsgToChat(respuesta, "Server");
                        } else {
                            let somethingFoundCust = me.createMatchCustomers(query);
                            let somethingFouncItem = me.createMatchItems(query);
                            if (!somethingFoundCust && !somethingFouncItem) {
                                me.sendMsgToChat(respuesta, "Server");
                            }
                        }

                    }
                }
            }
            request.send();
        }

        // FUNCTION: Contar Todos los Clientes 
        chatContarClientesTodos() {
            let num_cust = AllCustomersJSON.length;
            if (num_cust > 0) {
                this.sendMsgToChat("Actualmente tienes " + num_cust + " clientes.", "Server");
            } else {
                this.sendMsgToChat("Actualmente no tienes clientes. Da click en el siguiente botón para agregar tu primer cliente.", "Server");
                this.sendOptions(["<div onclick='OpenNewCustomer()'>Crear nuevo cliente</div>"])
            }
        }

        // FUNCTION: Contar Todos los Productos 
        chatContarProductosTodos() {
            let num_prods = dataProducts.length;
            if (num_prods > 0) {
                this.sendMsgToChat("Actualmente tienes " + num_prods + " ítems en tu negocio.", "Server");
            } else {
                this.sendMsgToChat("Actualmente no tienes ítems creados. Da click en el siguiente botón para agregar tu primer ítem.", "Server");
                this.sendOptions(["<div onclick='OpenNewItem()'>Crear nuevo ítem</div>"])
            }
        }

        // FUNCTION: Agreagar item a la cuenta.
        chatAgregarItemCuenta(nombre_item) {

            let regex = new RegExp(nombre_item, "ig");
            let resultId = [];
            let resultName = [];
            let c = 0;

            // Contar item.
            dataProducts.forEach(function (val) {
                let nombre = quitaacentos(val.prod_name);
                let ref_code = val.prod_id;
                if ((nombre.search(regex) != -1) || (ref_code.search(regex) != -1)) {
                    nameItem = val.prod_name;
                    resultId.push(val.prod_id);
                    resultName.push(val.prod_name);
                    c = c + 1;
                }
            });

            if (c > 1) {
                this.sendMsgToChat("Encontré " + c + " ítems que coinciden con el que deseas agregar. ¿Cuál deseas que agregue a la cuenta?", "Server");
                let listOptions = []
                for (let i = 0; i < resultId.length; i++) {
                    listOptions.push("<div onclick='addItemtoBillByIdCode(&#39;" + resultId[i] + "&#39;,false)'>" + resultName[i] + " (" + resultId[i] + ")</div>")
                }
                this.sendOptions(listOptions)
            } else if (c === 1) {
                addItemtoBillByIdCode(resultId[0], false);
                this.sendMsgToChat("El ítem " + resultName[0] + " (" + resultId[0] + ") fue agregado a la cuenta. <br></br><span class='link_opt' onclick='deleteItemtoBillByIdCode(&#39;" + resultId[0] + "&#39;)'>Deshacer acción</span>", "Server");
            } else {
                this.sendMsgToChat("No encontré un ítem con ese nombre o Id.", "Server");
            }
        }

        // FUNCTION: Full Analysis of Text
        fullTextAnalysis(text) {
            // Search for items
            let itemsMatch = this.searchItemExistText(text);
            // Search for customers
            let customerMatch = this.searchClientExistInText(text);
            // Search for commands
            let commandsMatch = this.searchOperation(text);
            // Search for entities 
            let entitiesMatch = this.searchEntities(text);
            // Search for numeric values
            let numericMatch = this.searchNumbers(text);
            // Search for dates
            let datesMatch = this.searchDates(text);

            return { itemsMatch, customerMatch, commandsMatch, entitiesMatch, numericMatch, datesMatch }
        }

        // FUNCTION: Search for items in specific text.
        searchItemExistText(text) {
            let matches = [];
            let flags = [];

            let text_cln = nl_CleanStopWords(quitaacentos2(text.toLowerCase()));
            let text_tkn = text_cln.split(" ");

            if (text_cln.length > 0) {
                dataProducts.forEach(el => {
                    var prod_name_cln = nl_CleanStopWords(quitaacentos2(el.prod_name.toLowerCase()));
                    var prod_id_cln = nl_CleanStopWords(quitaacentos2(el.prod_id.toLowerCase()));
                    text_tkn.forEach(text_w => {
                        if (prod_name_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push('Nombre');
                        } else if (prod_id_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push('Código');
                        }
                    });
                });
            }

            let finalMatches = [];
            let finalKeys = [];
            let finalFlags = [];
            let numFound = 0;
            for (let i = 0; i < matches.length; i++) {
                if (finalKeys.indexOf(matches[i].prod_id) === -1) {
                    finalMatches.push(matches[i]);
                    finalFlags.push(flags[i]);
                    finalKeys.push(matches[i].prod_id);
                    numFound += 1;
                }
            }

            return { matches: finalMatches, flags: finalFlags, number: numFound };

        }

        // FUNCTION: Search for clients inside a specific text.
        searchClientExistInText(text) {
            let matches = [];
            let flags = [];

            let text_cln = nl_CleanStopWords(quitaacentos2(text.toLowerCase()));
            let text_tkn = text_cln.split(" ");

            if (text_cln.length > 0) {
                AllCustomersJSON.forEach(el => {
                    var name_cln = nl_CleanStopWords(quitaacentos2(el.cust_name.toLowerCase()));
                    var id_cln = nl_CleanStopWords(quitaacentos2(el.cust_id.toLowerCase()));
                    var email_cln = nl_CleanStopWords(quitaacentos2(el.cust_email.toLowerCase()));
                    var phone_cln = nl_CleanStopWords(quitaacentos2(el.cust_phone.toLowerCase()));
                    text_tkn.forEach(text_w => {
                        if (name_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push("Nombre");
                        } else if (id_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push("ID");
                        } else if (email_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push("Email");
                        } else if (phone_cln.search(RegExp("(\\b)" + text_w + "(\\b)", "ig")) != -1) {
                            matches.push(el);
                            flags.push("Teléfono");
                        }
                    });
                });
            }

            let finalMatches = [];
            let finalKeys = [];
            let finalFlags = [];
            let numFound = 0;
            for (let i = 0; i < matches.length; i++) {
                if (finalKeys.indexOf(matches[i].cust_key) === -1) {
                    finalMatches.push(matches[i]);
                    finalFlags.push(flags[i]);
                    finalKeys.push(matches[i].cust_key);
                    numFound += 1;
                }
            }

            return { matches: finalMatches, flags: finalFlags, number: numFound };
        }

        // FUNCTION: Clean domain specific stop words
        clearDomainStopWords(text) {
            let searchKeys = /(\b)(quiero|por favor|porfavor|dime|dame|di)(\b)/g;
            return text.replace(searchKeys, "").trim();
        }

        // FUNCTION: Search dates in text
        searchDates(text) {
            // TEST searchDates("ventas del 20 de abril del 2010 y 20/01/2010 y 01/01/10 y 10-31-2010 y 12 de enero y 17 de mayo de 1990 y las ventas de febrero y tambien del 2020 pero tambien del martes")
            let months1 = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
            let months2 = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
            let dicTxtDates1 = { 'hoy': this.today, 'ayer': this.yesterday };
            let dicTxtDates2 = {
                'domingo': this.lastDom, 'lunes': this.lastLun, 'martes': this.lastMartes, 'miercoles': this.lastMie, 'jueves': this.lastJue, 'viernes': this.lastVie, 'sabado': this.lastSab,
                'dom': this.lastDom, 'lun': this.lastLun, 'mar': this.lastMartes, 'mie': this.lastMie, 'jue': this.lastJue, 'vie': this.lastVie, 'sab': this.lastSab
            };

            function transformDate(el) {
                let d = new Date();
                let d_y = d.getFullYear();
                let d_m = d.getMonth() + 1;
                let d_d = d.getDate();
                let x, y;

                el = el.replace(/\s|\.|\-/gi, "/").trim();
                el = el.split("/").map((el2) => {
                    if (months1.indexOf(el2) > -1) {
                        x = months1.indexOf(el2) + 1;
                        y = (x <= 9) ? "0" + x : x;
                        return y;
                    } else if (months2.indexOf(el2) > -1) {
                        x = months2.indexOf(el2) + 1;
                        y = (x <= 9) ? "0" + x : x;
                        return y;
                    } else {
                        y = (parseInt(el2) <= 9) ? "0" + el2 : el2;
                        return y;
                    }

                }).join("/");

                if (el.length == 5) {
                    //"ddmmmm" - "dd/mm"  
                    el = new Date(Date.parse(el.split("/")[1] + "/" + el.split("/")[0] + "/" + d_y))
                }

                return el;
            }

            let matches = [];
            let finalDates = { 'punctual': [], 'ranges': [] };
            text = quitaacentos2(text.toLowerCase()).trim();
            text = text.replace(/,| del| de/gi, "");

            let dF1, dF2, dF3, dF4, dF5, dF6, dF7, dF8, dF9, dF10, dF11, dF12, dF13, dF14, dF15, dF16, dF17;

            // Format 1: dd/mm/yyyy
            try { dF1 = text.match(/(\b)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-2])(\-|\/|\s|\.)(19[0-9][0-9]|2[0-1][0-9]{2})(\b)/gi).map((el) => new Date(Date.parse(el.replace(/\s|\.|\-/gi, "/").trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$2/$1/$3")))) } catch { dF1 = [] };
            // Format 2: mm/dd/yyyy
            try { dF2 = text.match(/(\b)([1-9]|0[1-9]|1[0-2])(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)(19[0-9][0-9]|2[0-1][0-9]{2})(\b)/gi).map((el) => new Date(Date.parse(el.replace(/\s|\.|\-/gi, "/").trim()))) } catch { dF2 = [] };
            // Format 3: yyyy/mm/dd
            try { dF3 = text.match(/(\b)(19[0-9][0-9]|2[0-1][0-9]{2})(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-2])(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\b)/gi).map((el) => new Date(Date.parse(el.replace(/\s|\.|\-/gi, "/").trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$2/$3/$1")))) } catch { dF3 = [] };
            // Format 4: yyyy/dd/mm
            try { dF4 = text.match(/(\b)(19[0-9][0-9]|2[0-1][0-9]{2})(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-2])(\b)/gi).map((el) => new Date(Date.parse(el.replace(/\s|\.|\-/gi, "/").trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$3/$2/$1")))) } catch { dF4 = [] };
            // Format 5: dd/mmmm/yyyy 
            try { dF5 = text.match(/(\b)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(\-|\/|\s|\.)(19[0-9][0-9]|2[0-1][0-9]{2})(\b)/gi).map((el) => new Date(Date.parse(transformDate(el).trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$2/$1/$3")))) } catch { dF5 = [] };
            // Format 6: mmmm/dd/yyyy
            try { dF6 = text.match(/(\b)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)(19[0-9][0-9]|2[0-1][0-9]{2})(\b)/gi).map((el) => new Date(Date.parse(transformDate(el).trim()))) } catch { dF6 = [] };
            // Format 7: yyyy/mmmm/dd
            try { dF7 = text.match(/(\b)(19[0-9][0-9]|2[0-1][0-9]{2})(\-|\/|\s|\.)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\b)/gi).map((el) => new Date(Date.parse(transformDate(el).trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$2/$3/$1")))) } catch { dF7 = [] };
            // Format 8: yyyy/dd/mmmm
            try { dF8 = text.match(/(\b)(19[0-9][0-9]|2[0-1][0-9]{2})(\-|\/|\s|\.)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\-|\/|\s|\.)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(\b)/gi).map((el) => new Date(Date.parse(transformDate(el).trim().replace(/(\d{1,4})\/(\d{1,4})\/(\d{1,4})/, "$3/$2/$1")))) } catch { dF8 = [] };
            // Format 9: Weekday day alone with no context. lunes,martes,miercoles,lun,mie,...
            try { dF9 = text.match(/(\b)(lunes|lun|martes|miercoles|mie|jueves|jue|viernes|vie|sabado|sab|domingo|dom)(?=\s[A-Za-z]+|$)(\b)/gi).map((el) => dicTxtDates2[el.trim()]) } catch { dF9 = [] };
            // Format 10: Month alone with no context. enero, marzo, abril, mayo,...
            //try { dF10 = text.match(/(\b)(?<=^|[A-Za-z]+\s)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(?=\s[A-Za-z]+|$)(\b)/gi).map(transformDate) } catch { dF10 = [] };
            // Format 11: dd mmmm -> 13 abril, 13 mayo, 45 enero,.... (poner presente año)
            try { dF11 = text.match(/(\b)([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\s)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(?=\s[A-Za-z]+|$)(\b)/gi).map(transformDate) } catch { dF11 = [] };
            // Format 12: mmmm yyyy -> abril 2010
            //try { dF12 = text.match(/(\b)(?<=^|[A-Za-z]+\s)(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)(\-|\/|\s|\.)(19[0-9][0-9]|2[0-1][0-9]{2})(\b)/gi).map(transformDate) } catch { dF12 = [] };
            // Format 13: Year alone with no context. 2010, 2020, ....
            //try { dF13 = text.match(/(\b)(?<!(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic|\/|\-|\.|\+|\*|\,|\~|\\|\;|\d)(\s|))(\d{4})(?!(\s|)(\/|\-|\.|\+|\*|\,|\~|\\|\;|\d))(\b)/gi).map(transformDate) } catch { dF13 = [] };
            // Format 14: dia/semana/mes/año pasado/anterior
            try { dF14 = text.match(/(\b)(dia|semana|mes|año)(\s)(pasado|pasada|anterior)/gi).map((el) => el.trim()) } catch { dF14 = [] };
            // Format 15: hace N dias/semanas/meses/años
            try { dF15 = text.replace(/\bun\b/gi, "1").replace(/\bdos\b/gi, "2").replace(/\btres\b/gi, "3").match(/(\b)(hace)(\s\d{1,3}\s)(dia|dias|semana|semanas|mes|meses|año|años)(\b)/gi).map((el) => el.trim()) } catch { dF15 = [] };
            // Format 16: hoy|ayer
            try { dF16 = text.match(/(\b)(hoy|ayer)(\b)/gi).map((el) => dicTxtDates1[el.trim()]) } catch { dF16 = [] };
            // Format 17: semana|dia|mes|año sin contexto
            try { dF17 = text.match(/(\b)(semana|dia|mes|año)(?!\s(\d{1,4}|anterior|paso|pasado|pasada|antes|hoy|ayer|enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic|lunes|lun|martes|miercoles|mie|jueves|jue|viernes|vie|sabado|sab|domingo|dom))(\b)/gi).map((el) => el.trim()) } catch { dF17 = [] };

            if (dF1.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "dd/mm/yyyy", 'format_trans:': "mm/dd/yyyy", 'matches': dF1 });
            if (dF2.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "mm/dd/yyyy", 'format_trans:': "mm/dd/yyyy", 'matches': dF2 });
            if (dF3.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "yyyy/mm/dd", 'format_trans:': "mm/dd/yyyy", 'matches': dF3 });
            if (dF4.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "yyyy/dd/mm", 'format_trans:': "mm/dd/yyyy", 'matches': dF4 });
            if (dF5.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "dd/mmmm/yyyy", 'format_trans:': "mm/dd/yyyy", 'matches': dF5 });
            if (dF6.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "mmmm/dd/yyyy", 'format_trans:': "mm/dd/yyyy", 'matches': dF6 });
            if (dF7.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "yyyy/mmmm/dd", 'format_trans:': "mm/dd/yyyy", 'matches': dF7 });
            if (dF8.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "yyyy/dd/mmmm", 'format_trans:': "mm/dd/yyyy", 'matches': dF8 });
            if (dF9.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "wwww", 'format_trans:': "mm/dd/yyyy", 'matches': dF9 });
            //if (dF10.length > 0) matches.push({ 'type': 'range', 'format_orig:': "mmmm", 'format_trans:': "mmmm", 'matches': dF10 });
            if (dF11.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "ddmmmm", 'format_trans:': "mm/dd/yyyy", 'matches': dF11 });
            //if (dF12.length > 0) matches.push({ 'type': 'range', 'format_orig:': "mmmmyyyy", 'format_trans:': "mmmmyyyy", 'matches': dF12 });
            //if (dF13.length > 0) matches.push({ 'type': 'range', 'format_orig:': "yyyy", 'format_trans:': "yyyy", 'matches': dF13 });
            if (dF14.length > 0) matches.push({ 'type': 'range', 'format_orig:': "pasado1", 'format_trans:': "pasado1", 'matches': dF14 });
            if (dF15.length > 0) matches.push({ 'type': 'range', 'format_orig:': "pasado2", 'format_trans:': "pasado2", 'matches': dF15 });
            if (dF16.length > 0) matches.push({ 'type': 'punctual', 'format_orig:': "hoy|ayer", 'format_trans:': "mm/dd/yyyy", 'matches': dF16 });
            if (dF17.length > 0) matches.push({ 'type': 'range', 'format_orig:': "tiempos2", 'format_trans:': "tiempos2", 'matches': dF17 });

            if (dF1.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF1);
            if (dF2.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF2);
            if (dF3.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF3);
            if (dF4.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF4);
            if (dF5.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF5);
            if (dF6.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF6);
            if (dF7.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF7);
            if (dF8.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF8);
            if (dF9.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF9);
            if (dF11.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF11);
            if (dF16.length > 0) finalDates.punctual.push.apply(finalDates.punctual, dF16);

            let number = finalDates.punctual.length + finalDates.ranges.length;

            return { matches, number, finalDates };
        }

        // FUNCTION: Search numbers in text
        searchNumbers(text) {
            let matches;
            text = quitaacentos2(text.toLowerCase());
            try { matches = text.replace(/([A-Za-z])(\s)(cien mil)/gi, (str, p1, p2, p3) => { return p1 + " 100000" }).replace(/([A-Za-z])(\s)(mil)/gi, (str, p1, p2, p3) => { return p1 + " 1000" }).replace(/([A-Za-z])(\s)(cien)/gi, (str, p1, p2, p3) => { return p1 + " 100" }).replace(/(\d{0,15})(\.|)(\d{0,20}|)(\s|)(millones|millon|millón)/gi, (str, p1, p2, p3, p4, p5) => { return parseFloat(p1 + p2 + p3) * 1000000 }).replace(/(\d{0,15})(\.|)(\d{0,20}|)(\s|)(mil|miles)/gi, (str, p1, p2, p3, p4, p5) => { return parseFloat(p1 + p2 + p3) * 1000 }).replace(/[^\d \.]/ig, "").trim().match(/\d{0,20}(\.|)(\d{1,20})/ig).map((el) => { return parseFloat(el) }) } catch { matches = [] };
            return { matches, number: matches.length };
        }

        // FUNCTION: Search Operation
        searchOperation(text) {
            let matches = [];
            text = nl_CleanStopWords(quitaacentos2(text.toLowerCase()));

            if (text.length > 0) {
                let searchKeys = /(\b)(busqueda|buscar|busca|localiza|localizar|localizacion|selecciona|seleccion|muestrame|mostrar)(\b)/;
                let editKeys = /(\b)(editar|edita|modifica|modificar|cambiar|cambia|alterar|altera|reformar|reforma|modificacion|alteracion|edicion)(\b)/;
                let removeKeys = /(\b)(remover|remueve|quitar|quita|sacar|saca|descartar|descarta|suprimir|suprime)(\b)/;
                let deleteKeys = /(\b)(elimiar|elimina|eliminacion)(\b)/;
                let addKeys = /(\b)(agregar|agrega|adiciona|adicionar|incorpora|incorporar|incorporacion|incluir|incluye)(\b)/;
                let openKeys = /(\b)(abrir|abre|inicia|iniciar)(\b)/;
                let scanKeys = /(\b)(escanear|escanea|leer codigo|scanner)(\b)/;
                let newKeys = /(\b)(nuevo|crear|crea|construir|contruye|creacion)(\b)/;
                let payKeys = /(\b)(pagar|paga|pago|pagando|pagamento|procesar|procesa|procesando)(\b)/;
                let exportKeys = /(\b)(guardar|guarda|salva|salvar|exportar|exporta|exportacion|exportando)(\b)/;
                let importKeys = /(\b)(importar|importa|importación)(\b)/;
                let sendKeys = /(\b)(enviar|envia|envio)(\b)/;
                let reportKeys = /(\b)(reportar|reporte|informe|estadisticas|estadistica|analizar|analitica|analisis|calcular|calcula|calculo|estimar|estima)(\b)/;

                if (editKeys.test(text)) matches.push('editar');
                if (newKeys.test(text)) matches.push('crear');
                if (addKeys.test(text)) matches.push('agregar');
                if (searchKeys.test(text)) matches.push('buscar');
                if (removeKeys.test(text)) matches.push('remover');
                if (deleteKeys.test(text)) matches.push('eliminar');
                if (reportKeys.test(text)) matches.push('reporte');
                if (openKeys.test(text)) matches.push('abrir');
                if (payKeys.test(text)) matches.push('pagar');
                if (exportKeys.test(text)) matches.push('exportar');
                if (importKeys.test(text)) matches.push('importar');
                if (sendKeys.test(text)) matches.push('enviar');
                if (scanKeys.test(text)) matches.push('escanear');
            }

            return { matches, number: matches.length };
        }

        // FUNCTION: Search Entities (client, item, ...)
        searchEntities(text) {

            let matches = [];

            text = nl_CleanStopWords(quitaacentos2(text.toLowerCase()));

            if (text.length > 0) {
                let clientKeys = /(\b)(cliente|persona|clientes|personas|usuario|usuarios)(\b)/;
                let itemKeys = /(\b)(item|items|producto|productos|elemento|elementos|servicio|servicios)(\b)/;
                let categoryKeys = /(\b)(categoria|categorias|grupo|grupos)(\b)/;
                let billsKeys = /(\b)(factura|facturas|cuenta|cuentas|nota|notas|recibo|recibos)(\b)/;
                let qrCodeKeys = /(\b)(qr|codigo qr)(\b)/;
                let eanCodeKeys = /(\b)(ean|codigo ean|codigo de barras|codigo barras)(\b)/;

                if (clientKeys.test(text)) matches.push('clientes');
                if (categoryKeys.test(text)) matches.push('categorias');
                if (billsKeys.test(text)) matches.push('cuentas');
                if (itemKeys.test(text)) matches.push('items');
                if (qrCodeKeys.test(text)) matches.push('qr');
                if (eanCodeKeys.test(text)) matches.push('ean');
                //if (deleteKeys.test(text)) return 'delete';          
            }

            return { matches, number: matches.length };
        }

        // FUNCTION Send to Chat Clients Founded in the text.
        createMatchCustomers(text) {
            let somethingFound = false;
            let operation = this.searchOperation(text);
            let custFound = this.searchClientExistInText(text);
            if (custFound.number > 0) {
                somethingFound = true;
                if (custFound.number == 1) {
                    this.sendMsgToChat("Encontré este cliente según tus criterios. Da click en el siguiente botón para confirmar la acción.", "Server");
                } else {
                    this.sendMsgToChat("Encontré los siguientes clientes según tus criteros. Selecciona el que deseas para confirmar la acción.", "Server");
                }
                let optionsCust = [];
                let functionsCust = [];
                for (let i = 0; i < custFound.number; i++) {
                    optionsCust.push("<div style='line-height:18px; margin: 2px 0px;'><div stye='text-transform: uppercase; font-size: 13px; margin-bottom: 5px;'><b>" + custFound.matches[i].cust_name + "</b></div> <div> Coincide por: " + custFound.flags[i] + "</div> <div><b>(" + custFound.matches[i].cust_id + ")</b></div></div>");
                    if (operation == 'edit') {
                        functionsCust.push(() => { this.editClient(this, custFound.matches[i].cust_key) });
                    } else if (operation == 'delete') {
                        functionsCust.push(() => { this.deleteClient(this, custFound.matches[i].cust_key) });
                    } else if (operation == 'add') {
                        functionsCust.push(() => { this.addClient(this, custFound.matches[i].cust_key) });
                    } else {
                        // Else is search by default
                        functionsCust.push(() => { this.searchClient(this, custFound.matches[i].cust_key) });
                    }
                }
                this.sendOptions(optionsCust, functionsCust);
            }
            return somethingFound;
        }

        // FUNCTION Buscar Cliente
        searchClient(el, client_key) {
            console.log("Open Search" + client_key);
        }

        // FUNCTION Editar Cliente
        editClient(el, client_key) {
            el.sendMsgToChat("Ok. Puedes editar al cliente en la ventana emergente.", "Server");
        }

        // FUNCTION Eliminar Cliente
        deleteClient(el, client_key) {
            el.sendMsgToChat("Puedes eliminar al cliente en la ventana de edición de clientes.", "Server");
            el.sendOptions(['<div>Editar Cliente</div>'], [() => { alert('abrir ventatna'); }]);
        }

        // FUNCTION Agregar Cliente a la cuenta
        addClient(el, client_key) {
            el.sendMsgToChat("Agregar cliente a la cuenta.", "Server");
        }

        // FUNCTION Send to Chat Items Founded in the text.
        createMatchItems(text) {
            let somethingFound = false;
            let operation = this.searchOperation(text);
            let custFound = this.searchItemExistText(text);
            if (custFound.number > 0) {
                somethingFound = true;
                if (custFound.number == 1) {
                    this.sendMsgToChat("Encontré este ítem según tus criterios. Da click en el siguiente botón para confirmar la acción.", "Server");
                } else {
                    this.sendMsgToChat("Encontré los siguientes ítems según tus criteros. Selecciona el que deseas para confirmar la acción.", "Server");
                }
                let optionsCust = [];
                let functionsCust = [];
                for (let i = 0; i < custFound.number; i++) {
                    optionsCust.push("<div style='display:flex; justify-content:start; text-align: left;'><div style='margin: auto 13px auto 0px;'><img style='width:30px;' src='" + custFound.matches[i].prod_icon + "'></div> <div style='line-height:18px;'><div style='margin-bottom: 3px;font-size: 13px;max-width: 350px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;'><b>" + custFound.matches[i].prod_name + "</b></div> <div>Coincide por: " + custFound.flags[i] + "</div> <div><b>(" + custFound.matches[i].prod_id + ")</b></div></div></div>");
                    if (operation == 'edit') {
                        functionsCust.push(() => { this.editItem(this, custFound.matches[i].prod_id) });
                    } else if (operation == 'remove') {
                        functionsCust.push(() => { this.removeItem(this, custFound.matches[i].prod_id) });
                    } else if (operation == 'add') {
                        functionsCust.push(() => { this.addItem(this, custFound.matches[i].prod_id) });
                    } else if (operation == 'delete') {
                        functionsCust.push(() => { this.deleteItem(this, custFound.matches[i].prod_id) });

                    } else {
                        // Else is search by default
                        functionsCust.push(() => { this.searchItem(this, custFound.matches[i].prod_id) });
                    }
                }
                this.sendOptions(optionsCust, functionsCust);
            }
            return somethingFound;
        }

        // FUNCTION Buscar Item
        searchItem(el, item_key) {
            config_item(item_key);
        }

        // FUNCTION Editar Item
        editItem(el, item_key) {
            el.sendMsgToChat("Ok. Puedes editar el ítem en la ventana emergente.", "Server");
            config_item(item_key);
        }

        // FUNCTION Eliminar Item
        deleteItem(el, item_key) {
            el.sendMsgToChat("Puedes eliminar el ítem en la ventana de edición de clientes.", "Server");
            config_item(item_key);
        }

        // FUNCTION Agregar Item a la cuenta
        addItem(el, item_key) {
            addItemtoBillByIdCode(item_key);
            el.sendMsgToChat("El ítem fue agregado a la cuenta.", "Server");
        }

        // FUNCTION Remover Item de la cuenta
        removeItem(el, item_key) {
            remove_item_bill(item_key);
            el.sendMsgToChat("El ítem fue removido a la cuenta.", "Server");
        }

        // SALES-DEF: Define important dates
        defineDates() {
            this.now = new Date();
            this.today = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate());
            this.yesterday = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate() - 1);
            this.lastWeek = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate() - 7);
            this.twoWeeksAgo = new Date(this.lastWeek.getFullYear(), this.lastWeek.getMonth(), this.lastWeek.getDate() - 7);
            this.threeWeeksAgo = new Date(this.twoWeeksAgo.getFullYear(), this.twoWeeksAgo.getMonth(), this.twoWeeksAgo.getDate() - 7);
            this.fourWeeksAgo = new Date(this.threeWeeksAgo.getFullYear(), this.threeWeeksAgo.getMonth(), this.threeWeeksAgo.getDate() - 7);
            this.lastMonth = new Date(this.today.getFullYear(), this.today.getMonth() - 1, this.today.getDate());
            this.twoMonthsAgo = new Date(this.today.getFullYear(), this.today.getMonth() - 2, this.today.getDate());
            this.sixMonthsAgo = new Date(this.today.getFullYear(), this.today.getMonth() - 6, this.today.getDate());
            this.lastYear = new Date(this.today.getFullYear() - 1, this.today.getMonth(), this.today.getDate());
            this.twoYearsAgo = new Date(this.today.getFullYear() - 2, this.today.getMonth(), this.today.getDate());
            this.allData = new Date(this.today.getFullYear() - 50, this.today.getMonth(), this.today.getDate());

            this.last1day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1);
            this.last2day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 2);
            this.last3day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 3);
            this.last4day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 4);
            this.last5day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 5);
            this.last6day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 6);
            this.last7day = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7);
            let lastDays = [this.last1day, this.last2day, this.last3day, this.last4day, this.last5day, this.last6day, this.last7day];
            let me = this;
            lastDays.forEach((el) => {
                if (el.getDay() == 0) me.lastDom = el;
                if (el.getDay() == 1) me.lastLun = el;
                if (el.getDay() == 2) me.lastMartes = el;
                if (el.getDay() == 3) me.lastMie = el;
                if (el.getDay() == 4) me.lastJue = el;
                if (el.getDay() == 5) me.lastVie = el;
                if (el.getDay() == 6) me.lastSab = el;
            });

            this.last1Month = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate());
            this.last2Month = new Date(new Date().getFullYear(), new Date().getMonth() - 2, new Date().getDate());
            this.last3Month = new Date(new Date().getFullYear(), new Date().getMonth() - 3, new Date().getDate());
            this.last4Month = new Date(new Date().getFullYear(), new Date().getMonth() - 4, new Date().getDate());
            this.last5Month = new Date(new Date().getFullYear(), new Date().getMonth() - 5, new Date().getDate());
            this.last6Month = new Date(new Date().getFullYear(), new Date().getMonth() - 6, new Date().getDate());
            this.last7Month = new Date(new Date().getFullYear(), new Date().getMonth() - 7, new Date().getDate());
            this.last8Month = new Date(new Date().getFullYear(), new Date().getMonth() - 8, new Date().getDate());
            this.last9Month = new Date(new Date().getFullYear(), new Date().getMonth() - 9, new Date().getDate());
            this.last10Month = new Date(new Date().getFullYear(), new Date().getMonth() - 10, new Date().getDate());
            this.last11Month = new Date(new Date().getFullYear(), new Date().getMonth() - 11, new Date().getDate());
            this.last12Month = new Date(new Date().getFullYear(), new Date().getMonth() - 12, new Date().getDate());
            let lastMonths = [this.last1Month, this.last2Month, this.last3Month, this.last4Month, this.last5Month, this.last6Month, this.last7Month, this.last8Month, this.last9Month, this.last10Month, this.last11Month, this.last12Month];
            lastMonths.forEach((el) => {
                if (el.getMonth() == 0) me.lastEne = el;
                if (el.getMonth() == 1) me.lastFeb = el;
                if (el.getMonth() == 2) me.lastMarzo = el;
                if (el.getMonth() == 3) me.lastAbr = el;
                if (el.getMonth() == 4) me.lastMay = el;
                if (el.getMonth() == 5) me.lastJun = el;
                if (el.getMonth() == 6) me.lastJul = el;
                if (el.getMonth() == 7) me.lastAgo = el;
                if (el.getMonth() == 8) me.lastSep = el;
                if (el.getMonth() == 9) me.lastOct = el;
                if (el.getMonth() == 10) me.lastNov = el;
                if (el.getMonth() == 11) me.lastDec = el;
            });
        }

        // SALES-SECONDARY OPERATIONS: Convert date to spanish readable format
        readableEspDate(dateS, options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) {
            // Example: readableEspDate(Date()) -> "sábado, 11 de enero de 2020"
            return dateS.toLocaleString("es-ES", options)
        }

        // SALES-OPERATIONS: Sort data by date
        sortDataByDate(data) {
            return data.sort(function (a, b) {
                var date1 = new Date(Date.parse(a["trx_date"].replace(" ", "T")));
                var date2 = new Date(Date.parse(b["trx_date"].replace(" ", "T")));
                return date1 - date2;
            });
        }

        // SALES-OPERATIONS: Total sales (count and value) {}
        countTotalSales() {
            let sales_num = (typeof this.tx_data == 'undefined') ? null : this.tx_data.length;
            return { sales_num }
        }

        // SALES-OPERATIONS: groups the data by bills
        groupByBills(xs) {
            return xs.reduce(function (rv, x) { let bill = x['id_bill']; (rv[bill] = rv[bill] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATIONS: groups the data by month
        groupByMonth(xs) {
            return xs.reduce(function (rv, x) { let month = x['trx_date'].substr(0, 7); (rv[month] = rv[month] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATIONS: groups the data by days
        groupByDay(xs) {
            return xs.reduce(function (rv, x) { let day = x['trx_date'].substr(0, 10); (rv[day] = rv[day] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATIONS: groups the data by profile name
        groupByProfile(xs) {
            return xs.reduce(function (rv, x) { let bill = x['prof_name']; (rv[bill] = rv[bill] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATION: groups the data by hour
        groupByHour(xs) {
            return xs.reduce(function (rv, x) { let hour = x['trx_date'].substr(0, 13); (rv[hour] = rv[hour] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATION: group by payment type
        groupByPaymentType(xs) {
            return xs.reduce(function (rv, x) { let payment = x['payment_type']; (rv[payment] = rv[payment] || []).push(x); return rv; }, {});
        };

        // SALES-OPERATION: Total valor de transacciones por datos
        SumTotalValue(data, rnd) {
            rnd = (rnd === undefined) ? 2 : rnd;
            let suma = 0;
            let bills = this.groupByBills(data);
            for (let bill in bills) { suma += parseFloat(bills[bill][0]["trx_value"]) };
            return numRound(suma, rnd);
        }

        dataReport(data) {
            // La data que entra debe tener el formato de la data original tx_data.
            let byBills = this.groupByBills(data);
            let byProfiles = this.groupByProfile(data);
            let byPayType = this.groupByPaymentType(data);
            let byDates = this.groupByDay(data);

            // Number of bills
            let numSales = Object.keys(byBills).length;
            // Number of items
            let numItems = data.map((c) => parseInt(c.num_items)).sumar()
            // Total value
            let totalValue = this.SumTotalValue(data);
            // Last Event
            let lastEvent = data.map((c) => c.trx_date).last();
            let timeSinceLastEvent = moment(lastEvent, "YYYY-MM-DD hh:mm:ss").fromNow();

            return { numSales, numItems, totalValue, lastEvent, timeSinceLastEvent }
        }

        generalReport() {
            let totalSales = this.countTotalSales();
            let answer;
            if (totalSales.sales_num == 0 || totalSales.sales_num == null) {
                answer = 'Parece que no has vendido nada todavía. Conoce cómo hacer tus primeras ventas siguiendo esta guía: <a href="https://medium.com/blog-2luca/primeros-pasos-521605828cf4" target="_blank">Primeros Pasos</a>';
            } else {
                let dataGR = this.dataReport(this.tx_data);
                answer = 'En total has hecho ' + dataGR.numSales.formaty() + ' ventas, en las cuales has vendido ' + dataGR.numItems.formaty() + ' ítems por un monto total de $' + dataGR.totalValue.formaty() + '. Además, tu última venta fue hace ' + dataGR.timeSinceLastEvent + '.';
            }
            this.sendMsgToChat(answer, "Server");
        }

        salesInterpreter(text) {
            let answer = '';
            let textAnalysis = this.fullTextAnalysis(text);

            if (textAnalysis.itemsMatch.number == 0 && textAnalysis.customerMatch.number == 0 && textAnalysis.datesMatch.number == 0) {
                // ==== GENERAL REPORT ====
                this.generalReport();
            } else if (textAnalysis.itemsMatch.number == 0 && textAnalysis.customerMatch.number == 0 && textAnalysis.datesMatch.number > 0) {
                // ===== GENERAL REPORT BY DATE ===== 

                if (textAnalysis.datesMatch.finalDates.punctual.length == 1 && textAnalysis.datesMatch.finalDates.ranges.length == 0) {

                    // Si solo hay una fecha puntual ingresada y no hay rangos de fechas muestre esa fecha.
                    let dateS = textAnalysis.datesMatch.finalDates.punctual[0];
                    var salesQuery = this.getSalesDate(dateS);
                    if (salesQuery.length == 0) {
                        answer = 'El día ' + this.readableEspDate(dateS) + ' no tuviste ventas.';
                    } else {
                        var val_sales = salesQuery.map((a) => parseInt(a.trx_value)).sumar();
                        answer = 'El día ' + this.readableEspDate(dateS) + ' realizaste un total de ' + salesQuery.length + ' ventas por un monto de $' + val_sales.formaty() + '.';
                    }

                } else if (textAnalysis.datesMatch.finalDates.punctual.length > 1 && textAnalysis.datesMatch.finalDates.ranges.length == 0) {

                    // Si hay mas de una fecha puntual ingresada y no hay rangos de fechas muestre todas.                                    
                    this.sendMsgToChat("Ok. Esto fue lo que encontré:", "Server");
                    textAnalysis.datesMatch.finalDates.punctual.forEach((dateS) => {
                        var salesQuery = this.getSalesDate(dateS);
                        if (salesQuery.length == 0) {
                            answer += '• El día ' + this.readableEspDate(dateS) + ' no tuviste ventas.<br><br>';
                        } else {
                            var val_sales = salesQuery.map((a) => parseInt(a.trx_value)).sumar();
                            answer += '• El día ' + this.readableEspDate(dateS) + ' realizaste un total de ' + salesQuery.length + ' ventas por un monto de $' + val_sales.formaty() + '.<br><br>';
                        }
                    });
                    answer = answer.substr(0, answer.length - 4)
                }

                this.sendMsgToChat(answer, "Server");

            } else if (textAnalysis.itemsMatch.number == 0 && textAnalysis.customerMatch.number > 0 && textAnalysis.datesMatch.number == 0) {
                // General Report by customer
            } else if (textAnalysis.itemsMatch.number > 0 && textAnalysis.customerMatch.number == 0 && textAnalysis.datesMatch.number == 0) {
                // General Report by item
            } else if (textAnalysis.itemsMatch.number > 0 && textAnalysis.customerMatch.number == 0 && textAnalysis.datesMatch.number > 0) {
                // General Report by item and date
            } else if (textAnalysis.itemsMatch.number == 0 && textAnalysis.customerMatch.number > 0 && textAnalysis.datesMatch.number > 0) {
                // General Report by customer and date
            } else if (textAnalysis.itemsMatch.number > 0 && textAnalysis.customerMatch.number > 0 && textAnalysis.datesMatch.number == 0) {
                // General Report by customer and item
            } else if (textAnalysis.itemsMatch.number > 0 && textAnalysis.customerMatch.number > 0 && textAnalysis.datesMatch.number > 0) {
                // General Report by item, customer and date
            } else {
                // Default: General report 
                this.sendMsgToChat('Ok. Esto fue lo que pude encontrar.', "Server");
                this.generalReport();
            }

            return answer;
        }

        // SALES-OPERATIONS: Sales of an specific date
        getSalesDate(date_search) {
            if (typeof this.tx_data != 'undefined') {
                return this.tx_data.filter((a) => { var d = a.trx_date.split(" ")[0].split("-"); var date_data = new Date(d[0], d[1] - 1, d[2]); return date_data.getTime() == date_search.getTime(); });
            } else {
                return [];
            }
        }

        // SALES-OPERATIONS: Sales of a range of dates
        getSalesRangeDate(dateFrom, dateTo) {
            if (typeof this.tx_data != 'undefined') {
                return this.tx_data.filter((a) => { var d = a.trx_date.split(" ")[0].split("-"); var date_data = new Date(d[0], d[1] - 1, d[2]); return (date_data.getTime() >= dateFrom.getTime()) && (date_data.getTime() <= dateTo.getTime()) });
            } else {
                return null;
            }
        }

        // SALES-OPERATIONS: Sales by customer and timeframe
        getSalesCustomer(dateFrom, dateTo, customerId) {
            if (typeof this.tx_data != 'undefined') {
                return 0;
            } else {
                return null;
            }
        }

        // SALES-OPERATIONS: Sales by item and timeframe
        getSalesItem(dateFrom, dateTo, itemId) {
            if (typeof this.tx_data != 'undefined') {
                return 0;
            } else {
                return null;
            }
        }

        // SALES-OPERATIONS: Top n most saled sales
        getSalesTopItems(n) {
            if (typeof this.tx_data != 'undefined') {
                return 0;
            } else {
                return null;
            }
        }

        // SALES-OPERATIONS: Top n customer with most sales
        getSalesTopCustomers(n) {
            if (typeof this.tx_data != 'undefined') {
                return 0;
            } else {
                return null;
            }
        }

    }

    // Create chat Object
    CheckPkgAccess(2).then(function (cpa) {
        checkFreeTrail(2).then(function (cft) {

            if (cpa || cft.isactive) {
                if (cft.isactive && cft.days_to_end <= 3 && !cpa) {
                    if (cft.days_to_end >= 2) {
                        alertify.message("Tu período de prueba caduca en " + cft.days_to_end + " días.");
                    } else if (cft.days_to_end == 1) {
                        alertify.message("Tu período de prueba caduca mañana.");
                    } else if (cft.days_to_end == 0) {
                        alertify.message("Tu período de prueba caduca hoy.");
                    }
                }

                const chat = new chatBotLuca();

                // Event Click to Chat
                $(".chatBotContainer>div:first-child>div:first-child").click(function () {
                    chat.openChat();
                });

                // Event Click Close Chat
                $(".chatBotContainer>div:first-child>div:last-child").click(function () {
                    chat.closeChat();
                });

                // Event Click Outside of chat to close it.
                $(document).mouseup(function (e) {
                    let container = $(".chatBotContainer");
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        chat.closeChat();
                    }
                });

                // Event Click on Button to Send Message
                $(".chatBotContainer>div:nth-child(2)>div:nth-child(2)>div").on("click", function () {
                    let val_input = $(".chatBotContainer>div:nth-child(2)>div:nth-child(2)>input").val().trim();
                    chat.sendMsgToChat(val_input, "User");
                    chat.call_api(val_input);
                });

                // Event Enter to Send Message
                $('.chatBotContainer>div:nth-child(2)>div:nth-child(2)>input').keyup(function (e) {
                    if (e.keyCode == 13) {
                        let val_input = $(".chatBotContainer>div:nth-child(2)>div:nth-child(2)>input").val().trim();
                        chat.sendMsgToChat(val_input, "User");
                        chat.call_api(val_input);
                    }
                });

                // Clean Chat
                $(".chatBotContainer>div:first-child>div:last-child>div:first-child").on("click", function () {
                    chat.cleanChat();
                });

            }
        });
    });

});


// function search_wikipedia(topic) {
//     //https://es.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=friends
//     var xhttp = new XMLHttpRequest();
//     var url = "https://es.wikipedia.org/w/api.php?action=opensearch&limit=1&format=json&origin=*&search=" + topic;
//     xhttp.onload = function () {
//         var data = JSON.parse(this.response);
//         if (xhttp.readyState == 4 && xhttp.status == 200) {
//             return data[3][0];
//         } else {
//             return "";
//         }
//     };
//     xhttp.open("GET", url, true);
//     xhttp.withCredentials = false;
//     xhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
//     xhttp.send();
// }


// let itemsCardCust = ['<div><span class="mdi mdi-cake-variant"></span>11 febrero 1990</div>',
//     '<div><span class="mdi mdi-cart-outline"></span>15 compras</div>',
//     '<div><span class="mdi mdi-timer"></span>Última compra hace 3 días</div>',
//     '<div class="link"><div onclick="alert(0)">Ver más</div><div  onclick="alert(1)">Ver análitica</div></div>'];
// this.sendCardToChat("María Fernanda Restrepo", itemsCardCust, "../avatars/girlreddresspony.svg");

// let itemsCardProd = ['<div><span class="mdi mdi-currency-usd"></span>$ 3,000</div>',
//     '<div><span class="mdi mdi-cart-outline"></span>15 ventas</div>',
//     '<div><span class="mdi mdi-timer"></span>Última venta hace 3 días</div>',
//     '<div class="link"><div onclick="alert(0)">Ver más</div><div  onclick="alert(1)">Ver análitica</div></div>'];
// this.sendCardToChat("Ensalada de Brocolli", itemsCardProd, "../icons/SVG/74-food/broccoli.svg");

//this.sendOptions(["<div>Sí</div>","<div>No</div>"], [()=>{this.ChangeIt(this);}, null]);