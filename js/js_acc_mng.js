/**
 * pointOfSaleObj: Create modals of point of sale. 
 * 
 * It will create:
 *  - Modal to create POS
 *  - Modal to mage POS relations with profiles
 *  - Modal to admin POS
 */
class pointOfSaleObj {

    constructor(create_over = false, manager_over = false, admin_over = false) {
        this.profsessid_list = [];
        this.pos_list = [];
        this.pos_list_open = false;

        // Modal Create POS
        this.create_over = create_over;
        this.id_create_cont = "msg-box-container-poscreate";
        this.build_modal_create();
        this.assign_fun_modal_create();
        this.mng_create = new modalsManager(this.id_create_cont);

        // Modal Manager POS
        this.manager_over = manager_over;
        this.id_manager_cont = "msg-box-container-posmanager";
        this.build_modal_manager();
        this.assign_fun_modal_manager()
        this.mng_manager = new modalsManager(this.id_manager_cont);

        // Modal Admin Pos
        this.admin_over = admin_over;
        this.id_admin_cont = "msg-box-container-posadmin";
        this.build_modal_adminpos();
        this.assign_fun_modal_adminpos()
        this.mng_admin = new modalsManager(this.id_admin_cont);
    }

    build_modal_create() {
        $('#' + this.id_create_cont).remove();
        let txtHTML = '';
        txtHTML += '<div class="msg-box-container" id="' + this.id_create_cont + '">';
        txtHTML += '<div class="msg-box-1">';
        txtHTML += '<p class="msgb-box-1-exit-mark">X</p>';
        txtHTML += '<p class="msgb-box-1-title">CREAR PUNTOS DE VENTA</p>';
        txtHTML += '<div class="pos_act"><p class="msg-box-1-description">Crea un nuevo punto de venta o asigna un punto de venta existente a un perfil. Si solo tienes un punto de venta en tu negocio, no es necesario que lo registres para poder empezar a vender.</p>';
        txtHTML += '<div class="sel_ext update_link mt10">Seleccionar de existente</div>'
        txtHTML += '<h4 class="hdd_2">Selecciona una opción:</h4>';
        txtHTML += '<select name="pos_list_pos" id="pos_list_pos" class="select_list_1"></select>';
        txtHTML += '<h4 class="hdd_1">Nombre:</h4>';
        txtHTML += '<input class="hdd_1 msg-box-1-input" type="text" name="pos_name" id="pos_name" placeholder="Nombre del punto de venta"></input>';
        txtHTML += '<h4>Asignar al perfil:</h4>';
        txtHTML += '<select name="pos_profile" id="pos_profile" class="select_list_1"></select>';
        txtHTML += '<span class="errorMsg" id="errorMsgPoscreate"></span>';
        txtHTML += '<button class="btn_green_full" id="btnPoscreate">CREAR</button>   ';
        txtHTML += '</div></div>';
        $('body').append(txtHTML);
    }

    build_modal_manager() {
        $('#' + this.id_manager_cont).remove();
        let txtHTML = '';
        txtHTML += '<div class="msg-box-container" id="' + this.id_manager_cont + '">';
        txtHTML += '<div class="msg-box-1">';
        txtHTML += '<p class="msgb-box-1-exit-mark">X</p>';
        txtHTML += '<p class="msgb-box-1-title">ASIGNACIONES DE PUNTOS DE VENTA</p>';
        txtHTML += '<div class="pos_act"><p class="msg-box-1-description">Administra las asignaciones de tus puntos de ventas a tus perfiles de vendedores.</p>';
        txtHTML += '<div class="admin_res_cont"><div class="admin_res_wrap"></div></div></div>';
        txtHTML += '<div class="no_data"><p>No tienes ningúna asignación creada entre tus puntos de venta y tus perfiles.</p></div>';
        txtHTML += '</div></div>';
        $('body').append(txtHTML);
    }

    build_modal_adminpos() {
        $('#' + this.id_admin_cont).remove();
        let txtHTML = '';
        txtHTML += '<div class="msg-box-container" id="' + this.id_admin_cont + '">';
        txtHTML += '<div class="msg-box-1">';
        txtHTML += '<p class="msgb-box-1-exit-mark">X</p>';
        txtHTML += '<p class="msgb-box-1-title">PUNTOS DE VENTA</p>';
        txtHTML += '<div class="pos_act"><p class="msg-box-1-description">Administra tus puntos de ventas.</p>';
        txtHTML += '<div class="admin_res_cont"><div class="admin_res_wrap"></div></div></div>';
        txtHTML += '<div class="no_data"><p>No tienes ningún puntos de venta creados.</p></div>';
        txtHTML += '</div></div>';
        $('body').append(txtHTML);
    }

    open_modal_create() {
        this.mng_create.open();
    }

    open_modal_manager() {
        this.mng_manager.open();
    }

    open_modal_admin() {
        this.mng_admin.open();
    }

    close_modal_create() {
        (this.create_over) ? this.mng_create.close_over() : this.mng_create.close();
    }

    close_modal_manager() {
        (this.manager_over) ? this.mng_manager.close_over() : this.mng_manager.close();
    }

    close_modal_admin() {
        (this.admin_over) ? this.mng_admin.close_over() : this.mng_admin.close();
    }

    assign_fun_modal_manager() {
        let me = this;

        // Close Icon
        $('#' + this.id_manager_cont + ' .msgb-box-1-exit-mark').off('click').click(() => { me.close_modal_manager(); });

        // Load POS to Div
        $.ajax({
            url: "get_all_pos_profile.php",
            dataType: "json",
            data: ({ x: '1' }),
            type: "POST",
            success: function (r) {
                if (r.length > 0 && r != undefined) {
                    $('#' + me.id_manager_cont + ' .admin_res_wrap').html("");
                    $('#' + me.id_manager_cont + ' .no_data').hide();
                    $('#' + me.id_manager_cont + ' .pos_act').show();

                    let listData = [];
                    let html = '';
                    for (let i = 0; i < r.length; i++) {
                        var d = r[i]["pos_creation_date"].substring(0, 10);
                        html += '<div class="res_elem_cont">';
                        html += '<div class="res_switch"><img alt="img1" src="' + r[i]["avatar"] + '"></div>'
                        html += '<div class="res_item">';
                        html += '<p>' + r[i]["pos_name"] + '</p>';
                        html += '<p>' + d + '</p>';
                        html += '<p> Asociado a: ' + r[i]["prof_name"] + '</p>';
                        html += '</div><div class="res_tools">';
                        html += '<span class="del_' + i + ' mdi mdi-delete-outline"></span>';
                        html += '<span class="edi_' + i + ' mdi mdi-file-document-edit-outline"></span>';
                        html += '</div></div>';
                        listData.push({ "profsessid": r[i]["profsessid"], "id_pos": r[i]["id_pos"], "pos_name": r[i]["pos_name"] });
                    }
                    $('#' + me.id_manager_cont + ' .admin_res_wrap').append(html);

                    // Assign events
                    for (let i = 0; i < r.length; i++) {

                        $('#' + me.id_manager_cont + ' .del_' + i).off('click').click(() => {
                            let profsessid = listData[i]["profsessid"];
                            let id_pos = listData[i]["id_pos"];
                            $.ajax({
                                url: "delete_pos_profile.php",
                                dataType: "json",
                                data: ({ profsessid, id_pos }),
                                type: "POST",
                                success: function (r) {
                                    if (r[0][0] == 'S') {
                                        alertify.message("Perfil desvinculado del punto de venta.");
                                        me.assign_fun_modal_manager();
                                    } else {
                                        alertify.error("Error desvinculando perfil.")
                                    }
                                }
                            });
                        });

                        $('#' + me.id_manager_cont + ' .edi_' + i).off('click').click(() => {
                            let profsessid = listData[i]["profsessid"];
                            let id_pos = listData[i]["id_pos"];
                            let pos_name = listData[i]["pos_name"];

                            if (me.pos_list_open == false) {
                                $("#pos_list_pos").show();
                                $('#' + me.id_create_cont + ' .hdd_2').show();
                                $('#' + me.id_create_cont + ' .hdd_1').hide();
                                me.pos_list_open = !me.pos_list_open;
                            }
                            $('#pos_list_pos').val(id_pos);
                            $('#pos_profile').val(profsessid);
                            $("#pos_name").val(pos_name);

                            me.close_modal_manager();
                            me.open_modal_create();
                        });
                    }

                } else {
                    $('#' + me.id_manager_cont + ' .pos_act').hide();
                    $('#' + me.id_manager_cont + ' .no_data').show();
                }

            }
        });

    }

    assign_fun_modal_create() {
        let me = this;

        // Close Icon
        $('#' + this.id_create_cont + ' .msgb-box-1-exit-mark').off('click').click(() => { me.close_modal_create(); });

        // Click to toggle list of POS
        this.pos_list_open = false;
        $("#pos_list_pos").hide();
        $(".hdd_2").hide();
        $('#' + this.id_create_cont + ' .sel_ext').off('click').click(() => {
            if (me.pos_list_open == true) {
                $("#pos_list_pos").hide();
                $('#' + me.id_create_cont + ' .hdd_2').hide();
                $('#' + me.id_create_cont + ' .hdd_1').show();
            } else {
                $("#pos_list_pos").show();
                $('#' + me.id_create_cont + ' .hdd_2').show();
                $('#' + me.id_create_cont + ' .hdd_1').hide();
            }
            me.pos_list_open = !me.pos_list_open;
        });

        // Load Salesmen to list
        $.ajax({
            url: "pull_salesmen.php",
            dataType: "json",
            data: ({ x: '1' }),
            type: "POST",
            success: function (r) {
                $("#pos_profile").html('');
                let txtHTML = '<option value="0" selected>Selecciona un perfil</option>';
                for (var i = 0; i < r.length; i++) {
                    txtHTML += '<option value="' + r[i]['profsessid'].trim() + '">' + r[i]['prof_name'].trim() + '</option>';
                    me.profsessid_list.push(r[i]['profsessid'].trim());
                }
                $("#pos_profile").html(txtHTML);
            }
        });

        // Load POS List        
        $.ajax({
            url: "get_all_pos.php",
            dataType: "json",
            data: ({ x: '1' }),
            type: "POST",
            success: function (r) {
                $("#pos_list_pos").html('');
                let txtHTML = '<option value="0" selected>Selecciona una opción</option>';
                for (var i = 0; i < r.length; i++) {
                    txtHTML += '<option value="' + r[i]['id_pos'].trim() + '">' + r[i]['pos_name'].trim() + '</option>';
                    me.pos_list.push(r[i]['id_pos'].trim());
                }
                $("#pos_list_pos").html(txtHTML);
            }
        });

        // Event on change selection of POS name
        $("#pos_list_pos").off('change').change((el) => {
            let sel_ls = $("#pos_list_pos option:selected").html();
            let sel_ls_val = $("#pos_list_pos option:selected").val();
            sel_ls = (sel_ls_val == '0' || sel_ls_val == '') ? '' : sel_ls;
            $("#pos_name").val(sel_ls);
        })

        // Submit new point of sale
        $("#btnPoscreate").off('click').click(() => {

            let pos_name = $("#pos_name").val().trim();
            let pos_profile = $("#pos_profile").val().trim();
            let pos_id = generateRandomString(5);

            if (pos_name.length == 0) {
                ErrorMsgModalBox('open', 'errorMsgPoscreate', 'Debes ingresar el nombre o escoger una opción de la lista.');
                ErrorInputBox('pos_name');
                ErrorInputBox('pos_list_pos');
            } else if (pos_name.length >= 50) {
                ErrorMsgModalBox('open', 'errorMsgPoscreate', 'El nombre debe tener menos de 50 caracteres.');
                ErrorInputBox('pos_name');
            } else if (pos_profile == "0" || pos_profile.length <= 5) {
                ErrorMsgModalBox('open', 'errorMsgPoscreate', 'Debes asociar el punto de venta a un perfil de vendedor.');
                ErrorInputBox('pos_profile');
            } else {
                let mr = new MainRoller();
                mr.startMainRoller();

                $.ajax({
                    url: "push_new_pos.php",
                    dataType: 'json',
                    data: ({ pos_name, pos_profile, pos_id }),
                    type: "POST",
                    success: function (r) {
                        mr.stopMainRoller();
                        if (r[0][0] == 'S') {
                            me.close_modal_create();
                            alertify.alert("", r[0][1]);
                            // Recreate modal                            
                            me.assign_fun_modal_create();
                            // Update the content of manager
                            me.assign_fun_modal_manager();
                        } else {
                            ErrorMsgModalBox('open', 'errorMsgPoscreate', r[0][1]);
                        }
                    }
                });

            }

        });
    }

    assign_fun_modal_adminpos() {
        let me = this;

        // Close Icon
        $('#' + this.id_admin_cont + ' .msgb-box-1-exit-mark').off('click').click(() => { me.close_modal_admin(); });

        // Load POS List        
        $.ajax({
            url: "get_all_pos.php",
            dataType: "json",
            data: ({ x: '1' }),
            type: "POST",
            success: function (r) {

                if (r.length > 0 && r != undefined) {
                    $('#' + me.id_admin_cont + ' .admin_res_wrap').html("");
                    $('#' + me.id_admin_cont + ' .no_data').hide();
                    $('#' + me.id_admin_cont + ' .pos_act').show();

                    let listData = [];
                    let html = '';
                    for (let i = 0; i < r.length; i++) {
                        var d = r[i]["pos_creation_date"].substring(0, 10);
                        html += '<div class="res_elem_cont">';
                        //html += '<div class="res_switch"><img alt="img1" src="' + r[i]["avatar"] + '"></div>'
                        html += '<div class="res_item">';
                        html += '<p>' + r[i]['pos_name'].trim() + '</p>';
                        html += '<p>' + d + '</p>';
                        html += '<p> ID: ' + r[i]['id_pos'].trim() + '</p>';
                        html += '</div><div class="res_tools">';
                        html += '<span class="del_' + i + ' mdi mdi-delete-outline"></span>';
                        html += '<span class="edi_' + i + ' mdi mdi-file-document-edit-outline"></span>';
                        html += '</div></div>';
                        listData.push({ "pos_name": r[i]["pos_name"], "id_pos": r[i]["id_pos"] });
                    }
                    $('#' + me.id_admin_cont + ' .admin_res_wrap').append(html);

                } else {
                    $('#' + me.id_admin_cont + ' .pos_act').hide();
                    $('#' + me.id_admin_cont + ' .no_data').show();
                }

            }
        });
    }
}

$(document).ready(function () {

    // Puntos de venta Object
    let pos_obj = new pointOfSaleObj();
    $("#new_pos").off('click').click(() => { pos_obj.open_modal_create() });
    $("#admin_pos").off('click').click(() => { pos_obj.open_modal_admin() });
    $("#edit_pos").off('click').click(() => { pos_obj.open_modal_manager() });


    // Default Open Lateral Menu
    //OpenMenu();
    // Check Cookies Support
    checkCookiesSupport();
    // Inicializate Spinner and Rollers
    $("#MainRoller_background").hide();
    // Cargar Paises
    loadCountries("newCountry");
    // Reset All Variables;
    ResetVariables();
    // Cargar resoluciones
    GetResolutions();
    // Cargar Lista de Impresoras 
    loadPrinters("printers_list");
    // Cargar impresora actual
    setTimeout(function () {
        getCurrentPrinter("printers_list");
    }, 2000);


    // Printer Event Update 
    $("#printers_list").change(function () {
        updatePrinter($("#printers_list").val());
    });

    // Help Report
    $("#help_report").click(function () {
        html_txt = '';
        html_txt = '<div class="cont_help_report">';
        html_txt += '<h1><span class="mdi mdi-file-document-outline"></span> Reportes</h1>';
        html_txt += '<p>Con el sistema de reportes de luca podrás recibir en tu correo electónico un reporte detallado de las principales estadísticas de tu negocio con la frecuencia que selecciones.</p>';
        html_txt += '<div class="subtitle_1 border_btn">Estadísticas</div>';
        html_txt += '<ul><li>Total de ventas</li><li>Total de ingresos</li><li>Ítem más vendido</li><li>Vendedor estrella</li><li>Número de clientes nuevos</li></ul>';
        html_txt += '<div class="subtitle_1 border_btn">Frecuencia del reporte</div>';
        html_txt += '<ul><li><b>Reportes diarios:</b> Recibirás cada mañana un reporte sobre el comportamiento de tu negocio del día anterior.</li>';
        html_txt += '<li><b>Reportes semanales:</b> Recibirás cada lunes en la mañana un reporte sobre el comportamiento de tu negocio de la semana anterior.</li>';
        html_txt += '<li><b>Reportes mensuales:</b> Recibirás el primero de cada mes un reporte sobre el comportamiento de tu negocio del mes anterior.</li>';
        html_txt += '</ul>';
        html_txt += '</div>';
        OpenHelpModalHTML(html_txt);
    });
    // Report Frequency Options
    $('#rep_d').click(function () {
        let isOn = $(this).hasClass('active');
        if (isOn) {
            ToggleReportFreq('d', 0, this);
        } else {
            ToggleReportFreq('d', 1, this);
        }
    });
    $('#rep_w').click(function () {
        let isOn = $(this).hasClass('active');
        if (isOn) {
            ToggleReportFreq('w', 0, this);
        } else {
            ToggleReportFreq('w', 1, this);
        }
    });
    $('#rep_m').click(function () {
        let isOn = $(this).hasClass('active');
        if (isOn) {
            ToggleReportFreq('m', 0, this);
        } else {
            ToggleReportFreq('m', 1, this);
        }
    });

    $("#btnAdminResolutions").click(function () {
        $("#MainRoller_background").show();
        let x = document.getElementsByClassName("res_elem_cont");
        if (x != undefined) {
            let res_current = "";
            let cont = 0;
            let type;

            for (let i = 0; i < x.length; i++) {
                if (x[i].getElementsByClassName("res_cb")[0].checked) {
                    res_current = x[i].getElementsByClassName("res_item")[0].getElementsByTagName("p")[0].textContent;
                    cont += 1;
                }
            }
            if (res_current != "" && cont == 1) {
                type = "1";
            } else if (cont == 0) {
                type = "0";
            } else {
                ErrorMsgModalBox("open", "errorMsgAdminResolutions", "Ocurrió un error actualizando la resolución.");
                console.error("ERROR -> FILE: JS_ACC_MNG OBJ: btnAdminResolutions", 2);
                $("#MainRoller_background").hide();
                return false;
            }

            $.ajax({
                url: "set_current_resolution.php",
                dataType: "json",
                data: ({ res_current: res_current, type: type }),
                type: "POST",
                success: function (r) {
                    if (r[0][0] == "S") {
                        $("#MainRoller_background").hide();
                        CloseAdminResolutions();
                        ResetVariables();
                        GetResolutions();
                        alertify.message("Actualización correcta");
                    } else {
                        $("#MainRoller_background").hide();
                        ErrorMsgModalBox("open", "errorMsgAdminResolutions", "Ocurrió un error actualizando la resolución.");
                        console.error("ERROR -> FILE: JS_ACC_MNG OBJ: btnAdminResolutions", 1);
                    }
                }
            });
        } else {
            $("#MainRoller_background").hide();
            ErrorMsgModalBox("open", "errorMsgAdminResolutions", "Ocurrió un error actualizando la resolución.");
            console.error("ERROR -> FILE: JS_ACC_MNG OBJ: btnAdminResolutions", 3);
        }
    });

    $("#btnActUpdateFactNum").click(function () {
        $("#MainRoller_background").show();
        let date_resol2 = $("#newDateRes").val();
        let resolution2 = $("#newResNum").val();
        let range_reso_ini2 = $("#newConsFrom").val();
        let range_reso_last2 = $("#newConsTo").val();
        let curr_resol2 = "0";
        if ($("#curr_resol").prop("checked") == true) { curr_resol2 = "1" }

        let isOk1 = CheckInputText("newResNum", "errorMsgUpdateFactNum", "Numero de resolución", true, 0, 50, true, false, true);
        let isOk2 = CheckInputText("newConsFrom", "errorMsgUpdateFactNum", "Consecutivo", true, 0, 50, true, false, true);
        let isOk3 = CheckInputText("newConsTo", "errorMsgUpdateFactNum", "Consecutivo", true, 0, 50, true, false, true);
        let isOk4 = true;
        if (range_reso_ini2 > range_reso_last2) {
            ErrorMsgModalBox("open", "errorMsgUpdateFactNum", "El consecutivo inicial no puede ser mayor que el final");
            ErrorInputBox("newConsFrom");
            isOk4 = false;
        }
        if (range_reso_ini2 < 0) {
            ErrorMsgModalBox("open", "errorMsgUpdateFactNum", "El consecutivo inicial no puede ser negativo");
            ErrorInputBox("newConsFrom");
            isOk4 = false;
        }
        if (range_reso_last2 < 0) {
            ErrorMsgModalBox("open", "errorMsgUpdateFactNum", "El consecutivo final no puede ser negativo");
            ErrorInputBox("newConsTo");
            isOk4 = false;
        }

        let isOk5 = CheckInputText("newDateRes", "errorMsgUpdateFactNum", "Fecha", true, 0, 50, false, false, false);
        if (moment(date_resol2) < moment("1900-01-01")) {
            ErrorMsgModalBox("open", "errorMsgUpdateFactNum", "Revisa la fecha de la resolución");
            ErrorInputBox("newDateRes");
            isOk5 = false;
        }
        let isOk = isOk1 * isOk2 * isOk3 * isOk4 * isOk5;

        if (isOk) {
            $.ajax({
                url: "update_resolution.php",
                dataType: "json",
                data: ({ resolution: resolution2, date_resolution: date_resol2, range_reso_ini: range_reso_ini2, range_reso_last: range_reso_last2, curr_resol: curr_resol2 }),
                type: "POST",
                success: function (r) {
                    $("#MainRoller_background").hide();
                    if (r[0][0] == "S") {
                        CloseUpdateFactNum();
                        ResetVariables();
                        GetResolutions();
                        alertify.message("Resolución actualizada");
                    } else if (r[0][0] == "F") {
                        ErrorMsgModalBox("open", "errorMsgUpdateFactNum", r[0][1]);
                    } else {
                        ErrorMsgModalBox("open", "errorMsgUpdateFactNum", "Se produjo un error actualizando. Intenta de nuevo.");
                    }

                }
            });

        } else {
            $("#MainRoller_background").hide();
        }

    });

    $("#set_var_sub").click(function () {
        $("#MainRoller_background").show();

        let AcctNameTitle2 = $("#newName").val();
        let AcctCountry2 = $("#newCountry").val();
        let AcctCity2 = $("#newCity").val();
        let AcctAddress2 = $("#newAddress").val();
        let AcctPhone2 = $("#newPhone").val().replace(/[^0-9]/g, "");
        let AcctNIT2 = $("#newNIT").val();
        let TypOfInd = $("#newIndustry").val();
        let ActEconm = $("#newActEcon").val();

        let isOk1 = CheckInputText("newName", "errorMsgUpdateUserVars", "nombre", true, 0, 50, false, false, true);
        let isOk2 = CheckInputText("newCity", "errorMsgUpdateUserVars", "ciudad", false, 0, 50, false, false, true);
        let isOk3 = CheckInputText("newAddress", "errorMsgUpdateUserVars", "dirección", false, 0, 50, false, false, false);

        let isOk4 = true;
        if (AcctPhone2.length > 10) {
            ErrorMsgModalBox("open", "errorMsgUpdateUserVars", "El teléfono no puede ser mayor a 10 dígitos");
            ErrorInputBox("newPhone");
            isOk4 = false;
        } else if (isNaN(AcctPhone2)) {
            ErrorMsgModalBox("open", "errorMsgUpdateUserVars", "El teléfono debe contener únicamente valores numéricos");
            ErrorInputBox("newPhone");
            isOk4 = false;
        }

        let isOk = isOk1 * isOk2 * isOk3 * isOk4;

        if (isOk) {
            $.ajax({
                url: "update_vars_accnt.php",
                dataType: "json",
                data: ({ bus_name: AcctNameTitle2, bus_address: AcctAddress2, bus_phone: AcctPhone2, bus_nit: AcctNIT2, bus_act_econ: ActEconm, bus_industry: TypOfInd, bus_country: AcctCountry2, bus_city: AcctCity2 }),
                type: "POST",
                success: function (r) {
                    $("#MainRoller_background").hide();
                    if (r[0][0] == "S") {
                        CloseUpdateUserVars();
                        ResetVariables();
                        alertify.message("Actualización correcta");
                    } else {
                        ErrorMsgModalBox("open", "errorMsgUpdateUserVars", "Se produjo un error actualizando. Intenta de nuevo.");
                    }
                }
            });
        }
        $("#MainRoller_background").hide();

    });

    $("#new_pass_1").keyup(function () {
        let pass = $("#new_pass_1").val();
        let strg_pass = checkStrengthPass(pass);

        if (strg_pass <= 2) {
            $("#pass_lvl").text("Débil");
            $("#pass_lvl_bar").css("width", "40px");
            $("#pass_lvl_bar").css("background-color", "#de5e5e");
        } else if (strg_pass == 3) {
            $("#pass_lvl").text("Media");
            $("#pass_lvl_bar").css("width", "80px");
            $("#pass_lvl_bar").css("background-color", "#FF9900");
        } else if (strg_pass == 4) {
            $("#pass_lvl").text("Buena");
            $("#pass_lvl_bar").css("width", "120px");
            $("#pass_lvl_bar").css("background-color", "#FFCC00");
        } else if (strg_pass == 5) {
            $("#pass_lvl").text("Muy buena");
            $("#pass_lvl_bar").css("width", "160px");
            $("#pass_lvl_bar").css("background-color", "#779933");
        } else if (strg_pass == 6) {
            $("#pass_lvl").text("Excelente!");
            $("#pass_lvl_bar").css("width", "200px");
            $("#pass_lvl_bar").css("background-color", "#669933");
        }

    });

});

/**************************************/
/********* INICIO FUNCIONES ***********/
/**************************************/

let ToggleReportFreq = function (freq, type, this_obj) {
    $.ajax({
        url: "update_report_frq.php",
        dataType: "json",
        data: ({ frq: freq, chng_type: type }),
        type: "POST",
        success: function (r) {
            if (r[0][0] == 'S') {
                $(this_obj).toggleClass('active');
            } else {
                alertify.alert('', 'Ocurrió un error durante la actualización.');
            }
        },
        error: function () {
            alertify.alert('', 'Ocurrió un error durante la actualización.');
        }
    });

}

function ResetVariables() {
    // Cargar Variables de Sesión de Usuario
    $.ajax({
        url: "get_session_vars.php",
        dataType: "json",
        data: ({ x: "1" }),
        type: "POST",
        success: function (r) {
            if (r != undefined) {
                let VBDefault = " - ";

                let AcctNameTitle = (r["bus_name"] == "" || r["bus_name"] == undefined) ? VBDefault : r["bus_name"];
                let AcctEmailTitle = (r["bus_email"] == "" || r["bus_email"] == undefined) ? VBDefault : r["bus_email"];
                let AcctUserSince = (r["creation_date"] == "" || r["creation_date"] == undefined) ? VBDefault : r["creation_date"];
                let AcctName = (r["bus_name"] == "" || r["bus_name"] == undefined) ? VBDefault : r["bus_name"];
                let AcctIndustry = (r["ind_name"] == "" || r["ind_name"] == undefined) ? VBDefault : r["ind_name"];
                let AcctAddress = (r["bus_address"] == "" || r["bus_address"] == undefined) ? VBDefault : r["bus_address"];
                let AcctCountry = (r["bus_country"] == "" || r["bus_country"] == undefined) ? VBDefault : r["bus_country"];
                let AcctCity = (r["bus_city"] == "" || r["bus_city"] == undefined) ? VBDefault : r["bus_city"];
                let AcctPhone = (r["bus_phone"] == "" || r["bus_phone"] == undefined) ? VBDefault : r["bus_phone"];
                let AcctNIT = (r["bus_nit"] == "" || r["bus_nit"] == undefined) ? VBDefault : r["bus_nit"];
                let AcctActEcon = (r["reg_name"] == "" || r["reg_name"] == undefined) ? VBDefault : r["reg_name"];
                let AcctCountryName = (r["country_name"] == "" || r["country_name"] == undefined) ? VBDefault : r["country_name"];
                let AcctCountryFlag = (r["country_flag"] == "" || r["country_flag"] == undefined) ? VBDefault : r["country_flag"];

                let date_resol = (r["date_resol"] == "" || r["date_resol"] == undefined) ? VBDefault : r["date_resol"];
                let resolution = (r["resolution"] == "" || r["resolution"] == undefined) ? VBDefault : r["resolution"];
                let range_reso_ini = (r["range_reso_ini"] == "" || r["range_reso_ini"] == undefined) ? VBDefault : r["range_reso_ini"];
                let range_reso_last = (r["range_reso_last"] == "" || r["range_reso_last"] == undefined) ? VBDefault : r["range_reso_last"];

                if (AcctUserSince != VBDefault) {
                    AcctUserSince = moment().diff(moment(r["creation_date"]), "days");
                }

                if (date_resol != VBDefault) {
                    date_resol = moment(date_resol).format("YYYY-MM-DD");
                }

                $("#AcctNameTitle").text(AcctNameTitle);
                $("#AcctEmailTitle").text(AcctEmailTitle);
                $("#AcctUserSince").text(AcctUserSince);

                $("#AcctName").text(AcctName);
                $("#AcctIndustry").text(AcctIndustry);
                $("#AcctAddress").text(AcctAddress);
                $("#AcctCountry").text(AcctCountryName);
                $("#AcctCity").text(AcctCity);
                $("#AcctPhone").text(AcctPhone);
                $("#AcctNIT").text(AcctNIT);
                $("#AcctActEcon").text(AcctActEcon);
                $("#date_resol").text(date_resol);
                $("#resolution").text(resolution);
                $("#range_reso_ini").text(range_reso_ini);
                $("#range_reso_last").text(range_reso_last);

                let AcctNameTitle2 = (AcctNameTitle == VBDefault) ? "" : AcctNameTitle;
                let AcctCountry2 = (AcctCountry == VBDefault) ? "" : AcctCountry;
                let AcctCity2 = (AcctCity == VBDefault) ? "" : AcctCity;
                let AcctAddress2 = (AcctAddress == VBDefault) ? "" : AcctAddress;
                let AcctPhone2 = (AcctPhone == VBDefault) ? "" : AcctPhone;
                let AcctNIT2 = (AcctNIT == VBDefault) ? "" : AcctNIT;
                let date_resol2 = (date_resol == VBDefault) ? "" : date_resol;
                let resolution2 = (resolution == VBDefault) ? "" : resolution;
                let range_reso_ini2 = (range_reso_ini == VBDefault) ? "" : range_reso_ini;
                let range_reso_last2 = (range_reso_last == VBDefault) ? "" : range_reso_last;

                let TypOfPaym = (r["bus_industry"] == "" || r["bus_industry"] == undefined) ? "0" : r["bus_industry"];
                let ActEconm = (r["bus_act_econ"] == "" || r["bus_act_econ"] == undefined) ? "0" : r["bus_act_econ"];

                $("#newName").val(AcctNameTitle2);
                us_cont = AcctCountry2;
                $("#newCountry").val(AcctCountry2);
                $("#newCity").val(AcctCity2);
                $("#newAddress").val(AcctAddress2);
                $("#newPhone").val(AcctPhone2);
                $("#newNIT").val(AcctNIT2);
                $("#newDateRes").val(date_resol2);
                $("#newResNum").val(resolution2);
                $("#newConsFrom").val(range_reso_ini2);
                $("#newConsTo").val(range_reso_last2);
                $("#newIndustry").val(TypOfPaym);
                $("#newActEcon").val(ActEconm);

                if (AcctCountryFlag == VBDefault) {
                    $("#flag_country").hide();
                    $("#flag_country").attr("src", "");
                    $("#newCountry").removeClass("select_country_sty");
                    $("#newCountry").css({ "background-image": "none" });
                } else {
                    $("#flag_country").show();
                    $("#flag_country").attr("src", AcctCountryFlag);
                    $("#newCountry").addClass("select_country_sty");
                    $("#newCountry").css({ "background-image": "url('" + AcctCountryFlag + "')" });
                }

                // REPORT 
                (r["sum_mail_d"] == "1") ? $('#rep_d').addClass('active') : $('#rep_d').removeClass('active');
                (r["sum_mail_w"] == "1") ? $('#rep_w').addClass('active') : $('#rep_w').removeClass('active');
                (r["sum_mail_m"] == "1") ? $('#rep_m').addClass('active') : $('#rep_m').removeClass('active');

            } else {
                alertify.alert("Error #30292", "No fue posible cargar la información de la cuenta");
            }

        }
    });
}

function verifyDeleteUser() {
    event.preventDefault();
    alertify.confirm("Estás a punto de darte de baja. ¿Estás seguro que deseas confirmar esta decisión?", function (e) {
        if (e) {
            alertify.alert().setContent("<h1 style='text-align: center;'> ¡Te esperamos pronto! </h1>").set("basic", true).show();
            $.ajax({
                url: "user_gone.php",
                data: ({ x: "1" }),
                type: "POST",
                success: function (r) {
                    setTimeout("location.href = 'https://www.2luca.co';", 1500);
                }
            });
        }
    }).setHeader("!Atención!");

}

function verify_set_pass() {
    event.preventDefault();
    let isOk = true;
    // Password 1
    if ($("#new_pass_1").val() == "") {
        ErrorMsgModalBox("open", "errorMsgNewPass", "Ingresa la contraseña");
        ErrorInputBox("new_pass_1");
        isOk = false;
    }
    // Validar Número de caracteres en contraseña
    if ($("#new_pass_1").val().length < 7) {
        ErrorMsgModalBox("open", "errorMsgNewPass", "La contraseña debe contener mínimo 7 caracteres");
        ErrorInputBox("new_pass_1");
        isOk = false;
    }
    // Validar Número de caracteres en contraseña
    if ($("#new_pass_1").val().length > 30) {
        ErrorMsgModalBox("open", "errorMsgNewPass", "La contraseña debe contener máximo 30 caracteres");
        ErrorInputBox("new_pass_1");
        isOk = false;
    }
    let strPas = checkStrengthPass($("#new_pass_1").val());
    // Password 1 - Strenght
    if (strPas <= 3) {
        ErrorMsgModalBox("open", "errorMsgNewPass", "La contraseña es muy débil. Intenta usando una combianción de letras mayúsculas, minúsculas, números y caracteres especiales.");
        ErrorInputBox("new_pass_1");
        isOk = false;
    }
    // Password 2
    if ($("#new_pass_2").val() == "") {
        ErrorMsgModalBox("open", "errorMsgNewPass", "Ingresa la validación de la contraseña");
        ErrorInputBox("new_pass_2");
        isOk = false;
    }
    // VALIDACIÓN DE CONTRASEÑA
    if ($("#new_pass_1").val() !== $("#new_pass_2").val()) {
        ErrorMsgModalBox("open", "errorMsgNewPass", "Las contraseñas no coinciden");
        ErrorInputBox("new_pass_2");
        isOk = false;
    }

    if (isOk) {
        let old_pass = $("#old_pass").val().trim();
        let new_pass_1 = $("#new_pass_1").val().trim();
        let new_pass_2 = $("#new_pass_2").val().trim();
        let mr = new MainRoller();
        mr.startMainRoller();
        $.ajax({
            url: "change_password.php",
            dataType: "json",
            data: ({ new_pass_1: new_pass_1, new_pass_2: new_pass_2, old_pass: old_pass }),
            type: "POST",
            success: function (r) {
                mr.stopMainRoller();
                if (r[0]["flag"] == 0) {
                    alertify.alert("", "La contraseña fue modificada correctamete.");
                    $("#old_pass").val("");
                    $("#new_pass_1").val("");
                    $("#new_pass_2").val("");
                } else {
                    ErrorMsgModalBox("open", "errorMsgNewPass", r[0]["flag_desc"]);
                }

            }
        });
    }

}
/**************** IMAGE JS FUNCTIONS *********************/
function loadLogoGallery(pathGalleryLogo) {
    //$("#img_avatar").attr("src", pathGalleryLogo);
    $("#img_avatar").css("background-image", "url(" + pathGalleryLogo + ")");
    $("#img_avatar").css("height", "120px"); //Adjust Size Height of Image 
    $("#img_gallery_path").attr("value", pathGalleryLogo);
    CloseGallery();
    CloseAvtrForm();
    // Click on Submit Form to send request to PHP
    $("#act_submit_2").click();
}
/**************** END OF GALLERY IMAGE JS FUNCTIONS *********************/


function GetResolutions() {
    $.ajax({
        url: "get_resolutions.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            if (r.length > 0 && r != undefined) {
                $("#admin_res_wrap").html("");
                $("#NoResolutions").hide();
                $("#resolt_act").show();
                let html = '';
                for (let i = 0; i < r.length; i++) {
                    html += '<div class="res_elem_cont"><div class="res_switch"><label class="switch">';
                    let ck = (r[i]["current_resolution"] == '1') ? 'checked' : '';
                    html += '<input class="res_cb" type="checkbox" ' + ck + '><span class="slider round"></span></label></div>';
                    html += '<div class="res_item">';
                    html += '<p>' + r[i]["resolution"] + '</p>';
                    let d = r[i]["date_resolution"];
                    d = d.substring(0, 10);
                    html += '<p>' + d + '</p>';
                    html += '<p>' + r[i]["range_reso_ini"] + ' a ' + r[i]["range_reso_last"] + '</p>';
                    html += '</div><div class="res_tools">';
                    html += '<span onclick="DeleteResolution(&#39;' + r[i]["resolution"] + '&#39;)" class="mdi mdi-delete-outline"></span>';
                    //html += '<span onclick="EditResolution(&#39;'+r[i]["resolution"]+'&#39;)" class="mdi mdi-file-document-edit-outline"></span>';
                    html += '</div></div>';
                }
                $("#admin_res_wrap").append(html);

                $(".res_cb").change(function () {
                    let x = this.checked;
                    ResetAllResCheck();
                    this.checked = x;
                });

            } else {
                $("#resolt_act").hide();
                $("#NoResolutions").show();
            }
        }
    });
}

function ResetAllResCheck() {
    let x = document.getElementsByClassName("res_cb");
    if (x != undefined) {
        for (let i = 0; i < x.length; i++) {
            x[i].checked = false;
        }
    }
}

function EditResolution(resolution) {
    //let mr = new MainRoller();
    //mr.startMainRoller();
}

function DeleteResolution(resolution) {

    alertify.confirm("Eliminar resolución", "¿Seguro desea eliminar la resolución?", function () {
        $("#MainRoller_background").show();
        if (resolution != "") {
            resolution = resolution.trim();
            $.ajax({
                url: "delete_resolution.php",
                dataType: "json",
                data: ({ resolution: resolution }),
                type: "POST",
                success: function (r) {
                    $("#MainRoller_background").hide();
                    if (r[0][0] == "S") {
                        GetResolutions();
                        ResetVariables();
                        alertify.message("Resolución eliminada correctamente");
                    } else {
                        ErrorMsgModalBox("open", "errorMsgAdminResolutions", "Ocurrió un error eliminando la resolución.");
                        console.error("ERROR -> FILE: JS_ACC_MNG FUN: DELETERESOLUTION");
                    }
                }
            });
        } else {
            $("#MainRoller_background").hide();
            ErrorMsgModalBox("open", "errorMsgAdminResolutions", "Ocurrió un error eliminando la resolución.");
            console.error("ERROR -> FILE: JS_ACC_MNG FUN: DELETERESOLUTION");
        }

    }, function () { });
}

