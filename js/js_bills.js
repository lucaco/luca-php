var search_bill_index = "td_id_bill";
var actFilters = new filtersOrders();
var salesman_v = [];

$(document).ready(function () {
    // Inicializate Spinner and Rollers
    $('#LoadingRoller_filterOrders').hide();

    // Populate Filter Salesman
    PopulateFilterSalesman();

    // Get all Orders
    RefreshOrders();

    // Comandas
    isRestaurant().then(function (r) { (r.value) ? $("#cmd_lnk").show() : $("#cmd_lnk").hide() });
    $("#cmd_lnk").click(function () { window.open('../php/comandas.php'); });

    /* MAX DATE  */
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("filter_date_from").setAttribute("max", today);
    $("#filter_date_to").val(today);
    document.getElementById("filter_date_to").setAttribute("max", today);
    $("#filter_date_to").val(today);

    $("#filter_date_sel").on('change', function () {
        var selection = this.value;
        var date_from, date_to;

        $("#filter_date_from_cont").hide();
        $("#filter_date_to_cont").hide();

        if (selection == "0") {
            date_from = "1910-01-01";
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 1) {
            date_from = moment().format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 2) {
            date_from = moment(moment().subtract(1, 'days')._d).format("YYYY-MM-DD");
            date_to = moment(moment().subtract(1, 'days')._d).format("YYYY-MM-DD");
        } else if (selection == 3) {
            date_from = moment(moment().subtract(7, 'days')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 4) {
            date_from = moment(moment().subtract(15, 'days')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 5) {
            date_from = moment(moment().subtract(1, 'months')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 6) {
            date_from = moment(moment().subtract(1, 'years')._d).format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 7) {
            date_from = moment().format("YYYY") + "-" + moment().format("MM") + "-01";
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 8) {
            date_from = moment().format("YYYY") + "-01-01"
            date_to = moment().format("YYYY-MM-DD");
        } else if (selection == 9) {
            date_from = "";
            date_to = "";
            $("#filter_date_from").focus();
            $("#filter_date_from_cont").show();
            $("#filter_date_to_cont").show();
        } else {
            date_from = moment().format("YYYY-MM-DD");
            date_to = moment().format("YYYY-MM-DD");
        }

        $("#filter_date_from").val(date_from);
        $("#filter_date_to").val(date_to);

        actFilters.dateFrom = date_from;
        actFilters.dateTo = date_to;

    });

    $('#select_type_search').on('change', function () {
        search_bill_index = this.value;
        CloseAllBillRows();
        $("#search_bill").val("");
        find_bill();
        if (search_bill_index == "td_id_bill") {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/41-shopping/receipt-4.svg")');
            $("#search_bill").attr('placeholder', 'Ingresa el ID de la cuenta ...');
        } else if (search_bill_index == "td_date") {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/05-time/calendar-2.svg")');
            $("#search_bill").attr('placeholder', 'Ingresa la fecha (AAAA-MM-DD) ...');
        } else if (search_bill_index == "td_cust_name") {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/07-users/male.svg")');
            $("#search_bill").attr('placeholder', 'Ingresa el nombre del cliente ...');
        } else if (search_bill_index == "td_vendor") {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/07-users/business-man-2.svg")');
            $("#search_bill").attr('placeholder', 'Ingresa el nombre del vendedor ...');
        } else if (search_bill_index == "td_cust_id") {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/07-users/id-card-2.svg")');
            $("#search_bill").attr('placeholder', 'Ingresa la identificación del cliente ...');
        } else {
            $("#select_type_search").css('background-image', 'url("../icons/SVG/41-shopping/receipt-4.svg")');
        }
        $("#search_bill").focus();
    });

    $("#btnFilterOrders").click(function () {
        actFilters.dateFrom = $("#filter_date_from").val();
        actFilters.dateTo = $("#filter_date_to").val();
        if (actFilters.valorFrom == '') { actFilters.valorFrom = 0 };
        if (actFilters.valorTo == '') { actFilters.valorTo = 100000000 };
        if (actFilters.dateFrom == '') { actFilters.dateFrom = "1910-01-01" };
        if (actFilters.dateTo == '') { actFilters.dateTo = moment().format("YYYY-MM-DD") };

        if ($("#filter_val_min").val() != '') {
            actFilters.valorFrom = parseFloat($("#filter_val_min").val().toString().replace(/[^0-9.]/g, ""));
        }
        if ($("#filter_val_max").val() != '') {
            actFilters.valorTo = parseFloat($("#filter_val_max").val().toString().replace(/[^0-9.]/g, ""));
        }
        if ($("#filter_val_min").val() != '' && $("#filter_val_max").val() != '') {
            if (actFilters.valorFrom > actFilters.valorTo) {
                ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'El valor no puede ser mayor que el del campo: menor o igual.');
                ErrorInputBox('filter_val_min');
                return false;
            }
        }
        if ($("#filter_date_from").val() != '' && $("#filter_date_to").val() != '') {
            if (actFilters.dateFrom > actFilters.dateTo) {
                ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'La fecha inicial no puede ser mayor que la fecha final.');
                ErrorInputBox('filter_date_from');
                return false;
            }
        }
        if ($("#filter_date_from").val() == '' && $("#filter_date_sel").val() == '9') {
            ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'Ingresa el campo de fecha de incio');
            ErrorInputBox('filter_date_from');
            return false;
        }

        if ($("#filter_date_to").val() == '' && $("#filter_date_sel").val() == '9') {
            ErrorMsgModalBox('open', 'errorMsgfilterOrders', 'Ingresa el campo de fecha final');
            ErrorInputBox('filter_date_to');
            return false;
        }
        $("#reset_filter_content").removeClass("hide");
        RefreshOrders(true);
        CloseFilterBills();

    });

});

function RefreshOrders(isFilter) {
    (isFilter == undefined) ? isFilter = false : isFilter = isFilter;

    let mr = new MainRoller();
    mr.startMainRoller();

    $.ajax({
        url: "get_all_orders.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            mr.stopMainRoller();
            $("#cont_table_billsdata").html("");
            var htmlBills = "";
            var tot_value = 0;
            var tot_items = 0;

            if (r.length > 0) {
                for (var i = 0; i <= r.length - 1; i++) {
                    itm_value = numRound(r[i]["trx_value"], 0);
                    itm_date = moment(r[i]["trx_date"].substring(0, 10));
                    itm_tyopm = r[i]['payment_type'];
                    itm_vendor = r[i]['prof_name'];
                    itm_points_red = r[i]["points_redeem"];

                    // Apply Filters
                    if (isFilter) {
                        if (actFilters.salesman.indexOf(itm_vendor) > -1 && actFilters.topym.indexOf(itm_tyopm) > -1 && itm_value >= actFilters.valorFrom && itm_value <= actFilters.valorTo && itm_date >= moment(actFilters.dateFrom) && itm_date <= moment(actFilters.dateTo)) {
                        } else {
                            continue;
                        }
                    }

                    tot_value += parseFloat(r[i]["trx_value"]);
                    tot_items += 1;

                    htmlBills += "<tr class='tr_gnr_data' onclick='toogleDeepData(&#39;" + r[i]["id_bill"] + "&#39;)'>";
                    htmlBills += "<td class='td_id_bill'>" + r[i]["id_bill"] + "</td>";
                    htmlBills += "<td class='td_date'>" + r[i]["trx_date"].substring(0, 10) + "</td>";
                    htmlBills += "<td class='td_value'>$ " + numCommas(numRound(r[i]["trx_value"], 0)) + "</td>";
                    if (r[i]['payment_type'] == '1') {
                        icn_payt_ty = 'bank-notes-3.svg';
                    } else if (r[i]['payment_type'] == '2') {
                        icn_payt_ty = 'credit-card-visa.svg';
                    } else if (r[i]['payment_type'] == '3') {
                        icn_payt_ty = 'credit-card.svg';
                    } else if (r[i]['payment_type'] == '4') {
                        icn_payt_ty = 'wallet-3.svg';
                    } else {
                        icn_payt_ty = 'piggy-bank.svg';
                    }
                    htmlBills += "<td class='icon_bill_paymtype'><img src='../icons/SVG/44-money/" + icn_payt_ty + "'></td>";
                    htmlBills += "</tr>";

                    htmlBills += "<tr id='cont_" + r[i]["id_bill"] + "' class='tr_deep_data'><td colspan='4' class='td_bill_deep_data'><div id='bill_info_" + r[i]["id_bill"] + "' class='info_bill'>";
                    htmlBills += "<div class='info_bill_1'>";
                    htmlBills += "<div class='info_bill_gnrdata'>";
                    c_name = (r[i]['cust_name'] == '' || r[i]['cust_name'] == 'null' || r[i]['cust_name'] == undefined) ? ' - ' : r[i]['cust_name'];
                    c_id = (r[i]['cust_id'] == '' || r[i]['cust_id'] == 'null' || r[i]['cust_id'] == undefined) ? ' - ' : r[i]['cust_id'];
                    htmlBills += "<p><span class='mdi mdi-account-circle-outline'></span> <b>Cliente:</b> <span class='td_cust_name'>" + c_name + "</span></p>";
                    htmlBills += "<p><span class='mdi mdi-account-badge-horizontal-outline'></span> <b>Identificación del Cliente:</b> <span class='td_cust_id'>" + c_id + "</span></p>";
                    htmlBills += "<p><span class='mdi mdi-cash-usd'></span> <b>Impuesto pagado:</b> $ " + numCommas(numRound(r[i]['taxes'], 0)) + "</p>";
                    htmlBills += "<p><span class='mdi mdi-coin'></span> <b>Propina pagada:</b> $ " + numCommas(numRound(r[i]['tips'], 0)) + "</p>";
                    if (r[i]['payment_type'] == '1') {
                        htmlBills += "<p><span class='mdi mdi-coins'></span> <b>Cambio devuelto:</b> $ " + numCommas(numRound(r[i]['cash_change'], 0)) + "</p>";
                    }
                    htmlBills += "<p><span class='mdi mdi-shopping'></span> <b>Total ítems:</b> " + numCommas(numRound(r[i]['num_items'], 0)) + " <span class='info_bill_link2' onclick='IniciateItemsBill(&#39;" + r[i]["id_bill"] + "&#39;)'> Ver todos los ítems</span></p>";
                    htmlBills += "</div>";
                    htmlBills += "<div class='info_bill_other_data'>";
                    if (itm_points_red > 0) {
                        htmlBills += "<div class='bill_info_pts_red'><span class='mdi mdi-star-outline'></span> <b>Puntos redimidos:</b> " + itm_points_red + " </div>";
                    }
                    if (r[i]['prof_name'] != '' && r[i]['prof_name'] != 'null' && r[i]['prof_name'] != undefined) {
                        htmlBills += "<div class='bill_info_vendor'><img src='" + r[i]['prof_avatar'] + "'> <b>Atendió:</b> <span class='td_vendor'>" + r[i]['prof_name'] + "</span></div>";
                    }
                    htmlBills += "</div>";
                    htmlBills += "</div>";
                    if (r[i]['cust_email'] != '' && r[i]['cust_email'] != 'null' && r[i]['cust_email'] != undefined) {
                        htmlBills += "<div onclick='SendInvoiceByIdBill(&#39;" + r[i]["id_bill"] + "&#39;,2)' class='info_bill_link'><span class='mdi mdi-email-outline'></span> Reenviar comprobante</div>";
                    }
                    htmlBills += "<div onclick='PrintInvoiceByIdBill(&#39;" + r[i]["id_bill"] + "&#39;)' class='info_bill_link'><span class='mdi mdi-printer'></span> Imprimir recibo</div>";
                    htmlBills += "<div onclick='DeleteInvoiceByIdBill(&#39;" + r[i]["id_bill"] + "&#39;)' class='info_bill_link red_bg'><span class='mdi mdi-delete-outline'></span> Eliminar</div>";
                    htmlBills += "</div></td></tr>";

                }

                $("#tot_value_header").text(numCommas(numRound(tot_value, 0)));
                $("#tot_bills_header").text(tot_items);
                $("#cont_table_billsdata").append(htmlBills);
                $("#noOrdersDataWrap").html("");
                $("#noOrdersDataWrap").hide();
                $("#bills_cont").show();

                if (isFilter) { alertify.message("Filtro actualizado correctamente"); }
            } else {
                htmlBills += "<img src='../icons/general/cactus.svg'>";
                htmlBills += "<p>No has realizado ventas aún</p>";
                $("#noOrdersDataWrap").html("");
                $("#noOrdersDataWrap").append(htmlBills);
                $("#cont_table_billsdata").html("");
                $("#tot_value_header").text('0');
                $("#tot_bills_header").text('0');
                $("#noOrdersDataWrap").show();
                $("#bills_cont").hide();
            }
        }
    });
}
function resetFilters() {
    actFilters.topym = ["1", "2", "3", "4"];
    actFilters.dateFrom = '1910-01-01';
    actFilters.dateTo = moment().format("YYYY-MM-DD");
    actFilters.salesman = [];
    actFilters.valorFrom = 0;
    actFilters.valorTo = 100000000;

    $("#topym_1").addClass("opt_selected");
    $("#topym_2").addClass("opt_selected");
    $("#topym_3").addClass("opt_selected");
    $("#topym_4").addClass("opt_selected");

    var x = document.getElementById("content_vendors_filter").getElementsByClassName("salesman_obj");
    var vendName = "";
    for (var i = 0; i < x.length; i++) {
        $("#" + x[i].id).addClass("opt_selected");
        vendName = x[i].innerText || x[i].textContent;
        vendName = (vendName == "ADMIN") ? "Administrador" : vendName;
        actFilters.salesman.push(vendName);
    }

    $("#filter_val_min").val("");
    $("#filter_val_max").val("");
    $("#filter_date_from").val("");
    $("#filter_date_to").val("");
    $("#filter_date_sel").val("0");
    $("#filter_date_from_cont").hide();
    $("#filter_date_to_cont").hide();
    $("#reset_filter_content").addClass("hide");

    RefreshOrders();
    CloseFilterBills();
}
function toogleDeepData(idBill) {
    if ($("#bill_info_" + idBill).height() > 0) {
        $("#bill_info_" + idBill).removeClass("info_bill_on");
        $("#cont_" + idBill).removeClass("display-block");
    } else {
        CloseAllBillRows();
        $("#bill_info_" + idBill).addClass("info_bill_on");
        $("#cont_" + idBill).addClass("display-block");
    }
}
function CloseAllBillRows() {
    $(".info_bill").removeClass("info_bill_on");
}

function find_bill() {
    CloseAllBillRows();
    var input, filter, table, tr, td, i, txtValue, k, valTot, val;
    input = document.getElementById("search_bill");
    filter = input.value.toUpperCase();
    table = document.getElementById("bills_table");
    tr = table.getElementsByTagName("tr");
    k = 0;
    valTot = 0;
    val = 0;

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByClassName(search_bill_index)[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            txtValue = quitaacentos(txtValue);
            txtValue = txtValue.trim();

            if (txtValue.toLowerCase().indexOf(quitaacentos(filter.trim())) > -1) {
                k += 1;
                if (search_bill_index == "td_cust_name" || search_bill_index == "td_cust_id" || search_bill_index == "td_vendor") {
                    // tr[i-1].style.display = "";
                    $(tr[i - 1]).removeClass("bye_row");
                    val = tr[i - 1].getElementsByClassName("td_value")[0];
                } else {
                    // tr[i].style.display = "";
                    $(tr[i]).removeClass("bye_row");
                    val = tr[i].getElementsByClassName("td_value")[0];
                }
                val = val.textContent || val.innerText;
                val = parseFloat(val.toString().replace(/[^0-9.]/g, ""));
                valTot += val;
            } else {
                if (search_bill_index == "td_cust_name" || search_bill_index == "td_cust_id" || search_bill_index == "td_vendor") {
                    //tr[i-1].style.display = "none"
                    $(tr[i - 1]).addClass("bye_row");
                } else {
                    //tr[i].style.display = "none";
                    $(tr[i]).addClass("bye_row");
                }
            }
        }
    }
    $("#tot_value_header").text(numCommas(numRound(valTot, 0)));
    $(".tr_deep_data>td").css('padding', '!important');
    $("#tot_bills_header").text(k);
}


function SelectItemFilter(e) {
    var id_e = e.id;
    var x;
    if (id_e.indexOf("vend_") > -1) {
        x = id_e.replace("vend_", "");
        x = salesman_v[x];

        if (e.classList.contains("opt_selected")) {
            $(e).removeClass("opt_selected");
            actFilters.salesman.splice(actFilters.salesman.indexOf(x), 1);
        } else {
            $(e).addClass("opt_selected");
            actFilters.salesman.push(x);
        }

    } else if (id_e.indexOf("topym_") > -1) {
        x = id_e.replace("topym_", "");

        if (e.classList.contains("opt_selected")) {
            $(e).removeClass("opt_selected");
            actFilters.topym.splice(actFilters.topym.indexOf(x), 1);
        } else {
            $(e).addClass("opt_selected");
            actFilters.topym.push(x);
        }

    }
}

function PopulateFilterSalesman() {

    $.ajax({
        url: "pull_salesmen.php",
        dataType: "json",
        data: ({ x: '1' }),
        type: "POST",
        beforeSend: function () { },
        success: function (r) {
            $("#content_vendors_filter").html('');
            if (r.length > 0) {
                $("#wrap_vendors_filter").show();
                var htmlTxt = '';
                salesman_v = [];
                for (var i = 0; i < r.length; i++) {
                    salesman_v.push(r[i]["prof_name"]);
                    actFilters.salesman.push(r[i]["prof_name"]);
                    if (r[i]["admin"] == '1') {
                        htmlTxt += '<div id="vend_' + i + '" onclick="SelectItemFilter(this)" class="salesman_obj opt_selected"><img src="' + r[i]["avatar"] + '"><p>ADMIN</p></div>';
                    } else {
                        htmlTxt += '<div id="vend_' + i + '" onclick="SelectItemFilter(this)" class="salesman_obj opt_selected"><img src="' + r[i]["avatar"] + '"><p>' + r[i]["prof_name"] + '</p></div>';
                    }
                }
                $("#content_vendors_filter").append(htmlTxt);
            } else {
                $("#wrap_vendors_filter").hide();
            }
        }
    });
}
function filtersOrders() {
    this.topym = ["1", "2", "3", "4"];
    this.dateFrom = '1910-01-01';
    this.dateTo = moment().format("YYYY-MM-DD");
    this.salesman = [];
    this.valorFrom = 0;
    this.valorTo = 100000000;
}
function export_trx_csv() {

    let mr = new MainRoller();
    mr.startMainRoller();

    var headers = { id_bill: 'CODIGO CUENTA', trx_date: 'FECHA', item_id: 'CODIGO ITEM', prod_name: 'NOMBRE ITEM', item_count: 'CANTIDAD', item_value: 'VALOR', pym_name: 'TIPO PAGO', item_discount: 'DESCUENTO', prod_unit_price: 'VALOR UNITARIO', prod_units: 'UNIDADES', item_note: 'NOTA', cust_id: "ID CLIENTE", doc_short: 'TIPO ID', prof_name: 'VENDEDOR' };

    $.ajax({
        url: "get_all_bills.php",
        dataType: "json",
        data: ({ x: 1 }),
        type: "POST",
        success: function (r) {
            var items = [];
            if (r.length > 0) {
                r.forEach(function (item) {
                    items.push({
                        id_bill: item.id_bill.replace(/,/g, ''),
                        trx_date: item.trx_date.replace(/,/g, ''),
                        item_id: item.item_id.replace(/,/g, ''),
                        prod_name: item.prod_name.replace(/,/g, ''),
                        item_count: item.item_count.replace(/,/g, ''),
                        item_value: item.item_value.replace(/,/g, ''),
                        pym_name: item.pym_name.replace(/,/g, ''),
                        item_discount: item.item_discount.replace(/,/g, ''),
                        prod_unit_price: item.prod_unit_price.replace(/,/g, ''),
                        prod_units: item.prod_units.replace(/,/g, ''),
                        item_note: item.item_note.replace(/,/g, ''),
                        cust_id: item.cust_id.replace(/,/g, ''),
                        doc_short: item.doc_short.replace(/,/g, ''),
                        prof_name: item.prof_name.replace(/,/g, '')

                    });
                });
                var fileTitle = 'descarga_ventas_' + moment().format("YYYY_MM_DD");
                mr.stopMainRoller();
                exportCSVFile(headers, items, fileTitle);
            } else {
                mr.stopMainRoller();
                alertify.alert("", "No hay datos para descargar");
            }
        }
    });

}

function DeleteInvoiceByIdBill(id_bill) {
    alertify.confirm('Borrar cuenta', "¿Seguro desea eliminar la cuenta?", function () {
        let mr = new MainRoller();
        mr.startMainRoller();
        $.ajax({
            url: "delete_trx.php",
            dataType: "json",
            data: ({ id_bill: id_bill }),
            type: "POST",
            success: function (r) {
                mr.stopMainRoller();
                if (r[0][0] == "S") {
                    RefreshOrders();
                    alertify.message("Cuenta eliminada correctamente");
                } else {
                    alertify.alert("Error", "No se pudo eliminar la cuenta");
                }
            }
        });
    }, function () { });
}