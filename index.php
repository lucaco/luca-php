<?php
session_start();
$php_warning = "";
$php_message = "";
$php_alert   = "";
$msg_error   = "";
$error_good  = 0;

include("/php/config.php");
include("/php/functions.php");

if ((isset($_SESSION['login_user']) && !empty($_SESSION['login_user']))){header("location:/php/sales.php");}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-135784524-1');
	</script>
	<title>2Luca</title>
	<link type="image/x-icon" href="favicon.ico" rel="shortcut icon" />
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Registro de ventas y reportes utilizando analítica de datos">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="/logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="/json/manifest.json?<?php echo time(); ?>">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="external_libs/wow.min.js"></script>

  	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" rel="stylesheet" href="/css/general_classes.css?<?php echo time();?>">
	<link rel="stylesheet" type="text/css" rel="stylesheet" href="/css/style_landing.css?<?php echo time();?>">
	<script src="/js/js_home_functions.js?<?php echo time();?>"></script>
  	<script src="/js/modals.js?<?php echo time();?>"></script>

	<!-- ANIMATED CSS -->
	<link rel="stylesheet" href="external_libs/animate.min.css?<?php echo time();?>">

	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>

	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
	
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

	<script>

		// Activate Wow.min.js
		new WOW().init();

		$(document).ready(function(){
			// Check Cookies Support
      checkCookiesSupport();
			deleteAllCookies();

			$("#currYear").text(new Date().getFullYear());

			// ******** PHP ALERTS AND MSGS *********
			var phpWarning = '<?php echo $php_warning;?>';
			var phpMessage = '<?php echo $php_message;?>';
			var phpAlert   = '<?php echo $php_alert;?>';
			if(phpWarning!=""){
				alertify.warning(phpWarning);
			} 
			if(phpMessage!=""){
				alertify.message(phpMessage);
			} 
			if(phpAlert!=""){
				alertify.alert('',phpAlert);
			} 
			// **************************************
		
			// ************ LOGIN ******************
			$('#login-form').submit(function(){
				email = $("#login-email").val();
				pass  = $("#login-pass").val();
				remem = String($("#login-rememberme").prop('checked'));
				
				$.ajax({
			      url: "/php/login_func.php",
						data: ({login_email: email, login_pass: pass, login_remember: remem}),
						type: "POST",
						success: function(r){
							if(r == '1'){
								window.location.href = "/php/sales.php";
							}else{
								ErrorMsgModalBox('open','errorMsgLogin',r);
							}
		        }
			    });
				return false;
			});
			//**************************************
			
		});

	</script>

</head>
<body>

	<!-- Begin of Chaport Live Chat code -->
	<script type="text/javascript">
	(function(w,d,v3){
		w.chaportConfig = { appId : '5d113c6d32ca114cebc8d6ad' };
		if(w.chaport)return;v3=w.chaport={};v3._q=[];v3._l={};v3.q=function(){v3._q.push(arguments)};v3.on=function(e,fn){if(!v3._l[e])v3._l[e]=[];v3._l[e].push(fn)};var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://app.chaport.com/javascripts/insert.js';var ss=d.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s,ss)})(window, document);
	</script>
	<!-- End of Chaport Live Chat code -->

	<div class="lndst1-bghome" id="bg-image-home">

			<!-- NAVIGATION BAR -->
			<div class="navigation_container" id="navigation_container">

				<img id="img_logo_nav" src="logos/logo5.png">
				
				<ul id="menu_wide_screen">
					<li>Analítica</li>
					<li>Ayuda</li>
					<li id="btn_nav_login">Iniciar Sesión</li>
					<li id="btn_nav_signin">Registrarme</li>
				</ul>

				<span class="mdi mdi-chevron-down menu_icon" id="menu_icon"></span>

				<ul id="menu_small_screen">
					<li>Analítica</li>
					<li>Ayuda</li>
					<li id="btn_nav_login">Iniciar Sesión</li>
					<li id="btn_nav_signin">Registrarme</li>
				</ul>

			</div>

			<div class="lndst1-wrap">
				<div class="lndst1-cont">
					<div class="lndst1-img">
						<img src="../images/undraw_growth_analytics.svg" alt="landing_img">
					</div>
					<div class="lndst1-txt">
						<h1 class="lndst1-title">Enfocado en los pequeños negocios</h1>
						<p class="lndst1-subtitle">Incrementa tus ventas, controla tu negocio y ahorra tiempo con nuestro sistema de registro de ventas inteligente</p>
					</div>
				</div>	
			</div>

	</div>

	<div class="video_promo_container">
		<h1 class="title_1_font">Luca es diferente</h1>
		<div class="video_promo_wrap">
			<video id="video_promo" controls>
				<source src="video/luca_promo.mp4" type="video/mp4">
				Your browser does not support the HTML5 player.
			</video>
		</div>
	</div>
	

	<div class="home-section-1">

		<div class="home-section-1-b">

			<div class="text_1b_container title_1_font">
				<h1>Características</h1>
			</div>

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_REPORTE.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>REPORTES</h3>
					<p>Monitorea tus ventas de forma intuitiva y amigable con reportes en tiempo real.</p>
				</div>
			</div>

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_PERFIL_CLIENTE.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>CLIENTES</h3>
					<p>Genera lealtad con tus clientes, mira quiénes son tus mejores clientes, qué están comprando y vende de forma inteligente.</p>
				</div>
			</div>

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_SECURITY.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>SEGURIDAD</h3>
					<p>Administra perfiles de vendedores para mantener el control de tus empleados y tus ventas.</p>
				</div>
			</div>

		</div>

		<div class="home-section-1-b">

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_DATABASE.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>ALMACENAMIENTO</h3>
					<p>Almacena y consolida todos tus registros de ventas en un solo lugar de manera organizada y eficiente.</p>
				</div>
			</div>

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_INVOICE.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>FACTURACIÓN</h3>
					<p>Genera un mail o recibo físico de confirmación de forma inteligente y rápida.</p>
				</div>
			</div>

			<div class="sec1b-items">
				<div class="sec1b-items-img">
					<img src="images/ICN_SUPPORT.png?<?php echo time(); ?>" class="wow pulse" data-wow-duration="1s" data-wow-delay="0.3s">
				</div>
				<div>
					<h3>SOPORTE</h3>
					<p>¿Tienes alguna duda o necesitas ayuda?. Con nuestro sistema de soporte resolveremos cualquier cosa que necesites.</p>
				</div>
			</div>
		</div>
		
	</div>


	<div class="home-section-1 bg_waves_w_w">

		<div class="home-section-1-a">

			<div class="text_1a_container">
				<h1 class="title_1_font">Tu Negocio Bajo Control</h1>
				<h2>Lleva tus ventas de forma inteligente y eficiente. Con 2Luca podrás registrar, facturar y tener control de todas las ventas así como generar reportes diarios inteligentes para que no pierdas detalle de lo que pasa en tu negocio.</h2>
			</div>

			<div class="img_1a_container wow fadeIn" data-wow-delay="0.3s" data-wow-duration="1s">
				
			</div>
			
		</div>
	
	</div>

	<div class="home-section-2">

		<div class="sec2a">
			<h1 class="title_1_font">Analítica donde te encuentres</h1>
		</div>

		<div class="sec2b">

			<div class="sec2b_1">
				<img src="images/guyindesktop.svg?<?php echo time();?>">
			</div>

			<div class="sec2b_2">
				<div>
					<p class="sec2b_2_title">Toma el control de tus ventas</p>
					<p class="sec2b_2_text">Con el sistema de reportes y el dashboard en vivo podrás llevar el control de tus ventas en tiempo real.</p>

					<p class="sec2b_2_title">Enfócate en lo que verdaderamente importa</p>
					<p class="sec2b_2_text">Deja de preocuparte por pensar en como llevar las cuentas de tus ventas, nosotros hacemos el trabajo por ti y tú te enfocas en generar valor a tu negocio.</p>

					<p class="sec2b_2_title">En cualquier dispositivo y plataforma</p>
					<p class="sec2b_2_text">Utiliza la herramienta desde cualquier dispositivo con acceso a internet. PC, Laptop, Ipad e incluso tu celular</p>
				</div>
				<a class="btn_style_2 bounceInUp" href="/php/register.php">EMPIEZA AHORA</a>
			</div>
		</div>

	</div>

	<div class="home-section-2 home-section-3">

		<div class="sec2a">
			<h1 class="title_1_font">Una solución para cada necesidad</h1>
		</div>

		<div class="sec2b">

			<div class="sec3b_1">

				<div class="sec3b_1_table_container">
					<div class="sec3b_1_itemtable">
						<img src="images/twoguys.svg?<?php echo time();?>" class="wow fadeIn" data-wow-duration="1s">
						<p>Mejora la relación con tus clientes y mantén tu <span>información en un solo lugar</span></p>
					</div>

					<div class="sec3b_1_itemtable">
						<img src="images/reportintablet.svg?<?php echo time();?>" class="wow fadeIn" data-wow-duration="2s">
						<p><span>Visualiza </span> tu información como nunca antes lo has hecho</p>
					</div>
				</div>

				<div class="sec3b_1_table_container">
					<div class="sec3b_1_itemtable">
						<img src="images/sendairplane.svg?<?php echo time();?>" class="wow fadeIn" data-wow-duration="3s">
						<p>Crea <span>comprobantes de tus ventas</span> y envialas a tus clientes directamente</p>
					</div>

					<div class="sec3b_1_itemtable">
						<img src="images/threestepssvg.svg?<?php echo time();?>" class="wow fadeIn" data-wow-duration="4s">
						<p>Interfaz intuitiva con registro de ventas en <span>solo 3 pasos</span></p>
					</div>
				</div>

			</div>

		</div>

	</div>	

	<div class="home-section-2 home-section-4">
			<div class="sec4_a">
				<div class="sec4_a_msg">
					<h1 class="title_1_font">TOTALMENTE <span>GRATIS</span></h1>
					<h2>SIN TARJETA DE CRÉDITO</h2>
				</div>
			</div>
	</div>


	<div class="foot_ready_start">

		<div class="foot_rs_a">
			<h1 class="title_1_font">¿LISTO PARA EMPEZAR?</h1>
		</div>

		<div class="foot_rs_b">
		<a class="btn_style_1 btnWhite" href="/php/register.php">REGÍSTRATE AHORA</a>
		</div>

	</div>

	<!-- PIE DE PAGINA -->
	<div class="home-section-footpage">
		<div class="foot_container">

			<div class="foot_cont_1">
				<div class="foot_cont_1_text">
					<h1>ACERCA DE</h1>
					<p class="link-pp"><a href="https://medium.com/blog-2luca/pol%C3%ADtica-de-privacidad-baec170a45e9" target="_blank">Política de privacidad</a></p>
					<p class="link-toc"><a href="https://medium.com/@2lucaco/t%C3%A9rminos-y-condiciones-9e4283b1d457" target="_blank">Términos de servicio</a></p>	
				</div>
			</div>

			<div class="foot_cont_2">
				<div class="foot_cont_1_text">
					<h1>RECURSOS</h1>
					<p class="click_login_foot" onclick="OpenLogInForm();">Iniciar Sesión</p>
					<p><a href="/php/register.php">Registrarse</a></p>	
					<p><a href="/php/analytics_home.php">Analítica</a></p>
					<p><a href="https://medium.com/blog-2luca" target="_blank">Ayuda</a></p>		
				</div>
			</div>

			<div class="foot_cont_3">
				<div class="foot_cont_3_logo">
					<img src="logos/logo8.png">
				</div>
				<p>Si tienes cualquier duda no dudes en contactarte a:</p>
				<p><a href="mailto:info@2luca.co">info@2luca.co</a></p>
			</div>
		</div>
	
		<div class="foot_social_netw">
			<a href="https://medium.com/blog-2luca" target="_blank"><img src="/images/social_medium_white.svg"></a>
			<a href="https://www.youtube.com/channel/UCKDmX9RgsSPg0eJCMItI5IA" target="_blank"><img src="/images/social_youtube_white.svg"></a>
			<a href="http://instagram.com/2lucaco/" target="_blank"><img src="/images/social_instagram_white.svg"></a>
			<a href="mailto:info@2luca.co"><img src="/images/social_email_white.svg"></a>
		</div>

		<div class="foot_last_band">
			<p>&reg 2LUCA.co   |   Bogotá, Colombia   |   <span id="currYear">1990</span></p>
		</div>

	</div>

	<!-- MODAL DE LOGIN -->
	<div class="msg-box-container" id="msg-box-container">
		<div class="msg-box-1" id="msg-box-1">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseLogInForm()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> INGRESAR</p>

			<form action="" method="POST" name="login-form" id="login-form">
				<input type="email" name="login-email" id="login-email" value="<?php if(isset($_COOKIE["rem_login_user"])) { echo $_COOKIE["rem_login_user"]; } ?>" placeholder="Email" required>
				<input type="password" name="login-pass" id="login-pass" placeholder="Contraseña" required>
				<p class="msg-box-1-forgotpass"><a href="/php/forgotpassword.php">¿Olvidaste tu contraseña?</a></p>
				<div class="checkboxCont"><input type="checkbox" name='login-rememberme' id="login-rememberme" <?php if(isset($_COOKIE["rem_remember"])) { ?> checked <?php } ?>> <label for="login-rememberme">Recordarme</label></div>
				
				<span class="simpleMsg" id="simpleMsgLogin"></span>
                <span class="errorMsg" id="errorMsgLogin"></span>
				<input type="submit" name="login-submit" id="login-submit" value="INGRESAR">
				<p class="msg-box-1-reg">¿No tienes cuenta aún? <a href="/php/register.php">Regístrate gratis</a></p>
			</form>

		</div>	
	</div>
	
</body>
</html>