<?php 
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$return_arr = Array();

	$user_id = $_SESSION['login_user'];
	$order_by_i = mysqli_real_escape_string($db, $_POST['order_by']);

	if($order_by_i==''){
		$order_by_i = 'prod_name';
	}

	$query = "SELECT A.*, (CASE WHEN B.cat_color IS NULL THEN '#FFF' ELSE B.cat_color END) AS cat_color\n"
	    . "FROM products A\n"
	    . "LEFT JOIN categories B\n"
	    . "ON A.bus_email = B.bus_email AND A.category = B.cat_name\n"
	    . "WHERE A.bus_email = '$user_id'\n"
	    . "ORDER BY A.$order_by_i";
	    
	$result = mysqli_query($db, $query); 

	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
	    array_push($return_arr,$row);
	}

	echo json_encode($return_arr);	
}



?>