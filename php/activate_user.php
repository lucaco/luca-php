<?php

include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);
date_default_timezone_set('America/Bogota');

if($_POST){
  
	$address   = mysqli_real_escape_string($db,$_POST["address"]);
	$country   = mysqli_real_escape_string($db,$_POST["country"]);
	$city      = mysqli_real_escape_string($db,$_POST["city"]);
	$phone     = mysqli_real_escape_string($db,$_POST["phone"]);
	$nit       = mysqli_real_escape_string($db,$_POST["nit"]);
	$bus_email = mysqli_real_escape_string($db,$_POST["email"]);
  $act_econ  = mysqli_real_escape_string($db,$_POST["act_econ"]);
  $hash      = mysqli_real_escape_string($db,$_POST["hash"]);
  $name      = mysqli_real_escape_string($db,$_POST["name"]);

  $todays_date    = date('Y-m-d H:i:s');
  
  $r = array();
	$query = "UPDATE register_users SET bus_address = '$address', bus_phone = '$phone', bus_nit = '$nit', bus_act_econ = '$act_econ', bus_country = '$country', bus_city = '$city', last_mod_date = '$todays_date' WHERE bus_email ='$bus_email' AND hash_act = '$hash'";
  mysqli_query($db,$query);
  
  if(mysqli_affected_rows($db)<=0){
    array_push($r, Array('E','No se pudo actualizar las datos.'));
  }else{

    // Revisar que el usuario existe
    $query  = "SELECT * FROM register_users WHERE bus_email = '$bus_email'";
    $result = mysqli_query($db,$query);
    $row    = mysqli_fetch_array($result,MYSQLI_ASSOC);

    $bus_name          	= $row['bus_name'];
    $pass_enc          	= $row['pass_enc'];
    $bus_industry      	= $row['bus_industry'];
    $creation_date     	= $row['creation_date'];
    $active            	= $row['active'];			// 1: User Active - 0: User Inactive
    $user_gone   				= $row['user_gone'];		// 1: User Not unregisted him/her self  - 0: User still Active
    $bus_address       	= $row['bus_address'];
    $bus_phone         	= $row['bus_phone'];
    $bus_nit           	= $row['bus_nit'];
    $bus_act_econ      	= $row['bus_act_econ'];
    $user_folder_name  	= $row['user_folder_name'];
    $avatar_type 		    = $row['avatar_type'];		// 0: Imagen - 1: Logo de Galeria
    $bus_country 		    = $row['bus_country'];
    $bus_city 			    = $row['bus_city'];
    $bus_email 			    = $bus_email;

    $count = mysqli_num_rows($result);

    if($count == 1) {

      // Activar cuenta
      $query = "UPDATE register_users SET active = '1' WHERE bus_email = '". $bus_email ."'";
      mysqli_query($db,$query);

      if(mysqli_affected_rows($db)<=0){
        array_push($r, Array('E','No se pudo activar la cuenta'));
      }else{

        SendMail_SMTP_SimpleMsg($bus_email,
        "Tu cuenta ha sido activada",
        $bus_name,
        "Te informamos que tu cuenta ha sido <b>activada correctamente</b>. <br><br> Tu correo electrónico se registró como:<br> <a style='color: #39ac95; text-decoration: none;' href='mailto:". $bus_email ."'> ". $bus_email ." </a><br><br> Si tienes alguna duda no dudes en contactarte con nosotros a través de nuestra <a style='color: #39ac95; text-decoration: none;' href='https://www.2luca.co'> página web </a> <br><br> Gracias, 
        <br><br> 2Luca - Colombia<br>Bogotá, Colombia <br> ");
        
        session_start();
        $_SESSION['login_user'] 			    = $bus_email;
        $_SESSION['login_bus_name'] 		  = $bus_name;
        $_SESSION['login_bus_industry'] 	= $bus_industry;
        $_SESSION['login_creation_date'] 	= $creation_date;
        $_SESSION['login_active'] 			  = '1';
        $_SESSION['bus_address']	 		    = $bus_address;
        $_SESSION['bus_phone'] 				    = $bus_phone;
        $_SESSION['bus_nit'] 				      = $bus_nit;
        $_SESSION['bus_act_econ'] 			  = $bus_act_econ;
        $_SESSION['user_folder_name'] 		= $user_folder_name;
        $_SESSION['avatar_type'] 			    = $avatar_type; 
        $_SESSION['bus_country'] 			    = $bus_country;
        $_SESSION['bus_city'] 				    = $bus_city;
            
        array_push($r, Array('S','1'));
      }

    }else{
      array_push($r, Array('E','No existe el email'));
    }
    
  }

  echo json_encode($r);

}


?>