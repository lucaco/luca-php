<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require("PHPMailer.php");
require("SMTP.php");
require("Exception.php");

// Fecha y hora y lugar de la IP
$user_ip = getenv('REMOTE_ADDR');
$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
$IP_country = $geo["geoplugin_countryName"];
$IP_city = $geo["geoplugin_city"];
$IP_timezone = $geo["geoplugin_timezone"];

// Current Local Time
//date_default_timezone_set($IP_timezone);
$TimeZone = 'America/Bogota';
date_default_timezone_set($TimeZone);
$IP_data_date = date('Y-m-d H:i:s', time());

// VARIABLES
$dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

// Arrays
$arr_type = array("No asignado"
                , "Régimen Común"
                , "Régimen Simplificado");

$arr_industry = array("No asignado"
                        ,"Agricultura"
                        ,"Alimentos y bebidas"
                        ,"Arte y entretenimiento"
                        ,"Artesanías"
                        ,"Bar"
                        ,"Construcción"
                        ,"Educación"
                        ,"Financiero"
                        ,"Hotelería y Turismo"
                        ,"Información"
                        ,"Mascotas y Animales"
                        ,"Producción Audiovisual"
                        ,"Restaurante"
                        ,"Retail"
                        ,"Retail Online"
                        ,"Ropa y moda"
                        ,"Salud"
                        ,"Seguros"
                        ,"Servicios Administrativos"
                        ,"Tecnología"
                        ,"Tendero"
                        ,"Transporte"
                        ,"Independiente"
                    );

// ALERT
function phpAlert($msg) {
    echo '<script type="text/javascript">alert("' . $msg . '")</script>';
}

// CONSOLE LOG
function phpConsole($msg) {
    echo '<script type="text/javascript">console.log("' . $msg . '")</script>';
}

// PHP Alertify
function phpAlertify_message($msg) {
    echo '<script type="text/javascript">alertify.message("' . $msg . '")</script>';
}
function phpAlertify_warning($msg) {
    echo '<script type="text/javascript">alertify.warning("' . $msg . '")</script>';
}
function phpAlertify_alert($title, $msg) {
    echo '<script type="text/javascript">alertify.alert("'. $title .'","' . $msg . '")</script>';
}

// Diferencia entre dos fechas (Resultado Ej: 2 meses, 1 dia, 8 horas y 45 minutos, 3 años ....)
function diff_dates_str($date_1,$date_2){
    $diff    = date_diff(date_create($date_1),date_create($date_2));
    $n_days  = $diff->format("%a");
    if($n_days <= 1){
        $user_time_creation = $diff->format("%h horas y %i minutos");
    }else if($n_days > 1 && $n_days <= 30){
        $user_time_creation = $diff->format("%a días");
    }else if($n_days > 30 && $n_days <= 60){
        $user_time_creation = $diff->format("%m mes");
    }else if($n_days > 60 && $n_days <= 365){
        $user_time_creation =  $diff->format("%m meses");
    }else if($n_days > 365 && $n_days <= 730){
        $user_time_creation =  $diff->format("%y año");
    }else if($n_days > 730){
        $user_time_creation =  $diff->format("%y años");
    }else{
        $user_time_creation = "0 días";
    }
    return $user_time_creation;
}

function SendMail_SMTP_SimpleMsg($to,$subject,$name_person,$msg_body){       
    
    $smtpHost = "smtp.1and1.com";  
    $smtpUser = "info@2luca.co"; 
    $smtpPass = "Panda100$";   
    
    $mail = new PHPMailer(); 

    // try {
    
        $mail->isSMTP();
        $mail->SMTPAuth=true;
        $mail->Port=587;
        $mail->SMTPSecure = 'tls';
        $mail->isHTML(true);
        $mail->CharSet = "utf-8";

        $mail->Host=$smtpHost;
        $mail->Username=$smtpUser;
        $mail->Password=$smtpPass;

        $mail->setFrom('info@2luca.co', 'Luca Colombia');
        $mail->addAddress($to);

        $mail->Subject = $subject;
        $mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
                        <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            </head>
                            <body style="background-color: #efefef; font-family: Arial, sans-serif; margin: 0; padding: 0;">

                            <table style="border-collapse: collapse; max-width: 650px;" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" align="center">

                                <tr>
                                <td align="left" style="padding-top: 65px; padding-bottom: 50px; background: #fff; background: linear-gradient(to bottom, rgba(0,0,0,0.15), rgba(255,255,255,0));">
                                    <img style="width:150px; padding-left:40px;" src="https://www.2luca.co/logos/logo5.png" alt="LUCA.co">
                                </td>
                                </tr>

                                <tr>
                                <td style="padding: 10px 40px 80px 40px;">
                                    <h1 style="color:#191919; font-size:20px; margin-bottom: 30px;">Hola '. $name_person .'!</h1>
                                    <p style="color:#666666; font-size:15px;">'. $msg_body .'</p>
                                </td>
                                </tr>

                                <tr>
                                <td align="center" style="padding: 30px 0;">
                                    <a href="https://www.facebook.com/2Luca-113522456692963/"><img src="https://www.2luca.co/images/facebook_logo.png" style="width: 40px; margin-right: 10px;" alt="FACEBOOK"></a>
                                    <a href="https://www.instagram.com/2lucaco"><img src="https://www.2luca.co/images/instagram_logo.png" style="width: 40px; margin-right: 10px;" alt="INSTAGRAM"></a>
                                    <a href="https://www.twitter.com/2luca1"><img src="https://www.2luca.co/images/twitter_logo.png" style="width: 40px; margin-right: 10px;" alt="TWITTER"></a>
                                    <a href="mailto:info@2luca.co"><img src="https://www.2luca.co/images/email_logo.png" style="width: 40px; margin-right: 10px;" alt="EMAIL"></a>
                                </td>
                                </tr>

                                <tr>
                                <td align="right" style="background-color: #575757; height: 20px; color:#efefef; font-size: 13px; padding: 5px 10px;">
                                    <p>&reg; 2Luca</p>
                                </td>
                                </tr>

                                <tr>
                                <td align="center" style="color: #888; font-size: 13px; padding: 20px 40px; " bgcolor="#efefef">
                                    <p style="margin: 7px 0;">Bogotá D.C., Colombia</p>
                                    <p style="margin: 7px 0; margin-bottom: 20px;">&reg; Luca '.date("Y").'</p>
                                    <p style="margin: 7px 0;">Si tienes alguna duda o quieres contacterte con nosotros, envíanos un mensaje a <a style="text-decoration: none; color: #262626;" href="mailto:info@2luca.co">info@2luca.co</a></p>                                    
                                    <p style="margin: 7px 0;">Este es un mensaje informativo enviado a <a style="text-decoration: underline; color: #888;" href="">'. $to .'</a></p>
                                    <p style="margin: 20px 0;"><a style="text-decoration: underline; color: #888;" href="https://medium.com/@2lucaco/t%C3%A9rminos-y-condiciones-9e4283b1d457">Términos y condiciones</a> / <a style="text-decoration: underline; color: #888;" href="https://medium.com/blog-2luca/pol%C3%ADtica-de-privacidad-baec170a45e9">Política de Privacidad</a></p>
                                </td>
                                </tr>

                            </table>

                            </body>
                        </html>';
        $mail->AltBody = "Tu correo no soporta versiones HTML \n\n";

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $stdSend = $mail->send();
    
    if($stdSend){
        return true;
    }else{
        return false;
    }
}

// **********************************************************
// ****************** SEND INVOICE EMAIL *******************
// **********************************************************

function SendMail_SMTP_InvoiceEmail($to,$subject,$name_person,$bus_name,$country,$jsonBill,$num_points_tot,$cp_name,$cp_avatar,$client_based_recom,$cust_key,$id_bill){       

    session_start();
    $bus_email      = $_SESSION['login_user'];
    $points_system  = 700; // 1 point for each 700$ purshased
    
    // DECIMAL NUMBERS 
    if($country == 'Colombia' || $country == 'Vietnam'){
        $DecNum = 0;
    }else{
        $DecNum = 1;
    }

    // PAYMENT TYPE
    if($jsonBill["paymentType"] == 1){
        $payType = "Efectivo";
    }elseif($jsonBill["paymentType"] == 2){
        $payType = "Tarjeta de Crédito";
    }elseif($jsonBill["paymentType"] == 3){
        $payType = "Tarjeta Débito";
    }elseif($jsonBill["paymentType"] == 4){
        $payType = "Otro - ".$jsonBill["otherPaymentOption"];
    }else{
        $payType = "No encontrado";
    }

    // POINT REDEMPTION
    if($jsonBill["PointsRedem"] > 0){
        $point_redem = '<p style="line-height: normal;color:#999999; font-size: 15px; margin-top: 20px">✓ Redimiste '.$jsonBill["PointsRedem"].' puntos en esta compra</p>';
    }
    
    // POINT LUCA OF THIS PURCHASE
    $trx_value = $jsonBill["super_total_bill"];
    $points = floor($trx_value/$points_system);

    // RECOMENDADO PRODUCTO
    $html_recom_prod = '';
    if(count($client_based_recom)>0){
        $html_recom_prod = $html_recom_prod . '<tr><td style="padding: 10px 20px 10px 20px;">';
        $html_recom_prod = $html_recom_prod . '<div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 25px; margin-bottom: 5px;"></div>';
        $html_recom_prod = $html_recom_prod . '<div style="position:relative;">';
        $html_recom_prod = $html_recom_prod . '<p style="color:#999999; font-size: 15px; margin-bottom: 20px;">✰ Recomendado para tu próxima compra en '.$bus_name.': </p>';
        $html_recom_prod = $html_recom_prod . '<div style="border: 1px solid #ccc;padding: 15px 3px;border-radius: 10px;width: 100%;">';
        $html_recom_prod = $html_recom_prod . '<table style="width: 100%"><tbody><tr>';
        $html_recom_prod = $html_recom_prod . '<td style="width:30%; text-align: center; padding: 0 10px; box-sizing: border-box;">';
        $html_recom_prod = $html_recom_prod . '<img style="max-width: 45px;" src="'.$client_based_recom[1]["prod_icn"].'?'.time().'">';
        $html_recom_prod = $html_recom_prod . '</td><td style="vertical-align: middle; width:70%;">';
        $html_recom_prod = $html_recom_prod . '<div><p style="line-height: normal;font-size: 15px;letter-spacing: 2px; display: inline;color:#333333;">'.$client_based_recom[0]["prod_name"].'</p></div>';
        $html_recom_prod = $html_recom_prod . '</td></tr></tbody></table></div></div></td></tr>';                                        
    }
    
    // CREATE ITEM LIST
    $ItemsList = '';
    foreach($jsonBill['ItemsInBill'] as $item){
        $ItemsList = $ItemsList . '<tr><td style="padding: 15px 20px 25px 20px; line-height: 5px">';
        $ItemsList = $ItemsList . '<img style="float:left; width:45px; display: inline;margin-right: 20px;" src="'.$item["itemIconHTTP"].'?'.time().'" alt="Img">';
        $ItemsList = $ItemsList . '<div style="text-align: right;">';
        $ItemsList = $ItemsList . '<p style="line-height:normal;color:#191919; font-size: 15px;">'.$item["itemName"].'<p>';
        $ItemsList = $ItemsList . '<p style="color:#999999; font-size: 13px;">'.$item["itemCount"].' '.$item["itemUnits"].'</p>';
        $ItemsList = $ItemsList . '<p style="color:#191919; font-size: 17px; font-weight: bold;">$ '.$item["itemAmountStyle"].'</p>';
        $ItemsList = $ItemsList . '</div></td></tr>';
    }

    $todays_date    = date('Y-m-d H:i:s');
    $smtpHost = "smtp.1and1.com";  
    $smtpUser = "info@2luca.co"; 
    $smtpPass = "Panda100$";   
    
    $mail = new PHPMailer(); 
    
        $mail->isSMTP();
        $mail->SMTPAuth=true;
        $mail->Port=587;
        $mail->SMTPSecure = 'tls';
        $mail->isHTML(true);
        $mail->CharSet = "utf-8";

        $mail->Host=$smtpHost;
        $mail->Username=$smtpUser;
        $mail->Password=$smtpPass;

        $mail->setFrom('info@2luca.co', 'Luca Colombia');
        $mail->addAddress($to);

        $mail->Subject = $subject;
        $mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
                            <html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            </head>
                            <body style="background-color: #efefef; font-family: Arial, sans-serif; margin: 0; padding: 15px;">

                                <table style="border-collapse: collapse; max-width: 450px;" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" align="center">

                                <tr>
                                    <td style="padding: 30px 20px 20px 20px;">
                                        <p style="color:#191919; font-size:23px; font-weight: bold">Hola '.$name_person.'!,</p>
                                        <p style="line-height: normal; text-align: justify; color:#666666; font-size:19px;">Gracias por comprar en <b>'.$bus_name.'</b>. Aquí te enviamos tu recibo de compra.</p>
                                        <div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 5px; margin-bottom: 5px;"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 10px 20px 10px 20px;">
                                        <div style="position:relative; margin-bottom: 30px;">
                                            <img style="float: left; width:50px; display: inline;margin-right: 20px;" src="https://www.2luca.co/icons/general/basket-checkout.png">
                                            <div>
                                                <p style="display:inline-block;color:#191919;margin-right: 10px;">TOTAL:</p><h1 style="display: inline;color:#00bb7d;">$ '.number_format($jsonBill["super_total_bill"],$DecNum).'</h1>
                                            </div>
                                        </div>
                                        <div style="text-align: left; line-height: 12px;">
                                            <p style="color:#999999; font-size: 15px; margin-top: 15px;">Método de pago: '.$payType.'</p>
                                            <p style="color:#999999; font-size: 15px;">Número de ítems: '.$jsonBill["total_items_bill"].'</p>
                                            <p style="color:#999999; font-size: 15px;">Código de cuenta: '.$id_bill.'</p>
                                        </div>
                                        <div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 25px; margin-bottom: 5px;"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td  style="padding: 20px 20px 35px 20px; line-height: 5px;">
                                        <p style="line-height: normal; color:#191919; font-size:23px; text-align: center; font-weight: bold">RESUMEN DE TU COMPRA</p>
                                        <p style="line-height: normal; color:#999999; font-size:15px; text-align: center;">del '.$todays_date.' en '.$country.'</p>
                                    </td>
                                </tr>

                                '.$ItemsList.'

                                <tr>
                                    <td style="padding: 0px 20px 45px 20px;">
                                        <div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 25px; margin-bottom: 5px;"></div>
                                        <div style="padding-right: 10px; color:#555555; text-align: right; font-size: 15px; line-height: 20px;">
                                            <p>Impuestos: $ '.number_format($jsonBill["tax_bill"],$DecNum).'</p>
                                            <p>SUBTOTAL: $ '.number_format($jsonBill["subtotal_bill"],$DecNum).'</p>
                                            <p>Descuento: $ '.number_format($jsonBill["discount_bill"],$DecNum).'</p>
                                            <p>Propina: $ '.number_format($jsonBill["tips_bill"],$DecNum).'</p>
                                            <p style="font-size: 17px; color:#191919;">TOTAL: <b>$ '.number_format($jsonBill["super_total_bill"],$DecNum).'</b></p>
                                        </div>
                                    </td>
                                </tr>

                                <tr style="background: linear-gradient(to left, #00bb7d, #00b4ad);">
                                    <td style="padding: 25px 20px 25px 20px;">
                                        <div style="text-align: center;">
                                            <p style="line-height: normal;color:#ffff; font-size: 13px;">CON ESTA COMPRA ACUMULASTE:</p>
                                            <p style="line-height: normal;color:#ffff; font-size: 27.5px; font-weight: bold;">'.$points.' PUNTOS</p>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 25px 20px 25px 20px; line-height: 10px; text-align: left;">
                                        '.$point_redem.'
                                        <p style="line-height: normal; color:#999999; font-size: 15px; margin-top: 20px">✓ Tienes acumulado a la fecha: '.($num_points_tot).' puntos</p>
                                        <div style="text-align: center;">
                                            <a style="line-height: normal; color:#00bb7d; font-size: 13px;" href="https://www.2luca.co/php/customerview.php?ckey='.$cust_key.'">Da click aquí para ver tu historial en Luca</a>
                                        </div>
                                    </td>
                                </tr>

                                '.$html_recom_prod.'

                                <tr>
                                    <td style="padding: 10px 20px 10px 20px;">
                                        <div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 25px; margin-bottom: 5px;"></div>
                                        <div style="position:relative;">
                                            <p style="color:#999999; font-size: 15px;">Hoy te atendió: </p>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:30%; text-align: center; padding: 0 10px; box-sizing: border-box;">
                                                            <img style="width:62px;" src="'.$cp_avatar.'" alt="IMG">
                                                        </td>
                                                        <td style="vertical-align: middle; width:70%;">
                                                            <div>
                                                                <p style="line-height: normal;font-size: 19px;letter-spacing: 2px; display: inline;color:#333333;margin-right: 10px;">'.$cp_name.'</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                
                                <tr> 
                                    <td>
                                        <div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-top: 25px; margin-bottom: 5px;"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" style="padding: 30px 0;">
                                        <a href="https://www.facebook.com/"><img src="https://www.2luca.co/images/facebook_logo.png" style="width: 40px; margin-right: 10px;" alt="FACEBOOK"></a>
                                        <a href="https://www.instagram.com/2lucaco"><img src="https://www.2luca.co/images/instagram_logo.png" style="width: 40px; margin-right: 10px;" alt="INSTAGRAM"></a>
                                        <a href="https://www.twitter.com/"><img src="https://www.2luca.co/images/twitter_logo.png" style="width: 40px; margin-right: 10px;" alt="TWITTER"></a>
                                        <a href="mailto:info@2luca.co"><img src="https://www.2luca.co/images/email_logo.png" style="width: 40px; margin-right: 10px;" alt="EMAIL"></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="right" style="background-color: #575757; height: 20px; color:#efefef; font-size: 13px; padding: 5px 10px;">
                                        <p> </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" style="color: #888; font-size: 13px; padding: 20px 40px; " bgcolor="#efefef">
                                    <p style="margin: 7px 0;">Bogotá D.C., Colombia</p>
                                    <p style="margin: 7px 0; margin-bottom: 20px;">&reg; Luca '.date("Y").'</p>
                                    <p style="margin: 7px 0;">Has recibido este correo porque estás suscrito a la empresa '.$bus_name.' a través del servicio de ventas de LUCA COLOMBIA</p>
                                    <p style="margin: 7px 0;">Este email es un comprobante de compra y NO CORRESPONDE A UNA FACTURA. Para solicitar la factura necesitamos que nos envíes tus datos de facturación a: <a style="text-decoration: none; color: #262626;" href="mailto:info@2luca.co">info@2luca.co</a></p>
                                    <p style="margin: 20px 0;"><a style="text-decoration: underline; color: #888;" href="https://medium.com/@2lucaco/t%C3%A9rminos-y-condiciones-9e4283b1d457">Términos y condiciones</a> / <a style="text-decoration: underline; color: #888;" href="https://medium.com/blog-2luca/pol%C3%ADtica-de-privacidad-baec170a45e9">Política de Privacidad</a></p>
                                    </td>
                                </tr>

                                </table>

                            </body>
                            </html>';
        $mail->AltBody = "Tu correo no soporta versiones HTML \n\n";

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $stdSend = $mail->send();
    
    if($stdSend){
        return true;
    }else{
        return false;
    }
}

// Funcion pra generar strings aleatorios
function generateRandomString($len = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $len; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Funcion pra generar numeros aleatorios
function generateNumber($len = 3) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $len; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Funcion que genera el nombre de la carpeta del usuario apertir del correo EJ: juancadh@gmail.com = juancadh_gmail_com
function splitEmail($email){
    $a1 = explode("@", $email);
    $username = $a1[0];
    $full_folder = $username;
    $dom = explode(".", $a1[1]);
    foreach( $dom as $d ) {
        $full_folder.="_".$d;
    }

    return $full_folder;
}

// ********** Get All Variables of session user if user in assosiative array ***********
// Call ---> $userData = getSessVariables();  $userData['bus_name'];
function getSessVariables(){
    include("config.php");
    session_start();
    $email = $_SESSION['login_user'];
    $query = "SELECT A.*, IFNULL(B.reg_name,'') AS reg_name, IFNULL(C.ind_name,'') AS ind_name, IFNULL(D.resolution,'') AS resolution, IFNULL(D.date_resolution,'') AS date_resol, IFNULL(D.range_reso_ini,'') AS range_reso_ini, IFNULL(D.range_reso_last,'') AS range_reso_last, IFNULL(E.country_name,'') AS country_name, IFNULL(E.country_flag,'') AS country_flag  FROM register_users A LEFT JOIN regime_type B ON A.bus_act_econ = B.reg_id LEFT JOIN industry_type C ON A.bus_industry = C.ind_id LEFT JOIN (SELECT * FROM resolutions WHERE current_resolution = '1') D ON A.bus_email = D.bus_email LEFT JOIN countries E ON A.bus_country = E.country_id WHERE A.bus_email = '". $email ."'";
    $result = mysqli_query($db,$query);
    $user_data_assc = mysqli_fetch_array($result,MYSQLI_ASSOC);
    return $user_data_assc;
}

// ********* Delete Folder by dirname path ***********
function delete_directory($dirname) {
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname."/".$file))
                unlink($dirname."/".$file);
            else
                delete_directory($dirname.'/'.$file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}


// ********* DELETE USER **********
// Call -----> deleteUser("frikisabig@hs130.com", true); ---> true if you want to delete user folder to 
// Output: True if deleted ok, false if not succeded deleting user
function deleteUser($email, $delete_folder){

    if($delete_folder == true){
        $dirname = "user_uploads/".splitEmail($email)."/";
        delete_directory($dirname);
    }

    include("config.php");

    $query = "UPDATE register_users SET user_gone = 1 WHERE bus_email = '". $email ."'";
    mysqli_query($db,$query);
    
    return true;
}

function recreateUser($email){

    $user_folder = splitEmail($email);
    $destination_folder = "user_uploads/".$user_folder;

    if (!file_exists($destination_folder)) {
        mkdir($destination_folder, 0777, true);

        $query = "UPDATE register_users SET user_folder_name = TRIM('".$user_folder."') WHERE bus_email = '". $email ."'";
        mysqli_query($db,$query);

    }

    include("config.php");

    $query = "UPDATE register_users SET user_gone = 0 WHERE bus_email = '". $email ."'";
    mysqli_query($db,$query);

    return true;
}

// ::::::::::::::::: CODIGO DE SANTIAGO :::::::::::::::::::::
function getUserTransactions($email){
    include("config.php");
    $query   = "SELECT t.trx_date, t.trx_value, t.num_items, t.cust_key, i.prod_name, b.item_count,  IFNULL(c.cust_email,'') AS cust_email, IFNULL(c.cust_bdate,'') AS cust_bdate, IFNULL(c.cust_gender,'') AS cust_gender FROM transactions t JOIN bills b ON b.id_bill = t.id_bill AND b.bus_email = t.bus_email JOIN products i ON b.item_id = i.prod_id AND b.bus_email = i.bus_email LEFT JOIN customers c ON t.cust_key = c.cust_key WHERE t.bus_email = '". $email ."' AND t.state = 1;";
    $results = mysqli_query($db, $query);
    $ret = array();
    while($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
        array_push($ret, $row);
    }
    return $ret;
}




// ***************************************************************
// ****************** SEND SUMMARY STATS EMAIL *******************
// ***************************************************************

function SendMail_SMTP_Summary_Mail($to,$subject,$frequency,$data,$txtDateRange){
    
    $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $txt_frq_1 = '';
    if($frequency == 'd'){
        $x = strtotime("-1 days");
        $tm_done =  $dias[date('w',$x)].", ".date('d',$x)." de ".$meses[date('n',$x)-1]. " del ".date('Y',$x) ;
        $txt_frq_1 = 'del día de ayer '.$tm_done;
    }elseif($frequency == 'w'){
        $txt_frq_1 = 'de la semana pasada';
    }elseif($frequency == 'm'){
        $x = strtotime("-1 months");
        $txt_frq_1 = 'del mes de '.$meses[date('n',$x)-1];
    }elseif($frequency == 'y'){
        $x = strtotime("-1 years");
        $txt_frq_1 = 'del año '.date('Y',$x);
    }else{
        $txt_frq_1 = '';
    }

    $bus_name      = $data[0][0]["bus_name"];
    $total_ventas  = $data[0][0]["NUM_VENTAS"];
    $valor_ventas  = $data[0][0]["TOT_ING"];
    $valor_ventas  = number_format($valor_ventas);
    $item_id       = $data[1][0]["ITEM_ID"];
    $item_name     = $data[1][0]["ITEM_NAME"];
    $item_icon     = $data[1][0]["ITEM_ICON"];
    $item_cant     = $data[1][0]["ITEM_CANT"];
    $vend_name     = $data[2][0]["VEND_NAME"];
    $vend_avatar   = $data[2][0]["VEND_AVATAR"];
    $vend_sales    = $data[2][0]["ITEM_CANT"];
    $num_cust_all  = $data[3][0]["NUM_CUSTS_ALL"];
    $num_new_custs = $data[3][0]["NUM_NEW_CUSTS"];


    //***** CODIGO MEJOR ITEM

    $html_item_best_sale = '';
    if($item_name != '' && $item_cant>0){

        if($item_icon!=''){
            $item_icon = "https://www.2luca.co".str_replace("..","",$item_icon);
        }

        $html_item_best_sale = '<tr>
                <td>
                    <div style="padding: 5px 15px; box-sizing: border-box;">
                        <p style="font-weight: 600; color: #333; letter-spacing: 1px; font-size: 14px; padding-bottom: 5px; border-bottom: 1px solid #dddddd;">ITEM MÁS VENDIDO</p>
                        <div style="display: table; position: relative; width: 100%; text-align: center; margin: 20px 0px;">
                            <div style="display: table-cell; width: 30%; vertical-align: middle; text-align: right; padding: 5px;"><img style="width: 50px;" alt="Img" src="'.$item_icon.'"></div>
                            <div style="display: table-cell; width: 70%; vertical-align: middle; text-align: center;">
                                <div style="font-size: 17px; color:#333333; padding-bottom: 5px;">'.$item_name.'</div>
                                <div style="font-size: 14px; color:#888888;">'.$item_cant.' ítems vendidos</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>';
    }


    //***** CODIGO MEJOR VENDEDOR

    $html_vend_best = '';
    if($vend_name != '' && $vend_sales>0){

        if($vend_avatar!=''){
            $vend_avatar = "https://www.2luca.co".str_replace("..","",$vend_avatar);
        }

        $html_vend_best = '<tr>
                <td style="margin-bottom: 30px;">
                    <div style="padding: 5px 15px; box-sizing: border-box;">
                        <p style="font-weight: 600; color: #333; letter-spacing: 1px; font-size: 14px; padding-bottom: 5px; border-bottom: 1px solid #dddddd;">VENDEDOR ESTRELLA</p>
                        <div style="display: table; position: relative; width: 100%; text-align: center; margin: 20px 0px;">
                            <div style="display: table-cell; width: 30%; vertical-align: middle; text-align: right; padding: 5px;"><img style="max-width: 60px;" alt="Img" src="'.$vend_avatar.'"></div>
                            <div style="display: table-cell; width: 70%; vertical-align: middle; text-align: center;">
                                <div style="font-size: 17px; color:#333333; padding-bottom: 5px;">'.$vend_name.'</div>
                                <div style="font-size: 14px; color:#888888;">'.$vend_sales.' ventas</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>';
    }


    $smtpHost = "smtp.1and1.com";  
    $smtpUser = "info@2luca.co"; 
    $smtpPass = "Panda100$";   
    
    $mail = new PHPMailer(); 

    $mail->isSMTP();
    $mail->SMTPAuth=true;
    $mail->Port=587;
    $mail->SMTPSecure = 'tls';
    $mail->isHTML(true);
    $mail->CharSet = "utf-8";

    $mail->Host=$smtpHost;
    $mail->Username=$smtpUser;
    $mail->Password=$smtpPass;

    $mail->setFrom('info@2luca.co', 'Luca Colombia');
    $mail->addAddress($to);

    $mail->Subject = $subject;
    $mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        </head>
        <body style="background-color: #efefef; font-family: Arial, sans-serif; margin: 0; padding: 0;">

            <table style="border-collapse: collapse; max-width: 650px; overflow: hidden;" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%" align="center">


            <tr>
                <td style="padding: 50px 15px 20px 15px;">
                <h1 style="color:#191919; font-size:20px; margin-bottom: 30px;">Hola '. $bus_name .'</h1>
                <p style="color:#666666; font-size:15px;">Aquí está el reporte <b>'.$txt_frq_1.'</b>. Recuerda que puedes cambiar las preferencias de envío de reportes en la configuración de tu cuenta.</p>
                </td>
            </tr>
            
            <tr>
                <td>
                    <div style="display: table; position: relative; width: 100%; text-align: center; margin: 30px 10px;">
                        <div style="display: table-cell; width: 50%; vertical-align: middle; border-right: 1px solid #dddddd; padding: 5px;">	
                            <div style="font-size: 15px; color: #888888; padding-bottom: 10px;">VENTAS</div>
                            <div style="font-size: 35px; color: #333333;">'.$total_ventas.'</div>
                        </div>
                        <div style="display: table-cell; width: 50%; vertical-align: middle; padding: 5px;">	
                            <div style="font-size: 15px; color: #888888; padding-bottom: 10px;">INGRESOS</div>
                            <div style="font-size: 30px; color: #333333;">$'.$valor_ventas.'</div>
                        </div>
                    </div>
                </td>
            </tr>

            '.$html_item_best_sale.'
            
            '.$html_vend_best.'
            
            <tr>
                <td style="padding: 0px 15px;">
                    <div style="font-weight: 600; color: #333; letter-spacing: 1px; font-size: 14px; padding-bottom: 5px; border-bottom: 1px solid #dddddd;">CLIENTES</div>
                    <div style="display: table; position: relative; width: 100%; text-align: center; margin: 20px 10px;">
                        <div style="display: table-cell; width: 50%; vertical-align: middle; border-right: 1px solid #dddddd; padding: 5px;">	
                            <div style="font-size: 35px; color: #333333;">'.$num_new_custs.'</div>
                            <div style="font-size: 15px; color: #888888; padding-top: 10px;">CLIENTES NUEVOS</div>
                        </div>
                        <div style="display: table-cell; width: 50%; vertical-align: middle; padding: 5px;">	
                            <div style="font-size: 30px; color: #333333;">'.$num_cust_all.'</div>
                            <div style="font-size: 15px; color: #888888; padding-top: 10px;">TOTAL CLIENTES</div>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td align="center" style="padding: 30px 0;">
                <a href="https://www.facebook.com/"><img src="https://www.2luca.co/images/facebook_logo.png" style="width: 40px; margin-right: 10px;" alt="FACEBOOK"></a>
                <a href="https://www.instagram.com/2lucaco"><img src="https://www.2luca.co/images/instagram_logo.png" style="width: 40px; margin-right: 10px;" alt="INSTAGRAM"></a>
                <a href="https://www.twitter.com/"><img src="https://www.2luca.co/images/twitter_logo.png" style="width: 40px; margin-right: 10px;" alt="TWITTER"></a>
                <a href="mailto:info@2luca.co"><img src="https://www.2luca.co/images/email_logo.png" style="width: 40px; margin-right: 10px;" alt="EMAIL"></a>
                </td>
            </tr>

            <tr>
                <td align="right" style="background-color: #575757; height: 20px; color:#efefef; font-size: 13px; padding: 5px 10px;">
                <p>&reg; Luca</p>
                </td>
            </tr>

            <tr>
                <td align="center" style="color: #888; font-size: 13px; padding: 20px 40px; " bgcolor="#efefef">
                <p style="margin: 7px 0;">Bogotá D.C., Colombia</p>
                <p style="margin: 7px 0; margin-bottom: 20px;">&reg; Luca '.date("Y").'</p>
                <p style="margin: 7px 0;">Si tienes alguna duda o quieres contacterte con nosotros, envíanos un mensaje a <a style="text-decoration: none; color: #262626;" href="mailto:info@2luca.co">info@2luca.co</a></p>
                <p style="margin: 7px 0;">Has recibido este correo porque estás suscrito los reportes Luca. '.$txtDateRange.'</p>
                <p style="margin: 7px 0;">Este es un mensaje informativo enviado a <a style="text-decoration: underline; color: #888;" href="">'. $to .'</a></p>
                </td>
            </tr>

            </table>

        </body>
        </html>';
    $mail->AltBody = "Tu correo no soporta versiones HTML \n\n";

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    $stdSend = $mail->send();
    
    if($stdSend){
        return true;
    }else{
        return false;
    } 
}

// ==================================================================
// CREAR REPORTE PERIORICO DE ESTADITICAS DEL NEGOCIO Y ENVIAR CORREOS
// A LAS PERSONAS QUE ESTEN REGISTRADAS A ESE SERVICIO
// ===================================================================
function SummaryReportEmail($frequency){

    include("config.php");

    /*** 
    * Frequency puede ser: 
    *             'd' => Daily
    *             'w' => Weekly
    *             'm' => Monthly
    *             'y' => Yearly
    ***/

    date_default_timezone_set('America/Bogota');

    if($frequency=='d'){
        // El reporte del día se envía el día siguiente y se toma la fecha de ayer
        $date_from = date("Y-m-d", strtotime("-1 days"));
        $date_to   = date("Y-m-d", strtotime("-1 days"));
        $indFreq   = "sum_mail_d";
        $txt_fq_1  = 'diario';
    }elseif($frequency=='w'){
        // El reporte de la semana se envía todos los lunes a primera hora y es de la semana anterior (de ln a dom)
        $date_from = date("Y-m-d", strtotime("-1 weeks"));
        $date_to   = date("Y-m-d", strtotime("-1 days"));
        $indFreq   = "sum_mail_w";
        $txt_fq_1  = 'semanal';
    }elseif ($frequency=='m') {
        // El reporte mensual se envía todos los primeros de cada mes y va desde el primero del mes anterior hasta el fin de mes
        $date_from = date("Y-m-d", strtotime("-1 months"));
        $date_to   = date("Y-m-d", strtotime("-1 days"));
        $indFreq   = "sum_mail_m";
        $txt_fq_1  = 'mensual';
    }elseif ($frequency=='y') {
        // El reporte anual se envía el primero de cada año y va desde el 1 de año anterior hasta el 31 dic del anterior
        $date_from = date("Y-m-d", strtotime("-1 years"));
        $date_to   = date("Y-m-d", strtotime("-1 days"));
        $indFreq   = "sum_mail_y";
        $txt_fq_1  = 'anual';
    }else{
        exit();
    }

    // LIST OF EMAILS REGISTERES TO REPORT MAIL
    $query = "SELECT bus_email, bus_name FROM register_users WHERE ".$indFreq." = 1 AND user_gone = 0;";
    $result = mysqli_query($db,$query);
    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {

        $bus_name  = $row['bus_name'];
        $bus_email = $row['bus_email'];
        $to        = $row['bus_email'];
        $subject   = '📋 Reporte '.$txt_fq_1.' de tu negocio';

        $stats_array = Array();
        $best_item_array = Array();
        $best_vend_array = Array();
        $cust_array = Array();

        $data = Array();

        $query1 =  "SELECT A.*, IFNULL(COUNT(B.trx_id),0) AS NUM_VENTAS, IFNULL(SUM(B.trx_value),0) AS TOT_ING FROM (SELECT bus_email, bus_name FROM register_users WHERE bus_email = '$bus_email') A LEFT JOIN ( SELECT bus_email, trx_id, trx_value FROM transactions WHERE state = 1 AND DATE_FORMAT(trx_date, '%Y-%m-%d') BETWEEN '$date_from' AND '$date_to') B ON A.bus_email = B.bus_email GROUP BY A.bus_email LIMIT 1;";
        $result1 = mysqli_query($db,$query1);
        while ($row1 = mysqli_fetch_array($result1,MYSQLI_ASSOC)) {
            array_push($stats_array,$row1);
        }

        $query2 = " SELECT C.*, IFNULL(D.item_id,'') AS ITEM_ID, IFNULL(E.prod_name,'') AS ITEM_NAME, IFNULL(E.prod_icon,'') AS ITEM_ICON, IFNULL(D.MAX_NUM_ITEMS, 0) AS ITEM_CANT FROM( SELECT bus_email, bus_name FROM register_users WHERE bus_email = '$bus_email') C LEFT JOIN ( SELECT A.*, B.item_id FROM ( SELECT bus_email, MAX(NUM_ITEMS) AS MAX_NUM_ITEMS FROM ( SELECT bus_email, item_id, SUM(item_count) as NUM_ITEMS FROM ( SELECT X1.*, X2.trx_date, X2.state FROM bills X1 INNER JOIN ( SELECT bus_email, id_bill, state, trx_date FROM transactions WHERE DATE_FORMAT(trx_date, '%Y-%m-%d') BETWEEN '$date_from' AND '$date_to' AND state = 1 ) X2 ON X1.bus_email = X2.bus_email AND X1.id_bill = X2.id_bill ) A2 GROUP BY bus_email, item_id ) A1 GROUP BY bus_email ) A LEFT JOIN ( SELECT bus_email, item_id, SUM(item_count) as NUM_ITEMS FROM ( SELECT X1.*, X2.trx_date, X2.state FROM bills X1 INNER JOIN ( SELECT bus_email, id_bill, state, trx_date FROM transactions WHERE DATE_FORMAT(trx_date, '%Y-%m-%d') BETWEEN '$date_from' AND '$date_to' AND state = 1 ) X2 ON X1.bus_email = X2.bus_email AND X1.id_bill = X2.id_bill ) A2 GROUP BY bus_email, item_id ) B ON A.bus_email = B.bus_email AND A.MAX_NUM_ITEMS = B.NUM_ITEMS ) D ON C.bus_email = D.bus_email LEFT JOIN ( SELECT bus_email, prod_id, prod_name, prod_icon FROM products ) E ON C.bus_email = E.bus_email AND D.item_id = E.prod_id LIMIT 1; ";
        $result2 = mysqli_query($db,$query2);
        while ($row2 = mysqli_fetch_array($result2,MYSQLI_ASSOC)) {
            array_push($best_item_array,$row2);
        }

        $query3 = "SELECT C.*, IFNULL(E.prof_name,'') AS VEND_NAME, IFNULL(E.avatar,'') AS VEND_AVATAR, IFNULL(D.MAX_NUM_SALES, 0) AS ITEM_CANT FROM( SELECT bus_email, bus_name FROM register_users WHERE bus_email = '$bus_email') C LEFT JOIN ( SELECT A.*, B.profsessid FROM ( SELECT bus_email, MAX(NUM_SALES) AS MAX_NUM_SALES FROM ( SELECT bus_email, profsessid, COUNT(trx_id) as NUM_SALES FROM transactions WHERE DATE_FORMAT(trx_date, '%Y-%m-%d') BETWEEN '$date_from' AND '$date_to' AND state = 1 GROUP BY bus_email, profsessid ) A1 GROUP BY bus_email ) A LEFT JOIN ( SELECT bus_email, profsessid, COUNT(trx_id) as NUM_SALES FROM transactions WHERE DATE_FORMAT(trx_date, '%Y-%m-%d') BETWEEN '$date_from' AND '$date_to' AND state = 1 GROUP BY bus_email, profsessid ) B ON A.bus_email = B.bus_email AND A.MAX_NUM_SALES = B.NUM_SALES ) D ON C.bus_email = D.bus_email LEFT JOIN ( SELECT bus_email, profsessid, prof_name, avatar FROM profiles ) E ON C.bus_email = E.bus_email AND D.profsessid = E.profsessid LIMIT 1;";
        $result3 = mysqli_query($db,$query3);
        while ($row3 = mysqli_fetch_array($result3,MYSQLI_ASSOC)) {
            array_push($best_vend_array,$row3);
        }

        $query4 = "SELECT C.*, IFNULL(A.NUM_NEW_CUSTS,0) AS NUM_NEW_CUSTS, IFNULL(A.NUM_CUSTS_ALL,0) AS NUM_CUSTS_ALL FROM( SELECT bus_email, bus_name FROM register_users WHERE bus_email = '$bus_email') C LEFT JOIN ( SELECT bus_email, SUM(CASE WHEN cust_date_register >= '$date_from 00:00:00' AND cust_date_register<= '$date_to 23:59:59' THEN 1 ELSE 0 END) NUM_NEW_CUSTS, COUNT(cust_id) NUM_CUSTS_ALL FROM customers WHERE bus_email = '$bus_email' GROUP BY bus_email ) A ON C.bus_email = A.bus_email LIMIT 1;";
        $result4 = mysqli_query($db,$query4);
        while ($row4 = mysqli_fetch_array($result4,MYSQLI_ASSOC)) {
            array_push($cust_array,$row4);
        }

        array_push($data,$stats_array,$best_item_array,$best_vend_array,$cust_array);

        $txtDateRange = 'Datos desde: '.$date_from.' hasta: '.$date_to;

        $ans = SendMail_SMTP_Summary_Mail($to,$subject,$frequency,$data,$txtDateRange);

        $curr_time = date('Y-m-d H:i:s', time());
        if($ans){
            $query_ans = "INSERT INTO log_summary_mail_users VALUES('$bus_email','$curr_time','1','$frequency');";
            mysqli_query($db,$query_ans);
        }else{
            $query_ans = "INSERT INTO log_summary_mail_users VALUES('$bus_email','$curr_time','0','$frequency');";
            mysqli_query($db,$query_ans);
        }

    }
}

?>