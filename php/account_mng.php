<?php
include("session.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135784524-1');
    </script>
	<title>Luca | Cuenta</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
    <link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_acc_mng.css?<?php echo time(); ?>">    

	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<!-- JSON DATA IVA -->
	<script src="../json/items_iva.json?<?php echo time(); ?>"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>

</head>
<body>

    <div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>

    <div class="OffLineMsg" id="OffLineMsg">
        <div></div>
        <p>Sin conexión</p>
        <span class="mdi mdi-signal-off"></span>
    </div>

    <div id="MenuNavg" class="MenuNavg"></div>

<div id="wrapper" class="wrapper">

    <div id="cont_main">
        <div class="cont_temp">	
            <h2 id="AcctNameTitle" class="AcctName"></h2>
            <p id="AcctEmailTitle" class="edit_user_dt display-block c_email"></p> 
            <p class="user_since">Usuario desde hace <span id="AcctUserSince"></span> días</p> 

            <div class="cont_main">
            
                <div class="cont_user_vars">

                    <div class="cont_user_vars_2">
                    
                        <div class="acct_section">
                            <h2 class="subtitle_1 border_btn">Variables del negocio</h2>
                            <p id="ed_name" class="edit_user_dt display-block" title="Nombre">
                                <span class="mdi mdi-comment-account-outline"></span> 
                                <span id="AcctName"></span>
                            </p>
                            <p id="ed_industry" class="edit_user_dt display-block" title="Industria">
                                <span class="mdi mdi-store"></span> 
                                <span id="AcctIndustry"></span>
                            </p> 
                            <p id="ed_address" class="edit_user_dt display-block" title="País">
                                <span class="mdi mdi-map-marker-outline"></span> 
                                <img id="flag_country" class="flag_country" src="">
                                <span id="AcctCountry"></span>
                            </p> 
                            <p id="ed_address" class="edit_user_dt display-block" title="Ciudad">
                                <span class="mdi mdi-city-variant-outline"></span> 
                                <span id="AcctCity"></span>
                            </p> 
                            <p id="ed_address" class="edit_user_dt display-block" title="Dirección">
                                <span class="mdi mdi-road-variant"></span> 
                                <span id="AcctAddress"></span>
                            </p> 
                            <p id="ed_phone" class="edit_user_dt display-block" title="Teléfono">
                                <span class="mdi mdi-phone"></span> 
                                <span id="AcctPhone"></span>
                            </p> 
                            <p id="ed_nit" class="edit_user_dt display-block" title="NIT">
                                <span class="mdi mdi-card-account-details-outline"></span>
                                <span id="AcctNIT"></span>
                            </p> 
                            <p id="ed_regm" class="edit_user_dt display-block" title="Regímen">
                                <span class="mdi mdi-bank"></span> 
                                <span id="AcctActEcon"></span>
                            </p>

                            <div onclick="OpenUpdateUserVars()" class="update_link">Actualizar variables</div>
                            
                        </div>

                        <div class="acct_section">
                            <h2 class="subtitle_1 border_btn">Numeración de facturación</h2>
                            
                            <div id="ed_reso_wrap">
                                <p id="ed_reso_date" class="edit_user_dt display-block" title="Fecha Resolución">
                                    <span class="mdi mdi-calendar-check"></span> 
                                    <span id="date_resol"></span>
                                </p>

                                <p id="ed_reso_num" class="edit_user_dt display-block" title="Número Resolución">
                                    <span class="mdi mdi-numeric"></span> 
                                    <span id="resolution"></span>
                                </p>

                                <p id="ed_reso_consec" class="edit_user_dt display-block" title="Consecutivo Resolución">
                                    <span class="mdi mdi-sort-numeric"></span> 
                                    <span id="range_reso_ini"></span> a <span id="range_reso_last"></span>
                                </p>
                            </div>

                            <div onclick="OpenUpdateFactNum()" class="update_link">Agregar nueva resolución</div>
                            <div onclick="OpenAdminResolutions()" class="update_link reset-padd-marg">Administrar resoluciones</div>
                            
                        </div>

                        <div class="option_box">
                            <h2 class="subtitle_1 border_btn">Puntos de venta<button class="infobox"><div>Administra tus puntos de venta como cajas o terminales de pago</div><span class="mdi mdi-information-outline"></span></button></h2>
                            
                            <p>Crea puntos de venta y asigna permisos de uso a perfiles de vendedores.</p>

                            <div id='new_pos' class="update_link">Agregar nuevo punto de venta</div>
                            <div id='admin_pos' class="update_link mt5">Administrar puntos de venta</div>
                            <div id='edit_pos' class="update_link reset-padd-marg">Asignaciones de puntos de venta</div>
                            
                        </div>

                    </div>
                </div>	


                <!-- SET NEW PASSWORD -->
                <div class="container_second">

                    <div class="option_box">
                        <h2 class="subtitle_1 border_btn">Opciones de reporte <span id="help_report" class="mdi mdi-information-outline"></span></h2>
                        <p>Selecciona la frecuencia con la que deseas recibir los reportes de tu negocio en tu correo electrónico.</p>
                        <div class="optionfy">
                            <div id="rep_d">Diario</div>
                            <div id="rep_w">Semanal</div>
                            <div id="rep_m">Mensual</div>
                        </div>
                    </div>

                    <div class="option_box mt30">
                        <h2 class="subtitle_1 border_btn">Opciones de impresión</h2>
                        <p>Selecciona la impresora:</p>
                        <select name="printers_list" id="printers_list" class="select_list_1"></select>
                    </div>
                    
                    <form action="" method="POST" onsubmit="return verify_set_pass()" id="form_set_pass" class="form_style_1 display-block mw300"> 
                        <h2 class="subtitle_1 border_btn">Cambiar Contraseña</h2>
                        <input type="password" name="old_pass" id="old_pass" placeholder="Contraseña actual" required>
                        <input type="password" name="new_pass_1" id="new_pass_1" placeholder="Nueva contraseña" required>
                        <div class="pass_strg_cont" id="pass_strg_cont"><div class="pass_strg_cont_bar" id="pass_strg_cont_bar"><div id="pass_lvl_bar"></div></div><p id="pass_lvl">Débil</p></div>
                        <input type="password" name="new_pass_2" id="new_pass_2" placeholder="Repetir contraseña" required>
                        <span class="errorMsg" id="errorMsgNewPass"></span>
                        <input type="submit" name="set_pass_sub" id="set_pass_sub" value="CAMBIAR">
                    </form>    
                    
                </div>
            
            </div>
        
            <!-- DAR DE BAJA  -->
            <form action="" method="POST" id="form_del_user" name="form_del_user" onsubmit="return verifyDeleteUser()" class="form-del-act display-block">
                <input type="submit" name="btn_del_user" id="btn_del_user" class="btn_style_2" value="Darme de Baja">
            </form>
                
        </div>
  
        <!-- MODAL UPDATE USER VARIABLES -->
        <div class="msg-box-container" id="msg-box-container-updateuservars">
            <div class="msg-box-1" id="msg-box-updateuservars">
                <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseUpdateUserVars()">X</p>

                <p class="msgb-box-1-title" id="msgb-box-1-title">ACTUALIZAR VARIABLES</p>

                <h4>Nombre del negocio:</h4>
                <input class="msg-box-1-input" type="text" name="newName" id="newName" placeholder="Nombre del negocio">
                <h4>Tipo de negocio:</h4>
                <select name="newIndustry" id="newIndustry" class="select_list_1"> 
                    <option value="0" selected disabled hidden>Tipo de industria:</option>
                    <option value='43'>Abogados</option>
                    <option value='41'>Accesorios de celulares</option>
                    <option value='40'>Accesorios de ropa</option>
                    <option value='1'>Agricultura</option>
                    <option value='37'>Alimentos Saludables</option>
                    <option value='2'>Alimentos y bebidas</option>
                    <option value='42'>Anteojos</option>
                    <option value='3'>Arte y entretenimiento</option>
                    <option value='4'>Artesanías</option>
                    <option value='5'>Bar</option>
                    <option value='35'>Barbería</option>
                    <option value='51'>Café Internet</option>
                    <option value='50'>Carnicería | Fama</option>
                    <option value='6'>Construcción</option>
                    <option value='39'>Dentista</option>
                    <option value='25'>Deporte</option>
                    <option value='7'>Educación</option>
                    <option value='24'>Farmacia</option>
                    <option value='33'>Ferretería</option>
                    <option value='8'>Financiero</option>
                    <option value='48'>Frutas y verduras</option>
                    <option value='26'>Gimnasio</option>
                    <option value='9'>Hotelería y Turismo</option>
                    <option value='10'>Información</option>
                    <option value='46'>Juguetería</option>
                    <option value='30'>Lavado de vehículos</option>
                    <option value='44'>Librería</option>
                    <option value='11'>Mascotas y Animales</option>
                    <option value='29'>Mecánico</option>
                    <option value='52'>Miscelánea</option>
                    <option value='49'>Panadería</option>
                    <option value='45'>Papelería</option>
                    <option value='34'>Peluquería</option>
                    <option value='12'>Producción Audiovisual</option>
                    <option value='28'>Productos de belleza</option>
                    <option value='38'>Productos de Café</option>
                    <option value='47'>Productos para bebé</option>
                    <option value='31'>Puesto ambulante</option>
                    <option value='13'>Restaurante</option>
                    <option value='14'>Retail</option>
                    <option value='15'>Retail Online</option>
                    <option value='16'>Ropa y moda</option>
                    <option value='17'>Salud</option>
                    <option value='18'>Seguros</option>
                    <option value='19'>Servicios Administrativos</option>
                    <option value='27'>Spa</option>
                    <option value='20'>Tecnología</option>
                    <option value='21'>Tienda</option>
                    <option value='22'>Transporte</option>
                    <option value='23'>Ventas Independientes</option>
                    <option value='32'>Viajes</option>
                    <option value='36'>Videojuegos</option>    
                </select>                 
                <h4>País:</h4>
                <select name="newCountry" id="newCountry" class="select_list_1 country_input"></select>
                <h4>Ciudad:</h4>
                <input class="msg-box-1-input" type="text" name="newCity" id="newCity" placeholder="Ingresa la cuidad">
                <h4>Dirección:</h4>
                <input class="msg-box-1-input" type="text" name="newAddress" id="newAddress" placeholder="Ingresa la dirección">
                <h4>Teléfono:</h4>
                <input class="msg-box-1-input input_phone" type="text" name="newPhone" id="newPhone" placeholder="Ingresa el télefono">
                <h4>NIT:</h4>
                <input class="msg-box-1-input" type="text" name="newNIT" id="newNIT" placeholder="Ingresa el NIT">
                <h4>Actividad Económica:</h4>
                <select name="newActEcon" id="newActEcon" class="select_list_1">
                    <option value="0" selected disabled hidden>Actividad Económica:</option>
                    <option value="1">Régimen Común</option>
                    <option value="2">Régimen Simplificado</option>
                </select>

                <span class="errorMsg" id="errorMsgUpdateUserVars"></span>
                <button class="btn_green_full" id="set_var_sub">ACTUALIZAR</button>              

            </div>	
        </div>      

        <!-- MODAL UPDATE FACTURAS NUMBERS -->
        <div class="msg-box-container" id="msg-box-container-updatefactnum">
            <div class="msg-box-1" id="msg-box-updatefactnum">
                <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseUpdateFactNum()">X</p>

                <p class="msgb-box-1-title" id="msgb-box-1-title">NUEVA FACTURACIÓN</p>

                <h4>Fecha de la Resolución:</h4>
                <input class="msg-box-1-input" type="date" name="newDateRes" id="newDateRes" placeholder="">
                <h4>Número de resolución:</h4>
                <input class="msg-box-1-input" type="text" name="newResNum" id="newResNum" placeholder="Ingresa el número de la resolución">
                <h4>Consecutivo:</h4>
                <div class="consect_wrap">
                    <input class="msg-box-1-input" type="text" name="newConsFrom" id="newConsFrom" placeholder="Desde"> 
                    <p> a </p>
                    <input class="msg-box-1-input" type="text" name="newConsTo" id="newConsTo" placeholder="Hasta">          
                </div>
                <h4 class="cr_title">Resolución vigente:</h4>
                <div class="current_resolution">
                    <label class="switch">
                        <input id="curr_resol" type="checkbox" checked>
                        <span class="slider round"></span>
                    </label>
                </div>

                <span class="errorMsg" id="errorMsgUpdateFactNum"></span>
                <button class="btn_green_full" id="btnActUpdateFactNum">AGREGAR</button>   

            </div>	
        </div>  


        <!-- MODAL EDIT FACTURAS NUMBERS -->
        <div class="msg-box-container" id="msg-box-container-editfactnum">
            <div class="msg-box-1" id="msg-box-editfactnum">
                <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditFactNum_over()">X</p>

                <p class="msgb-box-1-title" id="msgb-box-1-title">EDITAR FACTURACIÓN</p>

                <h4>Fecha de la Resolución:</h4>
                <input class="msg-box-1-input" type="date" name="editDateRes" id="editDateRes" placeholder="">
                <h4>Número de resolución:</h4>
                <input class="msg-box-1-input" type="text" name="editResNum" id="editResNum" placeholder="Ingresa el número de la resolución">
                <h4>Consecutivo:</h4>
                <div class="consect_wrap">
                    <input class="msg-box-1-input" type="text" name="editConsFrom" id="editConsFrom" placeholder="Desde"> 
                    <p> a </p>
                    <input class="msg-box-1-input" type="text" name="editConsTo" id="editConsTo" placeholder="Hasta">          
                </div>

                <span class="errorMsg" id="errorMsgEditFactNum"></span>
                <button class="btn_green_full" id="btnActEditFactNum">ACTUALIZAR</button>   

            </div>	
        </div>

        <!-- MODAL RESOLUTIONS ADMIN -->
        <div class="msg-box-container" id="msg-box-container-resolutionsadm">
            <div class="msg-box-1" id="msg-box-resolutionsadm">
                <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseAdminResolutions()">X</p>

                <p class="msgb-box-1-title" id="msgb-box-1-title">RESOLUCIONES</p>

                <div id="resolt_act" class="resolt_act">
                    <p class="msg-box-1-description">Selecciona la resolución activa.</p>

                    <div id="admin_res_cont" class="admin_res_cont">
                        <div id="admin_res_wrap" class="admin_res_wrap">
                        </div>
                    </div>

                    <span class="errorMsg" id="errorMsgAdminResolutions"></span>
                    <button class="btn_green_full" id="btnAdminResolutions">ACTUALIZAR</button>   
                </div>

                <div id="NoResolutions" class="NoResolutions"><p>No has ingresado resoluciones de facturación.</p></div>

                <div class="msg-box-1-footnone res_link_dian">
                    <a href="https://www.dian.gov.co/impuestos/sociedades/presentacionclientes/Solicitud_de_Autorizacion_de_Numeracion_de_Facturacion.pdf" target="_blank">¿Cómo solicitar la numeración de facturación?</a>
                </div>

            </div>	
        </div>    
        
        <!-- Modal Help -->
        <div class="msg-box-container" id="msg-box-container-help">
            <div class="msg-box-1" id="msg-box-help">
                <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseHelp()">X</p>
                <div id="help_container" class="help_container"></div>
            </div>	
        </div>

        <!-- Open modal gallery images -->
        <div class="container_prelogos_back" id="container_prelogos_back">
            <div class="container_prelogos animated fadeIn" id="container_prelogos">
                <p class="prelogos-exit-mark" id="prlog-exit-mark" onclick="CloseGallery()">X</p>
                <div class="container_prelogos_i1">
                    <select name="logo_categ_list" id="logo_categ_list" class="select_list_1"> 
                        <option value="74-food" selected>Selecciona una categoría</option>
                        <option value="74-food">Alimentos</option>
                        <option value="73-drink">Bebidas</option>
                        <option value="58-beauty-spas">Belleza y Spa</option>
                        <option value="75-kitchen">Cocina</option>
                        <option value="62-construction">Construcción</option>
                        <option value="60-sport">Deportes</option>
                        <option value="82-school-science">Educación</option>
                        <option value="63-space">Espacio y astronomía</option>
                        <option value="67-tools">Herramientas</option>	
                        <option value="57-hotel">Hotelería y Turismo</option>
                        <option value="69-lights">Luces</option>
                        <option value="53-places">Lugares del Mundo</option>
                        <option value="64-pet">Mascotas y Animales</option>
                        <option value="59-nature-ecology">Naturaleza</option>
                        <option value="81-christmas">Navidad</option>
                        <option value="80-children">Niños</option>
                        <option value="68-objects">Objetos</option>
                        <option value="77-leisure">Ocio </option>
                        <option value="65-religion">Religión</option>
                        <option value="79-romance">Romance</option>
                        <option value="76-clothes">Ropa</option>				   			    		   
                        <option value="72-health">Salud</option>				    			    
                        <option value="71-security">Seguridad</option>				    			    
                        <option value="61-transportation">Transporte</option>
                        <option value="55-travel">Viajes</option>		
                        <option value="78-video-games">Video Juegos</option>			    
                    </select>
                    
                </div>

                <div class="container_prelogos_i2" id="ctn_prelogos_i2"></div>

            </div>
        </div>
        
    </div>
    
</div>

<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
<script src="../js/js_acc_mng.js?<?php echo time(); ?>"></script>
<script src="../js/modals.js?<?php echo time(); ?>"></script>
<script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
</body>
</html>