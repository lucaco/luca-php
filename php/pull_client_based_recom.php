<?php
/*
 * FILE: pull_client_based_recom.php
 * WHAT FOR: Pull top items for customer based recomendation.
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 	  = $_SESSION['login_user'];
  $cust_key 	  = mysqli_real_escape_string($db,$_POST['cust_key']);
	$todays_date  = date('Y-m-d H:i:s');

	$num_items_pull = 4;

	$query = "SELECT DISTINCT A.bus_email, A.item_id, A.affinity, B.prod_name, B.prod_icon FROM (SELECT * FROM recomendations WHERE bus_email = '$bus_email' AND cust_key='$cust_key') A LEFT JOIN (SELECT bus_email, prod_id, prod_name, prod_icon FROM products) B ON A.bus_email = B.bus_email AND A.item_id = B.prod_id WHERE prod_name IS NOT NULL ORDER BY affinity DESC LIMIT $num_items_pull;";    
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);

	$return_arr = Array();
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		array_push($return_arr,$row);
	}
	
	echo json_encode($return_arr);
}
?>


