<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	
	$bus_email = $_SESSION['login_user'];
	$item_id   = mysqli_real_escape_string($db,$_POST['item_id']);
	$num_days  = mysqli_real_escape_string($db,$_POST['num_days']);
	
	$last_days = $num_days;
	// Count of Number of Sales in the last $num_days days
	$sql = "SELECT date_format (B.trx_date,'%Y-%m-%d') AS x, sum(A.item_count) AS y, sum(A.item_value) AS z FROM bills A LEFT JOIN transactions B ON A.bus_email = B.bus_email AND A.id_bill = B.id_bill WHERE A.bus_email = '$bus_email' AND A.item_id = '$item_id' AND B.state = '1' AND B.trx_date BETWEEN DATE_SUB(NOW(), INTERVAL $last_days DAY) AND NOW() GROUP BY x ORDER BY x;";
	$result = mysqli_query($db,$sql);
	
	$last_sales_count = Array();
	
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
	    array_push($last_sales_count,$row);
	}

	echo json_encode($last_sales_count);	
}

?>