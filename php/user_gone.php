<?php

include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);
session_start();

$ss_email = $_SESSION['login_user'];
$ss_name  = $_SESSION['login_bus_name'];
$ss_hash  = $_SESSION['hash_act'];

SendMail_SMTP_SimpleMsg($ss_email,"😢 No es más que un hasta luego",$ss_name, "!Gracias por tu tiempo en 2Luca!. <br></br> Hemos dado de baja satisfactoriamente al email: <b>" . $ss_email . ".</b> Esparamos poder contar contigo en una próxima oportunidad. <br></br> Sí en cualquier momento decides regresar a Luca siempre estaremos dispuestos a recibirte de nuevo, solo ingresa al siguiente link y todo será como antes:<br></br> <a style='color: #39ac95; text-decoration: none;'> https://www.2luca.co/php/usercomeback.php?email=". $ss_email ."&hash=". $ss_hash ."</a> <br></br> Gracias por preferirnos <br></br> 2Luca - Colombia<br>Bogotá, Colombia <br> ");

deleteUser($ss_email, false);

// Cerrar sesion de usuario 
session_start();
$_SESSION = array();
session_destroy();

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

?>