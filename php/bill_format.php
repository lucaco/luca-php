<?php

include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);
session_start();

// ===========================================================
$points_system  = 700;    // 1 point for each 700$ purchased

$bus_email      = $_SESSION['login_user'];
$id_bill        = mysqli_real_escape_string($db,$_GET["id_bill"]);
// ===========================================================


$return_arr = Array();
$TruePage = "block";

if($bus_email == ''){
  // throw new Exception("Email not founded.");
  // header("location:../index.php");
  $TruePage = "none";
  echo "Email not founded.";
}

if($id_bill==''){
  // throw new Exception("id_bill not founded.");
  // header("location:../index.php");
  $TruePage = "none";
  echo "id_bill not founded.";
}

// ******* Obtener Información del Cliente y Vendedor dado el ID BILL ***********
$sql = "SELECT id_bill, trx_date, trx_value, discount, taxes, tips, payment_type, other_payment_type, num_items, creditcard_compr_number, cash_change, IFNULL(B.cust_name,'') AS cust_name, IFNULL(B.cust_email,'') AS cust_email,  IFNULL(B.cust_key,'') AS cust_key, IFNULL(B.cust_gender,'') AS cust_gender, IFNULL(B.cust_id,'') AS cust_id, IFNULL(B.cust_type_id,'') AS cust_type_id, IFNULL(B.cust_points,'') AS cust_points, IFNULL(UCASE(C.prof_name),'') AS prof_name, IFNULL(C.avatar,'') AS prof_avatar, IFNULL(A.points_redeem,'0') AS points_redeem  FROM transactions A LEFT JOIN customers B ON A.bus_email = B.bus_email AND A.cust_key = B.cust_key LEFT JOIN profiles C ON A.bus_email = C.bus_email AND A.profsessid = C.profsessid WHERE A.bus_email = '$bus_email' AND A.id_bill = '$id_bill';";
$results = mysqli_query($db, $sql);
$count   = mysqli_num_rows($results);

if($count == 1){

  $row = mysqli_fetch_array($results,MYSQLI_ASSOC);

  $num_points     = $row['cust_points'];
  $customer_name  = $row['cust_name'];
  $cust_email     = $row['cust_email'];
  $cust_gender    = $row['cust_gender'];
  $cust_id        = $row['cust_id'];
  $cust_key       = $row['cust_key'];
  $cust_type_id   = $row['cust_type_id'];
  $trx_date       = $row['trx_date'];
  $trx_value      = $row['trx_value'];
  $discount       = $row['discount'];
  $taxes          = $row['taxes'];
  $tips           = $row['tips'];
  $payment_type   = $row['payment_type'];
  $other_payment_type = $row['other_payment_type'];
  $creditcard_compr_number = $row['creditcard_compr_number'];
  $cash_change    = $row['cash_change'];
  $num_items      = $row['num_items'];
  $profile_name   = $row['prof_name'];
  $cp_avatar      = $row['prof_avatar'];
  $cp_avatar      = "https://www.2luca.co" . str_replace(array("..",".svg"),array("",".png"),$cp_avatar);
  $points_redeem  = $row['points_redeem'];

  // PRINTER CONFIG
  $sql2    = "SELECT A.bus_email, B.* FROM register_users A LEFT JOIN printers B ON A.printer_id = B.printer_id WHERE A.bus_email = '$bus_email';";
  $result2 = mysqli_query($db,$sql2);    
  $row2    = mysqli_fetch_array($result2,MYSQLI_ASSOC);
  $printer_width  = $row2['printer_width'];
  $printer_margin = $row2['printer_margin'];

  if($printer_width == "" or $printer_width == null){
    $width_bill = "58mm";
  }else{
    $width_bill = $printer_width;
  }
  if($printer_margin == "" or $printer_margin == null){
    $margin_bill = "0mm";
  }else{
    $margin_bill = $printer_margin;
  }

  $red_points_sec = '';
  if($points_redeem!='0'){
    $red_points_sec = $red_points_sec . '<tr><td width="60%" align="right">PUNTOS REDIMIDOS</td>';
    $red_points_sec = $red_points_sec . '<td width="40%" align="center">'.number_format($points_redeem).'</td></tr>';
  }

  $subtotal_bill = ($trx_value-$tips+$discount);
  $totalbeforetaxes = ($subtotal_bill-$taxes);
  $points = floor($trx_value/$points_system);
  $paidbyuser = ($trx_value+$cash_change);

  $todays_date  = date('Y-m-d H:i:s');
  $todays_date2 = date('Y-m-d');

  if($payment_type==1){
      $payment_type = 'EFECTIVO';
  }else if($payment_type==2){
      $payment_type = 'TARJETA CRÉDITO';
  }else if($payment_type==3){
      $payment_type = 'TARJETA DÉBITO';
  }else if($payment_type==4){
      $payment_type = 'PAGADO (OTRO)';
  }else{
      $payment_type = 'PAGADO';
  }

  $sql = "SELECT * FROM register_users WHERE bus_email = '$bus_email';";
  $results = mysqli_query($db, $sql);
  $count   = mysqli_num_rows($results);

  if($count == 1){
    $row = mysqli_fetch_array($results,MYSQLI_ASSOC);

    $bus_name        = $row['bus_name'];
    $bus_nit         = $row['bus_nit'];
    $bus_address     = $row['bus_address'];
    $bus_phone       = $row['bus_phone'];
    $bus_regimen     = $row['bus_act_econ'];
    $bus_regimen_cod = $row['bus_act_econ'];

    if($bus_regimen=='1'){
        $bus_regimen='<p style="margin: 0;"> Régimen Común </p>';
    }else if($bus_regimen=='2'){
        $bus_regimen='<p style="margin: 0;"> Régimen Simplificado </p>';
    }

    if($bus_nit!=''){$bus_nit = '<p style="margin: 0;"> NIT. '.$bus_nit.'</p>';}
    if($bus_address!=''){$bus_address = '<p style="margin: 0;">'.$bus_address.'</p>';}
    if($bus_phone!=''){$bus_phone = '<p style="margin: 0;">Tel: '.$bus_phone.'</p>';}
    

    $puntos_luca = '';
    if($cust_key == ''){
        $customer_name = 'No Registrado'; 
        $cust_id       = ' - ';	
    }else{
        if($num_points == ''){$num_points = 0;}
        $puntos_luca = '<div width="100%" style="text-align: center; padding-top: 10px;">
                <center><img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&choe=ISO-8859-1&chl=https://www.2luca.co/php/customerview.php?ckey='.$cust_key.'" alt="QRCODE" width="120" height="120" border="0" /></center>
                <h6 align="center" style="font-size: 12px;margin-top: 0;margin-bottom: 0;border-bottom: 1px dashed;padding-bottom: 10px;">!HOLA <span style="text-transform: uppercase;">'.$customer_name.'</span>! TIENES <span>'.$num_points.'</span> PUNTOS LUCA ACUMULADOS AL '.$todays_date2.'</h6>
            </div>';
    }

    //::::::::::: CHECK RESOLUTION COUNTER :::::::::
    $resolution_cnt = '';
    $sql = "SELECT resolution_cnt FROM transactions WHERE bus_email = '$bus_email' AND id_bill = '$id_bill';";
    $res_count_resol = mysqli_query($db, $sql);
    while ($row = mysqli_fetch_array($res_count_resol,MYSQLI_ASSOC)) {
      $resolution_cnt = $row['resolution_cnt'];
    }
    if($resolution_cnt == ''){
      $resol_counter = '';
    }else{
      $resol_counter = '<p style="margin: 0;"> Factura número: <span>'.$resolution_cnt.'</span></p>';
    }
    
    // :::::::::::: INFORMACION DE RESOLUCIÓN ::::::::::::
    $sql 	= "SELECT *, DATE_FORMAT(date_resolution,'%Y-%m-%d') AS date_reso_form FROM resolutions WHERE current_resolution = '1' AND bus_email = '$bus_email'";
    $resolu = mysqli_query($db, $sql);
    $ss_reso_num = "";
    $ss_reso_date = "";
    $ss_reso_range = "";
    while ($row = mysqli_fetch_array($resolu,MYSQLI_ASSOC)) {
      $ss_reso_num   = $row['resolution'];
      $ss_reso_date  = $row['date_reso_form'];
      $ss_reso_range = $row['range_reso_ini'] . "-" . $row['range_reso_last'];
    }

    if(($bus_regimen_cod=='2') || ($bus_regimen_cod=='1' && $ss_reso_num != "")){

      $resolution_info = '';

      if($ss_reso_num != ""){        
        $resolution_info = '
        <div style="width:100%;">
          <p style="text-align:left; margin:1px 0; padding-top: 10px;">Resolución No. '.$ss_reso_num.'</p>
          <p style="text-align:left; margin:1px 0;">Fecha Resolución '.$ss_reso_date.'</p>
          <p style="text-align:left; margin:1px 0;">Numeración autorizada: '.$ss_reso_range.'</p>
          <p style="text-align:left; margin:1px 0; padding-bottom: 10px;">ICA Tarifa 9.66 X 1000</p>
        </div>
        <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center; border-bottom: 1px dashed;"></div>';
      }

      if($discount == ''){$discount=0;}
      if($tips == ''){$tips=0;}
      if($taxes == ''){$taxes=0;}
      //if($payment_type == ''){$payment_type=0;}
      //if($creditcard_compr_number == ''){$creditcard_compr_number=0;}
      //if($cash_change == ''){$cash_change=0;}

      // ***** CREAR VECTOR JSONBILL ******
      $query = "SELECT *, IFNULL(B.prod_name,'Ítem') AS prod_name, IFNULL(B.prod_units,'Unidades') AS prod_units, IFNULL(B.prod_icon,'../icons/SVG/78-video-games/mario-question-box.svg') AS prod_icon FROM bills A LEFT JOIN products B ON A.item_id = B.prod_id WHERE A.bus_email = '$bus_email' AND A.id_bill = '$id_bill';";
      $result = mysqli_query($db,$query);
      $count  = mysqli_num_rows($result);
      $itemsTxt = '';
      $trex = '';
      $items_list = "'',";
      if($count>0){

          while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
              $d  = $row['prod_icon'];
              $prod_icn  = "https://www.2luca.co" . str_replace(array("..","/SVG/",".svg"),array("","/PNG/",".png"),$d);
              $n = $row['item_value'];
              $itemAmountStyle = number_format($n);
              $items_list = $items_list ."'". $row['item_id'] . "',";
              $itemsTxt   = $itemsTxt . '{"itemId":"'.$row['item_id'].'","itemName":"'.$row['prod_name'].'","itemCount":"'.$row['item_count'].'","itemAmount":"'.$row['item_value'].'","itemAmountStyle":"'.$itemAmountStyle.'","itemDiscount":"'.$row['item_discount'].'","itemCoupon":"'.$row['item_coupon'].'","itemNote":"'.$row['item_note'].'","itemIcon":"'.$row['prod_icon'].'","itemIconHTTP":"'.$prod_icn.'","itemUnits":"'.$row['prod_units'].'"},'; 

              $trex = $trex . ' <tr><td width="20%" align="center">'.$row['item_count'].'</td><td width="45%" align="center">'.$row['prod_name'].'</td><td width="35%" align="center">'.$itemAmountStyle.'</td></tr>';
          }
          $itemsTxt   = rtrim($itemsTxt,',');
          $items_list = rtrim($items_list ,',');

          if($cust_key != ''){
            // ********** PRODUCT CLIENT BASED RECOMENDATION *************
            if(strlen($items_list)>0){
              $items_list_sql_recom = "AND A.item_id NOT IN (".$items_list.")";
            }else{
              $items_list_sql_recom = "";
            }
            $num_items_pull = 1;
            $query = "SELECT DISTINCT A.bus_email, A.item_id, A.affinity, B.prod_name, B.prod_icon FROM (SELECT * FROM recomendations WHERE bus_email = '$bus_email' AND cust_key='$cust_key') A LEFT JOIN (SELECT bus_email, prod_id, prod_name, prod_icon FROM products) B ON A.bus_email = B.bus_email AND A.item_id = B.prod_id WHERE prod_name IS NOT NULL ".$items_list_sql_recom." ORDER BY affinity DESC LIMIT $num_items_pull;";    
            $result = mysqli_query($db,$query);
            $count  = mysqli_num_rows($result);
            $prod_name = '';
            $prod_icn  = '';
            $customer_recom  = '';
            while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                $d  = $row['prod_icon'];
                $prod_icn  = "https://www.2luca.co" . str_replace(array("..","/SVG/",".svg"),array("","/PNG/",".png"),$d);
                $customer_recom = '<div width="100%" margin-bottom: 5px; margin-top: 5px; style="text-align: center;">
                    <p align="center" style="font-size: 13px; font-weight: bold;">RECOMENDADO PARA TU PRÓXIMA VISITA:</p>
                    <table border="0" cellspacing="0" cellpadding="3" width="100%">
                    <tr>
                        <td width="40%" align="right"><img src="'.$prod_icn.'" alt=" " width="50" height="50" border="0" /></td>
                        <td width="60%" align="left" style="font-size: 15px;">'.$row['prod_name'].'</td>
                    </tr>
                    </table>
                </div>';
            }
          }

      }

      $jsonBill_txt = '{"total_items_bill":"'.$num_items.'","cust_key":"'.$cust_key.'","tax_bill":"'.$taxes.'","bill_id":"'.$id_bill.'","subtotal_bill":"'.$subtotal_bill.'","discount_bill":"'.$discount.'","tips_bill":"'.$tips.'","super_total_bill":"'.$trx_value.'","cash_change":"'.$cash_change.'","bill_print_type":"1","creditCardComprNum":"'.$creditcard_compr_number.'","otherPaymentOption":"'.$other_payment_type.'", "paymentType":"'.$payment_type.'","totalbeforetaxes":"'.$totalbeforetaxes.'","paidbyuser":"'.$trx_value.'","PointsRedem":"0","PointThisBill":"'.$points.'","ItemsInBill":['.$itemsTxt.']}'; 
      $data = json_decode(stripslashes($jsonBill_txt),true);

    }else{
      // throw new Exception("La resolución no existe");
      // header("location:../index.php");
      $TruePage = "none";
      echo "Imposible generar la factura. No se encontró una resolución activa.";
    }

  }else{
    // throw new Exception("email does not exist.");
    // header("location:../index.php");
    $TruePage = "none";
    echo "email does not exist.";
  }
}else{
  // throw new Exception("email or id_bill do not match.");
  // header("location:../index.php");
  $TruePage = "none";
  echo "email or id_bill do not match.";
}
?>

<!DOCTYPE html>
<html lang="es" style="width: <?php echo $width_bill; ?>; height: auto;">
<head>
	<title>Factura de Venta</title>
	<meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body style="font: 500 13px Helvetica, sans-serif; margin: <?php echo $margin_bill; ?>;">

<div style="display: <? echo $TruePage; ?>;">
  <div style="overflow: hidden;">

      <div style="text-align:center; margin-bottom:20px;">  
          <p style="font-size: 15px; text-transform: uppercase;"><b><?php echo $bus_name; ?></b></p>
          <?php echo $bus_nit; ?>
          <?php echo $bus_address; ?>
          <?php echo $bus_phone; ?>
          <?php echo $bus_regimen; ?>
      </div>
      
      <div style="text-align:left; margin-bottom:10px;"> 
          <?php echo $resol_counter; ?>
          <p style="margin: 0;"> Código factura: <span><?php echo $id_bill; ?></span></p>
          <p style="margin: 0;"> Fecha: <span><?php echo $trx_date; ?></span></p>        
          <p style="margin: 0;"> Cliente: <span style="text-transform: uppercase;"><?php echo $customer_name; ?></span></p>
          <p style="margin: 0;"> ID Cliente: <span><?php echo $cust_id; ?></span></p>          
          <p style="margin: 0;"> Vendido por: <span style="text-transform: uppercase;"><?php echo $profile_name; ?></span></p>
      </div>
      
      <table border="0" cellspacing="0" cellpadding="5" width="100%">
        <tr style="margin-bottom: 5px;">
          <th width="20%" align="center"><b>Cant.</b></th>
          <th width="45%" align="center"><b>Descripción.</b></th>
          <th width="35%" align="center"><b>Total</b></th>
        </tr>
        <?php echo $trex; ?>
      </table>
      
      <div style="width:100%; text-align: center; margin-bottom: 5px; margin-top: 5px; border-bottom: 1px dashed;"></div>

  <div style="margin-bottom: 0px; margin-top: 2px;">
    <table border="0" cellspacing="0" cellpadding="3" width="100%">
          <tr>
          <td width="70%" align="right">Subtotal antes de impuestos</td>
          <td width="30%" align="center"><?php echo number_format($totalbeforetaxes); ?></td>
          </tr>
          <tr>
          <td width="70%" align="right">Impuestos</td>
          <td width="30%" align="center"><?php echo number_format($taxes); ?></td>
          </tr>
          <tr>
          <td width="70%" align="right">Subtotal</td>
          <td width="30%" align="center"><?php echo number_format($subtotal_bill); ?></td>
          </tr>
          <tr>
          <td width="70%" align="right">Descuento</td>
          <td width="30%" align="center"><?php echo number_format($discount); ?></td>
          </tr>
          <tr>
          <td width="70%" align="right">Propina</td>
          <td width="30%" align="center"><?php echo number_format($tips); ?></td>
          </tr>
          <tr>
          <td width="70%" align="right"><b>TOTAL A PAGAR</b></td>
          <td width="30%" align="center"><b><?php echo number_format($trx_value); ?></b></td>
          </tr>
        </table>
      </div>
      
      <div style="width:100%;">
        <p style="width:100%; margin-bottom: 0px; margin-top: 1px; text-align: center; ; border-bottom: 1px dashed;"></p>
          <p align="center"> <b>INFORMACIÓN TRIBUTARIA</b> </p>
          <p align="center"> Base gravable de $ <?php echo number_format($totalbeforetaxes); ?> para un valor total pagado de impuesto de $ <?php echo number_format($taxes); ?> </p>
          <p style="width:100%; text-align: center; border-bottom: 1px dashed;"></p>
        
          <p style="text-align:center;"> <b>PAGO CLIENTE</b> </p>
          <table border="0" cellspacing="0" cellpadding="2" width="100%">
            <tr>
          <td width="60%" align="right">TOTAL</td>
          <td width="40%" align="center"><?php echo number_format($trx_value); ?></td>
            </tr>
            <tr>
          <td width="60%" align="right"><?php echo $payment_type; ?></td>
          <td width="40%" align="center"><?php echo number_format($paidbyuser); ?></td>
            </tr>
            <tr>
          <td width="60%" align="right">CAMBIO</td>
          <td width="40%" align="center"><?php echo number_format($cash_change); ?></td>
            </tr>
          </table>
      </div>
      
      <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center; border-bottom: 1px dashed;"></div>
      
      <?php echo $resolution_info; ?>
      
      <?php echo $puntos_luca; ?>

      <?php echo $customer_recom; ?>

      <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center; border-bottom: 1px dashed;"></div>
      
      <div style="text-align:left; margin-top:10px;"> 
          <p style="margin: 0;"> Factura creada por 2Luca Colombia</p>
      </div>

  </div>
</div>

</body>
</html>