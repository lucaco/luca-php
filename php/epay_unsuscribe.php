<?php

include("functions.php");
include("config.php");
include("config_epayco.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){

  $bus_email 		 = $_SESSION['login_user'];
  $bus_name      = $_SESSION['login_bus_name'];
  $pkg_id        = trim(mysqli_real_escape_string($db,$_POST['pkg_id']));

  $r = Array();
  // ::::::::::::: OBTENER LLAVE DE SUBS DE LA BASE DE DATOS :::::::::::::
  $query  = "SELECT pkg_".$pkg_id."_key FROM marketplace WHERE bus_email = '$bus_email' AND pkg_".$pkg_id." = '1';";
  $result = mysqli_query($db,$query);
  $row    = mysqli_fetch_array($result,MYSQLI_ASSOC);
  $id_pay_cust = $row["pkg_".$pkg_id."_key"];

  if($id_pay_cust!=''){
  
    // ::::::::::::: CANCELAR SUBSCRIPCION :::::::::::::
    $sub_canc = $epayco->subscriptions->cancel($id_pay_cust);
    $sub_canc2      = json_encode($sub_canc);
    $sub_canc_json  = json_decode($sub_canc2,true);

    if($sub_canc_json["status"]==true){
      if($sub_canc_json["success"]==true){
        array_push($r, Array('S',$sub_canc_json["message"]));

        // ::::::::::::: DESUSCRIBIR DE LA BASE DE DATOS :::::::::::::
        $query = "UPDATE marketplace SET pkg_".$pkg_id." = '0' WHERE bus_email = '$bus_email';";
        mysqli_query($db,$query);
        if(mysqli_affected_rows($db)<=0){          
          array_push($r, Array('E',"No se pudo desuscribir de la base de datos."));
        }else{
          // ::::::::::::: ENVIAR CORREO ELECTRONICO :::::::::::::
          $email_sub = "Desuscrito correctamente";
          $email_body = "Tu suscripción con id: <b>".$id_pay_cust."</b> se a cancelado correctamente y desde ahora no se generará cobro por este paquete.</br></br> Si en cualquier momento desear volver a utilizarlo puedes activarlo de nuevo en el <b>MarketPlace</b> de <a href='https://www.2luca.co'>Luca</a>.";
          SendMail_SMTP_SimpleMsg($bus_email,$email_sub,$bus_name,$email_body);
        }

      }else{
        array_push($r, Array('E',"Ocurrió un error durante la desuscripción."));
      }
    }else{
      array_push($r, Array('E',"Ocurrió un error desuscribiendo el usuario."));
    }
  
  }else{
    array_push($r, Array('E',"No existe el código de suscripción"));
  }

  echo json_encode($r);
  
}

?>