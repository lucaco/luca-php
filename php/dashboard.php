<?php
include("session.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-135784524-1');
	</script>
	<title>Luca | Dashboard</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
	
	<!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">

	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_dashboard.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
	<script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>

	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css" />
	<!-- MATERIAL DESIGN ICONS AND FONTAWESOME -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<script src="https://use.fontawesome.com/releases/v5.9.0/js/all.js" data-auto-replace-svg="nest"></script>

	<!-- BOOTSTRAP STYLES -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
	<!-- BOOTSTRAP SCRIPTS -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<!-- JSON DATA IVA -->
	<script src="../json/items_iva.json?<?php echo time(); ?>"></script>
	<!-- TAGS AND AUTOCOMPLETE -->
	<link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
	<script src="../external_libs/jquery.magicsearch.min.js"></script>

</head>

<body>

	<div class="notify_alert"><span>x</span>
		<div class="na_title"><i class="fas fa-bell"></i><span></span></div>
		<div class="na_msg"></div>
	</div>
	<div class="MainRoller_background RollerMain" id="MainRoller_background">
		<div class="RollerMain_cont">
			<div class="LoadingRoller" id="MainRoller">
				<div class="lds-roller">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
			<p class="RollerMainText" id="RollerMainText">Cargando ...</p>
		</div>
	</div>

	<div class="bar_loading_cont">
		<div class="bar_loading">
			<div class="bars_wrap">
				<div class="bar bar1"></div>
				<div class="bar bar2"></div>
				<div class="bar bar3"></div>
				<div class="bar bar4"></div>
				<div class="bar bar5"></div>
				<div class="bar bar6"></div>
			</div>
			<div class="bar_ld_msg"></div>
		</div>
	</div>

	<div class="OffLineMsg" id="OffLineMsg">
		<div></div>
		<p>Sin conexión</p>
		<span class="mdi mdi-signal-off"></span>
	</div>

	<div id="MenuNavg" class="MenuNavg"></div>

	<div id="wrapper" class="wrapper">

		<ul class="nav nav-tabs" id="controlTabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="business-tab" data-toggle="tab" href="#business" aria-controls="business"
					aria-selected="true">Negocio</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="customer-tab" data-toggle="tab" href="#customer" aria-controls="customer"
					aria-selected="false">Cliente</a>
			</li>
		</ul>

		<div class="tab-content" id="controlTabsContent">

			<div id="noInfoDashboard" class="noInfoDashboard">
				<img src="../images/undraw_segment_analysis.svg" alt="NoDataDashoard">
				<div>
					<p>NO TIENES NINGUNA VENTA</p><a href="https://www.2luca.co/php/sales.php">Has tu primera venta</a>
				</div>
			</div>

			<!-- Business tab -->
			<div class="tab-pane fade show active tab_content" id="business" role="tabpanel" aria-labelledby="business-tab" style="padding-top: 30px">

				<div class="row">
					<div class="col-md-12">
							<div class="intro_tools_dash">
								<label for="period-btn" accesskey="p">Selecciona un rango de fechas:</label>
								<select class="select_box custom-select" id="period-btn" name="period-btn">
									<option value="last-day">Último día</option>
									<option value="last-week">Última semana</option>
									<option selected value="last-month">Último mes</option>
									<!--<option value="last-quarter">Último trimestre</option>-->
									<option value="last-year">Último año</option>
									<option value="this-month">Este mes</option>
									<!--<option value="this-quarter">Este trimestre</option>-->
									<option value="this-year">Este año</option>
									<option value="all">Todo</option>
								</select>
							</div>
					</div>
				</div>

				<div class="row" style="margin-top: 15px; margin-bottom: 15px;">
					<div class="col-md-3">
						<div class="box_db2">
							<p data-toggle="tooltip"><span class="big-text color333" id="trx-total">$ -</span></p>
							<p class='color999'>Valor de ventas</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="box_db2">
							<p data-toggle="tooltip"><span class="big-text color333" id="trx-amount">-</span> ventas</p>
							<p class='color999'>Número de ventas realizadas</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="box_db2">
							<p data-toggle="tooltip"><span class="big-text color333" id="trx-numitems">-</span> ítems</p>
							<p class='color999'>Cantidad de ítems vendidos</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="box_db2">
							<p><span id="delta-value" class="big-text delta">0%</span></p>
							<p class='color999'><span id="delta-direction">más</span> que <span id="delta-period">el mes</span> anterior</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-7">
						<div class="col box_container">
							<div class="row">
								<div class="col-md-6">
									<img src="../icons/SVG/05-time/calendar-2.svg" width="35px" />
									<p class="h3" style="display: inline-block;margin-left: 10px;margin-bottom: 0">Ventas</p>
									<button class="infobox">
										<div>Evolución del valor vendido.</div>
										<i class="far fa-question-circle"></i>
									</button>
								</div>								
							</div>
							
							<div class="row" style="margin-top: 50px">
								<div class="col">
									<canvas id="lineChart" style="height: 350px; width: 100%"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-5">
						<div class="col box_container">
							<div class="row">
								<div class="col">
									<img src="../icons/general/shopping-cart-full.png" width="35px" />
									<p class="h3" style="display: inline-block;margin-left: 10px">Ítems</p>
									<button class="infobox">
										<div>Información sobre ítems más vendidos por porcentaje, días de las semana y horas del día.</div>
										<i class="far fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="row" style="margin-top: 15px">
								<div class="col-md-6 pieChart_wrap">
									<canvas id="pieChart" style="height:300px;" height="300"></canvas>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-8 pie_txt_lbl">
											<p id="item-1" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="item-1-value" class="item-ptg"></p>
										</div>
										<div class="col-md-8 pie_txt_lbl">
											<p id="item-2" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="item-2-value" class="item-ptg"></p>
										</div>
										<div class="col-md-8 pie_txt_lbl">
											<p id="item-3" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="item-3-value" class="item-ptg"></p>
										</div>
										<div class="col-md-8 pie_txt_lbl">
											<p id="item-4" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="item-4-value" class="item-ptg"></p>
										</div>
										<div class="col-md-8 pie_txt_lbl">
											<p id="item-5" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="item-5-value" class="item-ptg"></p>
										</div>
										<div class="col-md-8 pie_txt_lbl">
											<p id="other-items" class="item-ptg"></p>
										</div>
										<div class="col-md-4 pie_txt_val">
											<p id="other-items-value" class="item-ptg"></p>
										</div>
									</div>
								</div>
							</div>
							<div class="row mt-4">
								<div class="col-md-6 week_chart">
									<div class='subtitle_chart'>Días de la Semana</div>
									<canvas id="barChart1" style="height: 400px" height="400"></canvas>
								</div>
								<div class="col-md-6 hours_chart">
									<div class='subtitle_chart'>Horas del día</div>
									<canvas id="barChart2" stle="height: 400px" height="400"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="col box_container">
							<div class="row">
								<div class="col">
									<img src="../icons/SVG/44-money/credit-card.svg" width="35px" />
									<p class="h3" style="display: inline-block;margin-left: 10px">Tipo de pago</p>
									<button class="infobox">
										<div>Información del total de ventas y total monto vendido por tipo de pago.</div>
										<i class="far fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div id="menuPaymentType" class="row menuPaymentType">
								<div class="col">
									<div id="pyVentas" class="PyTyActive">Por Ventas</div>
								</div>
								<div class="col">
									<div id="pyMonto">Por Monto</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="paytyp_cont_wrap table_style">
										<div>
											<img title="Efectivo" src="../icons/SVG/44-money/bank-notes-3.svg">
											<p><span id="cptp_1">0</span>%</p>
											<p>(<span id="cpval_1">0</span>)</p>
										</div>
										<div>
											<img title="Tarjeta de Crédito" src="../icons/SVG/44-money/credit-card-visa.svg">
											<p><span id="cptp_2">0</span>%</p>
											<p>(<span id="cpval_2">0</span>)</p>
										</div>
										<div>
											<img title="Tarjeta Débito" src="../icons/SVG/44-money/credit-card.svg">
											<p><span id="cptp_3">0</span>%</p>
											<p>(<span id="cpval_3">0</span>)</p>
										</div>
										<div>
											<img title="Otro" src="../icons/SVG/44-money/wallet-3.svg">
											<p><span id="cptp_4">0</span>%</p>
											<p>(<span id="cpval_4">0</span>)</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="col box_container">
							<div class="row">
								<div class="col">
									<img src="../icons/SVG/21-share/megaphone-2.svg" width="35px" />
									<p class="h3" style="display: inline-block;margin-left: 10px">Por vendedor</p>
									<button class="infobox">
										<div>Información del total de ventas y total monto vendido por vendedor.</div>
										<i class="far fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div id="menuVendedores" class="row menuPaymentType">
								<div class="col">
									<div id="vendVentas" class="PyTyActive">Por Ventas</div>
								</div>
								<div class="col">
									<div id="vendMonto">Por Monto</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div id=vendWrapItems class="paytyp_cont_wrap table_style"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Customer tab -->
			<div class="tab-pane fade tab_content" id="customer" role="tabpanel" aria-labelledby="customer-tab">
				<div class="row">
					<div class="col-md-4">
						<div class="row box_db2 cust_col1_boxp">
							<div class="col-sm-2 cust_col1_icon">
								<img src="../icons/SVG/08-users-actions/person-cash-1.svg" width="45px" style="margin-top: 10px" />
							</div>
							<div class="col-sm-10 pl-4">
								<p class="midsize-text"><span id="avg-txn-pc">$0</span></p>
								<p class="subtlt_db">Gasto por cliente</p>
							</div>
						</div>
						<div class="row box_db2 cust_col1_boxp">
							<div class="col-sm-2 cust_col1_icon">
								<img src="../icons/SVG/08-users-actions/person-information-1.svg" width="45px"
									style="margin-top: 10px" />
							</div>
							<div class="col-sm-10 pl-4">
								<p class="midsize-text" id="prod-per-txn">0</p>
								<p class="subtlt_db">Número de ítems por compras</p>
							</div>
						</div>
						<div class="row box_db2 cust_col1_boxp">
							<div class="col-sm-2 cust_col1_icon">
								<img src="../icons/SVG/07-users/account-up-down.svg" width="45px" style="margin-top: 10px" />
							</div>
							<div class="col-sm-10 pl-4">
								<p class="midsize-text" id="visits-per-month">0</p>
								<p class="subtlt_db">Número de visitas por mes</p>
							</div>
						</div>
						<div class="row box_db2 cust_col1_boxp">
							<div class="col-sm-2 cust_col1_icon">
								<img src="../icons/SVG/07-users/account-circle-1.svg" width="45px" style="margin-top: 10px" />
							</div>
							<div class="col-sm-10 pl-4">
								<p style="font-size: 20px; margin-bottom: 0;"><strong>Perfil del cliente</strong></p>
								<p class="subtlt_db"><span id="min-cust-age">0</span>-<span id="max-cust-age">100</span> Años. <span
										id="dom-gender">Hombre</span>.</p>
							</div>
						</div>
					</div>
					<div class="col-md-8 mw900">
						<div class="col-md-12 box_container">
							<div class="row mb-3">
								<div class="col">
									<img src="../icons/SVG/41-shopping/shopping-cart-full.svg" width="35px" />
									<p class="h3" style="display: inline-block;margin-left: 10px">Recurrencia</p>
									<button class="infobox">
										<div>Porcentaje de clientes recurrentes que visitaron más de una vez tu negocio en los últimos 6 meses.</div>
										<i class="far fa-question-circle"></i>
									</button>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6 cust_col2_icons">
									<div class="row">
										<div id="ptg-10" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-20" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-30" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-40" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-50" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
									</div>
									<div class="row" style="margin-top: 10px">
										<div id="ptg-60" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-70" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-80" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-90" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
										<div id="ptg-100" class="usr-ptg">
											<span class="fas fa-child fa-3x"></span>
										</div>
									</div>
								</div>
								<div class="col-md-6 cust_col2_txt">
									<div class="col-md-10 offset-md-1">
										<p style="color: #00bb7d; font-size:17px; font-weight: bold;"><span id="recurrent-ptg">0</span>% recurrentes</p>
										<p style="color: #666; font-size: 17px; font-weight: bold;"><span id="new-ptg">0</span>% no recurrentes</p>
										<p style="color: #999"><span id="no-info">0</span> ventas sin información de cliente.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="col box_container">

									<div class="row mb-3">
										<div class="col">
											<img src="../icons/SVG/07-users/hierarchy-business.svg" width="35px" />
											<p class="h3" style="display: inline-block;margin-left: 10px">Edad</p>
											<button class="infobox">
												<div>Número de clientes por edad.</div>
												<i class="far fa-question-circle"></i>
											</button>
										</div>
									</div>

									<div class="row pr-3 pl-3">								
										<div class="col-md-12 ageChart_wrap">
											<canvas id="barChart3" style="height: 350px" height="250"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">

								<div class="col box_container">

									<div class="row mb-3">
										<div class="col">
											<img src="../icons/SVG/07-users/gender-male-female.svg" width="35px" />
											<p class="h3" style="display: inline-block;margin-left: 10px">Género</p>
											<button class="infobox">
												<div>Porcentaje de clientes por género.</div>
												<i class="far fa-question-circle"></i>
											</button>
										</div>
									</div>

									<div class="row text-center">
										<div class="col-md-6 offset-md-3">
											<div class="row gender_perc">
												<div class="col-md-6">
													<img src="../icons/SVG/09-people/lady-1.svg" width="45px" />
												</div>
												<div class="col-md-6">
													<p class="h3" style="margin-top: 10px"><span id="female-ptg">0</span>%</p>
												</div>
											</div>
											<div class="row gender_perc">
												<div class="col-md-6">
													<img src="../icons/SVG/09-people/man.svg" width="45px" />
												</div>
												<div class="col-md-6">
													<p class="h3" style="margin-top: 10px"><span id="male-ptg">0</span>%</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							

						</div>
					</div>
				</div>
			</div>

		</div>

	</div>



	<script src="../js/js_dashboard.js?<?php echo time(); ?>"></script>
</body>

</html>