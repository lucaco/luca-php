<?php 
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);
$r = array();

if($_POST){

 	$email          = $_SESSION['login_user'];
 	$pos_name       = mysqli_real_escape_string($db,$_POST['pos_name']);
    $pos_profile    = mysqli_real_escape_string($db,$_POST['pos_profile']);
    $pos_id         = mysqli_real_escape_string($db,$_POST['pos_id']);
    $todays_date    = date('Y-m-d H:i:s'); 

    if (strlen($pos_name) > 0 && strlen($pos_profile) > 0 && strlen($pos_id) > 0){

        // Consultar si ya existe un POS en la tabla con ese nombre
        $query  = "SELECT pos_name, id_pos FROM points_of_sale WHERE bus_email = '$email' AND pos_name = '$pos_name'";
        $result = mysqli_query($db,$query);
        $count  = mysqli_num_rows($result);
        
        if($count >= 1){
            
            // Si ya existe el POS en la tabla, buscar si el perfil ingresado se encuentra asociado a él.
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $id_pos_ex  = $row['id_pos'];
    
            $query = "SELECT id_pos FROM pos_profiles WHERE bus_email = '$email' AND id_pos = '$id_pos_ex' AND profsessid = '$pos_profile'";
            $result  = mysqli_query($db, $query);
            $count2  = mysqli_num_rows($result);
            $row     = mysqli_fetch_array($result, MYSQLI_ASSOC);
    
            if ($count2 >= 1){
                // Si ese POS existe ya asociado al perfil entonces enviar mensaje diciendo que ya existe.        
                array_push($r, Array('E','El punto de venta con el nombre '. $pos_name .' ya se encuenta asociado al perfil. Ingresa otro nombre o cambia el perfil.'));
                echo json_encode($r);
            } else {
                // Si no hay un perfil asociado al POS entonces asociar el POS al perfil
                $query = "INSERT INTO pos_profiles (bus_email, id_pos, profsessid) VALUES ('$email','$id_pos_ex','$pos_profile');";
                mysqli_query($db, $query);
                if (mysqli_affected_rows($db)<=0){
                    array_push($r, Array('E','Ocurrió un error asociando el perfil al punto de venta.'));
                    echo json_encode($r);
                } else {
                    array_push($r, Array('S','El punto de ventas '.$pos_name.' fue asociado correctamente al perfil.'));
                    echo json_encode($r);
                }
            }
    
        }else{
            // Si el nombre del perfil ingresado no existe entonces agregarlo a la tabla de Puntos de venta ...
            $query = "INSERT INTO points_of_sale (bus_email, id_pos, pos_name, pos_creation_date, is_active) VALUES ('$email','$pos_id','$pos_name','$todays_date','1');";
            mysqli_query($db,$query);
    
            if (mysqli_affected_rows($db)<=0){
                array_push($r, Array('E','Ocurrió un error ingresando el punto de venta.'));
                echo json_encode($r);
            } else {
                // ... y Asociar el Punto de Venta al Perfil
                $query = "INSERT INTO pos_profiles (bus_email, id_pos, profsessid) VALUES ('$email','$pos_id','$pos_profile');";
                mysqli_query($db, $query);
                if (mysqli_affected_rows($db)<=0){
                    array_push($r, Array('E','Ocurrió un error asociando el perfil al punto de venta.'));
                    echo json_encode($r);
                } else {
                    array_push($r, Array('S','El punto de ventas '.$pos_name.' fue creado y asociado correctamente al perfil.'));
                    echo json_encode($r);
                }
            }
       } 
    } else {
        array_push($r, Array('E','Ingresa todos los campos.'));
        echo json_encode($r);
    }

 } 

?>