<?php
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
    // ===========================================================
    $bus_name  = $_SESSION['login_bus_name'];
	$bus_email = $_SESSION['login_user'];
    $id_bill        = mysqli_real_escape_string($db,$_POST["id_bill"]);
    $type_of_mail   = mysqli_real_escape_string($db,$_POST["type_of_mail"]);

    // type_of_mail = 1 -> Mail Normal
    // type_of_mail = 2 -> Resend Mail

    $points_system  = 700; // 1 point for each 700$ purshased
    // ===========================================================

    $return_arr = Array();

    if($id_bill==''){
        array_push($return_arr,Array('E','1'));
        echo json_encode($return_arr);
    }

    // ******* Obtener Información del Cliente y Vendedor dado el ID BILL ***********
    $sql = "SELECT id_bill, trx_date, trx_value, discount, taxes, tips, payment_type, other_payment_type, num_items, creditcard_compr_number, cash_change, IFNULL(B.cust_name,'') AS cust_name, IFNULL(B.cust_email,'') AS cust_email,  IFNULL(B.cust_key,'') AS cust_key, IFNULL(B.cust_gender,'') AS cust_gender, IFNULL(B.cust_id,'') AS cust_id, IFNULL(B.cust_type_id,'') AS cust_type_id, IFNULL(B.cust_points,'') AS cust_points, IFNULL(UCASE(C.prof_name),'') AS prof_name, IFNULL(C.avatar,'') AS prof_avatar, IFNULL(A.points_redeem,'0') AS points_redeem  FROM transactions A LEFT JOIN customers B ON A.bus_email = B.bus_email AND A.cust_key = B.cust_key LEFT JOIN profiles C ON A.bus_email = C.bus_email AND A.profsessid = C.profsessid WHERE A.bus_email = '$bus_email' AND A.id_bill = '$id_bill';";
    $results = mysqli_query($db, $sql);
    $count   = mysqli_num_rows($results);
    if($count == 1){

        $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
        $num_points_tot = $row['cust_points'];
        $name_person    = $row['cust_name'];
        $cust_email     = $row['cust_email'];
        $cust_gender    = $row['cust_gender'];
        $cust_id        = $row['cust_id'];
        $cust_key       = $row['cust_key'];
        $cust_type_id   = $row['cust_type_id'];
        $trx_date       = $row['trx_date'];
        $trx_value      = $row['trx_value'];
        $discount       = $row['discount'];
        $taxes          = $row['taxes'];
        $tips           = $row['tips'];
        $payment_type   = $row['payment_type'];
        $other_payment_type = $row['other_payment_type'];
        $creditcard_compr_number = $row['creditcard_compr_number'];
        $cash_change    = $row['cash_change'];
        $num_items      = $row['num_items'];
        $cp_name        = $row['prof_name'];
        $cp_avatar      = $row['prof_avatar'];
        $cp_avatar  = "https://www.2luca.co" . str_replace(array("..",".svg"),array("",".png"),$cp_avatar);
        $points_redeem  = $row['points_redeem'];

        if($cust_email==''){
            array_push($return_arr,Array('F','El cliente no tiene un email asociado.'));
            echo json_encode($return_arr);
        }else{        
            
            // ***** CREAR VECTOR JSONBILL ******
            $query = "SELECT *, IFNULL(B.prod_name,'Ítem') AS prod_name, IFNULL(B.prod_units,'Unidades') AS prod_units, IFNULL(B.prod_icon,'../icons/SVG/78-video-games/mario-question-box.svg') AS prod_icon FROM bills A LEFT JOIN products B ON A.item_id = B.prod_id WHERE A.bus_email = '$bus_email' AND A.id_bill = '$id_bill';";
            $result = mysqli_query($db,$query);
            $count  = mysqli_num_rows($result);
            $itemsTxt = '';
            $items_list = "'',";
            if($count>0){
                while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                    $d  = $row['prod_icon'];
                    $prod_icn  = "https://www.2luca.co" . str_replace(array("..","/SVG/",".svg"),array("","/PNG/",".png"),$d);
                    $n = $row['item_value'];
                    $itemAmountStyle = number_format($n);
                    $items_list = $items_list ."'". $row['item_id'] . "',";
                    $itemsTxt   = $itemsTxt . '{"itemId":"'.$row['item_id'].'","itemName":"'.$row['prod_name'].'","itemCount":"'.$row['item_count'].'","itemAmount":"'.$row['item_value'].'","itemAmountStyle":"'.$itemAmountStyle.'","itemDiscount":"'.$row['item_discount'].'","itemCoupon":"'.$row['item_coupon'].'","itemNote":"'.$row['item_note'].'","itemIcon":"'.$row['prod_icon'].'","itemIconHTTP":"'.$prod_icn.'","itemUnits":"'.$row['prod_units'].'"},'; 
                }
                $itemsTxt = rtrim($itemsTxt,',');
                $items_list = rtrim($items_list ,',');

                // ********** PRODUCT CLIENT BASED RECOMENDATION *************
                if(strlen($items_list)>0){
                    $items_list_sql_recom = "AND A.item_id NOT IN (".$items_list.")";
                }else{
                    $items_list_sql_recom = "";
                }
                
                $num_items_pull = 1;
                $query = "SELECT DISTINCT A.bus_email, A.item_id, A.affinity, B.prod_name, B.prod_icon FROM (SELECT * FROM recomendations WHERE bus_email = '$bus_email' AND cust_key='$cust_key') A LEFT JOIN (SELECT bus_email, prod_id, prod_name, prod_icon FROM products) B ON A.bus_email = B.bus_email AND A.item_id = B.prod_id WHERE prod_name IS NOT NULL ".$items_list_sql_recom." ORDER BY affinity DESC LIMIT $num_items_pull;";
                $result = mysqli_query($db,$query);
                $count  = mysqli_num_rows($result);
                $prod_name = '';
                $prod_icn  = '';
                $client_based_recom = array();
                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                    $client_based_recom[0] = array("prod_name" => $row['prod_name']);
                    $d  = $row['prod_icon'];
                    $prod_icn  = "https://www.2luca.co" . str_replace(array("..","/SVG/",".svg"),array("","/PNG/",".png"),$d);
                    $client_based_recom[1] = array("prod_icn" => $prod_icn);
                }
                
            }


            $subtotal_bill = $trx_value-$tips+$discount;
            $totalbeforetaxes = $subtotal_bill-$taxes;
            $points = floor($trx_value/$points_system);
            $paidbyuser = $trx_value+$cash_change;
            $jsonBill_txt = '{"total_items_bill":"'.$num_items.'","cust_key":"'.$cust_key.'","tax_bill":"'.$taxes.'","bill_id":"'.$id_bill.'","subtotal_bill":"'.$subtotal_bill.'","discount_bill":"'.$discount.'","tips_bill":"'.$tips.'","super_total_bill":"'.$trx_value.'","cash_change":"'.$cash_change.'","bill_print_type":"1","creditCardComprNum":"'.$creditcard_compr_number.'","otherPaymentOption":"'.$other_payment_type.'", "paymentType":"'.$payment_type.'","totalbeforetaxes":"'.$totalbeforetaxes.'","paidbyuser":"'.$paidbyuser.'","PointsRedem":"'.$points_redeem.'","PointThisBill":"'.$points.'","ItemsInBill":['.$itemsTxt.']}'; 

            $jsonBill = json_decode(stripslashes($jsonBill_txt),true);

            if($type_of_mail == 1){
                $subject = 'Tu recibo en '.$bus_name;
            }else if($type_of_mail == 2){
                $x = strtotime($trx_date);
                $tm_done =  $dias[date('w',$x)].", ".date('d',$x)." de ".$meses[date('n',$x)-1]. " del ".date('Y',$x) ;
                $subject = 'Tu recibo en '.$bus_name.' del '.$tm_done;
            }else{
                $subject = 'Tu recibo en '.$bus_name;
            }

            $country = $IP_country;
            $to = $cust_email;
            $okSend = SendMail_SMTP_InvoiceEmail($to,$subject,$name_person,$bus_name,$country,$jsonBill,$num_points_tot,$cp_name,$cp_avatar,$client_based_recom,$cust_key,$id_bill);

            array_push($return_arr,Array('S','1'));
            array_push($return_arr,Array($okSend));
            
            echo json_encode($return_arr);
        }

    }else{
        array_push($return_arr,Array('E','2'));
        echo json_encode($return_arr);
    }
}

//session_write_close();

?>