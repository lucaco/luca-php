<?php
header('Content-Type: text/html'); 
session_start();

include("config.php");
include("functions.php"); 
date_default_timezone_set($TimeZone);

if (isset($_SESSION['login_user']) && !empty($_SESSION['login_user'])){header("location:sales.php");}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title> 2Luca.co | Registro </title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">

	<!-- MY CLASSES -->
	<link type="text/css" rel="stylesheet" href="../css/general_classes.css?<?php echo time();?>">
	<link type="text/css" rel="stylesheet" href="../css/style_register.css?<?php echo time();?>">
	<script src="../js/js_home_functions.js?<?php echo time();?>"></script>
	<script src="../js/modals.js?<?php echo time();?>"></script>

	<!-- FONT AWESOME -->
	<script src="https://use.fontawesome.com/releases/v5.9.0/js/all.js" data-auto-replace-svg="nest"></script>

	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>

	<!-- ANIMATE.CSS -->
	<link href="../external_libs/animate.min.css">

</head>
<body>

	<div class="notify_alert"><span>x</span><div class="na_title"><i class="fas fa-bell"></i><span></span></div><div class="na_msg"></div></div>

	<div class="main_container">
		<div class="reg_logo_cont" id="logo_image">
			<img src="../logos/logo9.png">
		</div>

		<div class="reg_form_cont">
			<h1 class="title_1_font">Empecemos con el registro ...</h1>
			<h2>Ingresa los siguientes datos sobre tu nueva cuenta. <br> ¿Ya tienes una cuenta?, <span onclick="OpenLogInForm()">Ingresa aquí</span></h2>

			<form action="push_register_user.php" method="POST" name="RegForm" id="RegForm">

				<input type="text" name="reg-business-name" id="reg-business-name" placeholder="Nombre de tu negocio">
				<input type="email" name="reg-email" id="reg-email"  placeholder="Email">
				<input type="password" name="reg-pass" id="reg-pass" placeholder="Contraseña">
				<div class="pass_strg_cont" id="pass_strg_cont"><div class="pass_strg_cont_bar" id="pass_strg_cont_bar"><div id="pass_lvl_bar"></div></div><p id="pass_lvl">Débil</p></div>
				<input type="password" name="reg-pass-2" id="reg-pass-2" placeholder="Repite la contraseña">
				<select name="reg-industry" id="reg-industry" class="select_list_1"> 
					<option value="0" selected disabled hidden>Tipo de industria:</option>
					<option value='43'>Abogados</option>
					<option value='41'>Accesorios de celulares</option>
					<option value='40'>Accesorios de ropa</option>
					<option value='1'>Agricultura</option>
					<option value='37'>Alimentos Saludables</option>
					<option value='2'>Alimentos y bebidas</option>
					<option value='42'>Anteojos</option>
					<option value='3'>Arte y entretenimiento</option>
					<option value='4'>Artesanías</option>
					<option value='5'>Bar</option>
					<option value='35'>Barbería</option>
					<option value='51'>Café Internet</option>
					<option value='50'>Carnicería | Fama</option>
					<option value='6'>Construcción</option>
					<option value='39'>Dentista</option>
					<option value='25'>Deporte</option>
					<option value='7'>Educación</option>
					<option value='24'>Farmacia</option>
					<option value='33'>Ferretería</option>
					<option value='8'>Financiero</option>
					<option value='48'>Frutas y verduras</option>
					<option value='26'>Gimnasio</option>
					<option value='9'>Hotelería y Turismo</option>
					<option value='10'>Información</option>
					<option value='46'>Juguetería</option>
					<option value='30'>Lavado de vehículos</option>
					<option value='44'>Librería</option>
					<option value='11'>Mascotas y Animales</option>
					<option value='29'>Mecánico</option>
					<option value='52'>Miscelánea</option>
					<option value='49'>Panadería</option>
					<option value='45'>Papelería</option>
					<option value='34'>Peluquería</option>
					<option value='12'>Producción Audiovisual</option>
					<option value='28'>Productos de belleza</option>
					<option value='38'>Productos de Café</option>
					<option value='47'>Productos para bebé</option>
					<option value='31'>Puesto ambulante</option>
					<option value='13'>Restaurante</option>
					<option value='14'>Retail</option>
					<option value='15'>Retail Online</option>
					<option value='16'>Ropa y moda</option>
					<option value='17'>Salud</option>
					<option value='18'>Seguros</option>
					<option value='19'>Servicios Administrativos</option>
					<option value='27'>Spa</option>
					<option value='20'>Tecnología</option>
					<option value='21'>Tienda</option>
					<option value='22'>Transporte</option>
					<option value='23'>Ventas Independientes</option>
					<option value='32'>Viajes</option>
					<option value='36'>Videojuegos</option>   
				 </select>

				<span class="simpleMsg" id="simpleMsgRegister"></span>
                <span class="errorMsg" id="errorMsgRegister"></span>
				<div class="LoadingRoller" id="LoadingRoller_register"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
				<svg class="checkmark" id="checkmark_register" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
				<input type="submit" name="reg-submit" id="reg-submit" class="btn_style_2 bg_main_color_green" value="REGISTRARME">

				<p>Al dar click en este botón aceptas los <a href="https://medium.com/@2lucaco/t%C3%A9rminos-y-condiciones-9e4283b1d457" target="_blank">términos y condiciones</a> de nuestra página y la <a href="https://medium.com/blog-2luca/pol%C3%ADtica-de-privacidad-baec170a45e9" target="_blank">política de tratamiento de datos</a></p>

			</form>
		</div>
	</div>

	<div class="msg-box-container" id="msg-box-container">
		<div class="msg-box-1" id="msg-box-1">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseLogInForm()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> INGRESAR</p>

			<form action="" method="POST" name="login-form" id="login-form">
				<input type="email" name="login-email" id="login-email" value="<?php if(isset($_COOKIE["rem_login_user"])) { echo $_COOKIE["rem_login_user"]; } ?>" placeholder="Email" required>
				<input type="password" name="login-pass" id="login-pass" placeholder="Contraseña" required>
				<p class="msg-box-1-forgotpass"><a href="/php/forgotpassword.php">¿Olvidaste tu contraseña?</a></p>
				<div class="checkboxCont"><input type="checkbox" name='login-rememberme' id="login-rememberme" <?php if(isset($_COOKIE["rem_remember"])) { ?> checked <?php } ?>> <label for="login-rememberme">Recordarme</label></div>
				
				<span class="simpleMsg" id="simpleMsgLogin"></span>
        		<span class="errorMsg" id="errorMsgLogin"></span>
				<input type="submit" name="login-submit" id="login-submit" value="INGRESAR">
			</form>

		</div>	
	</div>

<script src="../js/js_register.js?<?php echo time();?>"></script>
</body>
</html>