<?php 
/* 
 * FILE: edit_profile.php
 * WHAT FOR: Edit profile register
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email      = $_SESSION['login_user'];
 	$prof_name      = mysqli_real_escape_string($db,$_POST['prof_name']);
    $prof_name_new  = mysqli_real_escape_string($db,$_POST['prof_name_new']);
    $prof_pass      = mysqli_real_escape_string($db,$_POST['prof_pass']);
    $avatar      	= mysqli_real_escape_string($db,$_POST['avatar']);
    $unprotectprofile = mysqli_real_escape_string($db,$_POST['unprotectprofile']);
    $items_1       	= mysqli_real_escape_string($db,$_POST['items_1']);
    $items_2       	= mysqli_real_escape_string($db,$_POST['items_2']);
    $items_3       	= mysqli_real_escape_string($db,$_POST['items_3']);
    $customers_1    = mysqli_real_escape_string($db,$_POST['customers_1']);
    $customers_2    = mysqli_real_escape_string($db,$_POST['customers_2']);
    $customers_3    = mysqli_real_escape_string($db,$_POST['customers_3']);
    $bills_1       	= mysqli_real_escape_string($db,$_POST['bills_1']);
    $bills_2       	= mysqli_real_escape_string($db,$_POST['bills_2']);
    $analytics_1    = mysqli_real_escape_string($db,$_POST['analytics_1']);
    $analytics_2    = mysqli_real_escape_string($db,$_POST['analytics_2']);
    
    $todays_date    = date('Y-m-d H:i:s');
    if($prof_pass == ''){
    	$passEncode = '';
    }else{
    	$passEncode = password_hash($prof_pass,PASSWORD_DEFAULT);
    }
       
	$query_v = "SELECT bus_email, prof_name, profsessid  FROM profiles WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
    $result = mysqli_query($db,$query_v);
    $count  = mysqli_num_rows($result);
      
    if($count == 1){
        if($unprotectprofile == '1'){
            $query = "UPDATE profiles SET prof_name='$prof_name_new', prof_pass='', avatar='$avatar', items_1='$items_1', items_2='$items_2', items_3='$items_3', customers_1='$customers_1', customers_2='$customers_2', customers_3='$customers_3', bills_1='$bills_1', bills_2='$bills_2', analytics_1='$analytics_1', analytics_2='$analytics_2' WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
        }else if($prof_pass == ''){
            $query = "UPDATE profiles SET prof_name='$prof_name_new', avatar='$avatar', items_1='$items_1', items_2='$items_2', items_3='$items_3', customers_1='$customers_1', customers_2='$customers_2', customers_3='$customers_3', bills_1='$bills_1', bills_2='$bills_2', analytics_1='$analytics_1', analytics_2='$analytics_2' WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
        }else{
            $query = "UPDATE profiles SET prof_name='$prof_name_new', prof_pass='$passEncode', avatar='$avatar', items_1='$items_1', items_2='$items_2', items_3='$items_3', customers_1='$customers_1', customers_2='$customers_2', customers_3='$customers_3', bills_1='$bills_1', bills_2='$bills_2', analytics_1='$analytics_1', analytics_2='$analytics_2' WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'"; 
        }
        mysqli_query($db,$query);
	    echo '1';
    }else{
        echo 'El perfil ' . $prof_name . ' no existe';
   } 
 } 
?>