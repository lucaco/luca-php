<?php
include("session.php");
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-135784524-1');
    </script>
	<title>Luca | Marketplace</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
	
	<!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_marketplace.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
    <script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>

</head>

<body>

    <div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>

    <div class="OffLineMsg" id="OffLineMsg">
        <div></div>
        <p>Sin conexión</p>
        <span class="mdi mdi-signal-off"></span>
    </div>

    <div id="MenuNavg" class="MenuNavg"></div>

    <div id="wrapper" class="wrapper">
        <div class="main_cont">
            <div class="head_title">
            <img src="https://www.2luca.co/icons/SVG/53-places/building-17.svg">
            <h1>Marketplace</h1>
            <h2>Elige los paquetes que se integren con las necesidades de tu negocio y comienza a crear soluciones personalizadas.</h2>
            </div>

            <div class="mp-cont">
            <div class="mp-wrap">
                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/85-office/file-text-chart.svg"></div>
                    <div>
                        <div>Analítica</div>
                        <div>Vende de forma inteligente</div>
                        <div id="mp_analiycs" class="tag luca_deg clickable">Conoce más</div>
                    </div>
                </div>

                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/82-school-science/science-robot-2.svg"></div>
                    <div>
                        <div>Chatbot</div>
                        <div>Tu propio asistente personal</div>
                        <div id="mp_chatbot" class="tag luca_deg clickable">Conoce más</div>
                    </div>
                </div>

                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/21-share/signal-person.svg"></div>
                    <div>
                        <div>Localizador</div>
                        <div>Conecta ordenes con clientes</div>
                        <div id="mp_tracker" class="tag luca_deg clickable">Conoce más</div>
                    </div>
                </div>
                
                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/74-food/food-truck.svg"></div>
                    <div>
                        <div>Domicilos</div>
                        <div>Conéctate la red de domiciliarios</div>
                        <div class="tag black5">Muy pronto</div>
                    </div>
                </div>

                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/43-shipping-delivery/box-1.svg"></div>
                    <div>
                        <div>Proveedores</div>
                        <div>Conéctate con la red de proveedores</div>
                        <div class="tag black5">Muy pronto</div>
                    </div>
                </div>

                <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/44-money/money-bag-dollar.svg"></div>
                    <div>
                        <div>Ayuda financiera</div>
                        <div>Solicita un prestamo</div>
                        <div class="tag black5">Muy pronto</div>
                    </div>
                </div>

                <!-- <div class="mp-el">
                    <div><img src="https://www.2luca.co/icons/SVG/44-money/piggy-bank.svg"></div>
                    <div>
                        <div>Fiar</div>
                        <div>Habilita la opción de fiar</div>
                        <div class="tag black5">Muy pronto</div>
                    </div>
                </div>                 -->

            </div>
            </div>

        </div>
    </div>

    <!-- MODAL ANALÍTICA -->
    <div class="msg-box-container" id="msg-box-container-mp_analitycs">
        <div class="msg-box-1" id="msg-box-mp_analitycs">
            <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseMpAnalytics()">X</p>

            <h1 class="mp_el_title">Analítica</h1>            
            <div class="mp_el_img mp_img_anal"><img src="https://www.2luca.co/icons/illustrations/mp_analytics2.svg"><span>1</span><span>0</span><span>1</span><span>1</span><span>0</span></div>            
            <div class="mp_el_desc">Con el módulo de analítica empieza a vender de forma inteligente e intuitiva. Agrega la opción de recomendaciones de productos a tus clientes, obtén información detallada del segmento de tus consumidores y recibe recomendaciones con la mejor acción a tomar para tus clientes dependiendo del perfil de cada uno.</div>
            <div id="mp_price_1" class="mp_el_price">Por solo: <span>$ 29,900/mes</span></div>
            <div id="btn_str_tst_1" class="mp_btn">Pruebalo gratis por 1 semana</div>
            <div id="btn_p_1" class="mp_btn">Comprar ahora</div>        
            <div id="wr_res_1"></div>

        </div>	
    </div>

    <!-- MODAL CHATBOT -->
    <div class="msg-box-container" id="msg-box-container-mp_chatbot">
        <div class="msg-box-1" id="msg-box-mp_chatbot">
            <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseMpChatbot()">X</p>

            <h1 class="mp_el_title">Chatbot</h1>
            <div class="mp_el_img mp_img_anal"><img src="https://www.2luca.co/icons/SVG/82-school-science/science-robot-1.svg"></div>
            <div class="mp_el_desc">Usa el Chatbot de Luca como tu asistente personal. Pregúntale lo que quieras sobre tu negocio, desde cuantos clientes tienes hasta generar reportes detallados de tus ventas. Además, realiza acciones sobre tus clientes y tus productos. No pierdas detalle de tus ventas con esta potente herramienta.</div>
            <div id="mp_price_2" class="mp_el_price">Por solo: <span>$ 19,900/mes</span></div>
            <div id="btn_str_tst_2" class="mp_btn">Pruebalo gratis por 1 semana</div>
            <div id="btn_p_2" class="mp_btn">Comprar ahora</div>        
            <div id="wr_res_2"></div>

        </div>	
    </div>

    <!-- MODAL LOCALIZADOR -->
    <div class="msg-box-container" id="msg-box-container-mp_tracker">
        <div class="msg-box-1" id="msg-box-mp_tracker">
            <p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseMpTracker()">X</p>
            
            <h1 class="mp_el_title">Localizador</h1>
            <div class="mp_el_img mp_img_anal"><img src="https://www.2luca.co/icons/SVG/21-share/signal-antenna-2.svg"></div>
            <div class="mp_el_desc">Con el sistema de localizador digital de Luca puedes conectar el estado de tus ordenes de compra directamente con tus clientes. Ellos dispondrán de un sistema digital de localización de pedidos una vez se genere la compra y de tener un teléfono móvil registrado recibirán un mensaje de texto una vez tu orden se encuentre terminada. <br></br> Reduce costos innecesarios de comprar un sistema físico de localizadores y avanza digitalmente conectando tus clientes con tu negocio.</div>
            <div id="mp_price_3" class="mp_el_price">Por solo: <span>$ 29,900/mes</span></div>
            <div id="btn_str_tst_3" class="mp_btn">Pruebalo gratis por 1 semana</div>
            <div id="btn_p_3" class="mp_btn">Comprar ahora</div>        
            <div id="wr_res_3"></div>

        </div>	
    </div>    

<script src="../js/js_marketplace.js?<?php echo time(); ?>"></script>
</body>

</html>
