<?php
include("session.php");
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Comandas</title>
  
  <script src="https://code.jquery.com/jquery.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
    <!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	    
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_comandas.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
    <script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
    <script src="../js/js_comandas.js?<?php echo time(); ?>"></script>

	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>

	
	
</head>
<body>

    <div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>

    <!––ORDER TOP-->
    <div class="top_kitchen_display">
    <div class="top_kitchen_display_1"> 
        <div class="top_kitchen_display_1a">
        <p><B> En Cola: </B> <span id='active_orders'></span> </p>
        <!--<p>Tasa: 2 ordenes/min</p> -->
        </div>  
        <div class="top_kitchen_display_1b">
        <p><B>Tomadas: </B> <span id='taked_orders'></span> </p>
        <!--<p>Tasa: 2 ordenes/min</p> -->
        </div>      
    </div>  
    <div class="top_kitchen_display_2"> 
        <div id='talk' onclick="talk_activate()"><img title="Anuncia" src="../icons/SVG/10-messages-chat/bubble-chat-check-2.svg"></div>  
        <div id='alarm' onclick="alert_activate()"><img title="Alerta" src="../icons/SVG/05-time/alarm-sound.svg"></div>  
        <div onclick="get_data_comandas(3)"><img title="Archivadas" src="../icons/SVG/47-applications/window-check.svg"></div>
        <div onclick="refresh_comnd()"><img title="Refrescar" src="../icons/general/basket-refresh.png"></div>
    </div>
    </div>

    <!––ORDER BOX-->
    <div class="body_kitchen_display" id="body_kitchen_display"> </div>

    <audio src="../audio/beep.wav" autostart="false" id="beepsound"></audio>

</body>
  
</html>