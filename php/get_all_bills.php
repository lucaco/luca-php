<?php
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$return_arr = Array();

	$bus_email = $_SESSION['login_user'];

    $query = "SELECT IFNULL(C.trx_date,'') AS trx_date, A.*, IFNULL(B.prod_name,'Ítem') AS prod_name, IFNULL(B.prod_unit_price,'') AS prod_unit_price, IFNULL(B.prod_units,'') AS prod_units, IFNULL(cust_id,'') AS cust_id, IFNULL(E.doc_short,'') AS doc_short, IFNULL(F.prof_name,'') AS prof_name, IFNULL(G.pym_name,'') AS pym_name FROM bills A LEFT JOIN products B ON A.item_id = B.prod_id AND A.bus_email = B.bus_email LEFT JOIN transactions C ON A.id_bill = C.id_bill AND A.bus_email = C.bus_email LEFT JOIN customers D ON A.bus_email = D.bus_email AND C.cust_key = D.cust_key LEFT JOIN document_type E ON D.cust_type_id = E.doc_id LEFT JOIN profiles F ON A.bus_email = F.bus_email AND C.profsessid = F.profsessid LEFT JOIN payment_type G ON C.payment_type = G.pym_id WHERE A.bus_email = '$bus_email' AND C.state = '1' ORDER BY trx_date DESC;";
    $result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
		array_push($return_arr,$row);
	}
	echo json_encode($return_arr);
}

//session_write_close();

?>