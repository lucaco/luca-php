<?php
include("session.php");
include("modals.js");
?>

<!doctype html>
<html lang="es">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135784524-1');
    </script>
	<title>Luca | Clientes</title>
	
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
    
	<!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_clients.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
    <script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS AND FONTAWESOME -->
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
    <script src="https://use.fontawesome.com/releases/v5.9.0/js/all.js" data-auto-replace-svg="nest"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>	
</head>

<body>

    <div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>
    <div class="bar_loading_cont"><div class="bar_loading"><div class="bars_wrap"><div class="bar bar1"></div><div class="bar bar2"></div><div class="bar bar3"></div><div class="bar bar4"></div><div class="bar bar5"></div><div class="bar bar6"></div></div><div class="bar_ld_msg"></div></div></div>
    <div id="MenuNavg" class="MenuNavg"></div>

    <div id="wrapper_grid" class="wrapper_grid">      
        <div class="main_bar">
            <div class="main_bar_wrap">
                <div onclick="OpenNewCustomer()"><img title="Nuevo Cliente" src="../icons/general/person-add-1.png"></div>
                <div onclick="export_customers_csv()"><img title="Exportar a CSV" src="../icons/SVG/23-data-transfer/download-harddrive-1.svg"></div>
                <!-- <div onclick="OpenFilterBills()"><img title="Filtrar" src="../icons/SVG/18-content/filter-1.svg"></div> -->
                <div onclick="resetFilters()"><img title="Refrescar" src="../icons/general/basket-refresh.png"></div>
            </div>
        </div>
        
        <div class="main_cont">
            <div class="header_bills">
                <h1>Clientes</h1>
                <div class="header_bills_1">
                    <p>Busca un cliente:</p>
                    <input name="search" id="search" class="msg-box-1-input" type="text" onkeyup="find_cust()" placeholder="ID o nombre ...">
                </div>
            </div>

            <div class="bills_wrap">
                <div class="preheader_table">
                    <div id="bills_summ_data" class="bills_summ_data">
                        <h3>Número de clientes: <span id="tot_header">0</span></h3>
                    </div>
                    <div id="reset_filter_content" class="reset_filter_content hide">
                        <div class="reset_filter" onclick="resetFilters()"><p>Eliminar filtro <span class="mdi mdi-filter-remove-outline"></span></p></div>
                    </div>
                </div>
                <div class="bills_cont" id="bills_cont">
                    <table id="table"> 
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>ID</th>         
                            </tr>
                        </thead>
                        <tbody id="cont_table_data" class="cont_table_data"></tbody>
                    </table>
                </div>
                <div class="noDataWrap" id="noClientsDataWrap"></div>
            </div>
            
        </div>

        <div class="main_overview">

            <div class="card card-tall3">
                <div class="panelcons_s1 table_style">
                    <div class="pn_1">
                            <div class="s1_c">
                                    <img title="Perfil de Usuario" src="../icons/SVG/07-users/account-circle-1.svg">
                            </div>
                    </div>
                    <div class="pn_2">
                            <div class="s1_b">
                                    <p id="infocust_name" class="data_info_hl wsNormal"></p>
                                    <p id="infocust_since" class="data_info_since"></p>
                                    <p id="red_pnt_cust" class="data_info_lucapnt"><span id="infocust_puntos"></span> Puntos</p>
                            </div>
                            <div class="s1_a">
                                    <img id = 'edit_customer' title="Editar" src="../icons/SVG/01-content-edition/pencil-circle.svg">
                                    <img id = 'delete_customer' title="Eliminar" src="../icons/SVG/01-content-edition/bin-1.svg">
                            </div>
                    </div>
				</div>
				<div class="panelcons_s2 table_style">
                    <div class="label_n_data w50p">
                            <p><span class="mdi mdi-card-account-details-outline"></span> <span id="infocust_id_type">ID</span></p>
                            <p id="infocust_id"> - </p>
                    </div>
                    <div class="label_n_data w50p">
                            <p><span class="mdi mdi-gift-outline"></span> Nacimiento</p>
                            <p id="infocust_bday"> - </p>
                    </div>
				</div>
				<div class="panelcons_s2 table_style">
                    <div class="label_n_data w50p">
                            <p><span class="mdi mdi-email-outline"></span> Email</p>
                            <p id="infocust_email" class="text_overflow_dots"> - </p>
                    </div>
                    <div class="label_n_data w50p">
                            <p><span class="mdi mdi-phone"></span> Teléfono</p>
                            <p id="infocust_tel"> - </p>
                    </div>
				</div>
				<div class="horizDivide w90p"></div>
				<div class="panelcons_s3">
                    <div id="s3_a" class="s3_a table_style">
                            <img src="../icons/SVG/41-shopping/shopping-cart-2.svg">
                            <p class="s3_a1">Últimas Compras</p>
                            <p id="s3_a2" class="s3_a2">Ver todas</p>
                    </div>
                    <div id="cont_last_orders_cust"></div>
				</div>
				<div class="horizDivide w90p"></div>
				<div class="panelcons_s4">
                    <!-- <button class="btn_soft_style" id="input_btn_analitica" onclick="OpenAnalyticsCust()">ANALÍTICA</button> -->
                    <button class="btn_soft_style" id="input_btn_recom">RECOMENDADOS</button>
				</div>
            </div>

            <div class="card">
                <div class="card-header"><div>Actividades</div><button class="infobox"><div>Indicadores de la actividad de compra del cliente.</div><i class="far fa-question-circle"></i></button></div>
                <div class="card-content">
                    <div class="card-icons">
                        <div title='Total de compras'><img src="../icons/SVG/41-shopping/shopping-cart-2.svg" alt="img1"><div id="icn_sales">0</div><div>Compras</div></div>
                        <div title='Total de ingresos'><img src="../icons/SVG/44-money/coin-bank-note.svg" alt="img2"><div id="icn_sales_val">$ 0</div><div>Ingresos</div></div>
                        <div title='Total de ítems comprados'><img src="../icons/SVG/41-shopping/shopping-bag-3.svg" alt="img3"><div id="icn_sales_items">0</div><div>Ítems</div></div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header"><div>Tipo de Pago</div><button class="infobox"><div>Preferencia de medio de pago del cliente en toda la historia.</div><i class="far fa-question-circle"></i></button></div>
                <div class="card-options"><div id='pay_perc_op'>Porcentaje</div><div id='pay_value_op'>Valor</div></div>
                <div class="card-content">
                    <div class="card-icons">
                        <div title='Efectivo'><img src="../icons/SVG/44-money/bank-notes-3.svg" alt="img1"><div id="tp_efectivo"></div><div>Efectivo</div></div>
                        <div title='Tarjeta de crédito'><img src="../icons/SVG/44-money/credit-card-visa.svg" alt="img1"><div id="tp_credito"></div><div>Crédito</div></div>
                        <div title='Tarjeta débito'><img src="../icons/SVG/44-money/credit-card.svg" alt="img1"><div id="tp_debito"></div><div>Débito</div></div>
                        <div title='Otro medio'><img src="../icons/SVG/44-money/wallet-3.svg" alt="img1"><div id="tp_otro"></div><div>Otro</div></div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header"><div>Visitas Promedio</div><button class="infobox"><div>Promedio de visitas que hace el cliente a tu negocio por semana/mes/año.</div><i class="far fa-question-circle"></i></button></div>
                <div class="card-options"><div id='last_y_ch1'>Último año</div><div id='last_m_ch1'>Último mes</div><div id='last_w_ch1'>Última semana</div></div>
                <div class="card-inline_indicator"><span id='sales_prom_indc'>-</span><span id='sales_prom_inc_name'>visitas</span></div>
                <div id='barChart-1' class="card-content"></div>
            </div>

            <div class="card">
                <div class="card-header"><div>Gasto Promedio</div><button class="infobox"><div>Promedio del gasto que hace el cliente a tu negocio por semana/mes/año.</div><i class="far fa-question-circle"></i></button></div>
                <div class="card-options"><div id='last_y_ch2'>Último año</div><div id='last_m_ch2'>Último mes</div><div id='last_w_ch2'>Última semana</div></div>
                <div class="card-inline_indicator"><span id='gasto_prom_indc'>-</span><span id='gasto_prom_inc_name'></span></div>
                <div id='barChart-2' class="card-content"></div>
            </div>

            <div id="anl_insg" class="card hide">
                <div class="card-header mb10"><div>Insignias</div><button class="infobox"><div>Reconocimientos que posee el cliente y por los cuales sobresale del resto de los clientes.</div><i class="far fa-question-circle"></i></button></div>
                <div id="badges_cust" class="badges_cust Data2_Style"></div>
                <div id="descr_badge" class="descr_badge hide"><p></p></div>
            </div>

            <div id='anl_seg' class="card hide">
                <div class="card-header mb10"><div>Segmento del cliente</div><button class="infobox"><div>Esta métrica te ayuda a hacer campañas de acuerdo a grupos homogéneos de clientes.</div><i class="far fa-question-circle"></i></button></div>
                <div id="segm_cont" class="segm_cont table_style">
                    <div class="segm_cust_box_cont">
                        <img id="segm_cut_icon" src="../icons/general/segm_6.svg">
                        <div id="segm_cust_box" class="segm_cust_box"></div>
                    </div>
                    <div id="segm_cust_desc" class="segm_cust_desc"></div>
                </div>
            </div>

            <div id='anl_nba' class="card hide">
                <div class="card-header mb10"><div>Siguiente Mejor Oferta</div><button class="infobox"><div>Representa la mejor acción recomendada en mercadeo para el cliente dado su perfil y actividad.</div><i class="far fa-question-circle"></i></button></div>
                <div id="nba_cont" class="nba_cont">
                    <div><img id="nba_icon" src="../icons/SVG/08-users-actions/group-chat.svg"></div>
                    <div><p id="nba_cust_txt"></p></div>
                </div>
            </div>


        </div>

    </div>


    <!-- MODAL FILTRAR -->
	<div class="msg-box-container" id="msg-box-container-filterbills">
		<div class="msg-box-1" id="msg-box-filterbills">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseFilterBills()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> FILTRAR CLIENTES</p>
			
			<div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por valor</div>
                <div class="ss1_content_table">
                        <div class="opt_selected" id="topym_1" onclick="SelectItemFilter(this)"><img alt='img1' title="Alto valor" src="../icons/SVG/44-money/bank-notes-3.svg"><p>Alto Valor</p></div>
                        <div class="opt_selected" id="topym_2" onclick="SelectItemFilter(this)"><img alt='img1' title="Bajo valor" src="../icons/SVG/44-money/credit-card-visa.svg"><p>Tarjeta Crédito</p></div>
                </div>
            </div>

            <div id="wrap_vendors_filter" class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por vendedor</div>
                <div id="content_vendors_filter" class="ss1_content_table"></div>                        
            </div>
            
            <div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por fecha</div>
                <div class="ss1_content filter_date_cont">
                        <p>Selecciona una ventana de tiempo</p>
                        <div>
                            <select name="filter_date_sel" id="filter_date_sel" class="select_list_1"> 
                                <option value="0" selected="">Todas las fechas</option>
                                <option value="1">Hoy</option>
                                <option value="2">Ayer</option>
                                <option value="3">Última semana</option>
                                <option value="4">Últimos 15 días</option>
                                <option value="5">Último mes</option>
                                <option value="6">Último año</option>
                                <option value="7">Este mes</option>
                                <option value="8">Este año</option>
                                <option value="9">Personalizada</option>
                            </select>
                        </div>

                        <div id="filter_date_from_cont">
                            <p>Desde:</p>
                            <input type="date" class="select_list_1" name="filter_date_from" id="filter_date_from" placeholder="Ej: 2018-01-01" min="1910-01-01" max="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                        </div>
                        <div id="filter_date_to_cont">
                            <p>Hasta:</p>
                            <input type="date" class="select_list_1" name="filter_date_to" id="filter_date_to" placeholder="Ej: 2018-12-01" min="1910-01-01" max="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                        </div>
                </div>
            </div>


            <div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por valor total</div>
                <div class="ss1_content filter_date_cont">
                    <p>Deja en blanco si no quieres filtrar por ese campo</p>
                    <div>
                        <p>Mayor o igual a:</p>
                        <input type="text" class="select_list_1 input_currency" name="filter_val_min" id="filter_val_min" placeholder="Ej: $ 100,000" min="0" max="100000000">
                    </div>
                    <div>
                        <p>Menor o igual a:</p>
                        <input type="text" class="select_list_1 input_currency" name="filter_val_max" id="filter_val_max" placeholder="Ej: $ 500,000" min="0" max="100000000">
                    </div>
                </div>
            </div>

            <span class="errorMsg" id="errorMsgfilterOrders"></span>
			<div class="LoadingRoller" id="LoadingRoller_filterOrders"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<button class="btn_green_full" id="btnFilterOrders">FILTRAR</button>
            <button class="btn_green_full" onclick="resetFilters()" id="btnFilterOrders">RESETEAR</button>

		</div>	
    </div>

    <!-- MODAL PRODUCT RECOMENDATION -->
	<div class="msg-box-container" id="msg-box-container-itemrecom">
		<div class="msg-box-1" id="msg-box-itemrecom">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemRecom()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title">RECOMENDADOS</p>
			<p class="msg-box-1-description" id="itemrecom_description"></p>
			<div class="msg-box-1-optionbox-container" id="itemrecom_items"></div>

		</div>	
    </div>
    
    <!-- MODAL CONSUMER ORDERS HISTORY -->
	<div class="msg-box-container" id="msg-box-container-consorders">
		<div class="msg-box-1" id="msg-box-consorders">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseConsOrders()">X</p>

                <p class="msgb-box-1-title" id="msgb-box-1-title">HISTÓRICO VENTAS</p>

                <div id="summ_bill_cons" class="summ_bill_cons table_style"></div>
                <div class="horizDivide"></div>                
                <div class="con_bill_header">
                    <div class="cbh1">Orden</div>
                    <div class="cbh2">Fecha</div>
                    <div class="cbh3">Valor</div>
                    <div class="cbh4"></div>
                </div>
                
                <div class="allord_wrap">
                    <div id="allord_cont" class="allord_cont table_style"></div>
                </div>
		</div>	
    </div>

    <!-- MODAL ITEMS BILL-->
    <div id="item-bills-container"></div>
    

<script src="../js/nlp_jcd.js?<?php echo time(); ?>"></script>
<script src="../js/js_clients.js?<?php echo time(); ?>"></script>
</body>
</html>	