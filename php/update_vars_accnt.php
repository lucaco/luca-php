<?php
/* 
 * FILE: update_vars_accnt.php
 * WHAT FOR: Update variables of user account
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email      = $_SESSION['login_user'];
    $bus_name       = trim(mysqli_real_escape_string($db,$_POST['bus_name']));
    $bus_address    = trim(mysqli_real_escape_string($db,$_POST['bus_address']));
    $bus_phone      = trim(mysqli_real_escape_string($db,$_POST['bus_phone']));
    $bus_nit        = trim(mysqli_real_escape_string($db,$_POST['bus_nit']));
    $bus_act_econ   = trim(mysqli_real_escape_string($db,$_POST['bus_act_econ']));
    $bus_industry   = trim(mysqli_real_escape_string($db,$_POST['bus_industry']));
    $bus_country    = trim(mysqli_real_escape_string($db,$_POST['bus_country']));
    $bus_city       = trim(mysqli_real_escape_string($db,$_POST['bus_city']));

    $r = Array();
    $query = "UPDATE register_users SET 
                bus_name = '$bus_name',
                bus_address = '$bus_address',
                bus_phone = '$bus_phone',
                bus_nit = '$bus_nit',
                bus_act_econ = '$bus_act_econ',
                bus_industry = '$bus_industry',
                bus_country = '$bus_country',
                bus_city = '$bus_city'
            WHERE bus_email = '$bus_email';";
            
    mysqli_query($db,$query);

    if(mysqli_affected_rows($db)<=0){
        array_push($r, Array('E','1'));
    }else{
        
        $_SESSION['bus_address'] = $bus_address;
        $_SESSION['bus_phone'] = $bus_phone;
        $_SESSION['bus_nit'] = $bus_nit;
        $_SESSION['login_bus_name'] = $bus_name;
        $_SESSION['login_bus_industry'] = $bus_industry;
        $_SESSION['bus_act_econ'] = $bus_act_econ;
        $_SESSION['bus_country'] = $bus_country;
        $_SESSION['bus_city'] = $bus_city;

        $ss_nit = $bus_nit;
        $ss_address = $bus_address;
        $ss_phone  = $bus_phone;
        $ss_name = $bus_name;
        $ss_industry = $bus_industry;
        $ss_act_econ = $bus_act_econ;
        $ss_country = $bus_country;
        $ss_city = $bus_city;

        array_push($r, Array('S','1'));
    }

    echo json_encode($r);

}
?>