<?php 
/*
 * FILE: send_invoice.php
 * WHAT FOR: Send email of invoice to customers
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
    $bus_name       = $_SESSION['login_bus_name'];
    $bus_email      = $_SESSION['login_user'];
    $cust_key       = mysqli_real_escape_string($db,$_POST['cust_key']);
    $jsonBillStr    = mysqli_real_escape_string($db,$_POST['jsonBillStr']);
    $jsonBill       = json_decode(stripslashes($jsonBillStr),true);
    $cp_name        = mysqli_real_escape_string($db,$_POST['cp_name']);
    $cp_avatar      = mysqli_real_escape_string($db,$_POST['cp_avatar']);

    $todays_date    = date('Y-m-d H:i:s');
    $profKey        = generateRandomString(30);

    $id_bill = generateRandomString(12);

    // Obtener Información del Cliente dada su llave
    $sql = "SELECT * FROM customers WHERE bus_email = '$bus_email' and cust_key = '$cust_key'";
    $results = mysqli_query($db, $sql);
    $count   = mysqli_num_rows($results);
    if($count == 1){
        $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
        $num_points_tot = $row['cust_points'];
        $name_person    = $row['cust_name'];
        $cust_email     = $row['cust_email'];
        $cust_gender    = $row['cust_gender'];
        $cust_id        = $row['cust_id'];
        $cust_type_id   = $row['cust_type_id'];
    }
        
    // PRODUCT CLIENT BASED RECOMENDATION
    $num_items_pull = 1;
    $query = "SELECT DISTINCT A.bus_email, A.item_id, A.affinity, B.prod_name, B.prod_icon FROM (SELECT * FROM recomendations WHERE bus_email = '$bus_email' AND cust_key='$cust_key') A LEFT JOIN (SELECT bus_email, prod_id, prod_name, prod_icon FROM products) B ON A.bus_email = B.bus_email AND A.item_id = B.prod_id WHERE prod_name IS NOT NULL ORDER BY affinity DESC LIMIT $num_items_pull;";    
    $result = mysqli_query($db,$query);
    $count  = mysqli_num_rows($result);
    $prod_name = '';
    $prod_icn  = '';
    $client_based_recom = array();
    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        $client_based_recom[0] = array("prod_name" => $row['prod_name']);
        $d  = $row['prod_icon'];
        $prod_icn  = "https://www.2luca.co" . str_replace(array("..","/SVG/",".svg"),array("","/PNG/",".png"),$d);
        $client_based_recom[1] = array("prod_icn" => $prod_icn);
    }
    
    $subject = 'Tu recibo en '.$bus_name;
    $to = $cust_email;
    $okSend = SendMail_SMTP_InvoiceEmail($to,$subject,$name_person,$bus_name,$IP_country,$jsonBill,$num_points_tot,$cp_name,$cp_avatar,$client_based_recom,$cust_key,$id_bill);
    echo $okSend;
 } 

?>