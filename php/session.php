<?php
	include("functions.php");
	include("config.php");
	date_default_timezone_set($TimeZone);
	
	session_start();
	$time = $_SERVER['REQUEST_TIME'];
	$timeout_duration = 60*60*24;
	$timeOut = 0;
    
	if (!isset($_SESSION['login_user'])){
	 	header("location:../index.php");
	}else{

		$ss_email 				= $_SESSION['login_user'];
		$ss_name 				= $_SESSION['login_bus_name'];
		$ss_industry 			= $_SESSION['login_bus_industry'];
		$ss_creation_date 		= $_SESSION['login_creation_date'];
		$ss_active 				= $_SESSION['login_active'];
		$ss_avatar_type 		= $_SESSION['avatar_type'];
		$ss_user_folder_name 	= $_SESSION['user_folder_name'];
		$ss_country 			= $_SESSION['bus_country'];
		$ss_city 				= $_SESSION['bus_city'];
		$ss_hash                = $_SESSION['hash_act'];

		$ss_user_since = diff_dates_str($ss_creation_date,$IP_data_date);

		if($_SESSION['bus_address']==""){
			$ss_address = "No Asignado";
		}else{
			$ss_address = $_SESSION['bus_address'];
		}
		if ($_SESSION['bus_phone'] == "") {
			$ss_phone = "No Asignado";
		} else {
			$ss_phone = $_SESSION['bus_phone'];
		}
		
		if ($_SESSION['bus_nit'] == "") {
			$ss_nit = "No Asignado";
		} else {
			$ss_nit = $_SESSION['bus_nit'];
		}
		
		if ($_SESSION['bus_act_econ'] == "") {
			$ss_act_econ = "No Asignado";
		} else {
			$ss_act_econ = $_SESSION['bus_act_econ'];
		}

        //if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
        //    $timeOut = 1;
        //    session_unset();
        //    session_destroy();
        //    session_start();
        //}
        
        $_SESSION['LAST_ACTIVITY'] = $time;
        
        //if($timeOut==1){
        //    header("location:../index.php");
        //}
		
	}

?>