<?php
/* 
 * FILE: get_vars_profname.php
 * WHAT FOR: Get all information about profile by profile name
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 		= $_SESSION['login_user'];
	$prof_name 		= mysqli_real_escape_string($db,$_POST['prof_name']);
	$todays_date    = date('Y-m-d H:i:s');
	
	$query = "SELECT prof_name, avatar, CASE WHEN LENGTH(prof_pass)>3 THEN 1 ELSE 0 END AS has_pass, items_1, items_2, items_3, customers_1, customers_2, customers_3, bills_1, bills_2, analytics_1, analytics_2 FROM profiles WHERE bus_email = '$bus_email' AND prof_name='$prof_name'";
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
    
    $return_arr = Array();
    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        array_push($return_arr,$row);
    }
	
	echo json_encode($return_arr);
}
?>
