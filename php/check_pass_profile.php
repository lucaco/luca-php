<?php
/* 
 * FILE: check_pass_profile.php
 * WHAT FOR: Check the password of 4 digits necessary to access to a profile of salesmen
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 	= $_SESSION['login_user'];
	$prof_name 	= mysqli_real_escape_string($db,$_POST['prof_name']);
	$pass 	    = mysqli_real_escape_string($db,$_POST['pass']);
		
	$query = "SELECT prof_pass FROM profiles WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
    $result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	
	if($count > 0){
		while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
			$pet = $row['prof_pass'];
		}
		$pass_encode = password_verify($pass, $pet);		
		if($pass_encode == TRUE){
			echo 's-0'; // Contraseña Correcta
		}else{
			echo 'e-1'; // Contraseña Incorrecta
		};
	}else{
		echo 'e-0'; // El usuario no existe;
	}


}
?>
