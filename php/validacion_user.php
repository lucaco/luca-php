<?php

include("config.php");
include("functions.php");
date_default_timezone_set($TimeZone);

session_start();
if (isset($_SESSION['login_user'])){header("location:sales.php");}


if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['email']) && isset($_GET['hash']) ) {

	$email = mysqli_real_escape_string($db,$_GET['email']);
	$hash  = mysqli_real_escape_string($db,$_GET['hash']);

	// Revisar que el usuario se creo correctamente
	$query = "SELECT * FROM register_users WHERE bus_email = '$email'";
	$result = mysqli_query($db,$query);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$hash_act = $row['hash_act'];
	$bus_name = $row['bus_name'];
	$active   = $row['active'];

	$bus_address   		= $row['bus_address'];
	$bus_phone     		= $row['bus_phone'];
	$bus_nit       		= $row['bus_nit'];
	$bus_act_econ  		= $row['bus_act_econ'];
	$user_folder_name = $row['user_folder_name'];

	$count = mysqli_num_rows($result);

	if($count == 1) {
		if($hash_act == $hash){
			if($active=="1"){
				header("location: https://www.2luca.co");
			}
		}else {
			header("location: https://www.2luca.co");
		}
	}else{
		header("location: https://www.2luca.co");
	}
}else{
	header("location: https://www.2luca.co");
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>2Luca | Activación</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Galada|Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

	<link rel="stylesheet" href="../external_libs/animate.min.css">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_validacion_user.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>

	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>

</head>
<body>

	<div class="notify_alert"><span>x</span><div class="na_title"><i class="fas fa-bell"></i><span></span></div><div class="na_msg"></div></div>
	
	<div class="bar_loading_cont">
		<div class="bar_loading">
			<div class="bars_wrap">
				<div class="bar bar1"></div>
				<div class="bar bar2"></div>
				<div class="bar bar3"></div>
				<div class="bar bar4"></div>
				<div class="bar bar5"></div>
				<div class="bar bar6"></div>
			</div>
			<div class="bar_ld_msg"></div>
		</div>
	</div>

	<div id="container_step_1" class="main_container animated fadeIn">

		<div class="reg_logo_cont" id="logo_image">
			<a href="../index.php"><img src="../logos/logo9.png"></a>
		</div>

		<div class="reg_form_cont">
			<h1>¡Solo un paso más para activar tu cuenta!</h1>
			<h2>Bienvenido <b><?php echo $bus_name; ?></b>. <br> Por favor llena estos campos con la información de tu negocio. Esta información es completamente confidencial y es esencial para poder registrar tus ventas, además de poder brindar una mejor experiencia tanto a ti como a tu negocio.</h2>

			<div class="form_wrap">

				<input type="text" name="act_email" id="act_email" value="<?php echo $email; ?>" hidden disabled>
				<input type="text" name="act_name" id="act_name" value="<?php echo $bus_name; ?><" hidden disabled>
				<input type="text" name="act_hash" id="act_hash" value="<?php echo $hash; ?>" hidden disabled>
				<input type="text" class="msg-box-1-input" name="act_dir" id="act_dir" placeholder="*Dirección" value="">
				<select id="newCountry" name="newCountry" class="select_list_1 mw400 country_input"></select>
				<input type="text" class="msg-box-1-input" name="newCity" id="newCity" placeholder="*Ciudad">
				<input type="text" class="msg-box-1-input"  name="act_nit" id="act_nit" placeholder="*ID (NIT, cédula,...)" value="">
				<input type="text" class="msg-box-1-input input_phone"  name="act_tel" id="act_tel" placeholder="Télefono" value="">
				<select name="act_tip_act_ecom" id="act_tip_act_ecom" class="select_list_1 mw400">
					<option value="0" selected disabled hidden>Actividad Económica:</option>
					<option value="1">Régimen Común</option>
					<option value="2">Régimen Simplificado</option>
				</select>

				<span class="errorMsg" id="errorMsgValidation"></span>
				<input type="submit" name="act_submit" id="act_submit" value="ACTIVAR LA CUENTA" class="btn_style_2 bg_main_color_green">
			</div>

		</div>

	</div>

<script src="../js/js_validacion_user.js?<?php echo time(); ?>"></script>
</body>
</html>
