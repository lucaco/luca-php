<?php
include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);
date_default_timezone_set('America/Bogota');

if($_POST){
	
	$login_email     = mysqli_real_escape_string($db,$_POST['login_email']); 
	$login_pass      = mysqli_real_escape_string($db,$_POST['login_pass']);
	$login_remember  = mysqli_real_escape_string($db,$_POST['login_remember']);

	$todays_date     = date('Y-m-d H:i:s');

	// Revisar que el usuario y constraseñas son correctos
	$query  = "SELECT * FROM register_users WHERE bus_email = '$login_email'";
	$result = mysqli_query($db,$query);
	$row    = mysqli_fetch_array($result, MYSQLI_ASSOC);

	$bus_name          	= $row['bus_name'];
	$pass_enc          	= $row['pass_enc'];
	$bus_industry      	= $row['bus_industry'];
	$creation_date     	= $row['creation_date'];
	$active            	= $row['active'];			// 1: User Active - 0: User Inactive
	$user_gone   		= $row['user_gone'];		// 1: User Not unregisted him/her self  - 0: User still Active
	$bus_address       	= $row['bus_address'];
	$bus_phone         	= $row['bus_phone'];
	$bus_nit           	= $row['bus_nit'];
	$bus_act_econ      	= $row['bus_act_econ'];
	$user_folder_name  	= $row['user_folder_name'];
	$avatar_type 		= $row['avatar_type'];		// 0: Imagen - 1: Logo de Galeria
	$bus_country 		= $row['bus_country'];
	$bus_city 			= $row['bus_city'];
	$hash_act 			= $row['hash_act'];
	
	$bus_email = $login_email;
	$count     = mysqli_num_rows($result);

	if($count == 1 and $user_gone == 0) {
		// El usuario existe 
		if (password_verify($login_pass, $pass_enc)) {

			// ACTUALIZAR SESION FECHA 
			$query = "UPDATE register_users SET last_mod_date = '$todays_date' WHERE bus_email ='$bus_email'";
			mysqli_query($db,$query);

			if ($active == "0"){
				$php_warning = "La cuenta ". $login_email ." no se ha activado aún. Revisa tu correo electrónico para activar tu cuenta y luego de activarla vuelve a ingresar.";
			}else{
				session_start();
				$_SESSION['login_user'] 			= $login_email;
				$_SESSION['login_bus_name'] 		= $bus_name;
				$_SESSION['login_bus_industry'] 	= $bus_industry;
				$_SESSION['login_creation_date'] 	= $creation_date;
				$_SESSION['login_active'] 			= $active;
				$_SESSION['bus_address']	 		= $bus_address;
				$_SESSION['bus_phone'] 				= $bus_phone;
				$_SESSION['bus_nit'] 				= $bus_nit;
				$_SESSION['bus_act_econ'] 			= $bus_act_econ;
				$_SESSION['user_folder_name'] 		= $user_folder_name;
				$_SESSION['avatar_type'] 			= $avatar_type; 
				$_SESSION['bus_country'] 			= $bus_country;
				$_SESSION['bus_city'] 				= $bus_city;
				$_SESSION['hash_act'] 				= $hash_act;

				$time_cookies = (86400 * 30 * 12);
				
				if ($login_remember == "true"){
					setcookie("rem_login_user", $login_email, time() + $time_cookies, "/", "2luca.co");
					setcookie("rem_remember", "1", time() + $time_cookies, "/", "2luca.co");					
				} else {
					setcookie("rem_login_user", $login_email, time() - $time_cookies, "/", "2luca.co");
					setcookie("rem_remember", "0", time() - $time_cookies, "/", "2luca.co");
				}

				$php_warning = "1";
			}

		} else {
		    $php_warning = 'La contraseña no es válida.';
		}

	}else {
		// EL usuario no existe
		$php_warning = 'El usuario no existe';
	}
	
	echo $php_warning;
	
	
}
?>