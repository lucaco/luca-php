<?php

$url = 'https://api-luca.herokuapp.com/api/v1/model/chatbot/predict?key_id=7BZubyla4N6bo1wqDzc9&query=Hola';
// $data = ['data' => 'this', 'data2' => 'that'];
$headers = [
    'Content-Type: application/json'
];

// open connection
$ch = curl_init();

// set curl options
$options = [
    CURLOPT_URL => $url,
    //CURLOPT_POST => count($data),
    //CURLOPT_POSTFIELDS => http_build_query($data),
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_RETURNTRANSFER => true,
];
curl_setopt_array($ch, $options);

// execute
$result = curl_exec($ch);

// close connection
curl_close($ch);


echo $result;

//{"answer":"\u00a1Hola! \ud83d\udc4b","predict_class":"saludo","predict_proba":0.9988530874252319,"query":"Hola"}

// -----------------------------------------------------


// require __DIR__ . '/vendor/twilio-php-master/src/Twilio/autoload.php';

// // Use the REST API Client to make requests to the Twilio REST API
// use Twilio\Rest\Client;

// // Your Account SID and Auth Token from twilio.com/console
// $sid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
// $token = 'your_auth_token';
// $client = new Client($sid, $token);

// // Use the client to do fun stuff like send text messages!
// $client->messages->create(
//     // the number you'd like to send the message to
//     '+15558675309',
//     [
//         // A Twilio phone number you purchased at twilio.com/console
//         'from' => '+15017250604',
//         // the body of the text message you'd like to send
//         'body' => "Hey Jenny! Good luck on the bar exam!"
//     ]
// );




// error_reporting(E_ALL);

// /* Permitir al script esperar para conexiones. */
// set_time_limit(0);

// /* Activar el volcado de salida implícito, así veremos lo que estamos obteniendo mientras llega. */
// ob_implicit_flush();

// /* Obtener el puerto para el servicio WWW. */
// $port = getservbyname('www', 'tcp');

// /* Obtener la dirección IP para el host objetivo. */
// $address = gethostbyname('www.2luca.co');

// if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
//   echo "socket_create() falló: razón: " . socket_strerror(socket_last_error()) . "\n";
// }

// echo "Intentando conectar a '$address' en el puerto '$port'...";
// $result = socket_connect($sock, $address, $port);
// if ($result === false) {
//     echo "socket_connect() falló.\nRazón: ($result) " . socket_strerror(socket_last_error($sock)) . "\n";
// }

// $msg = 'Hola todo bien?';
// socket_write($sock, $msg, strlen($msg));

// // read a line from the socket
// $line = socket_read($sock, 1024, PHP_NORMAL_READ);
// if(substr($line, -1) === "\r") {
//   // read/skip one byte from the socket
//   // we assume that the next byte in the stream must be a \n.
//   // this is actually bad in practice; the script is vulnerable to unexpected values
//   socket_read($sock, 1, PHP_BINARY_READ);
// }

// $message = parseLine($line);
// echo $message;

// socket_close($sock);

// // Bind socket to port and host address
// if (socket_bind($sock, $address, $port) === false) {
//   echo "socket_bind() falló: razón: " . socket_strerror(socket_last_error($sock)) . "\n";
// } else {
//     echo "OK";
// }



// // start listening the port
// if (socket_listen($socket, 5) === false) {
//   echo "socket_listen() falló: razón: " . socket_strerror(socket_last_error($socket)) . "\n";
// }else{
//   echo "OK";
// }

#socket_close();


// if (isset($_FILES['userfile'])){
//   print_r($_FILES);
// }
// session_start();
// require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
// use google\appengine\api\cloud_storage\CloudStorageTools;
// $fileName = 'gs://2luca.co/'.$_FILES['uploaded_files']['name'];
// echo $fileName."<br>";
// $options = array('gs'=>array('acl'=>'public-read','Content-Type' => $_FILES['uploaded_files']['type']));
// $ctx = stream_context_create($options);
// if (false == rename($_FILES['uploaded_files']['tmp_name'], $fileName, $ctx)) {
//   die('Could not rename.');
// }
// $object_public_url = CloudStorageTools::getPublicUrl($fileName, true);
// echo $object_public_url."<br>";
// $filename = $_FILES['uploaded_files']['name'];
// echo $filename."<br>";
// $filesize = $_FILES['uploaded_files']['size'];
// echo $filesize."<br>";

?>

<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
<!-- 
    <form action="" method="POST" role="form" enctype="multipart/form-data">
      <legend>Form title</legend>
    
      <div class="form-group">
        <label for="">Archivo</label>
        <input type="file" class="form-control" id="userfile" name="userfile">
      </div>
    
    
      <button type="submit" class="btn btn-primary">Submit</button>
    </form> -->
    
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </body>
</html>
