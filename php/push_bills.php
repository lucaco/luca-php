<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_id   = $_SESSION['login_user'];
	$sqlInput = mysqli_real_escape_string($db,$_POST["sqlInput"]);
	$bill_id = mysqli_real_escape_string($db,$_POST["bill_id"]);

	$data = json_decode(stripslashes($sqlInput), true);

    if(sizeof($data)>0){
     	$inputValues = "";
    	$num_items = 0;
    	foreach($data as $d){
    	    $num_items = $num_items + 1;
    	    if($num_items < sizeof($data)){
    	       $inputValues = $inputValues . "('".$bus_id."', '".$bill_id."', '".$d['itemId']."', '".$d['itemAmount']."', '".$d['itemCount']."', '".$d['itemDiscount']."', '".$d['itemCoupon']."', '".$d['itemNote']."'),"; 
    	    }else{
    	        $inputValues = $inputValues . "('".$bus_id."', '".$bill_id."', '".$d['itemId']."', '".$d['itemAmount']."', '".$d['itemCount']."', '".$d['itemDiscount']."', '".$d['itemCoupon']."', '".$d['itemNote']."')"; 
    	    }
            
        }
    
    	$sql = "INSERT INTO bills (bus_id, id_bill, item_id, item_value, item_count, item_discount, item_coupon, item_note) VALUES ";
        $sql = $sql.$inputValues;   
        mysqli_query($db,$sql);
    }else{
        echo "ERROR";
    }


}

?>