<?php
include("session.php");
include("modals.js");
?>

<!doctype html>
<html lang="es">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135784524-1');
    </script>
	<title>Luca | Cuentas</title>
	
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
    
	<!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="../css/style_bills.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
    <script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>

	
			
</head>

<body>

    <div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>

    <div class="OffLineMsg" id="OffLineMsg">
        <div></div>
        <p>Sin conexión</p>
        <span class="mdi mdi-signal-off"></span>
    </div>

    <div id="MenuNavg" class="MenuNavg"></div>

    <div class="main_bar">
        <div class="main_bar_wrap">
            <div id="cmd_lnk" class="cmd_lnk"><div class="btn_green_full"><span class="mdi mdi-file-document-outline"></span> COMANDAS</div></div>
            <div onclick="export_trx_csv()"><img title="Exportar a CSV" src="../icons/SVG/23-data-transfer/download-harddrive-1.svg"></div>
            <div onclick="OpenFilterBills()"><img title="Filtrar" src="../icons/SVG/18-content/filter-1.svg"></div>
            <div onclick="resetFilters()"><img title="Refrescar" src="../icons/general/basket-refresh.png"></div>
        </div>
    </div>

<div id="wrapper" class="wrapper">  

    <div class="main_cont">
        <div class="header_bills">
            <h1>Transacciones</h1>
            <div class="header_bills_1">
                <p>Busca una cuenta:</p>
                <input name="search_bill" id="search_bill" class="msg-box-1-input" type="text" onkeyup="find_bill()" placeholder="Ingresa el ID ...">
            </div>
            <div class="header_bills_2">
                <p>Buscar por:</p>
                <select name="select_type_search" id="select_type_search" class="select_list_1"> 
                    <option value="td_id_bill" selected>ID</option>
                    <option value="td_date">Fecha (AAAA-MM-DD)</option>
                    <option value="td_cust_name">Nombre Cliente</option>
                    <option value="td_cust_id">ID Cliente</option>
                    <option value="td_vendor">Vendedor</option>
                </select>
            </div>
        </div>

        <div class="bills_wrap">
            <div class="preheader_table">
                <div id="bills_summ_data" class="bills_summ_data">
                    <h3>Número de cuentas: <span id="tot_bills_header">0</span></h3>
                    <h3>Valor Total: $ <span id="tot_value_header">0</span></h3>
                </div>
                <div id="reset_filter_content" class="reset_filter_content hide">
                    <div class="reset_filter" onclick="resetFilters()"><p>Eliminar filtro <span class="mdi mdi-filter-remove-outline"></span></p></div>
                </div>
            </div>
            <div class="bills_cont" id="bills_cont">
                <table id="bills_table"> 
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>Valor</th>
                            <th>Tipo Pago</th>          
                        </tr>
                    </thead>
                    <tbody id="cont_table_billsdata" class="cont_table_billsdata"></tbody>
                </table>
            </div>
            <div class="noDataWrap" id="noOrdersDataWrap"></div>
        </div>
        
    </div>

</div>

    <!-- MODAL ITEMS BILL-->
	<div class="msg-box-container" id="msg-box-container-itemsbill">
		<div class="msg-box-1" id="msg-box-itemsbill">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemsBill()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> ÍTEMS</p>
			
			<center>
                <div class="items_bill_cont">
                    <div id="items_bill_wrap" class="items_bill_wrap"></div>
                </div>
			</center>

		</div>	
	</div>

    <!-- MODAL ITEMS BILL-->
	<div class="msg-box-container" id="msg-box-container-filterbills">
		<div class="msg-box-1" id="msg-box-filterbills">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseFilterBills()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> FILTRAR CUENTAS</p>
			
			<div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por tipo de pago</div>
                <div class="ss1_content_table">
                        <div class="opt_selected" id="topym_1" onclick="SelectItemFilter(this)"><img title="Efectivo" src="../icons/SVG/44-money/bank-notes-3.svg"><p>Efectivo</p></div>
                        <div class="opt_selected" id="topym_2" onclick="SelectItemFilter(this)"><img title="Tarjeta de Crédito" src="../icons/SVG/44-money/credit-card-visa.svg"><p>Tarjeta Crédito</p></div>
                        <div class="opt_selected" id="topym_3" onclick="SelectItemFilter(this)"><img title="Tarjeta Débito" src="../icons/SVG/44-money/credit-card.svg"><p>Tarjeta Débito</p></div>
                        <div class="opt_selected" id="topym_4" onclick="SelectItemFilter(this)"><img title="Otro" src="../icons/SVG/44-money/wallet-3.svg"><p>Otro</p></div>
                </div>
            </div>

            <div id="wrap_vendors_filter" class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por vendedor</div>
                <div id="content_vendors_filter" class="ss1_content_table"></div>                        
            </div>
            
            <div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por fecha</div>
                <div class="ss1_content filter_date_cont">
                        <p>Selecciona una ventana de tiempo</p>
                        <div>
                            <select name="filter_date_sel" id="filter_date_sel" class="select_list_1"> 
                                <option value="0" selected="">Todas las fechas</option>
                                <option value="1">Hoy</option>
                                <option value="2">Ayer</option>
                                <option value="3">Última semana</option>
                                <option value="4">Últimos 15 días</option>
                                <option value="5">Último mes</option>
                                <option value="6">Último año</option>
                                <option value="7">Este mes</option>
                                <option value="8">Este año</option>
                                <option value="9">Personalizada</option>
                            </select>
                        </div>

                        <div id="filter_date_from_cont">
                            <p>Desde:</p>
                            <input type="date" class="select_list_1" name="filter_date_from" id="filter_date_from" placeholder="Ej: 2018-01-01" min="1910-01-01" max="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                        </div>
                        <div id="filter_date_to_cont">
                            <p>Hasta:</p>
                            <input type="date" class="select_list_1" name="filter_date_to" id="filter_date_to" placeholder="Ej: 2018-12-01" min="1910-01-01" max="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                        </div>
                </div>
            </div>


            <div class="msg-box-1-sectionsty1">
                <div class="ss1_title">Por valor total</div>
                <div class="ss1_content filter_date_cont">
                    <p>Deja en blanco si no quieres filtrar por ese campo</p>
                    <div>
                        <p>Mayor o igual a:</p>
                        <input type="text" class="select_list_1 input_currency" name="filter_val_min" id="filter_val_min" placeholder="Ej: $ 100,000" min="0" max="100000000">
                    </div>
                    <div>
                        <p>Menor o igual a:</p>
                        <input type="text" class="select_list_1 input_currency" name="filter_val_max" id="filter_val_max" placeholder="Ej: $ 500,000" min="0" max="100000000">
                    </div>
                </div>
            </div>

            <span class="errorMsg" id="errorMsgfilterOrders"></span>
			<div class="LoadingRoller" id="LoadingRoller_filterOrders"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<button class="btn_green_full" id="btnFilterOrders">FILTRAR</button>
            <button class="btn_green_full" onclick="resetFilters()" id="btnFilterOrders">RESETEAR</button>

		</div>	
    </div>

<script src="../js/js_bills.js?<?php echo time(); ?>"></script>
</body>
</html>	