<?php
include("session.php");
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135784524-1');
	</script>
	<title>Luca</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="192x192" href="../logos/app_icon_192.png">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="manifest" href="../json/manifest.json?<?php echo time(); ?>">
	
	<!-- CHARTS.JS & MOMENT JS-->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_sales.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_chatbot.css?<?php echo time(); ?>">
	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS AND FONTAWESOME -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
	<script src="https://use.fontawesome.com/releases/v5.9.0/js/all.js" data-auto-replace-svg="nest"></script>
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- QR Camera Reader -->
	<script src="../external_libs/jsQR.js?<?php echo time(); ?>"></script>
  	<!-- Barcode Camera Reader -->
	<script src="../external_libs/quagga.min.js?<?php echo time(); ?>"></script>
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<!-- JSON DATA IVA -->
	<script src="../json/items_iva.json?<?php echo time(); ?>"></script>
	<!-- TAGS AND AUTOCOMPLETE -->
	<link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
	<script src="../external_libs/jquery.magicsearch.min.js"></script>
	<!-- MULTIPLE SELECT CHOSE -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

</head>

<body>

	<div class="notify_alert"><span>x</span><div class="na_title"><i class="fas fa-bell"></i><span></span></div><div class="na_msg"></div></div>

	<div class="MainRoller_background RollerMain" id="MainRoller_background"><div class="RollerMain_cont"><div class="LoadingRoller" id="MainRoller"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="RollerMainText" id="RollerMainText">Cargando ...</p></div></div>

	<div class="bar_loading_cont"><div class="bar_loading"><div class="bars_wrap"><div class="bar bar1"></div><div class="bar bar2"></div><div class="bar bar3"></div><div class="bar bar4"></div><div class="bar bar5"></div><div class="bar bar6"></div></div><div class="bar_ld_msg"></div></div></div>

	<div id="MenuNavg" class="MenuNavg"></div>

	<div id="wrapper" class="wrapper_grid">

		<div class="choose_salesman" id="choose_salesman">
			<div class="choose_salesman_bx1" id="choose_salesman_bx1"> 
				<img id='prof_avatar' src="../avatars/maninsute.svg">
				<p id='prof_name' title='Perfil activo'>Seleccionar Perfil</p>
			</div>
			<div class="choose_salesman_bx2">

				<div id="offline_noty" class="airmenu_cont">
					<div><span class="noty_square noty_top_left noty_luca noty_clickable noty_xs noty_show" id="num_off_bills">0</span></div>
					<img id="ex_id" title="Cuentas Guardadas Sin Conexión" alt="Offline" src="../icons/SVG/26-server/server-network-close.svg">
					<div id="menu_ex_id" class="airmenu_menu black">
						<div title="Enviar cuentas" id="sendAllOffBills"><i class="far fa-paper-plane"></i></div>
						<div title="Limpiar todo" id="cleanAllOffBills"><i class="far fa-trash-alt"></i></div>
						<div title="Ver cuentas" id="seeOffBills"><i class="far fa-file-alt"></i></div>
					</div>
				</div>

				<img id="refrAllItems" title="Refrescar Productos" src="../icons/general/basket-refresh.png" onclick="refresh_items()">			
				<img id="cmd_lnk" title="Comandas" src="../icons/SVG/87-notes/notepad-2.svg">

			</div>
		</div>


		<div class="cont_select_items">
			<div class="cont_search">
				<form autocomplete="off" class="search">
					<span class="mdi mdi-magnify"></span>
					<input autocomplete="false" type="search" name="items_search" id="items_search" placeholder="Busca por producto, código o categoría . . ." class="input_style_1_search input_search">
				</form>
				
				<div class="search_side_tools">
					<img id="opNewItem" title="Nuevo Ítem" src="../icons/general/shopping-cart-add-2.png" width="35px" onclick="OpenNewItem()">
					<img title="Registradora" src="../icons/general/register-machine.png" width="35px" onclick="OpenCashier()">	
					<img id="opVoiceCom" class="hide" title="Comandos de Voz" src="../icons/SVG/37-audio/microphone-3.svg" width="35px">
					<img title="Escanear QR" src="../icons/general/qr-code.png" width="35px" onclick="openQRScanner2('0')">
					<img title="Escanear Código Barras" src="../icons/SVG/41-shopping/barcode-scan.svg" width="35px" onclick="Quagga_Toggle('barcodecam_canvas')">			
					<div class="QRVideoScanCont">
						<p>Escanea el código QR del ítem para agregarlo a la factura</p>
						<div class="QRScanCam_Cont">
							<div id="QRcmpause" class="QRcmpause hide">
								<p>Camara en Pausa</p>
								<p>Da click aquí para activarla de nuevo</p>
							</div>
							<center>
								<canvas class="QRVideoScan" id="canvasQR" height="480" width="640"></canvas>
							</center>
						</div>
						<div id="cameras_link" class="cameras_link"></div>
					</div>
				</div>

				<div id="barcodecam_cont" class="barcodecam_cont">
					<p>Escanea el código de barras del ítem para agregarlo a la factura. <p class="bcod_pr1" onclick="OpenHelpScan()">¿Necesitas ayuda?<p></p>
					<div class="barcodecam_canvas" id="barcodecam_canvas"></div>
					<div id="cameras_link2" class="cameras_link"></div>
				</div>
				<audio src="../audio/beep.wav" autostart="false" id="beepsound"></audio>
			</div>

			<div id="cont_top_sells" class="cont_top_sells">
				<div id="header_top_sales" class="head_top_sells" data-toggle="collapse" data-target="#box_top_sells">
					<p>LO MÁS VENDIDO</p>
					<button id="btn_head_top_sales"><span class="mdi mdi-chevron-down"></span></button>
				</div>
				<div class="box_top_sells collapse in" id="box_top_sells"></div>
			</div>

			<div id="cont_products" class="cont_top_sells">
				<div id="header_products" class="head_top_sells">
					<p data-toggle="collapse" data-target="#box_products">PRODUCTOS</p>
					<button id="btn_head_products" data-toggle="collapse" data-target="#box_products"><span class="mdi mdi-chevron-down"></span></button>
					<div id="selectItmsCont" class="selectItmsCont">
						<div id="selectItmsImportCSV" class="selectItmsDel" onclick="OpenImportDataProduct()"><img title="Importar ítems" src="../icons/SVG/23-data-transfer/upload-harddrive-1.svg"></div>
						<div id="selectItmsExportCSV" class="selectItmsDel hide" onclick="export_selected_items_csv()"><img title="Exportar seleccionados a CSV" src="../icons/SVG/23-data-transfer/download-harddrive-1.svg"></div>
						<div id="selectItmsDel" class="selectItmsDel hide" onclick="DropSelectedItems()"><img title="Eliminar ítems" src="../icons/SVG/01-content-edition/bin-1.svg"></div>
						<div id="selectItmsDeselectAll" class="selectItmsDel hide" onclick="deselect_all_items_home()"><img title="Deseleccionar todos" src="../icons/essential/cancel.svg"></div>
						<div id="selectItmsSelectAll" class="selectItmsDel" onclick="select_all_items_home()"><img title="Seleccionar todos" src="../icons/SVG/02-status/checklist.svg"></div>
						<div id="selectItmsNum" class="selectItmsNum hide"></div>
					</div>
				</div>
				<div class="box_top_sells collapse in" id="box_products" ></div>
			</div>
		</div>
		
		<div class="cont_pre_bill">
			<div class="cont_bill" id="cont_bill">
				<form autocomplete="off" class="cb_search_consumer">
					<span class="noty_square noty_top_left noty_luca noty_clickable" id="num_luca_points" title="Puntos Luca">0</span>
					<input autocomplete="false" type="text" name="search_consumer" id="search_consumer" placeholder="Buscar Cliente . . ." class="cb_search_bar_cons" maxlength="50">
					<div class="cb_icon_new_cons" id="cb_icon_new_cons" onclick="OpenNewCustomer()">
						<img title="Nuevo Cliente" src="../icons/general/person-add-1.png">
					</div>
					<div class="cb_icon_new_cons hide_clearconsumer" id="cb_icon_info_cons" onclick="OpenConsumerInfoPanel()">
						<img title="Info cliente" src="../icons/general/person-setting-1.png">
					</div>
					<div class="cb_icon_new_cons hide_clearconsumer" id="cb_icon_clear_cons" onclick="ClearConsumerActive()">
						<img title="Limpiar cliente" src="../icons/SVG/02-status/close-badge.svg">
					</div>
				</form>

				<div class="cb_cont_items" id="cb_cont_items"></div>

				<div class="cb_sum_totals">
					<div class="cb_sums_subtotal">
						<p><span class="mdi mdi-cash-usd-outline"></span> Impuestos: </p>
						<p id="cb_tax">$ <span id="tax_bill">0</span></p>
					</div>
					<div class="cb_sums_subtotal subtotal_mg">
						<p>SUBTOTAL: </p>
						<p id="cb_subtotal">$ <span id="subtotal_bill">0</span></p>
					</div>	
					<div class="cb_sums_subtotal">
						<p title="Aplicar descuento" id="cb_discount_1"><span class="mdi mdi-percent"></span>  Descuento<span id="discAp"></span>:</p>
						<p title="Aplicar descuento" id="cb_discount">$ <span id="discount_bill">0</span></p>
					</div>
					<div class="cb_sums_subtotal">
						<p title="Aplicar propina" id="cb_tip_1"><span class="mdi mdi-currency-usd-circle-outline"></span>  Propina<span id="tipsAp"></span>: </p>
						<p title="Aplicar propina" id="cb_tip">$ <span id="tips_bill">0</span></p>
					</div>
					<div class="cb_sums_subtotal">
						<label title="Activar Localizador" class="container_radiobtn hide" for="tracker_op" id='lbltracker_op' title="Desproteger perfil">
							<p class="tracker_op_txt"><span class="mdi mdi-map-marker-radius-outline"></span> Activar localizador en esta</p>
							<input type="checkbox" id="tracker_op" name="tracker_op" value="1">
							<span class="radiobtnstl_checkbox"></span>
						</label>
					</div>
				</div>

				<div class="cb_button_checkout" id="checkout_bill">
					<p>TOTAL $ <span id="super_total_bill">0</span></p>
					<p>PROCESAR PAGO</p>
				</div>

				<div class="cb_count_delete">
					<p><span id="total_items_bill">0</span> ítems en la canasta</p>
					<img src="../icons/essential/bin.svg" onclick='clear_bill()'>
				</div>
			</div>
		</div>

		<div class="msg_items_added" id="little_bill">
			<p> <span class="mdi mdi-cart-outline"></span> <span id="litmsg_tot_items">0</span> ítems - Total: $ <span id="litmsg_tot_value">0</span> </p>
		</div>

	</div>

	<!-- MODAL CASHIER -->
	<div class="msg-box-container" id="msg-box-container-cashier">
		<div class="msg-box-1" id="msg-box-cashier">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseCashier()">X</p>

					<div class="cont_cashier">
						<div class="cash_screen">
							<div class="cash_cuantity">
								<p class="cash_quanty_up" onclick="quantUp()"><span class="mdi mdi-chevron-up"></span></p>
								<p>x<span id="cash_cant">1</span></p>
								<p class="cash_quanty_up" onclick="quantDown()"><span class="mdi mdi-chevron-down"></span></p>
							</div>
							<div class="cash_display">
								<p id="cash_value">$ 0</p>
							</div>
						</div>
						<table class="cash_cont_buttoms">
							<tr>
								<td onclick="cashier_btn_pressed('7')" class="cash_btn cash_tc cash_single cash_hbw">7</td>
								<td onclick="cashier_btn_pressed('8')" class="cash_btn cash_tc cash_single cash_hbw">8</td>
								<td onclick="cashier_btn_pressed('9')" class="cash_btn cash_tc cash_single cash_hbw">9</td>
								<td onclick="cashier_btn_pressed('erase')" class="cash_btn cash_tc cash_single chash_color_gray"><span class="mdi mdi-eraser-variant"></span></td>	
							</tr>
							<tr>
								<td onclick="cashier_btn_pressed('4')" class="cash_btn cash_tc cash_single cash_hbw">4</td>
								<td onclick="cashier_btn_pressed('5')" class="cash_btn cash_tc cash_single cash_hbw">5</td>
								<td onclick="cashier_btn_pressed('6')" class="cash_btn cash_tc cash_single cash_hbw">6</td>
								<td onclick="cashier_btn_pressed('clear')" class="cash_btn cash_tc cash_single chash_color_red"><span class="mdi mdi-delete-outline"></span></td>
							</tr>
							<tr>
								<td onclick="cashier_btn_pressed('1')" class="cash_btn cash_tc cash_single cash_hbw">1</td>
								<td onclick="cashier_btn_pressed('2')" class="cash_btn cash_tc cash_single cash_hbw">2</td>
								<td onclick="cashier_btn_pressed('3')" class="cash_btn cash_tc cash_single cash_hbw">3</td>
								<td onclick="cashier_btn_pressed('ok')" rowspan="2" class="cash_btn cash_tc cash_single chash_color_gradient borrad-botright"><span class="mdi mdi-cart-plus"></span></td>
							</tr>
							<tr>
								<td onclick="cashier_btn_pressed('0')" colspan="2" class="cash_btn cash_tc cash_single cash_hbw borrad-botleft">0</td>
								<td onclick="cashier_btn_pressed('000')" class="cash_btn cash_tc cash_single cash_hbw">000</td>
							</tr>
						</table>
					</div>

		</div>	
	</div>

	<!-- REGISTRO DE PRODUCTO -->
	<div class="msg-box-container" id="msg-box-container-new-item">
		<div class="msg-box-1" id="msg-box-new-item">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseNewItem()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-cart-plus"></span> NUEVO ÍTEM</p>

			<form action="push_new_item.php" method="POST" name="new-item-form" id="new-item-form">
					<input type="reset" value="Limpiar todo"/>
				<h4>Nombre:</h4>
				<input type="text" name="ni_prod_name" id="ni_prod_name" placeholder="Ej: Agua Mineral" required minlength="1" maxlength="50" autofocus>
				<h4>Código/Referencia:</h4>
				<h5 onclick="Generate_Code()">Generar</h5>
				<input type="text" name="ni_prod_code" id="ni_prod_code" placeholder="Ej: AM-0001" required minlength="1" maxlength="15">
				<h4>Precio Unitario:</h4>
				<h5 id="suggestPrice">Sugerir</h5>
				<input type="text" class="input_currency" name="ni_prod_price" id="ni_prod_price" placeholder="Ej: $4,200" required>
				<h4>Costo Unitario:</h4>
				<input type="text" class="input_currency" name="ni_prod_cost" id="ni_prod_cost" placeholder="Ej: $2,100">
				<h4>Unidades:</h4>
				<input type="text" name="ni_prod_units" id="ni_prod_units" placeholder="Ej: Botellas" required minlength="1" maxlength="20">
				<h4>Categoría:</h4>
				<h5 id="open_new_catg">Crear Categoría</h5>
				<select name="ni_prod_category" id="ni_prod_category" > 
				</select>
				<div class="new_category" id="new_category">
					<input type="text" name="catg_name" id="catg_name" placeholder="Nombre de la Categoría" class="input_cat_style" minlength="1" maxlength="30">
					<div class="cont_colors">
						<div onclick="AddColorSelectionClass(this)" class="color_crcl" id="c_c1"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c2"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c3"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c4"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c5"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c6"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c7"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c8"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c9"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c10"></div>
					</div>	
					<p class="btn_create_cat" id="btn_create_cat">CREAR CATEGORÍA</p>
					<p id="msg_cat"></p>
				</div>
				<h4>Impuesto:</h4>
				<h5 onclick="OpenHelpTax()">Ayuda</h5>
				<input type="text" name="ni_prod_tax" id="ni_prod_tax" placeholder="Ej: 19%" class="input_perc" minlength="1" maxlength="30">
				<h4>Cambiar Icono:</h4>
				<img onclick="OpenGallery()" class="icon_selection_btn" id="icon_selection_btn" src="../icons/SVG/78-video-games/mario-question-box.svg">
				<input type="text" name="ni_prod_icon" id="ni_prod_icon" value="../icons/SVG/78-video-games/mario-question-box.svg" hidden="true">
				
				<span class="errorMsg" id="errorMsgNewProduct"></span>
				<input type="submit" name="login-submit" id="login-submit" value="CREAR ÍTEM">
								<div class="horizDivide"></div>
								<p class="btn_soft_style" onclick="OpenImportDataProduct()">IMPORTAR DATOS DESDE CSV</p>
				
			</form>

		</div>	
	</div>

		<!-- IMPORT DATA PRODUCTS -->
	<div class="msg-box-container" id="msg-box-container-impodatprod">
		<div class="msg-box-1" id="msg-box-impodatprod">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseImportDataProduct()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> IMPORTAR ÍTEMS</p>
			<center>
				<div class="cont_inputcsv" id="cont_inputcsv">
						<p class="msg1-end-impodatprod">Selecciona el archivo .csv para empezar el proceso de importación de ítems. Recuerda que estos deben seguir los <span class="impptx1" onclick="OpenHelpCsv()">requerimientos de importación de ítems</span></p>
						<label class="inp_import_delimiter" for="inp_import_delimiter">Delimitador: </label>
						<input type="text" id="inp_import_delimiter" class="msg-box-1-input mw50" value=";"></input>
						<label for="inp_import_csvfile">
								<img src="../icons/illustrations/csv_file_icn.svg" class="img_csv_file" id="img_csv_file">
								<input type="file" id="inp_import_csvfile" class="inp_import_csvfile" name="inp_import_csvfile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
						</label>
				</div>
				<span class="errorMsg" id="errorMsgCsvFile"></span>
				<div id="wrap_tableErr" class="wrap_tableErr"></div>
				<div class="msg_idatprod" id="msg_idatprod">
				</div>
			</center>
		</div>	
	</div>

		<!-- HELP MODAL -->
	<div class="msg-box-container" id="msg-box-container-help">
		<div class="msg-box-1" id="msg-box-help">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseHelp_over()">X</p>
			<div id="help_container" class="help_container"></div>
		</div>	
	</div>
		
	<!-- REGISTRO DE PRODUCTO DONE -->
	<div class="msg-box-container" id="msg-box-container-end-newprod">
		<div class="msg-box-1" id="msg-box-end-newprod">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseNewItemEnd()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-cart-plus"></span> NUEVO ÍTEM</p>
			<p class="msg1-end-newprod">¡Todo listo! El ítem <b><span id="itemNamePostAdd">NOMBRE_ITEM</span></b> ha sido registrado correctamente. Aquí va un <b>código QR</b> que puedes usar para escanear tu ítem y agilizar el proceso de registro de la compra en nuestro sistema.</p>
			<p class="msg2-end-newprod">Da click sobre la imagen para descargarla</p>

			<center>
				<a id="aQRCode" href="#" target="_blank"><img id="QRCodeImg" width="200" height="200"></a>
			</center>

			<button class="btn_green_full" onclick="AddNewItem()">Agregar Nuevo Ítem</button>
			<button class="btn_green_full" onclick="ExitFinishedNewItem()">Salir</button>

		</div>	
	</div>
	
	<!-- HELP WITH TAXES -->
	<div class="msg-box-container" id="msg-box-container-help-tax">
		<div class="msg-box-1" id="msg-box-help-tax">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseHelpTax()">X</p>

			<p class="msg-box-1-description">Ingresa el nombre general del ítem (Ejemplo: Cerveza, Frutas o Arroz) al que quieres consultar su IVA y da click en el mismo una vez encuentres su clasificación más aproximada.</p>
				<center><input type="input" class="input_cat_style" name="tax-search" id="tax-search" placeholder="Buscar Ítem"></center>
				<center><div id="tax-filter-records" class="list_tax"></div></center>
				<center><p class="msg-box-1-footnone">Información obtenida del Listado Completo IVA Canasta Familiar de la <a href="https://www.dian.gov.co/impuestos/Paginas/RefTribuEstructu.aspx" target="_blank">Reforma Tributaria Estructural.</a></p></center>

		</div>	
	</div>

	<!-- MODAL SET DISCOUNT -->
	<div class="msg-box-container" id="msg-box-container-discount">
		<div class="msg-box-1" id="msg-box-discount">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseDiscount()">X</p>

			<div class="msg-box-1-optionbox-container">
								<div class="msg-box-1-optionbox-hrz luca_op_rd" id="discount_lucapoints_option"><p>Redimir Puntos Luca</p></div>
				<div class="msg-box-1-optionbox-hrz" id="discount_perc_option"><img src="../icons/general/coupon-discount.png"><p>Descuento en porcentaje</p></div>
				<div class="msg-box-1-optionbox-hrz" id="discount_money_option"><img src="../icons/general/price-tag.png"><p>Descuento en pesos</p></div>
			</div>
						<div class="horizDivide"></div>
			<center><input type="text" class="msg-box-1-input mw300" name="input_discount" id="input_discount" placeholder="Ingresar Descuento"></center>
			<div id="lr_cont" class="lr_cont">
								<div>
										<p id="lr_0" class="lr_0"><b><span id="lr_0_name"></span></b> tiene un total de <b><span id="lr_0_points"></span> puntos ($ <span id="lr_0_p_cash"></span>)</b>. Ingresa abajo la cantidad de puntos a redimir en esta compra o <span id="lr_0_all_points" class="lr_0_all_points">paga toda la cuenta.</span></p>
								</div>
								<div class="lr_1">
										<input type="text" class="msg-box-1-input mw100" name="input_luca_points" id="input_luca_points" placeholder="Ej: 50" value="0">
										<p>Puntos</p>
								</div>
								<table class="lr_table_nums">
										<tbody>
												<tr class="lr_2">
														<td>Subtotal:</td>
														<td>$ <span id="lr_subtot">0<span></td>
												</tr>
												<tr class="lr_3">
														<td>Ahorro:</td>
														<td class="lr_bb">-$ <span id="lr_save">0</span></td>
												</tr>
												<tr class="lr_hd">
												</tr>
												<tr class="lr_4">
														<td>TOTAL:</td>
														<td>$ <span id="lr_tot">0</span></td>
												</td>
										</tbody>
								</table>
						</div>
						<input type="text" id="discType" value="" hidden>
						<span class="errorMsg" id="errorMsgDiscount"></span>
			<button class="btn_green_full" id="input_btn_discount" onclick="ApplyDiscount()">APLICAR</button>

		</div>	
	</div>

	<!-- MODAL SET TIPS -->
	<div class="msg-box-container" id="msg-box-container-tips">
		<div class="msg-box-1" id="msg-box-tips">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseTips()">X</p>

			<div class="msg-box-1-optionbox-container">
				<div class="msg-box-1-optionbox" id="tips_perc_option"><img src="../icons/general/coupon-discount.png"><p>Propina en porcentaje</p></div>
				<div class="msg-box-1-optionbox" id="tips_money_option"><img src="../icons/general/price-tag.png"><p>Propina en pesos</p></div>
			</div>
			<center><input type="text" class="msg-box-1-input mw300" name="input_tips" id="input_tips" placeholder="Ingresar Propina"></center>
			<input type="text" id="tipsType" value="" hidden>
			<button class="btn_green_full" id="input_btn_tips" onclick="ApplyTips()">APLICAR</button>

		</div>	
	</div>
	
	<!-- NUEVO CLIENTE -->
	<div class="msg-box-container" id="msg-box-container-newcustomer">
		<div class="msg-box-1" id="msg-box-newcustomer">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseNewCustomer()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-account-plus-outline"></span> NUEVO CLIENTE</p>

			<form action="push_new_customer.php" method="POST" name="new-customer-form" id="new-customer-form">
					<input type="reset" value="Limpiar todo"/>
				<h4>Nombre:</h4>
				<input type="text" name="ncust_name" id="ncust_name" placeholder="Ej: Juan Pérez" required maxlength="60">
				<h4>Identificación:</h4>
				<input type="text" name="ncust_id" id="ncust_id" placeholder="Ej: 98765432" maxlength="30" required>
				<h4>Tipo Identificación:</h4>
				<select name="ncust_type_id" id="ncust_type_id" required> 
					<option value="1" selected>Cédula Ciudadania</option>
					<option value="2">Cédula Extranjería</option>
					<option value="3">Pasaporte</option>
					<option value="4">Tarjeta de Identidad</option>
					<option value="5">Registro Civil</option>
					<option value="6">Otro</option>
				</select>
				<h4>e-mail:</h4>
				<input type="email" name="ncust_email" id="ncust_email" placeholder="Ej: juanperez@micorreo.com" maxlength="50">
				<h4>Celular/Teléfono:</h4>
				<input type="tel" name="ncust_phone" id="ncust_phone" class="input_phone" placeholder="Ej: (321) 123 4567" maxlength="14">
				<h4>Fecha Nacimiento (aaaa-mm-dd):</h4>
				<input type="date" class="msg-box-date" name="ncust_bdate" id="ncust_bdate" placeholder="Ej: 1990-02-11" min="1910-01-01" max="2100-12-31" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
				<h4>Genero:</h4>
				<div id="container_gender">
					<label class="container_radiobtn" for="genderMale">
						<img src="../avatars/manblackjacket.svg">
						<input type="radio" id="genderMale" name="gender" value="1">
						<span class="radiobtnstl"></span>
					</label>
					<label class="container_radiobtn" for="genderFemale">
						<img src="../avatars/girlearingsgreen.svg">
						<input type="radio" id="genderFemale" name="gender" value="2">
						<span class="radiobtnstl"></span>
					</label>
				</div>

				<span class="simpleMsg" id="simpleMsgNewCustomer"></span>
				<span class="errorMsg" id="errorMsgNewCustomer"></span>
				<input type="submit" name="newcustomer-submit" id="newcustomer-submit" value="CREAR CLIENTE">
				
			</form>

		</div>	
	</div>

	<!-- EDITAR CLIENTE -->
	<div class="msg-box-container" id="msg-box-container-editcustomer">
		<div class="msg-box-1" id="msg-box-editcustomer">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditCustomer_over()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-account-plus-outline"></span> EDITAR CLIENTE</p>

			<form name="edit-customer-form" id="edit-customer-form">
					<input type="reset" value="Limpiar todo"/>
				<h4>Nombre:</h4>
				<input type="text" name="ed_cust_name" id="ed_cust_name" placeholder="Ej: Juan Pérez" required maxlength="60">
								<h4>Identificación:</h4>
				<input type="text" name="ed_cust_id" id="ed_cust_id" placeholder="Ej: 98765432" maxlength="30" required>
								<input type="text" name="ed_cust_id_old" id="ed_cust_id_old" hidden>
								<h4>Tipo Identificación:</h4>
				<select name="ed_cust_type_id" id="ed_cust_type_id" required> 
										<option value="1" selected>Cédula Ciudadania</option>
										<option value="2">Cédula Extranjería</option>
										<option value="3">Pasaporte</option>
										<option value="4">Tarjeta de Identidad</option>
										<option value="5">Registro Civil</option>
										<option value="6">Otro</option>
				</select>
								<input type="text" name="ed_cust_type_id_old" id="ed_cust_type_id_old" hidden>
				<h4>e-mail:</h4>
				<input type="email" name="ed_cust_email" id="ed_cust_email" placeholder="Ej: juanperez@micorreo.com" maxlength="50">
				<h4>Celular/Teléfono:</h4>
				<input type="tel" name="ed_cust_phone" id="ed_cust_phone" placeholder="Ej: (321) 123 4567" maxlength="14">
				<h4>Fecha Nacimiento (aaaa-mm-dd):</h4>
				<input type="date" class="msg-box-date" name="ed_cust_bdate" id="ed_cust_bdate" placeholder="Ej: 1990-02-11" min="1910-01-01" max="2100-12-31" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
				<h4>Genero:</h4>
				<div id="ed_container_gender">
										<label class="container_radiobtn" for="ed_genderMale">
											<img src="../avatars/manblackjacket.svg">
											<input type="radio" id="ed_genderMale" name="ed_gender" value="1">
											<span class="radiobtnstl"></span>
										</label>
									<label class="container_radiobtn" for="ed_genderFemale">
										<img src="../avatars/girlearingsgreen.svg">
											<input type="radio" id="ed_genderFemale" name="ed_gender" value="2">
											<span class="radiobtnstl"></span>
										</label>
								</div>
								
								<span class="simpleMsg" id="simpleMsgEditCustomer"></span>
								<span class="errorMsg" id="errorMsgEditCustomer"></span>
				<input type="submit" name="editcustomer-submit" id="editcustomer-submit" value="EDITAR CLIENTE">
				
			</form>

		</div>	
	</div>

		<!-- MODAL CONSUMER INFO PANEL -->
		<div class="msg-box-container" id="msg-box-container-consinfopanel">
		<div class="msg-box-1" id="msg-box-consinfopanel">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseConsumerInfoPanel()">X</p>

				<div class="panelcons_s1 table_style">
						<div class="pn_1">
								<div class="s1_c">
										<img title="Perfil de Usuario" src="../icons/SVG/07-users/account-circle-1.svg">
								</div>
						</div>
						<div class="pn_2">
								<div class="s1_b">
										<p id="infocust_name" class="data_info_hl wsNormal"></p>
										<p id="red_pnt_cust" class="data_info_lucapnt"><span id="infocust_puntos"></span> Puntos</p>
								</div>
								<div class="s1_a">
										<img title="Editar" src="../icons/SVG/01-content-edition/pencil-circle.svg" onclick="IniciateEditCustomer()">
										<img title="Eliminar" src="../icons/SVG/01-content-edition/bin-1.svg" onclick="DeleteCustomer()">
								</div>
						</div>
				</div>
				<div class="panelcons_s2 table_style">
						<div class="label_n_data w50p">
								<p><span class="mdi mdi-card-account-details-outline"></span> ID</p>
								<p id="infocust_id"> - </p>
						</div>
						<div class="label_n_data w50p">
								<p><span class="mdi mdi-gift-outline"></span> Nacimiento</p>
								<p id="infocust_bday"> - </p>
						</div>
				</div>
				<div class="panelcons_s2 table_style">
						<div class="label_n_data w50p">
								<p><span class="mdi mdi-email-outline"></span> Email</p>
								<p id="infocust_email" class="text_overflow_dots"> - </p>
						</div>
						<div class="label_n_data w50p">
								<p><span class="mdi mdi-phone"></span> Teléfono</p>
								<p id="infocust_tel"> - </p>
						</div>
				</div>
				<div class="horizDivide w90p"></div>
				<div class="panelcons_s3">
						<div id="s3_a" class="s3_a table_style">
								<img src="../icons/SVG/41-shopping/shopping-cart-2.svg">
								<p class="s3_a1">Últimas Compras</p>
								<p class="s3_a2" onclick="OpenConsOrders()">Ver todas</p>
						</div>
						<div id="cont_last_orders_cust"></div>
				</div>
				<div class="horizDivide w90p"></div>
				<div class="panelcons_s4">
						<button class="btn_soft_style" id="input_btn_analitica" onclick="OpenAnalyticsCust()">ANALÍTICA</button>
						<button class="btn_soft_style" id="input_btn_recom" onclick="CustomerRecomInfoPanel()">RECOMENDADOS</button>
				</div>
		</div>	
	</div>

		<!-- MODAL CONSUMER ORDERS HISTORY -->
		<div class="msg-box-container" id="msg-box-container-consorders">
		<div class="msg-box-1" id="msg-box-consorders">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseConsOrders_over()">X</p>

						<p class="msgb-box-1-title" id="msgb-box-1-title">HISTÓRICO VENTAS</p>

						<div id="summ_bill_cons" class="summ_bill_cons table_style">
						</div>
						
						<div class="horizDivide"></div>
						
						<div class="con_bill_header">
								<div class="cbh1">Orden</div>
								<div class="cbh2">Fecha</div>
								<div class="cbh3">Valor</div>
								<div class="cbh4"> </div>
						</div>
						
						<div class="allord_wrap">
								<div id="allord_cont" class="allord_cont table_style">
								</div>
						</div>

		</div>	
	</div>

		<!-- MODAL CONSUMER ORDERS ANALYTICS -->
		<div class="msg-box-container" id="msg-box-container-analyticscust">
		<div class="msg-box-1" id="msg-box-analyticscust">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseAnalyticsCust_over()">X</p>

						<p class="msgb-box-1-title" id="msgb-box-1-title">ANALÍTICA DEL CLIENTE</p>

						<div id="modAnalCust_1" class="Data1_Style">
							<div><p id="ca_ventas">0</p><p>Ventas</p></div>
							<div><p>$ <span id="ca_ingresos">0</span></p><p>Ingresos</p></div>
							<div>
								<p><span id="ca_days_last_prsh">-</span><span> días</span></p><p>Última Compra</p>
								<img id="warn_days_lp" title="El cliente lleva muchos días sin comprar." src="../icons/general/warning_1.svg">
							</div>
						</div>

						<div id="badges_wrap" class="hide">
							<p class="anal_subt">Insignias</p>
							<div id="badges_cust" class="badges_cust Data2_Style"></div>
							<div id="descr_badge" class="descr_badge hide"><p></p></div>
						</div>

						<div id="modAnalCust_2" class="modAnalCust_2 hide">
							<div class="segm_cust">
								<p class="anal_subt">Segmento del cliente</p>
								<div id="segm_cont" class="segm_cont table_style">
									<div class="segm_cust_box_cont">
										<img id="segm_cut_icon" src="../icons/general/segm_6.svg">
										<div id="segm_cust_box" class="segm_cust_box"></div>
									</div>
									<div id="segm_cust_desc" class="segm_cust_desc"></div>
								</div>
							</div>
							<div class="nba_wrap">
								<p class="anal_subt">Mejor acción a tomar</p>
								<div id="nba_cont" class="nba_cont">
									<div><img id="nba_icon" src="../icons/SVG/08-users-actions/group-chat.svg"></div>
									<div><p id="nba_cust_txt"></p></div>
								</div>
							</div>
						</div>

						<div class="paytyp_wrap">
								<p class="anal_subt">Compras por tipo de pago</p>
								<div class="paytyp_cont_wrap table_style">
										<div>
												<img title="Efectivo" src="../icons/SVG/44-money/bank-notes-3.svg">
												<p><span id="cptp_1">0</span>%</p>
										</div>
										<div>
												<img title="Tarjeta de Crédito" src="../icons/SVG/44-money/credit-card-visa.svg">
												<p><span id="cptp_2">0</span>%</p>
										</div>
										<div>
												<img title="Tarjeta Débito" src="../icons/SVG/44-money/credit-card.svg">
												<p><span id="cptp_3">0</span>%</p>
										</div>
										<div>
												<img title="Otro" src="../icons/SVG/44-money/wallet-3.svg">
												<p><span id="cptp_4">0</span>%</p>
										</div>
								</div>
						</div>

		</div>	
	</div>
	
	<!-- MODAL PRODUCT RECOMENDATION -->
	<div class="msg-box-container" id="msg-box-container-itemrecom">
		<div class="msg-box-1" id="msg-box-itemrecom">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemRecom()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title">RECOMENDADOS</p>
			<p class="msg-box-1-description" id="itemrecom_description"></p>
			<div class="msg-box-1-optionbox-container" id="itemrecom_items"></div>
			<button class="btn_green_full" id="input_btn_itemrecom" onclick="AddItemsRecom()">LISTO</button>

		</div>	
	</div>

	<!-- MODAL CHECKOUT 1 -->
	<div class="msg-box-container" id="msg-box-container-checkout1">
		<div class="msg-box-1" id="msg-box-checkout1">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseCheckOut1_clean()">X</p>

			<div id="offlineTagCheckout" class="tag red">Estás en modo offline</div>
			<div class="co1_a">
				<p class="co1_a_1">FACTURA: <span id="bill_id"></span></p>
				<p class="total_bill" id="total_bill"></p>
				<div class="co1_a_customer">
					<img src="../icons/SVG/07-users/account-circle-1.svg">
					<p id="customer_name"></p>
				</div>
			</div>
			<p class="final_points hide" id="final_points"><span id="points_this_bill">0</span> <span id="label_luca_points">PUNTOS LUCA</span><span class="luca_points_lab2">CON ESTA COMPRA</span></p>
			<div class="horizDivide"></div>
			<div class="msg-box-1-optionbox-container co1_btns">
				<div class="msg-box-1-optionbox" id="co1_type_email">
					<img src="../icons/SVG/06-email/email-receipt.svg">
					<p>EMAIL</p>
					<div class="counter_item_add" id="emailbill_checked"><span class="mdi mdi-check"></span></div>
				</div>
				<div class="msg-box-1-optionbox" id="co1_type_print">
					<img src="../icons/SVG/01-content-edition/print-text.svg">
					<p>IMPRESA</p>
					<div class="counter_item_add" id="printbill_checked"><span class="mdi mdi-check"></span></div>
				</div>
				<div class="msg-box-1-optionbox" id="co1_type_none">
					<img src="../icons/SVG/01-content-edition/delete-1.svg">
					<p>SIN FACTURA</p>
					<div class="counter_item_add" id="noprintill_checked"><span class="mdi mdi-check"></span></div>
				</div>
			</div>

		</div>	
	</div>
	
	<!-- MODAL CHECKOUT 2 -->
	<div class="msg-box-container" id="msg-box-container-checkout2">
		<div class="msg-box-1" id="msg-box-checkout2">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseCheckOut2_clean()">X</p>

			<span class="mdi mdi-chevron-left previous_arrow" id="go_to_co1"></span>

			<div class="msg-box-1-optionbox-container co1_btns">
				<div class="msg-box-1-optionbox" id="co1_pay_card" onclick="checkTypeOfPayment('card')">
					<img src="../icons/SVG/44-money/credit-card.svg">
					<p>TARJETA</p>
					<div class="counter_item_add" id="cardPay_checked"><span class="mdi mdi-check"></span></div>
				</div>
				<div class="msg-box-1-optionbox" id="co1_pay_cash" onclick="checkTypeOfPayment('cash')">
					<img src="../icons/SVG/44-money/bank-notes-3.svg">
					<p>EFECTIVO</p>
					<div class="counter_item_add" id="cashPay_checked"><span class="mdi mdi-check"></span></div>
				</div>
				<div class="msg-box-1-optionbox" id="co1_pay_other" onclick="checkTypeOfPayment('other')">
					<img src="../icons/SVG/44-money/wallet-3.svg">
					<p>OTRO</p>
					<div class="counter_item_add" id="otherPay_checked"><span class="mdi mdi-check"></span></div>
				</div>
			</div>

			<div class="horizDivide"></div>

			<div class="cash_option_cont hide" id="cash_option_cont">
				<p><span class="co2_cash_total_label">TOTAL: </span><span class="co2_cash_total_value" id="co2_cash_total_value"></span></p>
				<p class="co2_cash_total_label2">El cliente paga con:</p>
				<div class="cash_options_cont" id="cash_options_cont">
				</div>
				<div class="cash_other_value" id="cash_option_other">
					<span class="errorMsg" id="errorMsgCash"></span>
					<input type="text" name="cash_option_other" id="cash_option_other_value" placeholder="Ingresa otro valor">
					<button class="btn_green_full next_other_cash" id="next_other_cash">SIGUIENTE</button>
				</div>
			</div>

			<div class="card_option_cont hide" id="card_option_cont">
				<div class="SimpleOptionsBox">
					<div id="credit_card"> <img src="../icons/SVG/44-money/credit-card-visa.svg"> <p>Crédito</p></div>
					<div id="debit_card"> <img src="../icons/SVG/44-money/credit-card-hand.svg"> <p>Débito</p></div>
				</div>
				<div class="cash_other_value" id="card_option">
					<input type="text" name="card_option_other" id="card_option_value" placeholder="Número de comprobación">
					<span class="errorMsg" id="errorMsgCard"></span>
					<button class="btn_green_full next_card" id="next_card">SIGUIENTE</button>
				</div>
			</div>

			<div class="other_option_cont hide" id="other_option_cont">
				<div class="cash_other_value" id="other_option">
					<input type="text" name="other_option_other" id="other_option_value" placeholder="Método de pago" maxlength="50">
					<span class="errorMsg" id="errorMsgOther"></span>
					<button class="btn_green_full next_other" id="next_other">SIGUIENTE</button>
				</div>
			</div>

		</div>	
	</div>

	<!-- MODAL CHECKOUT 3 -->
	<div class="msg-box-container" id="msg-box-container-checkout3">
		<div class="msg-box-1" id="msg-box-checkout3">

			<span class="mdi mdi-chevron-left previous_arrow" id="go_to_co2"></span>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> ¡TRANSACCIÓN COMPLETA!</p>

			<p class="change_bill">Cambio de $ <span id="change_bill"></span></p>
			<p class="payment_final_bill">Pago $ <span id="payment_final_bill"></span></p>
			

			<div class="final_mark_trx_cont">
				<img class="final_mark_trx" src="../icons/SVG/41-shopping/shopping-bag-check.svg">
			</div>
			
			<span class="errorMsg" id="errorMsgCheckOut"></span>
			<div class="LoadingRoller" id="LoadingRoller_checkout"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<svg class="checkmark" id="checkmark_checkout" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
			<button class="btn_green_full" id="endBillTrx">TERMINAR</button>
		</div>	
	</div>
	
	<!-- MODAL ADMIN CREATOR -->
	<div class="msg-box-container" id="msg-box-container-adminCreator">
		<div class="msg-box-1" id="msg-box-adminCreator">

			<p class="msgb-box-1-title" id="caption_profile">ADMINISTRADOR</p>
			<p class="msg-box-1-description"> Parece que <b>no tienes un perfil de administrador</b> creado aún. Para crear uno, solo ingresa la contraseña de tu cuenta en luca y una clave de <b>4 digitos</b> que será utilizada para entrar a tu perfil de administrador.</p>
			
			<div class="cadm1">
				<p>Contraseña Luca:</p>
				<input type="password" class="msg-box-1-input" name="inpPassLuca" id="inpPassLuca" title="Tu contraseña de cuenta para iniciar sesión en Luca">
			</div>
			
			<div id="passProfileAdmin" class="mv2a mv2a_show">
				<div class="mv2a_1">
					<p>Clave:</p>
					<input type="password" class="msg-box-1-input" name="inpPassProfAdmin" id="inpPassProfAdmin" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
				</div>
				<div class="mv2a_1">
					<p>Repite la clave:</p>
					<input type="password" class="msg-box-1-input" name="inpPassProfAdmin2" id="inpPassProfAdmin2" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
				</div>
			</div>
			
			<span class="errorMsg" id="errorMsgAdminCreate"></span>
			<!--<div class="LoadingRoller" id="LoadingRoller_CreateProfile"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<svg class="checkmark" id="checkmark_CreateProfile" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
			--><button class="btn_green_full" id="BtnOpenAdminCreate">CREAR PERFIL</button>
		</div>	
	</div>
	
	<!-- MODAL VENDEDORES -->
	<div class="msg-box-container" id="msg-box-container-salesman_admin">
		<div class="msg-box-1" id="msg-box-salesman_admin">
			<p class="msgb-box-1-exit-mark" id="exit_mark_profile" onclick="CloseSalesman_admin()">X</p>

			<p class="msgb-box-1-title" id="caption_profile">PERFILES DE USUARIO</p>
			
			<div id="cont_admin"></div>
						<div class="LoadingRoller" id="LoadingRoller_Salesmen"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<div class="cont_ap" id="cont_ap"></div>
			
			<div class="cont_no_perf hide">
				<img src="../icons/SVG/08-users-actions/person-add-1.svg">
				<p>No tienes perfiles creados aún</p>
			</div>
			
			<span class="errorMsg" id="errorMsgAdminProfile"></span>
						<button class="btn_green_full" id="BtnOpenCreateProfile">CREAR NUEVO PERFIL</button>
		</div>	
	</div>

	<!-- MODAL PASWORD VENDEDORES -->
	<div class="msg-box-container" id="msg-box-container-SetPassProf">
		<div class="msg-box-1" id="msg-box-SetPassProf">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseSetPassProf()">X</p>
			
			<p id="pwVend" class="pwVend">El perfil <span id="pv_n"></span> no está protegido. Para protegerlo ingresa una clave de 4 dígitos a continuación.</p>
			
			<div id="passProfile2" class="mv2a mv2a_show">
				<div class="mv2a_1">
					<p>Contraseña:</p>
					<input type="password" placeholder="Ej: 1234" class="msg-box-1-input" name="inpPassProf1_b" id="inpPassProf1_b" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
				</div>
				<div class="mv2a_1">
					<p>Repite la contraseña:</p>
					<input type="password" class="msg-box-1-input" name="inpPassProf2_b" id="inpPassProf2_b" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
				</div>
			</div>
			
			<span class="errorMsg" id="errorSetPassword"></span>
			<!--<div class="LoadingRoller" id="LoadingRoller_CreateProfile"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<svg class="checkmark" id="checkmark_CreateProfile" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
			--><button class="btn_green_full" id="BtnSetPassword">ACEPTAR</button>
		</div>	
	</div>
	
	<!-- MODAL ENTER PASWORD VENDEDORES -->
	<div class="msg-box-container" id="msg-box-container-EntPassProf">
		<div class="msg-box-1" id="msg-box-EntPassProf">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEntPassProf()">X</p>
			
			<p id="pwVend" class="pwVend">Ingresa la contraseña de 4 dígitos para abrir el perfil: <span id="pv_n_2"></span></p>
			
			<div id="passProf_cont" class="mv2a mv2a_show">
				<div class="mv2a_1 mv2a_1_a">
					<input type="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" class="msg-box-1-input" name="PassProf" id="PassProf" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos" autofocus>
								</div>
			</div>
			
			<span class="errorMsg" id="errorPassword"></span>
						<div class="LoadingRoller" id="LoadingRoller_PassProf"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<button class="btn_green_full" id="BtnPassword">ACEPTAR</button>
		</div>	
	</div>
	
	<!-- MODAL CHOOSE AVATAR -->
	<div class="msg-box-container" id="msg-box-container-SetAvatarProf">
		<div class="msg-box-1" id="msg-box-SetAvatarProf">
			<div id='avtrs_close_mark'></div>
			
			<p id="pwVend" class="pwVend">Escoge un avatar para este perfil:</p>
			
			<div id='avtrs_container'></div>
			
		</div>	
	</div>

	<!-- MODAL CREAR VENDEDORES -->
	<div class="msg-box-container" id="msg-box-container-salesman">
		<div class="msg-box-1" id="msg-box-salesman">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseSalesman()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title">CREAR PERFIL</p>
			
			<div class="mv1">
				<div class="mv1a" id="choose_avtr" title="Seleccionar Avatar">
					<img id="avtProf" src="../avatars/manblackjacket.svg" alt="AVATAR">
				</div>
				
				<div class="mv1b">
					<p>Nombre del perfil</p>
					<input type="text" class="msg-box-1-input" name="inpNameProfile" id="inpNameProfile" placeholder="Ej: Cajero, Vendedor, ..." autofocus>
				</div>
			</div>
			
			<div class="mv2">		
				<label class="container_radiobtn" for="lockProfile" id='lblLockProfile' title="Proteger perfil con contraseña">
									<img id="imgLock" src="../icons/SVG/20-lock/lock-close-1.svg">
									<input type="checkbox" id="lockProfile" name="lockProfile" value="1">
									<span class="radiobtnstl"></span>
								</label>
							<div id="passProfile" class="mv2a">
					<div class="mv2a_1">
						<p>Contraseña de 4 dígitos</p>
						<input type="password" placeholder="Ej: 1234" class="msg-box-1-input" name="inpPassProf1" id="inpPassProf1" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
					</div>
					<div class="mv2a_1">
						<p>Repite la contraseña</p>
						<input type="password" class="msg-box-1-input" name="inpPassProf2" id="inpPassProf2" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
					</div>
				</div>
			</div>
			
			<div class="mv3">
				<p>Selecciona los permisos que le deseas otorgar a este perfil:</p>
				<div class="mv3a">
					<div class="mv3a_1">
						<p>ITEMS</p>
						<div><input type="checkbox" id="cbItmsCre" value="1"> <label for="cbItmsCre">Crear ítem</label></div>
						<div><input type="checkbox" id="cbItmsEdi" value="1"> <label for="cbItmsEdi">Editar ítem</label></div>
						<div><input type="checkbox" id="cbItmsDel" value="1"> <label for="cbItmsDel">Eliminar ítem</label></div>
					</div>
					<div class="mv3a_1">
						<p>CLIENTES</p>
						<div><input checked="true" type="checkbox" id="cbCustCre" value="1"> <label for="cbCustCre">Crear cliente</label></div>
						<div><input checked="true" type="checkbox" id="cbCustEdi" value="1"> <label for="cbCustEdi">Editar cliente</label></div>
						<div><input type="checkbox" id="cbCustDel" value="1"> <label for="cbCustDel">Eliminar cliente</label></div>
					</div>
				</div>
				
				<div class="mv3a">
					<div class="mv3a_1">
						<p>FACTURAS</p>
						<div><input checked="true" type="checkbox" id="cbBillsCre" value="1"> <label for="cbBillsCre">Crear factura</label></div>
						<div><input type="checkbox" id="cbBillsDel" value="1"> <label for="cbBillsDel">Eliminar factura</label></div>
					</div>
					<!-- <div class="mv3a_1">
						<p>ANALÍTICA</p>
						<div><input type="checkbox" id="cbAnalStats" value="1"> <label for="cbAnalStats">Ver Estadísticas</label></div>
						<div><input type="checkbox" id="cbAnalDashb" value="1"> <label for="cbAnalDashb">Ver Dashboard</label></div>
					</div> -->
				</div>
			</div>
			
			<span class="errorMsg" id="errorMsgCreateProfile"></span>
			<!--<div class="LoadingRoller" id="LoadingRoller_CreateProfile"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<svg class="checkmark" id="checkmark_CreateProfile" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
			--><button class="btn_green_full" id="BtnCreateProfile">CREAR PERFIL</button>
		</div>	
	</div>

		<!-- MODAL EDITAR VENDEDORES -->
	<div class="msg-box-container" id="msg-box-container-editsalesman">
		<div class="msg-box-1" id="msg-box-editsalesman">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditSalesman()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title">EDITAR PERFIL</p>
			
			<div class="mv1">
				<div class="mv1a" id="edit_choose_avtr" title="Seleccionar Avatar">
					<img id="avtEditProf" src="../avatars/manblackjacket.svg" alt="AVATAR">
				</div>
				
				<div class="mv1b">
					<p>Nombre del perfil</p>
					<input type="text" class="msg-box-1-input" name="inpNameEditProfile" id="inpNameEditProfile" placeholder="Ej: Cajero, Vendedor, ...">
										<input type="text" name="inpNameEditProfileOriginal" id="inpNameEditProfileOriginal" disabled hidden>
								</div>
			</div>
			
			<div class="mv2">		
							<div id="passEditProfile" class="mv2a mv2a_show">
										<p class="pep_1" id="pep_1"></p>
					<div class="mv2a_1">
						<p>Contraseña de 4 dígitos</p>
						<input type="password" placeholder="Ej: 1234" class="msg-box-1-input" name="inpPassEditProf1" id="inpPassEditProf1" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
					</div>
					<div class="mv2a_1">
						<p>Repite la contraseña</p>
						<input type="password" class="msg-box-1-input" name="inpPassEditProf2" id="inpPassEditProf2" maxlength="4" pattern="[0-9]{4}" title="Formato: 4 dígitos">
					</div>
				</div>
								<label class="container_radiobtn" for="unlockProfile" id='lblunLockProfile' title="Desproteger perfil">
									<p class="unlockProfile_txt">Desproteger el perfil (no recomendado)</p>
									<input type="checkbox" id="unlockProfile" name="unlockProfile" value="1">
									<span class="radiobtnstl"></span>
								</label>
			</div>
			
			<div class="mv3">
				<p>Selecciona los permisos que le deseas otorgar a este perfil:</p>
				<div class="mv3a">
					<div class="mv3a_1">
						<p>ITEMS</p>
						<div><input type="checkbox" id="edit_cbItmsCre" value="1"> <label for="edit_cbItmsCre">Crear ítem</label></div>
						<div><input type="checkbox" id="edit_cbItmsEdi" value="1"> <label for="edit_cbItmsEdi">Editar ítem</label></div>
						<div><input type="checkbox" id="edit_cbItmsDel" value="1"> <label for="edit_cbItmsDel">Eliminar ítem</label></div>
					</div>
					<div class="mv3a_1">
						<p>CLIENTES</p>
						<div><input type="checkbox" id="edit_cbCustCre" value="1"> <label for="edit_cbCustCre">Crear cliente</label></div>
						<div><input type="checkbox" id="edit_cbCustEdi" value="1"> <label for="edit_cbCustEdi">Editar cliente</label></div>
						<div><input type="checkbox" id="edit_cbCustDel" value="1"> <label for="edit_cbCustDel">Eliminar cliente</label></div>
					</div>
				</div>
				
				<div class="mv3a">
					<div class="mv3a_1">
						<p>FACTURAS</p>
						<div><input type="checkbox" id="edit_cbBillsCre" value="1"> <label for="edit_cbBillsCre">Crear factura</label></div>
						<div><input type="checkbox" id="edit_cbBillsDel" value="1"> <label for="edit_cbBillsDel">Eliminar factura</label></div>
					</div>
					<!-- <div class="mv3a_1">
						<p>ANALÍTICA</p>
						<div><input type="checkbox" id="edit_cbAnalStats" value="1"> <label for="edit_cbAnalStats">Ver Estadísticas</label></div>
						<div><input type="checkbox" id="edit_cbAnalDashb" value="1"> <label for="edit_cbAnalDashb">Ver Dashboard</label></div>
					</div> -->
				</div>
			</div>
			
			<span class="errorMsg" id="errorMsgEditProfile"></span>
			<!--<div class="LoadingRoller" id="LoadingRoller_CreateProfile"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
			<svg class="checkmark" id="checkmark_CreateProfile" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
			--><button class="btn_green_full" id="BtnEditProfile">ACEPTAR</button>
		</div>	
	</div>
	
	<!-- OPEN EDIT ITEM IN BILL -->
	<div class="msg-box-container" id="msg-box-container-edititembill">
		<div class="msg-box-1" id="msg-box-edititembill">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditItemBill()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> EDITAR ÍTEM</p>

			<form action="" name="edititembill_form" id="edititembill_form">
					<input type="reset" value="Limpiar todo"/>
				<h4>Cantidad:</h4>
				<div class="inputBoxOptions">
					<input id="edititembill_count" class="msg-box-1-input" type="text" placeholder="Ej: 110, 0.36, 10.1 . . .">
					<span id="EditItemInBill_addCant" title="+ 1"><img src="../icons/SVG/27-remove-add/add-circle-1.svg"></span>
					<span id="EditItemInBill_subCant" title="- 1"><img src="../icons/SVG/27-remove-add/subtract-circle-1.svg"></span>
				</div>
				<h4 class="rest_vis">Agregar Adición:</h4>
				<!-- <h5 class="rest_vis">Crear adición</h5> -->
				<select style="display: none;" tabindex="-1" name="edititembill_adicion" id="edititembill_adicion"  data-placeholder="Selecciona adiciones ..." multiple></select>
				<h4>Descuento:</h4>
				<div class="inputBoxOptions">
					<input class="msg-box-1-input" type="text" name="edititembill_discount_val" id="edititembill_discount_val" placeholder="Selecciona el tipo &#10230;">
					<span id="EditItemInBill_Porce" title="PORCENTAJE"><img src="../icons/SVG/41-shopping/coupon-discount.svg"></span>
					<span id="EditItemInBill_Pesos" title="PESOS"><img src="../icons/SVG/41-shopping/price-tag.svg"></span>
				</div>
				<h4>Nota:</h4>
				<textarea class="msg-box-1-textarea" name="edititembill_note" id="edititembill_note" rows="5" cols="30" placeholder="Agrega una nota al ítem . . . "></textarea>				
								<span class="simpleMsg" id="simpleMsgEdititembill"></span>
								<span class="errorMsg" id="errorMsgEdititembill"></span>
				<input type="submit" name="edititembill-submit" id="edititembill-submit" value="ACEPTAR">
			</form>

			<div class="horizDivide"></div>
			
			<div title="ELIMINAR TODO EL ÍTEM" id="removeAllItem" class="removeAllItem"><span class="mdi mdi-delete-outline"></span></div>

		</div>	
	</div>
		<!-- MODAL HELP SCAN BARCODE -->
	<div class="msg-box-container" id="msg-box-container-helpscan">
		<div class="msg-box-1" id="msg-box-helpscan">
						<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseHelpScan()">X</p>

			<div id="help_scan_1" class="box_help_scan box_help_scan_opn">
								<img class="img_helpscan" src="../icons/illustrations/code_light.svg">
								<p class="title_helpscan">LA BUENA LUZ ES ESENCIAL</p>
								<p class="descr_helpscan">Intenta utilizar siempre un espacio con una buena fuente de luz para escanear los ítems. Si es luz solar directa mucho mejor.</p>
								<div>
										<p onclick="goto_scan('2')" class="bcod_pr2">SIGUIENTE</p>
								</div>
						</div>
						<div id="help_scan_2" class="box_help_scan">
								<img class="img_helpscan" src="../icons/illustrations/code_resolution.svg">
								<p class="title_helpscan">UTILIZA BUENA RESOLUCIÓN</p>
								<p class="descr_helpscan">Intenta utilizar una cámara de buena resolución para escanear los ítems. La mínima resolución es 640x380 píxeles.</p>
								<div>
										<p onclick="goto_scan('1')" class="bcod_pr2">ATRÁS</p>
										<p onclick="goto_scan('3')" class="bcod_pr2">SIGUIENTE</p>
								</div>
						</div>
						<div id="help_scan_3" class="box_help_scan">
								<img class="img_helpscan" src="../icons/illustrations/code_closer.svg">
								<p class="title_helpscan">ACERCATE LO MÁS POSIBLE</p>
								<p class="descr_helpscan">Acerca la camara lo más posible al código de barras hasta la distancia mínimo de enfoque de tu cámara. No dejes que se pierda el enfoque.</p>
								<div>
										<p onclick="goto_scan('2')" class="bcod_pr2">ATRÁS</p>
								</div>
						</div>
			
		</div>	
	</div>
		
	<!-- MODAL CONFIG ITEM -->
	<div class="msg-box-container" id="msg-box-container-configitem">
		<div class="msg-box-1" id="msg-box-configitem">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemOptions()">X</p>
						
			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-cart-plus"></span> OPCIONES DE ÍTEM</p>
			<p class="msg-box-1-description">Selecciona una de las siguientes opciones para el ítem <span id="configitem_nameItem1"></span> (<span id="configitem_idItem1"></span>)</p>
			
			<button class="btn_green_full" id="optionItemEdit"><span class="mdi mdi-pencil"></span> EDITAR</button>
			<button class="btn_green_full" id="optionItemStats"><span class="mdi mdi-chart-arc"></span> ESTADÍSTICAS</button>
			<button class="btn_green_full" id="qrCodeGenerate"><span class="mdi mdi-qrcode"></span> CÓDIGO QR</button>
			<div class="horizDivide"></div>
			<button class="btn_green_full bgBtnRed" id="optionItemDelete"><span class="mdi mdi-delete-outline"></span> ELIMINAR</button>

		</div>	
	</div>
	
	<!-- QR MODAL -->
	<div class="msg-box-container" id="msg-box-container-qrcode">
		<div class="msg-box-1" id="msg-box-qrcode">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseQRCode()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> CÓDIGO QR</p>
			<p class="msg1-end-newprod"> Da click en la siguiente imagen para descargar el código QR de este producto </p>
			
			<center>
				<a id="aQRCode2" href="#" target="_blank"><img id="QRCodeImg2" width="250" height="250"></a>
			</center>
			
			<center><p class="msg-box-1-footnone" id="linkQRCode"></p></center>

		</div>	
	</div>

	<!-- MODAL ITEMS BILL-->
	<div class="msg-box-container" id="msg-box-container-itemsbill">
		<div class="msg-box-1" id="msg-box-itemsbill">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemsBill_over()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> ÍTEMS</p>
			
			<center>
				<div class="items_bill_cont">
						<div id="items_bill_wrap" class="items_bill_wrap"></div>
				</div>
			</center>

		</div>	
	</div>
	
	<!-- ANALITICA ITEM -->
	<div class="msg-box-container" id="msg-box-container-itemstats">
		<div class="msg-box-1 mw95p" id="msg-box-itemstats">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseItemStats()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> ESTADÍSTICAS</p>
			
			
			<div class="TimeStatsContainer" id="TimeStatsContainer">
				<select name="TimeStats" id="TimeStats" class="select_list_1"> 
						<option value="7" selected>Última semana</option>
						<option value="15">Últimos 15 días</option>
						<option value="30">Último mes</option>
						<option value="31">Este mes</option>
				</select>
			</div>

			<div class="LoadingRoller" id="LoadingRoller_itemStat">
				<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
			
			<div id="NoSalesItem" class="NoSalesItem hide">
				<img src='../icons/SVG/43-shipping-delivery/shopping-cart-box.svg' alt='No Sales'>
				<p>No has generado ventas de este ítem</p>
			</div>
			
			<div id="infoStatsItem" class="infoStatsItem hide">
				<div class="box1_info_stats infoBoxStyle1">
					<p id="TotWeekSales"></p>
					<p id="UnitsStatsItem"></p>
				</div>
				
				<div class="box1_info_stats infoBoxStyle1">
					<p id="TotWeekMoney"></p>
					<p>Pesos</p>
				</div>
				
				<div class="box1_info_stats infoBoxStyle1">
					<p id="TotWeekAvgSales"></p>
					<p><span id="UnitsStatsItem2"></span> x Día</p>
				</div>
				
				<div class="box1_info_stats infoBoxStyle1">
					<p id="TotWeekAvgSalesValue"></p>
					<p>Pesos x Día</p>
				</div>
				
			</div>
					
			<div id="chartsItem" class="chartsItem hide">
				<div class="ChartSalesItem" id="ChartSalesItem">
					<canvas id="ItemChartLastWeekSales"></canvas>
				</div>
				
				<div class="ChartSalesItem" id="ChartSalesItem2">
					<canvas id="ItemChartLastWeekSalesValue"></canvas>
				</div>
				
			</div>
			
		</div>	
	</div>
	
	<!-- EDITAR ÍTEM -->
	<div class="msg-box-container" id="msg-box-container-edititem">
		<div class="msg-box-1" id="msg-box-edititem">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseEditItem()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-pencil"></span> EDITAR ÍTEM</p>
			<p class="msg1-end-newprod">Cambia los campos y da click en aceptar para editar el ítem <span id="itemEditId"></span></p>
			
			<form action="edit_item.php" method="POST" name="edit-item-form" id="edit-item-form">
					<input type="reset" value="Limpiar todo"/>
				<h4>Nombre:</h4>
				<input type="text" name="ni_prod_name2" id="ni_prod_name2" placeholder="Ej: Agua Mineral" required minlength="1" maxlength="50">
				<input type="text" name="ni_prod_code2" id="ni_prod_code2" placeholder="Ej: AM-0001" required minlength="1" maxlength="15" hidden="true">
				<h4>Precio Unitario:</h4>
				<input type="text" class="input_currency" name="ni_prod_price2" id="ni_prod_price2" placeholder="Ej: $4,200" required>
				<h4>Costo Unitario:</h4>
				<input type="text" class="input_currency" name="ni_prod_cost2" id="ni_prod_cost2" placeholder="Ej: $2,100">
				<h4>Unidades:</h4>
				<input type="text" name="ni_prod_units2" id="ni_prod_units2" placeholder="Ej: Botellas" required minlength="1" maxlength="20">
				<h4>Categoría:</h4>
				<h5 id="open_new_catg2">Crear Categoría</h5>
				<select name="ni_prod_category2" id="ni_prod_category2" > 
				</select>
				<div class="new_category" id="new_category2">
					<input type="text" name="catg_name2" id="catg_name2" placeholder="Nombre de la Categoría" class="input_cat_style" minlength="1" maxlength="30">
					<div class="cont_colors">
						<div onclick="AddColorSelectionClass(this)" class="color_crcl" id="c_c1"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c2"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c3"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c4"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c5"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c6"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c7"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c8"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c9"></div>
						<div onclick="AddColorSelectionClass(this)"class="color_crcl" id="c_c10"></div>
					</div>	
					<p class="btn_create_cat" id="btn_create_cat2">CREAR CATEGORÍA</p>
					<p id="msg_cat2"></p>
				</div>
				<h4>Impuesto:</h4>
				<input type="text" name="ni_prod_tax2" id="ni_prod_tax2" placeholder="Ej: 19%" class="input_perc" minlength="1" maxlength="30">
				<h4>Cambiar Icono:</h4>
				<img onclick="OpenGallery()" class="icon_selection_btn" id="icon_selection_btn2" src="../icons/SVG/78-video-games/mario-question-box.svg">
				<input type="text" name="ni_prod_icon2" id="ni_prod_icon2" value="../icons/SVG/78-video-games/mario-question-box.svg" hidden="true">
				
				
				<span class="errorMsg" id="errorMsgEditProduct"></span>
				<div class="LoadingRoller" id="LoadingRoller_EditItem"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
				<svg class="checkmark" id="checkmark_EditItem" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
				<input type="submit" name="editItemSubmit" id="editItemSubmit" value="ACEPTAR">
				
			</form>

		</div>	
	</div>

	<!-- EDITAR ÍTEM -->
	<div class="msg-box-container" id="msg-box-container-barcodefound">
		<div class="msg-box-1" id="msg-box-barcodefound">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseBarcodeFound()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"><span class="mdi mdi-barcode-scan"></span> AGREGAR PRODUCTO</p>
			<p class="msg-box-1-description">Hemos encontrado un producto asociado con el código de barra que ingresaste</p>

						<div class="infobarcodeCont table_style">
								<div class="tablecell_style w30p">
										<img alt="Producto" id="barcode_fnd_icon" src="">
								</div>
								<div class="namebarcodecont label_n_data w70p">
										<p>Nombre del Producto</p>
										<p id="barcode_fnd_name"></p>
								</div>
						</div>

						<div id="barcode_img_cont" class="barcode_img_cont hide"></div>

						<div class="horizDivide w90p"></div>
						<div class="msg-box-1-panel_btns">
								<button class="btn_soft_style" id="btn_BarcodeEditAndAdd">EDITAR Y AGREGAR</button>
								<button class="btn_soft_style" id="btn_BarcodeIncludeOnce" onclick="BarcodeIncludeOnce()">INCLUIR SOLO ESTA VEZ</button>
						</div>

						<p class="coderef_url">*Código generado por <a href="https://www.tec-it.com" target="_blank">TEC-IT Barcode Software</a></p>
						
		</div>	
	</div>

	<!-- Report Voice Commands -->

	<div class='msg-box-container' id='msg-box-container-SpeechReport'>
		<div class='msg-box-1' id='msg-box-SpeechReport'>
			<p class='msgb-box-1-exit-mark' id='msgb-box-1-exit-mark' onclick='CloseSpeechReport()'>X</p>
			<p class='msgb-box-1-title' id='msgb-box-1-title'><span class="mdi mdi-robot"></span> REPORTE</p>
			<p class='msg-box-1-description' id='sp_rp_msg'></p>

			<div id="DataSpcReport" class="DataSpcReport">
				<div>
					<p id='sp_rp_numCnt'>0</p>
					<p>Ítems</p>
				</div>
				<div>
					<p id='sp_rp_amt'>$ 0</p>
					<p>Vendido</p>
				</div>
			</div>
			
		</div>
	</div>
	
	<!-- Help Voice Commands -->
	<div class='msg-box-container' id='msg-box-container-HelpModule'>
		<div class='msg-box-1' id='msg-box-HelpModule'>
			<p class='msgb-box-1-exit-mark' id='msgb-box-1-exit-mark' onclick='CloseHelpModule()'>X</p>
			<p class='msgb-box-1-title' id='msgb-box-1-title'><span class="mdi mdi-information-outline"></span> AYUDA</p>
			<p class='msg-box-1-description' id='sp_rp_msg'></p>

			<div class="helpModalVoice">
				<p>Sí te preguntas qué tipo de cosas puede hacer el asistente de Luca, aquí te va una lista de ejemplos que te pueden ser de ayuda:</p>
				<div class="listSentencesHelp">
					<div>
					<p><span class="mdi mdi-chart-areaspline"></span>Reporte de ventas</p>
					<p>Obtén un reporte sencillo de las ventas que realizaste en un período determinado diciendo:</p> 
					<p>"reporte de ventas de hoy" o "reporte del 13 de junio al 31 de diciembre" o "estadísticas del día/mes/año" o "¿cuánto vendí ayer?"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-cart-plus"></span>Añade productos a la cuenta</p>
					<p>Intenta decir <b>anãdir</b>, <b>incluir</b>, <b>adicionar</b> o <b>agregar</b> seguido de los nombres de uno o varios productos para agregarlos a la cuenta actual.</p> 
					<p>"añadir agua con gas" o "agregar agua y arroz con verduras"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-cart-minus"></span>Remueve productos de la cuenta</p>
					<p>Intenta decir <b>remover</b>, <b>quitar</b>, <b>suprimir</b> o <b>desagregar</b> seguido los nombres de uno o varios productos para removerlos de la cuenta.</p> 
					<p>"remover agua con gas"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-cart-remove"></span>Limpiar la cuenta</p>
					<p>Para limpiar todos los ítems que hay en la cuenta actualmente solo tienes que decir:</p> 
					<p>"limpiar cuenta" o "limpiar"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-cash-register"></span>Procesar pago</p>
					<p>Para procesar el pago actual que tienes en la cuenta intenta decir:</p> 
					<p>"pagar" o "procesar pago" o "facturar"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-account-plus-outline"></span>Crea un nuevo cliente</p>
					<p>Puedes abrir la ventana de creación de clientes con datos precargados diciendo:</p> 
					<p>"nuevo cliente de nombre Juan con cédula 123456" o "nuevo cliente"</p> 
					</div>
					<div>
					<p><span class="mdi mdi-carrot"></span>Crea un nuevo ítem</p>
					<p>Puedes abrir la ventana de creación de ítems con datos precargados diciendo:</p> 
					<p>"nuevo ítem manzana roja" o "nuevo ítem"</p> 
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<!-- Offline Container Admin  -->
	<div id="msg-box-container-OffLineAdmin" class="msg-box-container">
		<div class="box-wrap-list" id="msg-box-OffLineAdmin">

			<div class="box-header-list">
				<div><i class="far fa-bell"></i></div>
				<div>Cuentas por enviar</div>
				<div><p class="msgb-box-1-exit-mark" id="Close-OffLineAdmin" onclick="OffLineAdmin.close()">X</p></div>
			</div>
			
			<div id="wrap-offline-bills" class="box-content-list"></div>
			
			<div class="box-actions-list">
				<div id="sendAllOffBills2" class="btn_soft_style">
					<i class="far fa-paper-plane"></i> 
					Enviar todo
				</div>
				<div id="cleanAllOffBills2" class="btn_soft_style">
					<i class="far fa-trash-alt"></i> 
					Eliminar todo
				</div>
			</div>
			
			
		</div>
	</div>

	<!-- Open modal gallery images -->
	<div class="container_prelogos_back" id="container_prelogos_back">
		<div class="container_prelogos animated fadeIn" id="container_prelogos">
			<p class="prelogos-exit-mark" id="prlog-exit-mark" onclick="CloseGallery()">X</p>

			<div class="container_prelogos_i1">
				<select name="logo_categ_list" id="logo_categ_list" class="select_list_1"> 
						<option value="74-food" selected>Selecciona una categoría</option>
						<option value="74-food">Alimentos</option>
						<option value="73-drink">Bebidas</option>
						<option value="58-beauty-spas">Belleza y Spa</option>
						<option value="75-kitchen">Cocina</option>
						<option value="62-construction">Construcción</option>
						<option value="60-sport">Deportes</option>
						<option value="82-school-science">Educación</option>
						<option value="63-space">Espacio y astronomía</option>
						<option value="67-tools">Herramientas</option>	
						<option value="57-hotel">Hotelería y Turismo</option>
						<option value="69-lights">Luces</option>
						<option value="53-places">Lugares del Mundo</option>
						<option value="64-pet">Mascotas y Animales</option>
						<option value="59-nature-ecology">Naturaleza</option>
						<option value="81-christmas">Navidad</option>
						<option value="80-children">Niños</option>
						<option value="68-objects">Objetos</option>
						<option value="77-leisure">Ocio </option>
						<option value="65-religion">Religión</option>
						<option value="79-romance">Romance</option>
						<option value="76-clothes">Ropa</option>				   			    		   
						<option value="72-health">Salud</option>				    			    
						<option value="71-security">Seguridad</option>				    			    
						<option value="61-transportation">Transporte</option>
						<option value="55-travel">Viajes</option>		
						<option value="78-video-games">Video Juegos</option>		    
				</select>
				
			</div>

			<div class="container_prelogos_i2" id="ctn_prelogos_i2">
				
			</div>

		</div>
	</div>



<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
<script src="../js/modals.js?<?php echo time(); ?>"></script>
<script src="../js/test_sess_active.js?<?php echo time(); ?>"></script>
<script src="../js/js_chatbot.js?<?php echo time(); ?>"></script>
<script src="../js/js_sales.js?<?php echo time(); ?>"></script>
<script src="../js/nlp_jcd.js?<?php echo time(); ?>"></script>
</body>

</html>