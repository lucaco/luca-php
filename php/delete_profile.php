<?php
/* 
 * FILE: delete_profile.php
 * WHAT FOR: Delete the entire row of profile from profiles table.
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 		= $_SESSION['login_user'];
	$prof_name 		= mysqli_real_escape_string($db,$_POST['prof_name']);

	$query = "DELETE FROM profiles WHERE bus_email = '$bus_email' AND prof_name = '$prof_name';";
    mysqli_query($db,$query);
   	echo '1';
}
?>
