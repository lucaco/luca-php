<?php
/*
 * FILE: pull_client_based_recom.php
 * WHAT FOR: Pull top items for customer based recomendation.
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
    $barcode = trim(mysqli_real_escape_string($db,$_POST['barcode']));

    // ***** SEARCH ALL INFO
    $query = "SELECT * FROM a_products WHERE prod_id = '$barcode' LIMIT 1;";    
    $result = mysqli_query($db,$query);
    $count  = mysqli_num_rows($result);

	$return_arr = Array();
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		array_push($return_arr,$row);
	}

    if($count>0){ 
        // **** SEACH AVERAGE PRICE
        $query2 = "SELECT prod_id, AVG(prod_unit_price) AS AVG_PRICE FROM a_products WHERE prod_id = '$barcode' GROUP BY prod_id;";    
        $result2 = mysqli_query($db,$query2);
        $count2  = mysqli_num_rows($result2);

        while ($row2 = mysqli_fetch_array($result2,MYSQLI_ASSOC)) {
            array_push($return_arr,$row2);
        }
    }

    echo json_encode($return_arr);
}
?>


