<?php
/* 
 * FILE: create_admin_profile.php
 * WHAT FOR: Create a administrator profile in profiles account. Can just be one
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email    = $_SESSION['login_user'];
	$pass 		  = trim(mysqli_real_escape_string($db,$_POST['pass']));
	$pass_admin   = trim(mysqli_real_escape_string($db,$_POST['pass_admin']));
	$pass_admin2  = trim(mysqli_real_escape_string($db,$_POST['pass_admin2']));
	
	$todays_date    = date('Y-m-d H:i:s');
	$profKey        = generateRandomString(30);
	
	$query = "SELECT pass_enc FROM register_users WHERE bus_email = '$bus_email';";
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	
	$return_arr = Array();
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		$pass_enc = $row['pass_enc']; 
	}

    if(password_verify($pass, $pass_enc)){
        if($pass_admin == $pass_admin2){
            if(strlen($pass_admin) == 4){
                $passEncode = password_hash($pass_admin,PASSWORD_DEFAULT);
                $query = "INSERT INTO profiles (bus_email, prof_name, prof_pass, avatar, items_1, items_2, items_3, customers_1, customers_2, customers_3, bills_1, bills_2, analytics_1, analytics_2, creation_date, admin, profsessid) VALUES ('$bus_email', 'Administrador', '$passEncode', '../avatars/maninsute.svg', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '$todays_date', 1,'$profKey');";
                mysqli_query($db,$query);
                echo '1';
            }else{
                echo 'La contraseña debe ser de 4 digitos';
            }
        }else{
            echo 'Las contraseñas no coinciden';
        }
    }else{
        echo 'La contraseña Luca es incorrecta';
    }

	    
}
?>
