<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
    $bus_email    = $_SESSION['login_user'];
 	$JsonItems    = mysqli_real_escape_string($db,$_POST['JsonItems']);
    $data         = json_decode(stripslashes($JsonItems),true);
    $todays_date  = date('Y-m-d H:i:s');

    $c = 0;
    $totItems = count($data);
    
    $SqlQuery = "INSERT INTO products (bus_email, prod_id, prod_name, prod_unit_price, prod_units, prod_icon, category, tax, creation_date, last_mod_date) VALUES";
    foreach($data as $item){
        $c = $c + 1;
        $prod_name = $item[0];
        $prod_code = $item[1];
        $prod_price = $item[2];
        $prod_price_clean = preg_replace("/[^0-9.]/", "", $prod_price);
        $prod_units = $item[3];
        if($prod_units == ''){
            $prod_units = 'Unidades';
        }
        $prod_tax = $item[4];
        if($prod_tax == ''){
            $prod_tax = 0;
        }
        $prod_icon = '../icons/SVG/78-video-games/mario-question-box.svg';
        $prod_category = 'Otros';
        if($c == $totItems){
            $SqlQuery = $SqlQuery . "('$bus_email', '$prod_code', '$prod_name', '$prod_price_clean', '$prod_units', '$prod_icon', '$prod_category', '$prod_tax', '$todays_date', '$todays_date')";       
        }else{
             $SqlQuery = $SqlQuery . "('$bus_email', '$prod_code', '$prod_name', '$prod_price_clean', '$prod_units', '$prod_icon', '$prod_category', '$prod_tax', '$todays_date', '$todays_date'),";                 
        }
    }
    $RunOk = mysqli_query($db, $SqlQuery);
    echo $RunOk;
 } 

?>