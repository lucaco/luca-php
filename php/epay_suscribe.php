<?php

include("functions.php");
include("config.php");
include("config_epayco.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){

  $bus_email 		 = $_SESSION['login_user'];
  $bus_name      = $_SESSION['login_bus_name'];
	$card_numer    = trim(mysqli_real_escape_string($db,$_POST['card_numer']));   //'4575623182290326';
  $card_year 	   = trim(mysqli_real_escape_string($db,$_POST['card_year']));    //'2017'; 
  $card_month    = trim(mysqli_real_escape_string($db,$_POST['card_month']));   //'07';  
  $card_cvc      = trim(mysqli_real_escape_string($db,$_POST['card_cvc']));     //'123';  
  $cust_name     = trim(mysqli_real_escape_string($db,$_POST['cust_name']));    //'Joe Doe'; 
  $cust_email    = trim(mysqli_real_escape_string($db,$_POST['cust_email']));   //'joe@payco.co'; 
  $cust_city     = trim(mysqli_real_escape_string($db,$_POST['cust_city']));    //'Bogota'; 
  $cust_address  = trim(mysqli_real_escape_string($db,$_POST['cust_address'])); //'Cr 4 # 55 36'; 
  $cust_phone    = trim(mysqli_real_escape_string($db,$_POST['cust_phone']));   //'3005234321'; 
  $susc_id       = trim(mysqli_real_escape_string($db,$_POST['susc_id']));      //'Analitica';
  $pkg_id        = trim(mysqli_real_escape_string($db,$_POST['pkg_id']));       //'1';
  $doc_type      = trim(mysqli_real_escape_string($db,$_POST['doc_type']));     //'CC'; 
  $doc_number    = trim(mysqli_real_escape_string($db,$_POST['doc_number']));   //'5234567';

  $today_date  = date('Y-m-d H:i:s');

  // ::::::::::::: CREAR TOKEN :::::::::::::
  $token = $epayco->token->create(array(
    "card[number]"    => $card_numer,
    "card[exp_year]"  => $card_year,
    "card[exp_month]" => $card_month,
    "card[cvc]"       => $card_cvc
  ));
  $token2     = json_encode($token);
  $token_json = json_decode($token2,true);
  // print_r($token2);

  $r = Array();
  if($susc_id!=""){
    if($token_json["status"]==true){

      // ::::::::::::: CREAR CLIENTE EN EPAYCO :::::::::::::
      $customer = $epayco->customer->create(array(
        "token_card" => $token->id,
        "name" => $cust_name,
        "email" => $cust_email,
        "default" => true,
        // Los siguientes parametros son opcionales (solo sirven cuando se vaya a procesear el pago)
        "city" => $cust_city,
        "address" => $cust_address,
        "phone" => $cust_phone 
      ));

      $customer2 = json_encode($customer);
      $cust_json = json_decode($customer2,true);

      if($cust_json["status"]==true){
        if($cust_json["data"]["customerId"] != '' && $token_json["id"] != ''){

          // ::::::::::::: CREAR LLAVE DE CLIENTE EN BASE DE DATOS :::::::::::::
          $query = "UPDATE register_users SET id_cust_pay = '".$cust_json["data"]["customerId"]."' WHERE bus_email = '".$bus_email."';";
          mysqli_query($db,$query);
          
          //::::::::::::: CREAR SUSCRIPCIÓN EN EPAYCO :::::::::::::
          $subp = $epayco->subscriptions->create(array(
            "id_plan"    => $susc_id,
            "customer"   => $cust_json["data"]["customerId"],
            "token_card" => $token_json["id"],
            "doc_type"   => $doc_type,
            "doc_number" => $doc_number 
          ));
          $subp2       = json_encode($subp);
          $subp_json   = json_decode($subp2,true);
          $sub_sts     = $subp_json["status"];                // 1 si se creo bien la suscripcion
          $sub_mes     = $subp_json["message"];               // Mensaje: "Subscripcion creada" (-Si se creo bien)

          if($sub_sts==1){    
            $sub_cre     = $subp_json["created"];               // Fecha de subscripcion
            $sub_id      = $subp_json["id"];                    // Id de la subscripcion
            $sub_curend  = $subp_json["current_period_end"];    // Fecha de Próximo Pago
            $sub_curstr  = $subp_json["current_period_start"];  // Fecha de Inicio del periodo
            $sub_tr_days = $subp_json["data"]["trialDays"];     // Días de Prueba
            $sub_name    = $subp_json["data"]["name"];          // Nombre del plan (Vision Cliente)
            $sub_desc    = $subp_json["data"]["description"];   // Descipcion del plan (Vision Cliente)
            $sub_amnt    = $subp_json["data"]["amount"];        // Costo del plan (EJ. 30000)
            $sub_intrv   = $subp_json["data"]["interval"];      // Intervalo del plan (EJ. month)            
            
            // ::::::::::::: GENERAR PAGO DE SUSCRIPCION ::::::::::::::::::
            $sub = $epayco->subscriptions->charge(array(
              "id_plan"    => $susc_id,
              "customer"   => $cust_json["data"]["customerId"],
              "token_card" => $token_json["id"],
              // "url_confirmation": "https:/secure.payco.co/restpagos/testRest/endpagopse.php",
              "doc_type"   => $doc_type,
              "doc_number" => $doc_number,
              "ip"         => getenv('REMOTE_ADDR')
            ));

            $sub2     = json_encode($sub);
            $sub_json = json_decode($sub2,true);

            if($sub_json["success"] == true){
              if($sub_json["data"]["cod_respuesta"]==1){

                // ::::::::::::: GUARDAR LLAVE DE SUSCRIPCION EN BASE DE DATOS :::::::::::::
                $query = "UPDATE marketplace SET pkg_".$pkg_id."_key = '".$sub_id."', pkg_".$pkg_id." = '1' WHERE bus_email = '".$bus_email."';";
                mysqli_query($db, $query);
                
                // ::::::::::::: ENVIAR CORREO ELECTRONICO DE CONFIRMACION :::::::::::::
                $email_sub = "Gracias por suscribirte al paquete de ".$sub_name;
                $email_body = "!Felicitaciones! Tu suscripción al paquete: <b>".$sub_name."</b> ha sido creada con éxito y desde este momento puedes hacer uso de todas sus herramientas.<br></br><br></br> Te invitamos a visitar nuestra guía de usuario siguiendo este link: <a href='https://medium.com/blog-2luca'>Guia de Usuario</a>. En ella encontrarás información sobre cómo usar el paquete, así como preguntas frecuentes y mucho más.<br></br><br></br> Puedes cancelar tu suscripción al paquete en cualquier momento y recuerda que si tienes algún inconveniente con tu pago o suscripción por favor no dudes en contactarnos usando tu código único de suscripción: <b>".$sub_id."</b>. Te recordamos además que tu próxima fecha de pago es el <b>".$sub_curend."</b>.";
                SendMail_SMTP_SimpleMsg($bus_email,$email_sub,$bus_name,$email_body);

                // ::::::::::::: ENVIAR CORREO ELECTRONICO DE FACTURA APROBADA :::::::::::::
                $email_sub2 = "Comprobante de pago en Luca";
                $email_body2 = "Este es tu comprobante de pago por el paquete de <b>".$sub_name."</b> en la tienda de Luca. <br></br><br></br>Estado: ".$sub_json["data"]["estado"]."<br></br> Número de Factura: ".$sub_json["data"]["factura"]."<br></br> Referencia de pago: ".$sub_json["data"]["ref_payco"]."<br></br> Fecha: ".$sub_json["data"]["fecha"]."<br></br> Respuesta: ".$sub_json["data"]["respuesta"]."<br></br> Valor Total: ".$sub_json["data"]["valor"]."<br></br> IVA: ".$sub_json["data"]["iva"]."<br></br> Base de IVA: ".$sub_json["data"]["baseiva"]."<br></br> Moneda: ".$sub_json["data"]["moneda"]."<br></br> Banco: ".$sub_json["data"]["banco"]."<br></br>";
                SendMail_SMTP_SimpleMsg($bus_email,$email_sub2,$bus_name,$email_body2);

                array_push($r,Array('S', "Transacción realizada con éxito."));

              }else{
                array_push($r,Array('E',"Transacción ".$sub_json["data"]["estado"].". Motivo: ".$sub_json["data"]["respuesta"]));
              }

            } else {
              array_push($r,Array('E',$sub_json["data"]["description"]));
            }
            
          }else{
            array_push($r,Array('E',$sub_mes));
          }

        }else{
          array_push($r,Array('E',"El Id del cliente no se pudo crear."));
        }

      }else{
        array_push($r,Array('E',$cust_json["message"]));
      }

    }else{
      array_push($r,Array('E',$token_json["message"]));
    }
    
  }else{
    array_push($r,Array('E',"Módulo no ingresado"));
  }
  
  echo json_encode($r);
}
?>