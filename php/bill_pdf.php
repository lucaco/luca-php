<?php

require('tcpdf/tcpdf.php');
session_start();
if($_POST){

	include("functions.php");
	include("config.php");
	date_default_timezone_set($TimeZone);

	$points_system  = 700; // 1 point for each 700$ purshased

	$bus_email      = $_SESSION['login_user'];
	$trx_value      = mysqli_real_escape_string($db,$_POST["trx_value2"]);
	$id_bill        = mysqli_real_escape_string($db,$_POST["id_bill2"]);
	$num_items      = mysqli_real_escape_string($db,$_POST["num_items2"]);
	$discount       = mysqli_real_escape_string($db,$_POST["discount2"]);
	$tips           = mysqli_real_escape_string($db,$_POST["tips2"]);
	$taxes          = mysqli_real_escape_string($db,$_POST["taxes2"]);
	
	//$id_cupon       = mysqli_real_escape_string($db,$_POST["id_cupon2"]);
	//$bill_type      = mysqli_real_escape_string($db,$_POST["bill_type"]);
	$payment_type     = mysqli_real_escape_string($db,$_POST["payment_type2"]);
	$other_payment_type   = mysqli_real_escape_string($db,$_POST["other_payment_type2"]);
	$creditcard_compr_number   = mysqli_real_escape_string($db,$_POST["creditcard_compr_number2"]);
	$cash_change    = mysqli_real_escape_string($db,$_POST["cash_change2"]);
	$cust_key       = mysqli_real_escape_string($db,$_POST["cust_key"]);
	$sqlInput       = mysqli_real_escape_string($db,$_POST["sqlInput2"]);
	$data           = json_decode(stripslashes($sqlInput), true);
	$subtotal_bill  = mysqli_real_escape_string($db,$_POST["subtotal_bill2"]);
	$totalbeforetaxes  = mysqli_real_escape_string($db,$_POST["totalbeforetaxes2"]);
	$paidbyuser     = mysqli_real_escape_string($db,$_POST["paidbyuser2"]);
    $profile_name   = mysqli_real_escape_string($db,$_POST["profile_name"]);
	
	$bus_name        = mysqli_real_escape_string($db,$_POST["bus_name"]);
	$bus_nit         = mysqli_real_escape_string($db,$_POST["bus_nit"]);
	$bus_address     = mysqli_real_escape_string($db,$_POST["bus_address"]);
	$bus_phone       = mysqli_real_escape_string($db,$_POST["bus_phone"]);
	$bus_regimen     = mysqli_real_escape_string($db,$_POST["bus_regimen"]);
	
	$bus_name    = str_replace("&nbsp;", '', $bus_name);
	$bus_nit     = str_replace("&nbsp;", '', $bus_nit);
	$bus_address = str_replace("&nbsp;", '', $bus_address);
	$bus_phone   = str_replace("&nbsp;", '', $bus_phone);
	$bus_regimen = str_replace("&nbsp;", '', $bus_regimen);

	$todays_date  = date('Y-m-d H:i:s');
	$todays_date2 = date('Y-m-d');
	
	if($bus_regimen==1){
		$bus_regimen='<p> Régimen Común </p>';
	}else if($bus_regimen==2){
		$bus_regimen='<p> Régimen Simplificado </p>';
	}
	if($bus_nit!=''){
		$bus_nit = '<p> NIT. '.$bus_nit.'</p>';
	}
	if($bus_address!=''){
		$bus_address = '<p>'.$bus_address.'</p>';
	}
	if($bus_phone!=''){
		$bus_phone = '<p>Tel: '.$bus_phone.'</p>';
	}
	
	if($payment_type==1){
		$payment_type = 'EFECTIVO';
	}else if($payment_type==2){
		$payment_type = 'TARJETA CRÉDITO';
	}else if($payment_type==3){
		$payment_type = 'TARJETA DÉBITO';
	}else if($payment_type==4){
		$payment_type = 'PAGADO (OTRO)';
	}else{
		$payment_type = 'PAGADO';
	}
	
	$puntos_luca = '';
	if($cust_key == ''){
		$customer_name = 'No Registrado'; 	
	}else{
        $sql = "SELECT * FROM customers WHERE bus_email = '$bus_email' and cust_key = '$cust_key'";
        $results = mysqli_query($db, $sql);
        $count   = mysqli_num_rows($results);
        if($count == 1){
            $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
            $num_points     = $row['cust_points'];
            $customer_name  = $row['cust_name'];
            $cust_email     = $row['cust_email'];
            $cust_gender    = $row['cust_gender'];
            $cust_id        = $row['cust_id'];
            $cust_type_id   = $row['cust_type_id'];
        }else{
            $num_points = 0;
        }
		$puntos_luca = '<div width="100%" style="text-align: center;">
	        	<center><img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&choe=ISO-8859-1&chl=www.2luca.co/php/customerview.php?ckey='.$cust_id.'" alt="QRCODE" width="60" height="60" border="0" /></center>
	        	<h6 align="center" style="font-size: 10px;">!HOLA <span style="text-transform: uppercase;">'.$customer_name.'</span>! TIENES <span>'.$num_points.'</span> PUNTOS LUCA ACUMULADOS AL '.$todays_date2.'</h6>
	        </div>';
	}
	
	$sql 	= "SELECT *, DATE_FORMAT(date_resolution,'%Y-%m-%d') AS date_reso_form FROM resolutions WHERE current_resolution = '1' AND bus_email = '$bus_email'";
	$resolu = mysqli_query($db, $sql);
    while ($row = mysqli_fetch_array($resolu,MYSQLI_ASSOC)) {
	    $ss_reso_num   = $row['resolution'];
	    $ss_reso_date  = $row['date_reso_form'];
	    $ss_reso_range = $row['range_reso_ini'] . "-" . $row['range_reso_last'];
    }

	
	if($discount == ''){$discount=0;}
	if($tips == ''){$tips=0;}
	if($taxes == ''){$taxes=0;}
	//if($payment_type == ''){$payment_type=0;}
	//if($creditcard_compr_number == ''){$creditcard_compr_number=0;}
	//if($cash_change == ''){$cash_change=0;}
	
	$trex = '';
	foreach($data as $d){
		$trex = $trex . ' <tr><td width="20%" align="center">'.$d['itemCount'].'</td><td width="45%" align="center">'.$d['itemName'].'</td><td width="35%" align="center">'.$d['itemAmountStyle'].'</td></tr>';
    }
	
	$width = '48';
	$height = '600';  /* HACER QUE LA ALTURA SEA DINAMICA!!!!!!!!!*/
	$pageLayout = array($width, $height);
	// create new PDF document
	$pdf = new TCPDF('P', 'mm', $pageLayout, true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Luca Colombia');
	$pdf->SetTitle('RECIBO_'.$id_bill);
	$pdf->SetSubject('Luca Colombia');
	
	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	// set margins
	$pdf->SetMargins('0', '5', '0');
	
	// set auto page breaks etAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM)
	$pdf->SetAutoPageBreak(FALSE);
	
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    $pdf->setLanguageArray($l);
	}
	
	// ---------------------------------------------------------
	
	// set font
	$pdf->SetFont('Helvetica', '', 5.5);
	
	// add a page
	$pdf->AddPage();
	
	// create some HTML content
	$html = '
	    <div style="overflow: hidden;">
	    
	        <div style="text-align:center; margin-bottom:20px;">  
	            <p style="font-size: 10px; text-transform: uppercase;"><b>'.$bus_name.'</b></p>
	            '.$bus_nit.$bus_address.$bus_phone.$bus_regimen.'
	        </div>
	        
	        <div style="text-align:left; margin-bottom:10px;"> 
	            <p> Factura de venta número: <span>'.$id_bill.'</span></p>
	            <p> Fecha de expedición: <span>'.$todays_date.'</span></p>
	            <p> Cliente: <span style="text-transform: uppercase;">'.$customer_name .'</span></p>
	            <p> Vendido por: <span style="text-transform: uppercase;">'.$profile_name.'</span></p>
	        </div>
	        
	        <table border="0" cellspacing="0" cellpadding="5" width="100%">
	        	<tr style="margin-bottom: 5px;">
	        		<th width="20%" align="center"><b>Cant.</b></th>
	        		<th width="45%" align="center"><b>Descripción.</b></th>
	        		<th width="35%" align="center"><b>Total</b></th>
	        	</tr>
	        	'.$trex.'
	        </table>
	        
	        <div style="width:100%; text-align: center; margin-bottom: 0px; margin-top: 2px;">
	            -----------------------------------------------------------------
	        </div>
	
			<div style="margin-bottom: 0px; margin-top: 2px;">
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
		        	<tr>
					    <td width="70%" align="right">Subtotal antes de impuestos</td>
					    <td width="30%" align="center">'.$totalbeforetaxes.'</td>
		        	</tr>
		        	<tr>
					    <td width="70%" align="right">Impuestos</td>
					    <td width="30%" align="center">'.$taxes.'</td>
		        	</tr>
		        	<tr>
					    <td width="70%" align="right">Subtotal</td>
					    <td width="30%" align="center">'.$subtotal_bill.'</td>
		        	</tr>
		        	<tr>
					    <td width="70%" align="right">Descuento</td>
					    <td width="30%" align="center">'.$discount.'</td>
		        	</tr>
		        	<tr>
					    <td width="70%" align="right">Propina</td>
					    <td width="30%" align="center">'.$tips.'</td>
		        	</tr>
		        	<tr>
					    <td width="70%" align="right"><b>TOTAL A PAGAR</b></td>
					    <td width="30%" align="center"><b>'.$trx_value.'</b></td>
		        	</tr>
		        </table>
	        </div>
	        
	        <div style="width:100%;">
		        <p style="width:100%; margin-bottom: 0px; margin-top: 1px; text-align: center;">
		            -----------------------------------------------------------------
		        </p>
	            <p align="center"> <b>INFORMACIÓN TRIBUTARIA</b> </p>
	            <p align="center"> Base gravable de $ '.$totalbeforetaxes.' para un valor total pagado de impuesto de $ '.$taxes.' </p>
	            <p style="width:100%; text-align: center;">
		            -----------------------------------------------------------------
		        </p>
		        
	            <p style="text-align:center;"> <b>PAGO CLIENTE</b> </p>
	            <table border="0" cellspacing="0" cellpadding="2" width="100%">
	            	<tr>
					    <td width="60%" align="right">TOTAL</td>
					    <td width="40%" align="center">'.$trx_value.'</td>
	            	</tr>
	            	<tr>
					    <td width="60%" align="right">'.$payment_type.'</td>
					    <td width="40%" align="center">'.$paidbyuser.'</td>
	            	</tr>
	            	<tr>
					    <td width="60%" align="right">CAMBIO</td>
					    <td width="40%" align="center">'.$cash_change.'</td>
	            	</tr>
	            </table>
	        </div>
	        
	        <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center;">
	            --------------- CUENTA CERRADA ---------------
	        </div>
	        
	        <div style="width:100%;">
	            <p style="text-align:left; margin:1px 0;">Resolución No. '.$ss_reso_num.'</p>
	            <p style="text-align:left; margin:1px 0;">Fecha Resolución '.$ss_reso_date.'</p>
	            <p style="text-align:left; margin:1px 0;">Numeración autorizada: '.$ss_reso_range.'</p>
	            <p style="text-align:left; margin:1px 0;">ICA Tarifa 9.66 X 1000</p>
	            <p style="text-align:left; margin:1px 0;">NO SOMOS AUTORETENEDORES</p>
	            <p style="text-align:left; margin:1px 0;">NO SOMOS GRANDES CONTRIBUYENTES</p>
	        </div>
	        
	        <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center;">
	            -----------------------------------------------------------------
	        </div>
	        
	        '.$puntos_luca.' 
	    </div>
	';
	
	// output the HTML content
	$pdf->writeHTML($html, true, 0, true, 0);
	
	$pdf->endPage();
	// reset pointer to the last page
	$pdf->lastPage();
	
	// ---------------------------------------------------------
	
	//Close and output PDF document
	$pdf->Output('recibo_'.$id_bill.'.pdf', 'I');
}
?>