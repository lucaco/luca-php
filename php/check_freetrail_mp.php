<?php
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email = $_SESSION['login_user'];
	$pkg_id 	 = mysqli_real_escape_string($db,$_POST['pkg_id']);

	$todays_date  = date('Y-m-d H:i:s');
	$days_free_trail = 7;

	$query = "SELECT CASE WHEN dt_str_test_".$pkg_id." = '1900-01-01 00:00:00' THEN 0 ELSE (CASE WHEN datediff('$todays_date',dt_str_test_".$pkg_id.") >= 0 AND datediff('$todays_date',dt_str_test_".$pkg_id.") <= $days_free_trail  THEN 1 ELSE 0 END) END AS pkg_freetrl_active, CASE WHEN dt_str_test_".$pkg_id." <> '1900-01-01 00:00:00' THEN $days_free_trail-datediff('$todays_date',dt_str_test_".$pkg_id.") END AS days_to_end, dt_str_test_".$pkg_id." AS date_str  FROM marketplace WHERE bus_email = '$bus_email';";
  	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	
  $r = Array();
	if($count > 0){
    array_push($r, Array('S','1'));
		while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
			array_push($r,$row);
		}
	}else{
		array_push($r, Array('E','1'));
	}
    
  echo json_encode($r);
}
?>