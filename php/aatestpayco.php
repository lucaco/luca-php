<?php

include __DIR__ . '/vendor/autoload.php';

use Epayco\Epayco;
use Epayco\Client;


$epayco = new Epayco(array(
  "apiKey"     => "393ea2815ad02ba17ef6955bc4e86292",
  "privateKey" => "d9dc6dfdc97a164f639270e27ce57735",
  "lenguage"   => "ES",
  "test"       => true
));

$sub = $epayco->subscriptions->getList();
$sub_data2      = json_encode($sub);
$sub_data_json  = json_decode($sub_data2,true);
print_r($sub_data2);

// ======= CREAR SOLO "LOCAL" TDC =======
// $token = $epayco->token->create(array(
//   "card[number]"    => '4575623182290326',
//   "card[exp_year]"  => "2017",
//   "card[exp_month]" => "07",
//   "card[cvc]"       => "123"
// ));
// $token2     = json_encode($token);
// $token_json = json_decode($token2,true);
// print_r($token2);

// ======= CREAR CLIENTE =======
// $customer = $epayco->customer->create(array(
//   //"token_card" => $token->id,
//   "name" => "Joe Doe",
//   "email" => "joe@payco.co",
//   "default" => true,
//   // Los siguientes parametros son opcionales (solo sirven para cuando se vaya a procesear el pago)
//   "city" => "Bogota",
//   "address" => "Cr 4 # 55 36",
//   "phone" => "3005234321",
//   "cell_phone"=> "3010000001"
// ));


// ======= OBTENER TODOS LOS PLANES DE SUBSCRIPCION =======
// $plan      = $epayco->plan->getList();
// $plan2     = json_encode($plan);
// $plan_json = json_decode($plan2,true);
// $plans     = $plan_json["data"];
// foreach ($plans as $key => $plan) {
//   echo $plan["name"];
//   echo $plan["_id"];
//   echo $plan["id_plan"];
//   echo $plan["description"];
//   echo $plan["currency"];
//   echo $plan["interval_count"];
//   echo $plan["interval"];
//   echo $plan["trialDays"];
//   echo $plan["status"];
// }


// ======= OBTENER INFORMACION DE UN CLIENTE EN ESPECIFICO =======
// $id_pay_cust = "RMAvwiMFv5muPnKMj";

// $customer  = $epayco->customer->get($id_pay_cust);
// $customer2 = json_encode($customer);
// $cust_json = json_decode($customer2,true);
// // echo $cust_json["data"]["name"];
// print_r($customer2);


// ======= CREAR SUBSRIPCION =======
// if($token_json["success"]==true){
//   if($cust_json["success"]==true){
//     $subp = $epayco->subscriptions->create(array(
//       "id_plan"    => "Analitica",
//       "customer"   => $id_pay_cust,
//       "token_card" => $token_json["id"],
//       "doc_type"   => "CC",
//       "doc_number" => "5234567"
//     ));
//     $subp2     = json_encode($subp);
//     $subp_json = json_decode($subp2,true);
//     echo $subp_json["success"];  // 1 si se creo bien 
//     echo $subp_json["message"]; // Subscripcion creada (Si se creo bien)
//     echo $subp_json["created"]; // Fecha de subscripcion
//     echo $subp_json["id"];      // Id de la subscripcion
//     echo $subp_json["current_period_end"];   // Fecha de Próximo Pago
//     echo $subp_json["current_period_start"]; // Fecha de Inicio del periodo
//     echo "---------";
//   }else{
//     echo "Subscribir - El ID de cliente no existe.";
//   }
// }else{
//   echo "Subscribir - El token de pago no existe.";
// }


// ======= VER DATOS DE SUBSCRIPCION ========
// $sub_data = $epayco->subscriptions->get("TE5YKgFDGuvguzAjB");
// $sub_data2      = json_encode($sub_data);
// $sub_data_json  = json_decode($sub_data2,true);
// print_r($sub_data2);
// echo $sub_data_json["success"];     // 1 si se creo bien 
// echo $sub_data_json["customer"];    // Cliente asociado a la sub
// echo $sub_data_json["created"];     // Fecha de subscripcion
// echo $sub_data_json["id"];          // Id de la subscripcion
// echo $sub_data_json["current_period_end"];   // Fecha de Próximo Pago
// echo $sub_data_json["current_period_start"]; // Fecha de Inicio del periodo


// ======= CANCELAR SUBSCRIPCION =======
// $sub_canc = $epayco->subscriptions->cancel("7EGJBNfmx9R72ucDd");
// $sub_canc2      = json_encode($sub_canc);
// $sub_canc_json  = json_decode($sub_canc2,true);
// print_r($sub_canc2);


// ======= GENERAR PAGO DE SUBSCRIPCION =======
// if($token_json["success"]==true){
//   if($cust_json["success"]==true){
//     if($sub_data_json["success"] == 1){
//       $sub = $epayco->subscriptions->charge(array(
//         "id_plan"    => "Analitica",
//         "customer"   => $id_pay_cust,
//         "token_card" => $token_json["id"],
//         "doc_type"   => "CC",
//         "doc_number" => "5234567",
//         "address"    => "cr 44 55 66",
//         "phone"      => "2550102",
//         "cell_phone" => "3010000001",
//       ));
//       $sub2     = json_encode($sub);
//       $sub_json = json_decode($sub2,true);
//       if($sub_json["status"]==true){
//         echo "Transacción realizada con éxito";
//       }else{
//         echo $sub_json["data"]["description"];
//       }
//     }else{
//       echo "La subscripción al módulo no existe.";
//     }
//   }else{
//     echo "El ID de cliente no existe.";
//   }
// }else{
//   echo "El token de pago no existe.";
// }

?>