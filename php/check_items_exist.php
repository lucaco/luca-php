<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
    $bus_email    = $_SESSION['login_user'];
 	$JsonItems    = mysqli_real_escape_string($db,$_POST['JsonItems']);
    $data         = json_decode(stripslashes($JsonItems),true);
    $todays_date  = date('Y-m-d H:i:s');

    $c = 0;
    $totItems = count($data);
    
    $SqlQuery = "SELECT prod_id, prod_name FROM products WHERE bus_email = '$bus_email' AND  prod_id IN (";
    foreach($data as $item){
        $c = $c + 1;
        $prod_code = $item;
        if($c == $totItems){
            $SqlQuery = $SqlQuery . "'$prod_code')";       
        }else{
             $SqlQuery = $SqlQuery . "'$prod_code',";                 
        }
    }
    $result = mysqli_query($db,$SqlQuery);
    $count  = mysqli_num_rows($result);
    
	$return_arr = Array();
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		array_push($return_arr,$row);
	}
	
	echo json_encode($return_arr);

 } 

?>