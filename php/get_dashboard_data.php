<?php

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){

  $bus_email = $_SESSION['login_user'];
  
  $query   = "SELECT t.trx_date, t.id_bill, t.trx_value, t.payment_type, t.points_redeem, t.num_items, t.cust_key, IFNULL(i.prod_name,'') AS prod_name, b.item_count, b.item_value,  IFNULL(c.cust_email,'') AS cust_email, IFNULL(c.cust_bdate,'') AS cust_bdate, IFNULL(c.cust_gender,'') AS cust_gender, IFNULL(d.prof_name,'') AS prof_name, IFNULL(d.avatar,'') AS prof_avatar, IFNULL(d.admin,'') AS prof_admin FROM transactions t LEFT JOIN bills b ON b.id_bill = t.id_bill AND b.bus_email = t.bus_email LEFT JOIN products i ON b.item_id = i.prod_id AND b.bus_email = i.bus_email LEFT JOIN customers c ON t.cust_key = c.cust_key LEFT JOIN profiles d ON t.profsessid = d.profsessid AND t.bus_email = d.bus_email WHERE t.bus_email = '". $bus_email ."' AND t.state = 1 ORDER BY t.trx_date;";
  $results = mysqli_query($db, $query);
  $ret = array();
  while($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
      array_push($ret, $row);
  }
  echo json_encode(array_values($ret));
}

?>