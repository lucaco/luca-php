<?php
/* 
 * FILE: check_customer_exist.php
 * WHAT FOR: Check if customer by phone or email exist
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 	= $_SESSION['login_user'];
	$cust_key 	= mysqli_real_escape_string($db,$_POST['cust_key']);
    
    $d = array();
    
    if($cust_key == ''){
        // No hay cliente ingresado por lo que es posible seguir
        $d[0] = array('r' => "1");
        echo json_encode($d);
    }else{
        $query = "SELECT * FROM customers WHERE bus_email = '$bus_email' AND cust_key = '$cust_key'";
        $result = mysqli_query($db,$query);
        $count  = mysqli_num_rows($result);
        
        if($count > 0){
            $d[0] = array('r' => "1");
            $return_arr = Array();
            while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                array_push($return_arr,$row);
            }
            $d[1] = $return_arr;
            echo json_encode($d);
        }else{
            $d[0] = array('r' => "0");
            echo json_encode($d);
        }  
    }
	
}
?>