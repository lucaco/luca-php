<?php 
include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);

$return_arr = Array();
session_start();

if($_POST){
    $email = $_SESSION['login_user'];
    
    $sql = "SELECT bus_act_econ FROM register_users WHERE bus_email = '$email';";
    $results = mysqli_query($db, $sql);
    $count   = mysqli_num_rows($results);

    if($count == 1){

        $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
        $bus_regimen = $row['bus_act_econ'];

        $return_arr2 = Array();
        $ss_reso_num  = "";
        $query_v = "SELECT * FROM resolutions WHERE bus_email = '$email' AND current_resolution = 1";
        $result = mysqli_query($db,$query_v);
        while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
            array_push($return_arr2,$row);
            $ss_reso_num   = $row['resolution'];
        }
        
        if(($bus_regimen=='2') || ($bus_regimen=='1' && $ss_reso_num != "")){

            if($ss_reso_num != ""){
                // CHECK RESOLUTION COUNTER
                $res_counter = '';
                $sql_max_res = "SELECT MAX(resolution_cnt) AS MAX_RESOL_NUM, COUNT(resolution_cnt) AS CNT_RESOL_NUM FROM transactions WHERE bus_email = '$email' AND resolution = '$ss_reso_num';";
                $result_max_res = mysqli_query($db, $sql_max_res);
                while ($row = mysqli_fetch_array($result_max_res,MYSQLI_ASSOC)) {
                    $res_counter  = $row['MAX_RESOL_NUM'];
                }

                if($res_counter==''){
                    array_push($return_arr,Array('S',$return_arr2,'',$bus_regimen));
                }else{
                    array_push($return_arr,Array('S',$return_arr2,$res_counter,$bus_regimen));
                }
            }            
        }else{
            array_push($return_arr,Array('F','1'));
        }
        
    }else{
        array_push($return_arr,Array('E','1'));
    }

    echo json_encode($return_arr);

 } 

?>