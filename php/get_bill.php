<?php
include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$return_arr = Array();

	$bus_email = $_SESSION['login_user'];
    $idBill   = mysqli_real_escape_string($db,$_POST['idBill']);

    $query = "SELECT A.*, IFNULL(B.prod_name,'Ítem') AS prod_name, B.prod_unit_price, IFNULL(B.prod_units,'Unidades') AS prod_units, IFNULL(B.prod_icon,'../icons/SVG/78-video-games/mario-question-box.svg') AS prod_icon FROM bills A LEFT JOIN products B ON A.item_id = B.prod_id AND A.bus_email = B.bus_email WHERE A.bus_email = '$bus_email' AND A.id_bill = '$idBill'";
    $result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
		array_push($return_arr,$row);
	}
	echo json_encode($return_arr);
}

//session_write_close();

?>