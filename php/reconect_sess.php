
<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$hash_act 	 = trim(mysqli_real_escape_string($db,$_POST['CURRACCKEY']));

	$query = "SELECT * FROM register_users WHERE hash_act = '$hash_act'";
	$result = mysqli_query($db,$query);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

    $count = mysqli_num_rows($result);

	if($count == 1) {
        
        $bus_name          	= $row['bus_name'];
        $pass_enc          	= $row['pass_enc'];
        $bus_industry      	= $row['bus_industry'];
        $creation_date     	= $row['creation_date'];
        $active            	= $row['active'];			// 1: User Active - 0: User Inactive
        $user_gone   		= $row['user_gone'];		// 1: User Not unregisted him/her self  - 0: User still Active
        $bus_address       	= $row['bus_address'];
        $bus_phone         	= $row['bus_phone'];
        $bus_nit           	= $row['bus_nit'];
        $bus_act_econ      	= $row['bus_act_econ'];
        $user_folder_name  	= $row['user_folder_name'];
        $avatar_type 		= $row['avatar_type'];		// 0: Imagen - 1: Logo de Galeria
        $bus_country 		= $row['bus_country'];
        $bus_city 			= $row['bus_city'];
        $hash_act 			= $row['hash_act'];
        $bus_email          = $row['bus_email']; 

        session_start();
        $_SESSION['login_user'] 			= $bus_email;
        $_SESSION['login_bus_name'] 		= $bus_name;
        $_SESSION['login_bus_industry'] 	= $bus_industry;
        $_SESSION['login_creation_date'] 	= $creation_date;
        $_SESSION['login_active'] 			= $active;
        $_SESSION['bus_address']	 		= $bus_address;
        $_SESSION['bus_phone'] 				= $bus_phone;
        $_SESSION['bus_nit'] 				= $bus_nit;
        $_SESSION['bus_act_econ'] 			= $bus_act_econ;
        $_SESSION['user_folder_name'] 		= $user_folder_name;
        $_SESSION['avatar_type'] 			= $avatar_type; 
        $_SESSION['bus_country'] 			= $bus_country;
        $_SESSION['bus_city'] 				= $bus_city;
        $_SESSION['hash_act'] 				= $hash_act;
        
        echo '1';
    }else{
        echo '0';
    }
}


?>