<?php

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){

	$return_arr = Array();

	$bus_email = $_SESSION['login_user'];
	$cust_key  = mysqli_real_escape_string($db,$_POST["cust_key"]);

    $v_1 = 0;
    $v_2 = 0;
    $v_3 = 0;

    // Segmento y NBA
    $query = "SELECT A.cust_key, B.* FROM nba_segment A LEFT JOIN a_segment B ON A.segment_code = B.segment_code WHERE A.bus_email = '$bus_email' AND cust_key = '$cust_key';";
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
    if($count == 0){
        array_push($return_arr,$row);
    }else{
        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
            array_push($return_arr,$row);
        }
    }

    // Estadisticas descriptivas
    $query2 = "SELECT COUNT(trx_id) AS ventas, SUM(tips) AS tips, SUM(trx_value) AS ingresos, DATEDIFF(LOCALTIME,MAX(trx_date)) AS days_last_prsh, SUM(CASE WHEN payment_type = 1 THEN 1 ELSE 0 END) AS cnt_typy_1, SUM(CASE WHEN payment_type = 2 THEN 1 ELSE 0 END) AS cnt_typy_2, SUM(CASE WHEN payment_type = 3 THEN 1 ELSE 0 END) AS cnt_typy_3, SUM(CASE WHEN payment_type = 4 THEN 1 ELSE 0 END) AS cnt_typy_4 FROM transactions WHERE bus_email='$bus_email' AND cust_key = '$cust_key' AND state = '1' GROUP BY bus_email;";
    $result2 = mysqli_query($db,$query2);
	$count2  = mysqli_num_rows($result2);
    if($count2 == 0){
        array_push($return_arr,$row);
    }else{
        while($row2 = mysqli_fetch_array($result2,MYSQLI_ASSOC)){
            array_push($return_arr,$row2);
        }
    }

    // Promedios del total de consumidores
    $query3 = "SELECT A.*, A.NUM_TRX_CONS/NUM_CUST AS N_VIST_AVG_CUST, A.TOT_TIPS/A.NUM_TRX AS TIPS_AVG, A.TOT_TRX_VAL_CUST/A.NUM_CUST AS INC_AVG_CUST  FROM (SELECT COUNT(trx_id) AS NUM_TRX, SUM(CASE WHEN cust_key <> '' THEN 1 ELSE 0 END) AS NUM_TRX_CONS, SUM(trx_value) AS TOT_TRX_VAL, SUM(CASE WHEN cust_key <> '' THEN trx_value ELSE 0 END) AS TOT_TRX_VAL_CUST, SUM(tips) AS TOT_TIPS, COUNT(DISTINCT cust_key) AS NUM_CUST FROM transactions WHERE bus_email = '$bus_email' AND state = '1') A;";
    $result3 = mysqli_query($db,$query3);
	$count3  = mysqli_num_rows($result3);
    if($count3 == 0){
        array_push($return_arr,$row);
    }else{
        while($row3 = mysqli_fetch_array($result3,MYSQLI_ASSOC)){
            array_push($return_arr,$row3);
        }
    }

	echo json_encode($return_arr);
}

?>