<?php

/******* NO ESTÁ 100% REVISADO  ********/

include("functions.php");
include("config.php");
include("config_epayco.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){

  $bus_email 		 = $_SESSION['login_user'];
	$card_numer    = trim(mysqli_real_escape_string($db,$_POST['card_numer']));   //'4575623182290326';
  $card_year 	   = trim(mysqli_real_escape_string($db,$_POST['card_year']));    //'2017'; 
  $card_month    = trim(mysqli_real_escape_string($db,$_POST['card_month']));   //'07';  
  $card_cvc      = trim(mysqli_real_escape_string($db,$_POST['card_cvc']));     //'123';  
  $cust_name     = trim(mysqli_real_escape_string($db,$_POST['cust_name']));    //'Joe Doe'; 
  $cust_email    = trim(mysqli_real_escape_string($db,$_POST['cust_email']));   //'joe@payco.co'; 
  $cust_city     = trim(mysqli_real_escape_string($db,$_POST['cust_city']));    //'Bogota'; 
  $cust_address  = trim(mysqli_real_escape_string($db,$_POST['cust_address'])); //'Cr 4 # 55 36'; 
  $cust_phone    = trim(mysqli_real_escape_string($db,$_POST['cust_phone']));   //'3005234321'; 

  $today_date  = date('Y-m-d H:i:s');

  // ======= CREAR TDC =======
  $token = $epayco->token->create(array(
    "card[number]"    => $card_numer,
    "card[exp_year]"  => $card_year,
    "card[exp_month]" => $card_month,
    "card[cvc]"       => $card_cvc
  ));
  $token2     = json_encode($token);
  $token_json = json_decode($token2,true);
  // print_r($token2);

  $r = Array();
  if($token_json["status"]==true){

    // ======= CREAR CLIENTE =======
    $customer = $epayco->customer->create(array(
      "token_card" => $token->id,
      "name" => $cust_name,
      "email" => $cust_email,
      "default" => true,
      // Los siguientes parametros son opcionales (solo sirven para cuando se vaya a procesear el pago)
      "city" => $cust_city,
      "address" => $cust_address,
      "phone" => $cust_phone 
    ));

    $customer2 = json_encode($customer);
    $cust_json = json_decode($customer2,true);

    if($cust_json["status"]==true){
      if($cust_json["data"]["customerId"]){
        $query = "UPDATE register_users SET id_cust_pay = '".$cust_json["data"]["customerId"]."' WHERE bus_email = '".$bus_email."';";
        mysqli_query($db,$query);
        if(mysqli_affected_rows($db)<=0){
          array_push($r,Array('E','El cliente fue creado pero no fue posible ingresarlo al servidor.'));
        }else{
          array_push($r,Array('S','Cliente agregado correctamente.'));
        }
      }else{
        array_push($r,Array('E',"El Id del cliente no se pudo crear."));
      }
    }else{
      array_push($r,Array('E',$cust_json["message"]));
    }
  }else{
    array_push($r,Array('E',$token_json["message"]));
  }
  
  echo json_encode($r);
}
?>