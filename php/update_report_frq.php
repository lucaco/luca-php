<?php
/* 
 * FILE: update_vars_accnt.php
 * WHAT FOR: Update variables of user account
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email  = $_SESSION['login_user'];
    $frq        = trim(mysqli_real_escape_string($db,$_POST['frq']));
    $chng_type  = trim(mysqli_real_escape_string($db,$_POST['chng_type']));
    
    $r = Array();
    $query = "UPDATE register_users SET sum_mail_".$frq." = '$chng_type' WHERE bus_email = '$bus_email';";     
    mysqli_query($db,$query);

    if(mysqli_affected_rows($db)<=0){
        array_push($r, Array('E','1'));
    }else{
        array_push($r, Array('S','1'));
    }
    echo json_encode($r);

}
?>