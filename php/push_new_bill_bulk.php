<?php

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);
date_default_timezone_set('America/Bogota');

if($_POST){
  
	$points_system  = 700; // 1 point for each 700$ purshased

	$bus_email      = $_SESSION['login_user'];
	$trx_value      = mysqli_real_escape_string($db,$_POST["trx_value"]);
	$trx_type       = mysqli_real_escape_string($db,$_POST["trx_type"]);
	$num_items      = mysqli_real_escape_string($db,$_POST["num_items"]);
	$discount       = mysqli_real_escape_string($db,$_POST["discount"]);
	$tips           = mysqli_real_escape_string($db,$_POST["tips"]);
	$taxes          = mysqli_real_escape_string($db,$_POST["taxes"]);
	$id_cupon       = mysqli_real_escape_string($db,$_POST["id_cupon"]);
	$bill_type      = mysqli_real_escape_string($db,$_POST["bill_type"]);
	$payment_type   = mysqli_real_escape_string($db,$_POST["payment_type"]);
	$other_payment_type   = mysqli_real_escape_string($db,$_POST["other_payment_type"]);
	$creditcard_compr_number   = mysqli_real_escape_string($db,$_POST["creditcard_compr_number"]);
	$cash_change    = mysqli_real_escape_string($db,$_POST["cash_change"]);
	$cust_key       = mysqli_real_escape_string($db,$_POST["cust_key"]);
	$sqlInput       = mysqli_real_escape_string($db,$_POST["sqlInput"]);
	$data           = json_decode(stripslashes($sqlInput), true);
  $profsessid     = mysqli_real_escape_string($db,$_POST["profsessid"]);
  $points_redem   = mysqli_real_escape_string($db,$_POST["points_redem"]);

	$todays_date    = date('Y-m-d H:i:s');
  $id_bill        = generateRandomString(12); // ID DE LA CUENTA
  
	if($discount == ''){$discount=0;}
	if($tips == ''){$tips=0;}
	if($taxes == ''){$taxes=0;}
	if($bill_type == ''){$bill_type=0;}
	if($payment_type == ''){$payment_type=0;}
	if($creditcard_compr_number == ''){$creditcard_compr_number=0;}
	if($cash_change == ''){$cash_change=0;}
	
	$trx_id = time().generateNumber(3);
  $flag = "";

  // ADD IN TRANSACTIONS TABLE
  $sql = "INSERT INTO base_lucaco.transactions (bus_email, trx_id, trx_date, trx_value, trx_type, id_bill, num_items, discount, tips, taxes, id_cupon, bill_type, payment_type, other_payment_type, creditcard_compr_number, cash_change, profsessid, cust_key, points_redeem) VALUES ('$bus_email', '$trx_id', '$todays_date', '$trx_value', '$trx_type', '$id_bill', '$num_items', '$discount', '$tips', '$taxes', '', '$bill_type', '$payment_type', '$other_payment_type', '$creditcard_compr_number', '$cash_change', '$profsessid', '$cust_key','$points_redem');";
  mysqli_query($db, $sql);
  
  $sql = "SELECT trx_date FROM base_lucaco.transactions WHERE bus_email = '$bus_email' AND id_bill = '$id_bill'";
  $result = mysqli_query($db, $sql);
  $count  = mysqli_num_rows($result);

  // ADD IN BILLS TABLE
  $bad = 0;
    if(sizeof($data)>0 && $count>0){
      $inputValues = "";
      $num_items = 0;
      foreach($data as $d){
          $num_items = $num_items + 1;
          $itemDiscount = $d['itemDiscount'];
          if($itemDiscount == ''){$itemDiscount = 0;}
          if($num_items < sizeof($data)){
            $inputValues = $inputValues . "('".$bus_email."', '".$id_bill."', '".$d['itemId']."', '".$d['itemAmount']."', '".$d['itemCount']."', '".$itemDiscount."', '".$d['itemCoupon']."', '".$d['itemNote']."'),"; 
          }else{
              $inputValues = $inputValues . "('".$bus_email."', '".$id_bill."', '".$d['itemId']."', '".$d['itemAmount']."', '".$d['itemCount']."', '".$itemDiscount."', '".$d['itemCoupon']."', '".$d['itemNote']."')"; 
          }   
        }
      $sql = "INSERT INTO bills (bus_email, id_bill, item_id, item_value, item_count, item_discount, item_coupon, item_note) VALUES ";
      $sql = $sql.$inputValues;   
      mysqli_query($db,$sql);
      if(mysqli_affected_rows($db)<=0){
        $bad = 1;
          $flag = "1"; // No se pudo insertar el registro
      }
    }else{
      $bad = 1;
        $flag = "2"; // No se pudo registrar la transacción
    }

    // REDEM POINTS
    if($points_redem>0){
        $sql0 = "UPDATE customers SET cust_points = cust_points - ".$points_redem." WHERE bus_email = '".$bus_email."' AND cust_key = '".$cust_key."' AND cust_points>='".$points_redem."'";
      mysqli_query($db,$sql0);
      if(mysqli_affected_rows($db)<=0){
            $bad = 1;
            $flag = "3"; // No se pudo redimir los puntos
        }
    }

    // SUM POINTS TO USER 
    if($cust_key != '' && $count>0){
      $points = floor($trx_value/$points_system);
        $sql2 = "UPDATE customers SET cust_points = cust_points + ".$points." WHERE bus_email = '".$bus_email."' AND cust_key = '".$cust_key."'";
        mysqli_query($db,$sql2);
        if(mysqli_affected_rows($db)<=0){
            $bad = 1;
            $flag = "4"; // No se pudo acumular los puntos luca
        }
    }

  if($count == 1 && $bad == 0){
    echo "0";
  }else{
    echo $flag ;
  }



}


?>