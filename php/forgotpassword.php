<?php 
	include("config.php");
	include("functions.php");
  	date_default_timezone_set($TimeZone);
	session_start();

	if (isset($_SESSION['login_user'])){header("location:sales.php");}
	
	$clase_1 = "display-block";
	$clase_2 = "hide";


	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['reg-submit'])) {

		$reg_email = mysqli_real_escape_string($db,$_POST['reg-email']);

		$rnd_1 = generateRandomString(7);
		$rnd_1_hash = password_hash($rnd_1, PASSWORD_DEFAULT);

		// Revisar que el usuario exista ya
		$query = "SELECT * FROM register_users WHERE bus_email = '$reg_email'";
		$result = mysqli_query($db,$query);
		$count = mysqli_num_rows($result);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$bus_name = $row['bus_name'];
		$active = $row['active'];
		$hash_sec = $row['hash_act'];

		if($count == 1) {

			$query = "UPDATE register_users SET pass_enc = '". $rnd_1_hash ."' WHERE bus_email = '". $reg_email ."'";
			mysqli_query($db,$query);

			$msg_active = "";

			if($active == "0"){
				$msg_active = "<br><br> <b>¡Atención!:</b> Parece que no has activado tu cuenta aun y no podrás ingresar. Recuerda que para activar tu cuenta debes seguir el link de confirmación que fue enviado en el registro de tu cuenta. Si tuviste algún problema o no recibiste el correo por favor ingresa al siguente link: <a href='https://www.2luca.co/validacion_user.php?email=". $reg_email ."&hash=". $hash_sec ."' style='text-decoration: none; color:#39ac95;'>activar cuenta</a>.";
			}

			// Enviar correo de confirmación
			if (SendMail_SMTP_SimpleMsg($reg_email,
				   				"Solicitud de Recuperación de Contraseña",
				   				$bus_name,
				   				"Recientemente solicitaste recuperar tu clave a través de nuestra página. Para esto hemos generado la siguiente contraseña temporal: <br><br> Clave temporal: <b>". $rnd_1 ."</b> <br><br> Por favor <a href='https://www.2luca.co' style='text-decoration: none; color:#39ac95;'>ingresa aquí</a> con la nueva contraseña a tu cuenta. Te recomendamos cambiarla por una de tu preferencia <b>inmediatamente</b> en la sección de configuración de tu cuenta. ". $msg_active ."  <br><br> Gracias, <br><br> Servicio Técnico<br>2Luca - Colombia<br>Bogotá, Colombia <br>"))
			{

				// Clear variables 
				$clase_1 = "hide";
				$clase_2 = "display-block";

			}else{
				phpAlert('Upps, no fue posible enviar el correo de confirmación. Por favor intenta de nuevo.');
			}
		}else{
			phpAlert("Upps, parece que el correo electrónico no está registrado.");
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title> Luca.co | Recuperar Contraseña </title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Galada|Quicksand" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css">

	<style type="text/css">

        .bd_gray{
            background: #efefef !important;  
        }

		.main_container{
			border-radius: 5px;
			width: 95%;
			max-width: 550px;
			max-height: 95%;
			background-color: #fff;
			box-shadow: 3px 3px 6px 5px rgba(0,0,0,0.1);
			border: none;
			overflow: auto;
			position: fixed;
			left: 50%; 
			top: 50%; 
			transform: translate(-50%,-50%);
			display: block;
			z-index: 1;
			padding: 40px 35px;
			box-sizing: border-box;
		}

		.form_class_normal{
			display: block;
			width: 100%;
			padding: 10px 0px;
			/*border:1px solid #000;*/
			text-align: center;
			box-sizing: border-box;
		}

		.form_class_normal>input[type=email]{
			font-size: 15px;
			width: 100%;
			max-width: 400px;
			height: 55px;
			margin-bottom: 10px;
			padding: 0 15px;
			box-sizing : border-box; 
			border: 2px solid #282828;
			border-radius: 5px;
			color: #666666;
			-webkit-appearance: none;
		}

		.form_class_normal>input[type=email]:focus{
			outline: none;
			border: 2px solid #7febd0;
		}

		.form_class_normal>input[type=submit]{
			width: 250px;
			height: 55px;
			max-width: 350px;
			color: #fff;
			font-size: 17px;
			border:none;
			border-radius: 7px;
			background-color: var(--greenluca);
			letter-spacing: 2px;
			cursor: pointer;
			-webkit-appearance: none;
			padding: 13px 10px;
			margin-top: 25px;
            outline: none;
		}
		.form_class_normal>input[type=submit]:hover{
			background-color: var(--greenluca_hover_soft);
		}
		.form_class_normal>a{
			display: block;
			margin-top: 7px;
			font-size: 13px;
			color: #999;
		}
		.form_class_normal>a:hover{
			color: #333;
		}

		.title-1{
			display: table;
			margin-bottom: 10px;
		}
		.title-1>p{
			font-size: 20px;
			color: #191919;
			font-weight: bold;
			display: table-cell;
			vertical-align: middle;
			/*background-color: blue;*/
			
		}
		.title-1>img{
			width: 60px;
			margin-right: 10px;
			display: table-cell;
			vertical-align: top;
			/*background-color: red;*/
		}

		.title-2{
			font-size: 15px;
			color: #666666;
			margin-bottom: 30px;
			font-weight: lighter;
		}


		/*LOGO CONTAINER*/
		.logo-in-container{
			position: fixed;
			top: 0;
			left: 0;
			margin-top: 10px;
			margin-left: 10px;
		}
		.logo-in-container>img{
			width: 90px; 
		}
        .ab1{
            color: #00bb7d !important;
            cursor: pointer;
        }

	</style>

</head>
<body class="bd_gray">

	<div class="logo-in-container hide">
		<img src="../logos/logo5.png">
	</div>
	

	<div id="pass_recovery_1" class="main_container <?php echo $clase_1; ?>">
		
		<div class="title-1"><img src="../images/login-lock.png"><p class="text-uppercase"> Recuperar tu contraseña </p></div> 
		<p class="title-2"> No te preocupes, vamos a recuperar tu contraseña. Ingresa tu correo electrónico y te enviaremos un email con información para recuperar tu clave.</p>

		<form class="form_class_normal" action="" method="POST"> 
			<input type="email" name="reg-email" id="reg-email"  placeholder="Email" required>
			<input type="submit" name="reg-submit" id="reg-submit" class="btn_style_2" value="RECUPERAR">
			<a href="../index.php">REGRESAR</a>
		</form>
	</div>

	<div id="pass_recovery_2" class="main_container <?php echo $clase_2; ?>">
		<div class="title-1"><img src="../images/email-check-2.png"><p class="text-uppercase"> ¡Listo! </p></div>
		<p class="title-2"> Revisa la bandeja de entrada de tu correo electrónico, sigue las instrucciones para recuperar tu contraseña y vuelve a ingresar tus credenciales en nuestra página.</p>
		<a class="ab1" href="../index.php">Volver ></a>
	</div>

</body>
</html>