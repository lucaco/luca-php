<?php 

	include("config.php");
	include("functions.php");
	session_start();
	date_default_timezone_set($TimeZone);
	
	$title_1  = "Upps, parece que ocurrió un error";
	$main_msg = "Contáctate con el servicio de soporte para poder ayudarte";

	if (isset($_SESSION['login_user'])){header("location:dashboard.php");}

	if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['email']) && isset($_GET['hash']) ) {


		$email = mysqli_real_escape_string($db,$_GET['email']);
		$hash = mysqli_real_escape_string($db,$_GET['hash']);

		// Revisar que el usuario se creo correctamente
		$query = "SELECT bus_name, hash_act, user_gone, active FROM register_users WHERE bus_email = '$email'";
		$result = mysqli_query($db,$query);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$hash_act = $row['hash_act'];
		$active = $row['active'];
		$bus_name = $row['bus_name'];
		$user_gone = $row['user_gone'];


		$count = mysqli_num_rows($result);

		if($count == 1) {
			if($hash_act == $hash){

				if($active == 0){

					$title_1  = "¡Todo Listo ". $bus_name ."!";
					$main_msg = "Hemos enviado de nuevo el link de activación a tu correo electrónico. Revisa tu bandeja de entrada y tu <b>carpeta de correos no desados</b>. Si no te llega el mensaje no dudes en contactarte con nosotros. ";

					SendMail_SMTP_SimpleMsg($email,
					"🔗 Link de Activación de Cuenta",
					$bus_name,
					"Recientemente solicitaste el envío del link para la activación de tu cuenta en 2Luca. <br></br>Aquí va el enlace para <b>activar tu cuenta</b>:<br></br> <span style='color:#39ac95;text-decoration:none;'> https://www.2luca.co/php/validacion_user.php?email=". $email ."&hash=". $hash ."</span> <br></br> Sí no has sido tu quien solicitó este link, te invitamos a contactarte con el <b>servicio de soporte técnico.</b> <br></br> Gracias, <br></br> Soporte Técnico<br>2Luca<br>Bogotá, Colombia <br> ");

				}else{
					$title_1  = "El usuario ya está activado";
					$main_msg = "<a href='../index.php' style='color: #39ac95; text-decoration: none;'> Ingresar </a> ";
				}

			}
		}

	}



?>


<!DOCTYPE html>
<html>
<head>
	<title>Luca.co</title>

	<style>
		
		*{
			padding: 0;
			margin: 0;
		}
		body{
			background: #efefef;
			font-family: 'Quicksand', sans-serif;
		}

		.main_container{
			border-radius: 5px;
			width: 80%;
			height: auto;
			max-width: 750px;
			background-color: #fff;
			box-shadow: 3px 3px 6px 5px rgba(0,0,0,0.1);
			border: none;
			overflow: auto;
			position: absolute;
			left: 50%; 
			top: 50%; 
			transform: translate(-50%,-50%);
			display: block;
			border-collapse: collapse;
			z-index: 1;
			overflow: hidden;
		}


		.reg_logo_cont{
			display: table-cell;
			vertical-align: top;
			width: 25%;
			text-align: center;
			padding: 40px 10px;
			/*border:1px solid #000;*/
		}
		.reg_logo_cont>a>img{
			width: 150px;
			padding-left: 40px;
			padding-right: 30px;
			cursor: pointer;
		}

		.reg_logo_cont>a{
			text-transform: none;
		}

		.reg_form_cont{
			display: table-cell;
			vertical-align: top;
			width: 75%;
			padding: 40px 35px 40px 20px;
			/*border:1px solid #000;*/
		}
		.reg_form_cont>h1{
			font-size: 27px;
			color: #191919;
			margin-bottom: 20px;
		}
		.reg_form_cont>h2{
			font-size: 15px;
			color: #666666;
			font-weight: normal;
			margin-bottom: 30px;
		}
		.reg_form_cont>h2>span{
			color: #39ac95;
			cursor: pointer;
		}

	</style>
</head>
<body>


	<div id="container_step_0" class="main_container">
		
		<div class="reg_logo_cont" id="logo_image">
			<a href="../index.php"><img src="../logos/logo9.png"></a>
		</div>

		<div class="reg_form_cont">
			<h1><?php echo $title_1 ?></h1>
			<h2><?php echo $main_msg ?></h2>
		</div>

	</div>

</body>
</html>