<?php 

	include("config.php");
	include("functions.php");
	session_start();
	date_default_timezone_set($TimeZone);
	
	$title_1  = "Upps, parece que ocurrió un error";
	$main_msg = "Contáctate con el servicio de soporte para poder ayudarte";

	if (isset($_SESSION['login_user'])){header("location:sales.php");}

	if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['email']) && isset($_GET['hash']) ) {

		$email = mysqli_real_escape_string($db,$_GET['email']);
		$hash = mysqli_real_escape_string($db,$_GET['hash']);

		// Revisar que el usuario se creo correctamente
		$query = "SELECT bus_name, hash_act, user_gone FROM register_users WHERE bus_email = '$email'";
		$result = mysqli_query($db,$query);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$hash_act = $row['hash_act'];
		$bus_name = $row['bus_name'];
		$user_gone = $row['user_gone'];


		$count = mysqli_num_rows($result);

		if($count == 1) {
			if($hash_act == $hash){

				if($user_gone == 1){

					recreateUser($email);

					$query = "UPDATE register_users SET creation_date = '".$IP_data_date."', last_mod_date = '".$IP_data_date."'  WHERE bus_email = '". $email ."'";
					mysqli_query($db,$query);

					$title_1  = "¡Bienvenido de nuevo ". $bus_name ."!";
					$main_msg = "Que bueno que estes de vuelta. Solo <a href='https://www.2luca.co/index.php' style='color: #39ac95; text-decoration: none;'> ingresa de nuevo </a> con tu usuario y contraseña de siempre. <br><br> Si la olvidaste puedes dar click <a href='https://www.2luca.co/php/forgotpassword.php' style='color: #39ac95; text-decoration: none;'> aquí </a> para recuperarla.";

					SendMail_SMTP_SimpleMsg($email,
					"¡Bienvenido de nuevo!",
					$bus_name,
					"Siempre es bueno un aire nuevo. <br></br> Qué bueno tenerte de nuevo <b>".$bus_name."</b>!. Ya hemos reactivado tu sesión tal cual la dejaste la última vez. Solo <a href='https://www.2luca.co/index.php' style='color: #39ac95; text-decoration: none;'> ingresa de nuevo </a> con tu usuario y contraseña de siempre. <br><br> Si la olvidaste puedes dar click <a href='https://www.2luca.co/php/forgotpassword.php' style='color: #39ac95; text-decoration: none;'> aquí </a> para recuperarla. <br></br> En 2Luca siempre estamos mejorando para tu bienestar, esperamos que esta nueva sesión con nosotros sea de tu total agrado. <br></br> Gracias <br></br>  2Luca - Colombia<br>Bogotá, Colombia <br> ");

				}else{
					$title_1  = "El usuario ya existe";
					$main_msg = "<a href='../index.php' style='color: #39ac95; text-decoration: none;'> Ingresar </a> ";
				}

			}
		}

	}



?>


<!DOCTYPE html>
<html>
<head>
	<title>Luca | Bienvenido de Nuevo</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	
	<style>
		
		*{
			padding: 0;
			margin: 0;
		}
		body{
			background: #efefef;
			font-family: 'Quicksand', sans-serif;
		}

		.main_container{
			border-radius: 5px;
			width: 80%;
			height: auto;
			max-width: 750px;
			background-color: #fff;
			box-shadow: 3px 3px 6px 5px rgba(0,0,0,0.1);
			border: none;
			overflow: auto;
			position: absolute;
			left: 50%; 
			top: 50%; 
			transform: translate(-50%,-50%);
			display: block;
			border-collapse: collapse;
			z-index: 1;
			overflow: hidden;
		}


		.reg_logo_cont{
			display: table-cell;
			vertical-align: top;
			width: 25%;
			text-align: center;
			padding: 40px 10px;
			/*border:1px solid #000;*/
		}
		.reg_logo_cont>a>img{
			width: 150px;
			padding-left: 40px;
			padding-right: 30px;
			cursor: pointer;
		}

		.reg_logo_cont>a{
			text-transform: none;
		}

		.reg_form_cont{
			display: table-cell;
			vertical-align: top;
			width: 75%;
			padding: 40px 35px 40px 20px;
			/*border:1px solid #000;*/
		}
		.reg_form_cont>h1{
			font-size: 27px;
			color: #191919;
			margin-bottom: 20px;
		}
		.reg_form_cont>h2{
			font-size: 15px;
			color: #666666;
			font-weight: normal;
			margin-bottom: 30px;
		}
		.reg_form_cont>h2>span{
			color: #39ac95;
			cursor: pointer;
		}

	</style>
</head>
<body>


	<div id="container_step_0" class="main_container">
		
		<div class="reg_logo_cont" id="logo_image">
			<a href="../index.php"><img src="../logos/logo9.png"></a>
		</div>

		<div class="reg_form_cont">
			<h1><?php echo $title_1 ?></h1>
			<h2><?php echo $main_msg ?></h2>
		</div>

	</div>

</body>
</html>