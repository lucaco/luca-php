<?php 

include("config.php");
include("functions.php"); 
date_default_timezone_set($TimeZone);

if($_POST) {

	$reg_business_name = ucwords(strtolower(mysqli_real_escape_string($db,$_POST['reg-business-name'])));
	$reg_email         = strtolower(mysqli_real_escape_string($db,$_POST['reg-email'])); 
	$reg_pass          = mysqli_real_escape_string($db,$_POST['reg-pass']); 
	$reg_pass_enc      = password_hash($reg_pass, PASSWORD_DEFAULT);
	$reg_pass2         = mysqli_real_escape_string($db,$_POST['reg-pass-2']); 
	$reg_industry      = mysqli_real_escape_string($db,$_POST['reg-industry']); 

	$rnd_1 = generateRandomString();
  	$bus_ses_id = generateRandomString(20);

	// Revisar que el usuario no exista ya
	$query = "SELECT bus_name, user_gone, active, hash_act FROM register_users WHERE bus_email = '$reg_email'";
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	$row    = mysqli_fetch_array($result,MYSQLI_ASSOC);

	$user_gone  = $row['user_gone'];
	$active     = $row['active'];
	$bus_name   = $row['bus_name'];
	$hash_act   = $row['hash_act'];

	if($count == 1) {

		if($user_gone == 1){
			// El usuario existe pero quiere volver (User Gone)
			$msg_error = "Parece que quieres regresar ". $bus_name .". Si es así, da click en este recuadro";
			$link_to_come="usercomeback.php?email=". $reg_email ."&hash=". $hash_act;
			
			$results = array('UG',$msg_error, $link_to_come);
			echo json_encode($results);

		}else if($active == 0){
			// El usuario existe pero no está activado (User Not Acrivated) .. Enviar Link
			//$link_to_activate="sendactlink.php?email=". $reg_email ."&hash=". $hash_act;
			
			SendMail_SMTP_SimpleMsg($reg_email,
					"🔗 Link de Activación de Cuenta",
					$bus_name,
					"Recientemente solicitaste el envío del link para la activación de tu cuenta en 2Luca. <br></br>Aquí va el enlace para <b>activar tu cuenta</b>:<br></br> <span style='color:#39ac95;text-decoration:none;'> https://www.2luca.co/php/validacion_user.php?email=". $reg_email ."&hash=". $hash_act ."</span> <br></br> Sí no has sido tu quien solicitó este link, te invitamos a contactarte con el <b>servicio de soporte técnico.</b> <br></br> Gracias, <br></br> Soporte Técnico<br>2Luca<br>Bogotá, Colombia <br> ");

			$results = array('UNA');
			echo json_encode($results);

		}else{
			// Solo existe (User Already Registered (UAR))
			$results = array('UAR');
			echo json_encode($results);
		}

	}else {
		
			// User private folder name
			$user_folder = splitEmail($reg_email);

			// Ingresar el usuario en la tabla
			$query = "INSERT INTO register_users (bus_email, bus_name, pass_enc, bus_industry, creation_date, last_mod_date, active, hash_act, user_folder_name, bus_ses_id) VALUES (TRIM('" . $reg_email . "'), TRIM('". $reg_business_name ."'), '". $reg_pass_enc ."', '". $reg_industry ."', '".$IP_data_date."', '".$IP_data_date."', 0, '". $rnd_1 ."', TRIM('".$user_folder."'), '$bus_ses_id');";
			mysqli_query($db,$query);
			
			// Revisar que el usuario se creó correctamente
			$query = "SELECT bus_name FROM register_users WHERE bus_email = '$reg_email'";
			$result = mysqli_query($db,$query);
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

			$count = mysqli_num_rows($result);

			if($count == 1) {

				// Agregar el cliente a la tabla de market place 
				$query = "INSERT INTO marketplace (bus_email) VALUES ('".$reg_email."');";
				mysqli_query($db,$query);

				// Enviar correo a cuenta de LUCA para registro 
				SendMail_SMTP_SimpleMsg("2lucaco@gmail.com",
					"¡Nuevo Cliente en Luca!",
					"Luca",
					"Tenemos un nuevo cliente!. <br> Nombre: ".$reg_business_name." <br> Email: $reg_email. ");

				// Enviar correo de confirmación
				if (SendMail_SMTP_SimpleMsg($reg_email,
					"¡Bienvenido a 2Luca!",
					$reg_business_name,
					"Gracias por escoger 2Luca, el mejor sistema de <b>analítica de ventas!</b> <br></br> Has tomado el primer paso para lograr tus metas y tener control sobre tu negocio. Para empezar debes <b>activar tu cuenta</b> ingresando al siguiente link:<br></br> <span style='color: #39ac95; text-decoration: none;'> https://www.2luca.co/php/validacion_user.php?email=". $reg_email ."&hash=". $rnd_1 ."</span> <br></br> Te invitamos a ingresar ahora mismo y diligenciar los campos faltantes para completar el registro. <br></br>Recuerda que estos campos como el NIT de tu empresa, dirección y teléfono te permitirán acceder a más opciones y así sacar el mayor provecho de la aplicación. <br></br><br></br> Gracias por preferirnos <br></br> 2Luca - Colombia<br>Bogotá, Colombia <br> "))
				{
					$results = array('OK');
					echo json_encode($results);
				}else{
					$results = array('ERR','No se pudo enviar el correo electrónico de confirmación.');
					echo json_encode($results);
				}


			}else {
				$results = array('ERR','Upps! Hubo un error registrando tu cuenta. Intenta de nuevo.');
				echo json_encode($results);
			}


	}

}

?>