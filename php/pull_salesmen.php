<?php
/* 
 * FILE: pull_salesmen.php
 * WHAT FOR: Pull all the information of salesman profiles from SQL table to client.
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 		= $_SESSION['login_user'];
	$todays_date    = date('Y-m-d H:i:s');
	
	$query = "SELECT prof_name, avatar, creation_date, CASE WHEN LENGTH(prof_pass)>3 THEN 1 ELSE 0 END AS has_pass, admin, profsessid   FROM profiles WHERE bus_email = '$bus_email' ORDER BY admin DESC;";
    $result = mysqli_query($db,$query);
    $count  = mysqli_num_rows($result);
    
	$return_arr = Array();
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		array_push($return_arr,$row);
	}
	
	echo json_encode($return_arr);
}
?>


