<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email    = $_SESSION['login_user'];
    $ss_name      = $_SESSION['login_bus_name'];
    $pass1        = mysqli_real_escape_string($db,$_POST['new_pass_1']);
    $pass2        = mysqli_real_escape_string($db,$_POST['new_pass_2']);
    $oldPass      = mysqli_real_escape_string($db,$_POST['old_pass']);
    $reg_pass_enc = password_hash($pass1, PASSWORD_DEFAULT);
    $todays_date  = date('Y-m-d H:i:s'); 

    $old_pass   = $oldPass;
    $new_pass_1 = $pass1;
    $new_pass_2 = $pass2;

    $r = array();

    // Get actual password
    $query = "SELECT pass_enc FROM register_users WHERE bus_email = '$bus_email'";
    $result = mysqli_query($db,$query);
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $curr_pass = $row['pass_enc'];

    if (password_verify($oldPass, $curr_pass)) {
        if(password_verify($pass1, $curr_pass)){
            $r[0] = array('flag' => 1, 'flag_desc' => 'La nueva contraseña debe ser distinta a la actual');
            echo json_encode($r);
        }else{
            // Change Password
            $query = "UPDATE register_users SET pass_enc = '". $reg_pass_enc ."' WHERE bus_email = '". $bus_email ."'";
            mysqli_query($db,$query);

            // Update Date of Mod
            $query = "UPDATE register_users SET last_mod_date = NOW() WHERE bus_email = '". $bus_email ."'";
            mysqli_query($db,$query);
            
            // Enviar correo de confirmación
            if (SendMail_SMTP_SimpleMsg($bus_email,
                                "🔑 Contraseña Actualizada para " . $bus_email,
                                $ss_name,
                                "Tu contraseña para la cuenta <b>". $bus_email ."</b> ha sido actualizada correctamente el ". $IP_data_date ." en " . $IP_country .".<br></br> Si <b>fuiste tú</b> quien solicitó el cambio de contraseña puedes ignorar este mensaje. <br></br> Si <b>no fuiste tú</b>, por favor comunicate con servicio técnico.<br></br><br></br> Gracias, <br></br> Servicio técnico<br>2Luca<br>Bogotá, Colombia <br> "))
            {
                $r[0] = array('flag' => 0, 'flag_desc' => 'La contraseña se ha actualizado correctamente');
                echo json_encode($r);

            }else{
                $r[0] = array('flag' => 0, 'flag_desc' => 'La contraseña se ha actualizado correctamente');
                echo json_encode($r);
            }
        }
    }else{
        $r[0] = array('flag' => 2, 'flag_desc' => 'La contraseña actual es incorrecta');
        echo json_encode($r);
    }

 } 

?>