<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$ncust_bus_email = $_SESSION['login_user'];
	$ncust_name 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_name']));
	$ncust_email 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_email']));
	$ncust_phone 	 = (mysqli_real_escape_string($db,$_POST['ncust_phone']));
	$ncust_bdate 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_bdate']));
	$gender 		 = trim(mysqli_real_escape_string($db,$_POST['gender']));
    $ncust_id 		 = trim(mysqli_real_escape_string($db,$_POST['ncust_id']));
    $ncust_type_id   = trim(mysqli_real_escape_string($db,$_POST['ncust_type_id']));
    $todays_date     = date('Y-m-d H:i:s');
    
    $ncust_phone = preg_replace("/[^0-9]/", "", $ncust_phone);
    
    $return_arr = Array();

    //******* Verificar que el cliente no exista por NUMERO DE ID *******
    $query_consult = "SELECT cust_name, cust_id, cust_key FROM customers WHERE bus_email = '$ncust_bus_email' AND cust_id = '$ncust_id' AND cust_type_id = '$ncust_type_id'";
    $result = mysqli_query($db, $query_consult);
    $count_1 = mysqli_num_rows($result);
    if($count_1 == 1){
        // ******** F1: El cliente con ese ID y Tipo ID ya existe
        array_push($return_arr,Array('F','1'));
        while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
            array_push($return_arr,$row);
        }
        echo json_encode($return_arr);  
    }else{
        if($ncust_name == '' || $ncust_id  == '' || $ncust_type_id == ''){
            // ******** E1: Faltan datos
            array_push($return_arr,Array('E','1'));
            echo json_encode($return_arr);
        }else{
            if($ncust_phone == ''){$ncust_phone = 0;}
            if($gender == ''){$gender = 0;}
            if($ncust_bdate == ''){$ncust_bdate = '1900-01-01';}
            $query = "INSERT INTO customers (bus_email, cust_name, cust_email, cust_phone, cust_bdate, cust_gender, cust_date_register, cust_points, cust_key, cust_id, cust_type_id) VALUES ('$ncust_bus_email', '$ncust_name', '$ncust_email', '$ncust_phone', '$ncust_bdate', '$gender', '$todays_date','0',FLOOR(RAND()*(99999999999999-10000000000000+1))+10000000000000,'$ncust_id','$ncust_type_id')";
            mysqli_query($db, $query);
            
            $query_consult4 = "SELECT cust_name, cust_id, cust_key FROM customers WHERE bus_email = '$ncust_bus_email' AND cust_id = '$ncust_id' AND cust_type_id = '$ncust_type_id'";
            $result4 = mysqli_query($db, $query_consult4);
            $count_4 = mysqli_num_rows($result4);
            if($count_4 == 1){
                // ******** S1: Cliente Ingresado Correctamente
                array_push($return_arr,Array('S','1'));
                while ($row = mysqli_fetch_array($result4,MYSQLI_ASSOC)) {
                    array_push($return_arr,$row);
                }
                echo json_encode($return_arr);
            }else{
                // ******** E2: Error ingresando el cliente
                array_push($return_arr,Array('E','2'));
                echo json_encode($return_arr);
            }          
        }
       
    }
   	
}


?>