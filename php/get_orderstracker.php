<?php

include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);

if($_POST){

    $cust_key  = mysqli_real_escape_string($db,$_POST["cust_key"]);
    $date_cut  = date('Y-m-d H:i:s',strtotime("-1 days"));
    
    $ret = array();

    if($cust_key!=''){
        $query   = "SELECT t.trx_date, t.id_bill, b.bus_name, t.comanda_state FROM transactions t LEFT JOIN register_users b ON t.bus_email = b.bus_email WHERE t.cust_key = '$cust_key' AND t.trx_date >='$date_cut' AND t.state = 1 AND b.user_gone = 0 AND bus_industry IN ('13','2','37','5','49') ORDER BY t.trx_date ASC;";
        $results = mysqli_query($db, $query);
        while($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
            array_push($ret, $row);
        }
    }


    echo json_encode(array_values($ret));
}

?>