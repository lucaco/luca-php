<?php


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8"/>
</head>

<body style="width: 48mm; font-family: 'Helvetica', Times, Monospace; font-size: 7px; margin: 0; padding: 0;">
	<div id="x" style="overflow: hidden;">
    
        <div style="text-align:center; margin-bottom:20px;">  
            <p style="font-size: 7px;"> <b>POSTRES Y TORTAS FERNANDA</b> </p>
            <p> NIT. 900.928.070-70 </p>
            <p> Carrera 134 A 17 - 92 </p>
            <p> Tel: 310 209 8044 </p>
            <p> Régimen Simplificado (SAS)</p>
        </div>
        
        <div style="text-align:left; margin-bottom:10px;"> 
            <p> Factura de venta número: <span>150</span></p>
            <p> Fecha de expedición: <span>13/07/2018 2:09 p.m</span></p>
            <p> Cliente: <span style="text-transform: uppercase;">Peter Jesus Usnavy</span></p>
            <p> Vendido por: <span style="text-transform: uppercase;">Pancracio Allin</span></p>
        </div>
        
        <table border="0" cellspacing="0" cellpadding="5" width="100%">
        	<tr style="margin-bottom: 5px;">
        		<th width="20%" align="center"><b>Cant.</b></th>
        		<th width="45%" align="center"><b>Descripción.</b></th>
        		<th width="35%" align="center"><b>Total</b></th>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel de jamón y queso</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel de jamón y queso</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">1</td>
        		<td width="45%" align="center">Tinto Grande 220</td>
        		<td width="35%" align="center">4,300</td>
        	</tr>
        	<tr>
        		<td width="20%" align="center">2</td>
        		<td width="45%" align="center">Pastel</td>
        		<td width="35%" align="center">6,600</td>
        	</tr>
        </table>
        
        <div style="width:100%; text-align: center; margin-bottom: 0px; margin-top: 2px;">
            -----------------------------------------------------------------
        </div>

		<div style="margin-bottom: 0px; margin-top: 2px;">
			<table border="0" cellspacing="0" cellpadding="3" width="100%">
	        	<tr>
				    <td width="70%" align="right">Valor Venta</td>
				    <td width="30%" align="center">9,265</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right">Descuento(0%)</td>
				    <td width="30%" align="center">0</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right">Subtotal antes de impuestos</td>
				    <td width="30%" align="center">9,265</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right">Impuestos</td>
				    <td width="30%" align="center">1,635</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right">Subtotal</td>
				    <td width="30%" align="center">10,900</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right">Propina</td>
				    <td width="30%" align="center">1,000</td>
	        	</tr>
	        	<tr>
				    <td width="70%" align="right"><b>TOTAL A PAGAR</b></td>
				    <td width="30%" align="center"><b>11,900</b></td>
	        	</tr>
	        </table>
        </div>
        
        <div style="width:100%;">
	        <p style="width:100%; margin-bottom: 0px; margin-top: 1px; text-align: center;">
	            -----------------------------------------------------------------
	        </p>
            <p align="center"> <b>INFORMACIÓN TRIBUTARIA</b> </p>
            <p align="center"> Impuesto al consumo del 15% con una base gravable de 9,265 para un valor total de impuesto de 1,635 </p>
            <p style="width:100%; text-align: center;">
	            -----------------------------------------------------------------
	        </p>
	        
            <p style="text-align:center;"> <b>PAGO CLIENTE</b> </p>
            <table border="0" cellspacing="0" cellpadding="2" width="100%">
            	<tr>
				    <td width="60%" align="right">TOTAL</td>
				    <td width="40%" align="center">11,900</td>
            	</tr>
            	<tr>
				    <td width="60%" align="right">EFECTIVO</td>
				    <td width="40%" align="center">15,000</td>
            	</tr>
            	<tr>
				    <td width="60%" align="right">CAMBIO</td>
				    <td width="40%" align="center">3,100</td>
            	</tr>
            </table>
        </div>
        
        <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center;">
            --------------- CUENTA CERRADA ---------------
        </div>
        
        <div style="width:100%;">
            <p style="text-align:left; margin:1px 0;">Resolución No. 32000086308</p>
            <p style="text-align:left; margin:1px 0;">Fecha Resolución 26/04/2018</p>
            <p style="text-align:left; margin:1px 0;">Numercación autorizada del 1 al 500</p>
            <p style="text-align:left; margin:1px 0;">ICA Tarifa 9.66 X 1000</p>
            <p style="text-align:left; margin:1px 0;">NO SOMOS AUTORETENEDORES</p>
            <p style="text-align:left; margin:1px 0;">NO SOMOS GRANDES CONTRIBUYENTES</p>
        </div>
        
        <div style="width:100%; margin-bottom: 5px; margin-top: 5px; text-align: center;">
            -----------------------------------------------------------------
        </div>
        
        <div width="100%" style="text-align: center;">
        	<center><img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&choe=ISO-8859-1&chl=Rábano Rojo|1086|4300|Kilogramo|0" alt="QRCODE" width="60" height="60" border="0" /></center>
        	<h6 align="center" style="font-size: 10px;">!HOLA JUAN! TIENES <span>340</span> PUNTOS LUCA ACUMULADOS AL 05/07/2018</h6>
        </div>  
    </div>
</body>
</html>