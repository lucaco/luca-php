<?php 
/* 
 * FILE: push_new_profile.php
 * WHAT FOR: Insert in profiles SQL table a new profile register
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email      = $_SESSION['login_user'];
 	$prof_name      = mysqli_real_escape_string($db,$_POST['prof_name']);
    $prof_pass      = mysqli_real_escape_string($db,$_POST['prof_pass']);
    $avatar      	= mysqli_real_escape_string($db,$_POST['avatar']);
    $items_1       	= mysqli_real_escape_string($db,$_POST['items_1']);
    $items_2       	= mysqli_real_escape_string($db,$_POST['items_2']);
    $items_3       	= mysqli_real_escape_string($db,$_POST['items_3']);
    $customers_1    = mysqli_real_escape_string($db,$_POST['customers_1']);
    $customers_2    = mysqli_real_escape_string($db,$_POST['customers_2']);
    $customers_3    = mysqli_real_escape_string($db,$_POST['customers_3']);
    $bills_1       	= mysqli_real_escape_string($db,$_POST['bills_1']);
    $bills_2       	= mysqli_real_escape_string($db,$_POST['bills_2']);
    $analytics_1    = mysqli_real_escape_string($db,$_POST['analytics_1']);
    $analytics_2    = mysqli_real_escape_string($db,$_POST['analytics_2']);
    
    $todays_date    = date('Y-m-d H:i:s');
    $profKey        = generateRandomString(30);
	
	$query_v = "SELECT bus_email, prof_name FROM profiles WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
    $result = mysqli_query($db,$query_v);
    $count  = mysqli_num_rows($result);
    
    if($prof_pass == ''){
    	$passEncode = '';
    }else{
    	$passEncode = password_hash($prof_pass,PASSWORD_DEFAULT);
    }
    
    
    if($count == 1){
    	echo 'El perfil ' . $prof_name . ' ya existe';
    }else{
	    $query = "INSERT INTO profiles (bus_email, prof_name, prof_pass, avatar, items_1, items_2, items_3, customers_1, customers_2, customers_3, bills_1, bills_2, analytics_1, analytics_2, creation_date, admin, profsessid) VALUES ('$bus_email', '$prof_name', '$passEncode', '$avatar', '$items_1', '$items_2', '$items_3', '$customers_1', '$customers_2', '$customers_3', '$bills_1', '$bills_2', '$analytics_1', '$analytics_2', '$todays_date', 0, '$profKey')";
	    mysqli_query($db,$query);
	    echo '1';
   } 

 } 

?>