<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$ncust_bus_email = $_SESSION['login_user'];
	$ncust_name 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_name']));
	$ncust_email 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_email']));
	$ncust_phone 	 = (mysqli_real_escape_string($db,$_POST['ncust_phone']));
	$ncust_bdate 	 = trim(mysqli_real_escape_string($db,$_POST['ncust_bdate']));
	$gender 		 = trim(mysqli_real_escape_string($db,$_POST['gender']));
    $ncust_id 		 = trim(mysqli_real_escape_string($db,$_POST['ncust_id']));
    $ncust_id_old    = trim(mysqli_real_escape_string($db,$_POST['ncust_id_old']));
    $ncust_type_id   = trim(mysqli_real_escape_string($db,$_POST['ncust_type_id']));
    $ncust_type_id_old = trim(mysqli_real_escape_string($db,$_POST['ncust_type_id_old']));
    $cust_key 		 = trim(mysqli_real_escape_string($db,$_POST['cust_key']));
    
    $todays_date    = date('Y-m-d H:i:s');
    
    $valid = 1;
    $ncust_phone = preg_replace("/[^0-9]/", "", $ncust_phone);

    if($cust_key != '' && $ncust_id != ''){

        if($ncust_id != $ncust_id_old || $ncust_type_id != $ncust_type_id_old){
            // *** Check if the customer exists already by cust id ****/
            $query_consult = "SELECT cust_name, cust_id, cust_type_id FROM customers WHERE bus_email = '$ncust_bus_email' AND cust_id = '$ncust_id' AND cust_type_id = '$ncust_type_id'";
            $result = mysqli_query($db, $query_consult);
            $count_1 = mysqli_num_rows($result);
            if($count_1 == 1){
                $return_arr = Array();
                array_push($return_arr,Array('F','1'));
                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                    array_push($return_arr,$row);
                }
                $valid = 0;
                echo json_encode($return_arr);  
            }
        }

        if($valid == 1){
            if($ncust_phone == ''){$ncust_phone = 0;}
            if($gender == ''){$gender = 0;}
            if($ncust_bdate == ''){$ncust_bdate = '1900-01-01';}

            $return_arr3 = Array();

            $query = "UPDATE customers SET cust_name='$ncust_name', cust_email='$ncust_email', cust_phone='$ncust_phone', cust_bdate='$ncust_bdate', cust_gender='$gender', cust_id='$ncust_id', cust_type_id='$ncust_type_id' WHERE cust_key = '$cust_key'";
            mysqli_query($db,$query);

            if(mysqli_affected_rows($db)<=0){
                array_push($return_arr3,Array('E','1'));
            }else{
                array_push($return_arr3,Array('S','1'));
            }  

            echo json_encode($return_arr3);
        }   
        
    }      

}


?>