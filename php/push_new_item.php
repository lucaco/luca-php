<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$prod_email     = $_SESSION['login_user'];
 	$prod_name      = mysqli_real_escape_string($db,$_POST['ni_prod_name']);
	$prod_code      = mysqli_real_escape_string($db,$_POST['ni_prod_code']);
	$prod_units     = mysqli_real_escape_string($db,$_POST['ni_prod_units']);
	$prod_category  = mysqli_real_escape_string($db,$_POST['ni_prod_category']);
	$prod_icon      = mysqli_real_escape_string($db,$_POST['ni_prod_icon']);
	$prod_price     = mysqli_real_escape_string($db,$_POST['ni_prod_price']);
	$prod_cost      = mysqli_real_escape_string($db,$_POST['ni_prod_cost']);
	$prod_tax       = mysqli_real_escape_string($db,$_POST['ni_prod_tax']);
	$todays_date    = date('Y-m-d H:i:s');

	$prod_price_clean = preg_replace("/[^0-9.-]/", "", $prod_price);
	$prod_cost        = preg_replace("/[^0-9.-]/", "", $prod_cost);
	$prod_tax         = preg_replace("/[^0-9.]/", "", $prod_tax);

	if($prod_price_clean == ""){
		$prod_price_clean = 0;
	}
	if($prod_cost == ""){
		$prod_cost = 0;
	}
	if($prod_tax == ""){
		$prod_tax = 0;
	}

	$query_v = "SELECT bus_email FROM products WHERE bus_email = '$prod_email' AND prod_id = '$prod_code'";
	$result = mysqli_query($db,$query_v);
	$count  = mysqli_num_rows($result);

	if($count == 1){
		echo 'El ítem con código '. $prod_code .' ya existe';
	}else{

		$query = "INSERT INTO products (bus_email, prod_id, prod_name, prod_unit_price, prod_units, prod_icon, category, tax, creation_date, last_mod_date, prod_unit_cost) VALUES ('$prod_email', '$prod_code', '$prod_name', '$prod_price_clean', '$prod_units', '$prod_icon', '$prod_category', '$prod_tax', '$todays_date', '$todays_date', '$prod_cost')";
		mysqli_query($db,$query);

		$query_v = "SELECT bus_email, prod_id FROM products WHERE bus_email = '$prod_email' AND prod_id = '$prod_code'";
		$result = mysqli_query($db,$query_v);
		$count  = mysqli_num_rows($result);

		if($count == 1){
			echo '1';
		}else{
			echo 'No fue posible ingresar el ítem';
		}

	} 

 } 

?>