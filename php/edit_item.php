<?php 

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$prod_email     = $_SESSION['login_user'];
 	$prod_name      = mysqli_real_escape_string($db,$_POST['ni_prod_name2']);
  $prod_code      = mysqli_real_escape_string($db,$_POST['ni_prod_code2']);
  $prod_units     = mysqli_real_escape_string($db,$_POST['ni_prod_units2']);
  $prod_category  = mysqli_real_escape_string($db,$_POST['ni_prod_category2']);
  $prod_icon      = mysqli_real_escape_string($db,$_POST['ni_prod_icon2']);
  $prod_price     = mysqli_real_escape_string($db,$_POST['ni_prod_price2']);
	$prod_cost      = mysqli_real_escape_string($db,$_POST['ni_prod_cost2']);
	$prod_tax       = mysqli_real_escape_string($db,$_POST['ni_prod_tax2']);
  $todays_date    = date('Y-m-d H:i:s');

  $prod_price_clean = preg_replace("/[^0-9.]/", "", $prod_price);
	$prod_cost        = preg_replace("/[^0-9.]/", "", $prod_cost);
	$prod_tax         = preg_replace("/[^0-9.]/", "", $prod_tax);

  if($prod_price_clean == ""){
		$prod_price_clean = 0;
  }
  
	if($prod_cost == ""){
		$prod_cost = 0;
  }
  
	if($prod_tax == ""){
		$prod_tax = 0;
  }
  
	$query = "UPDATE products SET prod_name='$prod_name', prod_unit_price='$prod_price_clean', prod_units='$prod_units', prod_icon='$prod_icon', category='$prod_category', tax='$prod_tax', last_mod_date='$todays_date', prod_unit_cost='$prod_cost' WHERE bus_email = '$prod_email' AND prod_id = '$prod_code'";
  mysqli_query($db,$query);

  if(mysqli_affected_rows($db)<=0){
		echo 'No fue posible ingresar el ítem';
	}else{
		echo '1';
	}

 } 

?>