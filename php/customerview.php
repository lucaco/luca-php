<?php
include("functions.php");
include("config.php");
date_default_timezone_set($TimeZone);

$return_arr = Array();

$bus_email = $_SESSION['login_user'];
$ckey  = mysqli_real_escape_string($db,$_GET["ckey"]);

$displayMC = 'block';

$sql = "SELECT cust_id FROM customers WHERE cust_key = '$ckey';";
$result  = mysqli_query($db,$sql);
$row     = mysqli_fetch_array($result,MYSQLI_ASSOC);
$cust_id = $row['cust_id'];

$query = "SELECT A.cust_name, A.cust_points, B.bus_name, DATE_FORMAT(A.cust_date_register,'%Y-%m-%d') AS cust_date_register, B.bus_industry, D.ind_name, D.ind_icon, IFNULL(C.trx_id,'') AS trx_id, IFNULL(C.trx_date,'') AS trx_date, IFNULL(C.trx_value,'') AS trx_value, IFNULL(C.id_bill,'') AS id_bill, IFNULL(C.num_items,'') AS num_items, IFNULL(C.payment_type,'') AS payment_type, IFNULL(C.profsessid,'') AS profsessid, IFNULL(C.points_redeem,'') AS points_redeem FROM customers A LEFT JOIN register_users B ON A.bus_email = B.bus_email LEFT JOIN transactions C ON B.bus_email = C.bus_email AND A.cust_key = C.cust_key LEFT JOIN industry_type D ON B.bus_industry = D.ind_id WHERE A.cust_id = '$cust_id' ORDER BY trx_date DESC;";
$result = mysqli_query($db,$query);
$count  = mysqli_num_rows($result);
if($count > 0){
	while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
			array_push($return_arr,$row);
	}
}else{
	//header("location:../index.php");
	$displayMC = 'none';
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135784524-1"></script>
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135784524-1');
    </script>
	<title>Luca | Cuenta</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- CHARTS.JS -->
	<script src="../external_libs/moment.min.js?<?php echo time(); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="../css/style_customerview.css?<?php echo time(); ?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="../js/js_customerview.js?<?php echo time(); ?>"></script>
	<script src="../js/modals.js?<?php echo time(); ?>"></script>
	
	<!-- ANIMATE.CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css?<?php time();?>">
	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<script src="https://use.fontawesome.com/releases/v5.9.0/js/all.js" data-auto-replace-svg="nest"></script>
	<!-- BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- FORMAT CURRENCY -->
	<script src="../external_libs/simple.money.format.js?<?php time();?>"></script>
	<!-- AUTOCOMPLETE -->
	<script src="../external_libs/jquery.easy-autocomplete.min.js?<?php time();?>"></script>
	<link rel="stylesheet" href="../external_libs/easy-autocomplete.css?<?php time();?>">
	<!-- COOKIES JS -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <!-- TAGS AND AUTOCOMPLETE -->
    <link href="../external_libs/jquery.magicsearch.min.css" rel="stylesheet">
    <script src="../external_libs/jquery.magicsearch.min.js"></script>

</head>
<body>
<span id='ckphp' class='hide'><?php echo $ckey;?></span>

<div class="main_container" style="display:<?php echo $displayMC;?>;">
	<div class="main_wrap">		
	
		<div class="header_wrap">
			<img src="https://www.2luca.co/logos/logo5.png" alt="logo">
		</div>

		<div id="ord_tracker_container" class="header_wrap">
			<h1>Tus ordenes activas</h1>
			<h3 class="mb30">Estas son las ordenes que tienes activas actualmente en los comercios asociados.</h3>
			<div id='orders_wrap' class="orders_wrap"></div>
		</div>

		<div class="header_wrap">
			<h1>Tus transacciones</h1>
			<h3>Hola! Aquí tienes un resumen de tus transacciones en los negocios aliados de Luca.</h3>
		</div>

		<div class="boxc1_wrap bigmrg">
			<h3>Total gastado</h3>
			<div class="data_box">
				<h1>$ <span id="amount">-</span></h1>
				<!-- <i id="arrow_change" class="fas fa-arrow-down"></i>
				<p><span id="pgt_change_total">9</span>%</p> -->
			</div>
			<h4><span id="purchases"></span></h4>
		</div>
		
		<div class="boxc1_wrap bigmrg">
			<h5>Actividad de la semana (ítems)</h5>
			<div class="chart_bar">
				<div id="barS1" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS2" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS3" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS4" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS5" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS6" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS7" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar"></div></div><div class="ch_bar_lbl"></div></div>
			</div>
		</div>

		<div class="boxc1_wrap bigmrg">
			<h5>Actividad del año (compras)</h5>
			<div class="chart_bar">
				<div id="barS1_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS2_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS3_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS4_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS5_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS6_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS7_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS8_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS9_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS10_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS11_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
				<div id="barS12_yr" class="ch_bar_cont"><div class="ch_bar_wrap"><div class="ch_bar w15"></div></div><div class="ch_bar_lbl"></div></div>
			</div>
		</div>

		<div class="boxc1_wrap bigmrg">
			<h5>Por categoría</h5>
			<div id="listOfTrxCategories"></div>
		</div>
		
		<div class="boxc1_wrap bigmrg">
			<h5>Últimas transacciones</h5>
			<div id="listOfLastTrx"></div>  
		</div>
		
		
	</div>
</div>
   
<script>

//$(document).ready(function(){
		
	// ::::::::: FUNCTIONS ::::::::::
	
	let setValueCharBar = function(idBar, val, valPer, lbl){
		// **** Example: setValueCharBar("barS1_yr","0",0,Lbl[0]); *****
		$("#"+idBar+">.ch_bar_wrap>.ch_bar").css("top","calc(100px - "+valPer*100+"px)");
		//$("#"+idBar+">.bar_value").text(val);
		$("#"+idBar+">.ch_bar_wrap>.ch_bar").attr('data-content',val);
		$("#"+idBar+">.ch_bar_lbl").text(lbl);  
	}

	let sortDataByDate = function(data){
		return data.sort(function(a,b){date1 = new Date(Date.parse(a["trx_date"].replace(" ","T"))); date2 = new Date(Date.parse(b["trx_date"].replace(" ","T"))); return date1-date2});
	}

	let sortDataByKeyValue_asc = function(data,key,order){
		order = (order===undefined)?'ASC':order;
		return data.sort(function(a,b){
			val1 = parseFloat(a[key]); 
			val2 = parseFloat(b[key]);
			if(order=="ASC"){
				return val1-val2;
			}else if(order=="DESC"){
				return val2-val1;
			}else{
				return val1-val2;
			}
		});
	}
		
	let totalByKey = function(data, key, format, round){
		format = (format===undefined)?true:format;
		round  = (round===undefined)?2:round;
		var total = 0; 

		for(var i=0; i<data.length; i++){
			let x = parseFloat(data[i][key]);
			if(!isNaN(x)){
				total += parseFloat(data[i][key]);
			}	
		}
		if(format){
			return numCommas(numRound(total,round));
		}else{
			return total;
		}
	}

	let getLastWeekLabels = function(reverse){
		reverse  = (reverse===undefined)?false:reverse;
		moment.locale("es");
		let wlbl = [];
		if(!reverse){
			for(var i=0;i<7;i++){
				wlbl.push(moment().subtract(i, 'days').format('dddd').substr(0,3));
			}
		}else{
			for(var i=6;i>=0;i--){
				wlbl.push(moment().subtract(i, 'days').format('dddd').substr(0,3));
			}
		}
		return wlbl;
	}

	let getLastYearLabels = function(reverse){
		reverse  = (reverse===undefined)?false:reverse;
		moment.locale("es");
		let wlbl = [];
		if(!reverse){
			for(var i=0;i<12;i++){
				wlbl.push(moment().subtract(i, 'months').format('MMMM').substr(0,3));
			}
		}else{
			for(var i=11;i>=0;i--){
				wlbl.push(moment().subtract(i, 'months').format('MMMM').substr(0,3));
			}
		}
		return wlbl;
	}

	let groupByKey = function(data, key) {
		return data.reduce(function(rv, x) {
					payment = x[key];
					(rv[payment] = rv[payment] || []).push(x);
					return rv;
			}, {});
	};
		
	let timeFromNow = function(date){
		moment.locale("es");
		return moment(date).fromNow();
	}

	let getPercCountByGroup = function(dataGrp, round){
		round  = (round===undefined)?2:round;

		let total = 0;
		let itemVals = [];
		for(let it in dataGrp){
			total   += dataGrp[it].length;
			itemVals.push(dataGrp[it].length);
		}
		return itemVals.map(function(x){return numRound((x/total)*100,round)});
	}

	let fillCategTrx = function(data,idFill){

		$("#"+idFill).html("");
		let percIndGrp = getPercCountByGroup(data,1);

		let html = '';
		let j = 0;
		html += '<div class="list_cat_cont">';

		for(let ind in industryGrp){
			let ind_icon = encodeURI(industryGrp[ind][0]["ind_icon"]).replace(/%E2%81%A9/gm,"");
			html += '<div class="list_cat_item_cont">';             
			html += '<div class="lc_icon"><img src="'+ind_icon+'" alt="icon_el" width="40"></div>';
			html += '<div class="lc_txt_wrap">';
			html += '<h3>'+ind+'</h3>';
			html += '<h4>'+industryGrp[ind].length+' compras</h4></div>';
			html += '<div class="lc_ptg">'+percIndGrp[j]+'%</div></div>';
			j += 1;
		}

		html += '</div>';
		$("#"+idFill).html(html);

	}
	
	let fillLastTrx = function(data,idFill,numTrx){
		numTrx = (numTrx===undefined)?5:numTrx;
		$("#"+idFill).html("");
		let html = '';
		html += '<div class="list_cat_cont">';
		for(var i=0; i<numTrx; i++){
			if(cust_data[i]!=undefined){
				html += '<div class="list_cat_item_cont"><div class="lc_txt_wrap">';
				html += '<h3>'+cust_data[i]["bus_name"]+'</h3>';
				html += '<h4>'+cust_data[i]["num_items"]+' ítems | '+timeFromNow(cust_data[i]["trx_date"])+'</h4>';
				html += '</div><div class="lc_value">'+formatBigNumber(parseFloat(cust_data[i]["trx_value"]),1)+'</div></div>';
			}
		}
		html += '</div>';
		$("#"+idFill).html(html);
	}

	let DataByLastWeek = function(data){
		let weelLbl = getLastWeekLabels(true);
		let r = [0,0,0,0,0,0,0];
		for(var i=0; i<data.length; i++){
			let x = moment(data[i]["trx_date"]).format('dddd').substr(0,3);
			let y = weelLbl.indexOf(moment(data[i]["trx_date"]).format('dddd').substr(0,3));
			let z = parseFloat(data[i]["num_items"]);
			if(!isNaN(z)){
				r[y] = r[y] + z;
			}
		}
		return r;
	}

	let DataByLastYear = function(data){
		let weelLbl = getLastYearLabels(true);
		let r = [0,0,0,0,0,0,0,0,0,0,0,0];
		for(var i=0; i<data.length; i++){
			let x = moment(data[i]["trx_date"]).format('MMMM').substr(0,3);
			let y = weelLbl.indexOf(moment(data[i]["trx_date"]).format('MMMM').substr(0,3));
			let z_items = parseFloat(data[i]["num_items"]);
			//if(!isNaN(z_items)){r[y] = r[y] + z_items}
			if(!isNaN(z_items)){r[y] = r[y] + 1}
		}
		return r;
	}

	let fillLasYearData = function(data){
		let Lbl  = getLastYearLabels(true);
		let Data = DataByLastYear(data); 
		let max_val = Data.max();

		if(max_val == 0){
			setValueCharBar("barS1_yr","0",0,Lbl[0]);
			setValueCharBar("barS2_yr","0",0,Lbl[1]);
			setValueCharBar("barS3_yr","0",0,Lbl[2]);
			setValueCharBar("barS4_yr","0",0,Lbl[3]);
			setValueCharBar("barS5_yr","0",0,Lbl[4]);
			setValueCharBar("barS6_yr","0",0,Lbl[5]);
			setValueCharBar("barS7_yr","0",0,Lbl[6]);
			setValueCharBar("barS8_yr","0",0,Lbl[7]);
			setValueCharBar("barS9_yr","0",0,Lbl[8]);
			setValueCharBar("barS10_yr","0",0,Lbl[9]);
			setValueCharBar("barS11_yr","0",0,Lbl[10]);
			setValueCharBar("barS12_yr","0",0,Lbl[11]);
		}else{
			setValueCharBar("barS1_yr",Data[0],Data[0]/max_val,Lbl[0]);
			setValueCharBar("barS2_yr",Data[1],Data[1]/max_val,Lbl[1]);
			setValueCharBar("barS3_yr",Data[2],Data[2]/max_val,Lbl[2]);
			setValueCharBar("barS4_yr",Data[3],Data[3]/max_val,Lbl[3]);
			setValueCharBar("barS5_yr",Data[4],Data[4]/max_val,Lbl[4]);
			setValueCharBar("barS6_yr",Data[5],Data[5]/max_val,Lbl[5]);
			setValueCharBar("barS7_yr",Data[6],Data[6]/max_val,Lbl[6]);
			setValueCharBar("barS8_yr",Data[7],Data[7]/max_val,Lbl[7]);
			setValueCharBar("barS9_yr",Data[8],Data[8]/max_val,Lbl[8]);
			setValueCharBar("barS10_yr",Data[9],Data[9]/max_val,Lbl[9]);
			setValueCharBar("barS11_yr",Data[10],Data[10]/max_val,Lbl[10]);
			setValueCharBar("barS12_yr",Data[11],Data[11]/max_val,Lbl[11]);
		}

	}

	let fillLastWeekData = function(data){
		let weekLbl  = getLastWeekLabels(true);
		let weekData = DataByLastWeek(data); 
		let max_val = weekData.max();

		if(max_val == 0){
			setValueCharBar("barS1","0",0,weekLbl[0]);
			setValueCharBar("barS2","0",0,weekLbl[1]);
			setValueCharBar("barS3","0",0,weekLbl[2]);
			setValueCharBar("barS4","0",0,weekLbl[3]);
			setValueCharBar("barS5","0",0,weekLbl[4]);
			setValueCharBar("barS6","0",0,weekLbl[5]);
			setValueCharBar("barS7","0",0,weekLbl[6]);		
		}else{
			setValueCharBar("barS1",weekData[0],weekData[0]/max_val,weekLbl[0]);
			setValueCharBar("barS2",weekData[1],weekData[1]/max_val,weekLbl[1]);
			setValueCharBar("barS3",weekData[2],weekData[2]/max_val,weekLbl[2]);
			setValueCharBar("barS4",weekData[3],weekData[3]/max_val,weekLbl[3]);
			setValueCharBar("barS5",weekData[4],weekData[4]/max_val,weekLbl[4]);
			setValueCharBar("barS6",weekData[5],weekData[5]/max_val,weekLbl[5]);
			setValueCharBar("barS7",weekData[6],weekData[6]/max_val,weekLbl[6]);
		}

	}


	let filterByDate = function(data, date) {
		return data.filter(function(x){
				return new Date(Date.parse(x["trx_date"].replace(" ","T"))) >= date;
		});
	};

	let now           = new Date();
  let today         = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	let yesterday     = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
	let lastWeek      = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
	let twoWeeksAgo   = new Date(lastWeek.getFullYear(), lastWeek.getMonth(), lastWeek.getDate() - 7);
	let threeWeeksAgo = new Date(twoWeeksAgo.getFullYear(), twoWeeksAgo.getMonth(), twoWeeksAgo.getDate() - 7);
	let fourWeeksAgo  = new Date(threeWeeksAgo.getFullYear(), threeWeeksAgo.getMonth(), threeWeeksAgo.getDate() - 7);
	let lastMonth     = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate());
	let twoMonthsAgo  = new Date(today.getFullYear(), today.getMonth() - 2, today.getDate());
	let sixMonthsAgo  = new Date(today.getFullYear(), today.getMonth() - 6, today.getDate());
	let lastYear      = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
	let twoYearsAgo   = new Date(today.getFullYear() - 2, today.getMonth(), today.getDate());
	let allData       = new Date(today.getFullYear() - 50, today.getMonth(), today.getDate());

	let cust_data = <?php echo json_encode(array_values($return_arr));?>;

	let lastWeekData = filterByDate(cust_data,lastWeek);
	let lastMonthData = filterByDate(cust_data,lastMonth);
	let lastYearData = filterByDate(cust_data,lastYear);

	let cust_name  = cust_data[0].cust_name;
	let amount     = totalByKey(cust_data, "trx_value");
	let cant_items = totalByKey(cust_data, "num_items");
	let purchases  = cust_data.length;

	// Summary Data
	$("#name_user").text(cust_name);
	$("#amount").text(amount);
	if(purchases==1){
		$("#purchases").text(purchases+" compra");
	}else{
		$("#purchases").text(purchases+" compras");
	}

	// Last Transactions
	fillLastTrx(cust_data,"listOfLastTrx",5);

	// Total By Category
	let industryGrp = groupByKey(cust_data,"ind_name");
	fillCategTrx(industryGrp,"listOfTrxCategories");
  
	// Fill Week Data
	fillLastWeekData(lastWeekData);

	// Fill Year Data
	fillLasYearData(lastYearData);

//});

</script>

</body>
</html>