<?php

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);


if($_POST){

  $bus_email = $_SESSION['login_user'];
  $date_cut    = date('Y-m-d H:i:s',strtotime("-1 days"));
  
  $query   = "SELECT t.trx_date, t.id_bill, t.num_items, IFNULL(i.prod_name,'Ítem') AS prod_name, b.item_count, c.cust_key, IFNULL(c.cust_email,'') AS cust_email, IFNULL(c.cust_name, '') AS cust_name, IFNULL(c.cust_phone,'') AS cust_phone,  b.item_note as note, d.prof_name as salesman, t.comanda_state, u.bus_name FROM transactions t LEFT JOIN bills b ON b.id_bill = t.id_bill AND b.bus_email = t.bus_email LEFT JOIN products i ON b.item_id = i.prod_id AND b.bus_email = i.bus_email LEFT JOIN customers c ON t.cust_key = c.cust_key LEFT JOIN profiles d ON t.profsessid = d.profsessid AND t.bus_email = d.bus_email LEFT JOIN register_users u ON t.bus_email = u.bus_email WHERE t.bus_email = '" .$bus_email. "' AND t.state = 1 AND t.trx_date >='" .$date_cut. "' ORDER BY t.trx_date DESC;";
  $results = mysqli_query($db, $query);
  $ret = array();

  while($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
      array_push($ret, $row);
  }

  echo json_encode(array_values($ret));
}

?>