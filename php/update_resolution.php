<?php
/* 
 * FILE: update_vars_accnt.php
 * WHAT FOR: Update variables of user account
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email        = $_SESSION['login_user'];
    $resolution       = trim(mysqli_real_escape_string($db,$_POST['resolution']));
    $date_resolution  = trim(mysqli_real_escape_string($db,$_POST['date_resolution']));
    $range_reso_ini   = trim(mysqli_real_escape_string($db,$_POST['range_reso_ini']));
    $range_reso_last  = trim(mysqli_real_escape_string($db,$_POST['range_reso_last']));
    $curr_resol       = trim(mysqli_real_escape_string($db,$_POST['curr_resol']));
    
    $r = Array();
    $query = "SELECT resolution FROM resolutions WHERE bus_email='$bus_email' AND resolution='$resolution';";
    $result = mysqli_query($db,$query);
    $count  = mysqli_num_rows($result);

    if($count>=1){
        array_push($r, Array('F','La resolución número '.$resolution.' ya existe.'));
        echo json_encode($r);
    }else{
        if($curr_resol == '1'){
            // SET ALL RESOLUTIONS TO ZERO 
            $query = "UPDATE resolutions SET current_resolution = '0' WHERE bus_email = '$bus_email';";
            mysqli_query($db,$query);
        }

        $query = "INSERT INTO resolutions VALUES('$bus_email','$resolution','$date_resolution','$range_reso_ini','$range_reso_last','$curr_resol');";     
        mysqli_query($db,$query);

        if(mysqli_affected_rows($db)<=0){
            array_push($r, Array('E','1'));
        }else{
            array_push($r, Array('S','1'));
        }
        echo json_encode($r);
    }



}
?>