<?php 
/* 
 * FILE: set_avatar_profile.php
 * WHAT FOR: Set avatar to profile
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
 	$bus_email      = $_SESSION['login_user'];
 	$prof_name      = mysqli_real_escape_string($db,$_POST['prof_name']);
    $avatar_dir     = mysqli_real_escape_string($db,$_POST['avatar_dir']);

	$query_v = "UPDATE profiles SET avatar = '$avatar_dir' WHERE bus_email = '$bus_email' AND prof_name = '$prof_name'";
    mysqli_query($db,$query_v);
    
    if(mysqli_affected_rows($db)<=0){
		$RunCorrectly = 0; // Do not run correctly
	}else{
		$RunCorrectly = 1; // Do run correctly
	}
	
	echo $RunCorrectly;

 } 

?>