<?php
/* 
 * FILE: get_profile_key.php
 * WHAT FOR: Get key from a profile
 * CREATOR: Juan Camilo Díaz H
*/

include("functions.php");
include("config.php");
session_start();
date_default_timezone_set($TimeZone);

if($_POST){
	$bus_email 		= $_SESSION['login_user'];
	$prof_name 		= mysqli_real_escape_string($db,$_POST['prof_name']);
	$todays_date    = date('Y-m-d H:i:s');
	
	$query = "SELECT profsessid FROM profiles WHERE bus_email = '$bus_email' AND prof_name='$prof_name'";
	$result = mysqli_query($db,$query);
	$count  = mysqli_num_rows($result);
	
	while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		$key = $row['profsessid']; 
	}
	
	echo $key;
}
?>
