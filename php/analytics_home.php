<?php
	include("config.php");
	include("functions.php");
	date_default_timezone_set($TimeZone);
	
	session_start();
	$php_warning = "";
	$php_message = "";
	$php_alert = "";

	if (isset($_SESSION['login_user']) && !empty($_SESSION['login_user'])){header("location:sales.php");}

?>

<!DOCTYPE html>
<html>
<head>

	<title>2Luca | Analítica</title>

	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Registro de ventas y reportes utilizando analítica de datos">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="../external_libs/wow.min.js"></script>

  <!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- MY CLASSES -->
	<link rel="stylesheet" type="text/css" href="../css/general_classes.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" rel="stylesheet" href="../css/style_landing.css?<?php echo time();?>">
	<script src="../js/js_home_functions.js?<?php echo time(); ?>"></script>
	<script src="/js/modals.js?<?php echo time();?>"></script>

	<!-- ANIMATED CSS -->
	<link rel="stylesheet" href="../external_libs/animate.min.css">

	<!-- ALERTIFY -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>

	<!-- MATERIAL DESIGN ICONS -->
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">

	<script>

		// Activate Wow.min.js
		new WOW().init();

		$(document).ready(function(){
			
			deleteAllCookies();

			$("#currYear").text(new Date().getFullYear());

			// ************ LOGIN *****************
			$('#login-form').submit(function(){
				email = $("#login-email").val();
				pass  = $("#login-pass").val();
				remem = String($("#login-rememberme").prop('checked'));

				$.ajax({
						url: "login_func.php",
						data: ({login_email: email, login_pass: pass, login_remember: remem}),
						type: "POST",
						success: function(r){
							if(r == '1'){
								window.location.href = "sales.php";
							}else{
								ErrorMsgModalBox('open','errorMsgLogin',r);
							}
		        }
			    });
				return false;
			});
			//**************************************

			var winScreenW = $(window).width();
			var cook_hide = false;

			$('#btn_gotit_cookies').click(function(){
				$('#cookiesMsg').css({'height': '0px', 'padding' : '0'});
				cook_hide = true;
			});	

			if (winScreenW <= 710){
				setTimeout(function() {
				    $('#cookiesMsg').css({'height': '150px', 'padding' : '5px 20px 20px 20px'});
				  }, 4000);	
			}else{
				setTimeout(function() {
				    $('#cookiesMsg').css({'height': '65px', 'padding' : '5px 5px'});
				  }, 4000);				
			}

			$(window).on('resize', function(){

				var wsW = $(window).width();

				if (wsW <= 710 && cook_hide == false){
					setTimeout(function() {
					    $('#cookiesMsg').css({'height': '150px', 'padding' : '5px 20px 20px 20px'});
					  }, 100);	
				}else if(wsW > 710 && cook_hide == false){
					setTimeout(function() {
					    $('#cookiesMsg').css({'height': '65px', 'padding' : '5px 5px'});
					  }, 100);				
				}

			});


		});
		
	</script>
</head>
<body>

	<!-- NAVIGATION BAR -->
	<div class="navigation_container" id="navigation_container">

		<img id="img_logo_nav" src="../logos/logo8.png" class='logo_nav_tx'>
		
		<ul id="menu_wide_screen">
			<li>Analítica</li>
			<li>Ayuda</li>
			<li id="btn_nav_login">Iniciar Sesión</li>
			<li id="btn_nav_signin">Registrarme</li>
		</ul>

		<span class="mdi mdi-chevron-down menu_icon white" id="menu_icon"></span>

		<ul id="menu_small_screen">
			<li>Analítica</li>
			<li>Ayuda</li>
			<li id="btn_nav_login">Iniciar Sesión</li>
			<li id="btn_nav_signin">Registrarme</li>
		</ul>

	</div>

	<!-- COOKIES -->
	<div class="cookiesMsg" id="cookiesMsg">
		<p>Este sitio web utiliza cookies para poder obtener la mejor expericencia de usuario. <a href="#">Ver más</a></p>
		<button class="btn_style_1" id="btn_gotit_cookies">¡Entiendo!</button>
	</div>


	<div class="container_simple_msg">

		<div class="container_simple_msg_2">
			<h1 class="title_1_font fs50">ANALÍTICA</h1>
			<h2>Descubre qué es tener un asistente de ventas mientras facturas</h2>
		</div>


		<svg class="svg_poly_botm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none">
		 	<polygon points="0,100 100,0 100,100" style="fill:#fff;" />
		</svg>

	</div>

	<div class="container_info_main">
		<div class="container_info_box">
			<div class="info_box_text">
				<h1 class="title_1_font">Recomienda productos automáticamente</h1>
				<p>Entendemos a tus clientes y le sugerimos otros ítems dentro de tu tienda que sean más afines a sus intereses y perfil </p>
			</div>
			<div class="info_box_img txt_algn_left wow fadeIn">
				<img  src="../images/lucaproductrecom.svg">
			</div>
		</div>

		<svg class="svg_poly_botm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none">
		 	<polygon points="0,0 0,100 100,100" style="fill:#f9f9f9;" />
		</svg>

	</div>

	<div class="container_info_main gray_bg">
		<div class="container_info_box">
			<div class="info_box_img txt_algn_right wow fadeIn">
				<img src="../images/lucamaxconsumer.svg">
			</div>
			<div class="info_box_text">
				<h1 class="title_1_font">Maximiza el valor de tus clientes</h1>
				<p>Nuestro sistema de recomendación basado en analítica de datos incrementa las ventas y genera mayor lealtad con el mínimo esfuerzo</p>
			</div>
		</div>

		<svg class="svg_poly_botm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none">
		 	<polygon points="0,100 100,0 100,100" style="fill:#ffffff;" />
		</svg>

	</div>

	<div class="container_info_main">
		<div class="container_info_box">
			<div class="info_box_text">
				<h1 class="title_1_font"> Sugerencias de descuentos cuando se necesitan </h1>
				<p> Analizamos el comportamiento de tus clientes y sugerimos descuentos para generar lealtad en tus nuevos clientes y poder evitar la pérdida de tus clientes más valiosos </p>
			</div>
			<div class="info_box_img txt_algn_left wow fadeIn">
				<img src="../images/lucadiscount800.svg">
			</div>
		</div>

		<svg class="svg_poly_botm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none">
		 	<polygon points="0,0 0,100 100,100" style="fill:#f9f9f9;" />
		</svg>
	</div>

	<div class="container_info_main gray_bg">
		<div class="container_info_box">
			<div class="info_box_img txt_algn_right">
				<img class="wow fadeIn" src="../images/lucapricestores.svg" >
			</div>
			<div class="info_box_text">
				<h1 class="title_1_font"> Tips de catálogo de productos y precio </h1>
				<p> Identificamos por tu locación y perfil de clientes la incorporación de nuevos ítems a tu portafolio de productos o ajustes de precios para mejorar tus ventas </p>
			</div>
		</div>

		<!--<svg class="svg_poly_botm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none">
		 	<polygon points="0,100 100,0 100,100" style="fill:#272727;" />
		</svg>-->

	</div>

	<div class="container_simple_msg">
		<div class="container_simple_msg_2">
			<h1 class="title_1_font">Pruébalo tu mismo</h1>
			<h2> Descubre qué es tener un asistente de ventas mientras facturas. <b>Por solo $ 29.900 al mes</b> Incluye todas las funcionalidades de analítica.</h2>
			<a href="register.php" class="btn_style_2 bounceInUp">PROBARLO AHORA</a>
		</div>
	</div>

	<!-- PIE DE PAGINA -->
	<div class="home-section-footpage">
		<div class="foot_container">
			
			<div class="foot_cont_1">
				<div class="foot_cont_1_text">
					<h1>ACERCA DE</h1>
					<p class="link-pp"><a href="https://medium.com/blog-2luca/pol%C3%ADtica-de-privacidad-baec170a45e9" target="_blank">Política de privacidad</a></p>
					<p class="link-toc"><a href="https://medium.com/@2lucaco/t%C3%A9rminos-y-condiciones-9e4283b1d457" target="_blank">Términos de servicio</a></p>	
				</div>
			</div>

			<div class="foot_cont_2">
				<div class="foot_cont_1_text">
					<h1>RECURSOS</h1>
					<p class="click_login_foot" onclick="OpenLogInForm();">Iniciar Sesión</p>
					<p><a href="register.php">Registrarse</a></p>	
					<p><a href="analytics_home.php">Analítica</a></p>
					<p><a href="https://medium.com/blog-2luca" target="_blank">Ayuda</a></p>		
				</div>
			</div>

			<div class="foot_cont_3">
				<div class="foot_cont_3_logo">
					<img src="../logos/logo8.png">
				</div>
				<p>Si tienes cualquier duda no dudes en contactarte a:</p>
				<p><a href="mailto:info@2luca.co">info@2luca.co</a></p>
			</div>
		</div>
	
		<div class="foot_social_netw">
			<a href="https://medium.com/blog-2luca" target="_blank"><img src="../images/social_medium_white.svg"></a>
			<a href="https://www.youtube.com/channel/UCKDmX9RgsSPg0eJCMItI5IA" target="_blank"><img src="../images/social_youtube_white.svg"></a>
			<a href="http://instagram.com/2lucaco/" target="_blank"><img src="../images/social_instagram_white.svg"></a>
			<a href="mailto:info@2luca.co"><img src="../images/social_email_white.svg"></a>
		</div>

		<div class="foot_last_band">
			<p>&reg 2LUCA.co   |   Bogotá, Colombia   |   <span id="currYear">1990</span></p>
		</div>

	</div>


	<!-- MODAL DE LOGIN -->
	<div class="msg-box-container" id="msg-box-container">
		<div class="msg-box-1" id="msg-box-1">
			<p class="msgb-box-1-exit-mark" id="msgb-box-1-exit-mark" onclick="CloseLogInForm()">X</p>

			<p class="msgb-box-1-title" id="msgb-box-1-title"> INGRESAR</p>

			<form action="" method="POST" name="login-form" id="login-form">
				<input type="email" name="login-email" id="login-email" value="<?php if(isset($_COOKIE["rem_login_user"])) { echo $_COOKIE["rem_login_user"]; } ?>" placeholder="Email" required>
				<input type="password" name="login-pass" id="login-pass" placeholder="Contraseña" required>
				<p class="msg-box-1-forgotpass"><a href="forgotpassword.php">¿Olvidaste tu contraseña?</a></p>
				<div class="checkboxCont"><input type="checkbox" name='login-rememberme' id="login-rememberme" <?php if(isset($_COOKIE["rem_remember"])) { ?> checked <?php } ?>> <label for="login-rememberme">Recordarme</label></div>
				
				<span class="simpleMsg" id="simpleMsgLogin"></span>
        		<span class="errorMsg" id="errorMsgLogin"></span>
				<input type="submit" name="login-submit" id="login-submit" value="INGRESAR">
				<p class="msg-box-1-reg">¿No tienes cuenta aún? <a href="register.php">Regístrate gratis</a></p>
			</form>

		</div>	
	</div>

	

</body>
</html>